/**
 * This class is responsible for validate and register
 * the gifts selected in cart
 *
 * @package Esmart
 * @subpackage Gift
 * @author Evandro Veloso Gomes <evandro.gomes@e-smart.com.br>
 */

var Esmart_Gifts_Selection = Class.create({

    initialize : function() {

        $j("#count-selected").html($j("[name^='selected-gifts']:checked").length  + ' Selecionado');

        if($j("[name^='selected-gifts']:checked").length > 1) {
            $j("#count-selected").html($j("[name^='selected-gifts']:checked").length  + ' Selecionados');
        }

        if ($j("[name^='selected-gifts']:checked").length == $j('#max-select-gifts').val()) {
            $j("[name^='selected-gifts']:not(:checked)").prop("disabled", true);
        } else {
            $j("[name^='selected-gifts']:not(:checked)").prop("disabled", false);
        }

        $j("[name^='selected-gifts']").on("click", function () {

            if ($j("[name^='selected-gifts']:checked").length == $j('#max-select-gifts').val()) {
                $j("[name^='selected-gifts']:not(:checked)").prop("disabled", true);
            } else {
                $j("[name^='selected-gifts']:not(:checked)").prop("disabled", false);
            }

            var self    = $j(this);
            var product = $j(this).val();
            var checked = ($j(this).prop('checked') ? 1 : 0);

            $j.ajax({
                type: 'GET',
                url: BASE_URL + 'gift/selection/register/product/' + product + '/checked/' + checked,
                async: true,
                success: function (response) {
                    if (response) {
                        self.prop('checked', false);

                        alert(response);
                    }
                    
                    $j("#count-selected").html($j("[name^='selected-gifts']:checked").length + ' Selecionado');

                    if($j("[name^='selected-gifts']:checked").length > 1) {
                        $j("#count-selected").html($j("[name^='selected-gifts']:checked").length + ' Selecionados');
                    }

                },
            });
        });
    }
});

var giftsSelection = new Esmart_Gifts_Selection();