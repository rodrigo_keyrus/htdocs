/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category      Esmart
 * @package       Esmart_InfiniteScroll
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

var InfiniteScroll = Class.create();
InfiniteScroll.prototype = {
    currentParams: '',

    /**
     * Initialize constructor
     *
     * @param {int} curPage
     * @param {int} lastPage
     * @param {string} curMode
     * @param {string} pagerKey
     */
    initialize: function(curPage, lastPage, curMode, pagerKey)
    {
        this.curPage    = (curPage + 1);
        this.lastPage   = lastPage;
        this.products   = '.category-products .products-' + curMode;
        this.pagerKey   = pagerKey;
        this.ajaxKey    = 'isAjax';
        this.currentUrl = window.location.href;

        this.button = $('see_more_products');
        this.load   = $('more_products_load');
        this.result = $('more_products_result');

        this.addButtonEvent();
    },

    /**
     * Add button event
     *
     * @returns {InfiniteScroll}
     */
    addButtonEvent: function()
    {
        var $this = this;

        jQuery('#see_more_products').click(function (event) {
            event.stopImmediatePropagation();

            var button = this;

            new Ajax.Request(window.location.pathname, {
                method: 'GET',
                parameters: $this.getRequestParams(),
                onCreate: function()
                {
                    button.stopObserving('click');

                    jQuery('#more_products_load').show();

                    //Lazyload for new images after load ajax
                    jQuery('#more_products_load img').lazyload();
                },
                onSuccess: function(transport)
                {
                    var response = transport.responseText;

                    try {
                        response = response.evalJSON();
                        response = response.list;
                    } catch (e) {}

                    $this.result.update(response);

                    $this.result.select($this.products + ' .item').each(function(element) {
                        $$($this.products)[0].insert(element.outerHTML);
                    });

                    $this.result.update('');

                    if ($this.lastPage === $this.curPage) {
                        button.hide();
                    }

                    $this.curPage++;

                    //Lazyload for new images after load ajax
                    jQuery('.manufacturer-info .item img').lazyload();
                    jQuery('.products-grid .item img').lazyload();

                    return this;
                },
                onComplete: function()
                {
                    jQuery('#more_products_load').hide();

                    button.observe('click', $this.addButtonEvent());

                    if (CachePrice && (CachePrice instanceof BSeller_CachePrice)) {
                        CachePrice.getProducts();
                    }
                }
            });
        });

        return this;
    },

    /**
     * Return request params
     *
     * @returns Object
     */
    getRequestParams: function()
    {
        if (this.currentUrl == null) {
            this.currentUrl = window.location.href;
        }

        var locationQueryParams = (this.currentUrl.match(/\?./))
            ? this.currentUrl.toQueryParams()
            : {};

        locationQueryParams[this.pagerKey] = this.curPage;
        locationQueryParams[this.ajaxKey]  = 1;

        return locationQueryParams;
    }
};
