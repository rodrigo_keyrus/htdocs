/**
 * BSeller Platform | B2W - Companhia Digital
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category  Esmart
 * @package   Esmart_Catalog_Product_Bundle
 *
 * @copyright Copyright (c) 2018 B2W Digital - BSeller Platform. (http://www.bseller.com.br)
 *
 * @author   Jonas Oliveira <jonas.oliveira@e-smart.com.br>
 */

var Esmart_Catalog_Product_Bundle = Class.create({

    /**
     * Initialize defaults configs
     *
     * @param {object} installments
     */
    initialize: function(currencyFormat, installments) {
        this.currencyFormat = currencyFormat;
        this.installments = installments;

        this.reloadContent();
    },

    /**
     * Return installments by value
     * @TODO Method cloned of Uecommerce Mundipagg module
     *
     * @param {float|int} amount
     * @returns {number}
     */
    getInstallments: function(amount)
    {
        var curMinimalBoundary = -1;
        var resultingFreq      = 1;

        for (var i = 0; i < this.installments.length; i++) {
            var boundary  = parseFloat(this.installments[i][0]);
            var frequency = parseInt(this.installments[i][1]);

            if (amount <= boundary && (boundary <= curMinimalBoundary || curMinimalBoundary === -1)) {
                curMinimalBoundary = boundary;
                resultingFreq      = frequency;
            }

            if (isNaN(boundary) === true && curMinimalBoundary === -1) {
                resultingFreq = frequency;
            }
        }

        return resultingFreq;
    },

    /**
     * Reload info price content
     *
     * @returns {CatalogProductBundle}
     */
    reloadContent: function()
    {
        var instNumberEle = $$('.installments .number')[0];
        var instValueEle  = $$('.installments .value')[0];
        var bcPointEle    = $$('.bc-point-view .value')[0];
        var finalPrice    = $$('.full-product-price .price')[0].innerHTML;

        /**
         * Final price by bundle
         */
        finalPrice = finalPrice.replace('R$', '');
        finalPrice = finalPrice.replace('.', '');
        finalPrice = parseFloat(finalPrice);

        /**
         * Products selected beauty club points
         */
        bcPointEle.update(Math.ceil(finalPrice));

        /**
         * Installments number
         */
        var installments = this.getInstallments(finalPrice);

        /**
         * Products selected installments number
         */
        instNumberEle.update(installments);
        instValueEle.update(formatCurrency((finalPrice / installments), this.currencyFormat));
    },

    /**
     * Select item
     *
     * @param {object} element
     * @param {object} item
     */
    selectItem: function(element, item) {
        if($j(element).hasClass('open')) {

            $j(element).removeClass('open');

            $j(element).mCustomScrollbar('destroy');

            $j(element).find('label').removeClass('selected');

            $j(item).prev().addClass('selected');

            this.scrollTop(element, $j(item).attr('data-top'));

            return false;
        }

        this.selectedDefaultItem();
        $j(element).addClass('open');

        $j(element).mCustomScrollbar({
            theme  : 'dark-2'
        });
    },

    /**
     * Scroll top
     *
     * @param {object} element
     * @param {object} top
     */
    scrollTop: function(element, top) {
        $j(element).scrollTop(top);
    },

    /**
     * Selected item by admin configured
     */
    selectedDefaultItem: function() {
        var self = this;

        $j('.item-bundle-selected').removeClass('open');
        $j('.item-bundle-selected').mCustomScrollbar('destroy');

        $j('.item-bundle-selected').each(function(index, element){
            var item = $j(element).find('label.selected');

            self.scrollTop('#' + item.parent().parent().attr('id'), item.next().attr('data-top'));
        });
    },

    removeSelecton: function (item, itemNone, remove, add, checked, unchecked, locked) {
        var self = this;

        $j(item).each(function () {
            $j(this).children("input").removeClass("last-selected");

            if ($j(this).children("input").is(":checked")) {
                $j(this).children("input").prop("checked", false);
                $j(this).children("input").addClass("last-selected");
                $j('#product_addtocart_form input[name="'+ $j(itemNone).attr('name') +'"]').remove();
            }
        });
        $j(itemNone).trigger("click");
        $j(remove).hide();
        $j(add).show();
        $j(checked).hide();
        $j(unchecked).show();
        $j(locked).show();

    },

    addSelecton: function (item, add, remove, unchecked, checked, locked) {
        var self = this;
        var currentItem = "";
        $j(item).each(function () {

            if ($j(this).children("input").hasClass("last-selected")) {
                $j(this).children("input").prop("checked", true);
                $j(this).children("input").removeClass("last-selected");
                $j(this).children("input").trigger("click");
                currentItem = $j(this).children("input");
            }
        });
        $j(currentItem).trigger("click");
        self.selectItem(currentItem);
        $j(add).hide();
        $j(remove).show();
        $j(unchecked).hide();
        $j(checked).show();
        $j(locked).hide();

    }

});	