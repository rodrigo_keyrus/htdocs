// File: sephora.js
/**
 * @package ESmart
 * @subpackage BannerCategory
 * @author Jonas Oliveira
 */
var ESmart_BannerCategory = Class.create();
ESmart_BannerCategory.prototype = {

    /*
    * @todo Constructor
    */
    initialize: function() {

    },

    /**
     * @todo initializes the Slider
     * @param sliderBanner
     * @param navigation
     */
    initializeSlider: function(sliderBanner, animation, speed, navigation) {
        jQuery(sliderBanner).flexslider({
            animation: animation,
            slideshowSpeed: speed,
            start: function(slider){
                jQuery(slider).find(navigation).fadeIn(300);
            }
        });
    },

    /**
     * @todo reverses the positions of the advantages bar with the title bar if there is a banner
     * @param containerBanner
     * @param titleCategory
     * @param benefitsBar
     */
    invertPosition: function(containerBanner, titleCategory, benefitsBar){
        if (jQuery(containerBanner).length) {
            jQuery(titleCategory).remove().insertAfter(jQuery(benefitsBar));
        };
    },

    /**
     * @todo shows the banner and avoids loading delay
     * @param viewBanner
     * @param containerSlider
     */
    showBanner: function(viewBanner, containerSlider){
        jQuery(viewBanner).css('overflow', 'visible');
        jQuery(containerSlider).removeClass('no-display');
    }
}