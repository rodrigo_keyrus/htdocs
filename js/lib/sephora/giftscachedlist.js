/**
 * This class is responsible for
 * get the gift's cache data
 *
 * @package BSeller
 * @subpackage CacheProductData
 * @author Evandro Veloso Gomes <evandro.gomes@e-smart.com.br>
 */

var BSeller_GiftCache = Class.create({

    initialize : function() {
        this.getProducts();
    },

    getProducts : function() {

        var products = new Array();
        $j("[id^='add-product-']").each(function() {
            products.push($j(this).data('productid'));
        });

        if (products.length == 0) {
            return;
        }

        $j.ajax({
            type: 'GET',
            url: BASE_URL + 'restapi/gifts/getlist',
            data: {products: products},
            async: false,
            dataType: 'json',
            success: function(response) {

                $j("[id^='add-product-']").each(function() {
                    var product = $j(this).data('productid');
                    var object  = $j(this);

                    if (!response[product]) {
                        object.remove();

                        return true;
                    }

                    if (response[product].is_salable != 1) {
                        object.removeClass('no-display');
                        object.addClass('out-of-stock');

                        $j('#btn-want-' + product).addClass('no-display');
                        $j('#btn-out-of-stock-' + product).removeClass('no-display');
                        $j('#out-of-stock-message-' + product).removeClass('no-display');
                        $j('#flag-agreements-' + product).addClass('no-display');

                        $j(this).find('a').css('pointer-events', 'none');

                    } else {
                        object.removeClass('no-display');

                        $j('#btn-want-' + product).removeClass('no-display');
                        $j('#btn-out-of-stock-' + product).addClass('no-display');
                        $j('#out-of-stock-message-' + product).addClass('no-display');

                        $j('#flag-agreements-' + product).removeClass('no-display');

                        $j(this).find('a').css('pointer-events', 'auto');
                    }
                });
            }
        });
    }
});