/**
 * This class is responsible for
 * manipulate product and
 * get all cached data of current product
 * @package BSeller
 * @subpackage CacheProductData
 * @author Evandro Veloso Gomes <evandro.gomes@e-smart.com.br>
 */

var BSeller_CachePrice = Class.create({

    initialize : function(installments) {
        self = this;

        self.installments = installments;

        self.getProducts();
    },

    getProducts : function() {

        self = this;

        var products = new Array();
        $j("[id^='add-product-']").each(function() {
            var product = $j(this).data('productid');
            if ($j('#product-price-' + product).html().trim() == '') {
                products.push(product);

                $j('#btn-buy-' + product).addClass('no-display');
            }
        });

        if (products.length == 0) {
            return;
        }

        $j.ajax({
            type: 'GET',
            url: BASE_URL + 'restapi/price/getpriceslist',
            data: {products: products},
            async: false,
            dataType: 'json',
            success: function(response) {

                $j("[id^='add-product-']").each(function() {
                    var product = $j(this).data('productid');
                    var object  = $j(this);

                    if ($j('#product-price-' + product).html().trim() != '') {
                        return true;
                    }

                    if (!response[product]) {
                        object.remove();

                        return true;
                    }

                    if (response[product].is_salable == 1) {
                        object.removeClass('out-of-stock');

                        price         = response[product].price;
                        currentPrice  = response[product].current_price;
                        showDiscount  = response[product].show_discount;
                        free_shipping = response[product].free_shipping;

                        installments = self.getInstallments(currentPrice);

                        object.attr('data-product-price', price);
                        object.attr('data-product-final-price', currentPrice);

                        $j('#prices-' + product).removeClass('no-display');

                        $j('#product-price-' + product).html('');
                        if (currentPrice != price) {
                            $j('#old-price-' + product).html(self.getFormattedPrice(price));
                        }

                        $j('#discount-' + product).addClass('no-display');
                        if (showDiscount) {
                            $j('#discount-' + product).removeClass('no-display');
                            $j('#discount-' + product).html('<span class="t-right flag-stamp-text" ><b>' + showDiscount + '%</b> OFF</span>');
                        }

                        $j('#product-price-' + product).html(self.getFormattedPrice(currentPrice));
                        $j('#installments-' + product).html(installments + 'x de ' + self.getFormattedPrice(currentPrice / installments));

                        $j('#btn-buy-' + product).removeClass('no-display');
                        $j('#btn-out-of-stock-' + product).addClass('no-display');
                        $j('#out-of-stock-message-' + product).addClass('no-display');

                        $j('#prices-' + product).removeClass('no-display');
                        $j('#flags-' + product).removeClass('no-display');
                        $j('#ratings-' + product).removeClass('no-display');

                        if (free_shipping) {
                            $j('#frete-gratis-' + product).removeClass('no-display');
                        } else {
                            $j('#frete-gratis-' + product).addClass('no-display');
                        }
                    } else {
                        object.addClass('out-of-stock');

                        $j('#btn-buy-' + product).addClass('no-display');
                        $j('#btn-out-of-stock-' + product).removeClass('no-display');
                        $j('#out-of-stock-message-' + product).removeClass('no-display');

                        $j('#prices-' + product).addClass('no-display');
                        $j('#flags-' + product).addClass('no-display');
                        $j('#ratings-' + product).addClass('no-display');
                        $j('#frete-gratis-' + product).addClass('no-display');
                    }

                    $j('#btn-buy-' + product).removeClass('no-display');
                });
            }
        });
    },

    getFormattedPrice: function(price) {
        return 'R$ ' + parseFloat(price).toFixed(2).replace('.', ',');
    },

    /**
     * Return installments by value
     *
     * @param {float|int} amount
     * @returns {number}
     */
    getInstallments: function(amount)
    {
        var curMinimalBoundary = -1;
        var resultingFreq      = 1;
        for (var i = 0; i < this.installments.length; i++) {
            var boundary  = parseFloat(this.installments[i][0]);
            var frequency = parseInt(this.installments[i][1]);

            if (amount <= boundary && (boundary <= curMinimalBoundary || curMinimalBoundary === -1)) {
                curMinimalBoundary = boundary;
                resultingFreq      = frequency;
            }

            if (isNaN(boundary) === true && curMinimalBoundary === -1) {
                resultingFreq = frequency;
            }
        }

        return resultingFreq;
    }
});