/**
 * This class is responsible for
 * check if is a store website and
 * user is logged in
 * @package BitTools
 * @subpackage StoreRegistration
 * @author Evandro Veloso Gomes <evandro.gomes@e-smart.com.br>
 */

var BitTools_StoreRegistration = Class.create({

    initialize : function() {
        self = this;

        self.checkSeller();
    },

    checkSeller : function() {

        $j.ajax({
            type: 'POST',
            url: BASE_URL + 'bittools_storeregistration/account/redirectSeller',
            data: {currentURL: document.URL},
            dataType: 'JSON',
            async: true,
            success: function(response) {
                if (response.url) {
                    window.location.href = response.url;
                } else if (response.name) {
                    $j('#cast-bar').removeClass('no-display');
                    $j('#cast-bar-name').html(response.name);
                } else {
                    $j('#cast-bar').remove();
                }
            }
        });
    },
});

new BitTools_StoreRegistration;