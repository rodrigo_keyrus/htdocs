/**
 * This class is responsible for
 * manipulate product and
 * get all cached price of current product
 * @package BSeller
 * @subpackage CacheProductData
 * @author Evandro Veloso Gomes
 */
var BSeller_CachePrice = Class.create({

    response : false,

    initialize : function(installments) {
        $j('.product-options-bottom').addClass('no-display');

        self = this;

        this.installments = installments;

        $j(document).ready(function() {
            self.getProducts();
        });
    },

    getProducts : function() {
        self = this;

        if ($j(".bundle-option-select").length == 0) {
            return;
        }

        parentId = $j('input[name=product]').val();

        if (self.response == false) {
            $j.ajax({
                type: 'GET',
                url: BASE_URL + 'restapi/price/getpricesbyparent/product/' + parentId,
                async: false,
                dataType: 'json',
                success: function (response) {
                    self.response = response;
                }
            });
        }

        self.update();
    },

    update : function() {
        var response = this.response;

        $j(".bundle-option-select").each(function(index) {

            var object  = $j(this);

            product = object.data('productid');

            if (typeof response[product] === 'undefined') {
                object.parent().remove();

                if ($j(".bundle-option-select:enabled").length) {
                    $j(".bundle-option-select:enabled").first().click();
                    $j(".bundle-option-select:enabled").first().click();
                }

                return true;
            }

            price        = response[product].price;
            currentPrice = response[product].current_price;
            is_salable   = response[product].is_salable;

            object.attr('data-product-price', price);

            if (typeof bundle !== 'undefined') {
                bundle.config.options[$j(this).data('option')].selections[$j(this).data('selection')].price = price;
                bundle.config.options[$j(this).data('option')].selections[$j(this).data('selection')].tierPrice = price;
                bundle.config.options[$j(this).data('option')].selections[$j(this).data('selection')].priceInclTax = currentPrice;
                bundle.config.options[$j(this).data('option')].selections[$j(this).data('selection')].priceExclTax = currentPrice;

                if (bundle.config.options[$j(this).data('option')].selections[$j(this).data('selection')].specialPrice) {
                    bundle.config.options[$j(this).data('option')].selections[$j(this).data('selection')].specialPrice = currentPrice;
                }
            }

            if (is_salable) {
                $j('#containter-bundle-option-' + $j(this).data('option')).removeClass('no-display');
                $j('#items-bundle-option-' + $j(this).data('option')).removeClass('no-display');
                object.parent().removeClass('no-display');

                if (currentPrice != price) {
                    $j('#old-price-' + product).html(self.getFormattedPrice(price));
                }

                $j('#product-price-' + product).html(self.getFormattedPrice(currentPrice));

                object.attr('data-final-price', self.getFormattedPrice(currentPrice));

                object.prop("disabled", false);
                object.parent().find('label > div').css("opacity", '');
            } else {
                object.prop("disabled", true);
                object.parent().find('label > div').css("opacity", 0.2);
            }
        });

        if ($j(".bundle-option-select:enabled").length) {

            $j(".item-bundle-selected").each(function() {
                $j(this).find('input:enabled:first').click();
                $j(this).find('input:enabled:first').click();
            });

            $j('.product-options-bottom').removeClass('no-display');
            $j('.add-to-cart').removeClass('no-display');
            $j('.add-to-cart-wrapper').removeClass('no-display');
            $j('.price-as-configured').removeClass('no-display');
        } else {
            $j('.search-option').addClass('no-display');
            $j('.item-bundle-selected').addClass('no-display');

            $j('.product-options-bottom').addClass('no-display');
            $j('.add-to-cart').addClass('no-display');
            $j('.add-to-cart-wrapper').addClass('no-display');
            $j('.price-as-configured').addClass('no-display');
        }
    },

    getFormattedPrice: function(price) {
        return 'R$ ' + parseFloat(price).toFixed(2).replace('.', ',');
    },
});