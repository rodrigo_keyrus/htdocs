/**
 * This class is responsible for
 * manipulate product and
 * get all cached price of current product
 * @package BSeller
 * @subpackage CacheProductData
 * @author Fernando Alves
 */
var BSeller_CachePrice = Class.create({

    response : false,

    initialize : function(installments) {
        self = this;

        this.installments = installments;

        $j(document).ready(function() {
            self.getProducts();
        });
    },

    getProducts : function() {
        self = this;

        if ($j("[id^='add-product-option-']").length == 0) {
            return;
        }

        $j('.add-to-cart').addClass('no-display');
        $j('.add-to-cart-wrapper').addClass('no-display');
        $j('.search-option').addClass('no-display');
        $j('.discount').addClass('no-display');

        parentId = $j('input[name=product]').val();

        if (self.response == false) {
            $j.ajax({
                type: 'GET',
                url: BASE_URL + 'restapi/price/getpricesbyparent/product/' + parentId,
                async: false,
                dataType: 'json',
                success: function (response) {
                    self.response = response;
                }
            });
        }

        self.update();
    },

    update : function() {
        var response = this.response;

        $j("[id^='add-product-option-']").each(function(index) {

            var object  = $j(this);

            product = $j(this).data('productid');

            if (typeof response[product] === 'undefined') {

                object.remove();

                if ($j("[id^='add-product-option-']").length > 0) {
                    $j("[id^='add-product-option-']").first().click();

                    if ($j("[id^='add-product-option-']").length > 1) {
                        $j('.search-option').addClass('no-display');
                    }
                } else {
                    $j('.head-text').addClass('no-display');
                    $j('.option-selected').addClass('no-display');
                    $j('.prices').addClass('no-display');
                    $j('#bseller_productalert-subscribe-news').addClass('no-display');
                    $j('#msg-no-products').removeClass('no-display');
                }

                return true;
            }

            price        = response[product].price;
            currentPrice = response[product].current_price;
            is_salable   = response[product].is_salable;

            object.attr('data-product-price', price);
            object.attr('data-product-final-price', currentPrice);

            if (is_salable) {
                object.removeClass('unavailable');

                $j('#prices-' + product).removeClass('no-display');

                if (currentPrice != price) {
                    $j('#old-price-' + product).html(self.getFormattedPrice(price));
                }

                showDiscount = response[product].show_discount;
                if (showDiscount) {
                    $j('#discount-' + product).removeClass('no-display');
                    $j('#discount-' + product).html(showDiscount + '% OFF');

                    $j('#tag-special-price-' + product).removeClass('no-display');
                }

                $j('#product-price-' + product).html(self.getFormattedPrice(currentPrice));

                installments = self.getInstallments(currentPrice);
                $j('#installments-' + product).html(installments + 'x de ' + self.getFormattedPrice(currentPrice / installments));

                $j('#sold-out-' + product).addClass('no-display');

            } else {
                object.addClass('unavailable');
                $j('#sold-out-' + product).removeClass('no-display');
                $j('#prices-' + product).addClass('no-display');
                $j('#discount-' + product).addClass('no-display');

                if (typeof BSeller_ProductAlert !== 'undefined') {

                    if (index == 0) {
                        BSeller_ProductAlert.setProductId(product);
                    }

                    $j(this).click(function() {
                        BSeller_ProductAlert.setProductId(product);
                    });
                }
            }

            if (index == 0) {
                $j('#super_group_' + product).click();
            }
        });

        $j('.add-to-cart').removeClass('no-display');
        $j('.add-to-cart-wrapper').removeClass('no-display');

        if ($j("[id^='add-product-option-']").length <= 0) {
            $j('.head-text').addClass('no-display');
            $j('#msg-no-products').removeClass('no-display');
            $j('.option-selected').addClass('no-display');
            $j('.prices').addClass('no-display');
            $j('#bseller_productalert-subscribe-news').addClass('no-display');
        } else if ($j("[id^='add-product-option-']").length > 1) {
            $j('.search-option').removeClass('no-display');
        }
    },

    getFormattedPrice: function(price) {
        return 'R$ ' + parseFloat(price).toFixed(2).replace('.', ',');
    },

    /**
     * Return installments by value
     *
     * @param {float|int} amount
     * @returns {number}
     */
    getInstallments: function(amount)
    {
        var curMinimalBoundary = -1;
        var resultingFreq      = 1;
        for (var i = 0; i < this.installments.length; i++) {
            var boundary  = parseFloat(this.installments[i][0]);
            var frequency = parseInt(this.installments[i][1]);

            if (amount <= boundary && (boundary <= curMinimalBoundary || curMinimalBoundary === -1)) {
                curMinimalBoundary = boundary;
                resultingFreq      = frequency;
            }

            if (isNaN(boundary) === true && curMinimalBoundary === -1) {
                resultingFreq = frequency;
            }
        }

        return resultingFreq;
    }
});