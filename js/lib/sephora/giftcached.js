/**
 * This class is responsible for
 * get the gift's cache data
 *
 * @package BSeller
 * @subpackage CacheProductData
 * @author Evandro Veloso Gomes <evandro.gomes@e-smart.com.br>
 */

var BSeller_GiftCache = Class.create({

    initialize : function() {
        this.getProduct();
    },

    getProduct : function() {

        var products = new Array();
        $j("[id^='gift-product-']").each(function() {
            products.push($j(this).data('productid'));
        });

        if (products.length == 0) {
            return;
        }

        $j.ajax({
            type: 'GET',
            url: BASE_URL + 'restapi/gifts/getList',
            data: {products: products},
            async: false,
            dataType: 'json',
            success: function(response) {
                $j("[id^='gift-product-']").each(function() {
                    var product = $j(this).data('productid');
                    var object  = $j(this);

                    if (!response[product]) {
                        object.remove();
                    } else if (response[product].is_salable != 1) {
                        object.addClass('no-display');
                    } else {
                        object.removeClass('no-display');
                    }
                });
            }
        });
    }
});