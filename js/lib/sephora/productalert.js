
/**
 * This class is responsible for
 * manipulate subscribe customer
 * in newsletter
 * @package BSeller
 * @subpackage ProductAlert
 * @author Fernando Alves
 * @author Jonas Oliveira
 */
var BSeller_ProductAlert = Class.create();
BSeller_ProductAlert.prototype = {

    /**
     * @function constructor method
     */
    initialize: function() {
        this.subscribe();
        this.showSubscribe();
        this.setProductId();
        this.checkStockClass();
    },

    /**
     * @function performs the validations and runs ajax
     */
    subscribe: function() {
        var self = this;
        $j('#bseller_productalert-subscribe').click(function() {
            if (!self.validateNameSubscribe() && !self.validateEmailSubscribe()) {
                self.bothFieldNotPass();
                return;
            }
            if (!self.validateNameSubscribe() ) {
                self.nameNotPass();
                return;
            }
            if (!self.validateEmailSubscribe()) {
                self.emailNotPass();
                return;
            }

            self.ajaxSubscribe();
        });
    },

    /**
     * @function ajax function that receives the controller
     */
    ajaxSubscribe: function() {
        $j.ajax({
            type: 'POST',
            url: BASE_URL + 'productalert/subscribe/news',
            data: {
                name: $j('#subscribe_name').val(),
                email: $j('#subscribe_email').val(),
                product_id: $j('#subscribe_productid').val()
            },
            beforeSend: function(){
                $j('.loader-subscribe-container').show();
            },
            success: function(response) {
                $j('#subscribe-not-pass').text('');
                $j('#subscribe-not-pass').hide();

                $j('#subscribe-pass-success').text(Translator.translate('Warning successfully registered.'));
                $j('#subscribe-pass-success').show();
                $j('#bseller_productalert-subscribe-news').hide();
                $j('.loader-subscribe-container').hide();
                $j('#subscribe_name').val('');
                $j('input[name=subscribe_email]').val('');
            },
            error: function(response) {
                $j('#subscribe-not-pass').text(Translator.translate('Error registering'));
            }
        });
    },

    /**
     * @function Validates the field email
     */
    validateEmailSubscribe: function() {
        var email =  $j('#subscribe_email').val();
        var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    },

    /**
     * @function Validates the field name
     */
    validateNameSubscribe: function() {
        var name =  $j('#subscribe_name').val();
        var regex = /^[a-zA-Z\u00C0-\u00FF ]{2,30}$/;
        return regex.test(name);
    },

    /**
     * @function shows message if the email field does not pass validation
     */
    emailNotPass: function() {
        $j('#subscribe_email').val('');
        $j('#subscribe-not-pass').text(Translator.translate('Please enter a valid email address'));
        $j('#subscribe-not-pass').show();
    },

    /**
     * @function shows message if the name field does not pass validation
     */
    nameNotPass: function() {
        $j('#subscribe_name').val('');
        $j('#subscribe-not-pass').text(Translator.translate('Please enter a valid name'));
        $j('#subscribe-not-pass').show();
    },

    /**
     * @function shows message if both fields do not pass validation
     */
    bothFieldNotPass: function() {
        $j('#subscribe_email').val('');
        $j('#subscribe_name').val('');

        $j('#subscribe-not-pass').text(Translator.translate('Enter valid name and email'));
        $j('#subscribe-not-pass').show();
    },

    /**
     * @function show form on click
     */
    showSubscribe: function() {
        var self = this;

        $j('.grouped-list .item input').on('click', function(){
            $j('.grouped-list .item input').prop('checked', false);
            $j(this).prop('checked', true);

            $j('#subscribe-pass-success').slideUp('slow');
            $j('#bseller_productalert-subscribe-news').slideDown('slow');

            self.showAddToCartButton(this);
        });
    },

    showAddToCartButton: function(obj) {
        if ($j(obj).parent().hasClass('unavailable')) {
            $j('.product-alert-subscribe-container').show();
            $j('.add-to-cart').hide();
        } else {
            $j('.product-alert-subscribe-container').hide();
            $j('.add-to-cart').show();
        }
    },

    /**
     * @function set Product Id in form
     */
    setProductId: function(productId) {
        $j('input[name=subscribe_productid]').val(productId);
    },

    /**
     * @function check item is in stock and add class 'no-stock'
     */
    checkStockClass: function () {
        jQuery('.item').click(function () {
            if (jQuery(this).hasClass('unavailable')) {
                jQuery('.price-info-grouped').addClass('no-stock');
            } else {
                jQuery('.price-info-grouped').removeClass('no-stock');
            }
        });
    }


};