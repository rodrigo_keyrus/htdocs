    /**
     * This class is responsible for
     * manipulate all data when
     * connect with social network
     * @package BSeller
     * @subpackage SocialConnect
     * @author Fernando Alves
     * @author jonas Oliveira
     */
var BSeller_SocialConnect = Class.create();
BSeller_SocialConnect.prototype = {

    initialize: function() {
        this.ajaxGetCustomerByCPF();
        this.formRegister();
    },

    ajaxGetCustomerByCPF: function() {
        self = this
        $j('#cpf').keyup(function(){
            if ($j(this).val().length == 14 && $j(this).val().indexOf('_') === -1) {
                $j('#overlay-loader-socialconnect').show();
                $j.ajax({
                    type: 'POST',
                    url: BASE_URL + 'socialconnect/facebook/cpfvalidate',
                    data: { cpf: $j(this).val()},
                    success: function(data){
                        self.ajaxRemoveSession();
                        if (data == 0) {
                            $j('#bseller-socialconnect_associate-div').hide();
                            $j('#bseller-socialconnect_btn-register-div').show();
                            $j('#bseller-socialconnect_button-register').prop('disabled', false);
                            $j('#bseller-socialconnect_step-title').html('Falta pouco... Só precisamos confirmar mais algumas informações:');
                            $j('#bseller-socialconnect_step-title').fadeIn(1500);
                            $j('#bseller-socialconnect_step-2').fadeIn(1500);
                            $j('#bseller-socialconnect_step-2-form').fadeIn(1500);
                        } else {
                            $j('#bseller-socialconnect_btn-register-div').hide();
                            $j('#bseller-socialconnect_associate-div').show();
                            $j('#bseller-socialconnect_associate-div').html(data);
                            $j('#bseller-socialconnect_step-2').hide();
                            $j('#bseller-socialconnect_step-2-form').hide();
                        }
                        $j('#overlay-loader-socialconnect').hide();
                    }
                })
            }
        })
    },

    ajaxValidateCustomerPassword: function() {
        $j.ajax({
            type: 'POST',
            url: BASE_URL + 'socialconnect/facebook/validatepassword',
            data: {
                password: $j('#password-check-owner').val(),
                encrypted_data: $j('#encrypted_data').val()
            },
            success: function(data) {
                if (data == 1) {
                    location.reload();
                } else {
                    $j('#bseller-socialconnect_error-message').html('Acesso ou senha inválida');
                }
            },
            error: function(data) {
                $j('#bseller-socialconnect_error-message').html('Acesso ou senha inválida');
            }
        });
    },

    ajaxRemoveSession: function() {
        $j.ajax({
            type: 'POST',
            url: BASE_URL + 'socialconnect/facebook/removesession'
        });
    },

    formRegister: function() {
        //<![CDATA[
        var dataFormRegister = new VarienForm('register-form', true);
        Form.getElements('register-form').each(function(element) {
            element.setAttribute('autocomplete', 'off');
        });

        if ($$('.error-msg').length) {
            var msg = $j('.error-msg').html();
            if (msg.indexOf('Por favor confirme seus dados antes de criar uma nova conta') > -1) {
                dataFormRegister.validator.validate();
                $j('#cpf').focus();
            }
        }
        //]]>
    }
}
