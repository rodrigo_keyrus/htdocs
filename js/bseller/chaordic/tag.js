/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Chaordic
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

var ChaordicTag = Class.create();
ChaordicTag.prototype = {
    /**
     * Local storage key
     *
     * @var {string}
     */
    localStorageKey: 'bseller_chaordic',

    /**
     * Session lifetime
     *
     * @var {int}
     */
    lifetime: 3600,

    /**
     * Initialize constructor
     */
    initialize: function()
    {
        window.chaordic_meta = {};
    },

    /**
     * Push tag data
     *
     * @param   {object} object
     * @returns {ChaordicTag}
     */
    push: function(object)
    {
        for (var key in object) {
            if (!object.hasOwnProperty(key)) {
                continue;
            }

            chaordic_meta[key] = object[key];
        }

        return this;
    },

    /**
     * Unset keys from array
     *
     * @param   {*|object} keys
     * @returns {ChaordicTag}
     */
    unset: function(keys)
    {
        if (typeof keys === 'string') {
            keys = [keys];
        }

        keys.each(function(key) {
            if (!chaordic_meta.hasOwnProperty(key)) {
                return this;
            }

            delete chaordic_meta[key];
        });

        return this;
    }
};
