/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_NoVarnish
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

var NoVarnish = Class.create();

NoVarnish.prototype = {
    /**
     * Initialize constructor
     */
    initialize: function()
    {
        var $this = this;

        /**
         * Default settings
         */
        this.containers  = {};
        this.curProtocol = location.protocol.replace(':', '');

        /**
         * Cookie settings
         */
        this.cookieData     = {};
        this.cookieKey      = 'bseller_novarnish';
        this.cookieLifetime = 3600;

        /**
         * Local storage Settings
         */
        this.localStorageKey      = 'bseller_novarnish';
        this.localStorageLifetime = 3600;

        /**
         * Generate block data to load after
         */
        $$('.novarnish').each(function(ele) {
            $this.setContainer(ele.getAttribute('id'), ele.getAttribute('data-name'));
        });
    },

    /**
     * Set block to load after
     *
     * @param   {string} elementId
     * @param   {string} blockName
     * @returns {NoVarnish}
     */
    setContainer: function(elementId, blockName)
    {
        this.containers[elementId] = { name: blockName };

        return this;
    },

    /**
     * Send request to load blocks
     *
     * @param   {boolean} forceRenew
     * @returns {NoVarnish}
     */
    request: function(forceRenew)
    {
        var $this = this;

        var localStorage = {};

        for (var containerKey in this.containers) {
            if (this.containers.hasOwnProperty(containerKey) === false) {
                continue;
            }

            /**
             * Protocols available
             */
            this.cookieData[containerKey] = { http: true, https: true };

            /**
             * Try to retrieve info from cookie
             */
            try {
                this.cookieData[containerKey] = this.getCookie(containerKey).evalJSON();
            } catch (e) {}

            /**
             * Retrieve data from local storage
             */
            localStorage[containerKey] = this.getLocalStorage(containerKey);

            /**
             * Check if has data in local storage and cookie was not storage
             */
            if ((localStorage[containerKey] !== false && this.cookieData[containerKey][this.curProtocol] === false)
                || forceRenew === true) {
                /**
                 * Update block html
                 */
                $(containerKey).update(localStorage[containerKey]);

                delete this.containers[containerKey];
            }
        }

        /**
         * Check if has blocks to render
         */
        if (!Object.keys(this.containers).length) {
            return this;
        }

        /**
         * Dispatch request to an internal controller
         */
        new Ajax.Request('/novarnish/loader/blocks', {
            method: 'POST',
            parameters: { containers: Object.toJSON($this.containers) },
            onCreate: function()
            {
                $this.containers = {};
            },
            onSuccess: function(transport)
            {
                var response = transport.responseText.evalJSON();

                /**
                 * Check if has response error
                 */
                if (response.error === true) {
                    throw response.message;
                }

                /**
                 * Replace containers with blocks
                 */
                try {
                    var containers = response['blocks'];

                    for (var containerKey in containers) {
                        if (containers.hasOwnProperty(containerKey) === false) {
                            continue;
                        }

                        /**
                         * Don't send request to update blocks for this protocol
                         */
                        $this.cookieData[containerKey][$this.curProtocol] = false;

                        /**
                         * Renew cookie data
                         */
                        $this.setCookie(containerKey, $this.cookieData[containerKey]);

                        /**
                         * Set the blocks in local storage
                         */
                        $this.setLocalStorage(containerKey, containers[containerKey]);

                        /**
                         * Update block html
                         */
                        $(containerKey).update(containers[containerKey]);
                    }
                } catch (e) {}
            }
        });

        return this;
    },

    /**
     * Set local storage data
     *
     * @param   {string} suffix
     * @param   {string|object} jsonData
     * @returns {boolean}
     */
    setLocalStorage: function(suffix, jsonData)
    {
        if (typeof Storage === 'undefined') {
            return false;
        }

        var key = this.localStorageKey + '_' + suffix;

        /**
         * Create time to expire local storage item
         */
        var data = {
            value: JSON.stringify(jsonData),
            timestamp: (new Date().getTime() + (this.localStorageLifetime * 1000))
        };

        try {
            localStorage.setItem(key, JSON.stringify(data));
        } catch(e) {
            return false;
        }

        return true;
    },

    /**
     * Return local storage data
     *
     * @param   {string} suffix
     * @returns {boolean|object}
     */
    getLocalStorage: function(suffix)
    {
        if (typeof (Storage) === 'undefined') {
            return false;
        }
        
        var key = this.localStorageKey + '_' + suffix;

        /**
         * Retrieve data from local storage
         */
        try {
            var data = JSON.parse(localStorage.getItem(key));
        } catch(e) {
            return false;
        }

        if (data === null) {
            return false;
        }

        var jsonData = (new Date().getTime() < data.timestamp && JSON.parse(data.value));

        /**
         * Set local storage data
         */
        if (jsonData !== false) {
            this.setLocalStorage(suffix, jsonData);
        }

        return jsonData;
    },

    /**
     * Set cookie data
     *
     * @param   {string} suffix
     * @param   {object} data
     * @returns {NoVarnish}
     */
    setCookie: function(suffix, data)
    {
        var key = this.cookieKey + '_' + suffix;

        Mage.Cookies.set(key, Object.toJSON(data), new Date(new Date().getTime() + (this.cookieLifetime * 1000)));

        return this;
    },

    /**
     * Return cookie data
     *
     * @param {string} suffix
     * @returns {object}
     */
    getCookie: function(suffix)
    {
        return Mage.Cookies.get(this.cookieKey + '_' + suffix);
    }
};

/**
 * Initialize instance
 */
document.observe('dom:loaded', function() {
    window.noVarnish = new NoVarnish();

    noVarnish.request(false);
});
