/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_ZipCode
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

var ZipCode = Class.create();
ZipCode.prototype = {
    postcode: 'postcode',
    city: 'city',
    region: 'region',
    region_id: 'region_id',
    region_name: '',
    cache: [],

    /**
     * Constructor initialize
     *
     * @param {string} streetId
     * @param {string} numberId
     * @param {string} complementId
     * @param {string} neighborhoodId
     * @param {string} url
     * @returns {ZipCode}
     */
    initialize: function(streetId, numberId, complementId, neighborhoodId, url)
    {
        this.street       = streetId;
        this.neighborhood = neighborhoodId;
        this.number       = numberId;
        this.complement   = complementId;
        this.url          = url;

        this.prepareEvents();

        return this;
    },

    /**
     * Prepare input events
     *
     * @returns {ZipCode}
     */
    prepareEvents: function()
    {
        var $this = this;

        $$('input[name*=' + this.postcode + ']').each(function(element) {
            element.stopObserving('blur');

            element.observe('blur', function() {
                $this.removeFailedMsg(element);

                var value = element.value.replace(/[^\d]/g, '');

                if (value.empty() || value.length !== 8) {
                    $this.addFailedMsg(element);

                    return this;
                }

                var form  = element.up('form');
                var cache = $this.cache[element.readAttribute('id') + ':' + value];

                if (cache !== undefined) {
                    $this.insertData(form, cache);

                    return this;
                }

                $this.dispatchRequest(element, form, value);
            });
        });

        return this;
    },

    /**
     * Dispatch request data
     *
     * @param {element} element
     * @param {element} form
     * @param {string} value
     * @returns {ZipCode}
     */
    dispatchRequest: function(element, form, value)
    {
        var $this = this;

        new Ajax.Request(this.url, {
            method: 'GET',
            parameters: { zipcode: value },
            onCreate: function()
            {
                element.disabled = true;

                $this.removeFailedMsg(element);
                $this.insertData(form, {});
            },
            onSuccess: function(transport)
            {
                var response = transport.responseText.evalJSON();

                if (response.error === 1) {
                    $this.addFailedMsg(element);

                    return this;
                }

                $this.cache[element.readAttribute('id') + ':' + value] = response;
                $this.insertData(form, response);

                return this;
            },
            onFailure: function()
            {
                $this.addFailedMsg(element);
            },
            onComplete: function()
            {
                element.disabled = false;
            }
        });

        return this;
    },

    /**
     * Insert data in form inputs
     *
     * @param {element} form
     * @param {object} data
     * @returns {ZipCode}
     */
    insertData: function(form, data)
    {
        form.select('input[id*=' + this.street + ']')[0].value       = data.street ? data.street : '';
        form.select('input[id*=' + this.neighborhood + ']')[0].value = data.neighborhood ? data.neighborhood : '';
        form.select('input[id*=' + this.city + ']')[0].value         = data.city ? data.city : '';
        form.select('input[id*=' + this.city + ']')[0].setAttribute('readonly', data.city ? true : false);
        form.select('input[id*=' + this.region + ']')[0].value       = data.region_name ? data.region_name : '';
        form.select('select[id*=' + this.region_id + ']')[0].value   = data.region_id ? data.region_id : '';
        form.select('select[id*=' + this.region_id + '] option')[0][data.region_id ? 'hide' : 'show']();
        form.select('select[id*=' + this.region_id + ']')[0].setStyle({ pointerEvents: data.region_id ? 'none' : 'all' });
        form.select('input[id*=' + this.complement + ']')[0].value   = '';
        form.select('input[id*=' + this.number + ']')[0].value       = '';

        return this;
    },

    /**
     * Add fail message
     *
     * @param {element} input
     * @returns {ZipCode}
     */
    addFailedMsg: function(input)
    {
        if (!input.hasClassName('validation-failed')) {
            input.addClassName('validation-failed');
        }

        if (input.next('.postcode-advice') === undefined) {
            input.insert({
                after : new Element('div', { class: 'validation-advice postcode-advice' })
                    .update(Translator.translate('Please enter a valid zip code.'))
            });
        }

        return this;
    },

    /**
     * Remove fail message
     *
     * @param {element} input
     * @returns {ZipCode}
     */
    removeFailedMsg: function(input)
    {
        var advice = input.next('.postcode-advice');

        if (advice !== undefined) {
            advice.remove();
        }

        if (input.hasClassName('validation-failed')) {
            input.removeClassName('validation-failed');
        }

        return this;
    }
};
