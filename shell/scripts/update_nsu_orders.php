<?php
require_once __DIR__ . '/../abstract.php';

class BSeller_Erp_Shell extends Mage_Shell_Abstract
{
    public function run()
    {
        try {
            $ordersToUpdate = array(
                "500649078" => "THIAGO G PEREIRA",
                "500649128" => "NURIA D PRATES",
                "500649505" => "ANNA A V FIGUEIREDO",
                "500649563" => "DENISE DORATIOTTO DE FARIAS",
                "500649987" => "EVERTON DE SOUZA SERRA",
                "500650324" => "JOCENIRA NOGUEIRA",
                "500650343" => "MARINA M S ALVES",
                "500650836" => "SOLANGE RITA DA SILVA",
                "500651256" => "CARLA R M DA COSTA",
                "500652633" => "ROSENEIDE BOMFIM");

            foreach ($ordersToUpdate as $key => $orderToUpdate) {
                $order = Mage::getModel('sales/order')->loadByIncrementId($key);
                $payment = $order->getPayment();
                if ($payment) {
                    $payment->setAdditionalInformation('mundipagg_creditcard_payment_capture_nsu', $orderToUpdate);
                    $payment->save();
                    echo "Payment update: # " . $key . "\n";
                } else {
                    echo "Payment not founded: # " . $key . "\n";
                }
            }
        } catch (Exception $ex) {

        }
        return $this;
    }
}

$shell = new BSeller_Erp_Shell();
$shell->run();