<?php
require_once __DIR__ . '/../../abstract.php';

class BSeller_Erp_Shell extends Mage_Shell_Abstract
{
    /**
     * Run script
     *
     * @return $this
     */
    public function run()
    {
        try {
            Mage::getModel('bsellererp_channel/cron')->import();
            Mage::getModel('bsellererp_payment_brands/cron')->import();
            Mage::getModel('bsellererp_payment/cron')->import();
            Mage::getModel('bsellererp_shipping/carrier_cron')->import();
            Mage::getModel('bsellererp_vale/cron')->import();
        } catch (Exception $ex) {
            echo $ex->getMessage();
        }
        return $this;
    }
}

$shell = new BSeller_Erp_Shell();
$shell->run();