<?php
require_once __DIR__ . '/../abstract.php';

class Esmart_Shell_Script_UpdateSapcode extends Mage_Shell_Abstract
{
    public function run()
    {
        try {
            echo 'Starting update cod_sap...' . PHP_EOL;

            $_arrayItems = array(
                7083,
                8723,
                14436,
                16205,
                17188,
                17197,
                17208,
                17216,
                17227,
                17238,
                17249,
                17260,
                18496,
                19064,
                19697,
                19975,
                21609,
                22053,
                23108,
                30277,
                33253,
                40619,
                41938,
                64944,
                68783,
                72853,
                72874,
            );

            $collection = Mage::getResourceModel('catalog/product_collection');
            $collection->addAttributeToFilter('type_id','simple')
                ->addAttributeToFilter('status','1');

            foreach ($_arrayItems as $item) {
               $product = Mage::getModel('catalog/product')->load($item);

               $sku = $product->getSku();

               $product->setCodSap($sku);

               $product->save();

               echo 'Update cod_sap product : ' . $product->getId() . PHP_EOL;
            }

            echo 'End update cod_sap...' . PHP_EOL;

        } catch (Exception $ex) {
            echo $ex->getMessage() . "\n";
        }
        return $this;
    }
}

$shell = new Esmart_Shell_Script_UpdateSapcode();
$shell->run();