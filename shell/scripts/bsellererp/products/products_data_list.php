<?php
require_once __DIR__ . '/../../abstract.php';

class BSeller_Erp_Shell extends Mage_Shell_Abstract
{
    /**
     * Run script
     *
     * @return $this
     */
    public function run()
    {
        try {
            $lines = file("data/20170110_skus_products_tests.csv", FILE_IGNORE_NEW_LINES);
            $lineCount = 0;
            $store_id = 1;
            $updated = 0;
            if (empty($lines)) {
                echo "\n\n" . "arquivo vazio ou não existe" . "\n\n";
            } else {
                foreach ($lines as $line) {
                    if ($lineCount == 0) {
                        $lineCount++;
                        continue;
                    }
                    $lineArray = explode(',', $line);
                    $sapCode = $lineArray[0];

                    $helper = Mage::helper('bsellererp_product');
                    $productId = $helper->getProductIdByAttribute('cod_sap', $sapCode);
                    $product = Mage::getModel('catalog/product')->load($productId);

                    if ($product && $product->getId()) {
                        echo $product->getCodSap() . ',' . $product->getPrice() . "\n";
                    } else {
                        echo('Produto não existe na base: ' . implode(',', $lineArray) . "\n");
                    }

                    $lineCount++;
                }
            }
        } catch (Exception $ex) {
            echo $ex->getMessage();
        }
        return $this;
    }
}

$shell = new BSeller_Erp_Shell();
$shell->run();
