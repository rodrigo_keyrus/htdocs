<?php
require_once __DIR__ . '/../../abstract.php';

class BSeller_Erp_Shell extends Mage_Shell_Abstract
{
    /**
     * Run script
     *
     * @return $this
     */
    public function run()
    {
        try {
            $lines = file("data/E2E 17W2 Codes AX-SAP.csv", FILE_IGNORE_NEW_LINES);
            $lineCount = 0;
            $store_id = 1;
            $updated = 0;
            if (empty($lines)) {
                echo "\n\n" . "arquivo vazio ou não existe" . "\n\n";
            } else {
                foreach ($lines as $line) {
                    if ($lineCount == 0) {
                        $lineCount++;
                        continue;
                    }
                    $lineArray = explode(',', $line);
                    $sku = $lineArray[1];
                    $product = Mage::getModel('catalog/product')->loadByAttribute('sku', str_pad($sku, 7, "0", STR_PAD_LEFT));

                    if ($product && $product->getId()) {
                        $action = Mage::getModel('catalog/resource_product_action');
                        $action->updateAttributes(array($product->getId()), array(
                            'cod_sap' => $lineArray[3]
                        ), $store_id);
                        $updated ++;
                    } else {
                        echo ('Produto não existe na base: ' . implode(',', $lineArray) . "\n");
                    }

                    $lineCount++;
                }
            }

            echo 'Atualizados: ' . $updated;
        } catch (Exception $ex) {
            echo $ex->getMessage();
        }
        return $this;
    }
}

$shell = new BSeller_Erp_Shell();
$shell->run();
