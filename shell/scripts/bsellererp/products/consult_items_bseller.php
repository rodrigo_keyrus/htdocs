<?php
require_once __DIR__ . '/../../../abstract.php';

class BSeller_Erp_Shell_Consult extends Mage_Shell_Abstract
{
    /**
     * Run script
     *
     * @return $this
     */
    public function run()
    {
        try {
            $collection = Mage::getModel('catalog/product')->getCollection();
            $collection->addAttributeToSelect(array('entity_id', 'sku', 'cod_sap'))
                ->addAttributeToFilter('status',1)
                ->addAttributeToFilter('type_id','simple');

            $success    = 0;
            $failed     = 0;
            $totalItems = 0;
            foreach ($collection->getItems() as $item) {
                if (!is_null($item->getCodSap())) {
                    $totalItems++;
                    $this->log('Check item(' . $item->getCodSap() . ') BSeller...');

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL,"http://backstg.bseller.com.br/api/itens?codigoTerceiro=" . $item->getCodSap());
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $headers = [
                        'Content-Type: application/json; charset=utf-8',
                        'X-Auth-Token: 615700A6B4A7241BE0536AF3A8C0ED63'
                    ];
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                    $result = curl_exec($ch);
                    curl_close($ch);

                    $jsonDecode = json_decode($result);

                    if (!empty($jsonDecode->content)) {
                        $success++;
                        //var_dump($jsonDecode->content);
                    } else {
                        $failed++;
                    }
                }
            }

            $this->log('Total products: ' . $totalItems);
            $this->log('Total in BSeller: ' . $success);
        } catch (Exception $ex) {
            echo $ex->getMessage();
        }
        return $this;
    }

    public function log($message)
    {
        Mage::log($message, null, 'consult_items_bseller_api.log');

        return $this;
    }
}

$shell = new BSeller_Erp_Shell_Consult();
$shell->run();
