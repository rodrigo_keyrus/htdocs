<?php
require_once __DIR__ . '/../../../abstract.php';

class BSeller_Erp_Shell extends Mage_Shell_Abstract
{
    /**
     * Run script
     *
     * @return $this
     */
    public function run()
    {
        try {
            $model = Mage::getModel('bsellererp_vale/cron')->import();
        } catch (Exception $ex) {

        }
        return $this;
    }
}

$shell = new BSeller_Erp_Shell();
$shell->run();
