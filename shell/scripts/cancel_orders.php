<?php
require_once __DIR__ . '/../abstract.php';

class BSeller_Erp_Shell extends Mage_Shell_Abstract
{
    public function run()
    {
        try {
            $ordersToCancel = array(
                500382696,
                500390745,
                500386460
            );

            foreach ($ordersToCancel as $incrementId) {
                $order = Mage::getModel('sales/order')->loadByIncrementId($incrementId);
                if ($order->getStatus() == 'canceled') {
                    continue;
                }
                echo 'Canceling order # ' . $incrementId . "\n";
                if ($order->canCancel()) {
                    $order->cancel();
                    $order->setState('canceled')->setStatus('canceled')->save();
                } else {
                    foreach ($order->getAllVisibleItems() as $item) {
                        if (!$item->getQtyToInvoice()) {
                            $item->setQtyInvoiced(0);
                            $item->save();
                        }
                    }
                    if ($order->canCancel()) {
                        $order->cancel();
                        $order->setState('canceled')->setStatus('canceled')->save();
                    }
                }
            }
        } catch (Exception $ex) {
            echo $ex->getMessage() . "\n";
        }
        return $this;
    }
}

$shell = new BSeller_Erp_Shell();
$shell->run();