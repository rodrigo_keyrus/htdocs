<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition End User License Agreement
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magento.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Shell
 * @copyright Copyright (c) 2006-2016 X.commerce, Inc. and affiliates (http://www.magento.com)
 * @license http://www.magento.com/license/enterprise-edition
 */

require_once '../../abstract.php';

/**
 * Magento Compiler Shell Script
 *
 * @category    Mage
 * @package     Mage_Shell
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Shell_Product_Links_Position extends Mage_Shell_Abstract
{

    const TIMER_CODE = 'product_link_position_update';

    protected $start  = null;
    protected $finish = null;

    /**
     * Run script
     *
     */
    public function run()
    {
        $this->start();

        $productId = null;

        if ($this->getArg('product_id')) {
            $productId = (int) $this->getArg('product_id');
        }

        /** @var Esmart_Catalog_Model_Resource_Attribute_Reference_Order $resource */
        $resource = Mage::getResourceModel('esmart_catalog/attribute_reference_order');
        $resource->updateProductLinksPositions($productId);

        $this->finish();
    }


    /**
     * @return $this
     */
    protected function start()
    {
        Varien_Profiler::enable();

        echo "Starting to process...\n";
        Varien_Profiler::start(self::TIMER_CODE);

        return $this;
    }


    /**
     * @return $this
     */
    protected function finish()
    {
        Varien_Profiler::stop(self::TIMER_CODE);

        if ($timers = Varien_Profiler::getTimers()) {
            $timer  = $timers[self::TIMER_CODE];

            $sum = $timer['sum'];

            echo "It took {$sum} to finish.\n";
        }

        echo "The process has finished!\n";

        return $this;
    }

}

(new Mage_Shell_Product_Links_Position())->run();
