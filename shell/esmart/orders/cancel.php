<?php

require_once realpath(__DIR__) . '/../../abstract.php';

class Esmart_Shell_Order_Cancel extends Mage_Shell_Abstract
{

    /**
     * Run script
     */
    public function run()
    {
        /** @var Mage_Sales_Model_Resource_Order_Payment_Collection $payments */
        $payments = Mage::getResourceModel('sales/order_payment_collection');
        $payments->addFieldToFilter('method', (new Uecommerce_Mundipagg_Model_Boleto())->getCode());
        $payments->addFieldToFilter('parent_id', ['lteq' => '1051']);

        /** @var Varien_Db_Select $select */
        $select = $payments->getSelect()
            ->reset('columns')
            ->columns('parent_id');

        $orderIds = $this->getReadConnection()->fetchCol($select);

        $openedStates = [
            Mage_Sales_Model_Order::STATE_NEW,
            Mage_Sales_Model_Order::STATE_HOLDED,
            Mage_Sales_Model_Order::STATE_PAYMENT_REVIEW,
            Mage_Sales_Model_Order::STATE_PENDING_PAYMENT,
        ];

        /** @var Mage_Sales_Model_Resource_Order_Collection $orderCollection */
        $orderCollection = Mage::getResourceModel('sales/order_collection');
        $orderCollection->addFieldToFilter('entity_id', ['in' => $orderIds]);
        $orderCollection->addFieldToFilter('state',     ['in' => $openedStates]);

        /** @var Mage_Sales_Model_Order $order */
        foreach ($orderCollection as $order) {
            $this->consolePrint($order);

            try {
                if ($order->canUnhold()) {
                    $order->unhold();
                }

                if (!$order->canCancel()) {
                    $this->consolePrint(null, 'Order cannot be cannot be canceled.');
                    continue;
                }

                $this->consolePrint(null, 'Canceling order.');
                $order->cancel()->save();
                $this->consolePrint(null, 'Order was successfully canceled!');
            } catch (Exception $e) {
                $this->consolePrint(null, 'Problems when trying to cancel the order.');
                $this->consolePrint(null, sprintf('Error: %s', $e->getMessage()));
            }
        }
    }


    /**
     * @return Magento_Db_Adapter_Pdo_Mysql
     */
    protected function getReadConnection()
    {
        return Mage::getModel('core/resource')->getConnection('read');
    }


    /**
     * @param Mage_Sales_Model_Order|null $order
     * @param string                      $message
     *
     * @return $this
     */
    protected function consolePrint(Mage_Sales_Model_Order $order = null, $message = null)
    {
        if (!is_null($order) && $order->getId()) {
            echo sprintf("Order ID: %s (%s):\n", $order->getRealOrderId(), $order->getId());
        }

        if ($message) {
            echo $message . PHP_EOL;
        }

        return $this;
    }

}

(new Esmart_Shell_Order_Cancel())->run();
