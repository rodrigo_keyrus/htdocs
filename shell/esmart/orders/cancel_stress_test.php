<?php

require_once realpath(__DIR__) . '/../../abstract.php';

class Esmart_Shell_Order_CancelStressTest extends Mage_Shell_Abstract
{
    const ORDER_CLIENT_EMAIL_PREFIX = 'zdsephora';
    const ORDER_CLIENT_EMAIL_SUFFIX = '@zd.com.br';
    const LOOP_LIMIT = 1000;

    /**
     * Run script
     */
    public function run()
    {

        $this->consolePrint('Canceling stress test order start.');

        $dateRange  = $this->getArg('date');
        if(!$dateRange) {
            $dateRange = date('Y-m-d',strtotime("-2 days"));
        }

        $this->consolePrint('Date range to cancel "'.$dateRange.'"');
        $this->consolePrint('Order prefix to cancel "'.self::ORDER_CLIENT_EMAIL_PREFIX.'"');
        $this->consolePrint('Order suffix to cancel "'.self::ORDER_CLIENT_EMAIL_SUFFIX.'"');


        /** @var Mage_Sales_Model_Resource_Order_Collection $orderCollection */

        $cancelStates = [
            Mage_Sales_Model_Order::STATE_CANCELED
        ];

        $collection =  Mage::getResourceModel('sales/order_collection')
            ->addAttributeToFilter('customer_email',
                array('like' => self::ORDER_CLIENT_EMAIL_PREFIX . '%'))
            ->addAttributeToFilter('customer_email',
                array('like' => '%'.self::ORDER_CLIENT_EMAIL_SUFFIX))
            ->addFieldToFilter('state',     ['nin' => $cancelStates])
            ->addFieldToFilter('created_at', array('from' => $dateRange, true))
            ->setPageSize(self::LOOP_LIMIT);

        $this->consolePrint('"'.$collection->count().'" order(s) to cancel');
        foreach ($collection as $order) {
            $this->cancelOrder($order);
        }

        $this->consolePrint('Stress test cancellation finished.');
    }

    /**
     * Force order cancellation in Magento
     *
     * @param Mage_Sales_Model_Order $order
     */
    public function cancelOrder(Mage_Sales_Model_Order $order)
    {
        if ($order->hasInvoices()) {
            /** @var \Mage_Sales_Model_Order_Invoice $invoice */
            foreach ($order->getInvoiceCollection() as $invoice) {
                $this->_cancelInvoice($invoice);
            }
        }

        try{

            if ($order->getState() === Mage_Sales_Model_Order::STATE_HOLDED) {
                $order->unhold();
            }

            $order->cancel();
            $order->addStatusHistoryComment('Pedido cancelado por script de stress teste');
            $order->save();
            $this->consolePrint('Order "'.$order->getIncrementId().'" was cancel');

        } catch (Exception $e) {
            $this->consolePrintError($e->getMessage());
        }
    }

    /**
     * @param Mage_Sales_Model_Order_Invoice $invoice
     * @return $this
     */
    protected function _cancelInvoice(Mage_Sales_Model_Order_Invoice $invoice)
    {
        if ($invoice->isCanceled()) {
            return $this;
        }

        try {
            $invoice->cancel();

            Mage::getModel('core/resource_transaction')
                ->addObject($invoice)
                ->save();
            
        } catch (Exception $e) {
            $this->consolePrintError($e->getMessage());
        }
    }

    /**
     * @param string                      $message
     *
     * @return $this
     */
    protected function consolePrint($message = null)
    {
        if ($message) {
            echo $message . PHP_EOL;
            Mage::log($message . PHP_EOL, null, 'cancel_order_stress_test.log');
        }

        return $this;
    }

    /**
     * @param string                      $message
     *
     * @return $this
     */
    protected function consolePrintError($message = null)
    {
        if ($message) {
            echo "Error : ".$message . PHP_EOL;
            Mage::log("Error : ".$message . PHP_EOL, null, 'cancel_order_stress_test_error.log');
        }

        return $this;
    }

}

(new Esmart_Shell_Order_CancelStressTest())->run();
