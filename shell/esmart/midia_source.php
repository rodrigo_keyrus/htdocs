<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Não edite este arquivo caso você pretenda atualizar este módulo futuramente
 * para novas versões.
 *
 * @category      Esmart
 * @package       Esmart_Import
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 * @author        Guilherme Miguelete <guilherme.miguelete@e-smart.com.br>
 */
require_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'abstract.php';

class Esmart_Import_MidiaSource extends Mage_Shell_Abstract
{

    /**
     * Run script
     *
     */
    public function run()
    {

        if (!$file = $this->getArg('file')) {
            echo 'File is require for import';
            return;
        }

        ini_set('memory_limit', '-1');


        $file = getcwd().'/'.$file;
        $csvObject = new Varien_File_Csv();
        $csvData = $csvObject->getData($file);

        // Remove first line (header)
        array_shift($csvData);
        foreach ($csvData as $data) {
            $source = Mage::getModel('esmart_campaign/source');
            $source->setData([
                'origin' =>     $data[0],
                'source' =>     $data[1],
                'channel' =>    $data[2]
            ]);
            $source->save();
            echo $source->getData('channel'). ' - Imported Success';
            echo ",\n";
        }


    }

    protected function _validate()
    {
        return true;
    }

}

$shell = new Esmart_Import_MidiaSource();
$shell->run();
