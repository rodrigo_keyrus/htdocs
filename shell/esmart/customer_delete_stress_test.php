<?php

require_once realpath(__DIR__) . '/../abstract.php';

class Esmart_Shell_Customer_DeleteStressTest extends Mage_Shell_Abstract
{
    const CUSTOMER_EMAIL_PREFIX = 'zdsephora';
    const CUSTOMER_EMAIL_SUFFIX = '@zd.com.br';
    const LOOP_LIMIT = 1000;

    /**
     * Run script
     */
    public function run()
    {

        $this->consolePrint('Deleting stress test customer start.');

        $this->consolePrint('Customer prefix to delete "'.self::CUSTOMER_EMAIL_PREFIX.'"');
        $this->consolePrint('Customer suffix to delete "'.self::CUSTOMER_EMAIL_SUFFIX.'"');

        $customers = Mage::getModel("customer/customer")
            ->getCollection()
            ->addAttributeToFilter('email',
                array('like' => self::CUSTOMER_EMAIL_PREFIX.'%'))
            ->addAttributeToFilter('email',
                array('like' => '%'.self::CUSTOMER_EMAIL_SUFFIX));

        Mage::register('isSecureArea', true);

        $this->consolePrint('"'.$customers->count().'" customer(s) before validating the counter.');

        $counter = 0;
        foreach ($customers as $customer) {
            preg_match_all('!\d+!', $customer->getEmail(), $matches);
            $sequential = $matches[0][0];

            if (intval($sequential) > 4000) {
                $customer->delete();

                $counter++;
            }
        }

        Mage::unregister('isSecureArea');

        $this->consolePrint('"' . $counter . '" customer(s) deleted.');

        $this->consolePrint('Stress test delete customer finished.');
    }

    /**
     * @param string                      $message
     *
     * @return $this
     */
    protected function consolePrint($message = null)
    {
        if ($message) {
            echo $message . PHP_EOL;
            Mage::log($message . PHP_EOL, null, 'cancel_order_stress_test.log');
        }

        return $this;
    }

    /**
     * @param string                      $message
     *
     * @return $this
     */
    protected function consolePrintError($message = null)
    {
        if ($message) {
            echo "Error : ".$message . PHP_EOL;
            Mage::log("Error : ".$message . PHP_EOL, null, 'cancel_order_stress_test_error.log');
        }

        return $this;
    }
}

(new Esmart_Shell_Customer_DeleteStressTest())->run();
