<?php

require_once dirname(dirname(__DIR__)) . '/abstract.php';

/**
 * Magento Compiler Shell Script
 *
 * @category    Mage
 * @package     Mage_Shell
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Esmart_Ax_Shell_Stock extends Mage_Shell_Abstract
{

    /**
     * Run script
     *
     */
    public function run()
    {
        $this->initFilesystem();

        // $skus = $this->getSkus();

        /**
         * @var array $products
         */
        $products = [];

        if ($this->getArg('sku') && empty($products)) {
            $sku       = $this->getArg('sku');
            $productId = Mage::getResourceModel('catalog/product')->getIdBySku($sku);

            if (empty($productId)) {
                $this->consolePrint(sprintf("SKU '%s' does not exist!", $sku));
                return;
            }

            $products = [
                ['sku' => $sku]
            ];
        }

        if ($this->getArg('all') && empty($products)) {
            $products = $this->cronHelper()->getAllProductSkus();
        }

        $counter = 0;
        $success = 0;
        $error   = 0;

        /** @var Esmart_Axstock_Model_Resque_Jobs_AxStockJob $processor */
        $processor = Mage::getModel('esmart_axstock/resque_jobs_axStockJob');

        foreach ($products as $product) {
            $productSku = (string) $product['sku'] ?: null;

            if (!$productSku) {
                continue;
            }

            try {
                $this->consolePrint(sprintf("Consulting stock for Product SKU: %s.", $productSku));

                $args = [
                    'item_id'      => $productSku,
                    'process_type' => Esmart_Axstock_Model_Cron::INTEGRATION_TYPE_FULL
                ];

                $processor->setArgs($args);
                $processor->perform();

                $this->consolePrint(sprintf("SKU %s Successfully updated!", $productSku));

                $success++;
            } catch (Exception $e) {
                $this->consolePrint(
                    sprintf(
                        "Error when trying to get product's stock from AX. SKU %s. Error message: %s.",
                        $productSku,
                        $e->getMessage()
                    )
                );

                $error++;
            }

            $counter++;
        }

        $this->consolePrint(sprintf('The process has been finished with:'));
        $this->consolePrint(sprintf('%d consult tries in total.', $counter));
        $this->consolePrint(sprintf('%d success retrieves.', $success));
        $this->consolePrint(sprintf('%d errors.', $error));
    }


    /**
     * @return $this
     */
    protected function initFilesystem()
    {
        $path = $this->getDir();

        if (!is_dir($path)) {
            mkdir($path, 0777, true);
        }

        if (!is_dir_writeable($path)) {
            $this->consolePrint(sprintf('Dir "%s" is not writable.', $path));
            die();
        }

        $file = $this->getFilePath();
        if (is_file($file) && is_readable($file)) {
            unlink($file);
        }

        return $this;
    }


    /**
     * @return string
     */
    protected function getDir()
    {
        $path = Mage::getBaseDir('var') . DS . 'esmart' . DS . 'ax' . DS . 'stock' . DS;
        return $path;
    }


    /**
     * @return string
     */
    protected function getFilename()
    {
        return 'report.txt';
    }


    /**
     * @return string
     */
    protected function getFilePath()
    {
        return $this->getDir() . $this->getFilename();
    }


    /**
     * @return Esmart_Axstock_Helper_Cron
     */
    protected function cronHelper()
    {
        return Mage::helper('esmart_axstock/cron');
    }


    /**
     * @return array|bool
     */
    protected function getSkus()
    {
        $file = __DIR__ . DS . 'skus.txt';

        if (!file_exists($file) OR !is_readable($file)) {
            return false;
        }

        /** @var resource $handle */
        $handle = fopen($file, 'r');
        $skus   = [];

        while ($row = fgets($handle)) {
            $row = trim($row);

            if (!$row) {
                continue;
            }

            $skus[] = $row;
        }

        fclose($handle);

        return $skus;
    }


    /**
     * @param string $message
     *
     * @return $this
     */
    protected function consolePrint($message)
    {
        echo "{$message}\n";
        return $this;
    }


    /**
     * @return bool
     */
    protected function _validate()
    {
        return true;
    }

}

(new Esmart_Ax_Shell_Stock())->run();
