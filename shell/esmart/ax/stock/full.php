<?php

require_once dirname(dirname(dirname(__DIR__))) . '/abstract.php';

/**
 * Magento Compiler Shell Script
 *
 * @category    Mage
 * @package     Mage_Shell
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Esmart_Ax_Shell_Stock_Full extends Mage_Shell_Abstract
{

    /**
     * Run script
     *
     */
    public function run()
    {
        /** @var Esmart_Axstock_Model_Cron $cron */
        $cron = Mage::getModel('esmart_axstock/cron');
        $cron->allStockUpdate();
    }

}

(new Esmart_Ax_Shell_Stock_Full())->run();
