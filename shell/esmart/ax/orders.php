<?php

require_once dirname(dirname(__DIR__)) . '/abstract.php';

class Esmart_Ax_Shell_Orders extends Mage_Shell_Abstract
{

    /**
     * Run script
     */
    public function run()
    {
        /** @var Esmart_Axorder_Model_Resource_Order $resource */
        $resource = Mage::getResourceModel('esmart_axorder/order');
        $orderIds = null;

        /** @var Mage_Sales_Model_Resource_Order_Collection $orders */
        $orders = Mage::helper('esmart_axorder/cron')->getIntegrationOrdersCollection();

        if ($this->getArg('reset')) {
            echo sprintf('Resetting orders with Integration Fail status before integration.') . PHP_EOL;
            $resource->resetFailsToPending();
        }

        if ($this->getArg('fails')) {
            echo sprintf('Only orders with Integration Failed status will be integrated.') . PHP_EOL;
            $orderIds = $resource->getFailedOrderIds();

            if (empty($orderIds)) {
                echo sprintf('No order with status Integration Failed status was found.') . PHP_EOL;
                return;
            }
        }

        if (!empty($orderIds) && is_array($orderIds)) {
            $orders->addFieldToFilter('entity_id', ['in' => $orders]);
        }

        $count = $orders->getSize();

        if ($count <= 0) {
            return;
        }

        /** @var Esmart_Axorder_Model_Resque_Job_OrderJob $job */
        $job = Mage::getModel('esmart_axorder/resque_job_orderJob');

        echo sprintf('%s orders were found to be integrated!', $count) . PHP_EOL;

        /** @var Mage_Sales_Model_Order $order */
        foreach ($orders as $order) {
            if (!$order->hasInvoices()) {
                continue;
            }

            $args = [
                'order_id' => $order->getId(),
            ];

            echo sprintf(
                    'Starting integration process for order %s (%s)',
                    $order->getIncrementId(),
                    $order->getId()
                ) . PHP_EOL;

            $job->setArgs($args);
            $job->perform();

            $count--;

            echo sprintf('Finished! Now there are %s orders to be integrated.', $count) . PHP_EOL;
        }
    }

}

(new Esmart_Ax_Shell_Orders())->run();
