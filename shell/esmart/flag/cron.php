<?php

require_once realpath(__DIR__) . '/../../abstract.php';

class Esmart_Flag_Cron extends Mage_Shell_Abstract
{
    /**
     * Run script
     */
    public function run()
    {
        Mage::getModel('esmart_flag/cron')->manageNewsFlag();
    }
}

(new Esmart_Flag_Cron())->run();
