<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Não edite este arquivo caso você pretenda atualizar este módulo futuramente
 * para novas versões.
 *
 * @category      Esmart
 * @package       Esmart_Import
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 * @author        Guilherme Miguelete <guilherme.miguelete@e-smart.com.br>
 */
require_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'abstract.php';

class Esmart_Import_Customers extends Mage_Shell_Abstract
{

    /**
     * Run script
     *
     */
    public function run()
    {

        if (!$file = $this->getArg('file')) {
            echo 'File is require for import';
            return;
        }

        ini_set('memory_limit', '-1');
        $processor = Mage::getModel('esmart_import/import');
        $processor->importCustomers($file);

    }

    protected function _validate()
    {
        return true;
    }

}

$shell = new Esmart_Import_Customers();
$shell->run();
