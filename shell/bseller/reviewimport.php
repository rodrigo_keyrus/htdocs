<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Massimport
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Gabriel da Gama <gabriel.gama@e-smart.com.br>
 */

require_once __DIR__ . '/../abstract.php';

class BSeller_ReviewImport_Shell extends Mage_Shell_Abstract
{
    var $expectedKeys = [
        'sku',
        'email',
        'detail',
        'rating',
        'nickname',
        'created_at'
    ];


    /**
     * Run schedule
     *
     * @return string|null
     */
    public function run()
    {
        if (!$file = $this->getArg('file')) {
            echo $this->usageHelp();
            return;
        }

        if (!is_file($file)) {
            echo 'File path passed is not valid!' . PHP_EOL;
            return;
        }

        ini_set('auto_detect_line_endings', true);
        $fileContent = fopen($file, 'r') or die('Could not get handle');
        $line = 0;
        $jobLine = 1;
        $args = [];

        while ($row = fgetcsv($fileContent, 1024)) {
            $line++;

            if (!$row) {
                continue;
            }

            // Guarantees the UTF-8 encoding from fils
            $row = array_map(function ($value) {
                $value = trim($value);
                return (mb_detect_encoding($value, 'UTF-8', true) === false) ? utf8_encode($value) : $value;
            }, $row);

            if ($line == 1) {
                if (!$this->validateKeys($row)) {
                    echo 'Your header appear to be wrong! Look if ti have at least the fields: ' .
                        implode(', ', $this->expectedKeys) . PHP_EOL;
                    return;
                }

                $keys = $row;
                continue;
            }

            $combinedData = array_combine($keys, $row);
            
            $args[] = [
                    'combined_data' => $combinedData,
                    'line' => $line
                ];

            // Enqueues and reset for each 10000 items
            if ($line % 50000 == 0) {
                Mage::getModel('bseller_resque/resque')->enqueueMultiple('massimport_review',
                    'BSeller_MassImport_Model_Import_ReviewJob', $args, 1);
                $args = [];
                $jobLine = 1;
            }
        }

        Mage::getModel('bseller_resque/resque')->enqueueMultiple('massimport_review',
            'BSeller_MassImport_Model_Import_ReviewJob', $args, 1);

        
        fclose($fileContent);
        ini_set('auto_detect_line_endings', false);

        echo 'Reviews imported to resque successfully!' . PHP_EOL;
    }


    /**
     * Retrieve usage help message
     *
     * @return string
     */
    public function usageHelp()
    {
        return <<< USAGE
Usage:   --file   Path to the file to import [MANDATORY]
Example: --file   path/to/fileToImport.csv

USAGE;
    }


    /**
     * @param array $keys
     * @return bool
     */
    protected function validateKeys(array $keys)
    {
        $intersection = array_intersect($this->expectedKeys, $keys);
        return count($intersection) == count($this->expectedKeys);
    }

}

$shell = new BSeller_ReviewImport_Shell();
$shell->run();
