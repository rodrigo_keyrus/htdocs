<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Massimport
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

require_once __DIR__ . '/../abstract.php';

class BSeller_MassManufacturers_Shell extends Mage_Shell_Abstract
{
    /**
     * Expected keys
     *
     * @var array
     */
    var $expectedKeys = [
        'ax_code'
    ];

    /**
     * Run schedule
     *
     * @return string|null
     */
    public function run()
    {
        if (!$file = $this->getArg('file')) {
            echo $this->usageHelp();
            return;
        }

        if (!is_file($file)) {
            echo 'File path passed is not valid!' . PHP_EOL;

            return;
        }

        ini_set('auto_detect_line_endings', true);
        $fileContent = fopen($file, 'r') or die('Could not get handle.');
        $line = 0;
        $keys = [];

        while ($row = fgetcsv($fileContent, 1024)) {
            $line++;

            if (!$row) {
                continue;
            }

            /**
             * Guarantees the UTF-8 encoding from files
             */
            $row = array_map(
                function($value) {
                    $value = trim($value);
                    return (mb_detect_encoding($value, 'UTF-8', true) === false) ? utf8_encode($value) : $value;
                },
                $row
            );

            if ($line == 1) {
                if (!$this->validateKeys($row)) {
                    echo 'Your header appear to be wrong! Look if ti have at least the fields: ' .
                        implode(', ', $this->expectedKeys) . PHP_EOL;
                    return;
                }

                $keys = $row;

                continue;
            }

            $columns = array_combine($keys, $row);

            /** @var Esmart_Manufacturer_Model_Manufacturer $manufacturer */
            $manufacturer = Mage::getModel('esmart_manufacturer/manufacturer')->load($columns['ax_code'], 'ax_code');

            if ($manufacturer === false) {
                $message = 'Manufacturer does not exists: ' . $columns['ax_code'];

                Mage::log($message, Zend_Log::DEBUG, 'esmart_manufacturer.log', true);

                echo $message . PHP_EOL;

                continue;
            }

            unset($columns['ax_code']);

            $manufacturer->addData($columns);

            try {
                $manufacturer->save();
            } catch (Exception $e) {
                Mage::log($manufacturer->getData('ax_code'), Zend_Log::DEBUG, 'esmart_manufacturer.log', true);
                Mage::log($e->getMessage(), Zend_Log::DEBUG, 'esmart_manufacturer.log', true);

                continue;
            }

            echo 'Manufacturer data imported: ' . $manufacturer->getData('ax_code') . PHP_EOL;
        }

        fclose($fileContent);
        ini_set('auto_detect_line_endings', false);

        echo 'Successfully!' . PHP_EOL;
    }

    /**
     * Retrieve usage help message
     *
     * @return string
     */
    public function usageHelp()
    {
        return <<< USAGE
Usage:   --file   Path to the file to import [MANDATORY]
Example: --file   path/to/fileToImport.txt

USAGE;
    }

    /**
     * Validate attributes
     *
     * @param array $keys
     * @return bool
     */
    protected function validateKeys(array $keys)
    {
        $intersection = array_intersect($this->expectedKeys, $keys);

        return count($intersection) == count($this->expectedKeys);
    }
}

$shell = new BSeller_MassManufacturers_Shell();
$shell->run();
