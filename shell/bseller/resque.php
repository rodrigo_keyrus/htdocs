<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category    BSeller
 * @package     BSeller_Resque
 *
 * @copyright   Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author      Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

/**
 * Include abstract shell
 */
require_once __DIR__ . '/../abstract.php';

/**
 * Resque shell script
 *
 * @category    BSeller
 * @package     BSeller_Resque
 * @author      Alan Carvalho <alan.carvalho@e-smart.com.br>
 */
class BSeller_Resque_Shell extends Mage_Shell_Abstract
{
    /**
     * Group process identify
     *
     * @const string
     */
    const PROCESS_GROUP = 'bseller_resque';

    /**
     * Process manager status identifier
     *
     * @const string
     */
    const PROCESS_MANAGER_STATUS = 'status';

    /**
     * Process manager start identifier
     *
     * @const string
     */
    const PROCESS_MANAGER_START = 'start';

    /**
     * Process manager stop identifier
     *
     * @const string
     */
    const PROCESS_MANAGER_STOP = 'stop';

    /**
     * Process manager restart identifier
     *
     * @const string
     */
    const PROCESS_MANAGER_RESTART = 'restart';

    /**
     * Process manager identifier
     *
     * @const string
     */
    const PROCESS_MANAGER = 'manager';

    /**
     * Process single worker of test identifier
     *
     * @const string
     */
    const PROCESS_SINGLE_WORKER_TEST = 'single_worker_test';

    /**
     * Process multiple workers of test identifier
     *
     * @const string
     */
    const PROCESS_MULTIPLE_WORKERS_TEST = 'multiple_workers_test';

    /**
     * Script name
     *
     * @const string
     */
    const SCRIPT_NAME = __FILE__;

    /**
     * Resque log object
     *
     * @var Resque_Log
     */
    private $_logger;

    /**
     * Manager signal handles
     *
     * @var array
     */
    private $_sigHandles = [
        SIGINT,
        SIGQUIT,
        SIGTERM
    ];

    /**
     * Run schedule
     *
     * @return string|null
     */
    public function run()
    {
        /**
         * Configs defined by user
         */
        $active   = (bool)   $this->getConfig()->get('active');
        $host     = (string) $this->getConfig()->get('host');
        $port     = (string) $this->getConfig()->get('port');
        $useAuth  = (bool)   $this->getConfig()->get('use_auth');
        $user     = (string) $this->getConfig()->get('user');
        $pass     = (string) $this->getConfig()->get('pass');
        $prefix   = (string) $this->getConfig()->get('prefix');
        $database = (int)    $this->getConfig()->get('database');
        $interval = (int)    $this->getConfig()->get('interval');
        $timeout  = (int)    $this->getConfig()->get('timeout');
        $verbose  = (int)    $this->getConfig()->get('verbose');

        /**
         * Configs defined by default
         */
        $appFile      = $this->getConfig()->getAppFile();
        $libShellFile = $this->getConfig()->getLibShellFile();
        $logFile      = $this->getConfig()->getLogFile();

        /**
         * Check if required extensions was installed
         */
        foreach ($this->getConfig()->getRequiredExtensions() as $extension) {
            if (extension_loaded($extension)) {
                continue;
            }

            $this->getLogger()->log(
                Psr\Log\LogLevel::WARNING, 'The php-{ext} extension is required', ['ext' => $extension]
            );

            return $this;
        }

        /**
         * Check if module is enabled
         */
        if (!$active) {
            $this->getLogger()->log(
                Psr\Log\LogLevel::WARNING, 'Please, active on System > Configuration > BSeller / Resque'
            );

            return $this;
        }

        /**
         * View the status of processes manager and worker
         */
        if ($this->getArg(self::PROCESS_MANAGER_STATUS)) {
            $this->showStatusManager();
            $this->showStatusWorkers();

            return $this;
        }

        /**
         * Start manager process
         */
        if ($this->getArg(self::PROCESS_MANAGER_START)) {
            $this->startManager();

            return $this;
        }

        /**
         * Stop manager process
         */
        if ($this->getArg(self::PROCESS_MANAGER_STOP)) {
            $this->stopWorkers();
            $this->showStatusWorkers();
            $this->stopManager();

            return $this;
        }

        /**
         * Restart manager process
         */
        if ($this->getArg(self::PROCESS_MANAGER_RESTART)) {
            $this->stopWorkers();
            $this->showStatusWorkers();
            $this->stopManager();
            $this->startManager();

            return $this;
        }

        /**
         * Create single worker of test
         */
        if ($this->getArg(self::PROCESS_SINGLE_WORKER_TEST)) {
            Mage::getModel('bseller_resque/test_single')->create();

            $this->getLogger()->log(Psr\Log\LogLevel::INFO, 'Single worker test');
            $this->getLogger()->log(Psr\Log\LogLevel::INFO, 'Queue was added');

            return $this;
        }

        /**
         * Create multiple workers of test
         */
        if ($this->getArg(self::PROCESS_MULTIPLE_WORKERS_TEST)) {
            Mage::getModel('bseller_resque/test_multiple')->create();

            $this->getLogger()->log(Psr\Log\LogLevel::INFO, 'Multiple workers test');
            $this->getLogger()->log(Psr\Log\LogLevel::INFO, 'Queue was added');

            return $this;
        }

        /**
         * Manager worker process
         */
        if ($this->getArg(self::PROCESS_MANAGER)) {
            if (!$this->isManagerStarted()) {
                $this->getLogger()->log(Psr\Log\LogLevel::WARNING, 'Process not in execution');

                return $this;
            }

            /**
             * Save init environment data
             */
            $backend = ($useAuth) ? "{$user}:{$pass}@{$host}:{$port}" : "{$host}:{$port}";

            $this->setEnv('redis_backend', $backend);
            $this->setEnv('redis_backend_db', $database);
            $this->setEnv('prefix', $prefix);
            $this->setEnv('vverbose', $verbose);
            $this->setEnv('app_include', $appFile);

            /**
             * Worker manager
             */
            while (true) {
                /** @var array $queues */
                $queues = $this->getResque()->getParent()->queues();

                /**
                 * Create workers if queues has data
                 *
                 * @var string $queue
                 */
                foreach ($queues as $queue) {
                    $worker    = $this->getResque()->worker($queue);
                    $queueSize = $this->getResque()->getParent()->size($queue);

                    /**
                     * Check whether the worker exists
                     * Checks if there is something in the queue
                     */
                    if ($worker instanceof Resque_Worker || !$queueSize) {
                        continue;
                    }

                    /**
                     * Save queue environment data
                     */
                    $this->setEnv('queue', $queue);

                    /**
                     * Start worker process
                     */
                    system("{$libShellFile} >> {$logFile} 2>&1 &");
                }

                /** @var array $workers */
                $workers = $this->getResque()->workers();

                /**
                 * Delete workers if queues does not has data
                 *
                 * @var Resque_Worker $worker
                 */
                foreach ($workers as $worker) {
                    $queue          = $this->getResque()->workerInfo($worker, BSeller_Resque_Model_Resque::INFO_QUEUE);
                    $workerPid      = $this->getResque()->workerInfo($worker, BSeller_Resque_Model_Resque::INFO_PID);
                    $workerChildPid = $this->getResque()->workerChildPid($queue);

                    /** @var Resque $parent */
                    $parent = $this->getResque()->getParent();

                    /** @var Redis $redis */
                    $redis = $parent->redis();

                    $jobInfo = $redis->get('worker:' . (string) $worker);
                    $jobInfo = Zend_Json::decode($jobInfo);

                    $date1 = new DateTime("now");
                    $date2 = new DateTime($jobInfo['run_at']);
                    $diffSec = $date1->getTimestamp() - $date2->getTimestamp();

                    /**
                     * Checks if time limit of worker was exceeded and clean the queue
                     */
                    if ($timeout < $diffSec) {
                        $this->stopWorker($workerChildPid, SIGKILL);
                        $this->stopWorker($workerPid, SIGKILL);

                        $job = new Resque_Job($queue, $jobInfo['payload']);
                        $job->recreate();

                        $worker->unregisterWorker();
                    }

                    /** @var int $queueSize */
                    $queueSize = $this->getResque()->getParent()->size($queue);

                    if (!$queueSize && $workerPid !== false) {
                        $this->stopWorker($workerPid);

                        $worker->unregisterWorker();
                    }
                }

                /**
                 * Sleep interval
                 * For create workers
                 * For delete the dead workers
                 */
                sleep($interval);
            }

            return $this;
        }

        echo $this->usageHelp();

        return $this;
    }

    /**
     * Retrieve usage help message
     *
     * @return string
     */
    public function usageHelp()
    {
        return <<< USAGE
Usage:  php -f resque.php -- [option]

    status                  View the status of processes manager and worker
                            * Queue | Pending | Processed | Failed | Worker PID | Worker Child PID
    start                   Start manager process
    stop                    Stop manager process
    restart                 Restart manager process
    single_worker_test      Run single worker of test
    multiple_workers_test   Run multiple workers of test
                            * 10 worker with 100 items on queue

USAGE;
    }

    /**
     * Stop worker processes
     *
     * @return $this
     */
    private function stopWorkers()
    {
        $workerPids = $this->getResque()->workerPids();

        foreach ($workerPids as $workerPid) {
            $this->stopWorker($workerPid);
        }

        return $this;
    }

    /**
     * Start worker process
     *
     * @param int $workerPid
     * @param int $signal
     * @return $this
     */
    private function stopWorker($workerPid, $signal = SIGTERM)
    {
        posix_kill($workerPid, $signal);

        $this->getLogger()->log(Psr\Log\LogLevel::INFO, 'Process {pid} was killed', ['pid' => $workerPid]);

        return $this;
    }

    /**
     * Show status of workers
     *
     * @return $this
     */
    private function showStatusWorkers()
    {
        /** @var Varien_Data_Collection $workers */
        $workers = Mage::getModel('bseller_resque/resque')->getWorkerCollection();

        if (!$workers->count()) {
            $this->getLogger()->log(Psr\Log\LogLevel::INFO, 'There are no active workers');

            return $this;
        }

        /** @var Varien_Object $worker */
        foreach ($workers as $worker) {
            $this->getLogger()->log(Psr\Log\LogLevel::INFO, implode(' | ', $worker->toArray()));
        }

        return $this;
    }

    /**
     * Start manager process
     * 
     * @return $this
     */
    private function startManager()
    {
        if ($this->isManagerStarted()) {
            $this->getLogger()->log(Psr\Log\LogLevel::INFO, 'Process already in execution');

            return $this;
        }

        $this->getManagerOutput(BSeller_Resque_Model_Resque::MANAGER_START_SCRIPT);

        $this->registerSigHandlers();

        $this->getLogger()->log(Psr\Log\LogLevel::INFO, 'Process started');

        return $this;
    }

    /**
     * Stop manager process
     * 
     * @return $this
     */
    private function stopManager()
    {
        if (!$this->isManagerStarted()) {
            $this->getLogger()->log(Psr\Log\LogLevel::INFO, 'Process not in execution');

            return $this;
        }

        $output = $this->getManagerOutput(BSeller_Resque_Model_Resque::MANAGER_SEARCH_SCRIPT);

        $managerPids = [];

        foreach ($output as $line) {
            list($managerPids[]) = explode(' ', trim($line), 2);
        }

        $managerPid = array_shift($managerPids);

        posix_kill($managerPid, SIGTERM);

        $this->getLogger()->log(Psr\Log\LogLevel::INFO, 'Process stopped');

        return $this;
    }

    /**
     * Check if manager process already in execution
     *
     * @return bool
     */
    private function isManagerStarted()
    {
        return (bool) $this->getManagerOutput(BSeller_Resque_Model_Resque::MANAGER_SEARCH_SCRIPT);
    }

    /**
     * Return manager process status
     *
     * @return $this
     */
    private function showStatusManager()
    {
        $status = $this->isManagerStarted();

        $this->getLogger()->log(
            Psr\Log\LogLevel::INFO,
            'Process is {status}',
            ['status' => ($status ? 'running' : 'stopped')]
        );

        return $this;
    }

    /**
     * Return shell command manager output
     *
     * @param string $pattern
     * @return array
     */
    private function getManagerOutput($pattern)
    {
        $command = vsprintf(
            $pattern,
            [
                dirname(self::SCRIPT_NAME),
                basename(self::SCRIPT_NAME),
                self::PROCESS_GROUP,
                self::PROCESS_MANAGER
            ]
        );

        exec($command, $output);

        return $output;
    }

    /**
     * Register signal handlers that a worker should respond to.
     *
     * @return $this
     */
    private function registerSigHandlers()
    {
        if (!function_exists('pcntl_signal')) {
            return $this;
        }

        declare(ticks = 1);

        foreach ($this->_sigHandles as $sigHandle) {
            pcntl_signal(
                $sigHandle,
                function($sigHandle) {
                    $this->getLogger()->log('Exiting with signal: {sigHandle}', ['sigHandle' => $sigHandle]);

                    $this->stopWorkers();
                    $this->showStatusWorkers();
                    $this->stopManager();
                }
            );
        }

        $this->getLogger()->log(Psr\Log\LogLevel::INFO, 'Registered signals');

        return $this;
    }

    /**
     * Save environment data
     *
     * @param string $key
     * @param string $value
     * @return $this
     */
    private function setEnv($key, $value = '')
    {
        $key = strtoupper($key);

        putenv("{$key}={$value}");

        return $this;
    }

    /**
     * Return resque model
     *
     * @return BSeller_Resque_Model_Resque
     */
    private function getResque()
    {
        return Mage::getSingleton('bseller_resque/resque');
    }

    /**
     * Return resque config model
     *
     * @return BSeller_Resque_Model_Config
     */
    private function getConfig()
    {
        return Mage::getSingleton('bseller_resque/config');
    }

    /**
     * Return PSR log object
     *
     * @return Resque_Log
     */
    private function getLogger()
    {
        if (!$this->_logger instanceof Resque_Log) {
            $this->_logger = new Resque_Log($this->getConfig()->get('verbose'));
        }

        return $this->_logger;
    }
}

$shell = new BSeller_Resque_Shell();
$shell->run();
