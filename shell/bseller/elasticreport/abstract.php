<?php

require_once dirname(dirname(__DIR__)) . '/abstract.php';

abstract class BSeller_ElasticReport_Shell_Abstract extends Mage_Shell_Abstract
{

    /**
     * @return BSeller_ElasticReport_Model_Cron
     */
    protected function getCronModel()
    {
        /** @var BSeller_ElasticReport_Model_Resource_Orders_Queue $resource */
        $cron = new BSeller_ElasticReport_Model_Cron();
        return $cron;
    }
    
    
    /**
     * @return Mage_Cron_Model_Schedule
     */
    protected function getSchedule()
    {
        /** @var Mage_Cron_Model_Schedule $schedule */
        $schedule = Mage::getModel('cron/schedule');
        
        $limit = (int) $this->getArg('limit');
        $limit = max($limit, 0);
        
        if ($limit > 0) {
            $schedule->setData('limit', (int) $limit);
        }
        
        return $schedule;
    }

}
