<?php

require_once 'abstract.php';

class BSeller_ElasticReport_Shell_Collect_Orders extends BSeller_ElasticReport_Shell_Abstract
{

    public function run()
    {
        /** @var Mage_Cron_Model_Schedule $schedule */
        $schedule = $this->getSchedule();
        $this->getCronModel()->collectOrdersToQueue($schedule);

        if ($schedule->getMessages()) {
            echo $schedule->getMessages() . "\n";
        }
    }

}

(new BSeller_ElasticReport_Shell_Collect_Orders())->run();
