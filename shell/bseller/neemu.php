<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Neemu
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

require_once __DIR__ . '/../abstract.php';

class BSeller_Neemu_Shell extends Mage_Shell_Abstract
{
    /**
     * Run script
     *
     * @return $this
     */
    public function run()
    {
        /**
         * Create file using test mode
         */
        if ($productIds = $this->getArg('product_ids')) {
            /** @var array $productIds */
            $productIds = explode(',', $productIds);

            /** @var Mage_Core_Model_Store $store */
            $store = array_shift(Mage::app()->getStores());

            /**
             * Set current store in session
             */
            Mage::app()->setCurrentStore($store->getId());

            /** @var BSeller_Neemu_Model_Product_Full $instance */
            $instance = Mage::getModel('bseller_neemu/product_full');

            /** @var BSeller_Neemu_Model_File $file */
            $file = Mage::getModel('bseller_neemu/file');
            $file->setPrefix('test');
            $file->delete();
            $file->insert('header');
            $file->create($instance, $productIds);
            $file->insert('footer');

            return $this;
        }

        /**
         * Create file using full mode
         */
        if ($this->getArg('enqueue_full')) {
            Mage::getModel('bseller_neemu/observer')->enqueueFull();

            return $this;
        }

        /**
         * Create file using partial mode
         */
        if ($this->getArg('enqueue_partial')) {
            Mage::getModel('bseller_neemu/observer')->enqueuePartial();

            return $this;
        }

        echo $this->usageHelp();

        return $this;
    }

    /**
     * Retrieve usage help message
     *
     * @return string
     */
    public function usageHelp()
    {
        return <<< USAGE
Usage:  php -f neemu.php -- [option]

    --product_ids   {id, id, ...}   Create file using manual mode (no enqueue)
    enqueue_full                    Create file using full mode
    enqueue_partial                 Create file using partial mode

USAGE;
    }
}

$shell = new BSeller_Neemu_Shell();
$shell->run();
