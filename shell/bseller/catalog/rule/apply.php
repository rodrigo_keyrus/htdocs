<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_CatalogRule
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Guilherme Miguelete <guilherme.miguelete@e-smart.com.br>
 */

require_once __DIR__ . '/../../../abstract.php';

class BSeller_Catalog_Rule_Apply extends Mage_Shell_Abstract
{

    /**
     * Run script
     */
    public function run()
    {

        ini_set('memory_limit', '-1');
        /** @var BSeller_CatalogRule_Model_Rules $processor */
        $processor = Mage::getModel('bseller_catalogrule/rules');

        $startTime = microtime(true);

        if ($this->getArg('applyall')) {
            $processor->applyAllRules();
            echo 'The rules have been applied on '."\n";
            echo "Ended in Time: " . (microtime(true) - $startTime) . " seconds\n";
            return $this;
        }

//        if ($this->getArg('sku')) {
//
//            /** @var BSeller_Resque_Model_Resque $resque */
//            $resque = Mage::getModel('bseller_resque/resque');
//
//            $processor->applyAllRulesOnProduct($this->getArg('sku'));
//
//            $resque->enqueue(
//                BSeller_CatalogRule_Helper_Data::RESQUE_QUEUE_NAME,
//                BSeller_CatalogRule_Helper_Data::RESQUE_JOB_CLASS,
//                $args
//            );
//
//            echo 'The rules have been applied for sku '.$this->getArg('sku')."\n";
//            echo "Ended in Time: " . (microtime(true) - $startTime) . " seconds\n";
//            return $this;
//        }

        return <<<USAGE
Usage:  --help

USAGE;

    }


    /**
     * Retrieve Usage Help Message
     *
     */
    public function usageHelp()
    {
        return <<<USAGE
Usage:  php -f apply.php -- [options]

  --applyaall                   Aplly All Catalog Rules
  --apply <sku>                 Aplly Catalog Rules specified Product

  <sku>     SKU of product for apply all catalog rules

USAGE;
    }

}

$shell = new BSeller_Catalog_Rule_Apply();
$shell->run();
