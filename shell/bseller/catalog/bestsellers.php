<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_CatalogList
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Guilherme Miguelete <guilherme.miguelete@e-smart.com.br>
 */
require_once __DIR__ . '/../../abstract.php';

class BSeller_CatalogList_Bestsellers extends Mage_Shell_Abstract
{

    /**
     * Run script
     */
    public function run()
    {

        ini_set('memory_limit', '-1');
        /** @var BSeller_CatalogList_Model_Product_Discount $processor */
        $processor = Mage::getModel('bseller_cataloglist/product_bestsellers');

        if ($this->getArg('reset')) {
            $processor->reset();
            echo 'Reset Success'."\n";
            return;
        }

        $processor->batchUpdate(500);
        echo 'Update Success'."\n";
        return;

    }

}

$shell = new BSeller_CatalogList_Bestsellers();
$shell->run();
