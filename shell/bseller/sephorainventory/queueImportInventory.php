<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   sephora
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Anderson Cardoso <anderson.cardoso@e-smart.com.br>
 */

require_once realpath(__DIR__) . '/../../abstract.php';

class BSeller_Inventory_Shell extends Mage_Shell_Abstract
{
    public function run()
    {
        if (!Mage::helper('bseller_sephorainventory')->serviceIsActive()) {
            return exit;
        }

        try {
            Mage::getModel('bsellererp_stock/cron')->import();
            Mage::log('importando stock', '', 'ShellImportStock.log');
        } catch (Exception $e) {
            Mage::log($e->getMessage(), '', 'ShellImportStockError.log');
        }
        return $this;
    }

}