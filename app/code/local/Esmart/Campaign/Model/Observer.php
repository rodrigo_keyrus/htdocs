<?php

/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Não edite este arquivo caso você pretenda atualizar este módulo futuramente
 * para novas versões.
 *
 * @category
 * @package
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Gabriel da Gama <gabriel.gama@e-smart.com.br>
 */
class Esmart_Campaign_Model_Observer
{

    const PARAMS_REGISTRY_KEY = 'current_utm_params';


    /**
     * Get campaign param and set it to session
     * @param $observer
     * @return $this
     */
    public function getCampaignParams($observer)
    {
        $params = Mage::app()->getRequest()->getParams();

        if (empty($params)) {
            return $this;
        }

        $campaignParams = Esmart_Campaign_Model_Campaign::$campaignParams;

        foreach ($params as $key => $value) {
            if (!in_array($key, $campaignParams)) {
                unset($params[$key]);
            }
        }

        $this->registerParams($params);
        $this->setCookies($params);

        return $this;
    }

    /**
     * Get param from session and set it to address
     * @param $observer
     * @return $this
     */
    public function setCampaignParams($observer)
    {
        $utmParams = $this->initCampaignParams();

        if (empty($utmParams)) {
            return;
        }

        /** @var Mage_Sales_Model_Quote $quote */
        $quote = $observer->getQuote();

        if (!$quote) {
            return;
        }

        /** @var Mage_Sales_Model_Resource_Quote_Address_Collection $addressCollection */
        $addressCollection = $quote->getAddressesCollection();

        /** @var Mage_Sales_Model_Quote_Address $address */
        foreach ($addressCollection as $address) {
            $address->addData($utmParams);
        }
    }


    /**
     * Get UTMs params from session and set to quote
     */
    public function setUtmsToQuote()
    {

        /** @var Mage_Sales_Model_Quote $quote */
        $quote = Mage::getSingleton('checkout/session')->getQuote();

        if (!$quote->getId()) {
            return;
        }

        $directReferrers    = Esmart_Campaign_Model_Campaign::$directReferrers;
        $utmHistory         = Mage::getModel('core/cookie')->get('tagmizeGAhistory');
        $arrayUtmHistory    = explode('>', $utmHistory);

        $data = [
            'ga_history' => $utmHistory,
            'ga_history_last' => end($arrayUtmHistory),
            'ga_history_first' => array_shift($arrayUtmHistory)
        ];


        if (count($arrayUtmHistory) >= 2) {
            foreach ($directReferrers as $directReferrer) {
                if (stristr(strtolower(trim($data['ga_history_last'])), $directReferrer) !== false) {
                    $data['ga_history_last'] = $arrayUtmHistory[count($arrayUtmHistory) - 2];
                }
            }
        }

        $dataParams = array_merge($this->initCampaignParams(), $data);


        $quote->addData($dataParams);
    }


    /**
     * Get UTMs params from session and set to order
     * @param $observer
     * @return $this
     */
    public function setUtmsToOrder($observer)
    {
        $utmParams = $this->initCampaignParams();

        if (empty($utmParams)) {
            return;
        }

        /** @var Mage_Sales_Model_Order $order */
        $order = $observer->getOrder();

        if (!$order) {
            return;
        }

        /** @var Mage_Sales_Model_Resource_Order_Address_Collection $addressCollection */
        $addressCollection = $order->getAddressesCollection();

        /** @var Mage_Sales_Model_Quote_Address $address */
        foreach ($addressCollection as $address) {
            $address->addData($utmParams);
        }

        $this->removeCampaignParams();
    }


    /**
     * @param Varien_Event_Observer $observer
     * @return $this
     */
    function setPromoJsonToOrder(Varien_Event_Observer $observer)
    {

        /** @var Mage_Sales_Model_Order $order */
        $order = $observer->getOrder();

        if (!$order) {
            return $this;
        }

        try {

            if ($order->getAppliedRuleIds()) {
                /** @var  $rules Mage_SalesRule_Model_Resource_Rule_Collection */
                $rules = Mage::getModel('salesrule/rule')->getCollection()
                    ->addFieldToFilter('rule_id' , array('in' => explode(',', $order->getAppliedRuleIds())));
                $order->setData('promo_json', Mage::helper('esmart_campaign')->prepareRulesJson($rules));
            }

        }catch (Exception $e) {
            Mage::logException($e);
        }

        return $this;

    }



    /**
     * @return array
     */
    protected function initCampaignParams()
    {
        $campaignParams = Esmart_Campaign_Model_Campaign::$campaignParams;
        $utmParams      = [];

        foreach ($campaignParams as $paramKey) {
            $value = $this->getParams($paramKey);

            if (empty($value)) {
                continue;
            }

            $utmParams[$paramKey] = $value;
        }

        return $utmParams;
    }


    /**
     * @return $this
     */
    protected function removeCampaignParams()
    {
        Mage::unregister(self::PARAMS_REGISTRY_KEY);

        foreach (Esmart_Campaign_Model_Campaign::$campaignParams as $paramKey) {
            Mage::getSingleton('core/cookie')->delete($paramKey);
        }

        return $this;
    }


    /**
     * @param string|null $key
     *
     * @return string|array|null
     */
    protected function getParams($key = null)
    {
        $params = $this->getRegisteredParams($key);

        if (!$params && $key) {
            $params = Mage::getSingleton('core/cookie')->get($key);
        }

        return $params;
    }


    /**
     * @param string|null $key
     *
     * @return null
     */
    protected function getRegisteredParams($key = null)
    {
        $params = Mage::registry(self::PARAMS_REGISTRY_KEY);

        if (!is_null($key)) {
            return !empty($params[$key]) ? $params[$key] : null;
        }

        return $params;
    }


    /**
     * @param array $params
     *
     * @return $this
     */
    protected function registerParams($params = [])
    {
        Mage::register(self::PARAMS_REGISTRY_KEY, $params, true);
        return $this;
    }

    /**
     * @return Esmart_Campaign_Helper_Data|Mage_Core_Helper_Abstract
     */
    public function helper()
    {
        return Mage::helper('esmart_campaign');
    }

    /**
     * @param array $cookies
     *
     * @return $this|void
     */
    public function setCookies($cookies = array())
    {
        if (!is_array($cookies) || is_null($cookies)) {
            return;
        }

        foreach ($cookies as $key => $value) {
            //Register Cookies
            Mage::getSingleton('core/cookie')->set(
                $key,
                $value,
                $period = null,
                $path = null,
                $domain = null,
                $secure = null,
                $httponly = null
            );
        }

        return $this;
    }
}