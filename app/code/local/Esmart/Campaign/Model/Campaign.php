<?php

/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Não edite este arquivo caso você pretenda atualizar este módulo futuramente
 * para novas versões.
 *
 * @category
 * @package
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Gabriel da Gama <gabriel.gama@e-smart.com.br>
 * @author        Guilherme Miguelete <guilherme.miguelete@e-smart.com.br>
 */
class Esmart_Campaign_Model_Campaign
{
    /**
     * Data to be used in salesrule and saved to order
     * @var array
     */
    static $campaignParams = [
        'utm_source',
        'utm_medium',
        'utm_campaign'
    ];


    static $directReferrers = [
        '(direct) / (none)',
        'sephora.com.br'
    ];

}
