<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  Esmart
 * @package   Esmart_Campaign
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Guilherme Miguelete <guilherme.miguelete@e-smart.com.br>
 */

class Esmart_Campaign_Adminhtml_SourceController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Render index page
     *
     * @return void
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Render grid block
     *
     * @return void
     */
    public function gridAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Render form new
     *
     * @return void
     */
    public function newAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Render form edit
     *
     * @return $this
     */
    public function editAction()
    {
        $id = $this->getRequest()->getParam('id');

        if (!$id) {
            $this->_redirect('*/*/new');

            return $this;
        }

        /** @var Esmart_Campaign_Model_Source $object */
        $object = Mage::getModel('esmart_campaign/source')->load($id);

        if (!$object->getId()) {
            $this->_redirect('*/*/new');

            return $this;
        }

        Mage::register('current_source', $object);


        $this->helper()->addObjectInfo($object);

        $this->loadLayout();
        $this->renderLayout();

        return $this;
    }

    /**
     * Save post data
     *
     * @return $this
     */
    public function saveAction()
    {
        if (!$this->getRequest()->isPost()) {
            $this->_redirect('*/*');

            return $this;
        }

        if (!$this->_validateFormKey()) {
            $this->_redirectReferer();

            return $this;
        }

        /** @var Esmart_Campaign_Model_Source $object */
        $object = Mage::getModel('esmart_campaign/source');

        $back = $this->getRequest()->getParam('back');
        $post = $this->getRequest()->getPost();
        $id   = $this->getRequest()->getPost('id');

        if ($id) {
            $object->load($id);
        }

        $object->addData($post);


        try {

            $object->save();

            $id = $object->getId();

            $this->_getSession()->addSuccess($this->__('Successfully saved.'));
        } catch (Exception $e) {
            Mage::logException($e);

            $back = true;

            $this->_getSession()->addError($e->getMessage());
        }

        $this->_redirect('*/*/' . ($back ? 'edit' : 'index'), ($back ? ['id' => $id] : []));

        return $this;
    }

    /**
     * Delete row by id
     *
     * @return $this
     */
    public function deleteAction()
    {
        $id = $this->getRequest()->getParam('id');

        if (!$id) {
            $this->_redirect('*/*');

            return $this;
        }

        /** @var Esmart_Campaign_Model_Source $object */
        $object = Mage::getModel('esmart_campaign/source')->load($id);

        if (!$object->getId()) {
            $this->_redirect('*/*');

            return $this;
        }

        try {
            $object->delete();

            $this->_getSession()->addSuccess($this->__('Successfully deleted.'));
        } catch (Exception $e) {
            Mage::logException($e);

            $this->_getSession()->addError($e->getMessage());
        }

        $this->_redirect('*/*');

        return $this;
    }

    /**
     * Mass delete by ids
     *
     * @return $this
     */
    public function massDeleteAction()
    {
        $ids = (array) $this->getRequest()->getParam('entity_ids');

        foreach ($ids as $id) {
            /** @var Esmart_Campaign_Model_Source $object */
            $object = Mage::getModel('esmart_campaign/source')->load($id);

            if (!$object->getId()) {
                continue;
            }

            try {
                $object->delete();
            } catch (Exception $e) {
                Mage::logException($e);

                $this->_getSession()->addError($e->getMessage());
            }
        }

        $this->_getSession()->addSuccess($this->__('Successfully deleted.'));

        $this->_redirect('*/*');

        return $this;
    }

    /**
     * Export order grid to CSV format
     *
     * @return void
     */
    public function exportCsvAction()
    {
        /** @var Esmart_Campaign_Block_Adminhtml_Source_Grid $grid */
        $grid = $this->getLayout()->createBlock('esmart_campaign/adminhtml_source_grid');
        $this->_prepareDownloadResponse($this->helper()->getExportFilename('csv'), $grid->getCsvFile());
    }

    /**
     * Export order grid to Excel XML format
     *
     * @return void
     */
    public function exportExcelAction()
    {
        $filename = $this->helper()->getExportFilename('xml');

        /** @var Esmart_Campaign_Block_Adminhtml_Source_Grid $grid */
        $grid = $this->getLayout()->createBlock('esmart_campaign/adminhtml_source_grid');
        $this->_prepareDownloadResponse($filename, $grid->getExcelFile($filename));
    }

    /**
     * Is allowed to access
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('bseller/esmart_campaign/source');
    }

    /**
     * Return gift helper object
     *
     * @return Esmart_Campaign_Helper_Data
     */
    protected function helper()
    {
        return Mage::helper('esmart_campaign');
    }
}
