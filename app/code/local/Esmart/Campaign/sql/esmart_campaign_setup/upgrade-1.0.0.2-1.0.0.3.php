<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Não edite este arquivo caso você pretenda atualizar este módulo futuramente
 * para novas versões.
 *
 * @category
 * @package
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Guilherme Miguelete <guilherme.miguelete@e-smart.com.br>
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

/** @var Varien_Db_Adapter_Pdo_Mysql $connection */
$connection = $installer->getConnection();

/**
 *  Drop monitoring table if exists
 */
$connection->dropTable($installer->getTable('esmart_campaign/source'));

/**
 * Create monitoring report table
 */
$table = $connection->newTable($installer->getTable('esmart_campaign/source'))
    ->addColumn(
        'source_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        11,
        [
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
            'primary'  => true
        ],
        'Entity ID'
    )
    ->addColumn(
        'origin',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        null,
        [
            'nullable' => false,
            'default'  => ''
        ],
        'Origem'
    )
    ->addColumn(
        'source',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        null,
        [
            'nullable' => false,
            'default'  => ''
        ],
        'Source'
    )
    ->addColumn(
        'channel',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        null,
        [
            'nullable' => false,
            'default'  => ''
        ],
        'Canal'
    )
    ->setComment('BSeller Source');

$connection->createTable($table);

$installer->endSetup();
