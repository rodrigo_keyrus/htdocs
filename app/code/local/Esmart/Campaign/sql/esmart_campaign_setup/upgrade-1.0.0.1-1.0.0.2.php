<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Não edite este arquivo caso você pretenda atualizar este módulo futuramente
 * para novas versões.
 *
 * @category
 * @package
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Guilherme Miguelete <guilherme.miguelete@e-smart.com.br>
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();
$connection = $installer->getConnection();


$installer->getConnection()
    ->addColumn(
        $installer->getTable('sales/order')
        ,'utm_medium',
        [
            'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
            'comment' => 'UTM Medium'
        ]
);


$installer->getConnection()
    ->addColumn(
        $installer->getTable('sales/quote'),
        'ga_history',
        [
            'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
            'comment' => 'GA History'
        ]
    );

$installer->getConnection()
    ->addColumn(
        $installer->getTable('sales/quote'),
        'ga_history_first',
        [
            'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
            'comment' => 'GA First History'
        ]
    );

$installer->getConnection()
    ->addColumn(
        $installer->getTable('sales/quote'),
        'ga_history_last',
        [
            'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
            'comment' => 'GA Last History'
        ]
    );


$installer->getConnection()
    ->addColumn(
        $installer->getTable('sales/quote')
        ,'utm_source',
        [
            'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
            'comment' => 'UTM Source'
        ]
);

$installer->getConnection()
    ->addColumn(
        $installer->getTable('sales/quote'),
        'utm_medium',
        [
            'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
            'comment' => 'UTM Medium'
        ]
);

$installer->getConnection()
    ->addColumn(
        $installer->getTable('sales/quote'),
        'utm_campaign',
        [
            'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
            'comment' => 'UTM Campaign'
        ]
);


$installer->endSetup();