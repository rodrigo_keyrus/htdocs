<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  Esmart
 * @package   Esmart_Campaign
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Guilherme Miguelete <guilherme.miguelete@e-smart.com.br>
 */
class Esmart_Campaign_Block_Adminhtml_Source_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('grid_id');
         $this->setDefaultSort('source_id');
        $this->setDefaultDir('desc');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('esmart_campaign/source')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {

//       $this->addColumn('column_id',
//           array(
//               'header'=> $this->__('column header'),
//               'width' => '50px',
//               'index' => 'column_from_collection'
//           )
//       );

        $this->addColumn(
            'source_id',
            [
                'header' => $this->__('ID'),
                'align'  => 'left',
                'index'  => 'source_id',
                'type'   => 'number'
            ]
        );

        $this->addColumn(
            'origin',
            [
                'header' => $this->__('Origin'),
                'align'  => 'left',
                'index'  => 'origin',
                'type'   => 'text'
            ]
        );

        $this->addColumn(
            'source',
            [
                'header' => $this->__('Source'),
                'align'  => 'left',
                'index'  => 'source',
                'type'   => 'text'
            ]
        );

        $this->addColumn(
            'channel',
            [
                'header' => $this->__('Channel'),
                'align'  => 'left',
                'index'  => 'channel',
                'type'   => 'text'
            ]
        );

        $this->addExportType('*/*/exportCsv', $this->__('CSV'));
        $this->addExportType('*/*/exportExcel', $this->__('Excel XML'));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
       return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

        protected function _prepareMassaction()
    {
        $modelPk = Mage::getModel('esmart_campaign/source')->getResource()->getIdFieldName();
        $this->setMassactionIdField($modelPk);
        $this->getMassactionBlock()->setFormFieldName('ids');
        // $this->getMassactionBlock()->setUseSelectAll(false);
        $this->getMassactionBlock()->addItem('delete', array(
             'label'=> $this->__('Delete'),
             'url'  => $this->getUrl('*/*/massDelete'),
        ));
        return $this;
    }
    }
