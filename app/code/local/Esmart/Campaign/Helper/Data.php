<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  Esmart
 * @package   Esmart_Campaign
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Guilherme Miguelete <guilherme.miguelete@e-smart.com.br>
 */
class Esmart_Campaign_Helper_Data extends Mage_Core_Helper_Abstract
{

    /**
     * Register key prefix
     *
     * @var string
     */
    protected $_registerKeyPrefix = 'current_source';

    /**
     * Register key suffix
     *
     * @var string
     */
    protected $_registerKeySuffix = '';

    /**
     * Log filename
     *
     * @var string
     */
    protected $_log = 'Xdebug_Esmart_Campaign.log';

    /**
     * Save object data in register
     *
     * @param Varien_Object $value
     * @param true|bool $graceful
     * @return $this
     */
    public function addObjectInfo($value, $graceful = true)
    {
        Mage::register($this->_registerKeyPrefix . '_' . $this->_registerKeySuffix, $value, $graceful);

        return $this;
    }

    /**
     * Return object data of register
     *
     * @return BSeller_Core_Model_Abstract|Varien_Object
     */
    public function getObjectInfo()
    {
        if ($object = Mage::registry($this->_registerKeyPrefix . '_' . $this->_registerKeySuffix)) {
            return $object;
        }

        return new Varien_Object();
    }


    /**
     * @param $rules
     * @return string JSON encoded object
     */
    public function prepareRulesJson($rules)
    {

        /** @var array $data */
        $data = [];

        /** @var Mage_SalesRule_Model_Rule $rule */
        foreach ($rules as $rule) {
            $data[$rule->getId()] = $rule->getData();
            $data[$rule->getId()]['conditions_serialized'] = Zend_Serializer::unserialize($rule->getData('conditions_serialized'));
            $data[$rule->getId()]['actions_serialized'] = Zend_Serializer::unserialize($rule->getData('actions_serialized'));
        }

        return Zend_Json::encode($data);

    }

    /**
     * @param $message
     *
     * @return $this
     */
    public function log($message)
    {
        Mage::log($message, null, $this->_log);

        return $this;
    }

}