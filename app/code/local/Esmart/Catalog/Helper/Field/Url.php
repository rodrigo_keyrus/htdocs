<?php
/**
 * BSeller Platform | B2W - Companhia Digital
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category Esmart
 * @package Esmart_Catalog
 *
 * @copyright Copyright (c) 2019 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author Bruno Gemelli <bruno.gemelli@b2wdigital.com>
 */
class Esmart_Catalog_Helper_Field_Url extends Mage_Catalog_Helper_Product_Url
{
    /**
     * Get chars convertation table
     *
     * @return array
     */
    public function getConvertTable()
    {
        unset($this->_convertTable['@']);
        $this->_convertTable['ã'] = 'a';
        $this->_convertTable['Ã'] = 'A';
        return parent::getConvertTable();
    }
}