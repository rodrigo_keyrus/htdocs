<?php

class Esmart_Catalog_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        echo 'bseller_catalog';
    }

    public function getAssociatedProductsAction()
    {
        $productId  = $this->getRequest()->getParam('product_id');
        $curPage    = $this->getRequest()->getParam('cur_page');

        if (!$productId) {
            throw new Exception('Empty param product_id.');
        }

        $product = Mage::getModel('catalog/product')->load($productId);

        if (!$product) {
            throw new Exception('Product not exists.');
        }
        if (!is_null($curPage)) {
            $associatedProducts = Mage::helper('esmart_catalog/product_grouped')
                ->getChildrenByPageSize($product, $curPage, 5, $isAjax = true);
        }else{
            $associatedProducts = Mage::helper('esmart_catalog/product_grouped')
                ->getChildrenByPageSize($product, $curPage = 1, $pageSize = null, $isAjax = true);
        }

        $data = [];

        foreach ($associatedProducts as $item) {
            $data[] = $item->getData();
        }

        $convertJson = Zend_Json::encode($data);

        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody($convertJson);
    }

    /**
     * @todo Return stock variation to view
     * @return string
     */
    public function stockQtyAvailableAction()
    {
        if (!Mage::helper('esmart_catalog')->getLowStockActive()) {
            return;
        }

        $qtyStock = '';
        if ($this->getRequest()->getParam('productVariantId')) {
            $productId  = $this->getRequest()->getParam('productVariantId');
            $qtyMax  = $this->getRequest()->getParam('qtyMaxLowStock');
            $qtyStock = Mage::getModel('esmart_catalog/product')
                ->getStocksQty($productId, $qtyMax);
        }

        $this->getResponse()
            ->setBody($qtyStock);
    }
}