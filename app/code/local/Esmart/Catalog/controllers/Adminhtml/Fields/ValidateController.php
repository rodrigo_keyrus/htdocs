<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_ImportAssociate
 *
 * @copyright Copyright (c) 2017 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Rafael Falcão <rafael.falcao@e-smart.com.br>
 */

class Esmart_Catalog_Adminhtml_Fields_ValidateController extends Mage_Adminhtml_Controller_Action
{
    /**
     * @var array
     */
    private $dataRequest;

    /**
     * @var array
     */
    private $fields = array();

    /**
     * @var array
     */
    private $categories;

    /**
     * @var array
     */
    private $associatedProducts;

    /**
     * @var array
     */
    private $images = array('image', 'small_image', 'thumbnail');

    /**
     * array
     */
    private $response = array();

    /**
     * Call validations and success message
     */
    public function indexAction()
    {
        $this->getResponse()->clearHeaders()->setHeader('Content-type','application/json');

        /** @var Mage_Core_Controller_Request_Http dataRequest */
        $this->dataRequest = $this->getRequest();

        $this->response["_success"] = true;

        $this->validateParams();
        $this->setParams();

        $this->validateRequiredFields();

        $this->getResponse()->setBody(json_encode($this->response));
    }


    /**
     * Validate website id, categories, associated products and images
     *
     * @return $this
     */
    public function validateRequiredFields()
    {
        /** Check if website id was checked */
        if(empty($this->fields['website_ids'])){
            $this->addErrorResponse($this->__('The website ID field on the Website tab is required.'));
        }

        /** Check if two levels categories was checked */
        if(!$this->validateCategories()){
            $this->addErrorResponse($this->__('You must select at least two category levels in the Categories tab.'));
        }

        /** Check if has associated products */
        if(!$this->associatedProducts){
            $this->addErrorResponse($this->__('You must associate at least one simple product in the Associated Products tab.'));
        }

        /** Check if 'image', 'small_image', 'thumbnail' was checked */
        if(!$this->validateImages()) {
            $this->addErrorResponse($this->__('It is necessary to select a Base Image, Small Image and Thumbnail in the Images tab.'));
        }

        return $this;
    }

    /**
     * Validate received parameters post
     *
     * @return $this
     */
    public function validateParams()
    {
        if(empty($this->dataRequest->getParam('fields'))){
            $this->addErrorResponse($this->__('Invalid request.'));
        }

        return $this;
    }

    /**
     * Create array of fields, categories and associated
     *
     * @return $this
     */
    public function setParams()
    {
        $fields = json_decode($this->dataRequest->getParam('fields'));

        foreach($fields as $field) {
            $this->fields[$this->normalizeField($field->name)] = $field->value;
        }

        $categories = $this->dataRequest->getParam('categories');
        $this->categories = $this->hasField($categories, 'category_ids');

        $associatedProducts = $this->dataRequest->getParam('associated_products');
        $this->associatedProducts = $this->hasField($associatedProducts, 'linksgrouped');

        if($this->dataRequest->getParam('is_bundle')){
            $this->associatedProducts = $this->hasField($associatedProducts, 'bundle_selections00_id');
        }

        return $this;
    }

    /**
     * Validate pre set of categories and associated
     *
     * @param $preSet
     * @param $field
     * @return bool|mixed
     */
    public function hasField($preSet, $field)
    {
        if(!isset($this->fields[$field]) && $preSet){
            return $preSet;
        }

        if(!empty($this->fields[$field])){
            return $this->fields[$field];
        }

        return false;
    }

    /**
     * Validate product images
     *
     * @return bool
     */
    public function validateImages()
    {
        foreach ($this->images as $image){
            if(!$this->fields[$image] || $this->fields[$image] == 'no_selection'){
                return false;
            }
        }

        return true;
    }

    /**
     *  Validate category levels
     *
     * @return bool
     */
    public function validateCategories()
    {
        $categories = explode(',', $this->categories);

        /** @var Mage_Catalog_Model_Resource_Category_Collection $categoryCollection */
        $categoryCollection = Mage::getResourceModel('catalog/category_collection')
            ->addAttributeToSelect(array('entity_id','level'))
            ->addAttributeToFilter('entity_id', array('in' => $categories))
            ->load();

        /** Check if you have at least 2 registered categories */
        if(count($categoryCollection) < 1){
            return false;
        }

        /** @var Varien_Data_Collection $levels */
        $levels = $categoryCollection->getColumnValues('level');

        if(!is_array($levels)){
            return false;
        }

        $levelArray = array();
        foreach ($levels as $level){
            if($level > 1){
                $levelArray[$level] = $level;
            }
        }

        if(count($levelArray) < 2){
            return false;
        }

        return true;
    }

    /**
     * @param $msg
     */
    public function addErrorResponse($msg)
    {
        $this->response['_success'] = false;
        $this->response['_message'][] = $msg;
    }

    /**
     * @param $field
     * @return mixed
     */
    public function normalizeField($field)
    {
        $chars = array('product', '[', ']');

        return str_replace($chars, '', $field);
    }

    /**
     * @return boolean
     */
    public function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('esmart/fields_validate/index');
    }
}