<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_ImportAssociate
 *
 * @copyright Copyright (c) 2017 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Anderson Cardoso <anderson.cardoso@e-smart.com.br>
 */

class Esmart_Catalog_Adminhtml_Import_AssociateController extends Mage_Adminhtml_Controller_Action
{
    protected $_errorcount = array();
    /**
     * Render index page
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Render grid block
     */
    public function gridAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }
    /**
     * Save post data and redirect to index action
     *
     * @return $this|Mage_Core_Controller_Varien_Action
     */
    public function saveAction()
    {
        if (!$this->getRequest()->isPost()) {
            return $this->_redirect('*/*/index');
        }
        $request  = $this->getRequest();
        $path = basename($_FILES['file']['name']);
        move_uploaded_file($_FILES['file']['tmp_name'], $path);
        $model =  Mage::getModel('esmart_catalog/adminhtml_associate', $path);
        $model->processData();
        return $this->_redirect('*/*/');
    }

    /**
     * Is allowed to access
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('bseller/bseller_importprice');
    }

}