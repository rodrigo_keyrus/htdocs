<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category      Esmart
 * @package       Esmart_Catalog
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$entityTypeId = $installer->getEntityTypeId('catalog_product');

$attrData = [
    'ax_min_qty' => 'Ax Min Qty'
];

$data = [
    'type' => 'int',
    'input' => 'text',
    'default' => 0,
    'required'     => false,
    'user_defined' => true,
    'global'       => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'       => true,
    'searchable'    => false,
    'filterable'    => false,
    'comparable'    => false,
    'visible_on_front'  => true,
];

$modelGroup = Mage::getModel('eav/entity_attribute_group')->load('Ax Integration', 'attribute_group_name');

if (!$modelGroup->getId()) {
    $modelGroup->setAttributeGroupName('Ax Integration')
        ->setAttributeSetId(4)
        ->setSortOrder(3);
    $modelGroup->save();
}

$attributeSetId = $modelGroup->getAttributeSetId();

foreach ($attrData as $attrCode => $attrLabel) {
    $this->addAttribute($entityTypeId, $attrCode, array_merge($data, ['label' => $attrLabel]));
    $attribute = $installer->getAttribute($entityTypeId, $attrCode);
    $installer->addAttributeToSet($entityTypeId, $attributeSetId, $modelGroup->getId(), $attribute['attribute_id']);
}

$installer->endSetup();
