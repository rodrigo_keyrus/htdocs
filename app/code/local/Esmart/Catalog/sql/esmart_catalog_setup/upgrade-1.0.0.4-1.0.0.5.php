<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Não edite este arquivo caso você pretenda atualizar este módulo futuramente
 * para novas versões.
 *
 * @category
 * @package
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Gabriel da Gama <gabriel.gama@e-smart.com.br>
 */

/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$entityTypeId = $installer->getEntityTypeId('catalog_product');

$attrToCreate = [
    'ncm' => [
        'type' => 'varchar',
        'label' => 'NCM'
    ],
    'tax_treatment' => [
        'type' => 'int',
        'label' => 'Tratamento Fiscal'
    ]
];

foreach ($attrToCreate as $attrKey => $attrData) {

    $installer->addAttribute($entityTypeId, $attrKey,
        array_merge($attrData,
            array(
            'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
            'visible' => true,
            'required' => false,
            'user_defined' => true,
            )
        )
    );

    $attribute = $installer->getAttribute($entityTypeId, $attrKey);
    $attributeSetId = $installer->getAttributeSetId($entityTypeId, 'Default');
    $attributeGroup = $installer->getAttributeGroup($entityTypeId, $attributeSetId, 'Sephora');

    $installer->addAttributeToSet($entityTypeId, $attributeSetId, $attributeGroup['attribute_group_id'], $attribute['attribute_id']);

}

$installer->endSetup();
