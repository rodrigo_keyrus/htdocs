<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
<<<<<<< Updated upstream
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category      Esmart
 * @package       Esmart_Catalog
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Alan Carvalho <alan.carvalho@e-smart.com.br>
=======
 * Não edite este arquivo caso você pretenda atualizar este módulo futuramente
 * para novas versões.
 *
 * @category
 * @package
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Gabriel da Gama <gabriel.gama@e-smart.com.br>
>>>>>>> Stashed changes
 */

/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

/**
 * Add used in product listing in attribute
 */
$installer->updateAttribute(
    Mage_Catalog_Model_Product::ENTITY,
    'selo_exclusivo',
    'used_in_product_listing',
    true
);

$installer->updateAttribute(
    Mage_Catalog_Model_Product::ENTITY,
    'selo_lancamento',
    'used_in_product_listing',
    true
);

$installer->updateAttribute(
    Mage_Catalog_Model_Product::ENTITY,
    'selo_limitada',
    'used_in_product_listing',
    true
);

$installer->updateAttribute(
    Mage_Catalog_Model_Product::ENTITY,
    'selo_novidade',
    'used_in_product_listing',
    true
);

$installer->updateAttribute(
    Mage_Catalog_Model_Product::ENTITY,
    'selo_online',
    'used_in_product_listing',
    true
);

$installer->updateAttribute(
    Mage_Catalog_Model_Product::ENTITY,
    'selo_promocao',
    'used_in_product_listing',
    true
);

$installer->endSetup();
