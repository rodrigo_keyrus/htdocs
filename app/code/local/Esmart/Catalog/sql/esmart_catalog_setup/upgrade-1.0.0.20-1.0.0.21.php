<?php
$installer = $this;

$installer->startSetup();

$entityTypeId = $installer->getEntityTypeId('catalog_product');

$attrData = [
    'cod_sap' => 'SAP Code'
];

$data = [
    'type' => 'varchar',
    'input' => 'text',
    'default' => null,
    'required' => false,
    'user_defined' => true,
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible' => true,
    'searchable' => true,
    'filterable' => true,
    'comparable' => false,
    'visible_on_front' => true,
    'used_in_product_listing' => 1,
    'unique' => true,
];

$modelGroup = Mage::getModel('eav/entity_attribute_group')->load('BSeller Integration', 'attribute_group_name');

if (!$modelGroup->getId()) {
    $modelGroup->setAttributeGroupName('BSeller Integration')
        ->setAttributeSetId(4)
        ->setSortOrder(3);
    $modelGroup->save();
}

$attributeSetId = $modelGroup->getAttributeSetId();

foreach ($attrData as $attrCode => $attrLabel) {
    $this->addAttribute($entityTypeId, $attrCode, array_merge($data, ['label' => $attrLabel]));
    $attribute = $installer->getAttribute($entityTypeId, $attrCode);
    $installer->addAttributeToSet($entityTypeId, $attributeSetId, $modelGroup->getId(), $attribute['attribute_id']);
}

$installer->endSetup();