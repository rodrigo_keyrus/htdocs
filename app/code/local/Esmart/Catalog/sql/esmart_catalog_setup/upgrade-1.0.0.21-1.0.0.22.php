<?php
$installer = $this;

$installer->startSetup();

/**
 * Get the resource model
 */
$resource = Mage::getSingleton('core/resource');

/**
 * Retrieve the write connection
 */
$writeConnection = $resource->getConnection('core_write');

/**
 * Retrieve our product link attribute int table name
 */
$tableProductLinkAttributeInt = $resource->getTableName('catalog_product_link_attribute_int');

$sqlUpdate = "UPDATE $tableProductLinkAttributeInt SET value = 0 WHERE product_link_attribute_id = '7'";

$writeConnection->query($sqlUpdate);

$installer->endSetup();