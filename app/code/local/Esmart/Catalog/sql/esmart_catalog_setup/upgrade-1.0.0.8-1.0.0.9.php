<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Não edite este arquivo caso você pretenda atualizar este módulo futuramente
 * para novas versões.
 *
 * @category
 * @package
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Gabriel da Gama <gabriel.gama@e-smart.com.br>
 */

/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$entityTypeId = $installer->getEntityTypeId('catalog_product');

$attrData = [
    'selo_novidade' => 'Selo Novidade',
    'selo_lancamento' => 'Selo Lançamento',
    'selo_promocao' => 'Selo Promoção',
    'selo_exclusivo' => 'Selo Exclusivo',
    'selo_online' => 'Selo Online',
    'selo_limitada' => 'Selo Limitada',
];

$data = [
    'type' => 'int',
    'input' => 'select',
    'source'        => 'eav/entity_attribute_source_boolean',
    'required'     => false,
    'user_defined' => true,
    'global'       => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'       => true,
    'searchable'    => false,
    'filterable'    => false,
    'comparable'    => false,
    'visible_on_front'  => true,
];

$modelGroup = Mage::getModel('eav/entity_attribute_group')->load('Sephora Flags', 'attribute_group_name');

if (!$modelGroup->getId()) {
    $modelGroup->setAttributeGroupName('Sephora Flags')
        ->setAttributeSetId(4)
        ->setSortOrder(3);
    $modelGroup->save();
}

$attributeSetId = $installer->getAttributeSetId($entityTypeId, 'Default');

foreach ($attrData as $attrCode => $attrLabel) {
    $this->addAttribute($entityTypeId, $attrCode, array_merge($data, ['label' => $attrLabel]));
    $attribute = $installer->getAttribute($entityTypeId, $attrCode);
    $installer->addAttributeToSet($entityTypeId, $attributeSetId, $modelGroup->getId(), $attribute['attribute_id']);
}

$installer->endSetup();
