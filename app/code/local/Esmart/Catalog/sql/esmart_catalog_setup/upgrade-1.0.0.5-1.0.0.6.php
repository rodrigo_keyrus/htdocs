<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Não edite este arquivo caso você pretenda atualizar este módulo futuramente
 * para novas versões.
 *
 * @category
 * @package
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Gabriel da Gama <gabriel.gama@e-smart.com.br>
 */

/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$entityTypeId = $installer->getEntityTypeId('catalog_product');

$installer->removeAttribute(Mage_Catalog_Model_Category::ENTITY, 'reference');

$attrData = [
    'reference' => 'Referencia',
    'ax_department' => 'Departamento AX',
    'ax_category' => 'Categoria AX',
    'ax_subcategory' => 'Subcategoria AX',
];

$data = [
    'type' => 'varchar',
    'input' => 'text',
    'required'     => false,
    'user_defined' => true,
    'global'       => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'       => true,
    'searchable'    => false,
    'filterable'    => false,
    'comparable'    => false,
    'visible_on_front'  => true,
];

foreach ($attrData as $attrCode => $attrLabel) {
    $this->addAttribute($entityTypeId, $attrCode, array_merge($data, ['label' => $attrLabel]));
    $attribute = $installer->getAttribute($entityTypeId, $attrCode);
    $attributeSetId = $installer->getAttributeSetId($entityTypeId, 'Default');
    $attributeGroup = $installer->getAttributeGroup($entityTypeId, $attributeSetId, 'Sephora');
    $installer->addAttributeToSet($entityTypeId, $attributeSetId, $attributeGroup['attribute_group_id'], $attribute['attribute_id']);
}

$installer->endSetup();
