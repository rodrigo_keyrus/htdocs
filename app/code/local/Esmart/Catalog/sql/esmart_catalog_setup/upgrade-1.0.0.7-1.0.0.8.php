<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Não edite este arquivo caso você pretenda atualizar este módulo futuramente
 * para novas versões.
 *
 * @category
 * @package
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Gabriel da Gama <gabriel.gama@e-smart.com.br>
 */

/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$entityTypeId = $installer->getEntityTypeId('catalog_product');

$attrData = [
    'ax_id' => 'ID do Produto no AX',
    'youtube_url' => 'Youtube URL',
];

$data = [
    'type' => 'varchar',
    'input' => 'text',
    'required'     => false,
    'user_defined' => true,
    'global'       => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'       => true,
    'searchable'    => false,
    'filterable'    => false,
    'comparable'    => false,
    'visible_on_front'  => true,
];

$attributeSetId = $installer->getAttributeSetId($entityTypeId, 'Default');
$attributeGroup = $installer->getAttributeGroup($entityTypeId, $attributeSetId, 'Sephora');

foreach ($attrData as $attrCode => $attrLabel) {
    $this->addAttribute($entityTypeId, $attrCode, array_merge($data, ['label' => $attrLabel]));
    $attribute = $installer->getAttribute($entityTypeId, $attrCode);
    if (is_array($attributeGroup)) {
        $attributeGroup = $attributeGroup['attribute_group_id'];
    }
    $installer->addAttributeToSet($entityTypeId, $attributeSetId, $attributeGroup, $attribute['attribute_id']);
}

$installer->endSetup();
