<?php
class Esmart_Catalog_Block_LowStock extends Mage_Core_Block_Template
{
    public function getLowStockActive()
    {
        return Mage::helper('esmart_catalog')->getLowStockActive();
    }
    public function getStockMinQty()
    {
        return Mage::helper('esmart_catalog')->getStockMinQty();
    }
}