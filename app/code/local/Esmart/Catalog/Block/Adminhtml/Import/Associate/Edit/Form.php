<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_ImportAssociate
 *
 * @copyright Copyright (c) 2017 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Anderson Cardoso <anderson.cardoso@e-smart.com.br>
 */

class Esmart_Catalog_Block_Adminhtml_Import_Associate_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form(array(
            'id'        => 'edit_form',
            'action'    => $this->getUrl('*/*/save'),
            'method'    => 'post',
            'enctype'   => 'multipart/form-data'
        ));


        $fieldset = $form->addFieldset(
            'importprice_form_table',
            array(
                'legend' => $this->__('General')
            )
        );

        $fieldset->addField(
            'file',
            'file',
            array(
                'label'     => $this->__('File'),
                'required'  => true,
                'name'      => 'file'
            )
        );

        $fieldset->addField(
            'description',
            'text',
            array(
                'label'     => $this->__('Description'),
                'required'  => true,
                'name'      => 'description'
            )
        );

        $form->setUseContainer(true);
        $this->setForm($form);
        return parent::_prepareForm();
    }

}