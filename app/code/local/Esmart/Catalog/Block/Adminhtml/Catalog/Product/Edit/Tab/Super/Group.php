<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  Esmart
 * @package   Esmart_Catalog
 *
 * @copyright Copyright (c) 2019 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Evandro Veloso Gomes <evandro.gomes@e-smart.com.br>
 */

class Esmart_Catalog_Block_Adminhtml_Catalog_Product_Edit_Tab_Super_Group
    extends Mage_Adminhtml_Block_Catalog_Product_Edit_Tab_Super_Group
{
    /**
     * Prepare collection
     *
     * @return $this|Esmart_Catalog_Block_Adminhtml_Catalog_Product_Edit_Tab_Super_Group
     */
    protected function _prepareCollection()
    {

        $allowProductTypes = array();
        $allowProductTypeNodes = Mage::getConfig()
            ->getNode('global/catalog/product/type/grouped/allow_product_types')->children();
        foreach ($allowProductTypeNodes as $type) {
            $allowProductTypes[] = $type->getName();
        }

        $collection = Mage::getModel('catalog/product_link')->useGroupedLinks()
            ->getProductCollection()
            ->setProduct($this->_getProduct())
            ->addAttributeToSelect('*')
            ->addFilterByRequiredOptions()
            ->addAttributeToFilter('type_id', $allowProductTypes);

        if ($this->getIsReadonly() === true) {
            $collection->addFieldToFilter('entity_id', array('in' => $this->_getSelectedProducts()));
        }

        $this->setCollection($collection);

        return Mage_Adminhtml_Block_Widget_Grid::_prepareCollection();
    }

    /**
     * Overwrite to add seller_order_quantity column in grid
     *
     * @return $this|Esmart_Catalog_Block_Adminhtml_Catalog_Product_Edit_Tab_Super_Group
     *
     * @throws Exception
     */
    protected function _prepareColumns()
    {
        parent::_prepareColumns();

        $this->addColumn('seller_order_quantity', [
            'header'   => Mage::helper('catalog')->__('Sold amount'),
            'name'     => 'qty',
            'type'     => 'number',
            'sortable' => true,
            'width'    => '60px',
            'index'    => 'seller_order_quantity',
            'renderer' => 'Esmart_Catalog_Block_Adminhtml_Catalog_Product_Edit_Tab_Super_Renderer_SellerOrderQuantity',
            'filter_condition_callback' => array($this, '_filterSellerOrderQuantityCallback')
        ]);

        return $this;
    }

    /**
     * Overwrite to sort seller_order_quantity column in grid
     *
     * @param Mage_Adminhtml_Block_Widget_Grid_Column $column
     *
     * @return $this|Mage_Adminhtml_Block_Widget_Grid
     *
     * @throws Exception
     */
    protected function _setCollectionOrder($column)
    {
        $collection = $this->getCollection();
        if ($collection) {
            if ($column->getId() == 'seller_order_quantity') {
                foreach ($collection as $item) {
                    $item->setSellerOrderQuantity($item->getSellerOrderQuantity() ? $item->getSellerOrderQuantity() : 0);
                }

                $collectionItems = $collection->getItems();
                usort($collectionItems, array($this, '_sortSellerOrderQuantity'));  // re-sort collection
                $newCollection = new Varien_Data_Collection();        // create new collection

                foreach ($collectionItems as $item) {
                    $newCollection->addItem($item);                // assign each item to new collection
                }
                $this->setCollection($newCollection);
            }
        }

        return parent::_setCollectionOrder($column);
    }

    /**
     * Custom function to sort seller_order_quantity column in grid
     *
     * @param int $a
     * @param int $b
     *
     * @return int
     */
    public function _sortSellerOrderQuantity($a, $b)
    {
        $columnId = "seller_order_quantity";
        $dir      = $this->getParam($this->getVarNameDir(), $this->_defaultDir);

        $al = strtolower($a->getData($columnId));
        $bl = strtolower($b->getData($columnId));

        $al = (!$al ? 0 : $al);
        $bl = (!$bl ? 0 : $bl);

        if ($al == $bl) {
            return 0;
        }

        if ($dir == 'asc') {
            return ($al < $bl) ? -1 : 1;
        } else {
            return ($al > $bl) ? -1 : 1;
        }
    }

    /**
     * Overwrite to filter seller_order_quantity column in grid
     *
     * @param Varien_Data_Collection $collection
     * @param Mage_Adminhtml_Block_Widget_Grid_Column $column
     *
     * @return $this|Esmart_Catalog_Block_Adminhtml_Catalog_Product_Edit_Tab_Super_Group
     */
    protected function _filterSellerOrderQuantityCallback($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return $this;
        }

        if ((count($value) == 2) && ($value['from'] == 0) && ($value['to'] == 0) ) {
            $this->getCollection()->addAttributeToFilter('seller_order_quantity', array('null' => true), 'left');
            return $this;
        }

        if (array_key_exists('from', $value) && ($value['from'] != '0')) {
            $this->getCollection()->addAttributeToFilter(
                [
                    ['attribute' => 'seller_order_quantity', 'gteq' => $value['from']],
                    ['attribute' => 'seller_order_quantity', 'null' => ($value['from'] == '0' ? true : false)]
                ],
                '',
                ($value['from'] == '0' ? 'left' : 'inner')
            );
        }

        if (array_key_exists('to', $value)) {
            $this->getCollection()->addAttributeToFilter(
                [
                    ['attribute' => 'seller_order_quantity', 'lteq' => $value['to']],
                    [
                        'attribute' => 'seller_order_quantity',
                        'null' => ((count($value) == 1) || ($value['from'] == '0') ? true : false)
                    ]
                ],
                '',
                ((count($value) == 1) || ($value['from'] == '0') ? 'left' : 'inner')
            );
        }

        return $this;
    }
}