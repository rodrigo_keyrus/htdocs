<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   sephora
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author   Rafael Falcão <rafael.falcao@e-smart.com.br>
 */

class Esmart_Catalog_Block_Adminhtml_Catalog_Product_Edit_Edit extends Mage_Adminhtml_Block_Catalog_Product_Edit
{
    /**
     * @return Mage_Core_Block_Abstract|void
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        if(!$this->getIsGrouped() && !$this->getIsBundle()){
            return;
        }

        if (!$this->getProduct()->isReadonly()) {
            $this->setChild('save_and_edit_button',
                $this->getLayout()->createBlock('adminhtml/widget_button')
                    ->setData(array(
                        'label'     => Mage::helper('catalog')->__('Save and Continue Edit'),
                        'onclick'   => 'validateFields(\''.$this->getSaveAndContinueUrl().'\')',
                        'class' => 'save'
                    ))
            );

            $this->setChild('save_button',
                $this->getLayout()->createBlock('adminhtml/widget_button')
                    ->setData(array(
                        'label'     => Mage::helper('catalog')->__('Save'),
                        'onclick'   => 'validateFields()',
                        'class' => 'save'
                    ))
            );
        }
    }

    /**
     * @return string
     */
    public function getAssociatedSimpleProducts()
    {
        if(!$this->getIsGrouped() && !$this->getIsBundle()){
            return '';
        }

        if($this->getIsBundle()){
            return $this->__toHtml($this->getBundleAssociated());
        }

        $associatedProducts = $this->getProduct()->getTypeInstance()->getAssociatedProducts();

        $simpleProducts = array();
        foreach ($associatedProducts as $simple){
            $simpleProducts[] = $simple->getEntityId();
        }

        return $this->__toHtml($simpleProducts);
    }


    /**
     * @return array
     */
    public function getBundleAssociated()
    {
        $bundledProduct = $this->getProduct();

        if(!$bundledProduct->getId()){
            return [];
        }

        $selectionCollection = $bundledProduct->getTypeInstance(true)->getSelectionsCollection(
            $bundledProduct->getTypeInstance(true)->getOptionsIds($bundledProduct), $bundledProduct
        );

        $bundledItems = array();
        foreach($selectionCollection as $option)
        {
            if($option->getParentProductId() == $bundledProduct->getId()){
                $bundledItems[] = $option->product_id;
            }

        }

        return $bundledItems;
    }

    /**
     * @return string
     */
    public function getCategories()
    {
        if(!$this->getIsGrouped() && !$this->getIsBundle()){
            return '';
        }

        $categories = $this->getProduct()->getCategoryIds();

        return $this->__toHtml($categories);
    }

    /**
     * @param $items
     * @return string
     */
    protected function __toHtml($items)
    {
        if(!is_array($items)){
            return '';
        }

        return implode(',', $items);
    }

    /**
     * Check if product is a bundle
     *
     * @return bool
     */
    public function getIsBundle()
    {
        if($this->getProduct()->getTypeId() == 'bundle'){
            return true;
        }

        return false;
    }
}