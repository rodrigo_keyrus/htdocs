<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  Esmart
 * @package   Esmart_Catalog
 *
 * @copyright Copyright (c) 2019 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Evandro Veloso Gomes <evandro.gomes@e-smart.com.br>
 */

class Esmart_Catalog_Block_Adminhtml_Catalog_Product_Edit_Tab_Super_Renderer_SellerOrderQuantity
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Action
{
    /**
     * Format the field seller_order_quantity in the grid
     *
     * @param Varien_Object $row
     * @return int|string
     */
    public function render(Varien_Object $row)
    {
        return $row->getData('seller_order_quantity') ? intval($row->getData('seller_order_quantity')) : '0';
    }
}