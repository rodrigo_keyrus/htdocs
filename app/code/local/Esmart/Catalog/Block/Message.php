<?php

class Esmart_Catalog_Block_Message extends Mage_Core_Block_Template
{
    protected $_message = false;

    public function _construct()
    {
        $params = Mage::app()->getRequest()->getParams();
        if(isset($params['message']) && $params['message'] == 'disabled-product') {
            $this->_message = $this->__("The product you are interested in is not available, but we think you'll like it.");
        }
        parent::_construct();
    }

    public function getMessage()
    {
        return $this->_message;
    }
}