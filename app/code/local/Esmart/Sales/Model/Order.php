<?php

/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  Esmart
 * @package   Esmart_Sales
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Evandro Veloso Gomes <evandro.gomes@e-smart.com.br>
 */


class Esmart_Sales_Model_Order extends Mage_Sales_Model_Order
{
    /**
     * Retrieve order reorder availability
     *
     * @param bool $ignoreSalable
     * @return bool
     */
    protected function _canReorder($ignoreSalable = false)
    {
        if ($this->getActionFlag(self::ACTION_FLAG_REORDER) === false) {
            return false;
        }

        foreach ($this->getItemsCollection() as $item) {
            $productId = $item->getProductId();

            $product = Mage::getModel('catalog/product')
                ->setStoreId($this->getStoreId())
                ->load($productId);

            if (!$product->getId()) {
                return false;
            }

            if (($ignoreSalable || $product->isSalable()) && !$product->getData('is_sample') && $product->isAvailable()) {
                return true;
            }
        }

        return false;
    }

    public function getDateEmail($format)
    {
        if (is_null($format)) {
            return;
        }

        $date = $this->getData('created_at');
        return date_format(new DateTime($date), $format);
    }
}
