<?php

class Esmart_Sales_Model_Device extends BSeller_Core_Model_Abstract
{

    CONST DEVICE_ID_DESKTOP     = 'desktop';
    CONST DEVICE_ID_MOBILE      = 'mobile';

    CONST DEVICE_OS_WINDOWS     = 'windows';
    CONST DEVICE_OS_LINUX       = 'linux';
    CONST DEVICE_OS_ANDROID     = 'android';
    CONST DEVICE_OS_IOS         = 'ios';

}
