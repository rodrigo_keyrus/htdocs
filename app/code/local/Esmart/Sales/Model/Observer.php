<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category      Esmart
 * @package       Esmart_sales
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Guilherme Miguelete <guilherme.miguelete@e-smart.com.br>
 */

class Esmart_Sales_Model_Observer
{

    /**
     * Sort products by stock last
     *
     * @param Varien_Event_Observer $observer
     * @return $this
     */
    public function setIsMobileOnQuote(Varien_Event_Observer $observer)
    {

        /** @var Mage_Checkout_Model_Cart $collection */
        $cart = $observer->getEvent()->getData('cart');

        /** @var Mage_Sales_Model_Quote $quote */
        $quote = $cart->getQuote();

        if (!$quote->getId()) {
            return $this;
        }

        $data = [
            'device_id' => Esmart_Sales_Model_Device::DEVICE_ID_DESKTOP,
            'device_os' => $this->getOsDevice()
        ];

        if ($this->isMobile()) {
            $data['device_id'] = Esmart_Sales_Model_Device::DEVICE_ID_MOBILE;
        }

        $quote->addData($data);

        return $this;
    }


    /**
     * @return int
     */
    public function isMobile()
    {
        return preg_match(
            "/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i",
            $_SERVER["HTTP_USER_AGENT"]
        );
    }


    /**
     * @return bool
     */
    public function getOsDevice()
    {
        $userAgent  = strtolower(Mage::helper('core/http')->getHttpUserAgent());
        $iosDevice  = ['iphone', 'ipod', 'ipad', 'macosx'];
        $device     = Esmart_Sales_Model_Device::DEVICE_OS_WINDOWS;

        foreach ($iosDevice as $val) {
            if (stripos($userAgent, $val) !== false) {
                $device = Esmart_Sales_Model_Device::DEVICE_OS_IOS;
                break;
            }
        }

        if (stripos($userAgent, 'linux') !== false) {
            $device = Esmart_Sales_Model_Device::DEVICE_OS_LINUX;
        }

        if (stripos($userAgent, 'windows') !== false) {
            $device = Esmart_Sales_Model_Device::DEVICE_OS_WINDOWS;
        }

        if (stripos($userAgent, 'android') !== false) {
            $device = Esmart_Sales_Model_Device::DEVICE_OS_ANDROID;
        }

        return $device;
    }

}
