<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category      Esmart
 * @package       Esmart_Sales
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class Esmart_Sales_Block_Order_Items extends Mage_Sales_Block_Order_Items
{
    const STEP_FIRST = 'one';

    const STEP_SECOND = 'two';

    const STEP_THIRD = 'three';
    
    const STEP_FOURTH = 'four';

    const STEP_FIFTH = 'five';

    /**
     * Order state step options
     *
     * @var array
     */
    protected $_orderStateStep = array(
        Mage_Sales_Model_Order::STATE_CANCELED   => self::STEP_SECOND,
        Mage_Sales_Model_Order::STATE_CLOSED     => self::STEP_SECOND,
        Mage_Sales_Model_Order::STATE_PROCESSING => self::STEP_SECOND,
        Mage_Sales_Model_Order::STATE_COMPLETE   => self::STEP_FIFTH
    );

    protected $_customizedStatusStep = array(
        'nfe'   => self::STEP_THIRD,
        'shipping'   => self::STEP_FOURTH,
        'complete'   => self::STEP_FIFTH
    );

    /**
     * Return order state step
     *
     * @return string
     */
    public function getOrderStateStep()
    {
        $status = $this->getOrder()->getStatus();
        if (isset($this->_customizedStatusStep[$status])) {
            return $this->_customizedStatusStep[$status];
        }

        $state = $this->getOrder()->getState();

        if (!isset($this->_orderStateStep[$state])) {
            return self::STEP_FIRST;
        }

        return $this->_orderStateStep[$state];
    }

    /**
     * Return created at date
     *
     * @param null|string $instanceName
     * @return string
     */
    public function getCreatedAtDate($instanceName = null)
    {
        $order = $this->getOrder();

        switch ($instanceName) {
            case 'invoice':
                /** @var Mage_Sales_Model_Order_Invoice $invoice */
                $instance = Mage::getModel('sales/order_invoice')->load($order->getId(), 'order_id');

                break;
            case 'shipment':
                /** @var Mage_Sales_Model_Order_Invoice $invoice */
                $instance = Mage::getModel('sales/order_shipment')->load($order->getId(), 'order_id');

                break;
            case 'creditmemo':
                /** @var Mage_Sales_Model_Order_Invoice $invoice */
                $instance = Mage::getModel('sales/order_creditmemo')->load($order->getId(), 'order_id');

                break;

            case 'canceled':
            case 'complete':
                $instance = $order->getAllStatusHistory();
                $instance = reset($instance);
                break;

            default:
                $instance = $order;
        }

        if (!$instance->getCreatedAt()) {
            return '';
        }

        return Mage::helper('core')->formatDate($instance->getCreatedAt());
    }

    /**
     * Return freight identifier
     *
     * @return string
     */
    public function getFreightId()
    {
        $order = $this->getOrder();

        /**
         * NORMAL method
         */
        $freightId = '3164';

        /**
         * Ibge codes in array
         */

        $ibgeCode         = Mage::helper('esmart_sales')->getIbgeCode($order->getShippingAddress());
        $ibgeCodesString  = Mage::getStoreConfig('esmart_sales/ax_order/ibge_aereo');
        $ibgeCodesToAereo = (array) explode(',', $ibgeCodesString);

        /**
         * EXPRESS method
         */
        if (in_array($ibgeCode, $ibgeCodesToAereo)) {
            $freightId = '1013';
        }

        /**
         * SCHEDULE method
         */
        if ($order->getData('firecheckout_delivery_date')) {
            $freightId = '3003';
        }

        return $freightId;
    }

    public function getFreightIdBseller()
    {
        $freightId = '4542';
        return $freightId;
    }
}
