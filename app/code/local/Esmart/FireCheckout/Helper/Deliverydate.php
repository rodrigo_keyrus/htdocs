<?php

class Esmart_FireCheckout_Helper_Deliverydate extends TM_FireCheckout_Helper_Deliverydate
{
    /**
     * Validates date string
     *
     * @param Zend_Date $date
     * @return boolean
     */
    public function isValidDate(Zend_Date $date)
    {
        // check for past date
        $offset = $this->getOffset();
        $shippingDesc = Mage::getSingleton('checkout/session')
            ->getQuote()
            ->getShippingAddress()
            ->getShippingDescription();

        $daysToAdd = preg_replace('/\D/', '', $shippingDesc);

        $offset = $offset + ($daysToAdd - $offset);

        $minAllowedDate  = Mage::app()->getLocale()->date()
            ->setTimezone('UTC')
            ->setTime('00:00:00');

        $nonPeriodicalDates = $this->getExcludedNonPeriodicalDateStrings();
        $periodicalDates    = $this->getExcludedPeriodicalDateStrings();
        $excludeWeekands = Mage::getStoreConfig('firecheckout/delivery_date/exclude_weekend');
        $weekandDays = Mage::getStoreConfig('general/locale/weekend');
        $weekandDays = explode(',', $weekandDays);

        // offset minAllowedDate considering weekends and holidays
        $calculatedOffset = 0;
        $i = 0; // count iterations;
        while ($calculatedOffset < $offset) {
            $i++;
            if ($i >= 365) {
                // possible continious loop fix
                break;
            }
            $minAllowedDate->addDay(1);

            if ($excludeWeekands) {
                $weekDay = $minAllowedDate->get(Zend_Date::WEEKDAY_DIGIT);
                if (in_array($weekDay, $weekandDays)) {
                    continue;
                }
            }

            $dateString = $minAllowedDate->toString('MM/dd/yyyy');
            if (in_array($dateString, $nonPeriodicalDates)
                || in_array(substr($dateString, 0, 7), $periodicalDates)) {
                continue;
            }
            $calculatedOffset += 1;
        }

        // compare with end of delivery processing day
        $endTime = $this->getEndOfDeliveryProcessingDay();
        $endTime = $endTime->getTime();
        $nowTime = Mage::app()->getLocale()->date()->getTime();
        if ($nowTime->compare($endTime) > 0) { // delivery processing day is ended
            $minAllowedDate->addDay(1);
        }

        if (1 === $minAllowedDate->compare($date)) {
            return false;
        }

        $period = (int)Mage::getStoreConfig('firecheckout/delivery_date/date_period');
        $maxAllowedDate = $minAllowedDate->addDay($period)->setTime('23:59:59');
        if (-1 === $maxAllowedDate->compare($date)) {
            return false;
        }

        $dateString = $date->toString('MM/dd/yyyy');
        if (in_array($dateString, $nonPeriodicalDates)
            || in_array(substr($dateString, 0, 7), $periodicalDates)) {

            return false;
        }

        if (!$excludeWeekands) {
            return true;
        }

        $weekDay = $date->get(Zend_Date::WEEKDAY_DIGIT);

        return !in_array($weekDay, $weekandDays);
    }
}
