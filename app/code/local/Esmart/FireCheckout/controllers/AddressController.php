<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category      Esmart
 * @package       Esmart_FireCheckout
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class Esmart_FireCheckout_AddressController extends Mage_Core_Controller_Front_Action
{
    /**
     * Address form
     *
     * @return $this
     */
    public function formAction()
    {
        if (!$this->getRequest()->isXmlHttpRequest()) {
            $this->_forward('defaultNoRoute');

            return $this;
        }

        $addressId = $this->getRequest()->getParam('address_id');

        try {
            /** @var Esmart_FireCheckout_Block_Address_Form $block */
            $block = $this->getLayout()->createBlock('esmart_firecheckout/address_form');
            $block->setAddressId($addressId);

            $response['error'] = false;
            $response['html']  = $block->toHtml();
        } catch (Exception $e) {
            $response['error']   = true;
            $response['message'] = $e->getMessage();
        }

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));

        return $this;
    }

    /**
     * Save address in quote session
     *
     * @return $this
     */
    public function saveAction()
    {
        if (!$this->getRequest()->isXmlHttpRequest()) {
            $this->_forward('defaultNoRoute');

            return $this;
        }

        $params = $this->getRequest()->getParams();

        /**
         * Remove address id from shipping node
         */
        unset($params['shipping']['address_id']);

        /** @var false|int $addressId */
        $addressId = $this->getRequest()->getParam('address_id', false);

        /** @var Mage_Customer_Model_Address $address */
        $address = Mage::getModel('customer/address');

        if ($addressId !== false) {
            $address->load($params['address_id']);
        }

        $address->addData($params['shipping']);

        /** @var Mage_Customer_Model_Customer $customer */
        $customer = Mage::helper('checkout/cart')->getQuote()->getCustomer();

        /**
         * Set data to new address
         */
        if ($address->isObjectNew()) {
            $address->setCustomerId($customer->getId());
            $address->setData('save_in_address_book', true);
        }

        try {
            /**
             * Security treatment for customer
             */
            if ($address->getCustomerId() !== $customer->getId()) {
                Mage::throwException($this->__('An error has occurred.'));
            }

            /** @var array|bool $errors */
            $errors = $address->validate();

            if (is_array($errors)) {
                Mage::throwException(implode(PHP_EOL, $errors));
            }

            /**
             * Clone address before save
             */
            $renewedAddress = clone $address;

            /**
             * Create or update address
             */
            $address->save();

            /**
             * Persist address identifier
             */
            $renewedAddress->setId($address->getId());

            /** @var Esmart_FireCheckout_Block_Address_Radio $block */
            $block = $this->getLayout()->createBlock('esmart_firecheckout/address_radio');
            $block->setTemplate('tm/firecheckout/checkout/address/radio.phtml');
            $block->setRenewedAddress($renewedAddress);
            $block->setAddressOptions(Mage_Customer_Model_Address::TYPE_SHIPPING);

            $billingForm = $this->getLayout()->createBlock('firecheckout/checkout_billing');
            $billingForm->setAddress($renewedAddress);
            $billingForm->setTemplate('tm/firecheckout/checkout/billing.phtml');

            $response['error']      = false;
            $response['html']       = $block->toHtml();
            $response['form']       = $billingForm->toHtml();
            $response['address_id'] = $address->getId();
        } catch (Exception $e) {
            Mage::logException($e);

            $response['error']   = true;
            $response['message'] = $e->getMessage();
        }

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));

        return $this;
    }
}
