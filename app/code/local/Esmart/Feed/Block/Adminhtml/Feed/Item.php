<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category      Esmart
 * @package       Esmart_Feed
 *
 * @copyright     Copyright (c) 2015 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class Esmart_Feed_Block_Adminhtml_Feed_Item extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * Create widget feeds head
     */
    public function __construct()
    {
        $this->_controller = 'adminhtml_feed_item';
        $this->_blockGroup = 'esmart_feed';
        $this->_headerText = $this->__('Manage Feeds');

        parent::__construct();
    }
}
