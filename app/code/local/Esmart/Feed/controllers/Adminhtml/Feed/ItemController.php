<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category      Esmart
 * @package       Esmart_Feed
 *
 * @copyright     Copyright (c) 2015 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class Esmart_Feed_Adminhtml_Feed_ItemController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Feeds list
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Feeds grid
     */
    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('esmart_feed/adminhtml_feed_item_grid')->toHtml()
        );
    }

    /**
     * Add new feed
     */
    public function newAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Edit feed
     */
    public function editAction()
    {
        $entityId = (int) $this->getRequest()->getParam('entity_id');

        $item = Mage::getModel('esmart_feed/item')->load($entityId);

        Mage::helper('esmart_feed')->setRegistryItem($item);

        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Add feed on resque
     */
    public function enqueueAction()
    {
        $entityId = (int) $this->getRequest()->getParam('entity_id');
        $item     = Mage::getModel('esmart_feed/item')->load($entityId);

        try {
            $item->enqueue();

            Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Added to queue, please wait.'));
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }

        $this->_redirect('*/*/edit', array('entity_id' => $entityId));
    }

    /**
     * Delete feed
     */
    public function deleteAction()
    {
        $entityId = (int) $this->getRequest()->getParam('entity_id');
        $helper   = Mage::helper('esmart_feed');
        $item     = Mage::getModel('esmart_feed/item')->load($entityId);

        try {
            if ($item->getIsProcessing()) {
                Mage::throwException($this->__('%s can not be deleted because it is running.', $item->getName()));
            }

            $helper->deleteFile($item->getFilename());
            $item->delete();

            Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Successfully deleted.'));
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }

        $this->_redirect('*/*/index');
    }

    /**
     * Save feed data
     */
    public function saveAction()
    {
        if (!$this->getRequest()->isPost()) {
            $this->_redirect('*/*/index');
        }

        $post = $this->getRequest()->getPost();
        $back = $this->getRequest()->getParam('back');

        /** @var Esmart_Feed_Model_Item $item */
        $item = Mage::getModel('esmart_feed/item');
        $data = $item->getFilteredData($post);

        try {
            $item->addData($data);
            $item->save();

            Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Successfully saved.'));
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }

        $this->_redirect(
            '*/*/' . ($back ? 'edit' : 'index'),
            ($back ? ['entity_id' => $data['entity_id']] : [])
        );
    }

    /**
     * Delete massive feeds
     */
    public function massDeleteAction()
    {
        $entityIds = (array) $this->getRequest()->getParam('entity_id');
        $helper    = Mage::helper('esmart_feed');

        try {
            foreach ($entityIds as $entityId) {
                $item = Mage::getModel('esmart_feed/item')->load($entityId);
                if ($item->getIsProcessing()) {
                    Mage::getSingleton('adminhtml/session')->addNotice(
                        $this->__('%s can not be deleted because it is running.', $item->getName())
                    );

                    continue;
                }

                $helper->deleteFile($item->getFilename());
                $item->delete();
            }

            Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Successfully deleted.'));
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }

        $this->_redirect('*/*/index');
    }

    /**
     * Is allowed to access
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('esmart/esmart_feed/item');
    }
}
