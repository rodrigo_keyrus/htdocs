<?php
class Esmart_Integrationexception_Model_Resource_Integrationexception extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('esmart_integrationexception/exceptions', 'id');
    }
}