<?php
class Esmart_Integrationexception_Model_Resource_Integrationexception_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    public function _construct()
    {
        $this->_init('esmart_integrationexception/integrationexception');
    }
}