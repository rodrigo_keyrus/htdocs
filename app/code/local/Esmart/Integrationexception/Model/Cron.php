<?php

/**
 * Class Esmart_Integrationexception_Model_Cron
 */
class Esmart_Integrationexception_Model_Cron
{
    /**
     * @return $this
     */
    public function blockedOrders()
    {
        if (count($this->getCollectionOrdersBlocked()->getItems()) > 0) {
            foreach ($this->getCollectionOrdersBlocked()->getItems() as $order) {
                $collectionHistory = $this->getCollectionHistoryByParentId($order->getId());
                $comment = $collectionHistory->getComment();

                if (stripos($comment, 'PC_ITEG.cod_item')) {
                    $loadOrder = Mage::getModel('sales/order')->load($order->getId());
                    $loadException = Mage::getModel('esmart_integrationexception/integrationexception')->loadByIncrementId($loadOrder->getIncrementId());

                    if ($loadException->getId()) {
                        continue;
                    }

                    foreach ($loadOrder->getAllItems() as $item) {
                        $checkItemApi = json_decode($this->checkItemBSeller($item->getSku()));

                        if (!is_null($checkItemApi)) {
                            if (!empty($checkItemApi->message)) {
                                $this->addException(
                                    $loadOrder->getIncrementId(),
                                    'API BSeller: ' . $checkItemApi->message,
                                    '(1) Incluir/Atualizar cod_sap no Magento. <br/> (2) Incluir/Atualizar produto no SAP. <br/> (3) Aguardar a integração de produtos e pedidos.',
                                    Mage::getModel('core/date')->gmtDate()
                                );
                            }
                        }
                    }
                }
            }
        }

        return $this;
    }


    /**
     * @return $this
     */
    public function cleanGridException()
    {
        foreach ($this->getCollectionExceptions() as $e) {

            if ($e['erp_status'] != BSellerERP_Order_Helper_Status::BLOCKED) {
                $removeException = Mage::getModel('esmart_integrationexception/integrationexception')->load($e['id']);
                $removeException->delete();
            }
        }

        return $this;
    }

    /**
     * @return object
     */
    public function getCollectionExceptions()
    {
        $collectionException = Mage::getModel('esmart_integrationexception/integrationexception')->getCollection();
        $collectionException->addFieldToSelect(array('increment_id'));

        $joinOrders = $collectionException->getSelect()
            ->joinInner(
                array('sfo' => 'sales_flat_order'),
                'sfo.increment_id = main_table.increment_id',
                'erp_status'
            )
            ->query()
            ->fetchAll();

        return $joinOrders;
    }

    /**
     * @return object
     */
    public function getCollectionOrdersBlocked()
    {
        $collectionOrders = Mage::getModel('sales/order')->getCollection();
        $collectionOrders->addFieldToSelect(array('entity_id','increment_id'))
            ->addFieldToFilter('erp_status', 'blocked');

        return $collectionOrders;
    }

    /**
     * @param $parentId
     *
     * @return mixed
     */
    public function getCollectionHistoryByParentId($parentId)
    {
        $collectionHistory = Mage::getResourceModel('sales/order_status_history_collection')
            ->addAttributeToFilter('parent_id',$parentId)
            ->getLastItem();

        return $collectionHistory;
    }

    /**
     * @param $incrementId
     * @param $messageError
     * @param $solution
     * @param $createdAt
     *
     * @return $this
     */
    public function addException($incrementId, $messageError, $solution, $createdAt)
    {
        $addException = Mage::getModel('esmart_integrationexception/integrationexception');
        $addException->setIncrementId($incrementId)
            ->setMessageError($messageError)
            ->setSolution($solution)
            ->setCreatedAt($createdAt)
            ->save();

        return $this;
    }

    /**
     * @param null $codItem
     *
     * @return mixed
     */
    public function checkItemBSeller($codItem = null)
    {
        $ch = curl_init();

        $options = array(
            CURLOPT_URL => $this->getAccessApiBSeller()['endpoint'] . '/itens/' . $codItem,
            CURLOPT_HTTPHEADER => array('X-Auth-Token: ' . $this->getAccessApiBSeller()['token'] . ''),
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_RETURNTRANSFER => true
        );

        curl_setopt_array($ch, $options);

        $output = curl_exec($ch);

        return $output;
    }

    /**
     * @return array
     */
    public function getAccessApiBSeller()
    {
        $access = array();

        if (Mage::getStoreConfig('bsellererp_core/settings/mode') == 'homologation') {
            $access['endpoint'] = Mage::getStoreConfig('bsellererp_core/settings/homologation_endpoint');
            $access['token'] = Mage::getStoreConfig('bsellererp_core/settings/homologation_token');
        }else {
            $access['endpoint'] = Mage::getStoreConfig('bsellererp_core/settings/production_endpoint');
            $access['token'] = Mage::getStoreConfig('bsellererp_core/settings/production_token');
        }

        return $access;
    }
}