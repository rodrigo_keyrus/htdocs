<?php
class Esmart_Integrationexception_Model_Integrationexception extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('esmart_integrationexception/integrationexception');
    }

    public function loadByIncrementId($increment_id)
    {
        $collection = Mage::getResourceModel('esmart_integrationexception/integrationexception_collection')
            ->addFieldToFilter('increment_id',$increment_id)
            ->getLastItem();

        return $collection;
    }
}