<?php
class Esmart_Integrationexception_Block_Adminhtml_Integrationexception_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('esmart_integrationexception_grid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(false);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('esmart_integrationexception/integrationexception_collection');
        $this->setCollection($collection);
        parent::_prepareCollection();
        return $this;
    }

    protected function _prepareColumns()
    {
        $helper = Mage::helper('esmart_integrationexception');

        $this->addColumn('id', array(
            'header' => $helper->__('ID #'),
            'index'  => 'id',
            'filter' => false
        ));

        $this->addColumn('increment_id', array(
            'header' => $helper->__('Increment ID'),
            'type'   => 'text',
            'index'  => 'increment_id'
        ));

        $this->addColumn('message_error', array(
            'header' => $helper->__('Error Integration'),
            'type'   => 'text',
            'index'  => 'message_error',
            'filter' => false
        ));

        $this->addColumn('solution', array(
            'header' => $helper->__('Solution'),
            'type'   => 'text',
            'index'  => 'solution',
            'filter' => false
        ));

        $this->addColumn('created_at', array(
            'header' => $helper->__('Generated At'),
            'type'   => 'datetime',
            'index'  => 'created_at'
        ));

        return parent::_prepareColumns();
    }
}