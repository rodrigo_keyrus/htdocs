<?php

class Esmart_Integrationexception_Block_Adminhtml_Integrationexception extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'esmart_integrationexception';
        $this->_controller = 'adminhtml_integrationexception';
        $this->_headerText = Mage::helper('sales')->__('Integration Exception');

        parent::__construct();
        $this->_removeButton('add');
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('esmart_integrationexception/adminhtml_integrationexception_grid')->toHtml()
        );
    }
}