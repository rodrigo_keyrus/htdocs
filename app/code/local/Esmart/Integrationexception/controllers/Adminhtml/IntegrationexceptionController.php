<?php

class Esmart_Integrationexception_Adminhtml_IntegrationexceptionController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->_title($this->__('Sales'))->_title($this->__('Integration Exception'));
        $this->loadLayout();
        $this->_setActiveMenu('sales/sales');
        $this->_addContent($this->getLayout()->createBlock('esmart_integrationexception/adminhtml_integrationexception'));
        $this->renderLayout();
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('esmart_integrationexception/adminhtml_integrationexception_grid')->toHtml()
        );
    }

    public function exportInchooCsvAction()
    {
        $fileName = 'esmart_integrationexception.csv';
        $grid = $this->getLayout()->createBlock('esmart_integrationexception/adminhtml_integrationexception_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }

    public function exportInchooExcelAction()
    {
        $fileName = 'esmart_integrationexception.xml';
        $grid = $this->getLayout()->createBlock('esmart_integrationexception/adminhtml_integrationexception_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }
}