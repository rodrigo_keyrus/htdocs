<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition End User License Agreement
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magento.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Customer
 * @copyright Copyright (c) 2006-2016 X.commerce, Inc. and affiliates (http://www.magento.com)
 * @license http://www.magento.com/license/enterprise-edition
 */

/**
 * Customer address edit block
 *
 * @category   Mage
 * @package    Mage_Customer
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Esmart_Customer_Block_Address_Edit extends Mage_Customer_Block_Address_Edit
{

    /**
     * @return string
     */
    public function getSaveUrl()
    {
        $defaultBillingId = Mage::getSingleton('customer/session')->getCustomer()->getDefaultBilling();
        $defaultShippingId = Mage::getSingleton('customer/session')->getCustomer()->getDefaultShipping();
        if ($defaultBillingId == $defaultShippingId) {
            return Mage::getUrl('customer/address/formPost', array('_secure'=>true));
        }
        return Mage::getUrl('customer/address/formPost', array('_secure'=>true, 'id'=>$this->getAddress()->getId()));
    }

    /**
     * @return bool
     */
    public function IsBillingEdit()
    {
        if ($this->getRequest()->getParam('billing')) {
            return true;
        }
        return false;
    }

}
