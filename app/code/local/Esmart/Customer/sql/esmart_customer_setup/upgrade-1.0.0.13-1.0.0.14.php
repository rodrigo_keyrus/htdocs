<?php
/* @var $installer Mage_Sales_Model_Resource_Setup */
$installer = new Mage_Sales_Model_Resource_Setup('core_setup');
$connection = $installer->getConnection();
$installer->startSetup();

/**
 * Add column to sales_float_quote_address and sales_flat_order_address tables
 */
$tables = [
    'quote_address',
    'order_address'
];

foreach ($tables as $table) {
    $installer->addAttribute($table, 'cellphone', ['type' => Varien_Db_Ddl_Table::TYPE_VARCHAR]);
}

$installer->endSetup();