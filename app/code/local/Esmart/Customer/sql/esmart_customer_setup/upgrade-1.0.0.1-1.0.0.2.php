<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category      Esmart
 * @package       Esmart_Customer
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

/* @var $installer Mage_Customer_Model_Entity_Setup */
$installer = $this;

$installer->startSetup();

/** @var Varien_Db_Adapter_Pdo_Mysql $connection */
$connection = $installer->getConnection();

/**
 * Customer address forms
 */
$usedInForms = array(
    'adminhtml_customer_address',
    'customer_register_address',
    'customer_address_edit'
);

/**
 * Customer address attributes
 */
$installer->addAttribute(
    'customer_address',
    'cellphone',
    array(
        'type'         => 'varchar',
        'source'       => '',
        'visible'      => false,
        'required'     => false,
        'input'        => 'text',
        'label'        => 'Cellphone',
        'system'       => false,
        'user_defined' => true,
        'global'       => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL
    )
);

$attribute = Mage::getSingleton('eav/config')->getAttribute('customer_address', 'cellphone');
$attribute->setData('used_in_forms', $usedInForms);
$attribute->save();

$installer->updateAttribute('customer_address', 'telephone', 'is_visible', false);

$installer->endSetup();
