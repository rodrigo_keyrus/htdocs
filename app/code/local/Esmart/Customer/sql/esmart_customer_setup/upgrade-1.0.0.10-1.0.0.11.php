<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category      Esmart
 * @package       Esmart_Customer
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Guilherme Miguelete <guilherme.miguelete@e-smart.com.br>
 */

/* @var $installer Mage_Customer_Model_Entity_Setup */
$installer = $this;

$installer->startSetup();

/**
 * Create legacy_user_id in customer
 */
$installer->removeAttribute('customer', 'legacy_user_id');
$installer->addAttribute(
    'customer',
    'legacy_user_id',
    array(
        'type'         => 'varchar',
        'source'       => '',
        'visible'      => true,
        'required'     => false,
        'input'        => 'text',
        'label'        => 'User ID',
        'system'       => false,
        'user_defined' => false,
        'position'     => 122,
        'global'       => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL
    )
);

$attribute = Mage::getSingleton('eav/config')->getAttribute('customer', 'legacy_user_id');
$attribute->setData('used_in_forms', ['adminhtml_customer']);
$attribute->save();


/**
 * Create legacy_business_phone in customer
 */
$installer->removeAttribute('customer', 'legacy_business_phone');
$installer->addAttribute(
    'customer',
    'legacy_business_phone',
    array(
        'type'         => 'varchar',
        'source'       => '',
        'visible'      => true,
        'required'     => false,
        'input'        => 'text',
        'label'        => 'Business Phone',
        'system'       => false,
        'user_defined' => false,
        'position'     => 122,
        'global'       => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL
    )
);

$attribute = Mage::getSingleton('eav/config')->getAttribute('customer', 'legacy_business_phone');
$attribute->setData('used_in_forms', ['adminhtml_customer']);
$attribute->save();

$installer->endSetup();
