<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category      Esmart
 * @package       Esmart_Customer
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class Esmart_Customer_Model_Attribute_Data_Cpf extends Mage_Customer_Model_Attribute_Data_Text
{
    /**
     * Validate value
     *
     * @param string $value
     * @return array
     */
    public function validateValue($value)
    {
        $errors = parent::validateValue($value);
        $errors = ($errors === true) ? array() : $errors;

        /** @var Esmart_Customer_Helper_Cpf $helper */
        $helper = Mage::helper('esmart_customer/cpf');

        if ($helper->exists($value)) {
            $errors[] = $helper->__(
                'There is already an account with this cpf. If you are sure that it is your cpf, <a href="%s">click here</a> to get your password and access your account.',
                Mage::getUrl('customer/account/forgotpassword', array('_secure' => true))
            );
        }

        if (!$helper->validate($value)) {
            $errors[] = $helper->__('The CPF informed is invalid.');
        }

        return $errors;
    }
}
