<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Do not edit this file if you want to update this module future new versions.
 *
 * @category      Esmart
 * @package       Esmart_Sample
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class Esmart_Sample_Model_System_Config_Source_Attribute
{
    /**
     * Return options in select format
     *
     * @return array
     */
    public function toOptionArray()
    {
        $data = array();

        foreach ($this->toArray() as $value => $label) {
            $data[] = array(
                'value' => $value,
                'label' => $label
            );
        }

        return $data;
    }

    /**
     * Return options in key-value format
     *
     * @return array
     */
    public function toArray()
    {
        $data = array();

        $attributes = Mage::getSingleton('catalog/product')->getAttributes();

        /** @var Mage_Catalog_Model_Resource_Eav_Attribute $attribute */
        foreach ($attributes as $attribute) {
            if (!$attribute->getIsVisible()) {
                continue;
            }

            $data[$attribute->getAttributeCode()] = $attribute->getFrontendLabel();
        }

        return $data;
    }
}
