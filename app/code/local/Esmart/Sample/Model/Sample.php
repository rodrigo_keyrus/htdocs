<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category      Esmart
 * @package       Esmart_Sample
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class Esmart_Sample_Model_Sample extends Varien_Object
{
    /**
     * Product collection object
     *
     * @var Mage_Catalog_Model_Resource_Product_Collection
     */
    protected $_collection;

    /**
     * Return product collection
     *
     * @return Mage_Catalog_Model_Resource_Product_Collection
     * @throws Mage_Core_Exception
     */
    public function getProductCollection()
    {
        if ($this->_collection) {
            return $this->_collection;
        }

        $referenceValue = $this->helper()->getStoreConfig('reference_value');

        $collection = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
            ->addAttributeToFilter('type_id', Mage_Catalog_Model_Product_Type::TYPE_SIMPLE)
            ->addAttributeToFilter('visibility', Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE)
            ->addAttributeToFilter('is_sample', true)
            ->addAttributeToFilter('tax_treatment', $referenceValue);

        $collection->getSelect()
            ->order('qty DESC')
            ->limit(6);


        /**
         * Add stock filter in collection
         */
        Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($collection);

        /**
         * Add catalog inventory object to collection
         */
        Mage::getModel('cataloginventory/stock')->addItemsToProducts($collection);

        $this->_collection = clone $collection;

        // tratamento de estoque dos produtos
        if (Mage::getStoreConfig('esmart_sample/settings/synchronous_stock')) {
            $collection = $this->filterStock($collection);
        }

        return $collection;
    }

    public function filterStock(Mage_Catalog_Model_Resource_Product_Collection $collection)
    {
        foreach ($collection as $key => $product) {
            $stockItem = $product->getData('stock_item');
            if ($stockItem->getIsInStock() == false || $stockItem->getQty() <= 0) {
                $collection->removeItemByKey($key);
            }
        }

        return $collection;
    }

    /**
     * Return sample helper object
     *
     * @return Esmart_Sample_Helper_Sample
     */
    protected function helper()
    {
        return Mage::helper('esmart_sample/sample');
    }

    /**
     * @param array $productIds
     * @return bool
     */
    public function validate($productIds)
    {
        $ruleQty = $this->helper()->getActiveRuleQty();

        if (count($productIds) > $ruleQty) {
            $this->helper()->getSession()->addError(
                $errors[] = $this->helper()->__('You can select a maximum of %s sample(s).', $ruleQty)
            );

            return false;
        }

        foreach ($productIds as $productId) {
            if (!$this->checkProductCanBeAddedToCart($productId)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param int $productId
     * @return bool
     */
    public function checkProductCanBeAddedToCart($productId)
    {
        try {
            //We just simulate the addition to catch errors like inventory or exceptions
            $this->helper()->getCart()->addProduct($productId);
        } catch (Exception $exception) {
            $this->helper()->getSession()->addError($exception->getMessage());
            return false;
        }

        return true;
    }

    /**
     * @param array $productIds
     */
    public function addSamplesToCheckoutSession($productIds)
    {
        Mage::getSingleton('checkout/session')->setSampleProductIds($productIds);
    }
}
