<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Do not edit this file if you want to update this module future new versions.
 *
 * @category      Esmart
 * @package       Esmart_Sample
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class Esmart_Sample_Block_Adminhtml_Form_Field_Rule extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
{
    /**
     * Prepare to render
     *
     * @return void
     */
    protected function _prepareToRender()
    {
        $this->addColumn(
            'min_order_total',
            array(
                'label' => Mage::helper('customer')->__('Min Order Total'),
                'class' => 'input-text required-entry validate-currency-dollar'
            )
        );

        $this->addColumn(
            'sale_qty',
            array(
                'label' => Mage::helper('cataloginventory')->__('Sale Qty'),
                'class' => 'input-text required-entry validate-greater-than-zero',
                'style' => 'width: 100px'
            )
        );

        $this->_addAfter = false;
    }

    /**
     * Returns the HTML of the field
     *
     * @return string
     *
     * @throws Exception
     */
    protected function _toHtml()
    {
        return '<div id="' . $this->getElement()->getId() . '">' . parent::_toHtml() . '</div>';
    }
}
