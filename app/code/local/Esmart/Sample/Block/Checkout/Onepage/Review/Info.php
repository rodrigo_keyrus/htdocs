<?php
/**
 * BSeller Platform | B2W - Companhia Digital
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category  Esmart
 * @package   Esmart_Sample
 *
 * @copyright Copyright (c) 2017 B2W Digital - BSeller Platform. (http://www.bseller.com.br)
 *
 * @author   Weverson Cachinsky <weverson.cachinsky@e-smart.com.br>
 */
class Esmart_Sample_Block_Checkout_Onepage_Review_Info extends Mage_Checkout_Block_Onepage_Review_Info
{
    public function getItems()
    {
        $quoteItems = parent::getItems();
        $sampleItems = Mage::getSingleton('esmart_sample/sample_collection')->getAllItems();
        $beautyClubItems = Mage::getSingleton('esmart_bc/beautyClub_collection')->getAllItems();
        $giftItems = Mage::getSingleton('esmart_gift/gift_collection')->getFinalItems();

        return array_merge($quoteItems, $sampleItems, $beautyClubItems, $giftItems);
    }
}
