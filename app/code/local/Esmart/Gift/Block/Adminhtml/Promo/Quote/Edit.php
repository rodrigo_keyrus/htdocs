<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Não edite este arquivo caso você pretenda atualizar este módulo futuramente
 * para novas versões.
 *
 * @category
 * @package
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Gabriel da Gama <gabriel.gama@e-smart.com.br>
 */

class Esmart_Gift_Block_Adminhtml_Promo_Quote_Edit extends BSeller_PromoArchive_Block_Adminhtml_Promo_Quote_Edit
{
    /**
     * Class constructor
     */
    public function __construct()
    {
        parent::__construct();
        
        $this->_formScripts[] = " 
			function esmart_gift_hide() {
				$('rule_discount_qty').up().up().show();
				$('rule_discount_step').up().up().show();
				$('rule_apply_to_shipping').up().up().show();
				$('rule_actions_fieldset').up().show();
				$('rule_gift_sku').up().up().hide();
				$('rule_alternative_gifts').up().up().hide();
				$('rule_max_select_gifts').up().up().hide();

				if ('esmart_gift_cart' == $('rule_simple_action').value) {
				    $('rule_actions_fieldset').up().hide(); 
					$('rule_discount_qty').up().up().hide();
					$('rule_discount_step').up().up().hide();
					
					$('rule_apply_to_shipping').up().up().hide();
					$('rule_gift_sku').up().up().show();
					$('rule_alternative_gifts').up().up().show();
				} else if ('esmart_gift_items' == $('rule_simple_action').value){
					$('rule_apply_to_shipping').up().up().hide();
					$('rule_gift_sku').up().up().show();
					$('rule_alternative_gifts').up().up().show();
				} else if ('esmart_select_gifts_rule' == $('rule_simple_action').value){
				    $('rule_actions_fieldset').up().hide(); 
					$('rule_discount_qty').up().up().hide();
					$('rule_discount_step').up().up().hide();
					$('rule_discount_amount').up().up().hide();
					$('rule_apply_to_shipping').up().up().hide();

					$('rule_gift_sku').up().up().show();
					$('rule_alternative_gifts').up().up().show();
					$('rule_max_select_gifts').up().up().show();
				}
			}
			esmart_gift_hide();
        ";
    }
}