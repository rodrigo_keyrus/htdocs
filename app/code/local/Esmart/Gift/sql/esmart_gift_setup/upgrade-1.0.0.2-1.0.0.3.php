<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category      Esmart
 * @package       Esmart_Gift
 *
 * @copyright     Copyright (c) 2018 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Evandro Veloso Gomes <evandro.gomes@e-smart.com.br>
 */

/* @var $installer Mage_Sales_Model_Resource_Setup */
$installer = new Mage_Sales_Model_Resource_Setup('core_setup');

$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('salesrule/rule'),'max_select_gifts', array(
        'type'     => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'visible'  => true,
        'required' => false,
        'comment'  => 'Quantity of gifts to select'
    ));

$installer->endSetup();
