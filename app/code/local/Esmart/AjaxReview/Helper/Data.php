<?php

class Esmart_AjaxReview_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getReviewsCollection($productId, $curPage)
    {
        if (Mage::helper('bseller_review')->aggregateReviews()) {
            $storeId = Mage::helper('bseller_review')->getAggregatedStore();
        } else {
            $storeId = Mage::app()->getStore()->getId();
        }

        $reviewsCollection = Mage::getModel('review/review')->getCollection()
            ->addStoreFilter($storeId)
            ->addStatusFilter(Mage_Review_Model_Review::STATUS_APPROVED)
            ->addEntityFilter('product', $productId)
            ->setDateOrder()
            ->setCurPage($curPage)
            ->setPageSize(3)
            ->addRateVotes();

        return $reviewsCollection;
    }
}