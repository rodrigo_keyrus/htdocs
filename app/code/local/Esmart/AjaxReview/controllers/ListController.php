<?php

class Esmart_AjaxReview_ListController extends Mage_Core_Controller_Front_Action
{
    /**
     *
     */
    public function indexAction()
    {
        try {
            $productId = $this->getRequest()->getParam('product_id');
            $curPage   = $this->getRequest()->getParam('cur_page');
            $data = array();

            if (!$productId || !$curPage) {
                throw new Exception('Error processing request, params empty!');
            }

            $reviewsCollection = Mage::helper('esmart_ajaxreview')
                ->getReviewsCollection($productId,$curPage);

            $pageCount = $reviewsCollection->getLastPageNumber();

            if ($curPage > $pageCount) {
                return;
            }

            $reviewsCollection->getItems();

            $i = 0;
            foreach ($reviewsCollection as $item) {
                $data[$i]['nickname'] = $item->getNickname();
                $data[$i]['created_at'] = Mage::helper('core')->formatDate($item->getCreatedAt());
                $data[$i]['detail'] = $item->getDetail();

                $votes = $item->getRatingVotes();
                foreach ($votes as $vote) {
                    $percent = $vote->getPercent();
                }
                $data[$i]['votes_percent'] = $percent;
                $i++;
            }

            $convertJson = Mage::helper('core')->jsonEncode($data);
            $this->getResponse()->setHeader('Content-type', 'application/json');
            $this->getResponse()->setBody($convertJson);
        } catch (Exception $e) {
            $error = array('error' => $e->getMessage());
            $convertJson = Mage::helper('core')->jsonEncode($error);
            $this->getResponse()->setHeader('Content-type', 'application/json');
            $this->getResponse()->setBody($convertJson);
        }
    }
}
