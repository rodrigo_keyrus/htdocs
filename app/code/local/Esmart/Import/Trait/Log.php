<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Não edite este arquivo caso você pretenda atualizar este módulo futuramente
 * para novas versões.
 *
 * @category      Esmart
 * @package       Esmart_Import
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 * @author        Guilherme Miguelete <guilherme.miguelete@e-smart.com.br>
 */

trait Esmart_Import_Trait_Log
{

    public function log($field, $method, $request, $fullResponse)
    {
        return $this;
        $log = Mage::getModel('esmart_soap/log');
        $log->prepareLogMessage(
            $field,
            $method,
            $request,
            $fullResponse
        );
        $log->save('esmart_import.log');
    }

}
