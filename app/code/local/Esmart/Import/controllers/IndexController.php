<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Não edite este arquivo caso você pretenda atualizar este módulo futuramente
 * para novas versões.
 *
 * @category      Esmart
 * @package       Esmart_Import
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 * @author        Guilherme Miguelete <guilherme.miguelete@e-smart.com.br>
 */
class Esmart_Import_IndexController extends Esmart_Import_Controller_Import
{

    /**
     * @return bool
     */
    private function isEnable()
    {
        return true;
    }

    /**
     *  run import customer direct on browser
     */
    public function importCustomersAction()
    {

        if (!$this->isEnable()) {
            return;
        };

        $this->getImportModel()->importCustomers();

    }

}
