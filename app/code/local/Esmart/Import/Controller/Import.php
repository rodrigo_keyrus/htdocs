<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Não edite este arquivo caso você pretenda atualizar este módulo futuramente
 * para novas versões.
 *
 * @category      Esmart
 * @package       Esmart_Import
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 * @author        Guilherme Miguelete <guilherme.miguelete@e-smart.com.br>
 */
class Esmart_Import_Controller_Import extends Mage_Core_Controller_Front_Action
{

    /**
     * return base url to store
     * @return string
     */
    public function getBaseUrl()
    {
        return Mage::getBaseUrl();
    }

    /**
     * return helper data
     * @return Esmart_Import_Helper_Data
     */
    public function getHelper()
    {
        return Mage::helper('esmart_import');
    }

    /**
     * return import model
     * @return Esmart_Import_Model_Import
     */
    public function getImportModel()
    {
        return Mage::getModel('esmart_import/import');
    }

}
