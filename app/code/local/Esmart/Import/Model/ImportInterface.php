<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Não edite este arquivo caso você pretenda atualizar este módulo futuramente
 * para novas versões.
 *
 * @category      Esmart
 * @package       Esmart_Integration
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 * @author        Guilherme Miguelete <guilherme.miguelete@e-smart.com.br>
 */

interface Esmart_Import_Model_ImportInterface
{
    /**
     * Indicates that an integrator must import some data.
     *
     * @param array $csvData
     */
    public function import($csvData);

    /**
     * Validates CSV data import.
     *
     * @return boolean
     */
    public function validate();
}
