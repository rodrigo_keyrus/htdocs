<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Não edite este arquivo caso você pretenda atualizar este módulo futuramente
 * para novas versões.
 *
 * @category      Esmart
 * @package       Esmart_Import
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 * @author        Guilherme Miguelete <guilherme.miguelete@e-smart.com.br>
 */
class Esmart_Import_Model_Import_Customer extends Mage_Core_Model_Abstract
    implements Esmart_Import_Model_ImportInterface
{
    use Esmart_Import_Trait_Log;
    
    CONST STORE_ID = 1;

    private $attributes;


    /**
     * @return mixed
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    
    /**
     * Import CSV data to Magento
     *
     * @param $csvData
     */
    public function import($csvData)
    {
        // Remove first line (header)
        //array_shift($csvData);
        foreach ($csvData as $data) {

            $this->setAttributes($data);
            if (!$this->validate()) {
                continue;
            }

            try {

                $customer = $this->checkExistsByCpf($this->attributes['cpf']);
                if (!$customer) {
                    $customer = $this->create($data);
                }

                //Mage::getModel('esmart_import/import_customer_address')->create($customer, $data);
                //$this->log('customer', 'import', json_encode($this->attributes), 'Success imported customer');
            } catch (Exception $e) {
                $this->log('customer', 'import', json_encode($this->attributes), $e->getMessage());
                Mage::logException($e);
            }

        }
    }


    /**
     * @param $data
     * @return Esmart_BeautyClub_Model_Customer | Mage_Customer_Model_Customer
     * @throws Exception
     * @throws Mage_Core_Exception
     */
    private function create($data)
    {

        $storeId = self::STORE_ID;
        $store = Mage::getModel('core/store')->load($storeId); //

        $customer = Mage::getModel('customer/customer');
        $customer->setData($this->attributes);
        $customer->setWebsiteId($storeId)->setStore($store);

        $address   = Mage::getModel('customer/address');
        $address->addData($this->getDataCustomerAddress($data));
        $address->setSaveInAddressBook('1');

        $customer->addAddress($address);
        $customer->save();

        return $customer;

    }


    /**
     * @param $data
     * @return array
     */
    private function getDataCustomerAddress($data)
    {

        // set billing address
        $data = [
            'firstname'  => trim(@$data[6]),
            'lastname'   => trim(@$data[9]),
            'street'     => [
                '0' => trim(@$data[23]),
                '1' => trim(@$data[24]),
                '2' => trim(@$data[25]),
                '3' => trim(@$data[26]),
            ],
            'city'       => trim(@$data[16]),
            'region'     => trim(@$data[22]),
            'region_id'  => $this->getRegionId(trim(@$data[22])),
            'postcode'   => (string) trim(@$data[21]),
            'country_id' => 'BR',
            'telephone'  => trim(@$data[27]),
            'cellphone'  => trim(@$data[28]),
            'identifier' => trim(@$data[18]),
            'is_default_billing' => true,
            'is_default_shipping' => true
        ];

        //fix CEP
        if (strlen($data['postcode']) < 8) {
            $data['postcode'] = '0'.$data['postcode'];
        }

        return $data;

    }


    /**
     * @param $regionCode
     * @return mixed
     */
    private function getRegionId($regionCode)
    {
        $regionModel = Mage::getModel('directory/region')->loadByCode($regionCode, 'BR');
        return $regionModel->getId();
    }


    /**
     * @return Mage_Customer_Model_Resource_Customer_Collection
     */
    private function getCustomerResourceModel()
    {
        return Mage::getResourceModel('customer/customer_collection');
    }


    /**
     * @param $cpf
     * @return boolean
     */
    private function checkExistsByCpf($cpf)
    {
        $customer = $this->getCustomerResourceModel()->addFieldToFilter('cpf', $cpf);

        if ($customer->count() >= 1) {
            return $customer->getFirstItem();
        }

        return false;
    }


    /**
     * @param $email
     * @return boolean
     */
    private function checkExistsByEmail($email)
    {
        $customer = $this->getCustomerResourceModel()->addFieldToFilter('email', $email);

        if ($customer->count() >= 1) {
            return true;
        }

        return false;
    }


    /**
     * @param $email
     * @return mixed|null
     */
    private function loadByEmail($email)
    {
        $customer = $this->getCustomerResourceModel()->addFieldToFilter('email', $email);

        if (count($customer) >= 1) {
            return $customer->getFirstItem();
        }

        return false;
    }


    /**
     * @param $customer
     * @return array
     */
    public function setAttributes($customer)
    {
        $data = [
                'created_at' => trim(@$customer[4]),
                'legacy_user_id' => trim(@$customer[0]),
                'email' => trim(@$customer[1]),
                'cpf' => trim(@$customer[13]),
                'password' => trim(@$customer[15]),
                'firstname' => trim(@$customer[6]),
                'lastname' => trim(@$customer[9]),
                'gender' => $this->getGender(trim(@$customer[7])),
                'telephone' => trim(@$customer[27]),
                'cellphone' => trim(@$customer[28]),
                'legacy_business_phone' => trim(@$customer[28]),
                'is_bc' => trim(@$customer[31]),
                'is_integrated' => trim(@$customer[32]),
                'ax_updated' => (trim(@$customer[33]) == 1) ? 0 : 1
            ];

        //fix cpf
        if (strlen($data['cpf']) == 10) {
            $data['cpf'] = '0'.$data['cpf'];
        }

        if ($customer[5] != 'NULL') {
            $data['dob'] = trim(@$customer[5]);
        }

        $this->attributes = $data;

    }


    private function getGender($gender)
    {

        if ($gender == 'Masculino') {
            return 1;
        }

        if ($gender == 'Feminino') {
            return 2;
        }

        return null;

    }

    

    /**
     * Validates CSV data import.
     *
     * @return boolean
     */
    public function validate()
    {

        if ($this->attributes['firstname'] == '') {
            $this->log('customer', 'import', json_encode($this->attributes), 'firstname is required');
            return false;
        }

        if ($this->attributes['lastname'] == '') {
            $this->log('customer', 'import', json_encode($this->attributes), 'lastname is required');
            return false;
        }

        if ($this->attributes['cpf'] == '') {
            $this->log('customer', 'import', json_encode($this->attributes), 'cpf is required');
            return false;
        }

        return true;
    }

}
