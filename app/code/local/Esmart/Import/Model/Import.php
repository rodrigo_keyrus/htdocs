<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Não edite este arquivo caso você pretenda atualizar este módulo futuramente
 * para novas versões.
 *
 * @category      Esmart
 * @package       Esmart_Import
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 * @author        Guilherme Miguelete <guilherme.miguelete@e-smart.com.br>
 */
class Esmart_Import_Model_Import extends Mage_Core_Model_Abstract
{

    /**
     * import customers csv to magento database
     * @param $file
     * @throws Exception
     */
    public function importCustomers($file)
    {
        //$file = getcwd().'/var/import/' . Mage::getStoreConfig('esmart_configuration/general/customers_file');
        $file = getcwd().'/'.$file;
        $fileData = $this->getCsvObject()->getData($file);
        Mage::getModel('esmart_import/import_customer')->import($fileData);
    }


    /**
     * get Csv parse object
     */
    private function getCsvObject()
    {
        return new Varien_File_Csv();
    }

}
