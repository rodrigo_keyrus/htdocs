<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category      Esmart
 * @package       Esmart_Manufacturer
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class Esmart_Manufacturer_Model_Source_Status
{
    use BSeller_Core_Trait_Data;

    /**
     * Options in array
     *
     * @var array
     */
    protected $_options = [
        'Disabled',
        'Enabled'
    ];

    /**
     * Return options in select format
     *
     * @return array
     */
    public function toOptionArray()
    {
        $data[] = [];

        foreach ($this->toArray() as $value => $label) {
            $data[] = [
                'value' => $value,
                'label' => $label
            ];
        }

        return $data;
    }

    /**
     * Return options in key-value format
     *
     * @return array
     */
    public function toArray()
    {
        $data = [];

        foreach ($this->_options as $value => $label) {
            $data[$value] = $this->_helper()->__($label);
        }

        return $data;
    }
}
