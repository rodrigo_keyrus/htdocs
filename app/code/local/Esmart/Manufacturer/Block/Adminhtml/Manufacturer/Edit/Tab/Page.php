<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category      Esmart
 * @package       Esmart_Manufacturer
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class Esmart_Manufacturer_Block_Adminhtml_Manufacturer_Edit_Tab_Page extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Constructor initialization
     */
    public function __construct()
    {
        parent::__construct();

        $this->setId('manufacturer_page_grid');
        $this->setDefaultSort('entity_id');
        $this->setData('use_ajax', true);
    }

    /**
     * Return main buttons html
     *
     * @return string
     */
    public function getMainButtonsHtml()
    {
        /** @var Mage_Core_Block_Abstract $block */
        $block = $this->getLayout()
            ->createBlock('adminhtml/widget_button')
            ->setData(
                [
                    'label'   => $this->__('Add'),
                    'onclick' => 'setLocation(\'' . $this->getAddButtonUrl() . '\')',
                    'class'   => 'add'
                ]
            );

        $html  = parent::getMainButtonsHtml();
        $html .= $block->toHtml();

        return $html;
    }

    /**
     * Return add button url
     *
     * @return string
     */
    protected function getAddButtonUrl()
    {
        return $this->getUrl('*/*/pageNew', ['parent_id' => $this->getModelInstance()->getId()]);
    }

    /**
     * Return manufacturer object
     *
     * @return Esmart_Manufacturer_Model_Manufacturer
     */
    protected function getModelInstance()
    {
        return Mage::registry('current_manufacturer');
    }

    /**
     * Prepare grid collection
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        /** @var Esmart_Manufacturer_Model_Resource_Page_Collection $collection */
        $collection = Mage::getResourceModel('esmart_manufacturer/page_collection')
            ->addParentFilter($this->getModelInstance()->getId());

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * Add grid columns
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'entity_id',
            [
                'header' => $this->__('ID'),
                'index'  => 'entity_id'
            ]
        );

        $this->addColumn(
            'name',
            [
                'header' => $this->__('Name'),
                'index'  => 'name'
            ]
        );

        $this->addColumn(
            'status',
            [
                'header'  => $this->__('Status'),
                'index'   => 'status',
                'type'    => 'options',
                'options' => Mage::getSingleton('esmart_manufacturer/source_status')->toArray()
            ]
        );

        $this->addColumn(
            'created_at',
            [
                'header'   => $this->__('Created At'),
                'index'    => 'created_at',
                'type'     => 'datetime',
                'renderer' => 'Esmart_Manufacturer_Block_Adminhtml_Widget_Grid_Column_Renderer_Datetime'
            ]
        );

        $this->addColumn(
            'updated_at',
            [
                'header'   => $this->__('Updated At'),
                'index'    => 'updated_at',
                'type'     => 'datetime',
                'renderer' => 'Esmart_Manufacturer_Block_Adminhtml_Widget_Grid_Column_Renderer_Datetime'
            ]
        );

        $this->addColumn(
            'position',
            [
                'header' => $this->__('Position'),
                'index'  => 'position',
                'type'   => 'number'
            ]
        );

        $this->addColumn(
            'action',
            [
                'header'  => $this->__('Action'),
                'width'   => 50,
                'type'    => 'action',
                'getter'  => 'getId',
                'actions' => [
                    [
                        'caption'     => $this->__('Edit'),
                        'url'         => [
                            'base'   => '*/*/pageEdit/',
                            'params' => ['parent_id' => $this->getModelInstance()->getId()]
                        ],
                        'field'       => 'id',
                        'data-column' => 'action'
                    ]
                ],
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'edit',
                'is_system' => true
            ]
        );

        return parent::_prepareColumns();
    }
}
