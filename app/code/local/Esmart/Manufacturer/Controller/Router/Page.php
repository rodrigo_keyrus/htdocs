<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category      Esmart
 * @package       Esmart_Manufacturer
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class Esmart_Manufacturer_Controller_Router_Page extends Mage_Core_Controller_Varien_Router_Standard
{
    /**
     * Match the request
     *
     * @param Zend_Controller_Request_Http $request
     * @return bool
     */
    public function match(Zend_Controller_Request_Http $request)
    {
        $path   = trim($request->getPathInfo(), '/');
        $params = explode('/', $path);

        if (count($params) !== 2) {
            return false;
        }

        list($manufacturerIdentifier, $pageIdentifier) = $params;

        $storeId = Mage::app()->getStore()->getId();

        /** @var Esmart_Manufacturer_Model_Manufacturer $manufacturer */
        $manufacturer   = Mage::getModel('esmart_manufacturer/manufacturer');
        $manufacturerId = $manufacturer->checkIdentifier($manufacturerIdentifier, $storeId);

        if (!$manufacturerId) {
            return false;
        }

        /** @var Esmart_Manufacturer_Model_Page $manufacturerPage */
        $manufacturerPage = Mage::getResourceModel('esmart_manufacturer/page_collection')
            ->addParentFilter($manufacturerId)
            ->addIdentifierFilter($pageIdentifier)
            ->addStatusFilter()
            ->setPageSize(1)
            ->getFirstItem();

        if (!$manufacturerPage->getId()) {
            return false;
        }

        Mage::register('current_manufacturer_page', $manufacturerPage, true);

        $request->setModuleName('esmart_manufacturer')
            ->setControllerName('page')
            ->setActionName('view')
            ->setParam('id', $manufacturerPage->getId())
            ->setParam('manufacturer_id', $manufacturerId)
            ->setParams($request->getParams());

        $request->setAlias(
            Mage_Core_Model_Url_Rewrite::REWRITE_REQUEST_PATH_ALIAS,
            $path
        );

        return true;
    }
}
