<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  Esmart
 * @package   Esmart_Flag
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Rafael Falcao <rafael.falcao@e-smart.com.br>
 *
 */

class Esmart_Flag_Model_Cron extends Mage_Core_Model_Abstract
{
    /**
     * Helper module Esmart_Flag
     *
     * @var Esmart_Flag_Helper_Data
     */
    private $_helper;

    /**
     * Attributes to select collection products
     *
     * @var array
     */
    private $_productsAttributes = array('type_id', 'selo_novidade', 'selo_novidade_inicio');

    /**
     * Manage date attribute 'selo_novidade_inicio' and 'selo_novidade'
     * to show news flag
     */
    public function manageNewsFlag()
    {
        /** @var Esmart_Flag_Helper_Data _helper */
        $this->_helper = Mage::helper('esmart_flag');

        /** Check if module is enabled */
        if(!$this->_helper->isModuleEnabled()){
            return;
        }

        /** Use this for collections dont use flat tables */
        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

        $this->setFromDateFlag();
        $this->setDisableNewsFlag();
    }

    /**
     * Set from date to news flag
     */
    public function setFromDateFlag()
    {
        /** @var Mage_Catalog_Model_Resource_Product $productCollection */
        $productCollection = $this->getFlagCollection()
            ->addAttributeToFilter('selo_novidade_inicio', array('null' => true), 'left');

        /** @var array $allPro‌​ductIds */
        $allPro‌​ductIds = $productCollection->getColumnValues('entity_id');

        /** @var array $attrData */
        $attrData = array( 'selo_novidade_inicio' => $this->_helper->getCurrentDate() );

        /** Check if product array is empty */
        if(!count($allPro‌​ductIds)){
            return;
        }

        /** Save attribute 'selo_novidade_inicio' to all products */
        Mage::getModel('catalog/product_action')
                ->updateAttributes($allPro‌​ductIds, $attrData, $this->_helper->getStore());
    }

    /**
     * Set to disable news flag to old products
     */
    public function setDisableNewsFlag()
    {
        /** @var Mage_Catalog_Model_Resource_Product $productCollection */
        $productCollection = $this->getFlagCollection()
            ->addAttributeToFilter('selo_novidade_inicio', array('lt' => $this->_helper->getFlagEndDate()));

        /** @var array $allPro‌​ductIds */
        $allPro‌​ductIds = $productCollection->getColumnValues('entity_id');

        /** @var array $attrData */
        $attrData = array( 'selo_novidade' => 0 );

        /** Check if product array is empty */
        if(!count($allPro‌​ductIds)){
            return;
        }

        /** Save attribute 'selo_novidade' to all products */
        Mage::getModel('catalog/product_action')
            ->updateAttributes($allPro‌​ductIds, $attrData, $this->_helper->getStore());
    }

    /**
     * Collection that will be manipulated for new flags
     *
     * @return Mage_Catalog_Model_Resource_Product
     */
    public function getFlagCollection()
    {
        $flagCollection = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect($this->_productsAttributes)
            ->addAttributeToFilter('type_id', array('eq', 'grouped'))
            ->addAttributeToFilter('selo_novidade', 1);

        return $flagCollection;
    }
}