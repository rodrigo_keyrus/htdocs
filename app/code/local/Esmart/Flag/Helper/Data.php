<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category      Esmart
 * @package       Esmart_Flag
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class Esmart_Flag_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Setting that checks if module is enabled
     *
     * @return boolean
     */
    public function isModuleEnabled()
    {
        $enabled = $this->getConfig('bseller_flag/settings/active');

        return $enabled;
    }

    /**
     * Setting number of days the new flag will be active
     *
     * @return int
     */
    public function getFlagNewDays()
    {
        $days = $this->getConfig('bseller_flag/settings/flag_new_days');

        return $days;
    }

    /**
     * Module settings
     *
     * @param $path
     * @return mixed
     */
    public function getConfig($path)
    {
        $configValue = Mage::getStoreConfig($path, $this->getStore());

        return $configValue;
    }

    /**
     * Store
     *
     * @return Mage_Core_Model_Store
     * @throws Mage_Core_Model_Store_Exception
     */
    public function getStore()
    {
        $store = Mage_Core_Model_App::ADMIN_STORE_ID;

        return $store;
    }

    /**
     * Locale Date
     *
     * @return Zend_Date
     */
    public function getLocaleDate()
    {
        $date = Mage::app()->getLocale()->date();

        return $date;
    }

    /**
     * Date that will be used for filtering old flags
     *
     * @return string|null
     */
    public function getFlagEndDate()
    {
        if(!$this->getFlagNewDays()){
            return null;
        }

        $date = date('Y-m-d',
            strtotime('-'.$this->getFlagNewDays().' days', $this->getLocaleDate()->getTimestamp())
        );

        return $date;
    }

    /**
     * Current Date
     *
     * @return string
     */
    public function getCurrentDate()
    {
        $date = $this->getLocaleDate()->getTimestamp();

        return date('Y-m-d', $date);
    }
}
