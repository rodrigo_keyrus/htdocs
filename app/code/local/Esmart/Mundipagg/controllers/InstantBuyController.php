<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category      Esmart
 * @package       Esmart_Mundipagg
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class Esmart_Mundipagg_InstantBuyController extends Mage_Core_Controller_Front_Action
{
    /**
     * Action predispatch
     *
     * Check customer authentication for some actions
     */
    public function preDispatch()
    {
        parent::preDispatch();

        if (!Mage::getSingleton('customer/session')->authenticate($this)) {
            $this->setFlag('', 'no-dispatch', true);
        }
    }

    /**
     * Dashboard page
     *
     * @return $this
     */
    public function indexAction()
    {
        $collection = Mage::getModel('mundipagg/cardonfile')
            ->getCollection()
            ->addEntityIdFilter(Mage::helper('customer')->getCustomer()->getId());

        if (!$collection->getSize()) {
            $this->_redirect('*/*/new');
        }

        $this->loadLayout();
        $this->renderLayout();

        return $this;
    }

    /**
     * Form new page
     *
     * @return $this
     */
    public function newAction()
    {
        $this->loadLayout();
        $this->renderLayout();

        return $this;
    }

    /**
     * Form edit page
     *
     * @return $this
     */
    public function editAction()
    {
        if (!$id = $this->getRequest()->getParam('id')) {
            $this->_redirect('*/*/new');

            return $this;
        }

        /** @var Uecommerce_Mundipagg_Model_Cardonfile $cardOnFile */
        $cardOnFile = Mage::getModel('mundipagg/cardonfile')->load($id);

        if (!$cardOnFile->getId()) {
            $this->_redirect('*/*/new');

            return $this;
        }

        Mage::register('current_card', $cardOnFile);

        $this->loadLayout();
        $this->renderLayout();

        return $this;
    }

    /**
     * Save form data
     *
     * @return $this
     */
    public function saveAction()
    {
        if (!$this->_validateFormKey()) {
            $this->_redirect('*/*');

            return $this;
        }

        $post = $this->getRequest()->getPost();

        /** @var Mage_Customer_Model_Customer $customer */
        $customer = Mage::helper('customer')->getCustomer();

        /** @var Uecommerce_Mundipagg_Model_Cardonfile $cardOnFile */
        $cardOnFile = Mage::getModel('mundipagg/cardonfile');

        /** @var Esmart_Mundipagg_Model_Api_CreateInstantBuy $createInstantBuy */
        $createInstantBuy = Mage::getModel('esmart_mundipagg/api_createInstantBuy');

        /** @var Esmart_Mundipagg_Model_Api_DeleteInstantBuy $deleteInstantBuy */
        $deleteInstantBuy = Mage::getModel('esmart_mundipagg/api_deleteInstantBuy');

        /** @var Esmart_Mundipagg_Model_InstantBuy $instantBuy */
        $instantBuy = Mage::getModel('esmart_mundipagg/instantBuy');

        try {
            /**
             * Delete credit card step
             */
            if (isset($post['id'])) {
                $cardOnFile->load($post['id']);

                $deleteInstantBuy->setData('instant_buy_key', $cardOnFile->getData('token'));
                $deleteInstantBuy->send();

                $cardOnFile->delete();
            }

            /**
             * Create credit card step
             */
            $createInstantBuy->addData($post);

            $response = $createInstantBuy->send();

            $post['token'] = $response['data']['InstantBuyKey'];

            $instantBuy->prepareData($post, $customer);
            $instantBuy->save();

            Mage::getSingleton('core/session')->addSuccess($this->__('Card successfully saved.'));

            $this->_redirect('*/*');
        } catch (Exception $e) {
            Mage::getSingleton('core/session')->addError($e->getMessage());

            isset($post['id'])
                ? $this->_redirect('*/*/edit', array('id' => $post['id']))
                : $this->_redirect('*/*/new');

            $this->_redirect('*/*/new');
        }

        return $this;
    }

    /**
     * Delete card
     *
     * @return $this
     */
    public function deleteAction()
    {
        if (!$id = $this->getRequest()->getParam('id')) {
            $this->_redirect('*/*');

            return $this;
        }

        /** @var Uecommerce_Mundipagg_Model_Cardonfile $cardOnFile */
        $cardOnFile = Mage::getModel('mundipagg/cardonfile')->load($id);

        if (!$cardOnFile->getId()) {
            $this->_redirect('*/*');

            return $this;
        }

        /** @var Esmart_Mundipagg_Model_Api_DeleteInstantBuy $deleteInstantBuy */
        $deleteInstantBuy = Mage::getModel('esmart_mundipagg/api_deleteInstantBuy');

        try {
            $deleteInstantBuy->setData('instant_buy_key', $cardOnFile->getData('token'));
            $deleteInstantBuy->send();

            $cardOnFile->delete();

            Mage::getSingleton('core/session')->addSuccess($this->__('Card successfully deleted.'));

            $this->_redirect('*/*');
        } catch (Exception $e) {
            Mage::getSingleton('core/session')->addError($e->getMessage());

            $this->_redirect('*/*');
        }

        return $this;
    }
}
