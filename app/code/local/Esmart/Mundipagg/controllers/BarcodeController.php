<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category      Esmart
 * @package       Esmart_Mundipagg
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class Esmart_Mundipagg_BarcodeController extends Mage_Core_Controller_Front_Action
{
    /**
     * Barcode type method
     *
     * @var string
     */
    protected $_barcodeType = 'code25interleaved';

    /**
     * Barcode render type
     *
     * @var string
     */
    protected $_renderType = 'image';

    /**
     * Barcode image type
     *
     * @var string
     */
    protected $_imageType = 'png';

    /**
     * Render barcode image
     *
     * @return void
     */
    public function imageAction()
    {
        $code = $this->getRequest()->getParam('code');

        Zend_Barcode::render(
            $this->_barcodeType,
            $this->_renderType,
            array('text' => $code),
            array('imageType' => $this->_imageType)
        );
    }
}
