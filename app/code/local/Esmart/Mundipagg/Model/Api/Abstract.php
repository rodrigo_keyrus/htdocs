<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category      Esmart
 * @package       Esmart_Mundipagg
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

require_once Mage::getBaseDir('lib') . DS . 'Mundipagg/init.php';

abstract class Esmart_Mundipagg_Model_Api_Abstract extends Varien_Object
{
    /**
     * Integration url sandbox
     *
     * @var string
     */
    const URL_SANDBOX = 'https://sandbox.mundipaggone.com';

    /**
     * Integration url production
     *
     * @var string
     */
    const URL_PRODUCTION = 'https://transactionv2.mundipaggone.com';

    /**
     * Request base path
     *
     * @var string
     */
    protected $_requestBasePath = '\\Gateway\\One\\DataContract\\Request\\';

    /**
     * Request class name
     *
     * @var string
     */
    protected $_requestClassName;

    /**
     * Request method name
     *
     * @var string
     */
    protected $_requestMethodName;

    /**
     * Api client object
     *
     * @var \Gateway\ApiClient
     */
    protected $_client;

    /**
     * Common base object
     *
     * @var \Gateway\One\DataContract\Common\BaseObject
     */
    protected $_request;

    /**
     * Response base object
     *
     * @var \Gateway\One\DataContract\Response\BaseResponse
     */
    protected $_response;

    /**
     * Send request data
     *
     * @return array
     * @throws Mage_Core_Exception
     */
    public function send()
    {
        $this->init();
        $this->prepareData();

        try {
            $this->_response = $this->sendRequest();
        } catch (\Gateway\One\DataContract\Report\ApiError $e) {
            Mage::throwException($this->getErrorMessage($e));
        } catch (Exception $e) {
            Mage::logException($e);
            Mage::throwException($e->getMessage());
        }

        return $this->responseTreatment();
    }

    /**
     * Initialize default configs
     *
     * @return $this
     */
    protected function init()
    {
        \Gateway\ApiClient::setBaseUrl($this->getEndpointUrl());
        \Gateway\ApiClient::setMerchantKey($this->getMerchantKey());

        if (!is_null($this->_requestClassName)) {
            $class = $this->_requestBasePath . $this->_requestClassName;

            $this->_request = new $class;
        }

        $this->_client = new \Gateway\ApiClient();

        return $this;
    }

    /**
     * Send request data
     *
     * @return \Gateway\One\DataContract\Response\BaseResponse
     */
    protected function sendRequest()
    {
        return $this->_client->{$this->_requestMethodName}($this->_request);
    }

    /**
     * Return integration environment
     *
     * @return string
     */
    protected function getEnvironment()
    {
        return $this->getConfigData('environment');
    }

    /**
     * Return merchant key
     *
     * @return string
     */
    protected function getMerchantKey()
    {
        if ($this->getEnvironment() === 'development') {
            return $this->getConfigData('merchantKeyStaging');
        }

        return $this->getConfigData('merchantKeyProduction');
    }

    /**
     * Return endpoint url
     *
     * @return string
     */
    protected function getEndpointUrl()
    {
        if ($this->getEnvironment() === 'development') {
            return self::URL_SANDBOX;
        }

        return self::URL_PRODUCTION;
    }

    /**
     * Return error message from api
     *
     * @param \Gateway\One\DataContract\Report\ApiError $apiError
     * @return string
     */
    protected function getErrorMessage(\Gateway\One\DataContract\Report\ApiError $apiError)
    {
        /** @var stdClass $errorCollection */
        $errorCollection = $apiError->errorCollection;

        $messageCollection = array();

        foreach ($errorCollection->ErrorItemCollection as $errorItem) {
            $messageCollection[] = $errorItem->ErrorCode . ' - ' . $errorItem->Description;
        }

        if (!count($messageCollection)) {
            $messageCollection[] = $this->helper()->__('An unexpected error occurred.');
        }

        $message = implode(nl2br(PHP_EOL), $messageCollection);

        return $message;
    }

    /**
     * Return store config data
     *
     * @param string $key
     * @return string
     */
    protected function getConfigData($key)
    {
        return Mage::getStoreConfig('payment/mundipagg_standard/' . $key);
    }

    /**
     * Return data helper object
     *
     * @return Esmart_Mundipagg_Helper_Data
     */
    protected function helper()
    {
        return Mage::helper('esmart_mundipagg');
    }

    /**
     * Prepare request data
     *
     * @return $this
     */
    abstract function prepareData();

    /**
     * Return response after treatment
     *
     * @return $this
     */
    abstract function responseTreatment();
}
