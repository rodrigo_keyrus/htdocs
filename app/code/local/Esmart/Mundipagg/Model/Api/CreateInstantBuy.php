<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category      Esmart
 * @package       Esmart_Mundipagg
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class Esmart_Mundipagg_Model_Api_CreateInstantBuy extends Esmart_Mundipagg_Model_Api_Abstract
{
    /**
     * Request class name
     *
     * @var string
     */
    protected $_requestClassName = 'CreateInstantBuyDataRequest';

    /**
     * Request method name
     *
     * @var string
     */
    protected $_requestMethodName = 'createCreditCard';

    /**
     * Request create instant buy object
     *
     * @var \Gateway\One\DataContract\Request\CreateInstantBuyDataRequest
     */
    protected $_request;

    /**
     * Is one dollar auth enabled
     *
     * @var bool
     */
    protected $_isOneDollarAuthEnabled = false;

    /**
     * Prepare request data
     *
     * @return $this
     */
    public function prepareData()
    {
        $this->_request->setCreditCardBrand($this->helper()->getCcTypeLabel($this->getData('cc_type')));
        $this->_request->setCreditCardNumber($this->getData('cc_number'));
        $this->_request->setExpMonth($this->getData('cc_exp_month'));
        $this->_request->setExpYear($this->getData('cc_exp_year'));
        $this->_request->setHolderName($this->getData('cc_owner'));
        $this->_request->setSecurityCode($this->getData('cc_cvv'));
        $this->_request->setIsOneDollarAuthEnabled($this->_isOneDollarAuthEnabled);

        return $this;
    }

    /**
     * Return response after treatment
     *
     * @return array
     */
    public function responseTreatment()
    {
        $responseData['success'] = $this->_response->isSuccess();

        if ($responseData['success']) {
            $responseData['data'] = get_object_vars($this->_response->getData());
        }

        return $responseData;
    }
}
