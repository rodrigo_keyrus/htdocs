<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category      Esmart
 * @package       Esmart_Mundipagg
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class Esmart_Mundipagg_Block_Customer_Card_Dashboard extends Mage_Customer_Block_Account_Dashboard
{
    /**
     * Initialize constructor
     * Set template
     */
    public function _construct()
    {
        parent::_construct();

        $this->setTemplate('esmart/mundipagg/customer/card/dashboard.phtml');
    }

    /**
     * Return card collection
     *
     * @return Uecommerce_Mundipagg_Model_Resource_Cardonfile_Collection
     */
    public function getCardCollection()
    {
        if ($this->hasData('card_collection')) {
            return $this->getData('card_collection');
        }

        /** @var Mage_Customer_Model_Customer $customer */
        $customer = $this->getCustomer();

        $collection = Mage::getModel('mundipagg/cardonfile')
            ->getCollection()
            ->addEntityIdFilter($customer->getId());

        $this->setData('card_collection', clone $collection);

        return $this->getData('card_collection');
    }

    /**
     * Return payment cc type code
     *
     * @param string $label
     * @return string
     */
    public function getCcTypeCode($label)
    {
        return $this->helper('esmart_mundipagg')->getCcTypeCode($label);
    }

    /**
     * Return cc type label
     *
     * @param string $code
     * @return null
     */
    public function getCcTypeLabel($code)
    {
        return $this->helper('esmart_mundipagg')->getCcTypeLabel($code);
    }
}
