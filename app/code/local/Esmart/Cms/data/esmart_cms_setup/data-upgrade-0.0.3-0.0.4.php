<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Não edite este arquivo caso você pretenda atualizar este módulo futuramente
 * para novas versões.
 *
 * @category      Esmart
 * @package       Esmart_Core
 *
 * @copyright     Copyright (c) 2015 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        e-smart Core Team <core@e-smart.com.br>
 */

$storeName = 'Sephora | ';
$storeId   = Mage::app()->getStore()->getStoreId();

$cmsBlocks = array(

    array(
        'title'      => $storeName . 'Neemu Carrinho',
        'identifier' => 'neemu-cart',
        'content'    => 'SCRIPT NEEMU',
        'is_active'   => 1,
        'stores'      => $storeId
    )

);

/**
 * Insert default blocks
 */

foreach ($cmsBlocks as $data) {
    Mage::getModel('cms/block')->setData($data)->save();
}
