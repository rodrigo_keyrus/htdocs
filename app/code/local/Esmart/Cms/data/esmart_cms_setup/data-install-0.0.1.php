<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Não edite este arquivo caso você pretenda atualizar este módulo futuramente
 * para novas versões.
 *
 * @category      Esmart
 * @package       Esmart_Core
 *
 * @copyright     Copyright (c) 2015 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        e-smart Core Team <core@e-smart.com.br>
 */

$storeName = 'Sephora | ';
$storeId   = Mage::app()->getStore()->getStoreId();

$cmsBlocks = array(

    array(
        'title'      => $storeName . 'Footer - Destaques',
        'identifier' => 'footer-links_destaques',
        'content'    => '
        <dl>
            <dt>Destaques</dt>
            <dd><a href="#">Buscas Populares</a></dd>
            <dd><a href="#">Mais Vendidos</a></dd>
            <dd><a href="#">Novidades</a></dd>
            <dd><a href="#">Ofertas</a></dd>
            <dd><a href="#">Sephora Collection</a></dd>
        </dl>',
        'is_active'  => 1,
        'stores'      => $storeId
    ),

    array(
        'title'      => $storeName . 'Footer - Serviços',
        'identifier' => 'footer-links_servicos',
        'content'    => '
        <dl>
            <dt>Serviços</dt>
            <dd><a href="#">Atendimento</a></dd>
            <dd><a href="#">Meus Pedidos</a></dd>
            <dd><a href="#">Minha Conta</a></dd>
            <dd><a href="#">Trabalhe Conosco</a></dd>
        </dl>',
        'is_active'   => 1,
        'stores'      => $storeId
    ),

    array(
        'title'      => $storeName . 'Footer - Institucional',
        'identifier' => 'footer-links_institucional',
        'content'    => '
        <dl>
            <dt>Institucional</dt>
            <dd><a href="#">Sobre a Sephora</a></dd>
            <dd><a href="#">Segurança</a></dd>
            <dd><a href="#">Politíca de Privacidade</a></dd>
            <dd><a href="#">Sephora Internacional</a></dd>
        </dl>',
        'is_active'   => 1,
        'stores'      => $storeId
    ),

    array(
        'title'      => $storeName . 'Footer - Beauty Club',
        'identifier' => 'footer-links_beautyclub',
        'content'    => '
        <dl>
            <dt>Beauty Club</dt>
            <dd><a href="#">Regulamento</a></dd>
            <dd><a href="#">Vitrine de Prêmios</a></dd>
            <dd><a href="#">Meus Pontos e Status</a></dd>
        </dl>',
        'is_active'   => 1,
        'stores'      => $storeId
    ),

    array(
        'title'      => $storeName . 'Footer - Meu Beauty Studio',
        'identifier' => 'footer-links_beautystudio',
        'content'    => '
        <dl>
            <dt>Meu Beauty Studio</dt>
        </dl>',
        'is_active'   => 1,
        'stores'      => $storeId
    ),

    array(
        'title'      => $storeName . 'Footer - Beauty Board',
        'identifier' => 'footer-links_beautyboard',
        'content'    => '
        <dl>
            <dt>Beauty Board</dt>
        </dl>',
        'is_active'   => 1,
        'stores'      => $storeId
    ),

    array(
        'title'      => $storeName . 'Footer - Tutoriais',
        'identifier' => 'footer-links_tutoriais',
        'content'    => '
        <dl>
            <dt>Tutoriais</dt>
        </dl>',
        'is_active'   => 1,
        'stores'      => $storeId
    ),

    array(
        'title'      => $storeName . 'Footer - Social',
        'identifier' => 'footer-social',
        'content'    => '
        <ul>
            <li><a href="#" class="-ico facebook">&nbsp;</a></li>
            <li><a href="#" class="-ico twitter">&nbsp;</a></li>
            <li><a href="#" class="-ico instagram">&nbsp;</a></li>
            <li><a href="#" class="-ico youtube">&nbsp;</a></li>
            <li><a href="#" class="-ico pinterest">&nbsp;</a></li>
        </ul>',
        'is_active'   => 1,
        'stores'      => $storeId
    ),

    array(
        'title'      => $storeName . 'Footer - Selos',
        'identifier' => 'footer-selos',
        'content'    => '
        <img src="{{media url="wysiwyg/selos/selos.jpg"}}" alt="" />',
        'is_active'   => 1,
        'stores'      => $storeId
    ),

    array(
        'title'      => $storeName . 'Barra de Vantagens',
        'identifier' => 'advantages-bar',
        'content'    => '
        <ul>
            <li><span>Frete Gratis</span>a partir de R$ 119</li>
            <li><span>10x sem juros</span>parcela mínima de R$ 20 no cartão</li>
            <li><span>3 Amostras Gratis</span>nas compras a partir de R$ 150</li>
        </ul>',
        'is_active'   => 1,
        'stores'      => $storeId
    )

);

/**
 * Insert default blocks
 */

foreach ($cmsBlocks as $data) {
    Mage::getModel('cms/block')->setData($data)->save();
}
