<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Não edite este arquivo caso você pretenda atualizar este módulo futuramente
 * para novas versões.
 *
 * @category      Esmart
 * @package       Esmart_Core
 *
 * @copyright     Copyright (c) 2015 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        e-smart Core Team <core@e-smart.com.br>
 */

$storeName = 'Sephora | Modal - ';
$storeId   = Mage::app()->getStore()->getStoreId();

$cmsBlocks = array(

    array(
        'title'      => $storeName . 'Se tornar VIB',
        'identifier' => 'modal-vib',
        'content'    => '<p>modal</p>',
        'is_active'  => 1,
        'stores'      => $storeId
    ),

    array(
        'title'      => $storeName . 'Não faz parte do beauty club',
        'identifier' => 'modal-notbc',
        'content'    => '<p>modal</p>',
        'is_active'  => 1,
        'stores'      => $storeId
    ),

    array(
        'title'      => $storeName . 'Termo de frete',
        'identifier' => 'modal-shipping-term',
        'content'    => '<p>modal</p>',
        'is_active'  => 1,
        'stores'      => $storeId
    ),

    array(
        'title'      => $storeName . 'Escolha do frete',
        'identifier' => 'modal-chose-delivery',
        'content'    => '<p>modal</p>',
        'is_active'  => 1,
        'stores'      => $storeId
    )

);

/**
 * Insert default blocks
 */

foreach ($cmsBlocks as $data) {
    Mage::getModel('cms/block')->setData($data)->save();
}
