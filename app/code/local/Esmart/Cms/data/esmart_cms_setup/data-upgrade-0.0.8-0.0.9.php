<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Não edite este arquivo caso você pretenda atualizar este módulo futuramente
 * para novas versões.
 *
 * @category      Esmart
 * @package       Esmart_Core
 *
 * @copyright     Copyright (c) 2015 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        e-smart Core Team <core@e-smart.com.br>
 */

$storeName = 'Sephora | Mobile - ';
$storeId   = Mage::app()->getStore()->getStoreId();

$cmsBlocks = array(

    array(
        'title'      => $storeName . 'Header links',
        'identifier' => 'mobileheader-links',
        'content'    => '
        <ul class="header-links">
            <li><a href="#" class="sale">Oferta</a></li>
            <li><a href="#">+ Vendidos</a></li>
            <li><a href="#">Descontos</a></li>
            <li><a href="#">Lojas</a></li>
        </ul>',
        'is_active'  => 1,
        'stores'      => $storeId
    ),

    array(
        'title'      => $storeName . 'Footer links',
        'identifier' => 'mobilefooter-links',
        'content'    => '
        <ul>
            <li><a href="#">Atendimento</a></li>
            <li><a href="#">Pedidos</a></li>
            <li><a href="#">Privacidade</a></li>
        </ul>',
        'is_active'  => 1,
        'stores'      => $storeId
    )

);

/**
 * Insert default blocks
 */

foreach ($cmsBlocks as $data) {
    Mage::getModel('cms/block')->setData($data)->save();
}
