<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Não edite este arquivo caso você pretenda atualizar este módulo futuramente
 * para novas versões.
 *
 * @category      Esmart
 * @package       Esmart_Cms
 *
 * @copyright     Copyright (c) 2015 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        e-smart Core Team <core@e-smart.com.br>
 */

$storeName = 'Sephora | ';
$storeId   = 1;

$cmsBlocks = array(

    array(
        'title'      => $storeName . 'Menu Customizavel',
        'identifier' => 'menu-custom',
        'content'    => '
        <ul>
            <li class="level0 nav-tutorial nav-custom">
                <a class="level0" href="javascript:;">Tutoriais</a>
                <div class="container-menu">
                    <ul class="level0">
                        <table>
                            <tbody>
                                <tr>
                                    <td><a href="https://beautyboard.sephora.com.br/actived"><img alt="" src="https://i1.sephora.com.br/media/wysiwyg/banners/1.jpg" /></a></td>
                                    <td><a href="https://meubeautystudio.sephora.com.br/guia-do-primer/"><img alt="" src="https://i1.sephora.com.br/media/wysiwyg/banners/2.jpg" /></a></td>
                                    <td><a href="https://www.youtube.com/watch?v=UC5hufcD3PA" target="_blank"><img alt="" src="https://i1.sephora.com.br/media/wysiwyg/banners/3.jpg" /></a></td>
                                </tr>
                            </tbody>
                        </table>
                    </ul>
                </div>
            </li>
        </ul>',
        'is_active'  => 1,
        'stores'     => $storeId
    )

);

/**
 * Insert default blocks
 */

foreach ($cmsBlocks as $data) {
    Mage::getModel('cms/block')->setData($data)->save();
}
