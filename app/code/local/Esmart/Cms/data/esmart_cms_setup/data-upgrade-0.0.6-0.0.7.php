<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Não edite este arquivo caso você pretenda atualizar este módulo futuramente
 * para novas versões.
 *
 * @category      Esmart
 * @package       Esmart_Core
 *
 * @copyright     Copyright (c) 2015 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        e-smart Core Team <core@e-smart.com.br>
 */

$layout = '
<reference name="root">
        <action method="addBodyClass"><classname>institucional</classname></action>
</reference>
<reference name="left">
        <block type="core/template" name="dinamic.sidebar" template="cms/dinamic-sidebar.phtml">
                <action method="setTitle"><title>Institucional</title></action>
                <action method="setPath"><path>institucional</path></action>
        </block>
</reference>
<reference name="aftermain">
        <block type="cms/block" name="instituctional_neemu">
                <action method="setBlockId"><block_id>neemu_instituctional</block_id></action>
        </block>
</reference>';

$storeId = Mage::app()->getStore()->getStoreId();

$cmsPages = array(

    array(
        'title'             => 'Sobre a Sephora',
        'content_heading'   => 'Sobre a Sephora',
        'root_template'     => 'two_columns_left',
        'layout_update_xml' => $layout,
        'identifier'        => 'institucional/sobre-a-sephora',
        'is_active'         => 1,
        'stores'            => $storeId,
        'sort_order'        => 0,
        'content'           => '
            <p>A Sephora &eacute; a maior rede de produtos de beleza do mundo, fundada na Fran&ccedil;a por Dominique Mandonnaud em 1970 e adquirida em 1997 pela empresa LVMH Mo&euml;t Hennessy Louis Vuitton, maior conglomerado de luxo do mundo. Ingressou no Brasil em julho de 2010, quando a LVMH Mo&euml;t Hennessy Louis Vuitton, adquiriu a Sack&rsquo;s, principal loja online de produtos de beleza de prest&iacute;gio no Brasil e na Am&eacute;rica Latina.</p>
            <p>&nbsp;</p>
            <p>A Sephora oferece uma infinita variedade de produtos de beleza das melhores e mais desejadas marcas do mundo. Para todos os clientes, s&atilde;o oferecidos os melhores lan&ccedil;amentos, cole&ccedil;&otilde;es e produtos exclusivos, que s&oacute; podem ser encontrados na Sephora. O sortimento inclui maquiagem, tratamento, perfumaria, cuidados com os cabelos, acess&oacute;rios, produtos de corpo e banho e a sua marca pr&oacute;pria, SEPHORA COLLECTION. Cada categoria cont&eacute;m desde as mais recentes inova&ccedil;&otilde;es da ind&uacute;stria de cosm&eacute;ticos at&eacute; os verdadeiros cl&aacute;ssicos da hist&oacute;ria da perfumaria e da ind&uacute;stria cosm&eacute;tica.</p>
            <p>&nbsp;</p>
            <p>Sephora.com.br &ndash; O site da Sephora Brasil &eacute; o principal e maior site de perfumes e cosm&eacute;ticos da Am&eacute;rica Latina em termos de vendas e sortimento de marcas e produtos.</p>
        ',
        'under_version_control' => 0
    ),

    array(
        'title'             => 'Trabalhe conosco',
        'content_heading'   => 'Trabalhe conosco',
        'root_template'     => 'two_columns_left',
        'layout_update_xml' => $layout,
        'identifier'        => 'institucional/trabalhe-conosco',
        'is_active'         => 1,
        'stores'            => $storeId,
        'sort_order'        => 0,
        'content'           => '',
        'under_version_control' => 0
    ),

    array(
        'title'             => 'Perguntas Frequentes',
        'content_heading'   => 'Perguntas Frequentes',
        'root_template'     => 'two_columns_left',
        'layout_update_xml' => $layout . '
            <reference name="content">
                    <block type="faq/frontend_list" name="cms.faq" template="cms/faq.phtml" />
            </reference>
        ',
        'identifier'        => 'institucional/perguntas-frequentes',
        'is_active'         => 1,
        'stores'            => $storeId,
        'sort_order'        => 0,
        'content'           => '
            <div class="no-display">Perguntas Frequentes</div>
        ',
        'under_version_control' => 0
    ),

    array(
        'title'             => 'Segurança',
        'content_heading'   => 'Segurança',
        'root_template'     => 'two_columns_left',
        'layout_update_xml' => $layout,
        'identifier'        => 'institucional/seguranca',
        'is_active'         => 1,
        'stores'            => $storeId,
        'sort_order'        => 0,
        'content'           => '
            <p>Fique tranq&uuml;ilo! Voc&ecirc; est&aacute; em um ambiente seguro!</p>
            <p>&nbsp;</p>
            <p>Na Sephora, todas as opera&ccedil;&otilde;es s&atilde;o criptografadas e 100% processadas por computador, sem interven&ccedil;&atilde;o humana.</p>
            <p>&nbsp;</p>
            <p>Todas as informa&ccedil;&otilde;es que passam pelo nosso processo de compra online s&atilde;o codificadas, desde seus dados pessoais at&eacute; a forma de pagamento. Verifique o &iacute;cone "cadeado fechado" na parte inferior do seu monitor durante o seu pedido de compra na Sephora. Esta seguran&ccedil;a &eacute; certificada pela GeoTrust, uma empresa Verisign, a maior autoridade em prote&ccedil;&atilde;o do consumidor e seguran&ccedil;a online em todo o mundo.</p>
            <p>&nbsp;</p>
            <p>Na Sephora, o n&uacute;mero do seu cart&atilde;o de cr&eacute;dito &eacute; usado somente no processamento de compra, n&atilde;o sendo, de nenhuma forma, guardado nos arquivos depois da opera&ccedil;&atilde;o.</p>
            <p><strong>Boas compras!</strong></p>
        ',
        'under_version_control' => 0
    ),

    array(
        'title'             => 'Política de privacidade',
        'content_heading'   => 'Política de privacidade',
        'root_template'     => 'two_columns_left',
        'layout_update_xml' => $layout,
        'identifier'        => 'institucional/politica-de-privacidade',
        'is_active'         => 1,
        'stores'            => $storeId,
        'sort_order'        => 0,
        'content'           => '
            <p>A Sephora respeita a sua privacidade porque garante o sigilo total das informa&ccedil;&otilde;es que voc&ecirc; nos fornece. Seus dados pessoais s&atilde;o armazenados em nosso banco de dados com o intuito de melhorar nosso relacionamento atrav&eacute;s de e-mail, mala-direta, telemarketing, entre outras formas de intera&ccedil;&atilde;o. Assim, podemos sempre lhe oferecer os melhores produtos e servi&ccedil;os.</p>
            <p>&nbsp;</p>
            <p>Durante sua navega&ccedil;&atilde;o em nosso site, arquivamos estat&iacute;sticas sobre as p&aacute;ginas mais visitadas e o tempo gasto em cada &aacute;rea do site (perfumes, maquiagem, tratamento, corpo e banho, presentes e ofertas), visando ajud&aacute;-lo a encontrar rapidamente o que voc&ecirc; precisa.</p>
            <p>&nbsp;</p>
            <p>Fique tranq&uuml;ilo, o n&uacute;mero do seu cart&atilde;o de cr&eacute;dito &eacute; usado somente no processamento de compra, n&atilde;o sendo, de nenhuma forma, guardado nos arquivos depois da opera&ccedil;&atilde;o. S&atilde;o guardados apenas os seus dados pessoais.</p>
            <p>&nbsp;</p>
            <p>O seu endere&ccedil;o de e-mail ser&aacute; utilizado para divulga&ccedil;&atilde;o de promo&ccedil;&otilde;es e lan&ccedil;amentos de nosso site, desde que previamente autorizado por voc&ecirc; em seu cadastro na Sephora. Nossos informativos t&ecirc;m como remetente news@sephora.com.br.</p>
            <p>&nbsp;</p>
            <p>Para cancelar o recebimento dos nossos informativos, clique aqui. Forne&ccedil;a seu e-mail e senha. Digite seu CEP, prossiga e desmarque a op&ccedil;&atilde;o "Desejo receber por e-mail as promo&ccedil;&otilde;es e lan&ccedil;amentos da Sephora".</p>
            <p>&nbsp;</p>
            <p>Em caso de d&uacute;vidas ou sugest&otilde;es em rela&ccedil;&atilde;o &agrave; nossa pol&iacute;tica de privacidade, por favor, entre em contato:</p>
            <p>Telefone: (21) 3004-7500</p>
            <p>E-mail: atendimento@sephora.com.br</p>
            <p>Caixa postal: Av. das Am&eacute;ricas 700, Bloco 8 Sala 317 F - Rio da Janeiro - CEP: 22.640-100</p>
            <p>&nbsp;</p>
            <p>A Sephora garante a seguran&ccedil;a de sua compra e respeita a sua privacidade. Desejamos a voc&ecirc; boas compras!</p>
        ',
        'under_version_control' => 0
    ),

    array(
        'title'             => 'Formas de pagamento',
        'content_heading'   => 'Formas de pagamento',
        'root_template'     => 'two_columns_left',
        'layout_update_xml' => $layout,
        'identifier'        => 'institucional/formas-de-pagamento',
        'is_active'         => 1,
        'stores'            => $storeId,
        'sort_order'        => 0,
        'content'           => '
            <p><strong>Com qual cart&atilde;o de cr&eacute;dito eu posso pagar as minhas compras ? </strong></p>
            <p>&nbsp;</p>
            <p>Na Sephora voc&ecirc; pode pagar suas compras atrav&eacute;s dos cart&otilde;es Mastercard, Visa, American Express e Diners.</p>
            <p>&nbsp;</p>
            <p>A Sephora possui um sistema de cobran&ccedil;a por cart&atilde;o de cr&eacute;dito 100% seguro. Os n&uacute;meros s&atilde;o criptografados e n&atilde;o ficam armazenados no nosso banco de dados, sendo enviados diretamente para a administradora do cart&atilde;o de cr&eacute;dito.</p>
            <p>&nbsp;</p>
            <p><strong>Como funciona o parcelamento no Cart&atilde;o de Cr&eacute;dito ? </strong></p>
            <p>&nbsp;</p>
            <p>Todas as compras podem ser parceladas em at&eacute; 10x nos cart&otilde;es de cr&eacute;dito Mastercard, Visa, American Express e Diners .</p>
            <p>&nbsp;</p>
            <p><strong>Imperd&iacute;vel! Pagamento em at&eacute; 10x nos cart&otilde;es de cr&eacute;dito citados acima com parcela m&iacute;nima de R$ 20,00. </strong></p>
            <p>&nbsp;</p>
            <p>N&atilde;o se esque&ccedil;a que o seu cart&atilde;o de cr&eacute;dito possui um limite de compras estabelecido pela administradora. Este valor contabilizado &eacute; o total e n&atilde;o a parcela da sua compra. Sendo assim, alguns pedidos poder&atilde;o ser recusados por ultrapassarem esse limite, n&atilde;o cabendo a Sephora a responsabilidade pela n&atilde;o aprova&ccedil;&atilde;o.</p>
            <p>&nbsp;</p>
            <p><strong>Posso pagar com Boleto Banc&aacute;rio ? </strong></p>
            <p>&nbsp;</p>
            <p>Esta op&ccedil;&atilde;o cria um boleto banc&aacute;rio para pagar a sua compra. A forma de pagamento em boleto banc&aacute;rio n&atilde;o oferece parcelamento. Para fazer o pagamento, imprima o boleto no final do processo de compra e pague em qualquer ag&ecirc;ncia banc&aacute;ria ou pela internet. Importante: o prazo de entrega para compras em boleto passa a valer somente ap&oacute;s a confirma&ccedil;&atilde;o do pagamento pela institui&ccedil;&atilde;o financeira, o que pode levar at&eacute; 2 (dois) dias &uacute;teis. O boleto banc&aacute;rio n&atilde;o &eacute; enviado pelos Correios e tem validade de 3 (tr&ecirc;s) dias corridos.</p>
        ',
        'under_version_control' => 0
    ),

    array(
        'title'             => 'Prazo de Entrega',
        'content_heading'   => 'Prazo de Entrega',
        'root_template'     => 'two_columns_left',
        'layout_update_xml' => $layout,
        'identifier'        => 'institucional/prazo-de-entrega',
        'is_active'         => 1,
        'stores'            => $storeId,
        'sort_order'        => 0,
        'content'           => '
            <p><strong>TIPO DE ENTREGA</strong></p>
            <p>&nbsp;</p>
            <p>REGULAR</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>- O prazo para entrega varia de acordo com o local, forma de pagamento e disponibilidade do produto adquirido e pode ser visualizado no campo &ldquo;Calcular prazo de entrega&rdquo;, na p&aacute;gina minha sacola.</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>*O prazo para entrega come&ccedil;a a ser contado a partir da aprova&ccedil;&atilde;o do seu pedido e &eacute; contado em dias &uacute;teis*, ou seja, n&atilde;o inclui s&aacute;bados, domingos e feriados. Hor&aacute;rio: de 2&ordf; a 6&ordf; feira, das 8h &agrave;s 18h.</p>
            <p>&nbsp;</p>
            <p>Excepcionalmente entregas podem ocorrer aos s&aacute;bados, domingos e feriados.</p>
        ',
        'under_version_control' => 0
    ),

    array(
        'title'             => 'História da perfumaria',
        'content_heading'   => 'História da perfumaria',
        'root_template'     => 'two_columns_left',
        'layout_update_xml' => $layout,
        'identifier'        => 'institucional/historia-da-perfumaria',
        'is_active'         => 1,
        'stores'            => $storeId,
        'sort_order'        => 0,
        'content'           => '
            <p>Ao penetrarem pelas narinas, os aromas encontram o sistema l&iacute;mbico, respons&aacute;vel pela mem&oacute;ria, sentimentos e emo&ccedil;&otilde;es. A s&aacute;bia Cle&oacute;patra seduziu Marco Ant&ocirc;nio e Julio C&eacute;sar usando um perfume &agrave; base de &oacute;leos extra&iacute;dos das flores.</p>
            <p>&nbsp;</p>
            <p>Nos tempos mais remotos, os homens invocavam os deuses por meio da fuma&ccedil;a. Eles queimavam ervas, que liberavam diversos aromas. Foi neste contexto que surgiu a palavra "perfume", em latim "per fumum", que significa "atrav&eacute;s da fuma&ccedil;a".</p>
            <p>&nbsp;</p>
            <p>A hist&oacute;ria do perfume remonta h&aacute; tr&ecirc;s mil anos e as lendas que envolvem sua cria&ccedil;&atilde;o v&atilde;o mais longe ainda. Foi na &Iacute;ndia e na Ar&aacute;bia que surgiram os primeiros mestres da perfumaria. Ali j&aacute; havia sido criada a &aacute;gua de col&ocirc;nia, obtida pela macera&ccedil;&atilde;o de p&eacute;talas de rosas.</p>
            <p>&nbsp;</p>
            <p>Os &aacute;rabes n&atilde;o s&oacute; compreendiam e apreciavam os prazeres dos perfumes, mas tamb&eacute;m tinham conhecimentos avan&ccedil;ados de higiene e medicina. Eles produziram elixires partindo de plantas e animais com prop&oacute;sitos cosm&eacute;ticos e terap&ecirc;uticos. Por volta do s&eacute;culo X, Avicena descobriu a destila&ccedil;&atilde;o dos &oacute;leos essenciais das rosas, e assim criou a &Aacute;gua de Rosas. Depois veio a Eau de Toilette, feito para a rainha da Hungria. No s&eacute;culo XIX o perfume ganha novos usos, como o terap&ecirc;utico, por exemplo.</p>
            <p>&nbsp;</p>
            <p>O esplendor da perfumaria florescia com a renascen&ccedil;a. Foi ent&atilde;o, na Europa que o perfume desen volveu e se popularizou. Mesmo feito ainda de forma artesanal desempenhava sua forma social como parte dos luxos di&aacute;rios e necess&aacute;rios de toda mulher, encantando com suas doces fragr&acirc;ncias e charmosos frascos, capazes de transformar os perfumes at&eacute; os dias de hoje, em verdadeiros objetos de desejo.</p>
            <p>&nbsp;</p>
            <p>Foi nessa &eacute;poca que Paris se tornou uma refer&ecirc;ncia mundial em produ&ccedil;&atilde;o de fragr&acirc;ncias e perfumes, e fez com que os perfumes franceses conquistassem o mundo. Hoje os perfumes se dividem em alguns tipos.</p>
            <p>&nbsp;</p>
            <p><strong>Tipos de pefumes mais comuns</strong></p>
            <p>&nbsp;</p>
            <p>- Eau de parfum: mais forte, tem, em sua composi&ccedil;&atilde;o, de 10% a 20% de concentra&ccedil;&atilde;o de ess&ecirc;ncias e seu efeito de fixa&ccedil;&atilde;o chega a 12 horas. Bastam algumas gotas em lugares estrat&eacute;gicos como a nuca, atr&aacute;s da orelha e atr&aacute;s do joelho, para voc&ecirc; ficar perfumado o dia todo;</p>
            <p>&nbsp;</p>
            <p>- Eau de toilette: Com fragr&acirc;ncias mais discretas s&atilde;o perfeitos para serem usados em climas tropicais com o goiano. Sua fixa&ccedil;&atilde;o n&atilde;o passa de oito horas, e mesmo assim, em dias mais quentes. Sua concentra&ccedil;&atilde;o de ess&ecirc;ncia varia entre 6% e 12%;</p>
            <p>&nbsp;</p>
            <p>- Eau de cologne: Excelentes para o nosso clima, tamb&eacute;m podem ser usados durante o dia. Seu poder de fixa&ccedil;&atilde;o n&atilde;o dura mais do que cinco horas e a concentra&ccedil;&atilde;o fica entre 5% e 8%;</p>
            <p>&nbsp;</p>
            <p>- Deo col&ocirc;nia: O mais suave dos perfumes tem o m&iacute;nimo de concentra&ccedil;&atilde;o de ess&ecirc;ncia, chegando ao m&aacute;ximo de 10%, sendo sua fixa&ccedil;&atilde;o de duas a quatro horas, com algumas exce&ccedil;&otilde;es que chegam a at&eacute; 8h.</p>
            <p>&nbsp;</p>
            <p>&Eacute; comum o mesmo perfume apresentar cheiros diferentes quando aplicado em pessoas diferentes. Isso porque, os odores corporais s&atilde;o &uacute;nicos, sendo resultado da alimenta&ccedil;&atilde;o, das caracter&iacute;sticas pessoais, dos lip&iacute;deos e &aacute;cidos graxos que a pele exala. A temperatura da pele interfere diretamente na vaporiza&ccedil;&atilde;o do perfume, e portanto no cheiro que ele exala.</p>
            <p>&nbsp;</p>
            <p>Hoje sabemos que o perfume &eacute; capaz de revelar a personalidade das pessoas, bem como sua classe social. De fato, o perfume &eacute; muito mais do que um prazer dos sentidos. &Eacute; tamb&eacute;m uma mensagem, algo do pr&oacute;prio ser humano, projetando no exterior seu "eu" profundo, seus gostos, suas aspira&ccedil;&otilde;es secretas.</p>
            <p>&nbsp;</p>
            <p>Atualmente, a ind&uacute;stria se desenvolveu a tal ponto, que esse aroma &eacute; obtido sinteticamente. O perfume Chanel n&ordm; 5 deu in&iacute;cio a um boom no uso dos alde&iacute;dos na fabrica&ccedil;&atilde;o de perfumes. Outro m&eacute;rito da perfumaria foi refor&ccedil;ar a associa&ccedil;&atilde;o entre o uso do perfume e o jogo de sedu&ccedil;&atilde;o. A discuss&atilde;o ganhou f&ocirc;lego na d&eacute;cada de 50, quando a atriz Marilyn Monroe declarou que usava apenas uma gotinha de perfume para dormir. Mas a rela&ccedil;&atilde;o entre aroma agrad&aacute;vel e apelo sensual vem de &eacute;pocas remotas.</p>
            <p>&nbsp;</p>
            <p>S&atilde;o necess&aacute;rios entre 6 a 18 meses para criar um perfume. &Eacute; um per&iacute;odo m&aacute;gico em que as mat&eacute;rias primas se transformam em odor.</p>
        ',
        'under_version_control' => 0
    ),

);

$cmsBlocks = array(

    array(
        'title'      => 'Neemu Institucional',
        'identifier' => 'neemu_instituctional',
        'content'    => 'NEEMU',
        'is_active'  => 1,
        'stores'     => $storeId
    )

);

/**
 * Insert default Pages
 */

foreach ($cmsPages as $data) {
    Mage::getModel('cms/page')->setData($data)->save();
}

/**
 * Insert default blocks
 */

foreach ($cmsBlocks as $data) {
    Mage::getModel('cms/block')->setData($data)->save();
}
