<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Não edite este arquivo caso você pretenda atualizar este módulo futuramente
 * para novas versões.
 *
 * @category      Esmart
 * @package       Esmart_Core
 *
 * @copyright     Copyright (c) 2015 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        e-smart Core Team <core@e-smart.com.br>
 */

$storeName = 'Sephora | Mobile Blackfriday - ';
$storeId   = 2;

$cmsBlocks = array(

    array(
        'title'      => $storeName . 'Header links',
        'identifier' => 'mobileheader-links-blackfriday',
        'content'    => '
        <ul class="header-links">
            <li class="sale"><a href="/blackfriday"><img alt="" src="{{skin url=\'images/blackfriday.png\'}}" /></a></li>
            <li><a href="https://mpesquisa.sephora.com.br/mais-vendidos">+ Vendidos</a></li>
            <li><a href="https://mpesquisa.sephora.com.br/ofertas">Descontos</a></li>
            <li><a href="/nossas-lojas">Lojas</a></li>
        </ul>',
        'is_active'  => 1,
        'stores'      => $storeId
    )

);

/**
 * Insert default blocks
 */

foreach ($cmsBlocks as $data) {
    Mage::getModel('cms/block')->setData($data)->save();
}
