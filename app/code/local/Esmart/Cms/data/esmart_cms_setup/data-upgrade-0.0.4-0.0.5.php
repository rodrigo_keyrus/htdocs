<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Não edite este arquivo caso você pretenda atualizar este módulo futuramente
 * para novas versões.
 *
 * @category      Esmart
 * @package       Esmart_Core
 *
 * @copyright     Copyright (c) 2015 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        e-smart Core Team <core@e-smart.com.br>
 */

$storeName = 'Sephora | Beautyclub - ';
$storeId   = Mage::app()->getStore()->getStoreId();

$cmsBlocks = array(

    array(
        'title'      => $storeName . 'Extrato',
        'identifier' => 'beautyclub-history',
        'content'    => '
        <table border="0">
            <tbody>
                <tr>
                    <td>
                        <p><strong>T&iacute;tulo</strong></p>
                        <p>Lorem ipsum dolor sit amet, mea nisl appellantur referrentur cu, nobis admodum efficiendi quo ea. Zril laoreet noluisse id usu. Te sea soleat equidem, duo dicant cetero at. Sed id postea aeterno, meis conceptam ea vel. Qui an aeque ludus eleifend, zril eirmod numquam sed id. Id sale indoctum eam, rationibus efficiantur mea ad. Lorem ipsum dolor sit amet, mea nisl appellantur referrentur cu</p>
                    </td>
                </tr>
            </tbody>
        </table>',
        'is_active'   => 1,
        'stores'      => $storeId
    ),

);

/**
 * Insert default blocks
 */

foreach ($cmsBlocks as $data) {
    Mage::getModel('cms/block')->setData($data)->save();
}
