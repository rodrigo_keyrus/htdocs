<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Não edite este arquivo caso você pretenda atualizar este módulo futuramente
 * para novas versões.
 *
 * @category      Esmart
 * @package       Esmart_Core
 *
 * @copyright     Copyright (c) 2015 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        e-smart Core Team <core@e-smart.com.br>
 */

$storeName = 'Sephora | Produto - ';
$storeId   = Mage::app()->getStore()->getStoreId();

$cmsBlocks = array(

    array(
        'title'      => $storeName . 'Neemu Look',
        'identifier' => 'neemu_look',
        'content'    => 'NEEMU',
        'is_active'   => 1,
        'stores'      => $storeId
    ),

    array(
        'title'      => $storeName . 'Neemu Recomendações',
        'identifier' => 'neemu_recommendations',
        'content'    => 'NEEMU',
        'is_active'   => 1,
        'stores'      => $storeId
    ),

    array(
        'title'      => $storeName . 'Neemu Você já viu',
        'identifier' => 'neemu_seen',
        'content'    => 'NEEMU',
        'is_active'   => 1,
        'stores'      => $storeId
    )

);

/**
 * Insert default blocks
 */

foreach ($cmsBlocks as $data) {
    Mage::getModel('cms/block')->setData($data)->save();
}
