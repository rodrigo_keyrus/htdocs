<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Não edite este arquivo caso você pretenda atualizar este módulo futuramente
 * para novas versões.
 *
 * @category      Esmart
 * @package       Esmart_Core
 *
 * @copyright     Copyright (c) 2015 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        e-smart Core Team <core@e-smart.com.br>
 */

$storeName = 'Sephora | Beautyclub - ';
$storeId   = Mage::app()->getStore()->getStoreId();

$cmsBlocks = array(

    array(
        'title'      => $storeName . 'Junte-se ao clube',
        'identifier' => 'beautyclub-slider_one',
        'content'    => '<p><img alt="" src="{{media url="wysiwyg/beautyclub/banner1.jpg"}}" /></p>',
        'is_active'  => 1,
        'stores'      => $storeId
    ),

    array(
        'title'      => $storeName . 'Acumule pontos',
        'identifier' => 'beautyclub-slider_two',
        'content'    => '<p><img alt="" src="{{media url="wysiwyg/beautyclub/banner2.jpg"}}" /></p>',
        'is_active'  => 1,
        'stores'      => $storeId
    ),

    array(
        'title'      => $storeName . 'Troque pontos por prêmios',
        'identifier' => 'beautyclub-slider_three',
        'content'    => '<p><img alt="" src="{{media url="wysiwyg/beautyclub/banner3.jpg"}}" /></p>',
        'is_active'  => 1,
        'stores'      => $storeId
    ),

    array(
        'title'      => $storeName . 'Vá mais longe',
        'identifier' => 'beautyclub-slider_four',
        'content'    => '<p><img alt="" src="{{media url="wysiwyg/beautyclub/banner4.jpg"}}" /></p>',
        'is_active'  => 1,
        'stores'      => $storeId
    ),

    array(
        'title'      => $storeName . 'O Programa',
        'identifier' => 'beautyclub-dashboard',
        'content'    => '
        <table border="0">
            <tbody>
                <tr>
                    <td style="width: 750px;">EMBED VIDEO</td>
                    <td><img alt="" src="{{media url="wysiwyg/beautyclub/aniversario.jpg"}}" /></td>
                </tr>
                <tr>
                    <td colspan="2"><img alt="" src="{{media url="wysiwyg/beautyclub/resgate-epico.jpg"}}" /></td>
                </tr>
            </tbody>
        </table>',
        'is_active'   => 1,
        'stores'      => $storeId
    ),

    array(
        'title'      => $storeName . 'Resgate Épico - Produto',
        'identifier' => 'beautyclub-epic_product',
        'content'    => '<p><img alt="" src="{{media url="wysiwyg/beautyclub/epic.jpg"}}" /></p>',
        'is_active'   => 1,
        'stores'      => $storeId
    ),

    array(
        'title'      => $storeName . 'Resgate Épico - Info',
        'identifier' => 'beautyclub-epic_info',
        'content'    => '
        <table border="0">
            <tbody>
                <tr>
                    <td colspan="2">
                        <p>*Resgate feito apenas atrav&eacute;s da central de relacionamento com o cliente via telefone ou chat online // item n&atilde;o dispon&iacute;vel para venda // dispon&iacute;vel para resgate a partir do dia 29/02/2016 &agrave;s 9:30 da manh&atilde; // descri&ccedil;&atilde;o do pr&ecirc;mio: Exclusiva caixa de charms Carolina Herrera, com duas fragr&acirc;ncias no tamanho original (CH Fem 30ml e CH Masc 50ml) + 3 pulseiras com pingente CH fem (cor creme) + 3 pulseiras com pingente CH fem (cor vermelha) + 2 pulseiras masculinas + 24 charms.</p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p><strong>Como resgatar?</strong></p>
                        <p>Voc&ecirc; precisa ter em sua conta do Beauty Club a quantidade necess&aacute;ria de pontos para fazer o resgate.</p>
                        <p>Se voc&ecirc; tiver os pontos em sua conta ligue a para a nossa Central de Relacionamento ou entre em contato via chat e informe seu CPF para um atendente para que ele possa validar e fazer o resgate.</p>
                        <p>Voc&ecirc; receber&aacute; um e-mail confirmando o seu resgate. O pr&ecirc;mio ser&aacute; enviado para a sua casa em at&eacute; 5 dias &uacute;teis, ap&oacute;s o envio do e-mail de confirma&ccedil;&atilde;o.</p>
                        <p>Importante: N&atilde;o se esque&ccedil;a de manter seus dados do BEAUTY CLUB atualizados, sem eles n&atilde;o conseguimos finalizar seu resgate.</p>
                    </td>
                    <td>
                        <p><strong>Regulamento</strong></p>
                        <p>O resgate ser&aacute; feito somente atrav&eacute;s da Central de Relacionamento e chat Sephora.</p>
                        <p>S&oacute; poder&aacute; fazer o resgate o cliente que tiver os 10.000 pontos dispon&iacute;veis na hora da liga&ccedil;&atilde;o.</p>
                        <p>O item estar&aacute; dispon&iacute;vel para resgate at&eacute; 29/02/2016 ou at&eacute; acabarem os estoques.</p>
                        <p>A quantidade de Charm Box &eacute; limitada.</p>
                        <p>Ser&aacute; permitido o resgate de uma (1unidade) Charm Box por CPF.</p>
                        <p>Para resgatar a Charm Box o cliente deve fazer parte do Beauty Club (Programa de fidelidade da Sephora Brasil).</p>
                        <p>Os pontos referentes a promo&ccedil;&atilde;o &ldquo;Dobro de pontos em compras de Carolina Herrera&rdquo; ser&atilde;o creditados em sua conta Beauty Club no dia 28/02.</p>
                        <p>&nbsp;</p>
                        <p>Central de Relacionamento</p>
                        <p>Telefone: Capitais e regi&otilde;es metropolitanas &ndash; 3004-7500* // Outras regi&otilde;es &ndash; (21) 3004-7500*</p>
                        <p>Estado do Rio de Janeiro &ndash; 0800 722 7500&nbsp;</p>
                    </td>
                </tr>
            </tbody>
        </table>',
        'is_active'   => 1,
        'stores'      => $storeId
    ),

    array(
        'title'      => $storeName . 'Regulamento',
        'identifier' => 'beautyclub-regulation',
        'content'    => '
        <table border="0">
            <tbody>
                <tr>
                    <td>
                        <p><strong>T&iacute;tulo</strong></p>
                        <p>Lorem ipsum dolor sit amet, mea nisl appellantur referrentur cu, nobis admodum efficiendi quo ea. Zril laoreet noluisse id usu. Te sea soleat equidem, duo dicant cetero at. Sed id postea aeterno, meis conceptam ea vel. Qui an aeque ludus eleifend, zril eirmod numquam sed id. Id sale indoctum eam, rationibus efficiantur mea ad. Lorem ipsum dolor sit amet, mea nisl appellantur referrentur cu, nobis admodum efficiendi quo ea. Zril laoreet noluisse id usu. Te sea soleat equidem, duo dicant cetero at. Sed id postea aeterno, meis conceptam ea vel. Qui an aeque ludus eleifend, zril eirmod numquam sed id. Id sale indoctum eam, rationibus efficiantur mea ad. Lorem ipsum dolor sit amet, mea nisl appellantur referrentur cu, nobis admodum efficiendi quo ea. Zril laoreet noluisse id usu. Te sea soleat equidem, duo dicant cetero at. Sed id postea aeterno, meis conceptam ea vel. Qui an aeque ludus eleifend, zril eirmod numquam sed id. Id sale indoctum eam, rationibus efficiantur mea ad. Lorem ipsum dolor sit amet, mea nisl appellantur referrentur cu, nobis admodum efficiendi quo ea. Zril laoreet noluisse id usu. Te sea soleat equidem, duo dicant cetero at. Sed id postea aeterno, meis conceptam ea vel. Qui an aeque ludus eleifend, zril eirmod numquam sed id. Id sale indoctum eam, rationibus efficiantur mea ad.</p>
                    </td>
                    <td>
                        <p><strong>T&iacute;tulo</strong></p>
                        <p>Lorem ipsum dolor sit amet, mea nisl appellantur referrentur cu, nobis admodum efficiendi quo ea. Zril laoreet noluisse id usu. Te sea soleat equidem, duo dicant cetero at. Sed id postea aeterno, meis conceptam ea vel. Qui an aeque ludus eleifend, zril eirmod numquam sed id. Id sale indoctum eam, rationibus efficiantur mea ad. Lorem ipsum dolor sit amet, mea nisl appellantur referrentur cu, nobis admodum efficiendi quo ea. Zril laoreet noluisse id usu. Te sea soleat equidem, duo dicant cetero at. Sed id postea aeterno, meis conceptam ea vel. Qui an aeque ludus eleifend, zril eirmod numquam sed id. Id sale indoctum eam, rationibus efficiantur mea ad. Lorem ipsum dolor sit amet, mea nisl appellantur referrentur cu, nobis admodum efficiendi quo ea. Zril laoreet noluisse id usu. Te sea soleat equidem, duo dicant cetero at. Sed id postea aeterno, meis conceptam ea vel. Qui an aeque ludus eleifend, zril eirmod numquam sed id. Id sale indoctum eam, rationibus efficiantur mea ad.</p>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <p><strong>T&iacute;tulo</strong></p>
                        <p>Lorem ipsum dolor sit amet, mea nisl appellantur referrentur cu, nobis admodum efficiendi quo ea. Zril laoreet noluisse id usu. Te sea soleat equidem, duo dicant cetero at. Sed id postea aeterno, meis conceptam ea vel. Qui an aeque ludus eleifend, zril eirmod numquam sed id. Id sale indoctum eam, rationibus efficiantur mea ad. Lorem ipsum dolor sit amet, mea nisl appellantur referrentur cu, nobis admodum efficiendi quo ea. Zril laoreet noluisse id usu. Te sea soleat equidem, duo dicant cetero at. Sed id postea aeterno, meis conceptam ea vel. Qui an aeque ludus eleifend, zril eirmod numquam sed id. Id sale indoctum eam, rationibus efficiantur mea ad. Lorem ipsum dolor sit amet, mea nisl appellantur referrentur cu, nobis admodum efficiendi quo ea. Zril laoreet noluisse id usu. Te sea soleat equidem, duo dicant cetero at. Sed id postea aeterno, meis conceptam ea vel. Qui an aeque ludus eleifend, zril eirmod numquam sed id. Id sale indoctum eam, rationibus efficiantur mea ad. Lorem ipsum dolor sit amet, mea nisl appellantur referrentur cu, nobis admodum efficiendi quo ea. Zril laoreet noluisse id usu. Te sea soleat equidem, duo dicant cetero at. Sed id postea aeterno, meis conceptam ea vel. Qui an aeque ludus eleifend, zril eirmod numquam sed id. Id sale indoctum eam, rationibus efficiantur mea ad. Lorem ipsum dolor sit amet, mea nisl appellantur referrentur cu, nobis admodum efficiendi quo ea. Zril laoreet noluisse id usu. Te sea soleat equidem, duo dicant cetero at. Sed id postea aeterno, meis conceptam ea vel. Qui an aeque ludus eleifend, zril eirmod numquam sed id. Id sale indoctum eam, rationibus efficiantur mea ad. Lorem ipsum dolor sit amet, mea nisl appellantur referrentur cu, nobis admodum efficiendi quo ea. Zril laoreet noluisse id usu. Te sea soleat equidem, duo dicant cetero at. Sed id postea aeterno, meis conceptam ea vel. Qui an aeque ludus eleifend, zril eirmod numquam sed id. Id sale indoctum eam, rationibus efficiantur mea ad. Lorem ipsum dolor sit amet, mea nisl appellantur referrentur cu, nobis admodum efficiendi quo ea. Zril laoreet noluisse id usu. Te sea soleat equidem, duo dicant cetero at. Sed id postea aeterno, meis conceptam ea vel. Qui an aeque ludus eleifend, zril eirmod numquam sed id. Id sale indoctum eam, rationibus efficiantur mea ad. Lorem ipsum dolor sit amet, mea nisl appellantur referrentur cu, nobis admodum efficiendi quo ea. Zril laoreet noluisse id usu. Te sea soleat equidem, duo dicant cetero at. Sed id postea aeterno, meis conceptam ea vel. Qui an aeque ludus eleifend, zril eirmod numquam sed id. Id sale indoctum eam, rationibus efficiantur mea ad.</p>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <p><strong>Perguntas Frequentes</strong></p>
                        <p>Lorem ipsum dolor sit amet, mea nisl appellantur referrentur cu, nobis admodum efficiendi quo ea. Zril laoreet noluisse id usu. Te sea soleat equidem, duo dicant cetero at. Sed id postea aeterno, meis conceptam ea vel. Qui an aeque ludus eleifend, zril eirmod numquam sed id. Id sale indoctum eam, rationibus efficiantur mea ad. Lorem ipsum dolor sit amet, mea nisl appellantur referrentur cu, nobis admod.</p>
                    </td>
                </tr>
            </tbody>
        </table>',
        'is_active'   => 1,
        'stores'      => $storeId
    ),

    array(
        'title'      => $storeName . 'Perguntas Frequentes',
        'identifier' => 'beautyclub-faq',
        'content'    => '
        <table border="0">
            <tbody>
                <tr>
                    <td>
                        <p><strong>T&iacute;tulo</strong></p>
                        <p>Lorem ipsum dolor sit amet, mea nisl appellantur referrentur cu, nobis admodum efficiendi quo ea. Zril laoreet noluisse id usu. Te sea soleat equidem, duo dicant cetero at. Sed id postea aeterno, meis conceptam ea vel. Qui an aeque ludus eleifend, zril eirmod numquam sed id. Id sale indoctum eam, rationibus efficiantur mea ad. Lorem ipsum dolor sit amet, mea nisl appellantur referrentur cu</p>
                    </td>
                </tr>
            </tbody>
        </table>',
        'is_active'   => 1,
        'stores'      => $storeId
    ),

    array(
        'title'      => $storeName . 'Neemu',
        'identifier' => 'beautyclub_neemu',
        'content'    => 'SCRIPT NEEMU',
        'is_active'   => 1,
        'stores'      => $storeId
    )

);

/**
 * Insert default blocks
 */

foreach ($cmsBlocks as $data) {
    Mage::getModel('cms/block')->setData($data)->save();
}
