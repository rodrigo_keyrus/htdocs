<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Não edite este arquivo caso você pretenda atualizar este módulo futuramente
 * para novas versões.
 *
 * @category      Esmart
 * @package       Esmart_Core
 *
 * @copyright     Copyright (c) 2015 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        e-smart Core Team <core@e-smart.com.br>
 */

$storeName = 'Sephora | ';
$storeId   = Mage::app()->getStore()->getStoreId();

$cmsBlocks = array(

    array(
        'title'      => $storeName . 'Footer Onepage - Selos',
        'identifier' => 'footer-onepage_seals',
        'content'    => '<p><img alt="" src="{{media url="wysiwyg/selos/blindado.png"}}" /></p>',
        'is_active'  => 1,
        'stores'      => $storeId
    ),

    array(
        'title'      => $storeName . 'Footer Onepage - Copyright',
        'identifier' => 'footer-onepage_copyright',
        'content'    => '
            <p>A inclus&atilde;o de um produto na &ldquo;sacola&rdquo;n&atilde;o garante seu pre&ccedil;o. Em caso de varia&ccedil;&atilde;o, prevalecer&aacute; o pre&ccedil;o vigente</p>
            <p>na &ldquo;finaliza&ccedil;&atilde;o&rdquo;da compra. Copyright &copy; 2015, TODOS OS DIREITOS RESERVADOS. As fotos aqui veiculadas,</p>
            <p>logotipo e marca s&atilde;o de propriedade de www.sephora.com.br. &Eacute; vedada a sia reprodu&ccedil;&atilde;o, total ou parcial.</p>
        ',
        'is_active'  => 1,
        'stores'      => $storeId
    ),

    array(
        'title'      => $storeName . 'Prazo Carrinho',
        'identifier' => 'term-cart',
        'content'    => '
            <p>O prazo de entrega come&ccedil;a a contar a partir da aprova&ccedil;&atilde;o do pedido.</p>
            <p>Pedidos pagos por boleto banc&aacute;rio tem 3 dias &uacute;teis acrescidos ao prazo</p>
            <p>de entrega. <a href="#">Entenda os prazos</a></p>
        ',
        'is_active'  => 1,
        'stores'      => $storeId
    )

);

/**
 * Insert default blocks
 */

foreach ($cmsBlocks as $data) {
    Mage::getModel('cms/block')->setData($data)->save();
}
