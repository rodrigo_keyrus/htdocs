<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category      Esmart
 * @package       Esmart_MessageRule
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class Esmart_MessageRule_Block_Message extends Mage_Core_Block_Template
{
    /**
     * Initialize constructor
     */
    public function _construct()
    {
        parent::_construct();

        $this->setTemplate('esmart/messagerule/message.phtml');
    }

    /**
     * Return available conditions
     *
     * @return Varien_Data_Collection
     */
    protected function getCollection()
    {
        return Mage::getSingleton('esmart_messagerule/message')->getConditions();
    }
}
