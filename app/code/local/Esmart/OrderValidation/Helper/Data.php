<?php
/**
 * BSeller Platform | B2W - Companhia Digital
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category  Esmart
 * @package   Esmart_OrderValidation
 *
 * @copyright Copyright (c) 2016 B2W Digital - BSeller Platform. (http://www.bseller.com.br)
 *
 * @author    BSeller Core Team <dev@e-smart.com.br>
 */
class Esmart_OrderValidation_Helper_Data extends Esmart_Core_Helper_Data
{

    /**
     * Get saved settings in the admin
     *
     * @param string $path
     * @param int $level
     *
     * @return string
     */
    public function getConfig($path, $storeId = null)
    {
        return Mage::getStoreConfig($path);
    }

    /**
     * Checks if module is active or not
     *
     * @return boolean
     */
    public function isActive(){
        return $this->getConfig('esmart_ordervalidation/settings/active', $this->getStoreId());
    }

    /**
     * Get current storeId
     *
     * @return int
     */
    public function getStoreId(){
        return Mage::app()->getStore()->getStoreId();
    }

    /**
     * Format value to current currency
     *
     * @param float price
     *
     * @return string
     */
    public function formatPrice($price){
        return Mage::helper('core')->currency($price, true, false);
    }
}
