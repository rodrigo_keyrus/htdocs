<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category      Esmart
 * @package       Esmart_Voucher
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class Esmart_Voucher_Model_Voucher extends Mage_Core_Model_Abstract
{
    /**
     * Return available credit amount
     *
     * @return float
     */
    public function getAvailableCreditAmount()
    {
        /** @var Mage_Customer_Model_Session $session */
        $session = Mage::getSingleton('customer/session');
        if ($session->getData('voucher_credit_amount') !== null) {
            return $session->getData('voucher_credit_amount');
        }

        $value = $this->helper()->getValueAvailableVales($session->getCustomer()->getCpf());

        $customer = Mage::getSingleton('customer/session')->getCustomer();
        if($customer && $customer->getId()) {
            $this->_setSessionVoucherCreditAmount($customer, $value);
        }

        return $value;
    }

    private function _setSessionVoucherCreditAmount($customer, $value = false)
    {
        if ($value === false) {
            $value = $this->helper()->getValueAvailableVales($customer->getCpf());
        }

        $session = Mage::getSingleton('customer/session');
        $session->setData('voucher_credit_amount', floatval($value));
    }

    /**
     * Return credit amount from webservice
     *
     * @param string $value
     * @return float
     */
    public function getCreditAmount($customer)
    {
        $attrCode    = $this->helper()->getStoreConfig('document_attribute');
        $attrValue   = $customer->getData($attrCode);
        $custAccount = str_pad(preg_replace('/[^0-9]/', '', $attrValue), 11, '0', STR_PAD_LEFT);

        /**
         * If module run with approval mode
         * Show a test amount
         */
        if ($this->helper()->isApproval()) {
            $amount = $this->helper()->getStoreConfig('approval_credit_amount');

            return (float) $amount;
        }

        $amount = Mage::helper('bsellererp_vale')->getValeAmount($custAccount);

        return (float) $amount;
    }

    /**
     * Return credit amount history
     *
     * @param $customerId
     * @return float
     */
    protected function getCreditAmountHistory($customerId)
    {
        /** @var Esmart_Voucher_Model_Resource_History $resource */
        $resource = Mage::getResourceModel('esmart_voucher/history');

        /** @var Varien_Db_Adapter_Interface $readAdapter */
        $readAdapter = $resource->getReadConnection();

        $select = $readAdapter->select()
            ->from($resource->getMainTable(), new Zend_Db_Expr('SUM(credit_amount_used)'))
            ->where('customer_id = ?', $customerId)
            ->where('status = ?', Esmart_Voucher_Model_History::STATUS_NOT_PAYED);

        /** @var array $histories */
        $amountHistory = $readAdapter->fetchOne($select);

        return (float) ($amountHistory >= 0) ? $amountHistory : 0;
    }

    /**
     * Return data helper object
     *
     * @return Esmart_Voucher_Helper_Data
     */
    protected function helper()
    {
        return Mage::helper('esmart_voucher');
    }
}
