<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category      Esmart
 * @package       Esmart_Voucher
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class Esmart_Voucher_Model_Total_Invoice_Credit extends Mage_Sales_Model_Order_Invoice_Total_Abstract
{
    /**
     * Collect totals process
     *
     * @param Mage_Sales_Model_Order_Invoice $invoice
     * @return $this
     */
    public function collect(Mage_Sales_Model_Order_Invoice $invoice)
    {
        $amountUsed = $invoice->getData('voucher_credit_amount_used');

        if (!$this->helper()->isValidCreditAmount($amountUsed)) {
            return $this;
        }

        $invoice->setGrandTotal($invoice->getGrandTotal() - $amountUsed);
        $invoice->setBaseGrandTotal($invoice->getBaseGrandTotal() - $amountUsed);

        return $this;
    }

    /**
     * Return data helper object
     *
     * @return Esmart_Voucher_Helper_Data
     */
    protected function helper()
    {
        return Mage::helper('esmart_voucher');
    }
}
