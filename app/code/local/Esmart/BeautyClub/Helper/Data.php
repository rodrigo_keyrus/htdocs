<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Não edite este arquivo caso você pretenda atualizar este módulo futuramente
 * para novas versões.
 *
 * @category      Esmart
 * @package       Esmart_BeautyClub
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Diogo Santiago <diogo.santaigo@e-smart.com.br>
 */

class Esmart_BeautyClub_Helper_Data extends Mage_Core_Helper_Data
{
    /**
     * Clear Quote BC items
     */
    public function clearBcItems()
    {
        Mage::getSingleton('checkout/session')->unsBeautyClubProducts();
        Mage::getSingleton('checkout/session')->unsData('bc_birthday_product');
    }

    /**
     * @param array $products
     */
    public function setBcItems(array $products)
    {
        /** @var Mage_Checkout_Model_Session $checkoutSession */
        $checkoutSession = Mage::getSingleton('checkout/session');
        $checkoutSession->setBeautyClubProducts($products);
    }

    /**
     * Return if bc products is available to show
     *
     * @return bool
     */
    public function isAvailable()
    {
        /** @var Mage_Customer_Model_Session $customer */
        $customer = Mage::getSingleton('customer/session');

        if (!$customer->getCustomer()->getData('is_bc')) {
            return false;
        }

        /** @var Mage_Catalog_Model_Resource_Product_Collection $products */
        $products = Mage::getSingleton('esmart_bc/bc')->getProductCollection();

        if (!$products->count()) {
            return false;
        }

        if (Mage::getSingleton('checkout/session')->getData('bc_birthday_product')) {
            return true;
        }

        /** @var int $fullPoints */
        $fullPoints = $customer->getCustomer()->getFullPoints();
        if ($fullPoints < 0) {
            return false;
        }

        return true;
    }

    /**
     * @return void
     */
    public function setSkipUpdateBC()
    {
        if(!Mage::registry('SKIP_BC_UPDATE_INTEGRATION')){
            Mage::register('SKIP_BC_UPDATE_INTEGRATION', true);
        }
    }

    /**
     * @return string
     */
    public function getFullDate($createdAt)
    {

        $myDate = $createdAt;
        $fullDate = null;

        try {

            $myDateC = Mage::helper('core')->formatDate($myDate, 'medium', true);
            $date = explode(' ', $myDate);
            $time = explode(' ', $myDateC);
            $fullDate = $date[0].'T'.$time[1];
            return $fullDate;

        } catch (Exception $e) {
            return $fullDate;
        }

    }

    /**
     * @return string
     */
    public function enabled()
    {
        return Mage::getStoreConfigFlag('esmart_bc/settings/enabled');
    }

    /**
     * @return string
     */
    public function endpoint()
    {
        return Mage::getStoreConfig('esmart_bc/settings/endpoint');
    }

    /**
     * @return string
     */
    public function taxTreatment()
    {
        return Mage::getStoreConfig('esmart_bc/settings/tax_treatment');
    }

    /**
     * @return string
     */
    public function terminalID()
    {
        return Mage::getStoreConfig('esmart_bc/settings/terminal_id');
    }

    public function getUseSapCodeXreference()
    {
        return Mage::getStoreConfig('esmart_bc/settings/product_code');
    }

    public function getProductIdByAttribute($attributeName, $value)
    {
        $adapter = Mage::getSingleton('core/resource')->getConnection('core_read');
        $attribute = Mage::getSingleton('eav/config')->getCollectionAttribute(Mage_Catalog_Model_Product::ENTITY, $attributeName);
        $attributeId = $attribute->getId();
        $backendTable = $attribute->getBackendTable();
        $select = $adapter->select()
            ->from($backendTable, 'entity_id')
            ->where("value = '{$value}' and attribute_id = {$attributeId} and entity_type_id = 4");
        return $adapter->fetchOne($select);
    }

    public function logDebug($content, $logFileName = null)
    {
        $debug = (bool)Mage::getStoreConfig('esmart_bc/settings/debug');

        if ($debug === false) {
            return false;
        }

        if ($logFileName === null) {
            $logFileName = 'bc_debug.log';
        }

        Mage::log(Zend_Json::encode($content), null, $logFileName);

        return true;
    }
}
