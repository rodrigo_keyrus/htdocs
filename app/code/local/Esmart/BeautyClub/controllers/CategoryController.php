<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category      Esmart
 * @package       Esmart_BeautyClub
 *
 * @copyright     Copyright (c) 2015 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Tiago Silva <tiago.silva@e-smart.com.br>
 */
class Esmart_BeautyClub_CategoryController extends Mage_Core_Controller_Front_Action
{

    /**
     * List Bc Products
     */
    public function listAction()
    {
        $this->loadLayout();

        $block = $this->getLayout()->getBlock('bc_category_list');

        $response['html'] = $block->toHtml();

        $this->getResponse()->setBody(Zend_Json::encode($response));
    }

}
