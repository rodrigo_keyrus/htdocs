<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category      Esmart
 * @package       Esmart_BeautyClub
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Guilherme Miguelete <guilherme.miguelete@e-smart.com.br>
 */

class Esmart_BeautyClub_TestController
{

    /**
     * @return int
     */
    public function getCpf()
    {
        return preg_replace('/[^0-9]/', '', $this->getRequest()->getParam('cpf'));
    }


    /**
     * List Api Tests
     */
    public function indexAction()
    {
        echo '/beautyclub/test/customerLookup/cpf/'. '</br>';
        echo '/beautyclub/test/accountHistory/cpf/'. '</br>';
        echo '/beautyclub/test/promotionAvailability/cpf/'. '</br>';
        exit;
    }

    /**
     * get customerLookup information from API
     */
    public function customerLookupAction()
    {

        if (!$this->_isAllowed()) {
            die('is Not Allowed');
        };

        /** @var Esmart_BeautyClub_Model_Api_LookUp $lookUp */
        $lookUp = Mage::getModel('esmart_bc/api_lookUp');
        $lookUp->setPostData(['cpf' => $this->getCpf()]);

        $data = $lookUp->send();

        echo 'CPF: '.$this->getCpf().'</br>';
        echo '<pre/>';
        Zend_Debug::dump($data);
        exit;

    }


    /**
     * get customerLookup accountHistory from API
     */
    public function accountHistoryAction()
    {

        if (!$this->_isAllowed()) {
            die('is Not Allowed');
        };

        /** @var Esmart_BeautyClub_Model_Api_AccountHistory $lookUp */
        $histories = Mage::getModel('esmart_bc/api_accountHistory');
        $histories->setPostData($this->getCpf());

        $data = $histories->send();

        echo 'CPF: '.$this->getCpf().'</br>';
        echo '<pre/>';
        Zend_Debug::dump($data);
        exit;

    }


    /**
     * get promotionAvailability information from API
     */
    public function promotionAvailabilityAction()
    {

        if (!$this->_isAllowed()) {
            die('is Not Allowed');
        };


        /** @var Mage_Customer_Model_Customer $customer */
        $customer = Mage::getResourceModel('customer/customer_collection')
            ->addAttributeToFilter('cpf', $this->getCpf())
            ->getFirstItem();


        /** @var Esmart_BeautyClub_Model_Api_PromotionAvailability $promotionAvailability */
        $promotionAvailability = Mage::getModel('esmart_bc/api_promotionAvailability');
        $promotionAvailability->setPostData($customer);

        $data = $promotionAvailability->send();

        echo 'CPF: '.$customer->getCpf().'</br>';
        echo '<pre/>';
        Zend_Debug::dump($data);
        exit;

    }


    /** metodo de teste temporario para testes de stock do AX */
    public function axStockAction()
    {

        $sku = $this->getRequest()->getParam('sku', false);

        if (!$sku) {
            die('SKU is Required');
        }

        //$productId = Mage::getModel('catalog/product')->getIdBySku($sku);

        $helper = Mage::helper('esmart_bc');
        if ($helper->getUseSapCodeXreference()) {
            $productId = $helper->getProductIdByAttribute('cod_sap', $sku);
            $product = Mage::getModel('catalog/product')->load($productId);
        } else {
            $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
        }

        /** @var Esmart_Axstock_Model_Resource_Stock_Reserved_Detail $detail */
        $detail        = Mage::getResourceModel('esmart_axstock/stock_reserved_detail');
        $orderReserved = (double) $detail->getReservedQtySum($product->getId());

        echo '<pre>';
        echo 'getReservedQty: '.$orderReserved.'</br>';
        print_r($product->getData());
        echo '</br>';

        $axItem = Mage::getModel('esmart_axstock/item');
        $axItem->setItemId($sku);
        Zend_Debug::dump($axItem->getQtyFromAx());

        die('final');


    }


    /** Metodo de teste para pegar todos os bseller_code das marcas */
    public function manufacturerAction()
    {
        $manufacturers = Mage::getModel('zeon_manufacturer/manufacturer')->getCollection();
        foreach ($manufacturers as $manufacturer) {
            echo '"'.$manufacturer->getData('bseller_code').'",'.'</br>';
        }
        die;
    }



    /**
     * Is allowed to access
     *
     * @return bool
     */
    protected function _isAllowed()
    {

        if (!$this->getCpf()) {
            die('CPF is required');
        }

        return true;
    }


}
