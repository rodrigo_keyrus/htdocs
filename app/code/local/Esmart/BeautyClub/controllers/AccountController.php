<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   Esmart_BeautyClub
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Diogo Santiago <diogo.santiago@b2wdigital.com>
 */
class Esmart_BeautyClub_AccountController extends Mage_Core_Controller_Front_Action
{
    /**
     * Dashboard
     */
    public function dashboardAction()
    {
        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->getLayout()->getBlock('head')->setTitle($this->__('Beauty Club Dashboard'));
        $this->renderLayout();
    }


    /**
     * List Bc Products
     */
    public function listAction()
    {
        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->getLayout()->getBlock('head')->setTitle($this->__('Beauty Club List'));
        $this->renderLayout();
    }

    /**
     * Epic
     */
    public function epicAction()
    {
        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->getLayout()->getBlock('head')->setTitle($this->__('Beauty Club Epic'));
        $this->renderLayout();
    }

    /**
     * Faq
     */
    public function faqAction()
    {
        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->getLayout()->getBlock('head')->setTitle($this->__('Beauty Club Faq'));
        $this->renderLayout();
    }

    /**
     * Regulation
     */
    public function regulationAction()
    {
        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->getLayout()->getBlock('head')->setTitle($this->__('Beauty Club Regulation'));
        $this->renderLayout();
    }

    /**
     * History
     */
    public function historyAction()
    {
        if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
            return $this->customerLogin();
        }

        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->getLayout()->getBlock('head')->setTitle($this->__('Beauty Club History'));
        $this->renderLayout();
    }

    /**
     * Redirect customer login
     */
    protected function customerLogin()
    {
        return $this->_redirect('customer/account/login');
    }

}
