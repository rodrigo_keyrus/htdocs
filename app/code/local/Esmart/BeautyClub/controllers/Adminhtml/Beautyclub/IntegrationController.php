<?php

class Esmart_BeautyClub_Adminhtml_Beautyclub_IntegrationController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->_title($this->__('Customer'))->_title($this->__('BeautyClub - Integration'));
        $this->loadLayout();
        $this->_setActiveMenu('customer/esmart_bc');
        $this->_addContent(
            $this->getLayout()->createBlock('esmart_bc/adminhtml_integration')
        );
        $this->renderLayout();
    }

    /**
     * @return $this
     */
    public function reintegrateAction()
    {
        try {
            echo 'Customer reintegrated...<br/>';

            $email = $this->getRequest()->getParam('customer_email');
            $log = $this->getRequest()->getParam('log_id');

            if (empty($email)) {
                throw new Exception('Email param is empty!');
            }

            $customer = Mage::getModel('customer/customer');
            $customer->setWebsiteId('1');
            $customer->loadByEmail($email);

            if (!$customer->getId()) {
                throw new Exception('Customer not exists!');
            }

            $reintegrate = Mage::getModel('esmart_bc/observer_customer');
            $reintegrate->reintegrateCustomer($customer);

            echo 'Success customer reintegrated...<br/>';

            echo 'Deleting log...<br/>';
            $log = Mage::getModel('esmart_bc/integration')->load($log);
            $log->delete();
            echo 'Success detele log...<br/>';
            
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        return $this;
    }
}