<?php
$installer = $this;

$installer->startSetup();
$connection = $installer->getConnection();

/**
 * Bc Integration table
 */
$table = $connection
    ->newTable($installer->getTable('esmart_bc/integration'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, [
        'identity' => true,
        'unsigned' => true,
        'nullable' => false,
        'primary'  => true
    ])
    ->addColumn('cpf', Varien_Db_Ddl_Table::TYPE_TEXT, null)
    ->addColumn('name', Varien_Db_Ddl_Table::TYPE_TEXT, null)
    ->addColumn('email', Varien_Db_Ddl_Table::TYPE_TEXT, null)
    ->addColumn('errors', Varien_Db_Ddl_Table::TYPE_TEXT, null)
    ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP)
    ->setComment('Beauty Club Integration');

$installer->getConnection()->createTable($table);
$installer->endSetup();