<?php
/**
 * BSeller Platform | B2W - Companhia Digital
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category  Esmart
 * @package   Esmart_BeautyClub
 *
 * @copyright Copyright (c) 2017 B2W Digital - BSeller Platform. (http://www.bseller.com.br)
 *
 * @author   Weverson Cachinsky <weverson.cachinsky@e-smart.com.br>
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

$setup = new Mage_Eav_Model_Entity_Setup('core_setup');

$entityTypeId = $setup->getEntityTypeId('customer');
$attribute = $setup->getAttribute($entityTypeId, 'is_bc');

if (!$attribute) {
    $attributeSetId = $setup->getDefaultAttributeSetId($entityTypeId);
    $attributeGroupId = $setup->getDefaultAttributeGroupId($entityTypeId, $attributeSetId);

    $setup->addAttribute('customer', 'is_bc',  [
        'type'     => 'int',
        'backend'  => '',
        'label'    => 'Is BC',
        'input'    => 'text',
        'source'   => '',
        'visible'  => true,
        'required' => false,
        'default'  => '0',
        'frontend' => '',
        'unique'   => false
    ]);

    $attribute   = Mage::getSingleton("eav/config")->getAttribute('customer', 'is_bc');


    $setup->addAttributeToGroup(
        $entityTypeId,
        $attributeSetId,
        $attributeGroupId,
        'is_bc',
        '999'
    );
}

$installer->endSetup();
