<?php

class Esmart_BeautyClub_Model_Integration extends Mage_Core_Model_Abstract
{
    /**
     * Initialize constructor
     */
    public function _construct()
    {
        parent::_construct();

        $this->_init('esmart_bc/integration');
    }
}