<?php

class Esmart_BeautyClub_Model_Resource_Integration_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    /**
     * Collection constructor
     */
    public function _construct()
    {
        $this->_init('esmart_bc/integration');
    }
}