<?php

class Esmart_BeautyClub_Model_Resource_Integration extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Resource constructor
     */
    public function _construct()
    {
        $this->_init('esmart_bc/integration', 'id');
    }
}