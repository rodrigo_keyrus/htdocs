<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Não edite este arquivo caso você pretenda atualizar este módulo futuramente
 * para novas versões.
 *
 * @category      Esmart
 * @package       Esmart_BeautyClub
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Diogo Santiago <diogo.santiago@e-smart.com.br>
 */
class Esmart_BeautyClub_Model_Resource_History_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    /**
     * Collection constructor
     */
    public function _construct()
    {
        $this->_init('esmart_bc/history');
    }

    /**
     * Add customer filter to collection
     *
     * @param $customerId
     * @return $this
     */
    public function addCustomerToFilter($customerId)
    {
        $this->addFieldToFilter('customer_id', $customerId);

        return $this;
    }

    /**
     * Add status filter to collection
     *
     * @param array $status
     * @return $this
     */
    public function addStatusToFilter(array $status)
    {
        $this->addFieldToFilter('status', array('in' => $status));

        return $this;
    }
}
