<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  Esmart
 * @package   Esmart_BeautyClub
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Diogo Santiago <diogo.santiago@b2wdigital.com>
 */

abstract class Esmart_BeautyClub_Model_Api_Abstract extends Esmart_Soap_Model_Abstract
{
    /**
     * Endpoint uri
     */
    const ENDPOINT_URI = '';

    /**
     * Log configs
     */
    const LOG_FILE = 'beauty-club.log';
    const LOG_IN   = true;

    /**
     * Soap client params
     *
     * @var array
     */
    protected $soapClientParams = [
        'timeout'        => 3000,
        'connecttimeout' => 3000
    ];

    /**
     * Field that the service worked around
     *
     * @var string
     */
    protected $field = 'BeautyClub';

    public $hasErrors = false;

    /**
     * @param $silent
     *
     * @return bool * @throws Exception
     */
    public function send($silent = true)
    {
        if (!Mage::helper('esmart_bc')->enabled()) {
            return [];
        }
        
        $logRequestConfig = Mage::getStoreConfig('esmart_ax/settings/log_request_time');
        
        try {
            $timeStart = microtime(true);
            $response  = $this->getClient()->{static::METHOD}($this->postData);
            $timeEnd   = microtime(true);
            
            if ($logRequestConfig) {
                $logArray[] = [
                    'method'     => static::METHOD,
                    'time'       => ($timeEnd - $timeStart),
                    'request'    => $this->postData,
                    'time-start' => $timeStart,
                    'time-end'   => $timeEnd,
                    'endpoint'   => $this->endpoint()
                ];

                Mage::log(Zend_Json::encode($logArray), null, 'integration_request_time.log');

            }

            $logResponse = (bool)Mage::getStoreConfig('esmart_bc/settings/log_response');

            if ($logResponse) {
                $logArray[] = [
                    'response'    => $response,
                ];

                Mage::log(Zend_Json::encode($logArray), null, 'bc_integration_response.log');
            }

            $response     = json_decode(json_encode($response), true);
            $request      = $this->getClient()->__getRequest();
            $fullResponse = $this->getClient()->__getResponse();

            $this->logIntegration($request, $fullResponse);
        } catch (Exception $e) {
            Mage::logException($e);
            $this->hasErrors = true;
            $response = $e->getMessage();
        }

        return $this->responseTreatment($response, $silent);
    }

    /**
     * Return api endpoint
     *
     * @return string
     */
    public function endpoint()
    {
        return Mage::helper('esmart_bc')->endpoint() . static::ENDPOINT_URI;
    }

    /**
     * Response data treatment
     *
     * @param string $response
     * @param bool $silent
     * @return array
     * @throws Exception
     */
    protected function responseTreatment($response, $silent)
    {
        if (static::METHOD == 'CustomerRegistration' && $this->hasErrors) {
            throw new Exception($response);
            return [];
        }

        if (static::METHOD == 'CustomerProfileUpdate' && $this->hasErrors) {
            throw new Exception($response);
            return [];
        }

        if (!is_array($response)) {
            return $response;
        }

        $code = isset($response['transactionStatus']['code']) ? $response['transactionStatus']['code'] : null;

        /**
         * If we don't have a success
         */
        if ($code && $code != 0) {
            if (!$silent)  throw new Exception($response['transactionStatus']['systemMsg']);
            return [];
        }
        
        unset($response['transactionStatus']);

        return $response;
    }

}
