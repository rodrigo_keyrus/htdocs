<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Não edite este arquivo caso você pretenda atualizar este módulo futuramente
 * para novas versões.
 *
 * @category      Esmart
 * @package       Esmart_BeautyClub
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Diogo Santiago <diogo.santiago@e-smart.com.br>
 */
class Esmart_BeautyClub_Model_Api_PromotionAvailability extends Esmart_BeautyClub_Model_Api_Abstract
{
    const ENDPOINT_URI = 'SephoraCRMService/PromotionAvailability?wsdl';

    const METHOD = 'PromotionAvailability';


    /**
     * @param  Mage_Customer_Model_Customer
     * @return array
     */
    public function setPostData($postData)
    {
        if (!$postData instanceof Mage_Customer_Model_Customer) {
            return parent::setPostData([]);
        }

        /**
         * Just for readability
         * @var $customer Mage_Customer_Model_Customer
         */
        $customer = $postData;
        $cpf = str_pad(preg_replace('/[^0-9]/', '', $customer->getCpf()), 11, '0', STR_PAD_LEFT);

        $data = [
            'cpf'     => $cpf,
            'channel' => 'atg'
        ];

        $this->field = $cpf;
        return parent::setPostData(['userAccount' => $data]);
    }
}
