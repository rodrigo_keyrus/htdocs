<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Não edite este arquivo caso você pretenda atualizar este módulo futuramente
 * para novas versões.
 *
 * @category      Esmart
 * @package       Esmart_BeautyClub
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Diogo Santiago <diogo.santiago@e-smart.com.br>
 */
class Esmart_BeautyClub_Model_Api_ProfileUpdate extends Esmart_BeautyClub_Model_Api_Abstract
{
    const ENDPOINT_URI = 'SephoraCRMService/CustomerProfileUpdate?wsdl';

    const METHOD = 'CustomerProfileUpdate';



    /**
     * @param  Mage_Customer_Model_Customer
     * @return array
     */
    public function setPostData($postData)
    {
        if (!$postData instanceof Mage_Customer_Model_Customer) {
            return parent::setPostData([]);
        }

        /**
         * Just for readability
         * @var $customer Esmart_BeautyClub_Model_Customer
         */
        $customer = $postData;

        $phone = preg_replace('/[^0-9]/', '', $customer->getCellphone());
        $cpf = str_pad(preg_replace('/[^0-9]/', '', $customer->getCpf()), 11, '0', STR_PAD_LEFT);

        $data['userAccount'] = [
            'cpf'               => $cpf,
            'gender'            => $customer->getGender() == 1 ? 'male' : 'female',
            'phone'             => $phone,
            'statusCode'        => $customer->getIsBC() ? 'A' : 'I'
        ];

        if($customer->getName()) {
            $data['userAccount']['firstName'] = $customer->getName();
        }

        // Verify if email exists in BC database
        $bcData = Mage::getModel('esmart_bc/api_lookUp')
            ->setPostData(['email' => $customer->getEmail()])
            ->send();

        // Rules for changing Email: Do not have any account with the email or the account has the same cpf
        if (!is_array($bcData)) {
            $data['userAccount']['email'] = $customer->getEmail();
        }

        if (is_array($bcData) && array_key_exists('userAccount', $bcData) && $cpf == $bcData['userAccount']['cpf']) {
            $data['userAccount']['email'] = $customer->getEmail();
        }

        // In backoffice we can change dob
        if ($customer->getDob()) {
            $dob = substr($customer->getDob(), 0, 10);
            list($birthYear, $birthMonth, $birthDay) = explode('-', $dob);

            $data['userAccount']['birthDay'] = $birthDay;
            $data['userAccount']['birthMonth'] = $birthMonth;
            $data['userAccount']['birthYear'] = $birthYear;
            $data['userAccount']['birthDate'] = $dob . 'T00:00:00';
        }

        /**
         * First we try to get the data from the $customer model.
         *
         * @var Mage_Customer_Model_Address $address
         */
        $address = $customer->getData('updated_customer_address');

        if (!$address) {
            $address = $customer->getDefaultBillingAddress();
        }

        if (!$address) {
            $address = $customer->getAddressesCollection()->getLastItem();
        }

        if ($address) {
            $postcode = preg_replace('/[^0-9]/', '', $address->getPostcode());

            $address = [
                'street'            => $address->getStreet1(),
                'buildingNum'       => substr($address->getStreet2(), 0, 6),
                'complement'        => $address->getStreet3(),
                'district'          => $address->getStreet4(),
                'city'              => $address->getCity(),
                'contactFirstName'  => $address->getName(),
                'county'            => 'Brazil',
                'countryCode'       => 'BRL',
                'stateCode'         => $address->getRegionCode(),
                'state'             => $address->getRegion(),
                'postal'            => $postcode
            ];

            $address = array_map(function($value) {
                return preg_replace('/[^a-zA-Z0-9 ]/', '', Mage::helper('core')->removeAccents($value));
            }, $address);

            /** @var $address Mage_Customer_Model_Address */
            $data['addresses'][] = $address;
            $data['userAccount']['postalCode'] = $postcode;
        }

        $this->field = $cpf;
        return parent::setPostData($data);
    }
}
