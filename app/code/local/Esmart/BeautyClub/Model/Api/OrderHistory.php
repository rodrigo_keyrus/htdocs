<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Não edite este arquivo caso você pretenda atualizar este módulo futuramente
 * para novas versões.
 *
 * @category      Esmart
 * @package       Esmart_BeautyClub
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Diogo Santiago <diogo.santiago@e-smart.com.br>
 */
class Esmart_BeautyClub_Model_Api_OrderHistory extends Esmart_BeautyClub_Model_Api_Abstract
{
    const ENDPOINT_URI = 'SephoraCRMService/OrderHistory?wsdl';

    const METHOD = 'OrderHistory';

    const LOG_IN = false;


    /**
     * @param $cpf
     * @return $this
     */
    public function setPostData($cpf)
    {
        $cpf = str_pad(preg_replace('/[^0-9]/', '', $cpf), 11, '0', STR_PAD_LEFT);
        return parent::setPostData(['userAccount' => ['cpf' => $cpf]]);
    }
}
