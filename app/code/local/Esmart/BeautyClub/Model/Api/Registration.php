<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Não edite este arquivo caso você pretenda atualizar este módulo futuramente
 * para novas versões.
 *
 * @category      Esmart
 * @package       Esmart_BeautyClub
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Diogo Santiago <diogo.santiago@e-smart.com.br>
 */
class Esmart_BeautyClub_Model_Api_Registration extends Esmart_BeautyClub_Model_Api_Abstract
{
    const ENDPOINT_URI = 'SephoraCRMService/CustomerRegistration?wsdl';

    const METHOD = 'CustomerRegistration';


    /**
     * @param  $postData Mage_Customer_Model_Customer
     * @return array
     */
    public function setPostData($postData)
    {
        if (!$postData instanceof Mage_Customer_Model_Customer) {
            return parent::setPostData([]);
        }

        /**
         * Just for readability
         * @var $customer Mage_Customer_Model_Customer
         */
        $customer = $postData;
        $phone = preg_replace('/[^0-9]/', '', $customer->getCellphone());
        $cpf = str_pad(preg_replace('/[^0-9]/', '', $customer->getCpf()), 11, '0', STR_PAD_LEFT);
        $dob = substr($customer->getDob(), 0, 10);
        list($birthYear, $birthMonth, $birthDay) = explode('-', $dob);

        $data['userAccount'] = [
            'cpf'               => $cpf,
            'email'             => $customer->getEmail(),
            'firstName'         => $customer->getName(),
            'gender'            => $customer->getGender() == 1 ? 'male' : 'female',
            'phone'             => $phone,
            'birthDay'          => $birthDay,
            'birthMonth'        => $birthMonth,
            'birthYear'         => $birthYear,
            'birthDate'         => $dob . 'T00:00:00',
            'channel'           => 'COM', // site
            'signupStoreNumber' => '700',
            'statusCode'        => $customer->getBcStatusCode() ?: 'A'
        ];

        $this->field = $cpf;
        return parent::setPostData($data);
    }
}
