<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Não edite este arquivo caso você pretenda atualizar este módulo futuramente
 * para novas versões.
 *
 * @category      Esmart
 * @package       Esmart_BeautyClub
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Diogo Santiago <diogo.santiago@e-smart.com.br>
 */
class Esmart_BeautyClub_Model_Api_OrderCancel extends Esmart_BeautyClub_Model_Api_Abstract
{
    const ENDPOINT_URI = 'SephoraCRMService/OrderCancel?wsdl';

    const METHOD = 'OrderCancel';


    /**
     * @param array $postData
     * @return array
     */
    public function setPostData($postData)
    {
        $this->field = $postData['order']['orderNum'];
        return parent::setPostData($postData);
    }
}
