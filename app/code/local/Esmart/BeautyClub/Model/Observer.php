<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Não edite este arquivo caso você pretenda atualizar este módulo futuramente
 * para novas versões.
 *
 * @category      Esmart
 * @package       Esmart_BeautyClub
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Diogo Santiago <diogo.santiago@e-smart.com.br>
 */
class Esmart_BeautyClub_Model_Observer
{
    use Esmart_Core_Trait;
    


    /**
     * Add Bc tab in admin Customer
     *
     * @param Varien_Event_Observer $observer
     */
    public function addBcTab(Varien_Event_Observer $observer)
    {
        $block = $observer->getEvent()->getBlock();

        if (!$block instanceof Mage_Adminhtml_Block_Customer_Edit_Tabs) {
            return;
        }

        $block->addTab('beauty_club', array(
          'label'     => 'Beauty Club',
          'content'   => $block->getLayout()->createBlock('adminhtml/customer_edit_tab_account')->initForm()->toHtml(),
          'active'    => true
        ));
    }

    /**
     * Add is bc to product grid
     *
     * @param $observer
     */
    public function addIsBcToGrid($observer)
    {
        $grid = $observer->getBlock();

        /**
         * Mage_Adminhtml_Block_Customer_Grid
         */
        if ($grid instanceof Mage_Adminhtml_Block_Catalog_Product_Grid) {

            $grid->addColumnAfter('tax_treatment', [
                'header'    => Mage::helper('esmart_bc')->__('Tax Treatment'),
                'index'     => 'tax_treatment',
                'type'      => 'number',
            ], 'price');
        }
    }

    /**
     * Add is bc to product collection
     *
     * @param $observer
     */
    public function beforeProductCollectionLoad($observer)
    {
        $collection = $observer->getCollection();
        if (!isset($collection)) {
            return;
        }

        /**
         * Mage_Catalog_Model_Resource_Product_Collection
         */
        if ($collection instanceof Mage_Catalog_Model_Resource_Product_Collection) {
            /* @var $collection Mage_Catalog_Model_Resource_Product_Collection*/
            $collection->addAttributeToSelect('tax_treatment');
        }
    }


}
