<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Não edite este arquivo caso você pretenda atualizar este módulo futuramente
 * para novas versões.
 *
 * @category      Esmart
 * @package       Esmart_BeautyClub
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Diogo Santiago <diogo.santiago@e-smart.com.br>
 */

class Esmart_BeautyClub_Model_Customer extends Mage_Customer_Model_Customer
{
    use Esmart_BeautyClub_Trait;

    /**
     * LookUp service
     *
     * @param bool $force
     * @return array
     * @throws Exception
     */
    public function getBcData($force = false)
    {
        $isLoggedIn = Mage::getSingleton('customer/session')->isLoggedIn();

        if (!$force && (!$isLoggedIn || !$this->getData('is_bc'))) {
            return [];
        }

        $cacheKey = 'bc_data_' . $this->getData('cpf');
        $data     = Mage::app()->getCache()->load($cacheKey);

        if (!$force && $data && $isLoggedIn) {
            return Zend_Json::decode($data);
        }

        try {
            /** @var Esmart_BeautyClub_Model_Api_LookUp $lookUp */
            $lookUp = Mage::getModel('esmart_bc/api_lookUp');
            $lookUp->setPostData(['cpf' => $this->getData('cpf')]);

            $data = $lookUp->send();

            if ($lookUp->hasErrors === true) {
                return [];
            }
        } catch (Exception $exception) {
            Mage::logException($exception);
            return [];
        }

        if (!$isLoggedIn) {
            Mage::app()->getCache()->remove($cacheKey);
        }

        /**
         * Guarantees that we do not save in cache refused connections
         */
        if ($isLoggedIn && is_array($data) && !empty($data)) {
            Mage::app()->getCache()->save(Zend_Json::encode($data), $cacheKey);
        }
        // VIB
        $segment = '';
        if (isset($data['currentSegment'])) {
            $segment  = $data['currentSegment'];
        }
        if ($segment == 'VIB' && $this->getGroupId() == 1) {
            $group    = Mage::getModel('customer/group');
            $groupId  = $group->load('VIB', 'customer_group_code')->getId();
            $this->setGroupId($groupId)->save();
        }
        if ($segment != 'VIB') {
            $group    = Mage::getModel('customer/group');
            $groupId  = $group->load('VIB', 'customer_group_code')->getId();
            if ($this->getGroupId() == $groupId) {
                $this->setGroupId(1)->save();
            }
        }
        return $data;
    }

    /**
     * Beauty club customer registration
     *
     * @return array
     * @throws Exception
     */
    public function bcCustomerRegistration()
    {
        /** @var Esmart_BeautyClub_Model_Api_Registration $registration */
        $registration = Mage::getModel('esmart_bc/api_registration');
        $registration->setPostData($this);

        return $registration->send();
    }

    /**
     * Beauty club profile update
     *
     * @return array
     * @throws Exception
     */
    public function bcProfileUpdate()
    {
        /** @var Esmart_BeautyClub_Model_Api_ProfileUpdate $profileUpdate */
        $profileUpdate = Mage::getModel('esmart_bc/api_profileUpdate');
        $profileUpdate->setPostData($this);

        return $profileUpdate->send();
    }

    /**
     * Account History
     *
     * @return array
     * @throws Exception
     */
    public function getBcHistory()
    {
        /** @var Mage_Customer_Model_Session $session */
        $session = Mage::getSingleton('customer/session');

        if (!Mage::app()->getStore()->isAdmin() && (!$session->isLoggedIn() || !$this->getData('is_bc'))) {
            return [];
        }

        $histories = Mage::getModel('esmart_bc/api_accountHistory')->setPostData($this->getData('cpf'))->send();

        if (!isset($histories['accountHistoryLines'])) {
            return [];
        }

        $bcData = $this->getBcData();
        if (isset($bcData['currentSegment']) && $bcData['currentSegment']) {
            return $this->getBcVibHistory($histories);
        }

        if (Mage::app()->getStore()->isAdmin() || isset($histories['accountHistoryLines'])) {
            $histories = $histories['accountHistoryLines'];
        }

        if (isset($histories['creationDate'])) {
            $histories = [$histories];
        }


        $historiesReturn = [];

        foreach ($histories as $value) {
            $comprasElegiveis = isset($value['ytdDelta']) ?
                Mage::helper('core')->currency($value['ytdDelta'], true, false) : '';

            $comprasAcumuladas = isset($value['ytdResult']) ?
                Mage::helper('core')->currency($value['ytdResult'], true, false) : '';

            if (!$value['creationDate']) {
                continue;
            }

            $historiesReturn[] = [
                'data'               => date('d/m/Y', strtotime($value['creationDate'])),
                'lancamento'         => $value['activityDescription'],
                'compras_elegiveis'  => $comprasElegiveis,
                'compras_acumuladas' => $comprasAcumuladas,
                'pontos_utilizados'  => $value['pointsDelta'],
                'saldo_final'        => $value['pointsResult'],
                'vencimento'         => date('d/m/Y', strtotime($value['creationDate'] . ' + 365 day')),
                'ytdResult'          => isset($value['ytdResult']) ? $value['ytdResult'] : 0
            ];
        }

        return $historiesReturn;
    }

    /**
     * Return beauty club history by customer logged
     *
     * @param $histories
     * @return mixed
     */
    public function getBcVibHistory($histories)
    {
        $histories = $histories['accountHistoryLines'];

        array_walk(
            $histories,
            function(&$value) {
                $comprasElegiveis = isset($value['ytdDelta']) ?
                    Mage::helper('core')->currency($value['ytdDelta'], true, false) : '';

                $comprasAcumuladas = isset($value['ytdResult']) ?
                    Mage::helper('core')->currency($value['ytdResult'], true, false) : '';

                $value = [
                    'data'               => date('d/m/Y', strtotime($value['creationDate'])),
                    'lancamento'         => $value['activityDescription'],
                    'compras_elegiveis'  => $comprasElegiveis,
                    'compras_acumuladas' => $comprasAcumuladas,
                    'pontos_utilizados'  => $value['pointsDelta'],
                    'saldo_final'        => $value['pointsResult'],
                    'vencimento'         => date('d/m/Y', strtotime($value['creationDate'] . ' + 365 day')),
                    'ytdResult'          => isset($value['ytdResult']) ? $value['ytdResult'] : 0
                ];
            }
        );

        $size = count($histories);

        for ($i = 0; $i < $size; $i++) {
            if ($histories[$i]['ytdResult'] != 0) {
                continue;
            }

            for ($y = $i + 1; $y < $size; $y++) {
                if (isset($histories[$y]['ytdResult']) && $histories[$y]['ytdResult'] > 0) {
                    $histories[$i]['compras_acumuladas'] =
                        Mage::helper('core')->currency($histories[$y]['ytdResult'], true, false);

                    break;
                }
            }
        }

        return $histories;

    }

    /**
     * Return current points
     *
     * @return number
     */
    public function getCurrentPoints()
    {
        if (!$this->getData('is_bc')) {
            return 0;
        }

        $bcData = $this->getBcData();

        if (!count($bcData)) {
            return 0;
        }

        return $bcData['userAccount']['currentPoints'];
    }

    /**
     * Return the sum of available products on cart that represent the temporary points
     *
     * @return number
     */
    public function getTemporaryPoints()
    {
        if (!$this->getData('is_bc')) {
            return 0;
        }

        return $this->getItemsSumByType(false);
    }

    /**
     * Return the beauty club items currently in cart
     *
     * @return number
     */
    public function getUsedPoints()
    {
        if (!$this->getData('is_bc')) {
            return 0;
        }

        return $this->getItemsSumByType(true);
    }

    /**
     * Return the sum of beauty club products on cart that represent the points to be used
     *
     * @return int|number
     */
    public function getFullPoints()
    {
        $fullPoints = $this->getCurrentPoints() + $this->getTemporaryPoints() - $this->getUsedPoints();

        return $fullPoints;
    }


    /**
     * Check customer is VIB
     * @return bool
     */
    public function isVib()
    {

        /**
         * Array com retorno de informações do BC
         * @var array $bcData
         */
        $bcData = $this->getBcData();
        $isVib = isset($bcData['currentSegment']) ? $bcData['currentSegment'] : false;

        return $isVib;
    }

    /**
     * @param bool $includeTempPoints
     * @return int|string
     */
    public function nextLevelPoints($includeTempPoints = false)
    {
        /**
        * Array com retorno de informações do BC
        * @var array $bcData
        */
        $bcData = $this->getBcData();
        $ytdToNextLevel = isset($bcData['ytdToNextLevel']) ? $bcData['ytdToNextLevel'] : 1200;
        $fullPoints = $this->getFullPoints();
        $partialPoints = $this->getCurrentPoints();

        if ($fullPoints >= $ytdToNextLevel || $partialPoints >= $ytdToNextLevel) {
            return 0;
        }

        $nextLevelPoints = $ytdToNextLevel - $partialPoints;
        if ($includeTempPoints) {
            $nextLevelPoints = $ytdToNextLevel - $fullPoints;
        }

        return $nextLevelPoints;
    }

    /**
     * @param bool $includeTempPoints
     * @return int|string
     */
    public function getPercentageToVib($includeTempPoints = false)
    {
        /** @var int $maxPercentage */
        $maxPercentage = 100;

        /**
         * Array com retorno de informações do BC
         * @var array $bcData
         */
        $bcData = $this->getBcData();

        /**
         * Numero de pontos para de tornat VIB
         * @var integer $pointsToVib
         */
        $pointsToVib = isset($bcData['vibLimit']) ? $bcData['vibLimit'] : 1200;

        /**
         * Numero de pontos efetivos d ocliente
         * @var int $currentPoints
         */
        $currentPoints = $this->getCurrentPoints();

        /**
         * Numero de pontos temporarios do cliente
         * @var int $temporaryPoints
         */
        $temporaryPoints = $this->getTemporaryPoints();

        /**
         * Verifica se o cliente é VIB, caso seja retorna o percentual maximo (FULL)
         */
        if ($this->isVib()) {
            return $maxPercentage;
        }

        /** @var int $customerPoints */
        $customerPoints = $currentPoints;

        if ($includeTempPoints) {
            $customerPoints = $customerPoints + $temporaryPoints;
        }

        $percentage = ($customerPoints * $maxPercentage) / $pointsToVib;

        if ($percentage > $maxPercentage) {
            return $maxPercentage;
        }

        return number_format($percentage, 2);

    }
    

    /**
     * Return object data
     *
     * @param string $key
     * @param null $index
     * @return mixed
     */
    public function getData($key = '', $index = null)
    {
        if ($key === 'bc_data') {
            return $this->getBcData();
        }

        return parent::getData($key, $index);
    }


    /**
     * Processing object before save data
     * validate E-Mail prevent XSS script
     * @return Mage_Customer_Model_Customer
     */
    protected function _beforeSave()
    {
        parent::_beforeSave();

        $email = $this->getEmail();

        if ($email != null) {

            $this->setEmail(filter_var($email, FILTER_VALIDATE_EMAIL));

            /** @var Zend_Validate_EmailAddress $validator */
            $validator = new Zend_Validate_EmailAddress();

            if (!$validator->isValid($email)) {
                $this->setEmail(false);
            }

            /**
             * If any of other character which is not one of the above is found in e-mail then we got an error.
             */
            if (preg_match('/[^0-9a-zA-Z\-\_\.\@]/', $email, $matches)) {
                $this->setEmail(false);
            }

        }

        return $this;

    }

    /**
     * Send email with new account related information
     *
     * @param string $type
     * @param string $backUrl
     * @param string $storeId
     * @throws Mage_Core_Exception
     * @return Mage_Customer_Model_Customer
     */
    public function sendNewAccountEmail($type = 'registered', $backUrl = '', $storeId = '0')
    {
        // Not send new account email if the customer is subscribed in the newsletters
        if(!$this->getIsSubscribed()) {
            parent::sendNewAccountEmail($type, $backUrl, $storeId);
        }
    }
}
