<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Não edite este arquivo caso você pretenda atualizar este módulo futuramente
 * para novas versões.
 *
 * @category      Esmart
 * @package       Esmart_BeautyClub
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Diogo Santiago <diogo.santiago@e-smart.com.br>
 *
 *  _____________________________________
 * | Exemplo simples do YAML esperado:   |
 * |                                     |
 * | products:                           |
 * |   0001:                             |
 * |      name: Produto 1                |
 * |     description: Descricao 1        |
 * |     status: 1                       |
 * |                                     |
 * |   0002:                             |
 * |     name: Produto 2                 |
 * |     description: Descricao 2        |
 * |     status: 1                       |
 * |_____________________________________|
 */
class Esmart_BeautyClub_Model_Import_Product implements Esmart_BeautyClub_Model_Import_Product_Interface
{
    /**
     * Process products from YAML file
     *
     * @return string
     * @throws Exception
     */
    public function process()
    {
        try {
            $yaml = file_get_contents(Mage::getBaseDir() . '/product-import.yml');
        } catch (Exception $e) {
            return $e->getMessage();
        }

        $yaml = Zend_Config_Yaml::decode($yaml);

        if (!isset($yaml['products']) && !is_array($yaml['products'])) {
            return 'The "products" node does not appear to be set in your YAML file!';
        }

        $websiteId = Mage::app()->getWebsite()->getId();
        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

        $helper = Mage::helper('esmart_bc');
        foreach ($yaml['products'] as $sku => $data) {
            if ($helper->getUseSapCodeXreference()) {
                $productId = $helper->getProductIdByAttribute('cod_sap', $this->getCodSap());
                $product = Mage::getModel('catalog/product')->load($productId);
            } else {
                $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
            }

            if ($product) {
                continue;
            }

            $product = Mage::getModel('catalog/product');

            $product->setSku($sku);
            if (isset($data['cod_sap'])) {
                $product->setData('cod_sap', $data['cod_sap']);
            }
            $product->setStatus(Mage_Catalog_Model_Product_Status::STATUS_ENABLED);
            $product->setData('attribute_set_id', 4);
            $product->setData('tax_class_id', 0); // No taxes
            $product->setData('visibility', Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_CATALOG);
            $product->setData('website_ids', [$websiteId]);
            $product->setTypeId('simple');

            $product->setStockData([
                'manage_stock' => 1,
                'use_config_manage_stock' => 0,
                'is_in_stock' => 1,
                'qty' => $data['qty']
            ]);

            $product->addData($data);

            $product->setUrlKey(false);
            $product->setIsMassupdate(true);
            $product->save();
        }

        Mage::app()->setCurrentStore(Mage_Core_Model_App::DISTRO_STORE_ID);

        return 'OK!';
    }
}
