<?php

class Esmart_BeautyClub_Block_Adminhtml_Integration extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'esmart_bc';
        $this->_controller = 'adminhtml_integration';
        $this->_headerText = Mage::helper('esmart_bc')->__('BeautyClub - Integration');

        parent::__construct();
        $this->_removeButton('add');
    }
}