<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  Esmart
 * @package   Esmart_BeautyClub
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Diogo Santiago <diogo.santiago@b2wdigital.com>
 */

class Esmart_BeautyClub_Block_Adminhtml_Customer_Edit_Tab_PointHistory extends Mage_Adminhtml_Block_Widget_Grid
{
    protected $_filterVisibility = false;


    /**
     * Hide grid mass action elements
     *
     * @return Mage_Adminhtml_Block_Customer_Edit_Tab_Reviews
     */
    protected function _prepareMassaction()
    {
        return $this;
    }


    protected function _prepareCollection()
    {
        $collection = new Varien_Data_Collection();
        $histories = Mage::registry('current_customer')->getBcHistory();

        array_walk($histories, function($value) use ($collection){
            $collection->addItem(new Varien_Object($value));
        });

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }


    protected function _prepareColumns()
    {
        $this->addColumn('data', array(
            'header'    => 'Data',
            'align'     => 'right',
            'width'     => '50px',
            'index'     => 'data',
            'filter'    => false
        ));

        $this->addColumn('lancamento', array(
            'header'        => 'Lançamento',
            'align'         => 'left',
            'width'         => '220px',
            'index'         => 'lancamento',
            'filter'        => false
        ));

        $this->addColumn('compras_elegiveis', array(
            'header'        => 'Compras Elegíveis',
            'align'         => 'right',
            'width'         => '100px',
            'index'         => 'compras_elegiveis',
            'filter'        => false
        ));

        $this->addColumn('compras_acumuladas', array(
            'header'        => 'Compras Acumuladas',
            'align'         => 'right',
            'width'         => '100px',
            'index'         => 'compras_acumuladas',
            'filter'        => false
        ));

        $this->addColumn('pontos_utilizados', array(
            'header'        => 'Pontos Utilizados',
            'align'         => 'right',
            'width'         => '100px',
            'index'         => 'pontos_utilizados',
            'filter'        => false
        ));

        $this->addColumn('saldo_final', array(
            'header'        => 'Saldo Final',
            'align'         => 'right',
            'width'         => '100px',
            'index'         => 'saldo_final',
            'filter'        => false
        ));

        $this->addColumn('Vencimento', array(
            'header'        => 'Vencimento',
            'align'         => 'right',
            'width'         => '100px',
            'index'         => 'vencimento',
            'filter'        => false
        ));
    }
}
