<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  Esmart
 * @package   Esmart_BeautyClub
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Diogo Santiago <diogo.santiago@b2wdigital.com>
 */


class Esmart_BeautyClub_Block_Adminhtml_Customer_Edit_Tabs extends Mage_Adminhtml_Block_Customer_Edit_Tabs
{
    protected function _beforeToHtml()
    {
        parent::_beforeToHtml();

        $this->addTabAfter('beauty_club', [
            'label'     => 'Beauty Club',
            'content'   => $this->getLayout()
                                ->createBlock('esmart_bc/adminhtml_customer_edit_tab_pointHistory')->toHtml()
        ], 'giftregistry');

        $this->_updateActiveTab();

        // Forcing Mage_Adminhtml_Block_Widget_Tabs::_beforeToHtml code
        if ($activeTab = $this->getRequest()->getParam('active_tab')) {
            $this->setActiveTab($activeTab);
        } elseif ($activeTabId = Mage::getSingleton('admin/session')->getActiveTabId()) {
            $this->_setActiveTab($activeTabId);
        }

        $_new = array();
        foreach( $this->_tabs  as $key => $tab ) {
            foreach( $this->_tabs  as $k => $t ) {
                if( $t->getAfter() == $key ) {
                    $_new[$key] = $tab;
                    $_new[$k] = $t;
                } else {
                    if( !$tab->getAfter() || !in_array($tab->getAfter(), array_keys($this->_tabs)) ) {
                        $_new[$key] = $tab;
                    }
                }
            }
        }

        $this->_tabs = $_new;
        unset($_new);

        $this->assign('tabs', $this->_tabs);
    }
}
