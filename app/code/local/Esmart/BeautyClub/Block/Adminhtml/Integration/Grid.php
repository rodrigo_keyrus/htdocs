<?php

class Esmart_BeautyClub_Block_Adminhtml_Integration_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Esmart_BeautyClub_Block_Adminhtml_Integration_Grid constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('esmart_bc_integration_grid');
        $this->setDefaultSort('created_at');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(false);
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('esmart_bc/integration')->getCollection();
        $this->setCollection($collection);
        parent::_prepareCollection();

        return $this;
    }

    /**
     * @return $this
     */
    protected function _prepareColumns()
    {
        $helper = Mage::helper('esmart_bc');

        $this->addColumn('id', array(
            'header' => $helper->__('ID'),
            'index'  => 'id',
            'filter' => false
        ));

        $this->addColumn('cpf', array(
            'header' => $helper->__('CPF'),
            'index'  => 'cpf'
        ));

        $this->addColumn('name', array(
            'header' => $helper->__('Name'),
            'index'  => 'name'
        ));

        $this->addColumn('email', array(
            'header' => $helper->__('Email'),
            'index'  => 'email'
        ));

        $this->addColumn('erros', array(
            'header' => $helper->__('Message Error'),
            'index'  => 'errors',
            'width'  => '30%',
            'filter' => false,
            'frame_callback' => array($this, 'decorateMessages')
        ));

        $this->addColumn(
            'created_at', array(
            'header' => $this->__('Date of error'),
            'type'   => 'datetime',
            'index'  => 'created_at'
        ));

        $this->addColumn(
            'action', array(
            'header' => $this->__('Action'),
            'type'   => 'text',
            'filter' => false,
            'index'  => 'action',
            'frame_callback' => array($this, 'reintegrated')
        ));

        return parent::_prepareColumns();
    }

    /**
     * @param $value
     * @param $row
     *
     * @return string
     */
    public function decorateMessages($value, $row)
    {
        if (empty($row)) {
            return '';
        }

        $return = '<a href="#" onclick="$(\'messages_' . $row->getId() . '\').toggle(); return false;">' . $this->__('Show') . '</a>';
        $return .= '<div class="schedule-message" id="messages_' . $row->getId() . '" style="display: none; width: 100%; overflow: auto; font-size: small;"><pre>' . $value . '</pre></div>';

        return $return;
    }

    /**
     * @param $value
     * @param $row
     *
     * @return string
     */
    public function reintegrated($value, $row)
    {
        if (empty($row)) {
            return '';
        }

        $return = '';
        $currentUrl = $this->getUrl(
            '*/beautyclub_integration/reintegrate',
            array(
                'customer_email' => $row->getEmail(),
                'log_id'         => $row->getId()
            )
        );

        if (!empty($row)) {
            $return .= '<a href="' . $currentUrl . '" target="_blank">' . $this->__('Run now') . '</a>';
        }
        return $return;
    }
}