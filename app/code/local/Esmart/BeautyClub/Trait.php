<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Não edite este arquivo caso você pretenda atualizar este módulo futuramente
 * para novas versões.
 *
 * @category      Esmart
 * @package       Esmart_BeautyClub
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Diogo Santiago <diogo.santiago@e-smart.com.br>
 */

trait Esmart_BeautyClub_Trait
{

    /**
     * @param Mage_Catalog_Model_Product $product
     * @return bool
     */
    public function isInCart(Mage_Catalog_Model_Product $product)
    {
        $items = Mage::getSingleton('checkout/session')->getQuote()->getAllVisibleItems();

        foreach ($items as $item) {
            if ($product->getId() == $item->getProductId()) {
                return true;
            }
        }

        return false;
    }

    
    /**
     * Retrives sum of cart items based if its BC or not
     *
     * @param bool $onlyBc
     * @return int|number
     */
    private function getItemsSumByType($onlyBc = true)
    {
        $items = Mage::getSingleton('checkout/session')->getQuote()->getAllItems();
        $sum = [];

        foreach ($items as $item) {
            $isBc = (is_numeric($item->getPoints()) && $item->getPoints() >= 0);

            if ($onlyBc && $isBc) {
                $sum[] = $item->getPoints();
            }

            if (!$onlyBc && !$isBc) {
                $sum[] = $item->getPrice() * $item->getQty();
            }
        }

        $grandTotal = count($sum) > 0 ? ceil(array_sum($sum)) : 0;

        if ($onlyBc) {
            return ceil($grandTotal);
        }

        $total = $grandTotal - ($this->getQuote()->getSubtotal() - $this->getQuote()->getSubtotalWithDiscount());

        return ceil($total);
    }


    /**
     * @return Mage_Sales_Model_Quote
     */
    private function getQuote()
    {
        return Mage::getSingleton('checkout/session')->getQuote();
    }
}
