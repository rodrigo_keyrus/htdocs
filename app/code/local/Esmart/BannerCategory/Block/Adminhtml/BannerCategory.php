<?php
/**
 * BSeller Platform | B2W - Companhia Digital
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category  Esmart
 * @package   Esmart_BannerCategory
 *
 * @copyright Copyright (c) 2018 B2W Digital - BSeller Platform. (http://www.bseller.com.br)
 *
 * @author   Danilo Cavalcanti <danilo.moura@e-smart.com.br>
 */
class Esmart_BannerCategory_Block_Adminhtml_BannerCategory extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        parent::__construct();

        $this->_controller = 'adminhtml_bannerCategory';
        $this->_blockGroup = 'esmart_bannercategory';
        $this->_headerText = $this->__('Manage Banner Category');
    }
}