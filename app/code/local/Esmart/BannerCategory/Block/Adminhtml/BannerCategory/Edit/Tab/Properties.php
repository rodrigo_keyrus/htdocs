<?php
/**
 * BSeller Platform | B2W - Companhia Digital
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category  Esmart
 * @package   Esmart_BannerCategory
 *
 * @copyright Copyright (c) 2018 B2W Digital - BSeller Platform. (http://www.bseller.com.br)
 *
 * @author   Danilo Cavalcanti <danilo.moura@e-smart.com.br>
 */
class Esmart_BannerCategory_Block_Adminhtml_BannerCategory_Edit_Tab_Properties
    extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $htmlIdPrefix = 'bannercategory_properties_';
        $form->setHtmlIdPrefix($htmlIdPrefix);

        $model = Mage::registry('current_bannercategory');

        $fieldset = $form->addFieldset('base_fieldset',
            array('legend'=>Mage::helper('esmart_bannercategory')->__('Banner Properties'))
        );

        if ($model->getBannerId()) {
            $fieldset->addField('banner_id', 'hidden', array(
                'name' => 'banner_id',
            ));
        }

        $fieldset->addField(
            'name',
            'text',
            array(
                'label'     => Mage::helper('esmart_bannercategory')->__('Banner Name'),
                'name'      => 'name',
                'required'  => true
            )
        );

        $fieldset->addField(
            'link',
            'text',
            array(
                'label'     => Mage::helper('esmart_bannercategory')->__('Banner Link'),
                'name'      => 'link',
                'required'  => false,
            )
        );

        $fieldset->addField(
            'image',
            'image',
            array(
                'label'     => Mage::helper('esmart_bannercategory')->__('Banner Image'),
                'name'      => 'image',
                'required'  => true
            )
        );

        $fieldset->addField(
            'status',
            'select',
            array(
                'label'     => Mage::helper('esmart_bannercategory')->__('Active'),
                'name'      => 'status',
                'required'  => true,
                'disabled'  => (bool)$model->getIsReadonly(),
                'options'   => array(
                    1  => Mage::helper('esmart_bannercategory')->__('Yes'),
                    0 => Mage::helper('esmart_bannercategory')->__('No')
                ),
            )
        );

        $dateFormatIso = Mage::app()->getLocale()->getDateTimeFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
        $fieldset->addField(
            'from_date',
            'date',
            array(
                'name'   => 'from_date',
                'label'  => Mage::helper('esmart_bannercategory')->__('From Date'),
                'title'  => Mage::helper('esmart_bannercategory')->__('From Date'),
                'image'  => $this->getSkinUrl('images/grid-cal.gif'),
                'input_format' => $dateFormatIso,
                'format'       => $dateFormatIso,
                'time' => true
            )
        );

        $fieldset->addField(
            'to_date',
            'date',
            array(
                'name'   => 'to_date',
                'label'  => Mage::helper('esmart_bannercategory')->__('From Date'),
                'title'  => Mage::helper('esmart_bannercategory')->__('From Date'),
                'image'  => $this->getSkinUrl('images/grid-cal.gif'),
                'input_format' => $dateFormatIso,
                'format'       => $dateFormatIso,
                'time' => true
            )
        );

        $fieldset->addField(
            'order',
            'text',
            array(
                'label'     => Mage::helper('esmart_bannercategory')->__('Order'),
                'name'      => 'order',
                'required'  => false
            )
        );

        if (!$model->getId()) {
            $model->setData('status', 1);
        }

        $form->setValues($model->getData());
        $this->setForm($form);

        return $this;
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return Mage::helper('esmart_bannercategory')->__('Banner Properties');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return $this->getTabLabel();
    }

    /**
     * Returns status flag about this tab can be showen or not
     *
     * @return true
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden()
    {
        return false;
    }
}