<?php

/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Não edite este arquivo caso você pretenda atualizar este módulo futuramente
 * para novas versões.
 *
 * @category
 * @package
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Gabriel da Gama <gabriel.gama@e-smart.com.br>
 */
class Esmart_Ibge_Model_Ibge extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('esmart_ibge/ibge');
    }
}