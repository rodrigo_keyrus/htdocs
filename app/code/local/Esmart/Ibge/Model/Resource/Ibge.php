<?php

/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Não edite este arquivo caso você pretenda atualizar este módulo futuramente
 * para novas versões.
 *
 * @category
 * @package
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Gabriel da Gama <gabriel.gama@e-smart.com.br>
 */
class Esmart_Ibge_Model_Resource_Ibge extends Mage_Core_Model_Resource_Db_Abstract
{

    protected function _construct()
    {
        $this->_init('esmart_ibge/ibge', 'id');
    }

    /**
     * @param $region
     * @param $city
     * @return string
     */
    public function getIbgeCode($region, $city)
    {
        $helper  = Mage::helper('esmart_ibge');
        $adapter = $this->_getReadAdapter();
        $bind    = array(
            'load_region' => $region,
            'load_city' => $helper->prepareCity($city)
        );
        $select  = $adapter->select()
            ->from($this->getMainTable(), array('ibge_code'))
            ->where('region = :load_region')
            ->where('city = :load_city');

        $response = $adapter->fetchOne($select, $bind);
        
        $ibgeCode = $this->_getUfIbgeCode($region).str_pad($response,5,0,STR_PAD_LEFT);
        
        return $ibgeCode;
    }
    
    /**
     * @param string $uf
     * @return string
     */
    protected function _getUfIbgeCode($uf)
    {
        $ufIbgeArray = [
            'RO' => '11',
            'AC' => '12',
            'AM' => '13',
            'RR' => '14',
            'PA' => '15',
            'AP' => '16',
            'TO' => '17',
            'MA' => '21',
            'PI' => '22',
            'CE' => '23',
            'RN' => '24',
            'PB' => '25',
            'PE' => '26',
            'AL' => '27',
            'SE' => '28',
            'BA' => '29',
            'MG' => '31',
            'ES' => '32',
            'RJ' => '33',
            'SP' => '35',
            'PR' => '41',
            'SC' => '42',
            'RS' => '43',
            'MS' => '50',
            'MT' => '51',
            'GO' => '52',
            'DF' => '53',
        ];
        return $ufIbgeArray[$uf];
    }
}