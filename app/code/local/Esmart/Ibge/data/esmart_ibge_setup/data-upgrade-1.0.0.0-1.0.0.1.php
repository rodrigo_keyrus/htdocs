<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Não edite este arquivo caso você pretenda atualizar este módulo futuramente
 * para novas versões.
 *
 * @category 
 * @package 
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Gabriel da Gama <gabriel.gama@e-smart.com.br>
 */ 
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$csvFile = Mage::getModuleDir('/','Esmart_Ibge').DS.'csv'.DS.'ibge.csv';
$csvObject = new Varien_File_Csv();
$csvData = $csvObject->getData($csvFile);

$installer->getConnection()->beginTransaction();
foreach ($csvData as $lines => $line) {
    $lineData = explode(';',$line[0]);

    if ($lineData[0]=='ibge_code')
        continue;

    $installer->getConnection()->insert(
        'esmart_ibge',
        [
            'region'    => $lineData[2],
            'city'      => ucwords(strtolower($lineData[1])),
            'ibge_code' => $lineData[0]
        ]
    );
}
$installer->getConnection()->commit();

$installer->endSetup();