<?php
class Esmart_ContentSecurityPolicyJs_Helper_Data extends Mage_Core_Helper_Abstract
{
    const CONTENT_SECURITY_POLICY_JS_ENABLED = 'contentsecuritypolicyjs/scripts_src/enabled';
    const CONTENT_SECURITY_POLICY_JS_LIST_SRC = 'contentsecuritypolicyjs/scripts_src/list_src';

    public function isEnabled()
    {
        return Mage::getStoreConfig(self::CONTENT_SECURITY_POLICY_JS_ENABLED);
    }

    public function getListSrc()
    {
        $list = Mage::getStoreConfig(self::CONTENT_SECURITY_POLICY_JS_LIST_SRC);
        $list = preg_replace('/\s+/', '', $list);
        $list = str_replace(',', ' ', $list);

        return $list;
    }
}