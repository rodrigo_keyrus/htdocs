<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category      Esmart
 * @package       Esmart_StoreLocator
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class Esmart_StoreLocator_Helper_Store extends Esmart_StoreLocator_Helper_Data
{
    /**
     * Register key
     *
     * @var string
     */
    protected $_registerKey = 'storelocator_store';

    /**
     * Return object data of register
     *
     * @return Esmart_StoreLocator_Model_Store
     */
    public function getObjectInfo()
    {
        return parent::getObjectInfo();
    }
}
