<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Não edite este arquivo caso você pretenda atualizar este módulo futuramente
 * para novas versões.
 *
 * @category      Esmart
 * @package       Esmart_Core
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Diogo Santiago <diogo.santiago@e-smart.com.br>
 */
abstract class Esmart_Soap_Model_Abstract implements Esmart_Soap_Model_Interface
{
    const METHOD = '';

    const LOG_FILE = 'api-core';

    const LOG_IN = false;

    /**
     * @var string
     */
    protected $field = '';

    /**
     * @var Esmart_Soap_Model_Soap_Client
     */
    protected $client;

    /**
     * @var $postData
     */
    protected $postData = [];

    /**
     * @var array
     */
    protected $soapClientParams = ['timeout' => 5000, 'connecttimeout' => 5000];

    use BSeller_Core_Trait_Data;

    /**
     * @param $silent
     * @return array
     * @throws Exception
     */
    public function send($silent = true, $throwException = false)
    {
        $postData = $this->postData;
        $logRequestCofig = Mage::getStoreConfig('esmart_ax/settings/log_request_time');
        try {
            if ($logRequestCofig) {
                $time_start = microtime(true);
            }
            $response = $this->getClient()->{static::METHOD}($postData);
            if ($logRequestCofig) {
                $time_end = microtime(true);
                $time = $time_end - $time_start;
                $logArray[] = [
                    'method' => static::METHOD,
                    'time' => $time,
                    'request' => $postData,
                    'time-start' => $time_start,
                    'time-end' => $time_end,
                    'endpoint' => $this->endpoint()
                ];
                Mage::log(json_encode($logArray), null, 'integration_request_time.log');
            }
            $request = $this->getClient()->__getRequest();
            $fullResponse = $this->getClient()->__getResponse();
            $this->logIntegration($request, $fullResponse);
        } catch (Exception $e) {
            if (true === $throwException) {
                throw $e;
            }

            $response = $e->getMessage();
            Mage::logException($e);
        }

        return $this->responseTreatment($response, $silent);
    }


    /**
     * @param $postData
     * @return $this
     */
    public function setPostData($postData)
    {
        $this->postData = $postData;
        return $this;
    }


    /**
     * @param string $response
     * @return array
     * @throws Exception
     */
    protected function responseTreatment($response, $silent)
    {
        return $response;
    }

    /**
     * @param bool $renew
     * @return Esmart_Soap_Model_Soap_Client
     */
    protected function getClient($renew = false)
    {
        if ($this->client && ! $renew) {
            return $this->client;
        }

        $context = stream_context_create(['http' => [
            'timeout'        => $this->soapClientParams['timeout']/1000,
            'connecttimeout' => $this->soapClientParams['connecttimeout']/1000
        ]]);

        $this->soapClientParams['stream_context'] = $context;

        $this->client = new Esmart_Soap_Model_Soap_Client($this->endpoint(), $this->soapClientParams);
        $this->client->__setLocation($this->endpoint());

        return $this->client;
    }

    /**
     * @param $request
     * @param $response
     * @return $this
     */
    protected function logIntegration($request, $response)
    {
        //if (static::LOG_IN) {
            $log = Mage::getModel('esmart_soap/log');
            $log->prepareLogMessage(
                $this->field,
                static::METHOD,
                $request,
                $response
            );
            $log->save(static::LOG_FILE);
        //}
        return $this;
    }
}
