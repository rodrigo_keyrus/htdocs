<?php

/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Não edite este arquivo caso você pretenda atualizar este módulo futuramente
 * para novas versões.
 *
 * @category
 * @package
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Gabriel da Gama <gabriel.gama@e-smart.com.br>
 */
class Esmart_Soap_Model_Log
{
    /**
     * LOG SEPARATOR
     * Character used for separate columns at log file
     * @var String
     */
    const LS = '|';

    const XML_LOG_TYPE = 'xml';

    const JSON_LOG_TYPE = 'json';

    /**
     * Name of the log file
     * @var String
     */
    protected $_logFileNames = [
            'xml' => 'esmart_soap_xml.log',
            'json' => 'esmart_soap_json.log'
    ];

    /**
     * Variable used to save a log message
     * @var String
     */
    protected $_logMessage;

    /**
     * @var
     */
    protected $_logType = 'xml';

    /**
     * Save log message
     *
     * @param string $fileName
     * @return $this
     */
    public function save($fileName = null)
    {
        if (strlen($this->_logMessage) > 0) {
            $logFileName = $fileName ? $fileName : $this->_logFileNames[$this->_logType];
            $this->log($this->_logMessage, $logFileName);
            unset($this->_logMessage);
        }

        return $this;
    }

    /**
     * log facility
     *
     * @param string $message
     * @param string $file
     */
    public function log($message, $file = '')
    {
        $loggers = array();
        $level  = Zend_Log::DEBUG;

        if (empty($file)) {
            return;
        }

        try {
            if (!isset($loggers[$file])) {
                $logDir  = Mage::getBaseDir('var') . DS . 'log';
                $logFile = $logDir . DS . $file;

                if (!is_dir($logDir)) {
                    mkdir($logDir);
                    chmod($logDir, 0750);
                }

                if (!file_exists($logFile)) {
                    file_put_contents($logFile, '');
                    chmod($logFile, 0640);
                }

                $format = '%message%' . PHP_EOL;
                $formatter = new Zend_Log_Formatter_Simple($format);
                $writerModel = (string)Mage::getConfig()->getNode('global/log/core/writer_model');
                if (!$writerModel) {
                    $writer = new Zend_Log_Writer_Stream($logFile);
                }
                else {
                    $writer = new $writerModel($logFile);
                }
                $writer->setFormatter($formatter);
                $loggers[$file] = new Zend_Log($writer);
            }

            if (is_array($message) || is_object($message)) {
                $message = print_r($message, true);
            }

            $loggers[$file]->log($message, $level);
        } catch (Exception $e) {
        }
    }


    /**
     * @param string $object
     * @param string $method
     * @param string $request
     * @param string $response
     * @return string
     */
    public function prepareLogMessage($object, $method, $request, $response)
    {
        if ($this->_logType == self::XML_LOG_TYPE) {
            $request = str_replace(PHP_EOL,'', $request);
            $response = str_replace(PHP_EOL,'', $response);

            $request = json_encode($this->xmlToArray(simplexml_load_string($request)));
            $response = json_encode($this->xmlToArray(simplexml_load_string($response)));
        }
        
        $arrayLog = [
            'datetime' => date('Y-m-d H:i:s'),
            'object' => $object,
            'method' => $method,
            'request' => $request,
            'response' => $response,
            'success' => '1'
        ];

        $this->_logMessage = json_encode($arrayLog);

        return $this;
    }

    /**
     * @param string $type
     */
    public function setLogType($type)
    {
        $this->_logType = $type;
    }

    /**
     * @param $xml
     * @param array $options
     * @return array
     */
    public function xmlToArray($xml, $options = array()) {
        $defaults = array(
            'namespaceSeparator' => ':',//you may want this to be something other than a colon
            'attributePrefix' => '@',   //to distinguish between attributes and nodes with the same name
            'alwaysArray' => array(),   //array of xml tag names which should always become arrays
            'autoArray' => true,        //only create arrays for tags which appear more than once
            'textContent' => '$',       //key used for the text content of elements
            'autoText' => true,         //skip textContent key if node has no attributes or child nodes
            'keySearch' => false,       //optional search and replace on tag and attribute names
            'keyReplace' => false       //replace values for above search values (as passed to str_replace())
        );
        $options = array_merge($defaults, $options);
        $namespaces = $xml->getDocNamespaces();
        $namespaces[''] = null; //add base (empty) namespace

        //get attributes from all namespaces
        $attributesArray = array();
        foreach ($namespaces as $prefix => $namespace) {
            foreach ($xml->attributes($namespace) as $attributeName => $attribute) {
                //replace characters in attribute name
                if ($options['keySearch']) $attributeName =
                    str_replace($options['keySearch'], $options['keyReplace'], $attributeName);
                $attributeKey = $options['attributePrefix']
                    . ($prefix ? $prefix . $options['namespaceSeparator'] : '')
                    . $attributeName;
                $attributesArray[$attributeKey] = (string)$attribute;
            }
        }

        //get child nodes from all namespaces
        $tagsArray = array();
        foreach ($namespaces as $prefix => $namespace) {
            foreach ($xml->children($namespace) as $childXml) {
                //recurse into child nodes
                $childArray = $this->xmlToArray($childXml, $options);
                list($childTagName, $childProperties) = each($childArray);

                //replace characters in tag name
                if ($options['keySearch']) $childTagName =
                    str_replace($options['keySearch'], $options['keyReplace'], $childTagName);
                //add namespace prefix, if any
                if ($prefix) $childTagName = $prefix . $options['namespaceSeparator'] . $childTagName;

                if (!isset($tagsArray[$childTagName])) {
                    //only entry with this key
                    //test if tags of this type should always be arrays, no matter the element count
                    $tagsArray[$childTagName] =
                        in_array($childTagName, $options['alwaysArray']) || !$options['autoArray']
                            ? array($childProperties) : $childProperties;
                } elseif (
                    is_array($tagsArray[$childTagName]) && array_keys($tagsArray[$childTagName])
                    === range(0, count($tagsArray[$childTagName]) - 1)
                ) {
                    //key already exists and is integer indexed array
                    $tagsArray[$childTagName][] = $childProperties;
                } else {
                    //key exists so convert to integer indexed array with previous value in position 0
                    $tagsArray[$childTagName] = array($tagsArray[$childTagName], $childProperties);
                }
            }
        }

        //get text content of node
        $textContentArray = array();
        $plainText = trim((string)$xml);
        if ($plainText !== '') $textContentArray[$options['textContent']] = $plainText;

        //stick it all together
        $propertiesArray = !$options['autoText'] || $attributesArray || $tagsArray || ($plainText === '')
            ? array_merge($attributesArray, $tagsArray, $textContentArray) : $plainText;

        //return node as array
        return array(
            $xml->getName() => $propertiesArray
        );
    }
}