<?php
/**
 * BSeller Platform | B2W - Companhia Digital
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category  Esmart
 * @package   Esmart_DeleteRules
 *
 * @copyright Copyright (c) 2018 B2W Digital - BSeller Platform. (http://www.bseller.com.br)
 *
 * @author    Cristhian Andrade <cristhian.novaes@e-smart.com.br>
 *
 * Class Esmart_DeleteRules_Block_Adminhtml_Ruleshistory
 */

class Esmart_DeleteRules_Block_Adminhtml_Ruleshistory extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    /**
     * Esmart_DeleteRules_Block_Adminhtml_Ruleshistory constructor.
     */
    public function __construct()
    {
        $this->_controller = "adminhtml_ruleshistory";
        $this->_blockGroup = "esmart_deleterules";
        $this->_headerText = Mage::helper("esmart_deleterules")->__("Ruleshistory Manager");
        parent::__construct();

        $this->_removeButton('add');

    }

}