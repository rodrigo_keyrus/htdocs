<?php

$installer = $this;

$installer->startSetup();

$table = $installer->getConnection()
    ->newTable($installer->getTable('esmart_deleterules/coupon'))
    ->addColumn('coupon_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Rule Id')
    ->addColumn('rule_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
    ), 'Rule ID')
    ->addColumn('code', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
    ), 'Code')
    ->addColumn('usage_limit', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
    ), 'Usage Limite')
    ->addColumn('usage_per_customer', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
    ), 'Usage Per Customer')
    ->addColumn('times_used', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
    ), 'Times Used')
    ->addColumn('expiration_date', Varien_Db_Ddl_Table::TYPE_DATE, null, array(
    ), 'Expiration Date')
    ->addColumn('is_primary', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable'  => true
    ), 'Is Primary')
    ->addColumn('create_at', Varien_Db_Ddl_Table::TYPE_DATE, null, array(
    ), 'Create At')
    ->addColumn('type', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
    ), 'Type')
    ->addIndex($installer->getIdxName('esmart_deleterules/coupon', array('rule_id', 'create_at', 'expiration_date')),
        array('rule_id', 'create_at', 'expiration_date'))
    ->setComment('Salesrule Coupon');

$installer->getConnection()->createTable($table);

$installer->endSetup();