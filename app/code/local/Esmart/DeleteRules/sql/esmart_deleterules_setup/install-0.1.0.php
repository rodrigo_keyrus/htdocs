<?php

$installer = $this;

$installer->startSetup();

$table = $installer->getConnection()
    ->newTable($installer->getTable('esmart_deleterules/ruleshistory'))
    ->addColumn('rule_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Rule Id')
    ->addColumn('name', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
    ), 'Name')
    ->addColumn('coupon_code', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
    ), 'Coupon Code')
    ->addColumn('description', Varien_Db_Ddl_Table::TYPE_TEXT, '64k', array(
    ), 'Description')
    ->addColumn('from_date', Varien_Db_Ddl_Table::TYPE_DATE, null, array(
    ), 'From Date')
    ->addColumn('to_date', Varien_Db_Ddl_Table::TYPE_DATE, null, array(
    ), 'To Date')
    ->addColumn('deleted_date', Varien_Db_Ddl_Table::TYPE_DATE, null, array(
    ), 'Deleted Date')
    ->addColumn('uses_per_customer', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable'  => false,
        'default'   => '0',
    ), 'Uses Per Customer')
    ->addColumn('customer_group_ids', Varien_Db_Ddl_Table::TYPE_TEXT, '64k', array(
    ), 'Customer Group Ids')
    ->addColumn('is_active', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'nullable'  => false,
        'default'   => '0',
    ), 'Is Active')
    ->addColumn('conditions_serialized', Varien_Db_Ddl_Table::TYPE_TEXT, '2M', array(
    ), 'Conditions Serialized')
    ->addColumn('actions_serialized', Varien_Db_Ddl_Table::TYPE_TEXT, '2M', array(
    ), 'Actions Serialized')
    ->addColumn('stop_rules_processing', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'nullable'  => false,
        'default'   => '1',
    ), 'Stop Rules Processing')
    ->addColumn('is_advanced', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'default'   => '1',
    ), 'Is Advanced')
    ->addColumn('product_ids', Varien_Db_Ddl_Table::TYPE_TEXT, '64k', array(
    ), 'Product Ids')
    ->addColumn('sort_order', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'default'   => '0',
    ), 'Sort Order')
    ->addColumn('simple_action', Varien_Db_Ddl_Table::TYPE_TEXT, 32, array(
    ), 'Simple Action')
    ->addColumn('uses_per_coupon', Varien_Db_Ddl_Table::TYPE_TEXT, 32, array(
    ), 'Uses per coupon')
    ->addColumn('website_ids',  Varien_Db_Ddl_Table::TYPE_TEXT, '2M', array(
    ), 'Website Ids')
    ->addColumn('customer_group_ids',  Varien_Db_Ddl_Table::TYPE_TEXT, '2M', array(
    ), 'Group Ids')
    ->addColumn('gift_sku', Varien_Db_Ddl_Table::TYPE_TEXT, 32, array(
    ), 'Gift Sku')
    ->addColumn('alternative_gifts', Varien_Db_Ddl_Table::TYPE_TEXT, 32, array(
    ), 'Alternative Gifts')
    ->addColumn('store_labels', Varien_Db_Ddl_Table::TYPE_TEXT, '2M', array(
    ), 'Store Labels')
    ->addColumn('discount_amount', Varien_Db_Ddl_Table::TYPE_DECIMAL, array(12,4), array(
        'nullable'  => false,
        'default'   => '0.0000',
    ), 'Discount Amount')
    ->addColumn('discount_qty', Varien_Db_Ddl_Table::TYPE_DECIMAL, array(12,4), array(
    ), 'Discount Qty')
    ->addColumn('discount_step', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
    ), 'Discount Step')
    ->addColumn('simple_free_shipping', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'default'   => '0',
    ), 'Simple Free Shipping')
    ->addColumn('apply_to_shipping', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'default'   => '0',
    ), 'Apply To Shipping')
    ->addColumn('times_used', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'default'   => '0',
    ), 'Times Used')
    ->addColumn('is_rss', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'nullable'  => false,
        'default'   => '0',
    ), 'Is Rss')
    ->addColumn('coupon_type', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'default'   => '1',
    ), 'Coupon Type')
    ->addIndex($installer->getIdxName('esmart_deleterules/ruleshistory', array('is_active', 'sort_order', 'to_date', 'from_date')),
        array('is_active', 'sort_order', 'to_date', 'from_date'))
    ->setComment('Salesrule');

$installer->getConnection()->createTable($table);

$installer->endSetup();