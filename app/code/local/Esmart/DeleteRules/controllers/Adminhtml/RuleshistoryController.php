<?php
/**
 * BSeller Platform | B2W - Companhia Digital
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category  Esmart
 * @package   Esmart_DeleteRules
 *
 * @copyright Copyright (c) 2018 B2W Digital - BSeller Platform. (http://www.bseller.com.br)
 *
 * @author    Cristhian Andrade <cristhian.novaes@e-smart.com.br>
 *
 * Class Esmart_DeleteRules_Adminhtml_RuleshistoryController
 */

class Esmart_DeleteRules_Adminhtml_RuleshistoryController extends Mage_Adminhtml_Controller_Action
{
    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return true;
    }

    /**
     * @return $this
     */
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu("esmart_deleterules/ruleshistory")
            ->_addBreadcrumb(
                Mage::helper("adminhtml")->__("Ruleshistory  Manager"),
                Mage::helper("adminhtml")->__("Ruleshistory Manager")
            );
        return $this;
    }

    /**
     *  Render Layout
     *  @return void
     */
    public function indexAction()
    {
        $this->_title($this->__("DeleteRules"));
        $this->_title($this->__("Manager Ruleshistory"));

        $this->_initAction();
        $this->renderLayout();
    }

    /**
     *  Render Layout
     *  @return void
     */
    public function viewAction()
    {
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel("esmart_deleterules/ruleshistory");

        if (!$id) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('salesrule')->__('This rule no longer exists.')
            );
            $this->_redirect('*/*');
            return;
        }

        $model->load($id);

        if (!$model->getRuleId()) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('salesrule')->__('This rule no longer exists.')
            );
            $this->_redirect('*/*');
            return;
        }

        $model->getConditions()->setJsFormObject('rule_conditions_fieldset');
        $model->getActions()->setJsFormObject('rule_actions_fieldset');

        Mage::register('current_promo_quote_rule_deleted', $model);

        $this->_initAction()->getLayout()->getBlock('adminhtml_ruleshistory_edit');

        $this->renderLayout();
    }
}