<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category      Esmart
 * @package       Esmart_Newsletter
 *
 * @copyright     Copyright (c) 2015 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class Esmart_Newsletter_Model_Observer
{
    /**
     * Add guest columns full name and gender in subscriber grid
     *
     * @param Varien_Event_Observer $observer
     * @return $this
     */
    public function coreBlockAbstractPrepareLayoutBefore(Varien_Event_Observer $observer)
    {
        $block = $observer->getEvent()->getBlock();

        if (!$block instanceof Mage_Adminhtml_Block_Newsletter_Subscriber_Grid) {
            return $this;
        }

        $block->addColumnAfter(
            'full_name',
            array(
                'header' => Mage::helper('esmart_newsletter')->__('Guest Name'),
                'index'  => 'full_name',
                'type'   => 'text'
            ),
            'type'
        );

        $block->addColumnAfter(
            'male',
            array(
                'header'  => Mage::helper('esmart_newsletter')->__('Subscribe Male'),
                'index'   => 'male',
                'type'    => 'options',
                'options' => Mage::getSingleton('adminhtml/system_config_source_yesno')->toArray()
            ),
            'full_name'
        );

        $block->addColumnAfter(
            'female',
            array(
                'header'  => Mage::helper('esmart_newsletter')->__('Subscribe Female'),
                'index'   => 'female',
                'type'    => 'options',
                'options' => Mage::getSingleton('adminhtml/system_config_source_yesno')->toArray()
            ),
            'male'
        );

        return $this;
    }

    /**
     * timestampSave
     * @param $observer newsletter_subscriber_save_after
     * @return $this
     */
    public function timestampSave(Varien_Event_Observer $observer)
    {
        $object = $observer->getEvent()->getSubscriber();
        $email  = $object->getData('subscriber_email');
        if (!$email) {
            return $this;
        }

        $query       = "update newsletter_subscriber set ";
        $currentTime = Mage::getSingleton('core/date')->gmtDate();
        if ((!$object->getId() || $object->isObjectNew())
            && !$object->getCreatedAt()) {
            $query .= " subscriber_created_at='{$currentTime}',";
        }
        $query .= " subscriber_updated_at='{$currentTime}' ";

        $resource        = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        $query          .= " where subscriber_email='{$email}'";
        $writeConnection->query($query);

        return $this;
    }
}
