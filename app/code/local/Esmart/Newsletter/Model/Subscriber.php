<?php
/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category      Esmart
 * @package       Esmart_Newsletter
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class Esmart_Newsletter_Model_Subscriber
{
    /**
     * Save custom information in subscribers table
     *
     * @param string $email
     * @param array $data
     * @return $this
     * @throws Exception
     */
    public function insertInfo($email, $data)
    {
        /** @var Mage_Newsletter_Model_Subscriber $subscriber */
        $subscriber = Mage::getModel('newsletter/subscriber')->loadByEmail($email);
        $now        = Mage::getSingleton('core/date')->gmtDate();

        $subscriber->setData('full_name', (string) $data['full_name']);
        $subscriber->setData('male', (bool) $data['male']);
        $subscriber->setData('female', (bool) $data['female']);
        $subscriber->setData('subscriber_created_at', $now);

        $subscriber->save();

        return $this;
    }
}
