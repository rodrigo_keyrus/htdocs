<?php

class Creativestyle_Richsnippets_Block_Jsonld extends Mage_Core_Block_Template
{

    public function getProduct()
    {
        $product = Mage::registry('current_product');
        return ($product && $product->getEntityId()) ? $product : false;
    }

    public function getAttributeValue($attr)
    {
        $value = null;
        $product = $this->getProduct();
        if($product){
            $type = $product->getResource()->getAttribute($attr)->getFrontendInput();

            if($type == 'text' || $type == 'textarea'){
                $value = $product->getData($attr);
            }elseif($type == 'select'){
                $value = $product->getAttributeText($attr) ? $product->getAttributeText($attr) : '';
            }
        }

        return $value;
    }

    public function getStructuredData()
    {
        // get product
        $product = $this->getProduct();
        $groupedHelper = $this->helper('esmart_catalog/product_grouped');
        // check if $product exists
        if($product) {
            $categoryName = Mage::registry('current_category') ? Mage::registry('current_category')->getName() : '';
            $productId = $product->getEntityId();
            $storeId = Mage::app()->getStore()->getId();
            $currencyCode = Mage::app()->getStore()->getCurrentCurrencyCode();

            $json = array(
                'availability' => $product->isAvailable() ? 'http://schema.org/InStock' : 'http://schema.org/OutOfStock',
                'category' => $categoryName
            );

            // check if reviews are enabled in extension's backend configuration
            $review = Mage::getStoreConfig('richsnippets/general/review');
            if ($review) {
                $reviewSummary = Mage::getModel('review/review/summary');
                $ratingData = Mage::getModel('review/review_summary')->setStoreId($storeId)->load($productId);

                // get reviews collection
                $reviewsCollection = Mage::helper('esmart_ajaxreview')
                    ->getReviewsCollection($productId, 1);

                $reviews = $reviewsCollection->getItems();

                $ratingTotal = 0;
                $reviewData = array();
                if (count($reviews) > 0) {
                    foreach ($reviews as $r) {
                        $ratings = array();
                        foreach ($r->getRatingVotes() as $vote) {
                            $ratings[] = $vote->getPercent();
                        }

                        $avg = array_sum($ratings) / count($ratings);
                        $avg = number_format(floor(($avg / 20) * 2) / 2, 1); // average rating (1-5 range)

                        $ratingTotal = (float)$ratingTotal + (float)$avg;

                        $datePublished = explode(' ', $r->getCreatedAt());

                        // another "mini-array" with schema data
                        $reviewData[] = array(
                            '@type' => 'Review',
                            'author' => $this->htmlEscape($r->getNickname()),
                            'datePublished' => str_replace('/', '-', $datePublished[0]),
                            //'name' => $this->htmlEscape($r->getTitle()),
                            'reviewBody' => nl2br($this->escapeHtml($r->getDetail())),
                            'reviewRating' => array(
                                '@type' => 'Rating',
                                'ratingValue' => $avg
                            )
                        );
                    }
                }

                // let's put review data into $json array
                if (count($reviews) > 0) {
                    $json['reviewCount'] = $reviewSummary->getTotalReviews($product->getId(), true);
                    $json['ratingValue'] = number_format(floor(($ratingData['rating_summary'] / 20) * 2) / 2, 1); // average rating (1-5 range)
                    $json['ratingValue'] = number_format($ratingTotal / count($reviews), 1);
                    $json['review'] = $reviewData;
                }
            }

            if ($product->hasData('description')) {
                $descsnippet = Mage::helper('core/string')->substr(html_entity_decode(strip_tags($product->getDescription())), 0, 2000);
            } elseif($product->hasData('short_description')) {
                $descsnippet = html_entity_decode(strip_tags($product->getShortDescription()));
            } else {
                $descsnippet = $product->getName();
            }

            // Final array with all basic product data
            $data = array(
                '@context' => 'http://schema.org',
                '@type' => 'Product',
                'name' => $product->getName(),
                'sku' => $product->getSku(),
                'image' => $product->getImageUrl(),
                'url' => $product->getProductUrl(),
                //'description' => trim(preg_replace('/\s+/', ' ', $this->stripTags($product->getShortDescription()))),
                'description' => $descsnippet, //use Desc if Shortdesc not work
                'offers' => array(
                    '@type' => 'Offer',
                    'availability' => $json['availability'],
                    'priceCurrency' => $currencyCode,
                    'price' => $groupedHelper->getMinimalFinalPrice($product),
                    'category' => $json['category']
                )
            );

            if (isset($json['category']) && $json['category'] != 'Default Category') {
                $data['category'] = $json['category'];
            }

            $brand = Mage::getResourceModel('zeon_manufacturer/manufacturer_collection')
                ->addFieldToFilter('manufacturer_id',$product->getData('manufacturer'))
                ->fetchItem();
            if ($brand) {
                $data['brand'] = $brand->getData('manufacturer_name');
            }

            $children = $groupedHelper->getChildren($product);

            $seller = new stdClass();
            $seller->{'@type'} = 'Organization';
            $seller->name = 'Sephora';

            if (
//                $product->isGrouped() &&
                count($children) > 0) {

                unset($data['offers']);

                $x = 0;
                foreach($children as $c){
                    $data['offers'][$x]['@type']         = 'Offer';
                    $data['offers'][$x]['sku']           = $c->getSku();
                    $data['offers'][$x]['gtin13']        = $c->getData('gtin');
                    $data['offers'][$x]['priceCurrency'] = $currencyCode;
                    $data['offers'][$x]['price']         = number_format($c->getFinalPrice(),2);
                    $data['offers'][$x]['availability']  = $c->isAvailable() ? 'http://schema.org/InStock' : 'http://schema.org/OutOfStock';
                    $data['offers'][$x]['seller']        = $seller;
                    $x++;
                }
            }

            // if reviews enabled - join it to $data array
            $data['aggregateRating'] = array(
                '@type' => 'AggregateRating',
                'bestRating' => '5',
                'worstRating' => '0',
                'ratingValue' => isset($json['ratingValue'])?(int)$json['ratingValue']:0,
                'reviewCount' => isset($json['reviewCount'])?(int)$json['reviewCount']:0
            );
            $data['review'] = $reviewData;

            // getting all attributes from "Attributes" section of or extension's config area...
            $attributes = Mage::getStoreConfig('richsnippets/attributes');

            // ... and putting them into $data array if they're not empty
            foreach($attributes AS $key => $value){
                if($value){
                    $data[$key] = $this->getAttributeValue($value);
                }
            }

            // return $data table in JSON format
            return '[' . json_encode($data) . ']';
        }

        return null;
    }
}
