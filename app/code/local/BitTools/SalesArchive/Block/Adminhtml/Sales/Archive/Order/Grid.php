<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BitTools
 * @package   BitTools_SalesArchive
 *
 * @copyright Copyright (c) 2019 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Evandro Veloso Gomes <evandro.gomes@e-smart.com.br>
 */

/**
 * Archive orders grid block
 *
 */

class BitTools_SalesArchive_Block_Adminhtml_Sales_Archive_Order_Grid  extends Enterprise_SalesArchive_Block_Adminhtml_Sales_Archive_Order_Grid
{
    /**
     * Prepare collection
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel($this->_getCollectionClass());

        $collection->getSelect()
            ->join(
                ['order' => $collection->getTable('sales/order')],
                'order.entity_id = main_table.entity_id',
                [
                    'discount_amount',
                    'shipping_amount',
                    'voucher_credit_amount_used',
                    'shipping_description',
                    'integration_id',
                    'customer_email',
                    'bseller_skyhub_code',
                    'customer_cpf AS cpf'
                ]
            );

        $collection->getSelect()
            ->join(
                ['order_payment' => $collection->getTable('sales/order_payment')],
                'order_payment.parent_id = main_table.entity_id',
                ['method AS payment_method']
            );

        $this->_collection = $collection;

        return Mage_Adminhtml_Block_Widget_Grid::_prepareCollection();
    }

    /**
     * Prepare columns
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     * @throws Exception
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'real_order_id',
            [
                'header'       => $this->__('Order #'),
                'width'        => '80',
                'type'         => 'text',
                'index'        => 'increment_id',
                'filter_index' => 'main_table.increment_id'
            ]
        );

        $this->addColumn(
            'bseller_skyhub_code',
            [
                'header'       => $this->__('Código Marketplace'),
                'index'        => 'bseller_skyhub_code',
                'filter_index' => 'order.bseller_skyhub_code',
                'type'         => 'text',
                'width'        => '70'
            ]
        );

        if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn(
                'store_id',
                [
                    'header'          => $this->__('Purchased From (Store)'),
                    'index'           => 'store_id',
                    'filter_index'    => 'main_table.store_id',
                    'type'            => 'store',
                    'store_view'      => true,
                    'display_deleted' => true
                ]
            );
        }

        $storesCollection = Mage::getSingleton('bittools_storeregistration/store')
            ->getCollection();

        $stores = [];
        foreach($storesCollection as $store) {
            $stores[$store->getId()] = $store->getName();
        }

        $this->addColumn(
            'bittools_storeregistration_store_id',
            [
                'header'       => $this->__('BitTools Store Registration Store'),
                'index'        => 'bittools_storeregistration_store_id',
                'filter_index' => 'main_table.bittools_storeregistration_store_id',
                'type'         => 'options',
                'options'      => $stores
            ]
        );

        $sellersCollection = Mage::getSingleton('bittools_storeregistration/seller')
            ->getCollection();

        $sellers = [];
        foreach($sellersCollection as $seller) {
            $sellers[$seller->getId()] = $seller->getName();
        }

        $this->addColumn(
            'bittools_storeregistration_seller_id',
            [
                'header'       => $this->__('BitTools Store Registration Seller'),
                'index'        => 'bittools_storeregistration_seller_id',
                'filter_index' => 'main_table.bittools_storeregistration_seller_id',
                'type'         => 'options',
                'options'      => $sellers
            ]
        );

        $this->addColumn(
            'created_at',
            [
                'header'       => $this->__('Purchased On'),
                'index'        => 'created_at',
                'filter_index' => 'main_table.created_at',
                'type'         => 'datetime',
                'width'        => '100'
            ]
        );

        $this->addColumn(
            'billing_name',
            [
                'header'       => $this->__('Bill to Name'),
                'index'        => 'billing_name',
                'filter_index' => 'main_table.billing_name'
            ]
        );

        $this->addColumn(
            'shipping_name',
            [
                'header'       => $this->__('Ship to Name'),
                'index'        => 'shipping_name',
                'filter_index' => 'main_table.shipping_name'
            ]
        );

        $this->addColumn(
            'customer_email',
            [
                'header'       => $this->__('E-Mail'),
                'index'        => 'customer_email',
                'filter_index' => 'order.customer_email'
            ]
        );

        $this->addColumn(
            'cpf',
            [
                'header'                    => $this->__('CPF Number'),
                'index'                     => 'cpf',
                'renderer'                  => 'Esmart_Customer_Block_Adminhtml_Widget_Grid_Column_Renderer_Cpf',
                'filter_condition_callback' => [$this, 'filterCpf'],
                'width'                     => '100'
            ]
        );

        $statuses = Mage::getSingleton('esmart_sales/system_config_source_status')->toArray();

        $this->addColumn(
            'status',
            [
                'header'       => $this->__('Status'),
                'index'        => 'status',
                'filter_index' => 'main_table.status',
                'type'         => 'options',
                'width'        => '70',
                'options'      => $statuses
            ]
        );

        $this->addColumn(
            'erp_status',
            [
                'header'       => $this->__('ERP Status'),
                'index'        => 'order.erp_status',
                'filter' => 'bsellererp_order/adminhtml_sales_order_filter_erpstatuscolumn',
                'renderer' => 'bsellererp_order/adminhtml_sales_order_grid_erpstatuscolumn',
                'type' => 'text'
            ]
        );

        $this->addColumn(
            'shipping_description',
            [
                'header'       => $this->__('Shipping Method'),
                'index'        => 'shipping_description',
                'filter_index' => 'order.shipping_description',
                'type'         => 'text',
                'renderer'     => 'Esmart_Sales_Block_Adminhtml_Widget_Grid_Column_Renderer_ShippingMethod'
            ]
        );

        $this->addColumn(
            'shipping_amount',
            [
                'header'       => $this->__('Shipping Amount'),
                'index'        => 'shipping_amount',
                'filter_index' => 'order.shipping_amount',
                'type'         => 'currency',
                'currency'     => 'order_currency_code',
                'renderer'     => 'Esmart_Sales_Block_Adminhtml_Widget_Grid_Column_Renderer_Price'
            ]
        );

        $this->addColumn(
            'discount_amount',
            [
                'header'       => $this->__('Discount Amount'),
                'index'        => 'discount_amount',
                'filter_index' => 'order.discount_amount',
                'type'         => 'currency',
                'currency'     => 'order_currency_code',
                'renderer'     => 'Esmart_Sales_Block_Adminhtml_Widget_Grid_Column_Renderer_Price'
            ]
        );

        $this->addColumn(
            'voucher_credit_amount_used',
            [
                'header'       => $this->__('Credit'),
                'index'        => 'voucher_credit_amount_used',
                'filter_index' => 'order.voucher_credit_amount_used',
                'type'         => 'currency',
                'currency'     => 'order_currency_code',
                'renderer'     => 'Esmart_Sales_Block_Adminhtml_Widget_Grid_Column_Renderer_Price'
            ]
        );

        $this->addColumn(
            'grand_total',
            [
                'header'       => $this->__('G.T. (Purchased)'),
                'index'        => 'grand_total',
                'filter_index' => 'main_table.grand_total',
                'type'         => 'currency',
                'currency'     => 'order_currency_code'
            ]
        );

        $paymentActiveMethods = Mage::getSingleton('payment/config')->getActiveMethods();

        $payments = [];

        foreach ($paymentActiveMethods as $paymentCode => $paymentModel) {
            $paymentTitle = Mage::getStoreConfig('payment/' . $paymentCode . '/title');

            if (!$paymentTitle) {
                continue;
            }

            $payments[$paymentCode] = $paymentTitle;
        }

        $this->addColumn(
            'payment_method',
            [
                'header'       => $this->__('Payment Method'),
                'index'        => 'payment_method',
                'filter_index' => 'order_payment.method',
                'type'         => 'options',
                'options'      => $payments
            ]
        );

        if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/view')) {
            $this->addColumn(
                'action',
                [
                    'header'  => $this->__('Action'),
                    'width'   => '50',
                    'type'    => 'action',
                    'getter'  => 'getId',
                    'actions' => [
                        [
                            'caption'     => $this->__('View'),
                            'url'         => ['base' => '*/sales_order/view'],
                            'field'       => 'order_id',
                            'data-column' => 'action'
                        ]
                    ],
                    'filter'    => false,
                    'sortable'  => false,
                    'index'     => 'stores',
                    'is_system' => true
                ]
            );
        }

        $this->addRssList('rss/order/new', $this->__('New Order RSS'));
        $this->addExportType('*/*/exportCsv', $this->__('CSV'));
        $this->addExportType('*/*/exportExcel', $this->__('Excel XML'));

        return Mage_Adminhtml_Block_Widget_Grid::_prepareColumns();
    }

    /**
     * Filter CPF on grid collection
     *
     * @param Mage_Customer_Model_Resource_Customer_Collection $collection
     * @param Mage_Adminhtml_Block_Widget_Grid_Column $column
     * @return $this
     */
    protected function filterCpf($collection, $column)
    {
        /** @var Esmart_Adminhtml_Block_Widget_Grid_Column_Filter_Text $filter */
        $filter = $column->getFilter();
        $value  = $filter->getData('value');

        if (empty($value)) {
            return $this;
        }

        $collection->addAttributeToFilter('order.customer_cpf', preg_replace('/[^0-9]/', '', $value));

        return $this;
    }



}