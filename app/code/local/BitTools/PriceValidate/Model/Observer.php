<?php

/**
 * BIT Tools Platform | B2W - Companhia Digital
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category  BitTools
 * @package   BitTools_PriceValidate
 *
 * @copyright Copyright (c) 2018 B2W Digital - BIT Tools.
 *
 * @author    Julio Reis <julio.reis@b2wdigital.com>
 */
class BitTools_PriceValidate_Model_Observer
{
    protected $byPassTaxTreatmentsValidation = [
        2,
        3
    ];

    public function catalogProductSaveBefore($observer)
    {
        if (!Mage::helper('bittools_pricevalidate')->isEnabled()) {
            return;
        }

        //check if this request came from adminhtml backoffice; if not, do nothing;
        if ($actionInstance = Mage::app()->getFrontController()->getAction()) {
            $action = $actionInstance->getFullActionName();
            if ($action == 'adminhtml_catalog_product_save') {
                $product = $observer->getProduct();

                //check if the product is active... if it is, just let it go...
                if ($product->getStatus() == Mage_Catalog_Model_Product_Status::STATUS_DISABLED) {
                    return;
                }

                //check tax_treatment
                if (in_array($product->getData('tax_treatment'), $this->byPassTaxTreatmentsValidation)) {
                    return;
                }

                //verify if price is not zero at bseller, if not, throw an error;
                try {
                    $integrator = Mage::getModel('bittools_pricevalidate/integrator');
                    $integrator->setCustomPathParams([$product->getSku()]);
                    $result = $integrator->init();
                } catch (Exception $ex) {
                    //inform the customer a problem happened checking the product price;
                    Mage::getSingleton('adminhtml/session')->addError(
                        Mage::helper('bittools_pricevalidate')->__("It wasn't possible to check the price of this product at BSeller. (%s)", $ex->getMessage())
                    );
                    $product->setStatus(Mage_Catalog_Model_Product_Status::STATUS_DISABLED);
                    return;
                }
                if (is_numeric($result)) {
                    $price = $result;
                    if (!empty($price)) {
                        return;
                    }
                } else {
                    //couldn't get the price
                    return;
                }
                //manter produto desativado e exibir mensagem do problema
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('bittools_pricevalidate')->__("The product %s wasn't enabled because it's price is 0. Fix it before trying to activate.", $product->getSku())
                );
                $product->setStatus(Mage_Catalog_Model_Product_Status::STATUS_DISABLED);
            }

        }
    }
}
