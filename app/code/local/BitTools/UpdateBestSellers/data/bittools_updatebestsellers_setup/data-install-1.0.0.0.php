<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BitTools
 * @package   BitTools_UpdateBestSellers
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Evandro Veloso Gomes <evandro.gomes@e-smart.com.br>
 */

/* @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;

$installer->startSetup();

$today = time();
$last = $today - (60 * 60 * 24 * 90);

$from = date("Y-m-d H:i:s", $last);
$to = date("Y-m-d H:i:s", $today);

$attributeCode = 'seller_order_quantity';

$products = Mage::getResourceModel('reports/product_collection')
    ->addOrderedQty($from, $to)
    ->setOrder('ordered_qty', 'desc')
    ->joinAttribute($attributeCode, 'catalog_product/'.$attributeCode, 'entity_id', null, 'left');

$products->getSelect()
    ->joinLeft(['relation'=> $this->getTable('catalog/product_relation')], 'relation.child_id = e.entity_id', ['parent_id'])
    ->where('at_seller_order_quantity.value IS NULL');

$parents = [];

// Updating the children products
foreach ($products as $product) {
    if (!array_key_exists($product->getParentId(), $parents)) {
        $parents[$product->getParentId()] = 0;
    }

    $parents[$product->getParentId()] += $product->getOrderedQty();

    Mage::getSingleton('catalog/product_action')->updateAttributes(
        [$product->getId()],
        ['seller_order_quantity' => $product->getOrderedQty()],
        Mage_Core_Model_App::ADMIN_STORE_ID
    );
}

// Updating the parent products
foreach ($parents as $parentId => $qty) {
    Mage::getSingleton('catalog/product_action')->updateAttributes(
        [$parentId],
        ['seller_order_quantity' => $qty],
        Mage_Core_Model_App::ADMIN_STORE_ID
    );

}

$installer->endSetup();
