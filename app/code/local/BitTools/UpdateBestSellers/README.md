# Update Best Sellers

Update the field 'seller_order_quantity' increment it with the sold quantity.
This field is used to order the products by best seller in PDP and list pages.


## Dependencies

                Esmart_Catalog

### Changelog

