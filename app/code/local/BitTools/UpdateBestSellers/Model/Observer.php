<?php

/**
 * BIT Tools Platform | B2W - Companhia Digital
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category  BitTools
 * @package   BitTools_UpdateBestSellers
 *
 * @copyright Copyright (c) 2019 B2W Digital - BIT Tools.
 *
 * @author    Evandro Veloso Gomes <evandro.gomes@b2wdigital.com>
 * @author    Bruno Gemelli <bruno.gemelli@b2wdigital.com>
 */
class BitTools_UpdateBestSellers_Model_Observer
{
    const PATH_CONFIG_IS_ACTIVE = 'bittools_updatebestsellers/settings/active';

    protected $_quantityAttributeId;

    /**
     * BitTools_UpdateBestSellers_Model_Observer constructor.
     */
    public function __construct()
    {
        $this->_quantityAttributeId = Mage::getResourceModel('eav/entity_attribute')
            ->getIdByCode('catalog_product', 'seller_order_quantity');
    }

    private function isActive()
    {
        return Mage::getStoreConfigFlag(self::PATH_CONFIG_IS_ACTIVE);
    }

    /**
     * @param Varien_Event_Observer $observer
     */
    public function updateBestSellers(Varien_Event_Observer $observer)
    {
        if (!$this->isActive()) {
            return $this;
        }

        /** @var array $items */
        $items = $observer->getEvent()->getOrder()->getAllItems();

        /** @var Mage_Sales_Model_Order_Item $item */
        foreach ($items as $item) {

            /**
             * skip sample and gift products
             */
            if (!$this->_validateTaxTreatment($item->getProduct())) {
                continue;
            }

            /**
             * Update child product attribute
             */
            $this->_updateItemQuantity(
                $item->getProductId(),
                $this->_getCurrentProductQtySold($item->getProductId()),
                $item->getQtyOrdered()
            );

            /** @var int $parentId */
            $parentId = Mage::helper('esmart_catalog')->getParentId($item->getProductId());

            if (!$parentId) {
                continue;
            }

            /**
             * Update parent product attribute
             */
            $this->_updateItemQuantity(
                $parentId,
                $this->_getCurrentProductQtySold($parentId),
                $item->getQtyOrdered()
            );

        }
    }


    /**
     * Validate product 'tax_treatment' attribute.
     * If tax treatment different than 1, return false;
     * 1 - normal products | 2 - sample products | 3 - gift products
     *
     * @param Mage_Catalog_Model_Product $product
     *
     * @return bool
     */
    protected function _validateTaxTreatment($product)
    {
        if (!$product->getData('tax_treatment') || $product->getData('tax_treatment') == 1) {
            return true;
        }

        return false;
    }


    /**
     * Update product 'seller_order_quantity' attribute value
     * Used for child and parent products
     *
     * @param int $productId
     * @param int $currentQty
     * @param int $qtyOrdered
     */
    protected function _updateItemQuantity($productId, $currentQty, $qtyOrdered)
    {
        if (!$qtyOrdered) {
            return;
        }

        try {
            $product = Mage::getModel('catalog/product')->setId($productId);
            $product->setSellerOrderQuantity($currentQty + $qtyOrdered);
            $product->getResource()->saveAttribute($product, 'seller_order_quantity');
        } catch (Exception $e) {
            Mage::logException($e);
        }
    }


    /**
     * Get current 'seller_order_quantity' attribute value for product
     *
     * @param int $productId
     * @param int $attributeId
     *
     * @return int|null
     */
    protected function _getCurrentProductQtySold($productId)
    {
        if (!$this->_quantityAttributeId) {
            return null;
        }

        /** @var Mage_Catalog_Model_Product $productModel */
        $productModel = Mage::getModel('catalog/product');

        $currentQty = $productModel->getResource()->getAttributeRawValue(
            $productId,
            $this->_quantityAttributeId,
            Mage_Core_Model_App::ADMIN_STORE_ID
        );

        return (int)$currentQty;
    }
}
