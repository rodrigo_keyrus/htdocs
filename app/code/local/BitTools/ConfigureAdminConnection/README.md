# Configure Admin Connection

Allows to change the MySQL connection used in the admin grids to one already configured in the Magento settings.


## Dependencies

                Mage_Core

### Changelog

