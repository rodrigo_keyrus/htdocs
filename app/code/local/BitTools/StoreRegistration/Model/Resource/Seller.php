<?php
/**
 * BIT Tools | B2W - Companhia Digital
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category  BitTools
 * @package   BitTools_StoreRegistration
 *
 * @copyright Copyright (c) 2019 B2W Digital - BIT Tools.
 *
 * @author    Evandro Veloso Gomes <evandro.gomes@b2wdigital.com>
 */

class BitTools_StoreRegistration_Model_Resource_Seller
    extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('bittools_storeregistration/seller', 'id');
    }

    /**
     * Load seller by username
     *
     * @param BitTools_StoreRegistration_Model_Seller $seller
     * @param string $username
     * @return bool
     */
    public function loadByUsername(BitTools_StoreRegistration_Model_Seller $seller, $username)
    {
        if (!$username) {
            return false;
        }

        $resource = Mage::getSingleton('core/resource');
        $read = $this->_getReadAdapter();

        $select = $read->select()->from($this->getMainTable())
            ->join(
                array("store" => $resource->getTableName('bittools_storeregistration/store')),
                $this->getMainTable() . ".store_id = store.id",
                array()
            )
            ->where($this->getMainTable() . '.username = ?', $username)
            ->where($this->getMainTable() . '.is_active = ?', 1)
            ->where('store.magento_store_id = ?', Mage::app()->getStore()->getId())
            ->where('store.is_active = ?', 1);

        $data = $read->fetchRow($select);
        if (!$data) {
            return false;
        }

        $seller->setData($data);

        $this->_afterLoad($seller);

        return true;
    }
}