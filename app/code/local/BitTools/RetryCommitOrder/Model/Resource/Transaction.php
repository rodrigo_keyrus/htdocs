<?php
/**
 * BIT Tools Platform | B2W - Companhia Digital
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category  BitTools
 * @package   BitTools_RetryCommitOrder
 *
 * @copyright Copyright (c) 2019 B2W Digital - BIT Tools.
 *
 * @author    Evandro Veloso Gomes <evandro.gomes@b2wdigital.com>
 */

class BitTools_RetryCommitOrder_Model_Resource_Transaction extends Mage_Core_Model_Resource_Transaction
{
    /**
     * Initialize objects save transaction
     *
     * @return Mage_Core_Model_Resource_Transaction
     * @throws Exception
     */
    public function save()
    {
        $this->_startTransaction();
        $error     = false;

        try {
            foreach ($this->_objects as $object) {
                 if ($object instanceof Mage_Sales_Model_Order) {
                    try {
                        $object->save();
                    } catch (Exception $e) {
                        $helperLog = new Uecommerce_Mundipagg_Helper_Log(__METHOD__);
                        $helperLog->error("Order retried: " . $object->getIncrementId(), true);

                        $object->save();
                    }
                } else {
                    $object->save();
                }
            }
        } catch (Exception $e) {
            $error = $e;
        }

        if ($error === false) {
            try {
                $this->_runCallbacks();
            } catch (Exception $e) {
                $error = $e;
            }
        }

        if ($error) {
            $this->_rollbackTransaction();
            throw $error;
        } else {
            $this->_commitTransaction();
        }

        return $this;
    }
}