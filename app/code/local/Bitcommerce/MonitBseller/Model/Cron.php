<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   Bitcommerce_MonitBseller
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Anderson Cardoso <anderson.cardoso@e-smart.com.br>
 */

class Bitcommerce_MonitBseller_Model_Cron extends Mage_Core_Model_Abstract
{

    /**
     * @var Bitcommerce_MonitBseller_Model_Process_Order|false|Mage_Core_Model_Abstract
     */
    public $process;

    /**
     * @var Bitcommerce_MonitBseller_Helper_Data|Mage_Core_Helper_Abstract
     */
    public $helper;

    /**
     * Order blocked
     *
     * @const string
     */
    const STATUS = 'blocked';

    /**
     * Order pending of the monitoring
     *
     * @const string
     */
    const MONITORING = 'pending';

    /**
     * Limit from order collection
     *
     * @const string
     */
    const LIMIT = '100';

    /**
     * Field that skip in the collection
     *
     * @const string
     */
    const NIN = 'canceled';

    public function __construct()
    {
        parent::_construct();
        $this->helper  = Mage::helper('bitcommerce_monitbseller');
        $this->process = Mage::getModel('bitcommerce_monitbseller/process_order');
    }

    /**@todo save orders with payment trouble
     * @return $this
     */
    public function loadOrders()
    {
        if (!$this->helper->isActive()) {
            return $this;
        }

        /** @var Mage_Sales_Model_Resource_Order_Collection $monitoring */
        $monitoring = Mage::getResourceModel('sales/order_collection')
            ->addFieldToFilter('erp_status', self::STATUS)
            ->addFieldToFilter('bseller_status', self::MONITORING)
            ->addAttributeToFilter('created_at', array(
                'from' => date('Y-m-d H:i:s', strtotime('-1 day')),
                'to'   => date('Y-m-d H:i:s'),
                'date' => true,
            ))
            ->addFieldToFilter('status', array('nin' => array(self::NIN)));

        $monitoring->getSelect()->limit(self::LIMIT);

        if (count($monitoring) <= 0) {
            return $this;
        }

        $listSend = [];
        foreach ($monitoring as $order) {
            $listSend[] =  $order->getIncrementId();
        }

        $this->process->sendTransactionEmail($listSend);

        return $this;
    }
}
