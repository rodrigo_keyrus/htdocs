<?php

/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   Bitcommerce_MonitBseller
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Anderson Cardoso <anderson.cardoso@e-smart.com.br>
 */

class Bitcommerce_MonitBseller_Model_Process_Order extends Mage_Core_Model_Abstract
{
    /**
     * @var Bitcommerce_MonitBseller_Helper_Data|Mage_Core_Helper_Abstract
     */
    private $helper;

    public function __construct()
    {
        parent::_construct();
        $this->helper  = Mage::helper('bitcommerce_monitbseller');
    }

    /**
     * @param $listSend
     */
    public function sendTransactionEmail($listSend)
    {
        $this->helper->log('init send email', 'sendEmail');

        $html = 'Follow the orders with integrations trouble:<br /><ul>' . $this->subject($listSend) . '</ul>';

        $mail = Mage::getModel('core/email');
        $mail->setSubject('BSeller Monitoring');
        $mail->setFromEmail($this->helper->fromEmail());
        $mail->setFromName('BSeller Monitoring');
        $mail->setType('html');
        $mail->setBody($html);
        try {
            foreach ($this->helper->emails() as $email) {
                $mail->setToEmail($email);
                $mail->send();
            }

            $this->alterStatusOrder($listSend);
            $this->helper->log('email send with success' . $html, 'sendEmail');
        } catch (Exception $e) {
            $this->helper->log('problem whit send email' . $html, 'sendEmail');
        }
    }

    /**
     * @param $listOrders
     * @return null|string
     */
    public function subject($listOrders)
    {
        $html = null;
        foreach ($listOrders as $orders) {
            $html .= "<li>$orders</li>";
        }
        return $html;
    }

    /**
     * @param $listSend
     * @return $this
     * @throws Exception
     */
    public function alterStatusOrder($listSend)
    {
        foreach ($listSend as $orders) {
            $order = Mage::getModel('sales/order')->loadByIncrementId($orders);
            $order->setData('bseller_status', 'checked');
            $order->save();
        }
        return $this;
    }
}
