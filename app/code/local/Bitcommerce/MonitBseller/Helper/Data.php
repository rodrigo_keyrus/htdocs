<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   Default (Template) Project
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Anderson Cardoso <anderson.cardoso@e-smart.com.br>
 */

class Bitcommerce_MonitBseller_Helper_Data extends Mage_Core_Helper_Abstract
{
    /** Method used to easily generate logs
     * @param $message
     * @param $method
     */
    public function log($message, $method)
    {
        Mage::log($message, null, 'MonitoringBseller_'.$method.'.log');
    }

    /**
     * if module is active
     * @return bool
     */
    public function isActive()
    {
        $active = (bool)Mage::getStoreConfig('bitcommerce_monitbseller/settings/active');

        return ($this->isModuleEnabled() && $active);
    }

    /**
     * validate if service is active
     * @return bool
     */
    public function serviceIsActive()
    {
        return (bool)Mage::getStoreConfig('bitcommerce_monitbseller/settings/service_mode');
    }

    /**
     * validate if service is active
     * @return bool
     */
    public function emails()
    {
        return explode(',', Mage::getStoreConfig('bitcommerce_monitbseller/settings/emails'));
    }

    /**
     * validate if service is active
     * @return bool
     */
    public function fromEmail()
    {
        return Mage::getStoreConfig('bitcommerce_monitbseller/settings/fromemail');
    }
}