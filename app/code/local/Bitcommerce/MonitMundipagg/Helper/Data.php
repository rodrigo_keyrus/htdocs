<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_SephoraInventory
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Anderson Cardoso <anderson.cardoso@e-smart.com.br>
 */

class Bitcommerce_MonitMundipagg_Helper_Data extends Mage_Core_Helper_Abstract
{

    /** Method used to easily generate logs
     * @param $message
     * @param $method
     */
    public function log($message, $method)
    {
        Mage::log($message, null, 'MonitoringMundipagg_'.$method.'.log');
    }

    /**
     * if module is active
     * @return bool
     */
    public function isActive()
    {
        $active = (bool)Mage::getStoreConfig('bitcommerce_monitmundipagg/settings/active');

        return ($active && $this->isModuleEnabled());
    }

    /**
     * validate if service is active
     * @return bool
     */
    public function serviceIsActive()
    {
        return (bool)Mage::getStoreConfig('bitcommerce_monitmundipagg/settings/service_mode');
    }

    /**
     * return token
     * @return bool
     */
    public function getMerchantKey()
    {
        return Mage::getStoreConfig('bitcommerce_monitmundipagg/settings/merchantkey');
    }

    /**
     * return entpoint
     * @return bool
     */
    public function getEndpoint()
    {
        return Mage::getStoreConfig('bitcommerce_monitmundipagg/settings/endpoint');
    }

    /**
     * return period
     * @return bool
     */
    public function getPeriod()
    {
        return Mage::getStoreConfig('bitcommerce_monitmundipagg/settings/period');
    }

    /**
     * return ignored days
     * @return bool
     */
    public function getIgnoredDays()
    {
        return Mage::getStoreConfig('bitcommerce_monitmundipagg/settings/days');
    }




}
