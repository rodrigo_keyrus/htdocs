<?php

/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  Bitcommerce
 * @package   Bitcommerce_MonitMundipagg
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Anderson Cardoso <anderson.cardoso@e-smart.com.br>
 */

class Bitcommerce_MonitMundipagg_Adminhtml_MonitoringController extends Mage_Adminhtml_Controller_Action
{

    public function indexAction()
    {
        $this->_title($this->__('Sales'))->_title($this->__('Monitored Orders'));
        $this->loadLayout();
        $this->_setActiveMenu('sales/sales');
        $this->_addContent($this->getLayout()->createBlock('bitcommerce_monitmundipagg/adminhtml_sales_order'));
        $this->renderLayout();
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('bitcommerce_monitmundipagg/adminhtml_sales_order_grid')->toHtml()
        );
    }


    public function sendPostMundipaggAction()
    {
        $orderIds = $this->getRequest()->getParam('id');
        if (!is_array($orderIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('tax')->__('Please select the order(s).'));
            $this->_redirect('*/*/index');
        }

        $resendPost = Mage::getModel('bitcommerce_monitmundipagg/postMundipagg_postMundipagg');

        try {
            $monitoring = Mage::getResourceModel('bitcommerce_monitmundipagg/monitoring_register_collection')
                ->addFieldToFilter('order_id', array('in'=> $orderIds));

            foreach ($monitoring as $order) {
                $resendPost->validadePost($order->getData());
            }

            Mage::getSingleton('adminhtml/session')
                ->addSuccess(Mage::helper('tax')->__('Total of %d record(s) were resend.', count($orderIds)));
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }

        $this->_redirect('*/*/index');
    }
}