<?php

/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   Bitcommerce_MonitMundipagg
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Anderson Cardoso <anderson.cardoso@e-smart.com.br>
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$tableName = $installer->getTable('bitcommerce_monitmundipagg/monitoring_register');
if (!$installer->getConnection()->isTableExists($tableName)) {
    try {
        $newTable = $installer->getConnection()->newTable($tableName)
            ->addColumn(
                'id',
                Varien_Db_Ddl_Table::TYPE_INTEGER,
                null,
                [
                    'unsigned' => true,
                    'nullable' => false,
                    'primary'  => true,
                    'identity' => true,
                ],
                'Primary key - ID'
            )
            ->addColumn(
                'order_id',
                Varien_Db_Ddl_Table::TYPE_INTEGER,
                null,
                [
                    'unsigned' => true,
                    'nullable' => false,
                ],
                'Order ID'
            )
            ->addColumn(
                'created_at',
                Varien_Db_Ddl_Table::TYPE_DATETIME,
                null,
                [
                    'nullable' => false,
                ],
                'Date of register'
            )
            ->addColumn(
                'updated_at',
                Varien_Db_Ddl_Table::TYPE_DATETIME,
                null,
                [
                    'nullable' => false,
                ],
                'updated of register'
            )
            ->addColumn(
                'qty_retry',
                Varien_Db_Ddl_Table::TYPE_INTEGER,
                null,
                [
                    'unsigned' => true,
                    'nullable' => false,
                ],
                'item id'
            )
            ->addColumn(
                'additional_information',
                Varien_Db_Ddl_Table::TYPE_VARCHAR,
                null,
                [
                    'unsigned' => true,
                    'nullable' => true,
                ],
                'xml for updated '
            )
            ->addColumn(
                'status_mundipagg',
                Varien_Db_Ddl_Table::TYPE_VARCHAR,
                null,
                [
                    'unsigned' => true,
                    'nullable' => true,
                ],
                'xml for updated '
            )
            ->addColumn(
                'xml_retry',
                Varien_Db_Ddl_Table::TYPE_VARCHAR,
                null,
                [
                    'unsigned' => true,
                    'nullable' => true,
                ],
                'xml for updated '
            )
            ->addColumn(
                'order_number',
                Varien_Db_Ddl_Table::TYPE_VARCHAR,
                null,
                [
                    'unsigned' => true,
                    'nullable' => true,
                ],
                'xml for updated '
            )
            ->addColumn(
                'response_mundipagg',
                Varien_Db_Ddl_Table::TYPE_VARCHAR,
                null,
                [
                    'unsigned' => true,
                    'nullable' => true,
                ],
                'original Value before low inventory'
            );
        $installer->getConnection()->createTable($newTable);
    } catch (Zend_Db_Exception $e) {
        Mage::log($e->getMessage(), '', 'bsellerInventoryQueueHistory.log');

    }
}
$installer->endSetup();

$setup = Mage::getResourceModel('sales/setup', 'default_setup');

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

/**
 * Add attribute for result integration
 */
$setup->addAttribute('order', 'monit_status', array(
    'type' => 'varchar',
    'default' => 'pending',
));

$installer->endSetup();