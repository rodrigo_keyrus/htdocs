<?php

/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   Bitcommerce_MonitMundipagg
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Anderson Cardoso <anderson.cardoso@e-smart.com.br>
 */

class Bitcommerce_MonitMundipagg_Model_Orders_Orders extends Mage_Core_Model_Abstract
{
    /**
     * @var Bitcommerce_MonitMundipagg_Helper_Data
     */
    public $log;

    /**
     * @var false|Mage_Core_Model_Abstract
     */
    public $connect;

    /**
     * @var Bitcommerce_MonitMundipagg_Helper_Data|Mage_Core_Helper_Abstract
     */
    public $helper;

    /**
     * Order created
     *
     * @const string
     */
    const STATUS = 'pending';

    /**
     * Method payment
     *
     * @const string
     */
    const METHOD_PAYMENT = 'mundipagg_creditcard';

    /**
     * For consult between date from orders
     *
     * @const string
     */
    const PERIOD = '50';

    /**
     * Limit from order collection
     *
     * @const string
     */
    const LIMIT = '10';

    /**
     * Field that skip in the collection
     *
     * @const string
     */
    const NIN = 'canceled';

    /**
     * Bitcommerce_MonitMundipagg_Model_Orders_Orders constructor
     */
    public function __construct()
    {
        parent::_construct();
        $this->helper  = Mage::helper('bitcommerce_monitmundipagg');
        $this->connect = Mage::getModel('bitcommerce_monitmundipagg/connect_connect_connect');
    }

    /**
     * @return Mage_Sales_Model_Resource_Order_Collection|object
     */
    public function getOrders()
    {
        /** @var Mage_Sales_Model_Resource_Order_Collection $orders */
        $orders = Mage::getResourceModel('sales/order_collection');

        $orders->getSelect()
            ->join(
                array('sfop' => $orders->getTable('sales/order_payment')),
                "sfop.parent_id = main_table.entity_id",
                array('method' => 'sfop.method')
            );

        $orders->addFieldToFilter('erp_status', self::STATUS)
            ->addFieldToFilter('monit_status', self::STATUS)
            ->addFieldToFilter('sfop.method', self::METHOD_PAYMENT)
            ->addAttributeToFilter('created_at', array(
                'from' => date('Y-m-d', strtotime('-' . self::PERIOD . " days")),
                'to'   => date('Y-m-d', strtotime('-' . $this->helper->getIgnoredDays() . " days")),
                'date' => true,
            ))
            ->addFieldToFilter('status', array('nin' => array(self::NIN)));

        return $orders;
    }

    /**@todo save orders with payment trouble
     * @return $this
     */
    public function loadOrders()
    {
        if (!$this->helper->isActive()) {
            return $this;
        }

        $orders = $this->getOrders();
        if (count($orders) <= 0) {
            return $this;
        }

        foreach ($orders as $order) {

            /** @var Bitcommerce_MonitMundipagg_Model_Monitoring_Register $monit */
            $monit = $this->createdMonitoring($order->getId(), $order->getIncrementId());

            $this->connect->connectMundipagg($monit);
        }

        return $this;
    }

    /**
     * @todo save the request in the table for future monitoring
     * @param $orderId
     * @return $this
     */
    public function createdMonitoring($orderId, $incrementId)
    {
        if (!$orderId) {
            return $this;
        }

        try {

            $this->helper->log('init save the order with payment trouble '. $orderId, 'Trouble_orders');

            /** @var Bitcommerce_MonitMundipagg_Model_Monitoring_Register $monit */
            $monit = Mage::getModel('bitcommerce_monitmundipagg/monitoring_register')->load($orderId, 'order_id');

            if ($monit->getOrderId()) {
                return $monit;
            }

            $monit->setOrderId($orderId);
            $monit->setCreatedAt(date('Y-m-d H:i:s'));
            $monit->setUpdatedAt(date('Y-m-d H:i:s'));
            $monit->setQtyRetry(0);
            $monit->setAdditionalInformation(0);
            $monit->setAdditionalInformation(0);
            $monit->setResponseMundipagg(0);
            $monit->setOrderNumber($incrementId);
            $monit->save();

            $this->helper->log('finish the order with payment trouble '. $orderId, 'Trouble_orders');
        } catch (Exception $e) {
            $this->helper
                ->log('No possible save the order with payment trouble'
                    . $orderId . $e->getMessage(), 'Trouble_orders');
        }

        return $monit;
    }
}