<?php

/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   Bitcommerce_MonitMundipagg
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Anderson Cardoso <anderson.cardoso@e-smart.com.br>
 */

class Bitcommerce_MonitMundipagg_Model_PostMundipagg_PostMundipagg extends Bitcommerce_MonitMundipagg_Model_Rest
{

    public function validadePost($orders)
    {
        if (!$orders) {
            return $this;
        }
        $this->resendPost($orders['xml_retry']);

        return $this;
    }

    protected function resendPost($post)
    {
        $api = Mage::getModel('mundipagg/api');

        // Process order
        $result = $api->processOrder([
            'xmlStatusNotification' => $post
        ]);

        $token = json_decode($result, true);
        $token = $token['token'];
        Mage::getSingleton('core/session')->setKeercusToken($token);

        return $token;
    }

    /**
     * @return array
     */
    public function getContentType()
    {
        return $this->merchantKey =  [
            'MerchantKey' => 'application/x-www-form-urlencoded'
        ];
    }

    /**
     * @return string
     */
    public function getUri()
    {
        return Mage::getBaseUrl();
    }

    public function getHeader()
    {
        return array(
            'Content-Type' => 'application/x-www-form-urlencoded'
        );
    }



}