<?php

/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   Bitcommerce_MonitMundipagg
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Anderson Cardoso <anderson.cardoso@e-smart.com.br>
 */

class Bitcommerce_MonitMundipagg_Model_Connect_Connect_Connect extends Bitcommerce_MonitMundipagg_Model_Rest
{
    /**
     * @param Bitcommerce_MonitMundipagg_Model_Monitoring_Register $monit
     * @return $this
     */
    public function connectMundipagg(Bitcommerce_MonitMundipagg_Model_Monitoring_Register $monit)
    {
        $order = $monit->getOrderNumber();

        try {
            $this->helper->log('init connection with Mundipagg' . $order, 'connectMundipagg');
            $this->setMerchantKey($this->helper->getMerchantKey());
            $this->setClient($this->getFullEndpoint($order), array('timeout' => self::HTTP_CLIENT_TIMEOUT));
            $this->helper->log('endpoint Mundipagg' . $this->getFullEndpoint($order), 'connectMundipagg');
            $this->client->setHeaders($this->getMerchantKey());
            $this->client->setUri($this->getFullEndpoint($order));
            $response = $this->client->request(Zend_Http_Client::GET);
            $this->helper->log('End connection with Mundipagg' . $order, 'connectMundipagg');

            Mage::getModel('bitcommerce_monitmundipagg/xmlStructures')->loadResponse($response->getBody());

            $this->updateStatusMonitoring($order);

        } catch (Exception $exception) {
            $this->helper->log('Error connection with Mundipagg' . $order, 'connectMundipagg');
        }
        return $this;
    }

    /**
     * @todo Set new status by monitoring in the order
     * @param $orderId
     * @return $this
     * @throws Exception
     */
    public function updateStatusMonitoring($orderId)
    {
        $order = Mage::getModel('sales/order')->load($orderId, 'increment_id');
        $order->setData('monit_status', 'checked');
        $order->save();
        return $this;
    }
}
