<?php

/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   Bitcommerce_MonitMundipagg
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Anderson Cardoso <anderson.cardoso@e-smart.com.br>
 */

class Bitcommerce_MonitMundipagg_Model_Rest extends Bitcommerce_MonitMundipagg_Model_Abstract
{
    public $generalArray;

    /**
     * @return Bitcommerce_MonitMundipagg_Helper_Data
     */
    public function getMerchantKey()
    {
        return $this->merchantKey;
    }

    /**
     * @param Bitcommerce_MonitMundipagg_Helper_Data $merchantKey
     */
    public function setMerchantKey($merchantKey)
    {
        $this->merchantKey =  [
                'MerchantKey' => $merchantKey
            ];
    }

    /**
     * @param string $uri
     * @param array $options
     * @return $this|Zend_Http_Client|Zend_Soap_Client
     */
    protected function setClient($uri, $options = array())
    {
        if (!$uri) {
            return $this;
        }

        $this->client = new Zend_Http_Client($uri, $options);

        return $this;
    }

    /**
     * @param $structures
     * @return $this
     */
    public function setStructures($structures)
    {
        if (!$structures) {
            return $this;
        }

        $this->structures = $structures;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMundipaggResponse()
    {
        return $this->mundipaggResponse;
    }

    /**
     * @param mixed $mundipaggResponse
     */
    public function setMundipaggResponse($mundipaggResponse)
    {
        $this->mundipaggResponse = $mundipaggResponse;
    }

    /**
     * Get complete endpoint with the service path
     *
     * @return string
     */
    protected function getFullEndpoint($pedido)
    {
        $url = $this->endpoint . $this->servicePath;
        $pathParams  = self::PARAMS;
        $url = $url . $pathParams . '=' . $pedido ;

        return $url;
    }

}
