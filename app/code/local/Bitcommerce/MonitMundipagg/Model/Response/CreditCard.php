<?php

/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   Bitcommerce_MonitMundipagg
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Anderson Cardoso <anderson.cardoso@e-smart.com.br>
 */

class Bitcommerce_MonitMundipagg_Model_Response_CreditCard extends Bitcommerce_MonitMundipagg_Model_Response_General
{
    /**
     * @var
     */
    public $acquire;

    /**
     * @var
     */
    public $amountInCents;

    /**
     * @var
     */
    public $authorizationCode;

    /**
     * @var
     */
    public $authorizedAmountInCents;

    /**
     * @var
     */
    public $capturedAmountInCents;

    /**
     * @var
     */
    public $creditCardBrand;

    /**
     * @var
     */
    public $customStatus;

    /**
     * @var
     */
    public $isRecurrence;

    /**
     * @var
     */
    public $refundedAmountInCents;

    /**
     * @var
     */
    public $statusChangedDate;

    /**
     * @var
     */
    public $transactionIdentifier;

    /**
     * @var
     */
    public $transactionKey;

    /**
     * @var
     */
    public $transactionReference;

    /**
     * @var
     */
    public $uniqueSequentialNumber;

    /**
     * @var
     */
    public $previousCreditCardTransactionStatus;

    /**
     * @var
     */
    public $transactionStatus;

    /**
     * @var
     */
    public $orderReference;

    /**
     * @var $creditCard
     */
    public $creditCard;

    /**
     * @var $sale
     */
    public $sale;

    /**
     * @var $response
     */
    public $response;

    /**
     * @var $orderKey
     */
    public $orderKey;

    /**
     * @return mixed
     */
    public function getAcquire()
    {
        $this->acquire = null;

        if (!empty($this->creditCard)) {
            $this->acquire = $this->creditCard['CreditCardTransactionData']['AcquirerName'];
        }

        return $this->acquire;
    }

    /**
     * @return mixed
     */
    public function getAmountInCents()
    {
        $this->amountInCents = null;

        if (!empty($this->creditCard)) {
            $this->amountInCents = $this->creditCard['CreditCardTransactionData']['AmountInCents'];
        }

        return $this->amountInCents;
    }

    /**
     * @return mixed
     */
    public function getAuthorizationCode()
    {
        $this->authorizationCode = null;

        if (!empty($this->creditCard)) {
            $this->authorizationCode = $this->creditCard['CreditCardTransactionData']['UniqueSequentialNumber'];
        }

        return $this->authorizationCode;
    }

    /**
     * @return mixed
     */
    public function getAuthorizedAmountInCents()
    {
        $this->authorizedAmountInCents = null;

        if (!empty($this->creditCard)) {
            $this->authorizedAmountInCents = $this->creditCard['CreditCardTransactionData']['AuthorizedAmountInCents'];
        }

        return $this->authorizedAmountInCents;
    }

    /**
     * @return mixed
     */
    public function getCapturedAmountInCents()
    {
        $this->capturedAmountInCents = null;

        if (!empty($this->creditCard)) {
            $this->capturedAmountInCents = $this->creditCard['CreditCardTransactionData']['CapturedAmountInCents'];
        }

        return $this->capturedAmountInCents;
    }

    /**
     * @return mixed
     */
    public function getCreditCardBrand()
    {
        $this->creditCardBrand = null;

        if (!empty($this->creditCard)) {
            $this->creditCardBrand = $this->creditCard['CreditCardTransactionData']['CreditCard']['CreditCardBrand'];
        }

        return $this->creditCardBrand;
    }

    /**
     * @return mixed
     */
    public function getCustomStatus()
    {
        return $this->customStatus;
    }

    /**
     * @return mixed
     */
    public function getIsRecurrence()
    {
        $this->isRecurrence = null;

        if (!empty($this->creditCard)) {
            $this->isRecurrence = $this->creditCard['CreditCardTransactionData']['IsReccurency'];
        }

        return $this->isRecurrence;
    }

    /**
     * @return mixed
     */
    public function getRefundedAmountInCents()
    {
        return $this->refundedAmountInCents;
    }

    /**
     * @return mixed
     */
    public function getStatusChangedDate()
    {
        $this->statusChangedDate = null;

        if (!empty($this->sale)) {
            if (key($this->sale['HistoryCollection']['QuerySaleAntiFraudAnalysisHistory']) !== 0) {
                $this->statusChangedDate = $this->sale['HistoryCollection']['QuerySaleAntiFraudAnalysisHistory']['StatusChangedDate'];
            } else {
                foreach ($this->sale['HistoryCollection']['QuerySaleAntiFraudAnalysisHistory'] as $history) {
                    if (!empty($history['StatusChangedDate'])) {
                        $this->statusChangedDate = $history['StatusChangedDate'];
                    }
                }
            }
        }

        return $this->statusChangedDate;
    }

    /**
     * @return mixed
     */
    public function getTransactionIdentifier()
    {
        $this->transactionIdentifier = null;

        if (!empty($this->creditCard)) {
            $this->transactionIdentifier = $this->creditCard['CreditCardTransactionData']['TransactionIdentifier'];
        }

        return $this->transactionIdentifier;
    }

    /**
     * @return mixed
     */
    public function getTransactionKey()
    {
        $this->transactionKey = null;

        if (!empty($this->creditCard)) {
            $this->transactionKey = $this->creditCard['CreditCardTransactionData']['TransactionKey'];
        }

        return $this->transactionKey;
    }

    /**
     * @return mixed
     */
    public function getTransactionReference()
    {
        $this->transactionReference = null;

        if (!empty($this->creditCard)) {
            $this->transactionReference = $this->creditCard['CreditCardTransactionData']['TransactionReference'];
        }

        return $this->transactionReference;
    }

    /**
     * @return mixed
     */
    public function getUniqueSequentialNumber()
    {
        $this->uniqueSequentialNumber =  null;

        if (!empty($this->creditCard)) {
            $this->uniqueSequentialNumber = $this->creditCard['CreditCardTransactionData']['UniqueSequentialNumber'];
        }

        return $this->uniqueSequentialNumber;
    }

    /**
     * @return mixed
     */
    public function getPreviousCreditCardTransactionStatus()
    {
        $this->previousCreditCardTransactionStatus = null;

        if (!empty($this->creditCard)) {
            $this->previousCreditCardTransactionStatus = $this->creditCard['CreditCardTransactionData']['AffiliationCode'];
        }

        return $this->previousCreditCardTransactionStatus;
    }

    /**
     * @return mixed
     */
    public function getCreditCardTransactionStatus()
    {
        $this->transactionStatus = null;

        if (!empty($this->creditCard)) {
            $this->transactionStatus = $this->creditCard['CreditCardTransactionData']['CreditCardTransactionStatus'];
        }

        return $this->transactionStatus;
    }

    /**
     * @return mixed
     */
    public function getOrderKey()
    {
        $this->orderKey = null;

        if (key($this->response['SaleDataCollection']['Sale']) !== 0) {
            $this->orderKey = $this->response['SaleDataCollection']['Sale']['OrderData']['OrderKey'];
        } else {
            foreach ($this->response['SaleDataCollection']['Sale'] as $sale) {
                if (!empty($sale['OrderData'])) {
                    $this->orderKey = $sale['OrderData']['OrderKey'];
                }
            }
        }

        return $this->orderKey;
    }

    /**
     * @return mixed
     */
    public function getOrderReference()
    {
        $this->orderReference = null;

        if (key($this->response['SaleDataCollection']['Sale']) !== 0) {
            $this->orderReference = $this->response['SaleDataCollection']['Sale']['OrderData']['OrderReference'];
        } else {
            foreach ($this->response['SaleDataCollection']['Sale'] as $sale) {
                if (!empty($sale['OrderData'])) {
                    $this->orderReference = $sale['OrderData']['OrderReference'];
                }
            }
        }

        return $this->orderReference;
    }

    /**
     * Return the array with credit Card Information From Mundipagg API
     * @return array
     */
    public function getCreditCardTransactionSchema()
    {
        $ccTransaction = [
            'CreditCardTransaction'=> [
                    'Acquirer'                            => $this->getAcquire(),
                    'AmountInCents'                       => $this->getAmountInCents(),
                    'AuthorizationCode'                   => $this->getAuthorizationCode(),
                    'AuthorizedAmountInCents'             => $this->getAuthorizedAmountInCents(),
                    'CapturedAmountInCents'               => $this->getCapturedAmountInCents() ,
                    'CreditCardBrand'                     => $this->getCreditCardBrand(),
                    'CustomStatus'                        => null,
                    'IsRecurrency'                        => $this->getIsRecurrence() ,
                    'RefundedAmountInCents'               => null,
                    'StatusChangedDate'                   => $this->getStatusChangedDate(),
                    'TransactionIdentifier'               => $this->getTransactionIdentifier() ,
                    'TransactionKey'                      => $this->getTransactionKey(),
                    'TransactionReference'                => $this->getTransactionReference(),
                    'UniqueSequentialNumber'              => $this->getUniqueSequentialNumber(),
                    'PreviousCreditCardTransactionStatus' => $this->getPreviousCreditCardTransactionStatus(),
                    'CreditCardTransactionStatus'         => $this->getCreditCardTransactionStatus(),
                ]
        ];
        return $ccTransaction;
    }

    /**
     * @return array
     */
    public function setInfoSchema()
    {
        return [
            "MerchantKey"       => $this->response['MerchantKey'],
            "OrderKey"          => $this->getOrderKey(),
            "OrderReference"    => $this->getOrderReference(),
            "OrderStatus"       => (!empty($this->sale) ? $this->sale['AntiFraudAnalysisStatus'] : null)
        ];
    }
}
