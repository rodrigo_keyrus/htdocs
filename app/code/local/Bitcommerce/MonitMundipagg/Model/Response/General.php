<?php

/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   Bitcommerce_MonitMundipagg
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Anderson Cardoso <anderson.cardoso@e-smart.com.br>
 */

class Bitcommerce_MonitMundipagg_Model_Response_General
{
    /**
     * @var $amountInCents
     */
    public $amountInCents;

    /**
     * @var $creditCard
     */
    public $creditCard;

    /**
     * @var $sale
     */
    public $sale;

    /**
     * @var $status
     */
    public $status;

    /**
     * @var $product
     */
    public $product;

    /**
     * @var $returnCode
     */
    public $returnCode;

    /**
     * @var $message
     */
    public $message;

    /**
     * @var $score
     */
    public $score;

    /**
     * @var $response
     */
    public $response;


    /**
     * @return mixed
     */
    public function getAmountInCents()
    {
        $this->amountInCents = null;

        if (!empty($this->creditCard)) {
            $this->amountInCents = $this->creditCard['CreditCardTransactionData']['AmountInCents'];
        }

        return $this->amountInCents;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param mixed $responseGeneral
     */
    public function setResponse($responseGeneral)
    {
        $this->creditCard = array();
        $this->sale       = array();
        $this->response   = $responseGeneral;

        if (key($this->response['SaleDataCollection']['Sale']) !== 0) {
            $this->creditCard = $this->response['SaleDataCollection']['Sale']['CreditCardTransactionDataCollection'];
            $this->sale = $this->response['SaleDataCollection']['Sale']['AntiFraudAnalysisData'];
        } else {
            foreach ($this->response['SaleDataCollection']['Sale'] as $sale) {
                if (!empty($sale['CreditCardTransactionDataCollection'])) {
                    $this->creditCard = $sale['CreditCardTransactionDataCollection'];
                }

                if (!empty($sale['AntiFraudAnalysisData'])) {
                    $this->sale = $sale['AntiFraudAnalysisData'];
                }
            }
        }
    }

    /**
     * @return mixed
     */
    public function getProduct()
    {
        $this->product = null;

        if (!empty($this->sale)) {
            $this->product = $this->sale['AntiFraudServiceName'];
        }

        return $this->product;
    }

    public function getSale()
    {
        return $this->sale;
    }

    public function getAnalysisStatus()
    {
        if (!empty($this->sale)) {
            return $this->sale['AntiFraudAnalysisStatus'];
        }

        return null;
    }

    /**
     * @return mixed
     */
    public function getReturnCode()
    {
        return $this->returnCode;
    }

    /**
     * @return mixed
     */
    public function getReturnMessage()
    {
        $this->message = null;

        if (!empty($this->sale)) {
            if (key($this->sale['HistoryCollection']['QuerySaleAntiFraudAnalysisHistory']) !== 0) {
                $this->message = $this->sale['HistoryCollection']['QuerySaleAntiFraudAnalysisHistory']['ReturnMessage'];
            } else {
                foreach ($this->sale['HistoryCollection']['QuerySaleAntiFraudAnalysisHistory'] as $history) {
                    if (!empty($history['ReturnMessage'])) {
                        $this->message = $history['ReturnMessage'];
                    }
                }
            }
        }

        return $this->message;
    }

    /**
     * @return mixed
     */
    public function getScore()
    {
        $this->score = null;

        if (!empty($this->sale)) {
            if (key($this->sale['HistoryCollection']['QuerySaleAntiFraudAnalysisHistory']) !== 0) {
                $this->score = $this->sale['HistoryCollection']['QuerySaleAntiFraudAnalysisHistory']['Score'];
            } else {
                foreach ($this->sale['HistoryCollection']['QuerySaleAntiFraudAnalysisHistory'] as $history) {
                    if (!empty($history['Score'])) {
                        $this->score = $history['Score'];
                    }
                }
            }
        }

        return $this->score;
    }

    /**
     * @return array
     */
    public function getGeneralSchema()
    {
        $general = [
            "AmountInCents"     => $this->getAmountInCents(),
            "AmountPaidInCents" => null,
            "Antifraud"         =>  $this->getAntiFraud(),
        ];
        return $general;
    }

    /**
     * @return array
     */
    public function getAntiFraud()
    {
        return [
            "status"        => $this->getStatus(),
            "Product"       => $this->getProduct(),
            "ReturnCode"    => null,
            "ReturnMessage" => $this->getReturnMessage(),
            "Score"         => $this->getScore()
        ];
    }
}
