<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   sephora
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Anderson Cardoso <anderson.cardoso@e-smart.com.br>
 */

abstract class Bitcommerce_MonitMundipagg_Model_Abstract extends Mage_Core_Model_Abstract
{

    const PARAMS = 'Sale/Query/OrderReference';

    const HTTP_CLIENT_TIMEOUT = 30;

    /**
     * Endpoint for start api client
     *
     * @var string
     */
    public $endpoint;

    /**
     * Set $structures for export make xml
     *
     * @var array
     */
    public $structures;

    /**
     * Body param for setting in raw data
     *
     * @var array
     */
    public $bodyParam = array();

    /**
     * Path params for append in endpoint url
     *
     * @var array
     */
    public $pathParams = array();

    /**
     * Query params for append in endpoint url
     *
     * @var array
     */
    public $queryParams = array();

    /**
     * Request Method
     *
     * @var array
     */
    public $requestMethod = Zend_Http_Client::GET;

    /**
     * Response
     *
     * @var Zend_Http_Response
     */
    public $response;


    /**
     * Returns helper object
     *
     * @var Bitcommerce_MonitMundipagg_Helper_Data
     */
    public $helper;

    /**
     * Returns helper object
     *
     * @var Bitcommerce_MonitMundipagg_Helper_Data
     */
    public $merchantKey;

    public $mundipaggResponse;

    public $infoGeneral;

    public $infoCreditCard;

    public $infoTransaction;

    /**
     * Http Client
     *
     * @var Zend_Http_Client
     */
    protected $client;

    /**
     * Path for append in endpoint url
     *
     * @var string
     */
    public $servicePath;

    /**
     * Getting endpoint url according to the integration mode
     */
    public function __construct()
    {
        parent::__construct();

        $this->helper           = Mage::helper('bitcommerce_monitmundipagg');
        $this->infoGeneral      = $this->infoGeneral = Mage::getModel('bitcommerce_monitmundipagg/response_general');
        $this->infoCreditCard   = $this->infoGeneral = Mage::getModel('bitcommerce_monitmundipagg/response_creditCard');
        $this->endpoint         = $this->helper->getEndpoint();
    }

    /**
     * @param $structures
     * @return $this
     */
    public function setStructures($structures)
    {
        if (!$structures) {
            return $this;
        }

        $this->structures = $structures;

        return $this;
    }

    /**
     * Return a client object.
     *
     * @param string $uri
     * @param array  $options
     *
     * @return Zend_Soap_Client|Zend_Http_Client
     */
    abstract protected function setClient($uri, $options = array());
}
