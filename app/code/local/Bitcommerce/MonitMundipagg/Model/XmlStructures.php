<?php

/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   Bitcommerce_MonitMundipagg
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Anderson Cardoso <anderson.cardoso@e-smart.com.br>
 */

class Bitcommerce_MonitMundipagg_Model_XmlStructures extends Bitcommerce_MonitMundipagg_Model_Rest
{
    /**
     *
     */
    const XMLTYPE = 'SimpleXMLElement';

    /**
     * @var
     */
    public $creditCard;

    /**
     * @var
     */
    public $sale;

    /**
     * @var
     */
    public $xml;

    /**
     * @return array
     */
    public function getTransaction()
    {
        if (key($this->sale) !== 0) {
            $sale = $this->sale;

            if (!empty($sale['BoletoTransactionDataCollection'])) {
                return $sale['BoletoTransactionDataCollection']['BoletoTransactionData'];
            } else {
                return $sale['CreditCardTransactionDataCollection']['CreditCardTransactionData'];
            }

        } else {
            $rsale = array();
            foreach ($this->sale as $sale) {
                if (
                    !empty($sale['BoletoTransactionDataCollection']) &&
                    ($sale['BoletoTransactionDataCollection']['BoletoTransactionData']['BoletoTransactionStatus'] != 'Overpaid')
                ) {
                    $rsale = $sale['BoletoTransactionDataCollection']['BoletoTransactionData'];
                } elseif (
                        !empty($sale['CreditCardTransactionDataCollection']) &&
                        ($sale['CreditCardTransactionDataCollection']['CreditCardTransactionData']['CreditCardTransactionStatus'] != 'Overpaid')
                ) {
                    $rsale = $sale['CreditCardTransactionDataCollection']['CreditCardTransactionData'];
                }
            }

            return $rsale;
        }

    }

    /**
     * Get Structures form make object before do xml
     * @return array
     */
    public function getStructures()
    {
        $general    = $this->infoGeneral->getGeneralSchema();
        $creditCard = $this->infoCreditCard->getCreditCardTransactionSchema();
        $info       = $this->infoCreditCard->setInfoSchema();
        return array_merge($general, $creditCard, $info);
    }

    /**
     * @param $response
     * @return mixed
     */
    public function loadResponse($response)
    {
        $xmlValidate      = simplexml_load_string($response, self::XMLTYPE, LIBXML_NOCDATA);
        $jsonEncoded      = json_encode($xmlValidate);
        $this->response   = json_decode($jsonEncoded, true);
        $this->sale       = $this->response['SaleDataCollection']['Sale'];

        $this->infoGeneral->setResponse($this->response);
        $this->infoCreditCard->setResponse($this->response);

        $makeXml = $this->getStructures();
        $this->xml = $this->convertXml($makeXml);

        $this->updateMonitoring();

        return $this;
    }

    /**
     * @param $object
     * @return mixed
     */
    public function convertXml($object)
    {
        $xml = new SimpleXMLElement("<?xml version=\"1.0\"?><StatusNotification></StatusNotification>");
        $this->convertArray($object, $xml);
        Header('Content-type: text/xml');
        return $xml->asXML();
    }

    /**
     * @return $this
     */
    public function updateMonitoring()
    {
        $this->helper
            ->log('Init Save response' . $this->infoCreditCard->getOrderReference(), 'saveResponseMundipagg');
        $monitoring = Mage::getModel('bitcommerce_monitmundipagg/monitoring_register')
            ->getCollection()
            ->addFieldToFilter('order_number', $this->infoCreditCard->getOrderReference());
        $monitoring = current($monitoring->getData());
        $id         = current($monitoring);

        $model      = Mage::getModel('bitcommerce_monitmundipagg/monitoring_register')
            ->load($id)->addData($this->schemeMonitoringXml());
        try {
            $model->setId($id)->save();
            $this->helper
                ->log('Success Save response' . $this->infoCreditCard->getOrderReference(), 'saveResponseMundipagg');

        } catch (Exception $e) {
            $this->helper
                ->log('Error Save Response' . $e->getMessage(), 'saveResponseMundipagg');
        }

        return $this;
    }

    /**
     *
     */
    public function schemeMonitoringXml()
    {
        $data = array(
            'xml_retry'             => $this->xml,
            'additional_information'=> 1,
            'updated_at'            => date('Y-m-d H:i:s'),
            'qty_retry'             => 1,
            'status_mundipagg'      => $this->infoCreditCard->getAnalysisStatus(),
        );

        return $data;
    }

    /**
     * @param $array
     * @param $xml
     */
    public function convertArray($array, $xml)
    {
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                if (!is_numeric($key)) {
                    $subNode = $xml->addChild("$key");
                    $this->convertArray($value, $subNode);
                } else {
                    $subNode = $xml->addChild("item$key");
                    $this->convertArray($value, $subNode);
                }
            } else {
                $xml->addChild("$key", htmlspecialchars("$value"));
            }
        }
    }
}
