<?php   
class Creare_DynamicSitemap_Block_Dynamicsitemap extends Mage_Core_Block_Template
{
	public function getCreareCMSPages()
    {
		$storeId = $this->helper('core')->getStoreId(); // thanks to drawandcode for this
		$cms = Mage::getModel('cms/page')->getCollection()
				->addFieldToFilter('is_active',1)
				->addStoreFilter($storeId);
		$url = Mage::getBaseUrl();
		$html = "";
		foreach($cms as $cmspage) {
            $page = $cmspage->getData();
            if ( $page[ 'identifier' ] == "no-route" || $page[ 'identifier' ] == "enable-cookies" || $page[ 'identifier' ] == "empty" ) {
                continue;
            }

            if ( $page[ 'identifier' ] == "home" ) {

                // this is for a nice local link to home
                $html .= "<li>
                            <a href=\"$url\" title=\"" . $page[ 'title' ] . "\">
                                " . $page[ 'title' ] . "
                            </a>
                          </li>\n";
            } else {
                $html .= "<li>
                            <a href=\"$url" . $page[ 'identifier' ] . "\" title=\"" . $page[ 'title' ] . "\">
                                " . $page[ 'title' ] . "
                            </a>
                          </li>\n";
            }
        }

		return $html;	
	}

	public function getSitemap()
    {
        return Mage::getModel('cms/block')
            ->setStoreId(Mage::app()->getStore()->getId())->load('custom-sitemap');
    }

    public function getManufacturers()
    {
        return Mage::getModel('zeon_manufacturer/manufacturer')
            ->getCollection()
            ->addFieldToFilter('status', 1)
            ->addFieldToFilter('is_display_sitemap', 1)
            ->setOrder('manufacturer_name','ASC');
    }

    public function getPagesByManufacturer($manufacturerId)
    {
        return Mage::getModel('esmart_manufacturer/page')
            ->getCollection()
            ->addParentFilter($manufacturerId)
            ->addFieldToFilter('status', 1)
            ->addFieldToFilter('is_display_sitemap', 1)
            ->setOrder('entity_id','ASC');
    }
}