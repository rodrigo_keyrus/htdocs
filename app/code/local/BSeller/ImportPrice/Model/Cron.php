<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_ImportPrice
 *
 * @copyright Copyright (c) 2017 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Andre Manoel <andre.manoel@e-smart.com.br>
 */

class BSeller_ImportPrice_Model_Cron extends Mage_Core_Model_Abstract
{

    /**
     * Routine to update scheduled prices
     */
    public function checkSchedule()
    {
        $now = Mage::getModel('core/date')->date('Y-m-d H:i:s');
        $now = Mage::getModel('core/date')->gmtTimestamp($now);
        $schedules = Mage::getModel('bseller_importprice/schedule')
            ->getCollection()
            ->addFieldToFilter('is_active', true);

        $productIds = [];

        foreach ($schedules as $schedule) {
            $fromDate = strtotime($schedule->getFromDate());
            $toDate   = strtotime($schedule->getToDate());
            if ($fromDate < $now) {
                if ($toDate < $now) {
                    $productIds[] = $this->cancelSchedule($schedule);
                    continue;
                }
                if ($schedule->getStatus() == 0) {
                    $productIds[] = $this->activeSchedule($schedule);
                }
            }
        }

        $productIds = array_filter($productIds, function ($id) {
            return $id !== false;
        });

        if (count($productIds)) {
            Mage::getResourceModel('catalog/product_indexer_price')->reindexProductIds($productIds);
            Mage::getModel('catalog/product_flat_indexer')->updateProduct($productIds);
        }
    }

    /**
     * Cancel the active imported price
     *
     * @param BSeller_ImportPrice_Model_Schedule $schedule
     * @return int | bool
     */
    private function cancelSchedule(BSeller_ImportPrice_Model_Schedule $schedule)
    {
        $history = Mage::getModel('bseller_importprice/history')->load($schedule->getHistoryId());
        /** @var Mage_Catalog_Model_Product $product */
        $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $schedule->getSku());
        if (!$product) {
            $schedule->delete();
            return false;
        }
        $product->setSpecialPrice('');
        $product->getResource()->saveAttribute($product, 'special_price');

        $this->setProductUpdated($product);

        $history->setStatus(0)->save( );
        $schedule->delete();

        Mage::dispatchEvent(
            'bseller_skyhub_integrate_product',
            array(
                'product' => $product
            )
        );

        return $product->getId();
    }

    /**
     * Active the price scheduled
     *
     * @param BSeller_ImportPrice_Model_Schedule $schedule
     * @return int | bool
     */
    private function activeSchedule(BSeller_ImportPrice_Model_Schedule $schedule)
    {
        /** @var Mage_Catalog_Model_Product $product */
        $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $schedule->getSku());
        if (!$product || $product->getTypeId() != 'simple') {
            $schedule->delete();
            return false;
        }

        $product->setSpecialPrice($schedule->getSpecialPrice());

        $product->setSpecialFromDate($this->getCurrentTimezoneDate());
        $product->setSpecialToDate('');

        $product->getResource()->saveAttribute($product, 'special_price');
        $product->getResource()->saveAttribute($product, 'special_from_date');
        $product->getResource()->saveAttribute($product, 'special_to_date');
        $this->setProductUpdated($product);

        $schedule->setStatus(1)->save();

        Mage::dispatchEvent(
            'bseller_skyhub_integrate_product',
            array(
                'product' => $product
            )
        );

        return $product->getId();
    }

    /**
     * Return employee helper
     *
     * @return BSeller_ImportPrice_Helper_Data
     */
    protected function getHelper()
    {
        return Mage::helper('bseller_importprice');
    }

    /**
     * @param $product
     */
    private function setProductUpdated($product)
    {
        $write = $product
            ->getResource()
            ->getWriteConnection();

        $write->update(
            $product->getResource()->getTable('catalog/product'),
            ['updated_at' => now()],
            'entity_id = '. $product->getId()
        );

        $store = Mage::app()->getStore('default');
        $timestamp = Mage::app()->getLocale()->storeTimeStamp($store);

        $write->update(
            'catalog_product_index_website',
            ['website_date' => Varien_Date::formatDate($timestamp, false)],
            null
        );
    }

    /**
     * @return false|string
     */
    private function getCurrentTimezoneDate()
    {
        $timezone = Mage::app()->getStore('default')->getConfig(Mage_Core_Model_Locale::XML_PATH_DEFAULT_TIMEZONE);
        $currentTimezone = @date_default_timezone_get();
        @date_default_timezone_set($timezone);
        $date = date('Y-m-d H:i:s');
        @date_default_timezone_set($currentTimezone);

        return $date;
    }
}