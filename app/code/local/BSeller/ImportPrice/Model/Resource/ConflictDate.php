<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_ImportPrice
 *
 * @copyright Copyright (c) 2017 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Andre Manoel <andre.manoel@e-smart.com.br>
 */

class BSeller_ImportPrice_Model_Resource_ConflictDate extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('bseller_importprice/importprice_conflict_date', 'id');
    }

    public function truncate()
    {
        $model = Mage::getModel('bseller_importprice/conflictDate');
        $resource = $model->getResource();
        $connection = $resource->_getWriteAdapter();

        /* @see Varien_Db_Adapter_Pdo_Mysql */
        $connection->truncateTable($resource->getMainTable());
        $connection->changeTableAutoIncrement($resource->getMainTable(), 1);
    }
}