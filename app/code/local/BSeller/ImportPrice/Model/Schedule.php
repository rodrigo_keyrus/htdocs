<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_ImportPrice
 *
 * @copyright Copyright (c) 2017 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Andre Manoel <andre.manoel@e-smart.com.br>
 */

class BSeller_ImportPrice_Model_Schedule extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('bseller_importprice/schedule');
    }

    /**
     * Get a Schedule Register (BSeller_ImportPrice_Model_Schedule)
     * register based on a id of a History Register (BSeller_ImportPrice_Model_History)
     *
     * @param int $id ID of History register
     *
     * @return BSeller_ImportPrice_Model_History
     */
    public function getScheduleByHistory($id)
    {
        $schedule = $this->getCollection()->addFieldToFilter('history_id', $id);
        return $schedule->getFirstItem();
    }

    /**
     * Save a Schedule Register (BSeller_ImportPrice_Model_Schedule)
     * based in a ConflictDate Register (BSeller_ImportPrice_Model_ConflictDate)
     *
     * @param BSeller_ImportPrice_Model_ConflictDate $conflict Conflict Register
     * @param int $historyId ID of History register
     *
     * @throws Varien_Exception
     */
    public function saveScheduleFromConflict(BSeller_ImportPrice_Model_ConflictDate $conflict, $historyId)
    {
        $schedule = Mage::getModel('bseller_importprice/schedule');
        $schedule->setSku($conflict->getSku());
        $schedule->setSpecialPrice($conflict->getNewSpecialPrice());
        $schedule->setStatus(0);
        $schedule->setFromDate($conflict->getFromDate());
        $schedule->setToDate($conflict->getToDate());
        $schedule->setHistoryId($historyId);
        $schedule->save();
    }
}