<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_ImportPrice
 *
 * @copyright Copyright (c) 2017 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Andre Manoel <andre.manoel@e-smart.com.br>
 */

class BSeller_ImportPrice_Model_History extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('bseller_importprice/history');
    }

    /**
     * Save History Data From a Conflict
     *
     * @param BSeller_ImportPrice_Model_ConflictDate $conflict
     *
     * @return bool
     * @throws Exception
     */
    public function saveHistoryFromConflict ( BSeller_ImportPrice_Model_ConflictDate $conflict)
    {
        $created = date(
            'Y-m-d H:i:s', Mage::getModel('core/date')->gmtTimestamp(
                strtotime(str_replace('/', '-', Mage::getModel('core/date')->date('Y-m-d H:i:s')))
            )
        );

        $admUser = Mage::getSingleton('admin/session')->getUser()->getUsername();

        $history = Mage::getModel('bseller_importprice/history');
        $history->setData(
            array(
                'sku'           => $conflict->getSku(),
                'price'         => $conflict->getPrice(),
                'special_price' => $conflict->getNewSpecialPrice(),
                'status'        => 0,
                'user'          => $admUser,
                'description'   => $conflict->getDescription(),
                'from_date'     => $conflict->getFromDate(),
                'to_date'       => $conflict->getToDate(),
                'created_at'    => $created,
                'overwritten'   => 0
            )
        );
        $history->save();

        return $history->getId();
    }

    /**
     * Get a history register (BSeller_ImportPrice_Model_History)
     * based on a Conflict Register (BSeller_ImportPrice_Model_ConflictDate)
     *
     * @param BSeller_ImportPrice_Model_ConflictDate $conflict
     *
     * @return array
     * @throws Exception
     */
    public function getHistoryUsingConflict(BSeller_ImportPrice_Model_ConflictDate $conflict)
    {
        $histories = array();

        $historyCollection = Mage::getResourceModel('bseller_importprice/history_collection')
            ->addFieldToSelect('*')
            ->addFieldToFilter('sku', $conflict->getSku())
            ->addFieldToFilter('overwritten', 0);

        /** @var BSeller_ImportPrice_Model_History $history */
        foreach ($historyCollection as $history) {

            if (($conflict->getFromDate() >= $history->getFromDate()) && ($history->getFromDate() <= $conflict->getToDate())) {
                $histories[] = $history;
            } elseif (($conflict->getToDate() <= $history->getToDate()) && ($conflict->getToDate() >= $history->getFromDate())) {
                $histories[] = $history;
            } elseif (($conflict->getFromDate() <= $history->getFromDate() && $conflict->getFromDate() <= $history->getToDate()) && ($conflict->getToDate() >= $history->getToDate())) {
                $histories[] = $history;
            }
        }

        return $histories;
    }
}