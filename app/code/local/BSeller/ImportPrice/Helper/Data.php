<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_ImportPrice
 *
 * @copyright Copyright (c) 2017 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Andre Manoel <andre.manoel@e-smart.com.br>
 */

class BSeller_ImportPrice_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Register key
     *
     * @var string
     */
    protected $_registerKey = 'bseller_importprice';

    /**
     * Save object data in register
     *
     * @param $value
     * @param bool $graceful
     * @return $this
     */
    public function addObjectInfo($value, $graceful = true)
    {
        Mage::register($this->_registerKey, $value, $graceful);

        return $this;
    }

    /**
     * Return object data of register
     *
     * @return Varien_Object
     */
    public function getObjectInfo()
    {
        if ($object = Mage::registry($this->_registerKey)) {
            return $object;
        }

        return new Varien_Object();
    }

    /**
     * get Csv parse object
     */
    public function getCsvObject()
    {
        return new Varien_File_Csv();
    }

    /**
     * Return store config data
     *
     * @param $key
     * @return string
     */
    public function getStoreConfig($key)
    {
        return Mage::getStoreConfig('bseller_importprice/importprice_settings/' . $key);
    }

    /**
     * Retrieve the last grouped SKU and sums +1 to it
     * @return string
     */
    public function generateGroupedSku()
    {
        $numberPart = str_replace('PRD', '', $this->getLastSku());
        $zeroLeftCount = substr_count($numberPart, '0');
        $sum = (int) $numberPart + 1;

        if ($zeroLeftCount > 0 && (strlen($sum) + $zeroLeftCount) > strlen($numberPart)) {
            $zeroLeftCount--;
        }

        return sprintf('PRD%s%s', str_repeat('0', $zeroLeftCount), $sum);
    }

    /**
     * @return string
     */
    public function getLastSku()
    {
        /** @var Mage_Catalog_Model_Resource_Product_Collection $productCollection */
        $productCollection = Mage::getModel('catalog/product')
            ->getCollection();
        $productCollection->getSelect()
            ->reset('columns')
            ->columns(['sku'])
            ->where(new Zend_Db_Expr("sku REGEXP 'PRD[0-9]{1,10}$'"))
            ->order('sku DESC')
            ->limit(1);

        return $productCollection->fetchItem()->getSku();
    }

    /**
     * Get all schedules given a product SKU and from and to dates
     *
     * @param string $sku       SKU of the product
     * @param string $fromDate  Initial Date
     * @param string $toDate    Final Date
     *
     * @return array
     *
     * @throws Varien_Exception
     */
    public function skuHasDateConflicts($sku, $fromDate, $toDate)
    {
        $scheduleCollection = Mage::getModel('bseller_importprice/schedule')
            ->getCollection()
            ->addFieldToFilter('sku', $sku);

        $schedules = [];

        /** @var BSeller_ImportPrice_Model_Schedule $schedule */
        foreach ($scheduleCollection as $schedule) {
            $scheduleFrom = Mage::getModel('core/date')->gmtTimestamp($schedule->getFromDate());
            $scheduleTo = Mage::getModel('core/date')->gmtTimestamp($schedule->getToDate());

            if (($fromDate >= $scheduleFrom) && ($fromDate <= $scheduleTo)) {
                $schedules[] = $schedule;
            }

            if (($toDate <= $scheduleTo) && ($toDate >= $scheduleFrom)) {
                $schedules[] = $schedule;
            }

            if (($fromDate <= $scheduleFrom && $fromDate <= $scheduleTo) && ($toDate >= $scheduleTo)) {
                $schedules[] = $schedule;
            }
        }

        return $schedules;
    }

    /**
     * Convert dates to input in database
     *
     * @param $strDate
     * @return false|string
     */
    public function convertDate($strDate)
    {
        return date(
            'Y-m-d H:i:s', Mage::getModel('core/date')->gmtTimestamp(strtotime(str_replace('/', '-', $strDate)))
        );
    }
}