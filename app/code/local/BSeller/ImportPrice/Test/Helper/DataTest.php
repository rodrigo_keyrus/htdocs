<?php
/**
 * BSeller Platform | B2W - Companhia Digital
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category  BSeller
 * @package   BSeller_ImportPrice
 *
 * @copyright Copyright (c) 2017 B2W Digital - BSeller Platform. (http://www.bseller.com.br)
 *
 * @author   Weverson Cachinsky <weverson.cachinsky@e-smart.com.br>
 */
class BSeller_ImportPrice_Test_Helper_DataTest extends EcomDev_PHPUnit_Test_Case
{
    /** @var BSeller_ImportPrice_Helper_Data */
    private $helper;

    public function setUp()
    {
        $this->helper = Mage::helper('bseller_importprice');
        $observerMock = $this->getModelMock('bundle/observer', ['loadProductOptions']);
        $this->replaceByMock('model', 'bundle/observer', $observerMock);
    }

    /**
     * @loadFixture grouped_product
     */
    public function testGetLastSkuReturnsTheCorrectOne()
    {
        $this->assertEquals('PRD00999', $this->helper->getLastSku());
    }

    /**
     * @loadFixture grouped_product
     */
    public function testGenerateSkuSums1ToTheLastOne()
    {
        $this->assertEquals('PRD01000', $this->helper->generateGroupedSku());
    }

    /**
     * @loadFixture schedule
     * @dataProvider dataProvider
     */
    public function testSkuHasDateConflictReturnFalse($dateFrom, $dateTo)
    {
        $this->assertFalse(
            $this->helper->skuHasDateConflict('123', strtotime($dateFrom), strtotime($dateTo))
        );
    }

    /**
     * @loadFixture schedule
     * @dataProvider dataProvider
     */
    public function testSkuHasDateConflictReturnTrue($dateFrom, $dateTo)
    {
        $this->assertTrue(
            $this->helper->skuHasDateConflict('123', strtotime($dateFrom), strtotime($dateTo))
        );
    }
}
