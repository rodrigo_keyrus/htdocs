<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_ImportPrice
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Evandro Veloso Gomes <evandro.gomes@e-smart.com.br>
 */

class BSeller_ImportPrice_Adminhtml_Import_DateConflictsController extends Mage_Adminhtml_Controller_Action
{
    protected function initMessages()
    {
        $this->_getSession()->addNotice(
            $this->getHelper()->__(
                'The discount should not be lower than %s%% nor bigger than %s%%',
                $this->getHelper()->getStoreConfig('min_discount'),
                $this->getHelper()->getStoreConfig('max_discount')
            )
        );

        $cron = Mage::getModel('aoe_scheduler/job')->load('bseller_importprice_schedule');

        $this->_getSession()->addNotice(
            sprintf(
                '%s <a target="_blank" href="https://crontab.guru/#%s">(%s)</a>',
                $this->getHelper()->__('The cron expression is %s', $cron->getCronExpression()),
                str_replace(' ', '_', $cron->getCronExpression()),
                $this->getHelper()->__('see explanation')
            )
        );
    }

    /**
     * Render grid block
     */
    public function indexAction()
    {
        $this->initMessages();

        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Update prices
     *
     */
    public function massUpdateAction()
    {
        $errors = 0;

        $ids = $this->getRequest()->getPost("id");
        if (is_array($ids)) {
            foreach ($ids as $id) {
                if (empty($id)) {
                    continue;
                }

                $conflict = Mage::getSingleton('bseller_importprice/conflictDate')->load($id);
                if ($conflict->getId()) {
                    try{
                        $this->_disableSchedulesFromConflict($conflict);

                        $histories = Mage::getSingleton('bseller_importprice/history')
                            ->getHistoryUsingConflict($conflict);

                        foreach ($histories as $history) {
                            $history->setOverwritten( 1 );
                            $history->save();
                        }

                        $historyId = Mage::getSingleton('bseller_importprice/history')
                            ->saveHistoryFromConflict($conflict);

                        Mage::getSingleton('bseller_importprice/schedule')
                            ->saveScheduleFromConflict($conflict, $historyId);

                        $conflict->delete();
                    } catch (Exception $e) {
                        $this->_getSession()->addNotice(
                            $this->__(
                                'Could not remove the conflict from the table: #%s',
                                $conflict->getId()
                            )
                        );

                        $errors++;
                    }
                }
            }
        }

        $this->_getSession()->addSuccess(
            $this->__('%d of %g Records imported', count($ids) - $errors, count($ids))
        )->setTableData(false);


        $this->_redirect('*/import_price/');
    }

    /**
     * Disable Schedule Based on a Conflict Register
     *
     * @param BSeller_ImportPrice_Model_ConflictDate $conflict
     *
     * @return bool
     * @throws Exception
     */
    private function _disableSchedulesFromConflict ( BSeller_ImportPrice_Model_ConflictDate $conflict)
    {
        $fromDate = Mage::getModel('core/date')->gmtTimestamp($conflict->getFromDate());
        $toDate = Mage::getModel('core/date')->gmtTimestamp($conflict->getToDate());

        $schedules = $this->getHelper()->skuHasDateConflicts($conflict->getSku(), $fromDate, $toDate);

        /** @var BSeller_ImportPrice_Model_Schedule $schedule */
        foreach($schedules as $schedule) {
            $schedule->setData('is_active', false);
            $schedule->save();
        }
    }

    /**
     * Render form new
     */
    public function newAction()
    {
        $this->_redirect('*/import_price/new');
    }

    /**
     * Return importprice helper
     *
     * @return BSeller_ImportPrice_Helper_Data
     */
    protected function getHelper()
    {
        return Mage::helper('bseller_importprice');
    }
    
    /**
     * Is allowed to access
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('bseller/bseller_importprice');
    }
}