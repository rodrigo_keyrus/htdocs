<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_ImportPrice
 *
 * @copyright Copyright (c) 2017 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Andre Manoel <andre.manoel@e-smart.com.br>
 */
class BSeller_ImportPrice_Adminhtml_Import_PriceController extends Mage_Adminhtml_Controller_Action
{
    protected $_errorcount = array();

    protected function initMessages()
    {
        $this->_getSession()->addNotice(
            $this->getHelper()->__(
                'The discount should not be lower than %s%% nor bigger than %s%%',
                $this->getHelper()->getStoreConfig('min_discount'),
                $this->getHelper()->getStoreConfig('max_discount')
            )
        );

        $cron = Mage::getModel('aoe_scheduler/job')->load('bseller_importprice_schedule');

        $this->_getSession()->addNotice(
            $this->getHelper()->__('The cron expression is %s', $cron->getCronExpression()) .
            sprintf(
                ' <a target="_blank" href="https://crontab.guru/#%s">(%s)</a>',
                str_replace(' ', '_', $cron->getCronExpression()),
                $this->getHelper()->__('see explanation')
            )
        );
    }

    /**
     * Render index page
     */
    public function indexAction()
    {
        $this->initMessages();

        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Render grid block
     */
    public function gridAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Render form new
     */
    public function newAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Redirect to import price grid
     */
    public function editAction()
    {
        return $this->_redirect('*/*/index');
    }

    /**
     * Save post data and redirect to index action
     *
     * @return false|Mage_Core_Controller_Varien_Action
     *
     * @throws Exception
     */
    public function saveAction()
    {
        if (!$this->getRequest()->isPost()) {
            return $this->_redirect('*/*/index');
        }

        $request  = $this->getRequest();
        $postData = $request->getParams();

        $error = false;

        if (!$postData['from_date']) {
            $error = "The from date can't be blank!";
        } elseif (!$postData['to_date']) {
            $error = "The from date can't be blank!";
        } else {
            $sNow = Mage::getModel ( 'core/date' )->date ( 'Y-m-d H:i:s' );
            $now = Mage::getModel ( 'core/date' )->gmtTimestamp ( $sNow );

            $fromDate = Mage::getModel ( 'core/date' )->gmtTimestamp ( $this->convertDate ( $postData[ 'from_date' ] ) );
            $toDate = Mage::getModel ( 'core/date' )->gmtTimestamp ( $this->convertDate ( $postData[ 'to_date' ] ) );

            if ( $fromDate < $now ) {
                $error = 'The start date should be greater then today';
            } elseif ( $toDate < $now ) {
                $error = 'The end date should be greater then today';
            } elseif ( $toDate < $fromDate ) {
                $error = 'The end date should be greater then from date';
            }
        }

        if ($error) {
            $this->_getSession()->addError(
                $this->__($error)
            );

            $this->_redirect('*/*/');
            return false;
        }

        $path     = basename($_FILES['file']['name']);
        move_uploaded_file($_FILES['file']['tmp_name'], $path);
        $fileData = $this->getHelper()->getCsvObject()->setDelimiter(";")->getData($path);

        if ($fileData[0][0] == "sku") {
            array_shift($fileData);
        }

        Mage::getResourceModel('bseller_importprice/conflictDate')->truncate();

        $dateConflicts = 0;
        foreach ($fileData as $data) {
            if ($postData['from_date'] || $postData['to_date']) {

                /** @var Mage_Catalog_Model_Product $product */
                $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $data[0]);

                if (!$product || $product->getTypeId() != 'simple') {
                    $this->_errorcount[] = $data[0];
                    continue;
                }

                /** @var float $price **/
                $price        = $product->getPrice();

                /** @var float $specialPrice **/
                $specialPrice = $product->getSpecialPrice();

                $schedules = $this->getHelper()->skuHasDateConflicts($data[0], $fromDate, $toDate);
                if (count($schedules)) {

                    $dateConflicts++;

                    $this->saveConflict(
                        $data[0],
                        $price,
                        $specialPrice,
                        $data[1],
                        $postData['description'],
                        $schedules[0]->getId(),
                        $postData['from_date'],
                        $postData['to_date']
                    );

                    continue;
                }

                $minDiscount = $this->getHelper()->getStoreConfig('min_discount');
                $maxDiscount = $this->getHelper()->getStoreConfig('max_discount');
                $discountTax = $price * $maxDiscount / 100;
                if ($data[1] <= 0 || $price - $data[1] < $minDiscount || $price - $data[1] > $discountTax) {
                    $this->_errorcount[] = $data[0];
                    continue;
                }

                $historyId = $this->saveHistory(
                    $data[0],
                    $price,
                    $data[1],
                    $postData['description'],
                    $postData['from_date'],
                    $postData['to_date']
                );

                $this->saveSchedule($data[0], $data[1], $historyId, $postData['from_date'], $postData['to_date']);
                continue;
            }
            $this->saveImportPrice($data[0], $data[1], $postData['description']);
        }

        $importcount = count($fileData) - count($this->_errorcount) - $dateConflicts;
        $this->_getSession()->addSuccess(
            $this->__('%d of %g Records imported', (string)$importcount, count($fileData))
        )->setTableData(false);
        if (count($this->_errorcount) > 0) {
            $this->_getSession()->addError(
                $this->__('SKUs with error: ') . implode(',', $this->_errorcount)
            );
        }
        unlink($_FILES['file']['name']);

        if ($dateConflicts) {
            $this->_redirect('*/import_dateConflicts');
            return;
        }

        $this->_redirect('*/*/');
        return;
    }

    /**
     * Delete employee by id and redirect to index action
     *
     * @return $this|Mage_Core_Controller_Varien_Action
     */
    public function deleteAction()
    {
        $id = $this->getRequest()->getParam('id');
        if (empty($id)) {
            $this->_getSession()->addError($this->__('Unable to find a employee to delete.'));
            return $this->_redirect('*/*/index');
        }

        $register = Mage::getModel('bseller_importprice/history');

        try {
            $register->load($id);
            $register->setStatus(0);
            $register->save();
            $this->_getSession()->addSuccess($this->__('The import record has been deleted.'));
        } catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        }
        return $this->_redirect('*/*/index');
    }

    /**
     * Mass delete imported prices
     *
     * @return $this|Mage_Core_Controller_Varien_Action
     */
    public function massDeleteAction()
    {
        $ids = $this->getRequest()->getParam('history_id');
        if (!is_array($ids)) {
            $this->_getSession()->addError($this->__('Please select a import item.'));
            return $this->_redirect('*/*/index');
        }
        try {
            $history = Mage::getModel('bseller_importprice/history');
            foreach ($ids as $id) {
                $history->load($id)->setStatus(0);
                $toDate = date(
                    'Y-m-d H:i:s', Mage::getModel('core/date')->gmtTimestamp(
                        strtotime(str_replace('/', '-', Mage::getModel('core/date')->date('Y-m-d H:i:s')))
                    )
                );

                if(strtotime($history->getFromDate()) > strtotime($toDate)){
                    $history->setFromDate($toDate);
                }

                $history->setToDate($toDate)->save();

                $schedule = Mage::getModel('bseller_importprice/schedule')->getScheduleByHistory($id);
                if ($schedule->getId()) {
                    $schedule->delete();
                }

                $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $history->getSku());
                $product->setSpecialPrice('');
                $product->setSpecialToDate('');
                $product->setSpecialFromDate('');
                $product->save();
            }
            $this->_getSession()->addSuccess(
                $this->__('Total of %d record(s) were deleted.', count($ids))
            );
        } catch (Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        }
        return $this->_redirect('*/*/index');
    }

    /**
     * Save Product Special Price
     *
     * @param $sku
     * @param $price
     * @param $description
     * @return bool
     */
    protected function saveImportPrice($sku, $price, $description)
    {
        $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
        if (!$product || $product->getTypeId() != 'simple') {
            $this->_errorcount[] = $sku;
            return false;
        }

        $product->setSpecialPrice($price);
        $product->setSpecialFromDate('');
        $product->setSpecialToDate('');
        $product->save();

        $this->saveHistory($sku, $product->getPrice(), $price, $description);
    }

    /**
     * * Save History Data
     *
     * @param $sku
     * @param $price
     * @param $special
     * @param $description
     * @param null $date
     * @return mixed
     */
    protected function saveHistory($sku, $price, $special, $description, $strFromDate = null, $strToDate = null)
    {
        $created = date(
            'Y-m-d H:i:s', Mage::getModel('core/date')->gmtTimestamp(
                strtotime(str_replace('/', '-', Mage::getModel('core/date')->date('Y-m-d H:i:s')))
            )
        );

        $fromDate = null;
        $toDate   = null;

        if ($strFromDate) {
            $fromDate = date(
                'Y-m-d H:i:s', Mage::getModel('core/date')->gmtTimestamp(strtotime(str_replace('/', '-', $strFromDate)))
            );
        }
        if ($strToDate) {
            $toDate = date(
                'Y-m-d H:i:s', Mage::getModel('core/date')->gmtTimestamp(strtotime(str_replace('/', '-', $strToDate)))
            );
        }

        $admUser = Mage::getSingleton('admin/session')->getUser()->getUsername();
        $history = Mage::getModel('bseller_importprice/history');
        $history->setSku($sku);
        $history->setPrice($price);
        $history->setSpecialPrice($special);
        $history->setStatus(1);
        $history->setUser($admUser);
        $history->setDescription($description);
        $history->setFromDate($fromDate);
        $history->setToDate($toDate);
        $history->setCreatedAt($created);
        $history->save();

        return $history->getId();
    }

    /**
     * @param string       $sku                   Sku of of the product
     * @param float        $price                 New price of the product
     * @param float        $current_special_price Current special price of the product
     * @param float        $new_special_price     New special price of the product
     * @param string       $description           Description of this import
     * @param int          $scheduleId            Id of the schedule register (BSeller_ImportPrice_Model_Schedule)
     * @param string|false $strFromDate           Initial date of the price
     * @param string|false $strToDate             Final date of the price
     *
     * @return mixed
     *
     * @throws Exception
     */
    protected function saveConflict(
        $sku,
        $price,
        $current_special_price,
        $new_special_price,
        $description,
        $scheduleId,
        $strFromDate = false,
        $strToDate = false
    )
    {
        $fromDate = null;
        $toDate   = null;

        if ($strFromDate) {
            $fromDate = $this->getHelper()->convertDate($strFromDate);
        }

        if ($strToDate) {
            $toDate = $this->getHelper()->convertDate($strToDate);
        }

        $conflict = Mage::getModel('bseller_importprice/conflictDate');
        $conflict->setSku($sku);
        $conflict->setPrice($price);
        $conflict->setCurrentSpecialPrice($current_special_price);
        $conflict->setNewSpecialPrice($new_special_price);
        $conflict->setDescription($description);
        $conflict->setFromDate($fromDate);
        $conflict->setToDate($toDate);
        $conflict->setScheduleId($scheduleId);
        $conflict->save();

        return $conflict->getId();
    }

    /**
     * Export grid to CSV
     */
    public function exportImportpriceCsvAction()
    {
        $fileName = 'bseller_importprice_imported.csv';
        $grid = $this->getLayout()->createBlock('bseller_importprice/adminhtml_import_price_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }

    /**
     * Export grid to XML
     */
    public function exportImportpriceExcelAction()
    {
        $fileName = 'bseller_importprice_imported.xml';
        $grid = $this->getLayout()->createBlock('bseller_importprice/adminhtml_import_price_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }

    /**
     * Save schedule of imported prices
     *
     * @param string       $sku           Sku of the product
     * @param float        $special_price New special price of the pr
     * @param string|false $strFromDate   Initial date of the price
     * @param string|false $strToDate     Final date of the price
     *
     * @throws Exception
     */
    private function  saveSchedule($sku, $special_price, $historyId, $strFromDate = false, $strToDate = false)
    {
        $toDate    = null;
        $fromDate  = null;

        if ($strFromDate) {
            $fromDate = $this->convertDate($strFromDate);
        }
        if ($strToDate) {
            $toDate = $this->convertDate($strToDate);
        }

        /** @var BSeller_ImportPrice_Model_Schedule $schedule */
        $schedule = Mage::getModel('bseller_importprice/schedule');
        $schedule->setSku($sku);
        $schedule->setSpecialPrice($special_price);
        $schedule->setIsActive(true);
        $schedule->setFromDate($fromDate);
        $schedule->setToDate($toDate);
        $schedule->setHistoryId($historyId);
        $schedule->save();
    }

    /**
     * Return employee helper
     *
     * @return BSeller_ImportPrice_Helper_Data
     */
    protected function getHelper()
    {
        return Mage::helper('bseller_importprice');
    }

    /**
     * Is allowed to access
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('bseller/bseller_importprice');
    }

    /**
     * @param $dates
     * @return false|string
     */
    private function convertDate($date)
    {
        return date(
            'Y-m-d H:i:s', Mage::getModel('core/date')->gmtTimestamp(strtotime(str_replace('/', '-', $date)))
        );
    }
}