<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_ImportPrice
 *
 * @copyright Copyright (c) 2017 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Andre Manoel <andre.manoel@e-smart.com.br>
 */

class BSeller_ImportPrice_Adminhtml_Import_HistoryController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Render index page
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }
    /**
     * Render grid block
     */
    public function gridAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function exportImportpriceCsvAction()
    {
        $fileName = 'bseller_importprice_history.csv';
        $grid = $this->getLayout()->createBlock('bseller_importprice/adminhtml_import_history_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }

    public function exportImportpriceExcelAction()
    {
        $fileName = 'bseller_importprice_history.xml';
        $grid = $this->getLayout()->createBlock('bseller_importprice/adminhtml_import_history_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }

    /**
     * Redirect to history price grid
     */
    public function editAction()
    {
        return $this->_redirect('*/*/index');
    }

    /**
     * Return importprice helper
     *
     * @return BSeller_ImportPrice_Helper_Data
     */
    protected function getHelper()
    {
        return Mage::helper('bseller_importprice');
    }

    /**
     * Is allowed to access
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('bseller/bseller_importprice');
    }

}