<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_ImportPrice
 *
 * @copyright Copyright (c) 2017 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    GUilherme Miguelete <guilherme.miguelete@e-smart.com.br>
 */
class BSeller_ImportPrice_Adminhtml_Import_ProductController extends Mage_Adminhtml_Controller_Action
{

    /**
     * Render form new
     */
    public function newAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }


    /**
     * Save post data and redirect to index action
     *
     * @return $this|Mage_Core_Controller_Varien_Action
     */
    public function saveAction()
    {

        if (!$this->getRequest()->isPost()) {
            $this->_getSession()->addError('Post Only Accepted');
            return $this->_redirect('*/*/new');
        }

        $data = $this->getRequest()->getParams();

        Mage::getSingleton('adminhtml/session')->setData('bseller_catalog_product_data', $data);

        if (!is_array($data)) {
            $this->_getSession()->addError('Data is Required');
            return $this->_redirect('*/*/new');
        }

        /** @var Mage_Catalog_Model_Product $product */
        $product = Mage::getModel('catalog/product');

        /** set defaults attributes */
        $product->setTypeId('grouped');
        $product->setAttributeSetId(Mage::getModel('catalog/product')->getDefaultAttributeSetId());
        $product->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH);
        $product->setStatus(Mage_Catalog_Model_Product_Status::STATUS_ENABLED);
        $product->setTaxClassId(0);

        /** set category ids on product */
        $this->_associateCategories($data['bseller_subcategory'], $product);

        /** Adding Informed Atributes in csv to product data model */
        $product->addData($data);
        $product->setSku($this->getHelper()->generateGroupedSku());

        try {

            $product->save();

            $catalogInventory = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product);
            if (!$catalogInventory->hasData('product_id')) {
                $catalogInventory = Mage::getModel('cataloginventory/stock_item');
                $catalogInventory->setProduct($product);
                $catalogInventory->setStockId(1);
                $catalogInventory->save();
            }

            /** @var Mage_Catalog_Model_Product_Link_Api $productsLinks */
            $productsLinks = Mage::getModel('catalog/product_link_api');

            $childrenProducts = explode(',', $data['associados']);

            foreach ($childrenProducts as $sku) {

                $children = Mage::getModel('catalog/product')->loadByAttribute('sku', trim($sku));

                if (!($children instanceof  Mage_Catalog_Model_Product)) {
                    throw new Exception($this->__('Children sku not found: %s', $sku));
                }

                if (!($children->getId())) {
                    throw new Exception($this->__('This product is not valid, please redefine sku: %s', $sku));
                }

                if ($productsLinks->assign('grouped', $product->getId(), $children->getId())) {
                    $children->setData('ax_parent_code', $data['sku']);
                    $children->setCategoryIds($product->getCategoryIds());
                    $children->save();
                }

            }
            Mage::getSingleton('adminhtml/session')->setData('bseller_catalog_product_data', []);
            $this->_getSession()->addSuccess($this->__('Product Successfully saved. Sku generate "%s"', $product->getSku()));

        } catch (Exception $ex) {
            Mage::logException($ex);
            $this->_getSession()->addError($ex->getMessage());
        }

        $this->_redirect('*/*/new');

    }



    /**
     *
     * @param string $bSellerCode
     * @param Mage_Catalog_Model_Product $groupedProduct
     * @return Mage_Catalog_Model_Product
     */
    protected function _associateCategories($bSellerCode, $groupedProduct)
    {
        /** @var Mage_Catalog_Model_Category $subCategory */
        $subCategory = Mage::getModel('catalog/category');
        $object      = $subCategory->loadByAttribute('bseller_code', $bSellerCode);

        if ($object instanceof Mage_Catalog_Model_Abstract) {
            $subCategory = $object;
        }

        $category   = $subCategory->getParentCategory();
        $categoryId = $subCategory->getParentId();

        $departmentId = $category->getParentId();

        $categoryIds = [
            (int) $subCategory->getId(),
            (int) $categoryId,
            (int) $departmentId
        ];

        $groupedProduct->setCategoryIds($categoryIds);
        return $groupedProduct;
    }



    /**
     * Return employee helper
     *
     * @return BSeller_ImportPrice_Helper_Data
     */
    protected function getHelper()
    {
        return Mage::helper('bseller_importprice');
    }

    /**
     * Is allowed to access
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('bseller/bseller_importprice');
    }
}