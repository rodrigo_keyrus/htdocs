<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_ImportPrice
 *
 * @copyright Copyright (c) 2017 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    GUilherme Miguelete <guilherme.miguelete@e-smart.com.br>
 */

class BSeller_ImportPrice_Block_Adminhtml_Import_Product_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Prepare edit form.
     *
     * @return Mage_Adminhtml_Block_Widget_Form
     * @throws Exception
     */
    protected function _prepareForm()
    {
        $this->createForm();
        $object = $this->helper('bseller_importprice')->getObjectInfo();
        $this->createFormFields($object);

        $data = Mage::getSingleton('adminhtml/session')->getData('bseller_catalog_product_data');
        $form = $this->getForm();
        $form->setValues($data);

        return parent::_prepareForm();
    }

    /**
     * Create edit form.
     *
     * @return void
     * @throws Exception
     */
    protected function createForm()
    {
        $form = new Varien_Data_Form(
            array(
                'id'            => 'edit_form',
                'action'        => $this->getUrl('*/*/save'),
                'method'        => 'post',
                'use_container' => true
            )
        );

        $this->setForm($form);
    }

    /**
     * Create form fields.
     *
     * @return void
     */
    protected function createFormFields($object)
    {

        $table = $this->getForm()->addFieldset(
            'importprice_form_table',
            array(
                'legend' => $this->__('General')
            )
        );

        $table->addField(
            'sku',
            'hidden',
            array(
                'label'       => $this->__('Sku'),
                'required'    => false,
                'name'        => 'sku',
                'placeholder' => 'sku automático'
              
            )
        );

        $table->addField(
            'name',
            'text',
            array(
                'label'     => $this->__('Nome'),
                'required'  => true,
                'name'      => 'name'
            )
        );

        $table->addField(
            'manufacturer',
            'select',
            [
                'name'     => 'manufacturer',
                'label'    => $this->__('Manufacturer'),
                'options'  => Mage::getSingleton('bseller_giftshowcase/system_config_source_manufacturer')->toArray(),
                'required' => true
            ]
        );


        $table->addField(
            'description',
            'textarea',
            [
                'name'     => 'description',
                'label'    => $this->__('Description')
            ]
        );

        $table->addField(
            'status',
            'select',
            [
                'name'     => 'status',
                'label'    => $this->__('Status'),
                'options'  => Mage::getSingleton('bseller_giftshowcase/system_config_source_status')->toArray(),
                'required' => true
            ]
        );


        $table->addField(
            'tax_treatment',
            'select',
            [
                'name'     => 'tax_treatment',
                'label'    => $this->__('Tax Treatment'),
                'options'  => [
                    1 => 'Produtos',
                    2 => 'Amostra',
                    3 => 'Brinde'
                ],
                'required' => true
            ]
        );

        $table->addField(
            'associados',
            'text',
            [
                'name'     => 'associados',
                'label'    => $this->__('Associados'),
                'required' => true
            ]
        );



        $table->addField(
            'bseller_manufacturer',
            'text',
            [
                'name'     => 'bseller_manufacturer',
                'label'    => $this->__('Bseller Manufacturer')
            ]
        );

        $table->addField(
            'bseller_department',
            'text',
            [
                'name'     => 'bseller_department',
                'label'    => $this->__('Bseller Department')
            ]
        );

        $table->addField(
            'bseller_category',
            'text',
            [
                'name'     => 'bseller_category',
                'label'    => $this->__('Bseller Category')
            ]
        );

        $table->addField(
            'bseller_subcategory',
            'text',
            [
                'name'     => 'bseller_subcategory',
                'label'    => $this->__('Bseller Sub-Category')
            ]
        );

        $table->addField(
            'ncm',
            'text',
            [
                'name'     => 'ncm',
                'label'    => $this->__('NCM')
            ]
        );

        $table->addField(
            'youtube_url',
            'text',
            [
                'name'     => 'youtube_url',
                'label'    => $this->__('Youtube URL')
            ]
        );

        $table->addField(
            'selo_exclusivo',
            'select',
            [
                'name'     => 'selo_exclusivo',
                'label'    => $this->__('Selo Exclusivo'),
                'options'  => [
                    0 => 'Não',
                    1 => 'Sim'
                ],
                'required' => true
            ]
        );

        $table->addField(
            'selo_online',
            'select',
            [
                'name'     => 'selo_online',
                'label'    => $this->__('Selo Online'),
                'options'  => [
                    0 => 'Não',
                    1 => 'Sim'
                ],
                'required' => true
            ]
        );

        $table->addField(
            'selo_limitada',
            'select',
            [
                'name'     => 'selo_limitada',
                'label'    => $this->__('Selo Limitada'),
                'options'  => [
                    0 => 'Não',
                    1 => 'Sim'
                ],
                'required' => true
            ]
        );


        $table->addField(
            'howto',
            'textarea',
            [
                'name'     => 'howto',
                'label'    => $this->__('How To')
            ]
        );


        $table->addField(
            'composition',
            'textarea',
            [
                'name'     => 'composition',
                'label'    => $this->__('Composition')
            ]
        );






    }
}