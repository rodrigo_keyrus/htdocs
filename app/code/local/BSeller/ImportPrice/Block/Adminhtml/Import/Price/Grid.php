<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_ImportPrice
 *
 * @copyright Copyright (c) 2017 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Andre Manoel <andre.manoel@e-smart.com.br>
 */

class BSeller_ImportPrice_Block_Adminhtml_Import_Price_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * BSeller_ImportPrice_Block_Adminhtml_Import_Price_Grid constructor
     */
    public function __construct()
    {
        parent::__construct();

        $this->setId('bseller_importprice_grid');
        $this->setDefaultSort('created_at');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    /**
     * Prepare grid collection
     *
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('bseller_importprice/history')
            ->getCollection();

        $this->setCollection($collection);
        parent::_prepareCollection();

        return $this;
    }

    /**
     * Prepare grid columns
     *
     * @return $this
     * @throws Exception
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'sku',
            array(
                'header' => $this->__('Product SKU'),
                'align'  => 'right',
                'width'  => '110px',
                'index'  => 'sku'
            )
        );

        $this->addColumn(
            'price',
            array(
                'header' => $this->__('Price'),
                'align'  => 'right',
                'width'  => '110px',
                'index'  => 'price'
            )
        );

        $this->addColumn(
            'special_price',
            array(
                'header' => $this->__('Special Price'),
                'align'  => 'right',
                'width'  => '110px',
                'index'  => 'special_price'
            )
        );

        $this->addColumn(
            'description',
            array(
                'header' => $this->__('Description'),
                'align'  => 'left',
                'index'  => 'description'
            )
        );

        $this->addColumn(
            'user',
            array(
                'header'  => $this->__('Admin user'),
                'align'   => 'left',
                'width'   => '140px',
                'index'   => 'user'
            )
        );

        $this->addColumn(
            'from_date',
            array(
                'header' => $this->__('Price from Date'),
                'type'   => 'datetime',
                'width'  => '140px',
                'format' => 'dd/MM/yyyy HH:mm',
                'index'  => 'from_date'
            )
        );

        $this->addColumn(
            'to_date',
            array(
                'header' => $this->__('Price to Date'),
                'type'   => 'datetime',
                'width'  => '140px',
                'format' => 'dd/MM/yyyy HH:mm',
                'index'  => 'to_date'
            )
        );

        $this->addColumn(
            'created_at',
            array(
                'header' => $this->__('Date of import'),
                'type'   => 'datetime',
                'width'  => '140px',
                'format' => 'dd/MM/yyyy HH:mm',
                'index'  => 'created_at'
            )
        );

        $this->addColumn('overwritten', array(
            'header'    => $this->__('Overwritten'),
            'align'     => 'left',
            'width'     => '80px',
            'index'     => 'overwritten',
            'type'      => 'options',
            'options'   => array(
                1 => $this->__('Yes'),
                0 => $this->__('No')
            ),
        ));

        $this->addExportType('*/*/exportImportpriceCsv', $this->__('CSV'));
        $this->addExportType('*/*/exportImportpriceExcel', $this->__('Excel XML'));

        return parent::_prepareColumns();
    }

    /**
     * Return grid url
     *
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current' => true));
    }

    /**
     * Return row url
     *
     * @param Varien_Object $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    protected function _prepareMassaction()
    {
        parent::_prepareMassaction();

        $this->setMassactionIdField('bseller_importprice_grid_filter_sku');
        $this->getMassactionBlock()->setFormFieldName('history_id');

        $this->getMassactionBlock()->addItem(
            'delete', array(
                'label'   => $this->__('Delete'),
                'url'     => $this->getUrl('*/*/massDelete', array('' => '')),
                'confirm' => $this->__('Are you sure?')
            )
        );

        return $this;
    }
}
