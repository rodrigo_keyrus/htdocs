<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_ImportPrice
 *
 * @copyright Copyright (c) 2017 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Andre Manoel <andre.manoel@e-smart.com.br>
 */

class BSeller_ImportPrice_Block_Adminhtml_Import_Price_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Prepare edit form.
     *
     * @return Mage_Adminhtml_Block_Widget_Form
     * @throws Exception
     */
    protected function _prepareForm()
    {
        $this->createForm();
        $object = $this->helper('bseller_importprice')->getObjectInfo();
        $this->createFormFields($object);

        return parent::_prepareForm();
    }

    /**
     * Create edit form.
     *
     * @return void
     * @throws Exception
     */
    protected function createForm()
    {
        $form = new Varien_Data_Form(
            array(
                'id'            => 'edit_form',
                'action'        => $this->getUrl('*/*/save'),
                'method'        => 'post',
                'use_container' => true,
                'enctype'       => 'multipart/form-data'
            )
        );

        $this->setForm($form);
    }

    /**
     * Create form fields.
     *
     * @return void
     */
    protected function createFormFields($object)
    {
        $table = $this->getForm()->addFieldset(
            'importprice_form_table',
            array(
                'legend' => $this->__('General')
            )
        );

        $table->addField(
            'file',
            'file',
            array(
                'label'     => $this->__('File'),
                'required'  => true,
                'name'      => 'file'
            )
        );

        $table->addField(
            'description',
            'text',
            array(
                'label'     => $this->__('Description'),
                'required'  => true,
                'name'      => 'description'
            )
        );

        $dateTimeFormatIso = Mage::app()->getLocale()->getDateTimeFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
        $table->addField(
            'from_date',
            'datetime',
            array(
                'label'    => $this->__('Price from Date'),
                'time'     => true,
                'required' => false,
                'name'     => 'from_date',
                'tabindex' => 0,
                'image'    => $this->getSkinUrl('images/grid-cal.gif'),
                'format'   => $dateTimeFormatIso ,
            )
        );

        $table->addField(
            'to_date',
            'datetime',
            array(
                'label'    => $this->__('Price to Date'),
                'time'     => true,
                'required' => false,
                'name'     => 'to_date',
                'tabindex' => 1,
                'image'    => $this->getSkinUrl('images/grid-cal.gif'),
                'format'   => $dateTimeFormatIso,
            )
        );
    }
}