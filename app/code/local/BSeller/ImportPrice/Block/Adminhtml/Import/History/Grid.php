<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_SephoraEmployees
 *
 * @copyright Copyright (c) 2017 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Andre Manoel <andre.manoel@e-smart.com.br>
 */

class BSeller_ImportPrice_Block_Adminhtml_Import_History_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * BSeller_ImportPrice_Block_Adminhtml_Import_History_Grid constructor
     */
    public function __construct()
    {
        parent::__construct();

        $this->setId('bseller_importprice_history_grid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    /**
     * Prepare grid collection
     *
     * @return $this
     */
    protected function _prepareCollection()
    {
        /** @var BSeller_ImportPrice_Model_Resource_History_Collection $collection */
        $collection = Mage::getResourceModel("bseller_importprice/history_collection");
        $this->setCollection($collection);
        parent::_prepareCollection();

        return $this;
    }

    /**
     * Prepare grid columns
     *
     * @return $this
     * @throws Exception
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'sku',
            array(
                'header' => $this->__('Product SKU'),
                'align'  => 'right',
                'width'  => '120px',
                'index'  => 'sku'
            )
        );

        $this->addColumn(
            'price',
            array(
                'header' => $this->__('Price'),
                'align'  => 'right',
                'width'  => '120px',
                'index'  => 'price'
            )
        );

        $this->addColumn(
            'special_price',
            array(
                'header' => $this->__('Special Price'),
                'align'  => 'right',
                'width'  => '120px',
                'index'  => 'special_price'
            )
        );

        $this->addColumn(
            'user',
            array(
                'header'  => $this->__('Admin user'),
                'align'   => 'left',
                'width'   => '120px',
                'index'   => 'user'
            )
        );

        $this->addColumn(
            'description',
            array(
                'header' => $this->__('Description'),
                'align'  => 'left',
                'index'  => 'description'
            )
        );

        $this->addColumn(
            'created_at',
            array(
                'header' => $this->__('Date of import'),
                'type'   => 'datetime',
                'width'  => '140px',
                'format' => 'dd/MM/yyyy HH:mm',
                'index'  => 'created_at'
            )
        );

        $this->addColumn(
            'from_date',
            array(
                'header' => $this->__('From date'),
                'type'   => 'datetime',
                'width'  => '140px',
                'format' => 'dd/MM/yyyy HH:mm',
                'index'  => 'from_date'
            )
        );

        $this->addColumn(
            'to_date',
            array(
                'header' => $this->__('Expiration date'),
                'type'   => 'datetime',
                'width'  => '140px',
                'format' => 'dd/MM/yyyy HH:mm',
                'index'  => 'to_date'
            )
        );

        $this->addExportType('*/*/exportImportpriceCsv', $this->__('CSV'));
        $this->addExportType('*/*/exportImportpriceExcel', $this->__('Excel XML'));

        return parent::_prepareColumns();
    }

    /**
     * Return grid url
     *
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current' => true));
    }

    /**
     * Return row url
     *
     * @param Varien_Object $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }
}