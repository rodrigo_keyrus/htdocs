<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_ImportPrice
 *
 * @copyright Copyright (c) 2017 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Andre Manoel <andre.manoel@e-smart.com.br>
 */

class BSeller_ImportPrice_Block_Adminhtml_Import_Price_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * BSeller_ImportPrice_Block_Adminhtml_Import_Price_Edit constructor
     */
    public function __construct()
    {
        parent::__construct();

        $this->_controller = 'adminhtml_import_price';
        $this->_blockGroup = 'bseller_importprice';
        $this->_headerText = $this->__('Update Price');
    }
}
