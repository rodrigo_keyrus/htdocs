<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_ImportPrice
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Evandro Veloso Gomes <evandro.gomes@e-smart.com.br>
 */

class BSeller_ImportPrice_Block_Adminhtml_Import_DateConflicts_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * BSeller_ImportPrice_Block_Adminhtml_Import_DateConflicts_Grid constructor
     */
    public function __construct()
    {
        parent::__construct();

        $this->setId('bseller_import_conflictDate_grid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(false);
    }

    /**
     * Prepare grid collection
     *
     * @return $this
     */
    protected function _prepareCollection()
    {
        /** @var Varien_Data_Collection $collection */
        $collection = Mage::getModel('bseller_importprice/conflictDate')->getCollection();
        $this->setCollection($collection);
        parent::_prepareCollection();

        return $this;
    }

    /**
     * Prepare grid columns
     *
     * @return $this
     * @throws Exception
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'sku',
            array(
                'header' => $this->__('Product SKU'),
                'align'  => 'right',
                'width'  => '110px',
                'index'  => 'sku'
            )
        );

        $this->addColumn(
            'price',
            array(
                'header' => $this->__('Price'),
                'align'  => 'right',
                'width'  => '110px',
                'index'  => 'price'
            )
        );

        $this->addColumn(
            'current_special_price',
            array(
                'header' => $this->__('Current Special Price'),
                'align'  => 'right',
                'width'  => '110px',
                'index'  => 'current_special_price'
            )
        );

        $this->addColumn(
            'new_special_price',
            array(
                'header' => $this->__('New Special Price'),
                'align'  => 'right',
                'width'  => '110px',
                'index'  => 'new_special_price'
            )
        );

        return parent::_prepareColumns();
    }

    /**
     * Return grid url
     *
     * @return bool
     */
    public function getGridUrl()
    {
        return false;
    }

    /**
     * Return row url
     *
     * @param Varien_Object $row
     * @return bool
     */
    public function getRowUrl($row)
    {
        return false;
    }

    /**
     * Add mass-actions to grid
     *
     * @return BSeller_ImportPrice_Block_Adminhtml_Import_DateConflicts_Grid
     */
    protected function _prepareMassaction()
    {
        parent::_prepareMassaction();

        $this->setMassactionIdField('bseller_importprice_grid_filter_sku');
        $this->getMassactionBlock()->setFormFieldName('id');

        $this->getMassactionBlock()->addItem(
            'update',
            array(
                'label'   => $this->__('Update selected prices'),
                'url'     => $this->getUrl('*/*/massUpdate', array('' => '')),
                'confirm' => $this->__('Are you sure?')
            )
        );

        return $this;
    }
}
