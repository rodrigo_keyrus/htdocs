<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_ImportPrice
 *
 * @copyright Copyright (c) 2017 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Andre Manoel <andre.manoel@e-smart.com.br>
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$tableName = $installer->getTable('bseller_importprice/importprice_history');
if (!$installer->getConnection()->isTableExists($tableName)) {
    $newTable = $installer->getConnection()->newTable($tableName)
        ->addColumn(
            'id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, [
                'unsigned' => true,
                'nullable' => false,
                'primary'  => true,
                'identity' => true,
            ], 'Primary key - ID'
        )
        ->addColumn(
            'sku', Varien_Db_Ddl_Table::TYPE_TEXT, null, [
                'unsigned' => true,
                'nullable' => false,
            ], 'SKU'
        )
        ->addColumn(
            'price', Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,2', [
                'unsigned' => true,
                'nullable' => false,
            ], 'Price'
        )
        ->addColumn(
            'special_price', Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,2', [
                'unsigned' => true,
                'nullable' => false,
            ], 'Special Price'
        )
        ->addColumn(
            'to_date', Varien_Db_Ddl_Table::TYPE_DATETIME, null, [
                'nullable' => false,
            ], 'Price to Date'
        )
        ->addColumn(
            'status', Varien_Db_Ddl_Table::TYPE_BOOLEAN, null, [
                'unsigned' => true,
                'nullable' => false,
            ], 'Status'
        )
        ->addColumn(
            'user', Varien_Db_Ddl_Table::TYPE_TEXT, null, [
                'unsigned' => true,
                'nullable' => false,
            ], 'Admin User'
        )
        ->addColumn(
            'description', Varien_Db_Ddl_Table::TYPE_TEXT, null, [
                'unsigned' => true,
                'nullable' => false,
            ], 'Description'
        )
        ->addColumn(
            'created_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, [
                'nullable' => false,
            ], 'Date of history'
        );

    $installer->getConnection()->createTable($newTable);
}

$installer->endSetup();