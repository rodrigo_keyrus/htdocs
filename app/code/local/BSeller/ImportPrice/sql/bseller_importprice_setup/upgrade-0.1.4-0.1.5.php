<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_ImportPrice
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Evandro Veloso Gomes <evandro.gomes@e-smart.com.br>
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

/** @var Varien_Db_Adapter_Pdo_Mysql $connection */
$connection = $installer->getConnection();

$installer->getConnection()->changeColumn($installer->getTable('bseller_importprice/importprice_conflict_date'),
    'special_price',
    'current_special_price',
    [
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'length'  => '12,2',
        'comment' => 'Current Special Price'
    ]
);

$connection->addColumn($installer->getTable('bseller_importprice/importprice_conflict_date'),
    'new_special_price',
    [
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'length'  => '12,2',
        'after'   => 'current_special_price',
        'comment' => 'New Special Price'
    ]
);

$installer->endSetup();