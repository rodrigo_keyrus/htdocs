<?php
/**
 * B Seller Platform | B2W Digital
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_MaxLength
 *
 * @copyright Copyright (c) 2017 B2W Digital - B Seller Platform (http://www.bseller.com.br)
 *
 * @author    Tiago Daniel <tiago.daniel@e-smart.com.br>
*/ 
class BSeller_MaxLength_Helper_Data extends Mage_Core_Helper_Abstract
{

    const FIRSTNAME         = 'firstname';
    const LASTNAME          = 'lastname';
    const EMAIL             = 'email';
    const STREET1           = 'street1';
    const STREET2           = 'street2';
    const STREET3           = 'street3';
    const STREET4           = 'street4';
    const CITY              = 'city';
    private $_maxLength     = 40;

    /**
     * Set maxlength to input fields below
     * @return int
     */
    public function getMaxLength($attributeCode)
    {

        if ($attributeCode == self::FIRSTNAME) {
            $this->_maxLength   = Mage::getStoreConfig('bseller_maxlength/settings/firstname_max');
            return $this->_maxLength ;
        }

        if ($attributeCode == self::LASTNAME) {
            $this->_maxLength     = Mage::getStoreConfig('bseller_maxlength/settings/lastname_max');
            return $this->_maxLength ;
        }

        if ($attributeCode == self::EMAIL) {
            $this->_maxLength    = Mage::getStoreConfig('bseller_maxlength/settings/email_max');
            return $this->_maxLength;
        }

        if ($attributeCode == self::STREET1) {
            $this->_maxLength    = Mage::getStoreConfig('bseller_maxlength/settings/street1_max');
            return $this->_maxLength;
        }

        if ($attributeCode == self::STREET2) {
            $this->_maxLength    = Mage::getStoreConfig('bseller_maxlength/settings/street2_max');
            return $this->_maxLength;
        }

        if ($attributeCode == self::STREET3) {
            $this->_maxLength    = Mage::getStoreConfig('bseller_maxlength/settings/street3_max');
            return $this->_maxLength;
        }

        if ($attributeCode == self::STREET4) {
            $this->_maxLength    = Mage::getStoreConfig('bseller_maxlength/settings/street4_max');
            return $this->_maxLength;
        }

        if ($attributeCode == self::CITY) {
            $this->_maxLength    = Mage::getStoreConfig('bseller_maxlength/settings/city_max');
            return $this->_maxLength;
        }

        return 255;
    }

}