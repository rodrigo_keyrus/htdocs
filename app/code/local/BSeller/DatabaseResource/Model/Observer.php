<?php
/**
 * BSeller Platform | B2W - Companhia Digital
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category  BSeller
 * @package   BSeller_DatabaseResource
 *
 * @copyright Copyright (c) 2018 B2W Digital - BSeller Platform. (http://www.bseller.com.br)
 *
 * @author    Bruno Gemelli <bruno.gemelli@e-smart.com.br>
 */
class BSeller_DatabaseResource_Model_Observer
{
    const SPECIAL_CONNECTION = 'bseller_special_database_resource';

    /**
     * Set special database resource info for future uses
     *
     * @param Varien_Event_Observer $observer
     */
    public function setSpecialDatabaseResource(Varien_Event_Observer $observer)
    {
        /** @var string $connectionName */
        $connectionName = $observer->getConnection() ? $observer->getConnection() : 'write';

        Mage::register(self::SPECIAL_CONNECTION, $connectionName, true);
    }


    /**
     * Unset special database resource info for future uses
     *
     * @param Varien_Event_Observer $observer
     */
    public function unsetSpecialDatabaseResource(Varien_Event_Observer $observer)
    {
        Mage::unregister(self::SPECIAL_CONNECTION);
    }
}