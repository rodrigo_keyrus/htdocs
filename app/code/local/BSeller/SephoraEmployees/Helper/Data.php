<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_SephoraEmployees
 *
 * @copyright Copyright (c) 2017 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Andre Manoel <andre.manoel@e-smart.com.br>
 */

class BSeller_SephoraEmployees_Helper_Data extends Mage_Core_Helper_Abstract
{
    const STOCK_OUT_OF_STOCK        = 0;
    const STOCK_IN_STOCK            = 1;

    /**
     * Register key
     *
     * @var string
     */
    protected $_registerKey = 'bseller_sephoraemployees';

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return (bool)Mage::getStoreConfig('bseller_sephoraemployees/settings/active');
    }

    /**
     * save object data in register
     * @param $value
     * @param bool $graceful
     * @return $this
     * @throws Mage_Core_Exception
     */
    public function addObjectInfo($value, $graceful = true)
    {
        Mage::register($this->_registerKey, $value, $graceful);

        return $this;
    }

    /**
     * Return object data of register
     *
     * @return Varien_Object
     */
    public function getObjectInfo()
    {
        if ($object = Mage::registry($this->_registerKey)) {
            return $object;
        }

        return new Varien_Object();
    }

    /**
     *  Return customer id
     *
     * @param $cpf
     * @return null
     */
    public function getCustomerIdByCpf($cpf)
    {
        $customer = Mage::getModel('customer/customer')->getCollection()
            ->addFieldToFilter('cpf', $cpf)->load()->getFirstItem();
        if ($customer->getId()) {
            $customer->setIsEmployee(true)->save();
            return $customer->getId();
        }
        return null;
    }

    /**
     * Update register of employee
     *
     * @param $customerId
     * @param $cpf
     * @return bool
     */
    public function updateCustomerEmployee($customerId, $cpf)
    {
        $register = Mage::getModel('bseller_sephoraemployees/register')->getCollection()
            ->addFieldToFilter('cpf', $cpf)->load()->getFirstItem();
        if ($register->getId()) {
            $register->setCustomerId($customerId)->save();
            return true;
        }
        return false;
    }

    /**
     * Return store config data
     *
     * @param $key
     * @return string
     */
    public function getStoreConfig($key)
    {
        return Mage::getStoreConfig('bseller_sephoraemployees/settings/' . $key);
    }

    public function getUploadPath()
    {
        return Mage::getBaseDir('media').DIRECTORY_SEPARATOR.'sephora-employees-uploader'.DIRECTORY_SEPARATOR;

    }

    /**
     * @param Mage_Customer_Model_Customer $customer
     * @return bool
     */
    public function isCustomerEmployee(Mage_Customer_Model_Customer $customer)
    {
        if (!$customer->getId()) {
            return false;
        }
        
        $register = Mage::getModel('bseller_sephoraemployees/register')->load($customer->getId(), 'customer_id');

        return $register->getCustomerId() == $customer->getId();
    }


    /**
     * @return bool
     */
    public function canProcessRuleIfCustomerIsEmployee()
    {
        return (bool) !Mage::getStoreConfig('bseller_firstorder/settings/disable_when_is_employee');
    }

    /**
     * @return bool
     */
    public function canProcessRuleIfAnotherRuleIsInApplied()
    {
        return (bool) !Mage::getStoreConfig('bseller_firstorder/settings/disable_when_catalog_has_rules');
    }

    /**
     * Checks if the rule can be processed due to module settings
     *
     * @param Mage_Sales_Model_Quote $quote
     * @return bool
     */
    public function canProcessRule(Mage_Sales_Model_Quote $quote)
    {
        if (!$this->isCustomerEmployee($quote->getCustomer())) {
            return false;
        }

        if (!$this->canProcessRuleIfAnotherRuleIsInApplied() && $quote->getAppliedRuleIds()) {
            return false;
        }

        return true;
    }
}
