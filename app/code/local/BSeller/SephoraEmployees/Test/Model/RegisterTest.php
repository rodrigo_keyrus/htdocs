<?php
/**
 * BSeller Platform | B2W - Companhia Digital
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category  BSeller
 * @package   BSeller_SephoraEmployees
 *
 * @copyright Copyright (c) 2017 B2W Digital - BSeller Platform. (http://www.bseller.com.br)
 *
 * @author   Weverson Cachinsky <weverson.cachinsky@e-smart.com.br>
 */
class BSeller_SephoraEmployees_Test_Model_RegisterTest extends EcomDev_PHPUnit_Test_Case
{
    /**
     * @return BSeller_SephoraEmployees_Model_Register|false|Mage_Core_Model_Abstract
     */
    protected function getRegisterModel()
    {
        return Mage::getModel('bseller_sephoraemployees/register');
    }

    /**
     * @param array $data
     * @return BSeller_SephoraEmployees_Model_Register
     */
    protected function registerEmployee($data)
    {
        $register = $this->getRegisterModel()
            ->setData($data)
            ->save();

        return $register;
    }

    /**
     * @expectedException Mage_Core_Exception
     * @expectedExceptionMessage The employee is not registered in this store.
     */
    public function testRegisterWithCPFNotMatchingAnyCustomerThrowsException()
    {
        $this->registerEmployee([
            'cpf' => '09876543211',
        ]);
    }

    /**
     * @loadFixture customer
     * @loadFixture config
     */
    public function testEmployeeIsRegisteredWithDefaultLimit()
    {
        $register = $this->registerEmployee([
            'cpf' => '65446514785',
        ]);
        $customer = Mage::getModel('customer/customer')->load(1);

        $this->assertEquals(1000, $register->getLimit());
        $this->assertEquals($register->getLimit(), $register->getBalance());
        $this->assertEquals(1, $register->getCustomerId());
        $this->assertEquals($customer->getName(), $register->getName());

        $this->assertEquals(1, $customer->getIsEmployee());
    }

    /**
     * @loadFixture customer
     * @loadFixture config
     */
    public function testEmployeeIsRegisteredWithCustomData()
    {
        $register = $this->registerEmployee([
            'cpf' => '65446514785',
            'limit' => 300,
            'balance' => 200,
            'name' => 'Mithrandir'
        ]);

        $customer = Mage::getModel('customer/customer')->load(1);

        $this->assertEquals(300, $register->getLimit());
        $this->assertEquals(200, $register->getBalance());
        $this->assertEquals(1, $register->getCustomerId());
        $this->assertEquals('Mithrandir', $register->getName());

        $this->assertEquals(1, $customer->getIsEmployee());
    }

    /**
     * @loadFixture customer
     * @loadFixture config
     */
    public function testCustomerIsNotEmployeeWhenRegisterIsDeleted()
    {
        $register = $this->registerEmployee([
            'cpf' => '65446514785'
        ]);

        $register->delete();

        $customer = Mage::getModel('customer/customer')->load(1);

        $this->assertEquals(0, $customer->getIsEmployee());
    }
}
