<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_SephoraEmployees
 *
 * @copyright Copyright (c) 2017 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Andre Manoel <andre.manoel@e-smart.com.br>
 */

class BSeller_SephoraEmployees_Block_Adminhtml_Sephora_History extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * BSeller_SephoraEmployees_Block_Adminhtml_Sephora_History constructor
     */
    public function __construct()
    {
        $this->_controller = 'adminhtml_sephora_history';
        $this->_blockGroup = 'bseller_sephoraemployees';
        $this->_headerText = $this->__('Employees History');

        parent::__construct();
        $this->_removeButton('add');
        $this->setUseAjax(true);
    }
}