<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_SephoraEmployees
 *
 * @copyright Copyright (c) 2017 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Andre Manoel <andre.manoel@e-smart.com.br>
 */

class BSeller_SephoraEmployees_Block_Adminhtml_Sephora_History_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * BSeller_SephoraEmployees_Block_Adminhtml_Sephora_History_Grid constructor
     */
    public function __construct()
    {
        parent::__construct();

        $this->setId('bseller_sephoraemployees_history_grid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    /**
     * Prepare grid collection
     *
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel("bseller_sephoraemployees/history")->getCollection();
        $this->setCollection($collection);
        parent::_prepareCollection();

        return $this;
    }

    /**
     * Prepare grid columns
     *
     * @return $this
     * @throws Exception
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'order_id', array(
                'header' => $this->__('Order ID'),
                'align'  => 'right',
                'width'  => '150px',
                'index'  => 'order_id'
        ));

        $this->addColumn(
            'cpf', array(
                'header' => $this->__('CPF'),
                'align'  => 'left',
                'index'  => 'cpf'
        ));

        $this->addColumn(
            'employee_name', array(
                'header' => $this->__('Employee Name'),
                'align'  => 'left',
                'index'  => 'employee_name'
        ));

        $this->addColumn(
            'order_total', array(
                'header' => $this->__('Order Total'),
                'align'  => 'left',
                'index'  => 'order_total'
        ));

        $this->addColumn(
            'old_balance', array(
                'header' => $this->__('Old Balance'),
                'align'  => 'left',
                'index'  => 'old_balance'
        ));

        $this->addColumn(
            'new_balance', array(
                'header'  => $this->__('New Balance'),
                'align'   => 'left',
                'editable' => false,
                'index'   => 'balance'
        ));

        $this->addColumn(
            'created_at', array(
            'header' => $this->__('Date of history'),
            'type'   => 'datetime',
            'index'  => 'created_at'
        ));

        $this->addExportType('*/*/exportEmployeeCsv', $this->__('CSV'));
        $this->addExportType('*/*/exportEmployeeExcel', $this->__('Excel XML'));

        return parent::_prepareColumns();
    }

    /**
     * Return grid url
     *
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current' => true));
    }

    /**
     * Return row url
     *
     * @param Varien_Object $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }
}