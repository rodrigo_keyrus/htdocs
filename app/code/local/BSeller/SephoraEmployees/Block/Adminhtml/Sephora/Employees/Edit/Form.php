<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_SephoraEmployees
 *
 * @copyright Copyright (c) 2017 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Andre Manoel <andre.manoel@e-smart.com.br>
 */

class BSeller_SephoraEmployees_Block_Adminhtml_Sephora_Employees_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Prepare edit form.
     *
     * @return Mage_Adminhtml_Block_Widget_Form
     * @throws Exception
     */
    protected function _prepareForm()
    {
        $this->createForm();
        $object = $this->helper('bseller_sephoraemployees')->getObjectInfo();
        $this->createFormFields($object);
        $this->populateForm($object);

        return parent::_prepareForm();
    }

    /**
     * Create edit form.
     *
     * @return void
     * @throws Exception
     */
    protected function createForm()
    {
        $form = new Varien_Data_Form(
            array(
                'id'            => 'edit_form',
                'action'        => $this->getUrl('*/*/save'),
                'method'        => 'post',
                'use_container' => true,
            )
        );

        $this->setForm($form);
    }

    /**
     * Create form fields.
     *
     * @return void
     */
    protected function createFormFields($object)
    {
        $table = $this->getForm()->addFieldset(
            'sephoraemployees_form_table',
            array(
                'legend' => $this->__('General')
            )
        );


        if ($object->getId()) {
            $table->addField(
                'id',
                'hidden',
                [
                    'name'     => 'id',
                    'required' => true
                ]
            );
        }

        $table->addField(
            'cpf',
            'text',
            array(
                'label'     => $this->__('CPF'),
                'required'  => true,
                'name'      => 'cpf'
            )
        );

        $table->addField(
            'name',
            'text',
            array(
                'label'     => $this->__('Employee name'),
                'required'  => true,
                'name'      => 'name'
            )
        );

        $table->addField(
            'limit',
            'text',
            array(
                'label'     => $this->__('Discount limit'),
                'required'  => false,
                'name'      => 'limit'
            )
        );

        $table->addField(
            'percent',
            'text',
            array(
                'label'     => $this->__('Discount percent'),
                'required'  => false,
                'name'      => 'percent'
            )
        );
        $table->addField(
            'percentcollection',
            'text',
            array(
                'label'     => $this->__('Collection Discount'),
                'required'  => false,
                'name'      => 'percentcollection'
            )
        );
    }

    /**
     * Complete the form with the necessary data.
     *
     * @return void
     */
    protected function populateForm($object)
    {
        $form    = $this->getForm();
        /** @var BSeller_SephoraEmployees_Helper_Data $object */

        $form->setValues($object->getData());
    }
}