<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_SephoraEmployees
 *
 * @copyright Copyright (c) 2017 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Andre Manoel <andre.manoel@e-smart.com.br>
 */

class BSeller_SephoraEmployees_Block_Adminhtml_Sephora_Employees_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * BSeller_SephoraEmployees_Block_Adminhtml_Sephora_Employees_Grid constructor
     */
    public function __construct()
    {
        parent::__construct();

        $this->setId('bseller_sephoraemployees_grid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    /**
     * Prepare grid collection
     *
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel("bseller_sephoraemployees/register")->getCollection();
        $this->setCollection($collection);
        parent::_prepareCollection();

        return $this;
    }

    /**
     * Prepare grid columns
     *
     * @return $this
     * @throws Exception
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'customer_id',
            array(
                'header' => $this->__('Employee ID'),
                'align'  => 'right',
                'width'  => '150px',
                'index'  => 'customer_id'
            )
        );

        $this->addColumn(
            'cpf',
            array(
                'header' => $this->__('CPF'),
                'align'  => 'left',
                'index'  => 'cpf'
            )
        );

        $this->addColumn(
            'name',
            array(
                'header' => $this->__('Employee Name'),
                'align'  => 'left',
                'index'  => 'name'
            )
        );

        $this->addColumn(
            'limit',
            array(
                'header' => $this->__('Employee Limit'),
                'align'  => 'left',
                'index'  => 'limit'
            )
        );

        $this->addColumn(
            'percent',
            array(
                'header' => $this->__('Percent Discount'),
                'align'  => 'left',
                'index'  => 'percent'
            )
        );        

        $this->addColumn(
            'balance',
            array(
                'header'  => $this->__('Current Balance'),
                'align'   => 'left',
                'editable' => false,
                'index'   => 'balance'
            )
        );
        $this->addColumn(
            'percentcollection',
            array(
                'header'  => $this->__('percentcollection'),
                'align'   => 'left',
                'editable' => false,
                'index'   => 'percentcollection'
            )
        );

        return parent::_prepareColumns();
    }

    /**
     * Return grid url
     *
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current' => true));
    }

    /**
     * Return row url
     *
     * @param Varien_Object $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }
}
