<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category BSeller
 * @package  BSeller_SephoraEmployees
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author Diogo Baracho <diogo.baracho@e-smart.com.br>
 */
class BSeller_SephoraEmployees_Block_Adminhtml_Sephora_Employees_Multipleupload

    extends Mage_Uploader_Block_Multiple
{
    public function __construct()
    {
        parent::__construct();

        $this->setTemplate('bseller/sephoraemployees/uploader.phtml');

    }

    protected function _prepareLayout()
    {
        return $this;

    }

    public function getMaxFileSize()
    {
        $res = array(
            ini_get('post_max_size'),
            ini_get('upload_max_filesize'),
        );
        $min = min($res);
        return (int)$min * 1000000;
    }
}