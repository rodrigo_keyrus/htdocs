<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_SephoraEmployees
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Anderson Cardoso <anderson.cardoso@e-smart.com.br>
 */

class BSeller_SephoraEmployees_Model_Observer extends Mage_Core_Model_Abstract
{
    /**
     * Variable to set new balance of employee
     * @var String
     */
    protected $_newBalance;

    /**
     * @return Mage_Core_Helper_Abstract
     */
    protected function getHelper()
    {
        return Mage::helper('bseller_sephoraemployees');
    }

    /**
     * @return false|Mage_Core_Model_Abstract
     */
    public function getModelDiscount()
    {
        return Mage::getModel('BSeller_SephoraEmployees_Model_Discount');
    }

    /**
     * @todo Updates the customer if he or she is an employee
     *
     * @param Varien_Event_Observer $observer
     * @return BSeller_SephoraEmployees_Model_Observer $this
     */
    public function employeeLogin(Varien_Event_Observer $observer)
    {
        if ($this->getHelper()->isModuleEnabled()) {

            if(Mage::helper('core')->isModuleEnabled('Esmart_BeautyClub')) {
                Mage::helper('esmart_bc')->setSkipUpdateBC();
            }

            $customer   = $observer->getEvent()->getCustomer();

            $isEmployee = $this->getHelper()->updateCustomerEmployee($customer->getId(), $customer->getCpf());

            if ($isEmployee && !$customer->getIsEmployee()) {
                $customer->setIsEmployee(true)->save();
                return $this;
            }

            if (!$isEmployee && $customer->getIsEmployee()) {
                $customer->setIsEmployee(false)->save();
            }
        }
    }

    /**
     * @todo Calculates the employee discount amount and new balance
     *
     * @param Varien_Event_Observer $observer
     * @return $this
     */
    public function employeeCheckout(Varien_Event_Observer $observer)
    {
        if (!Mage::helper('bseller_sephoraemployees')->isEnabled()) {
            return $this;
        }
        /** @var Mage_Sales_Model_Quote $quote*/
        $quote          =  $observer->getEvent()->getQuote();
        $quoteid        =  $quote->getId();
        $discountAmount =  $this->getModelDiscount()->valueDiscount($quote);

        if ($quoteid) {
            $this->applyDiscount($quote, $discountAmount);
        }

        return $this;
    }

    /**
     *@todo  Save the history and update the employee balance
     * @param Varien_Event_Observer $observer
     * @throws Exception
     */
    public function employeeOrder(Varien_Event_Observer $observer)
    {
        $order    = $observer->getEvent()->getOrder();
        $customer = $order->getCustomer();
        if ($this->getHelper()->isModuleEnabled() && $customer->getIsEmployee()) {
            Mage::log($customer . $order, null, 'EmployeeDiscount.log');
            $register = Mage::getModel('bseller_sephoraemployees/register')
                ->getEmployeeByCpf($customer->getCpf());

            $balance  = Mage::getSingleton('customer/session')->getEmployeeBalance();
            if ($balance <= 0) {
                $balance = 0.001;
            }
            $this->saveHistory($customer, $order, $register->getBalance(), $balance);
            $register->setBalance($balance)->save();
        }
    }

    /**
     *@todo Return the value of canceled order to the balance
     * @param Varien_Event_Observer $observer
     * @throws Exception
     */
    public function employeeOrderCancel(Varien_Event_Observer $observer)
    {
        $order    = $observer->getEvent()->getOrder();

        $history  = Mage::getModel('bseller_sephoraemployees/history')
            ->getHistoryByOrderId($order->getId());

        if ($history->getId()) {
            $balance  = $history->getOldBalance();
            $register = $register = Mage::getModel('bseller_sephoraemployees/register')
                ->getEmployeeByCpf($history->getCpf());
            $register->setBalance($balance)->save();
        }
    }

    /**
     * @todo Save the history of employee order
     * @param $customer
     * @param $order
     * @param $oldBalance
     * @param $newBalance
     * @throws Exception
     */
    private function saveHistory($customer, $order, $oldBalance, $newBalance)
    {
        $history  = Mage::getModel('bseller_sephoraemployees/history');
        $history->setCpf($customer->getCpf());
        $history->setEmployeeName($customer->getName());
        $history->setCreatedAt($order->getCreatedAt());
        $history->setOrderId($order->getId());
        $history->setOrderTotal($order->getGrandTotal());
        $history->setOldBalance((float)$oldBalance);
        $history->setNewBalance((float)$newBalance);
        $history->save();
    }

    /**
     * @todo update balance
     * @param $quote
     * @return mixed
     */
    public function updateBalance($quote, $discountAmount)
    {
        $customer = $quote->getCustomer();

        $register = Mage::getModel('bseller_sephoraemployees/register')
            ->getEmployeeByCpf($customer->getCpf());

        $balance  = $register->getBalance();
        return Mage::getSingleton('customer/session')
            ->setEmployeeBalance($balance - $discountAmount);
    }

    /**
     * @todo Apply the employee discount at checkout
     * @param Mage_Sales_Model_Quote $quote
     * @param $discountAmount
     * @return mixed
     */
    private function applyDiscount(Mage_Sales_Model_Quote $quote, $discountAmount)
    {
        /** @var Mage_Sales_Model_Quote $quoteModel */
        $quoteModel = Mage::getModel('bseller_sephoraemployees/applyDiscount');

        if (!Mage::helper('customer')->isLoggedIn()) {
            return $this;
        }

        if ($discountAmount > 0) {
            $quoteModel->quoteModel($quote, $discountAmount);
        }

        return $this->updateBalance($quote, $discountAmount);
    }

    /**
     * @todo check if rule is active
     * @param Varien_Event_Observer $observer
     */
    public function salesruleRuleEmployeeDiscountConditionCombine(Varien_Event_Observer $observer)
    {
        if (!Mage::helper('bseller_sephoraemployees')->isEnabled()) {
            return;
        }

        $additional = $observer->getEvent()->getAdditional();
        $conditions = $additional->getConditions();

        $conditions[] = [
            'label' => Mage::helper('bseller_sephoraemployees')->__('Is Employee'),
            'value' => 'bseller_sephoraemployees/Condition'
        ];

        $additional->setConditions($conditions);
    }
}
