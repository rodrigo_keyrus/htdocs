<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_SephoraEmployees
 *
 * @copyright Copyright (c) 2017 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Andre Manoel <andre.manoel@e-smart.com.br>
 */

class BSeller_SephoraEmployees_Model_Cron extends Mage_Core_Model_Abstract
{
    /**
     * Reset the balance of all employees
     */
    public function resetEmployeeLimit()
    {
        $defaultLimit = $this->getHelper()->getStoreConfig('limit_default');
        $write = Mage::getSingleton('core/resource')->getConnection('core_write');
        $write->update(
            "employees_register",
            array("balance" => $defaultLimit),
            array("`limit`=?" => $defaultLimit)
        );

        $this->resetCustomEmployeeLimit();
    }

    private function resetCustomEmployeeLimit()
    {
        $defaultLimit = $this->getHelper()->getStoreConfig('limit_default');
        $collection   = Mage::getModel('bseller_sephoraemployees/register')->getCollection();
        $collection->addFieldToFilter('limit', array('neq' => $defaultLimit));

        foreach ($collection as $item) {
            $item->setBalance($item->getLimit());
        }
        $collection->save();
    }

    /**
     * Return employee helper
     *
     * @return BSeller_SephoraEmployees_Helper_Data
     */
    protected function getHelper()
    {
        return Mage::helper('bseller_sephoraemployees');
    }
}