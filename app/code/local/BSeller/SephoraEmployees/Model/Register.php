<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_SephoraEmployees
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Anderson Cardoso <Anderso.cardoso@e-smart.com.br>
 */

/**
 * @method string getBalance()
 * @method BSeller_SephoraEmployees_Model_Register setBalance(string $value)
 * @method string getCpf()
 * @method BSeller_SephoraEmployees_Model_Register setCpf(string $value)
 * @method string getName()
 * @method BSeller_SephoraEmployees_Model_Register setName(string $value)
 * @method string getLimit()
 * @method BSeller_SephoraEmployees_Model_Register setLimit(string $value)
 * @method int getCustomerId()
 * @method BSeller_SephoraEmployees_Model_Register setCustomerId(int $value)
 */
class BSeller_SephoraEmployees_Model_Register extends Mage_Core_Model_Abstract
{
    /** @var  Mage_Customer_Model_Customer */
    protected $_customer;

    protected function _construct()
    {
        $this->_init('bseller_sephoraemployees/register');
    }

    /**
     * Processing object after load data
     *
     * @return Mage_Core_Model_Abstract
     */
    protected function _afterLoad()
    {
        $this->loadCustomer();
        return parent::_afterLoad();
    }

    /**
     * Processing object before save data
     * @return Mage_Core_Model_Abstract
     * @throws Mage_Core_Model_Store_Exception
     */
    protected function _beforeSave()
    {
        if (!$this->getLimit() || $this->getLimit() == 0) {
            $limitDefault = Mage::getStoreConfig(
                'bseller_sephoraemployees/settings/limit_default',
                Mage::app()->getStore()
            );

            $this->setLimit($limitDefault);
        }

        if (!$this->getBalance() || $this->getBalance() == 0) {
            $this->setBalance($this->getLimit());
        }

        return parent::_beforeSave();
    }

    /**
     * Processing object after save data
     *
     * @return Mage_Core_Model_Abstract
     */
    protected function _afterSave()
    {
        if ($this->_customer) {
            $this->setCustomerIsEmployee(true);
        }

        return parent::_afterSave();
    }

    /**
     * Processing object before delete data
     *
     * @return Mage_Core_Model_Abstract
     */
    protected function _beforeDelete()
    {
        if ($this->_customer) {
            $this->setCustomerIsEmployee(false);
        }

        return parent::_beforeDelete();
    }

    /**
     * @param string $cpf
     * @return $this
     */
    public function getEmployeeByCpf($cpf)
    {
        $this->load($cpf, 'cpf');

        return $this;
    }

    /**
     * @return Mage_Customer_Model_Customer
     */
    public function getCustomer()
    {
        if (!$this->_customer) {
            $this->loadCustomer();
        }
        return $this->_customer;
    }

    /**
     * @param Mage_Customer_Model_Customer $customer
     * @return $this
     */
    public function setCustomer(Mage_Customer_Model_Customer $customer)
    {
        $this->_customer = $customer;
        $this->setCustomerId($customer->getId());

        if (!$this->getName()) {
            $this->setName($customer->getName());
        }

        return $this;
    }

    /**
     * @return Mage_Customer_Model_Customer
     */
    protected function getCustomerModel()
    {
        return Mage::getModel('customer/customer');
    }

    protected function throwInvalidCpfException()
    {
        Mage::throwException(
            Mage::helper('bseller_sephoraemployees')->__(
                'The employee is not registered in this store. Check the CPF %s and try again.',
                $this->getCpf()
            )
        );
    }

    protected function loadCustomer()
    {
        $customer = $this->getCustomerModel()->load($this->getCustomerId());
        if ($customer->getId()) {
            $this->_customer = $customer;
        }
    }

    /**
     * @param bool $isEmployee
     */
    protected function setCustomerIsEmployee($isEmployee)
    {
        Mage::getModel('bseller_sephoraemployees/customer')->setIsEmployee($this->_customer, $isEmployee);
    }
}