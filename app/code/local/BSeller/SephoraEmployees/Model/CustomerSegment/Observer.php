<?php
/**
 * BSeller Platform | B2W - Companhia Digital
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category  BSeller
 * @package   BSeller_FirstOrder
 *
 * @copyright Copyright (c) 2018 B2W Digital - BSeller Platform. (http://www.bseller.com.br)
 *
 * @author   Anderson Cardoso <anderson.cardoso@e-smart.com.br>
 */
  
class BSeller_SephoraEmployees_Model_CustomerSegment_Observer extends Enterprise_CustomerSegment_Model_Observer
{
    /**
     * Add Customer Segment condition to the salesrule management
     *
     * @param Varien_Event_Observer $observer
     */
    public function addSegmentsToSalesRuleCombine(Varien_Event_Observer $observer)
    {
        if (!Mage::helper('enterprise_customersegment')->isEnabled()) {
            return;
        }
        $additional = $observer->getEvent()->getAdditional();
        $conditions = $additional->getConditions();
        $conditions[] = [
            'label' => Mage::helper('enterprise_customersegment')->__('Customer Segment'),
            'value' => 'enterprise_customersegment/segment_condition_segment'
        ];

        $additional->setConditions($conditions);
    }
}
