<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_SephoraEmployees
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Anderson Cardoso <anderson.cardoso@e-smart.com.br>
 */
class BSeller_SephoraEmployees_Model_Discount
{
    protected $_newBalance;

    /**
     * @return Mage_Core_Helper_Abstract
     */
    public function getHelper()
    {
        return Mage::helper('bseller_sephoraemployees');
    }

    /**
     * @return false|Mage_Core_Model_Abstract
     */
    public function getModel()
    {
        return Mage::getModel('bseller_sephoraemployees/register');
    }


    /**
     * @todo Returns item discount rate
     * @param Mage_Catalog_Model_Product $product
     * @return string
     */
    public function getTax(Mage_Catalog_Model_Product $product, $cpfCustomer)
    {
        /** @var BSeller_SephoraEmployees_Model_Register $customer */
        $customer = Mage::getModel('bseller_sephoraemployees/register')
            ->getEmployeeByCpf($cpfCustomer);

        if ($this->isCollection($product)) {

            return $this->discountIfCollection($customer);
        }

        return $this->simpleDiscount($customer);
    }

    /**
     *
     * @todo  check if item is collection
     * @param Mage_Catalog_Model_Product $product
     * @return bool
     */
    public function isCollection(Mage_Catalog_Model_Product $product)
    {
        $manufacturers   = explode(",", $this->getHelper()->getStoreConfig('manufacturer'));
        $isCollection    = in_array($product->getManufacturer(), $manufacturers) ? true : false;

        return $isCollection;
    }

    /**
     * @todo returns discount if item is collection
     * @param $customer
     * @return mixed
     */
    public function discountIfCollection($customer)
    {
        $collection = $customer->getPercentcollection();
        $collection = ($collection != null) ? $collection : $this->getHelper()->getStoreConfig('special_discount');

        return $collection;
    }

    /**
     * @todo returns discount if item is not collection
     * @param $customer
     * @return mixed
     */
    public function simpleDiscount($customer)
    {
        $simpleDiscount = $customer->getPercent();
        $simpleDiscount = ($simpleDiscount != null) ? $simpleDiscount : $this->getHelper()->getStoreConfig('discount');

        return $simpleDiscount;
    }

    /**
     * @todo Return the item discount
     * @param Mage_Sales_Model_Quote_Item $item
     * @return float|int
     */
    private function getItemDiscount(Mage_Sales_Model_Quote_Item $item, $cpfCustomer)
    {
        $itemDiscount = $this->getTax($item->getProduct(), $cpfCustomer);

        return $item->getProduct()->getPrice() * $itemDiscount / 100;
    }

    /**
     * @todo return value discount for  current employee
     * @param $quote
     * @return $this|float|int
     */
    public function valueDiscount($quote)
    {
        $customer      = $quote->getCustomer();
        $totalDiscount = 0;

        if ($this->getHelper()->isModuleEnabled()) {

            $register  = $this->getModel()->getEmployeeByCpf($customer->getCpf());
            $balance   = $register->getBalance();
            $discount  = $this->calcEmployeeDiscount($quote);

            if ($balance < 10) {
                return $totalDiscount;
            }

            if ($discount > 0) {
                $totalDiscount = $this->checkLimitDiscount($quote, $discount, $balance);
            }
        }

        return $totalDiscount;
    }

    /**
     * @todo check the discount range to be applied
     * @param Mage_Sales_Model_Quote $quote
     * @param $employeeDiscount
     * @param $balance
     * @return $this|mixed|string
     */
    public function checkLimitDiscount(Mage_Sales_Model_Quote $quote, $employeeDiscount, $balance)
    {
        $employeeDiscount     = str_replace(',', '', $employeeDiscount);

        if ($balance < $employeeDiscount) {
            $employeeDiscount = $this->calcPartialDiscount($balance, $employeeDiscount);
        }

        $promoDiscount = $quote->getSubtotal() - $quote->getSubtotalWithDiscount();

        if ($promoDiscount > $employeeDiscount) {
            return $this;
        }

        return $employeeDiscount;
    }

    /**
     * Return the employee discount amount
     *
     * @param Mage_Sales_Model_Quote $quote
     * @return float|int
     */
    private function calcEmployeeDiscount(Mage_Sales_Model_Quote $quote)
    {
        $cpfCustomer   = $quote->getCustomer()->getCpf();

        $itemDiscount = $this->itemDiscountApply($quote, $cpfCustomer);

        return number_format($itemDiscount, 2);
    }

    /**
     * @todo retunr item discount for all quote
     * @param Mage_Sales_Model_Quote $quote
     * @param $cpfCustomer
     * @return float|int
     */
    protected  function itemDiscountApply(Mage_Sales_Model_Quote $quote, $cpfCustomer)
    {
        $employeeDiscount  = 0;
        $this->_newBalance = 0;

        foreach ($quote->getAllItems() as $item) {
            if ($item->getProduct()->getPrice() <= $item->getProduct()->getFinalPrice()) {
                $itemDiscount = $this->getItemDiscount($item, $cpfCustomer);
                $employeeDiscount  += $itemDiscount * $item->getQty();
                $this->_newBalance += $item->getProduct()->getPrice() * $item->getQty();
                continue;
            }


            $discount = $item->getProduct()->getPrice() - $item->getProduct()->getFinalPrice();
            if ($discount < $itemDiscount = $this->getItemDiscount($item, $cpfCustomer)) {
                $employeeDiscount  += ($itemDiscount - $discount) * $item->getQty();
                $this->_newBalance += $item->getProduct()->getPrice() * $item->getQty();
            }
        }
        return $employeeDiscount;
    }

    /**
     * @todo calculate partial discount if the limit is less than the discount
     * @param $limit
     * @return string
     */
    protected function calcPartialDiscount($limit, $discount)
    {
        if ($discount > $limit) {
            return $limit;
        }

        return $discount;
    }

}