<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_SephoraEmployees
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Anderson Cardoso <anderson.cardoso@e-smart.com.br>
 */

class BSeller_SephoraEmployees_Model_ApplyDiscount
{
    const SHIPPING_METHOD = 'shipping';

    /**
     * @return Mage_Core_Helper_Abstract
     */
    public function getHelper()
    {
        return Mage::helper('bseller_sephoraemployees');
    }

    /**
     * @param Mage_Sales_Model_Quote $quote
     * @param $discountAmount
     * @throws Exception
     */
    public function quoteModel(Mage_Sales_Model_Quote $quote, $discountAmount)
    {
        $quote->getBaseSubtotal();
        $quote->setSubtotal(0);
        $quote->setBaseSubtotal(0);
        $quote->setSubtotalWithDiscount(0);
        $quote->setBaseSubtotalWithDiscount(0);
        $quote->setGrandTotal(0);
        $quote->setBaseGrandTotal(0);

        $this ->setAddress($quote, $discountAmount);
    }

    /**
     * @param Mage_Sales_Model_Quote $quote
     * @param $discountAmount
     * @throws Exception
     */
    public function setAddress(Mage_Sales_Model_Quote $quote, $discountAmount)
    {

        foreach ($quote->getAllAddresses() as $address) {

            $address->setSubtotal(0);
            $address->setBaseSubtotal(0);
            $address->setGrandTotal(0);
            $address->setBaseGrandTotal(0);
            $address->collectTotals();

            $quote->setSubtotal((float) $quote->getSubtotal() + $address->getSubtotal());
            $quote->setBaseSubtotal((float) $quote->getBaseSubtotal() + $address->getBaseSubtotal());

            $quote->setSubtotalWithDiscount(
                (float) $quote->getSubtotalWithDiscount() + $address->getSubtotalWithDiscount()
            );
            $quote->setBaseSubtotalWithDiscount(
                (float) $quote->getBaseSubtotalWithDiscount() + $address->getBaseSubtotalWithDiscount()
            );
            $quote->setGrandTotal((float) $quote->getGrandTotal() + $address->getGrandTotal());
            $quote->setBaseGrandTotal((float) $quote->getBaseGrandTotal() + $address->getBaseGrandTotal());
            $quote->setGrandTotal($quote->getBaseSubtotal()- $discountAmount + $address->getShippingAmount())
                ->setBaseGrandTotal($quote->getBaseSubtotal() - $discountAmount)
                ->setSubtotalWithDiscount($quote->getBaseSubtotal() - $discountAmount)
                ->setBaseSubtotalWithDiscount($quote->getBaseSubtotal() - $discountAmount)
                ->save();

            $this->canAddItens($address, $discountAmount);
        }

    }

    /**
     * @param Mage_Sales_Model_Quote_Address $quote
     * @param $employeeDiscount
     * @throws Exception
     */
    public function canAddItens(Mage_Sales_Model_Quote_Address $quote , $employeeDiscount)
    {
        $address = $quote->getQuote()->getShippingAddress();
        $address->setDiscountDescription($this->getHelper()->__('Employee Discount'));
        $address->addTotal(
            array(
                'code' => 'discount',
                'title' => $this->getHelper()->__("Employee Discount"),
                'value' => -$employeeDiscount,
            )
        );
        $address->setDiscountAmount(-$employeeDiscount);
        $address->setBaseDiscountAmount(-$employeeDiscount);
        $address->setSubtotalWithDiscount($address->getSubtotal() - $employeeDiscount);
        $address->setBaseSubtotalWithDiscount($address->getBaseSubtotal() - $employeeDiscount);
        $address->setGrandTotal($address->getSubtotal() - $employeeDiscount + $address->getShippingAmount());
        $address->setBaseGrandTotal(
            $address->getBaseSubtotal() - $employeeDiscount + $address->getShippingAmount()
        );
        $address->save();
    }


}