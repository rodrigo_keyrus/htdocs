<?php

class BSeller_SephoraEmployees_Model_Source_Manufacturer extends Mage_Core_Model_Abstract
{
    public function toOptionArray()
    {
        $collection = Mage::getModel('zeon_manufacturer/manufacturer')->getCollection();
        $arr = array();
        foreach ($collection as $option) {
            $arr[] = array('value' => $option->getManufacturerId(), 'label' => $option->getManufacturerName());
        }
        return $arr;
    }
}