<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_SephoraEmployees
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Anderson Cardoso <Anderso.cardoso@e-smart.com.br>
 */
class BSeller_SephoraEmployees_Model_Customer
{
    /**
     * @param string $cpfNumber
     * @return Mage_Customer_Model_Customer | bool
     */
    public function findOneByCpf($cpfNumber)
    {
        $collection = Mage::getModel('customer/customer')->getResourceCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('cpf', $cpfNumber)
            ->setPage(1,1);

        foreach ($collection as $object) {
            return $object;
        }

        return false;
    }

    /**
     * @param Mage_Customer_Model_Customer $customer
     * @param bool $isEmployee
     */
    public function setIsEmployee(Mage_Customer_Model_Customer $customer, $isEmployee)
    {
        $customer->setIsEmployee($isEmployee);
        $customer->getResource()->saveAttribute($customer, 'is_employee');
    }
}
