<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category BSeller
 * @package  BSeller_SephoraEmployees
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author Diogo Baracho <diogo.baracho@e-smart.com.br>
 */
umask(0);
class BSeller_SephoraEmployees_Adminhtml_Sephora_UploadController
    extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->loadLayout();

        $this->_addContent(
            $this->getLayout()
                ->createBlock('bseller_sephoraemployees/adminhtml_sephora_employees_multipleupload')
        );

        $this->renderLayout();
    }

    /**
     * return json
     */
    public function uploadAction()
    {
        $helper = Mage::helper('bseller_sephoraemployees');

        $filePathUpload = $helper->getUploadPath();

        try {
            $uploader = new BSeller_SephoraEmployees_Model_Adminhtml_Media_Uploader();
            $uploader->setAllowedExtensions(array('csv'));
            $uploader->setValidMimeTypes(
                array(
                    'text/comma-separated-values',
                    'text/csv',
                    'text/plain',
                    'application/csv',
                    'application/excel',
                    'application/vnd.ms-excel',
                    'application/vnd.msexcel',
                    'text/anytext')
            );
            $uploader->setAllowRenameFiles(true);
            $uploader->setAllowCreateFolders(true);
            $uploader->setFilesDispersion(false);
            $result = $uploader->save(
                $filePathUpload
            );

            $file = $result['file'];

            $errors = Mage::getModel('bseller_sephoraemployees/batchUpload', $filePathUpload . DS . $file)->process();
            unlink($filePathUpload . DIRECTORY_SEPARATOR . $file);

            if (count($errors)) {
                throw new Exception(implode("\n", $errors));
            }
        } catch (Exception $e) {
            $result = array(
                'error' => $e->getMessage(),
                'errorcode' => $e->getCode());
        }

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }


    public function cmsuploadAction()
    {
        try {
            $this->_initCmsAction();
            $targetPath = $this->getStorage()->getSession()->getCurrentPath();
            $uploader = new BSeller_SephoraEmployees_Model_Adminhtml_Media_Uploader();
            $uploader->setAllowedExtensions(array('csv'));
            $uploader->setAllowRenameFiles(true);
            $uploader->setValidMimeTypes(
                array(
                'text/comma-separated-values',
                'text/csv',
                'application/csv',
                'application/excel',
                'application/vnd.ms-excel',
                'application/vnd.msexcel',
                'text/anytext')
            );
            $uploader->setFilesDispersion(true);
            $result = $uploader->save(
                $targetPath
            );

        } catch (Exception $e) {
            $result = array(
                'error' => $e->getMessage(),
                'errorcode' => $e->getCode());
        }

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

    public function removeAction()
    {
        try {
            $path = $this->getRequest()->getParam('path');
            $file = $this->getRequest()->getParam('file');
            $uploader = new Maven_Html5uploader_Model_Media_Uploader();
            $result = array('success' => $uploader->remove($file, $path));

        } catch (Exception $e) {
            $result = array(
                'error'     => $e->getMessage(),
                'success'   => false,
                'errorcode' => $e->getCode());
        }

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

    /**
     * Register storage model and return it
     *
     * @return Mage_Cms_Model_Wysiwyg_Images_Storage
     */
    public function getStorage()
    {
        if (!Mage::registry('storage')) {
            $storage = Mage::getModel('cms/wysiwyg_images_storage');
            Mage::register('storage', $storage);
        }
        return Mage::registry('storage');
    }

    protected function _initCmsAction()
    {
        $this->getStorage();
        return $this;
    }


    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('bseller/bseller_sephoraemployees');
    }
}