<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_SephoraEmployees
 *
 * @copyright Copyright (c) 2017 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Andre Manoel <andre.manoel@e-smart.com.br>
 */

class BSeller_SephoraEmployees_Adminhtml_Sephora_HistoryController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Render index page
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }
    /**
     * Render grid block
     */
    public function gridAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function exportEmployeeCsvAction()
    {
        $fileName = 'bseller_sephoraemployee_history.csv';
        $grid = $this->getLayout()->createBlock('bseller_sephoraemployees/adminhtml_sephora_history_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }

    public function exportEmployeeExcelAction()
    {
        $fileName = 'bseller_sephoraemployee_history.xml';
        $grid = $this->getLayout()->createBlock('bseller_sephoraemployees/adminhtml_sephora_history_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }

    /**
     * Return employee helper
     *
     * @return BSeller_SephoraEmployees_Helper_Data
     */
    protected function getHelper()
    {
        return Mage::helper('bseller_sephoraemployees');
    }

    /**
     * Is allowed to access
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('bseller/bseller_sephoraemployees');
    }

}