<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_SephoraEmployees
 *
 * @copyright Copyright (c) 2017 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Andre Manoel <andre.manoel@e-smart.com.br>
 */
class BSeller_SephoraEmployees_Adminhtml_Sephora_EmployeesController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Render index page
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Render grid block
     */
    public function gridAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Render form new
     */
    public function newAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Render form edit
     */
    public function editAction()
    {
        $id = $this->getRequest()->getParam('id');

        $employee = Mage::getModel('bseller_sephoraemployees/register')->load($id);

        $this->getHelper()->addObjectInfo($employee);

        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Save post data and redirect to index action
     *
     * @return $this|Mage_Core_Controller_Varien_Action
     */
    public function saveAction()
    {
        if (!$this->getRequest()->isPost()) {
            return $this->_redirect('*/*/index');
        }
        $request = $this->getRequest();
        $postData = $request->getParams();
        $register = Mage::getModel('bseller_sephoraemployees/register');

        try {
            $id = $this->getRequest()->getPost('id');
            if ($id) {
                $register->load($id);
            }
            $register->addData($postData);
            $register->save();

            $this->_getSession()->addSuccess($this->__('Register saved'))->setTableData(false);
            if ($request->getParam('back')) {
                $this->_redirect('*/*/edit', array('id' => $register->getId()));
                return;
            }
            $this->_redirect('*/*/');
            return;
        }
        catch (Exception $e) {
            $this->_getSession()->addError($e->getMessage())->setTableData($request->getPost());
            $this->_redirect('*/*/edit', array('id' => $request->getParam('id')));
            return;
        }

        return $this->_redirect('*/*/index');
    }

    /**
     * Delete employee by id and redirect to index action
     *
     * @return $this|Mage_Core_Controller_Varien_Action
     */
    public function deleteAction()
    {
        $id = $this->getRequest()->getParam('id');

        if (empty($id)) {
            $this->_getSession()->addError($this->__('Unable to find a employee to delete.'));
            return $this->_redirect('*/*/index');
        }

        /** @var BSeller_SephoraEmployees_Model_Register $register */
        $register = Mage::getModel('bseller_sephoraemployees/register');

        try {
            $register->load($id);
            $register->delete();
            $this->_getSession()->addSuccess($this->__('The employee record has been deleted.'));
        } catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Exception $e) {
            $this->_getSession()->addError(
                $this->__('An error occurred while deleting the employee. Please try again.')
            );
            Mage::logException($e);
            return $this->_redirect('*/*/edit', array('id' => $id));
        }

        return $this->_redirect('*/*/index');
    }

    /**
     * Return employee helper
     *
     * @return BSeller_SephoraEmployees_Helper_Data
     */
    protected function getHelper()
    {
        return Mage::helper('bseller_sephoraemployees');
    }

    /**
     * Is allowed to access
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('bseller/bseller_sephoraemployees');
    }
}