<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_SephoraEmployees
 *
 * @copyright Copyright (c) 2017 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Andre Manoel <andre.manoel@e-smart.com.br>
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

$tableName = $installer->getTable('bseller_sephoraemployees/employees_register');
if (!$installer->getConnection()->isTableExists($tableName)) {
    $newTable = $installer->getConnection()->newTable($tableName)
        ->addColumn(
            'id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, [
                'unsigned' => true,
                'nullable' => false,
                'primary'  => true,
                'identity' => true,
            ], 'Primary key - ID'
        )
        ->addColumn(
            'customer_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, [
                'unsigned' => true,
                'nullable' => false,
            ], 'Customer ID'
        )
        ->addColumn(
            'cpf', Varien_Db_Ddl_Table::TYPE_TEXT, null, [
                'unsigned' => true,
                'nullable' => false,
            ], 'Employee CPF'
        )
        ->addColumn(
            'name', Varien_Db_Ddl_Table::TYPE_TEXT, null, [
                'unsigned' => true,
                'nullable' => false,
            ], 'Employee name'
        )
        ->addColumn(
            'limit', Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,2', [
                'unsigned' => true,
                'nullable' => false,
            ], 'Employee Limit'
        )
        ->addColumn(
            'balance', Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,2', [
                'unsigned' => true,
                'nullable' => false,
            ], 'Current Balance'
        );
    $installer->getConnection()->createTable($newTable);
}

$tableName = $installer->getTable('bseller_sephoraemployees/employees_history');
if (!$installer->getConnection()->isTableExists($tableName)) {
    $newTable = $installer->getConnection()->newTable($tableName)
        ->addColumn(
            'id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, [
                'identity' => true,
                'unsigned' => true,
                'nullable' => false,
                'primary' => true,
            ], 'Primary key - ID'
        )
        ->addColumn(
            'cpf', Varien_Db_Ddl_Table::TYPE_TEXT, null, [
                'unsigned' => true,
                'nullable' => false,
            ], 'Employee CPF'
        )
        ->addColumn(
            'employee_name', Varien_Db_Ddl_Table::TYPE_TEXT, null, [
                'unsigned' => true,
                'nullable' => false,
            ], 'Employee name'
        )
        ->addColumn(
            'created_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, [
                'nullable' => false,
            ], 'Date of history'
        )
        ->addColumn(
            'order_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, [
                'unsigned' => true,
                'nullable' => false,
            ], 'Order ID'
        )
        ->addColumn(
            'order_total', Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,2', [
                'unsigned' => true,
                'nullable' => false,
            ], 'Order Total'
        )
        ->addColumn(
            'old_balance', Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,2', [
                'unsigned' => true,
                'nullable' => false,
            ], 'Old Balance'
        )
        ->addColumn(
            'new_balance', Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,2', [
                'unsigned' => true,
                'nullable' => false,
            ], 'New Balance'
        );

    $installer->getConnection()->createTable($newTable);
}

/**
 * Customer entity forms
 */
$usedInForms = array(
    'adminhtml_customer',
    'adminhtml_checkout'
);
/**
 * Customer entity attributes
 */

$installer->addAttribute(
    'customer',
    'is_employee',
    array(
        'type'     => 'int',
        'label'    => 'Funcionário?',
        'source'   => 'eav/entity_attribute_source_boolean',
        'input'    => 'select',
        'visible'  => true,
        'required' => false,
        'unique'   => false,
    )
);

$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$entityTypeId     = $setup->getEntityTypeId('customer');
$attributeSetId   = $setup->getDefaultAttributeSetId($entityTypeId);
$attributeGroupId = $setup->getDefaultAttributeGroupId($entityTypeId, $attributeSetId);
$attribute = Mage::getSingleton('eav/config')->getAttribute('customer', 'is_employee');
$setup->addAttributeToGroup(
    $entityTypeId,
    $attributeSetId,
    $attributeGroupId,
    'is_employee',
    '150'  //sort_order
);
$attribute->setData('used_in_forms', $usedInForms)
    ->setData("is_used_for_customer_segment", true)
    ->setData("is_system", 0)
    ->setData("is_user_defined", 1)
    ->setData("is_visible", 1)
    ->setData("sort_order", 150);
$attribute->save();

$installer->endSetup();