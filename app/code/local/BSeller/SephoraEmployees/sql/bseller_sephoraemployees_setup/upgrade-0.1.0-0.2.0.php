<?php

$installer = $this;
$connection = $installer->getConnection();
 
$installer->startSetup();
 
$installer->getConnection()
          ->addColumn(
              $installer->getTable('bseller_sephoraemployees/employees_register'),
              'percent',
              array(
                  'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
                  'nullable' => true,
                  'default' => null,
                  'comment' => '% de descontos'
              )
          );
 
$installer->endSetup();