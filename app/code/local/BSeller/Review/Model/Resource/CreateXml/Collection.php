<?php
class BSeller_Review_Model_Resource_CreateXml_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    public function _construct()
    {
        $this->_init('bseller_review/createXml');
    }
}