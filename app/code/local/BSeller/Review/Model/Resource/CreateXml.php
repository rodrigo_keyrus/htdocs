<?php
class BSeller_Review_Model_Resource_CreateXml extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('bseller_review/logxml', 'id');
    }
}