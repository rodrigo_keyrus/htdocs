<?php
class BSeller_Review_Model_CreateXml extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('bseller_review/createXml');
    }

    public function create()
    {
        try {
            @ini_set('memory_limit', '1024M');
            $createXml   = $this->templateXml();
            $count       = 0;
            $selectItems = $this->getSelectReviews();

            $this->log('Starting cron...');
            $this->log('Total reviews: ' . count($selectItems));

            foreach ($selectItems as $item) {
                $average = ($item['rating_summary'] / 20);

                /* Node <review> */
                $nodeReview = $createXml->reviews->addChild('review');

                /* Node <reviewer> */
                $reviewer = $nodeReview->addChild('reviewer');
                $reviewer->addChild('name', htmlspecialchars($item['nickname']));

                /* Node <review_timestamp> */
                $nodeReview->addChild('review_timestamp', $item['created_at']);

                /* Node <content> */
                $nodeReview->addChild('content');
                $nodeReview->content = htmlspecialchars($item['detail']);

                /* Node <review_url> */
                $reviewUrl = $nodeReview->addChild('review_url', $item['url_product']);
                $reviewUrl->addAttribute('type','singleton');

                /* Node <ratings> */
                $ratings = $nodeReview->addChild('ratings');
                $overall = $ratings->addChild('overall', number_format((float)$average, 1, '.', ''));
                $overall->addAttribute('min','1');
                $overall->addAttribute('max','5');

                /* Node <products> */
                $products   = $nodeReview->addChild('products');
                $product    = $products->addChild('product');
                $productUrl = $product->addChild('product_url', $item['url_product']);

                $count++;
            }

            $this->log('Total reviews add: ' . $count);

            if (!file_exists($this->getFilePath() . 'xml_reviews.xml')) {
                mkdir($this->getFilePath(), 0777, true);
            }

            $dom = new DOMDocument("1.0");
            $dom->preserveWhiteSpace = false;
            $dom->formatOutput = true;
            $dom->loadXML($createXml->asXML());

            $this->log('Saving file...');
            $this->saveFile($this->getFilePath() . 'xml_reviews.xml',$dom->saveXML());

            $this->log('Saving log...');
            $this->saveXmlLog(Mage::getStoreConfig('bseller_review/settings/file_path') . 'xml_reviews.xml');

            $this->log('End cron...');
        } catch (Exception $e) {
            $this->log('Error cron: ');
            $this->log($e->getMessage() . PHP_EOL);
            $this->log('End cron...');
        }

        return $this;
    }

    protected function templateXml()
    {
        $xml =<<<XML
<?xml version="1.0" encoding="UTF-8"?>
<feed xmlns:vc="http://www.w3.org/2007/XMLSchema-versioning" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
    xsi:noNamespaceSchemaLocation="http://www.google.com/shopping/reviews/schema/product/2.1/product_reviews.xsd">
    <aggregator>
        <name>Sephora</name>
    </aggregator>
    <publisher>
        <name>Sephora</name>
    </publisher>
    <reviews></reviews>
</feed>
XML;

        $createXml = new SimpleXMLElement($xml);

        return $createXml;
    }

    public function saveFile($filepath, $content)
    {
        file_put_contents($filepath,$content);
        return $this;
    }

    public function saveXmlLog($filepath)
    {
        $transaction = Mage::getResourceModel('core/transaction');

        $model = Mage::getModel('bseller_review/createXml');
        $model->setFilepath($filepath)
            ->setCreatedAt(Mage::getModel('core/date')->gmtDate());

        $transaction->addObject($model);
        $transaction->save();

        return $this;
    }

    public function getFilePath()
    {
        return Mage::getBaseDir() . DIRECTORY_SEPARATOR . Mage::getStoreConfig('bseller_review/settings/file_path');
    }

    public function log($message)
    {
        Mage::log($message, null, 'xml_review_google.log');

        return $this;
    }

    public function getSelectReviews()
    {
        $resource = Mage::getSingleton('core/resource');
        $query = <<<SQL
SELECT
  cpf1.entity_id,
  cpf1.url_key,
  r.entity_pk_value,
  res.reviews_count,
  res.rating_summary,
  rd.review_id,
  rd.title,
  rd.nickname,
  rd.detail,
  r.created_at,
  CONCAT(
      (SELECT value FROM core_config_data WHERE path = 'web/secure/base_url' AND scope = 'default'),
      cpf1.url_key
  ) AS url_product
FROM catalog_product_flat_1 cpf1
  INNER JOIN review r
    ON cpf1.entity_id = r.entity_pk_value
       AND r.status_id = 1
       AND r.entity_id = 1
  INNER JOIN review_entity_summary res
    ON cpf1.entity_id = res.entity_pk_value
       AND res.reviews_count > 0
       AND res.store_id = 1
  INNER JOIN review_detail rd
    ON r.review_id = rd.review_id
       AND rd.store_id = 1
WHERE cpf1.type_id = 'grouped'
      AND cpf1.status = 1
      AND cpf1.visibility = 4
SQL;
        $readConnection = $resource->getConnection('core_read');

        return $readConnection->fetchAll($query);
    }
}