<?php
class BSeller_Review_Block_Adminhtml_CreateXml_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('bseller_review_grid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(false);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('bseller_review/createXml_collection');
        $this->setCollection($collection);
        parent::_prepareCollection();
        return $this;
    }

    protected function _prepareColumns()
    {
        $helper = Mage::helper('bseller_review');

        $this->addColumn('id', array(
            'header' => $helper->__('ID #'),
            'index'  => 'id',
            'filter' => false
        ));

        $this->addColumn('filepath', array(
            'header' => $helper->__('File path'),
            'type'   => 'text',
            'index'  => 'filepath',
            'filter' => false,
            'frame_callback' => array($this, 'downloadFile')
        ));

        $this->addColumn('created_at', array(
            'header' => $helper->__('Generated at'),
            'type'   => 'datetime',
            'index'  => 'created_at'
        ));

        return parent::_prepareColumns();
    }

    public function downloadFile($value, $row)
    {
        //$file = str_replace('/','-',$row['filepath']);
        $return = '';
        $currentUrl = $this->getBaseUrl() . $row['filepath'];
        $currentUrl = str_replace('index.php/','',$currentUrl);

        if (!empty($row)) {
            $return .= '<a href="' . $currentUrl . '" target="_blank">' . $value . '</a>';
        }
        return $return;
    }
}