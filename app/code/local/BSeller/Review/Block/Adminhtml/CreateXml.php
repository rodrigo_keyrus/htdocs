<?php
class BSeller_Review_Block_Adminhtml_CreateXml extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'bseller_review';
        $this->_controller = 'adminhtml_createXml';
        $this->_headerText = Mage::helper('bseller_review')->__('Xml Review - Google');

        parent::__construct();
        $this->_removeButton('add');
    }
}