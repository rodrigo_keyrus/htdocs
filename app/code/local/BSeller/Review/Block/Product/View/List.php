<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category    BSeller
 * @package     BSeller_Review
 *
 * @copyright   Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author      Bruno Gemelli <bruno.gemelli@e-smart.com.br>
 * @author      Julio Reis <julio.reis@e-smart.com.br>
 */

class BSeller_Review_Block_Product_View_List extends Mage_Review_Block_Product_View_List
{
    public function getReviewsCollection()
    {
        if (null === $this->_reviewsCollection) {

            if (Mage::helper('bseller_review')->aggregateReviews()) {
                $storeId = Mage::helper('bseller_review')->getAggregatedStore();
            } else {
                $storeId = Mage::app()->getStore()->getId();
            }

            $this->_reviewsCollection = Mage::getModel('review/review')->getCollection()
                ->addStoreFilter($storeId)
                ->addStatusFilter(Mage_Review_Model_Review::STATUS_APPROVED)
                ->addEntityFilter('product', $this->getProduct()->getId())
                ->setDateOrder()
                ->addRateVotes();
        }
        return $this->_reviewsCollection;
    }


    /**
     * Method responsible for getting review collection removed (due review AJAX load)
     *
     * @return Mage_Core_Block_Abstract
     */
    protected function _beforeToHtml()
    {
        return Mage_Review_Block_Product_View::_beforeToHtml();
    }
}
