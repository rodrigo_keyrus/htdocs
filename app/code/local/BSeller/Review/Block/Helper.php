<?php
/**
 * BIT Tools Platform | B2W - Companhia Digital
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category  BSeller
 * @package   BSeller_Review
 *
 * @copyright Copyright (c) 2019 B2W Digital - BIT Tools.
 *
 * @author    Evandro Veloso Gomes <evandro.gomes@b2wdigital.com>
 */

class BSeller_Review_Block_Helper extends Mage_Review_Block_Helper
{
    public function getSummaryHtml($product, $templateType, $displayIfNoReviews)
    {
        // pick template among available
        if (empty($this->_availableTemplates[$templateType])) {
            $templateType = 'default';
        }
        $this->setTemplate($this->_availableTemplates[$templateType]);

        $this->setDisplayIfEmpty($displayIfNoReviews);

        if (!$product->getRatingSummary()) {
            if (Mage::helper('bseller_review')->aggregateReviews()) {
                $storeId = Mage::helper('bseller_review')->getAggregatedStore();
            } else {
                $storeId = Mage::app()->getStore()->getId();
            }

            Mage::getModel('review/review')
                ->getEntitySummary($product, $storeId);
        }
        $this->setProduct($product);

        return $this->toHtml();
    }
}