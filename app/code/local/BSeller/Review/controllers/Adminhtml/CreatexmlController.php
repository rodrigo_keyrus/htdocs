<?php
class BSeller_Review_Adminhtml_CreatexmlController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->_title($this->__('bseller_review'))->_title($this->__('Xml Review - Google'));
        $this->loadLayout();
        $this->_setActiveMenu('report/bseller_review');
        $this->_addContent($this->getLayout()->createBlock('bseller_review/adminhtml_createXml'));
        $this->renderLayout();
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('bseller_review/adminhtml_createxml_grid')->toHtml()
        );
    }

    public function downloadAction()
    {
        $file = $this->getRequest()->getParam('file_path');
        $file = str_replace('-','/',$file);

        try {
            if (file_exists($file)) {
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="'.basename($file).'"');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($file));
                readfile($file);
                exit;
            }else{
                throw new Exception('File not exists...');
            }
        }catch (Exception $e){
            echo $e->getMessage();
        }
    }
}