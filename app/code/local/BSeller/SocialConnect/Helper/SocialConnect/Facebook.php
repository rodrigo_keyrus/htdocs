<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_SocialConnect
 *
 * @copyright Copyright (c) 2017 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author   Luiz Fernando Alves <luiz.anjos@b2wdigital.com>
 */
class BSeller_SocialConnect_Helper_SocialConnect_Facebook
    extends Inchoo_SocialConnect_Helper_Facebook
{

    /**
     * @todo Method responsible for filter a single customer
     * by his CPF attribute
     * @override getCustomerByCpf
     * @param $cpf
     * @return $collection
     */
    public function getCustomerByCpf($cpf)
    {

        $customer = Mage::getModel('customer/customer');

        $collection = $customer->getCollection()
            ->addAttributeToSelect('cpf')
            ->addAttributeToFilter('cpf', $cpf)
            ->getFirstItem();

        return $collection;
    }

    /**
     * @todo Override method to get user CPF
     * @override getCustomersByFacebookId
     * @param $facebookId
     * @return $collection
     */
    public function getCustomersByFacebookId($facebookId)
    {
        $customer = Mage::getModel('customer/customer');

        $collection = $customer->getCollection()
            ->addAttributeToSelect('inchoo_socialconnect_ftoken')
            ->addAttributeToSelect('cpf')
            ->addAttributeToFilter('inchoo_socialconnect_fid', $facebookId)
            ->getFirstItem();

        return $collection;
    }

    /**
     * @todo Verify is current user is
     * the owner of account
     * @param $email
     * @param $password
     * @return bool
     * @throws Mage_Core_Model_Store_Exception
     */
    public function checkAccountOwner($email, $password)
    {
        try {
            $customer = Mage::getModel('customer/customer')
                ->setWebsiteId(Mage::app()->getStore()->getWebsiteId());

            if ($customer->authenticate($email, $password)) {
                return true;
            }
        } catch (Exception $e) {
            return false;
        }

        return false;
    }

    /**
     * @todo Create a custom password
     * and set in session
     * @return string
     */
    public function getPassword()
    {
        $customer = Mage::getModel('customer/customer');

        $password = $customer->generatePassword(10);

        return $password;
    }

    /**
     * Set session to be accessible in template
     * @param $email
     * @param $firstName
     * @param $lastName
     * @param $id
     * @param $birthday
     * @param $gender
     * @param $token
     * @param $userPicture
     * @return void
     */
    public function setSessionForRegister(
        $email,
        $firstName,
        $lastName,
        $id,
        $birthday,
        $gender,
        $token,
        $userPicture
    )
    {
        $birthday = $this->validateDate($birthday);
        # Set session to access in template
        Mage::getSingleton('core/session')
            ->setFacebookLogin(true);
        Mage::getSingleton('core/session')
            ->setEmail($email);
        Mage::getSingleton('core/session')
            ->setFirstName($firstName);
        Mage::getSingleton('core/session')
            ->setLastName($lastName);
        Mage::getSingleton('core/session')
            ->setBirthday($birthday);
        Mage::getSingleton('core/session')
            ->setGender($gender);
        Mage::getSingleton('core/session')
            ->setFacebookId($id);
        Mage::getSingleton('core/session')
            ->setFacebookToken($token);
        Mage::getSingleton('core/session')
            ->setPassword($this->getPassword());
        Mage::getSingleton('customer/session')
            ->setFacebookPicture($userPicture);
    }


    /**
     * @todo Validate if date is different of current date
     * @param $date
     * @return string
     */
    protected function validateDate($date)
    {
        $currentDate = date('Y-m-d');

        $dateFacebook = date($date);
        $finalDate ='';

        if ($dateFacebook < $currentDate) {
            //$finalDate = $this->convertDate($dateFacebook,'d/m/Y');
            $finalDate = '';
        }

        return $finalDate;
    }

    /**
     * @param $format
     * @param $date
     * @return false|string
     */
    protected function convertDate($date, $format)
    {
        $formatDate = new DateTime($date);
        $formatDate = $formatDate->format($format);
        return $formatDate;
    }

    public function cleanCpf($cpf)
    {
        return str_replace(array('.', '-'), '', $cpf);
    }

    /**
     * @param $type
     * @param null $msg
     * @throws Exception
     */
    public function setException($type, $msg = null)
    {
        switch ($type) {
            case 'firstname':
                throw new Exception(
                    $this->__('Sorry, could not retrieve your Facebook first name. Please try again.')
                );
                break;
            case 'lastname':
                throw new Exception(
                    $this->__('Sorry, could not retrieve your Facebook last name. Please try again.')
                );
                break;
            case 'errorCode':
                throw new Exception(
                    sprintf(
                        $this->__('Sorry, "%s" error occured. Please try again.'),
                        $msg
                    )
                );
                break;
            default:
                throw new Exception(
                    $this->__('Sorry, could not connect your Facebook. Please try again.')
                );
                break;
        }
    }

    /**
     * @todo If user already account, just set as logged in
     * @param $customersByEmail
     * @param $info
     * @param $token
     * @return $this
     */
    public function setCustomerLoggedIn($customersByEmail, $info, $token)
    {
        $customer = $customersByEmail->getFirstItem();
        $customer = $customer->load($customer->getId());

        if (is_object($info)) {
            $facebookId = $info->getId();
        } else {
            $facebookId = $info;
        }

        $this->connectByFacebookId(
            $customer,
            $facebookId,
            $token
        );

        Mage::getSingleton('core/session')->addSuccess(
            $this->__('We have discovered you already have an account at our store. Your Facebook account is '.
                'now connected to your store account.')
        );

        return $this;
    }

    /**
     * @param $customersByFacebookId
     * @param $info
     * @param $token
     * @return $this
     */
    public function isLoggedIn($customersByFacebookId, $info, $token)
    {
        // Logged in user
        if ($customersByFacebookId->getSize()) {
            /**
             * Deny/Facebook account already connected
             * with other account
             */
            $msg = $this->__( 'Your Facebook account 
                            is already connected to one of our store accounts.');
            Mage::getSingleton('core/session')
                ->addNotice($msg);

            return $this;
        }

        // Connect from account dashboard - attach
        $customer = Mage::getSingleton('customer/session')
            ->getCustomer();

        $this->connectByFacebookId(
            $customer,
            $info->getId(),
            $token
        );

        $msg = Mage::helper('bseller_socialconnect')
            ->__('Your Facebook account is now connected to your store account.');
        Mage::getSingleton('core/session')->addSuccess($msg);
    }

    public function getEmailEncrypted($email)
    {
        $emailEncrypted = preg_replace('/(?<=.{2}).(?=.*@)/', '*', $email);

        return $emailEncrypted;
    }

}