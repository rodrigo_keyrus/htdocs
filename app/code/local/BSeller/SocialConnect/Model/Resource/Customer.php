<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_SocialConnect
 *
 * @copyright Copyright (c) 2017 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author   Luiz Fernando Alves <luiz.anjos@b2wdigital.com>
 */

class BSeller_SocialConnect_Model_Resource_Customer extends Mage_Customer_Model_Resource_Customer
{
    use Esmart_Core_Trait;

    /**
     * @param $userCpf
     * @param bool $testOnly
     * @return bool|Mage_Core_Model_Abstract
     */
    public function loadByCpf($userCpf, $testOnly = false)
    {
        $adapter = $this->_getReadAdapter();
        /**
         * Search customer by email
         */
        $select = $adapter->select()
            ->from(array('ce' => $this->getEntityTable()), array($this->getEntityIdField()));

        /**
         * Search customer by document
         */
        if (Mage::helper('esmart_customer/cpf')->validate($userCpf)) {
            $cpf = Mage::getModel('eav/config')->getAttribute('customer', 'cpf');

            $select->joinLeft(
                array('cev' => 'customer_entity_varchar'),
                'ce.entity_id = cev.entity_id',
                null
            );

            $select->reset('where');
            $select->where('cev.attribute_id = ?', $cpf->getAttributeId());
            $select->where('cev.value = ?', preg_replace('/[^0-9]/', '', $userCpf));
        }

        $customerId = $adapter->fetchOne($select);

        if ($customerId) {
            $customer = Mage::getModel('customer/customer')->load($customerId);

            return $customer;
        }

        return false;
    }
}