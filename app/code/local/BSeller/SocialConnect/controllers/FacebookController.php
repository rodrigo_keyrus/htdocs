<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_SocialConnect
 *
 * @copyright Copyright (c) 2017 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author   Luiz Fernando Alves <luiz.anjos@b2wdigital.com>
 */

require_once(Mage::getModuleDir('controllers','Inchoo_SocialConnect').DS.'FacebookController.php');
class BSeller_SocialConnect_FacebookController
    extends Inchoo_SocialConnect_FacebookController
{
    /** @var BSeller_SocialConnect_Helper_SocialConnect_Facebook */
    protected $_helper;

    /** @var Inchoo_SocialConnect_Model_Facebook_Info $info */
    protected $_info;

    /**
     * @todo Override main constructor.
     * @param Zend_Controller_Request_Abstract $request
     * @param Zend_Controller_Response_Abstract $response
     * @param array $invokeArgs
     */
    public function __construct(
        Zend_Controller_Request_Abstract $request,
        Zend_Controller_Response_Abstract $response,
        array $invokeArgs = array())
    {
        /** @var BSeller_SocialConnect_Helper_SocialConnect_Facebook */
        $this->_helper = Mage::helper('bseller_socialconnect/socialConnect_facebook');

        /** @var Inchoo_SocialConnect_Model_Facebook_Info $info */
        $this->_info = Mage::getModel('inchoo_socialconnect/facebook_info');

        $this->_request = $request;
        $this->_response= $response;
        parent::_construct($this->_request, $this->_response);
    }

    /**
     * @todo This method capture incoming data
     * of Facebook applying the business logic
     * @return $this
     * @throws Exception
     */
    protected function _connectCallback()
    {
        $errorCode  = $this->getRequest()->getParam('error');
        $code       = $this->getRequest()->getParam('code');
        $state      = $this->getRequest()->getParam('state');

        if (!($errorCode || $code) && !$state) {
            return $this;
        }

        if (!$state || $state != Mage::getSingleton('core/session')
                ->getFacebookCsrf()) {
            return $this;
        }

        if ($errorCode) {
            if ($errorCode === 'access_denied') {
                Mage::getSingleton('core/session')
                    ->addNotice(
                        $this->__('Facebook Connect process aborted.')
                    );
                return $this;
            }
            $this->_helper->setException('errorCode', $errorCode);
        }

        if ($code) {
            /** Facebook API green light - proceed */
            $token = $this->_info->getClient()->getAccessToken($code);
            $this->_info->load();

            $customersByFacebookId = $this->_helper
                ->getCustomersByFacebookId($this->_info->getId());

            if (Mage::getSingleton('customer/session')->isLoggedIn()) {
                $this->_helper->isLoggedIn($customersByFacebookId, $this->_info, $token);
                return $this;
            }

            if ($customersByFacebookId->getId()) {
                // Existing connected user - login
                $this->_helper->loginByCustomer($customersByFacebookId);

                $msg = $this->__('You have successfully logged in using your Facebook account.');
                Mage::getSingleton('core/session')
                    ->addSuccess($msg);

                return $this;
            }

            /**If customer already account, set logged in */
            $customersByEmail = $this->_helper->getCustomersByEmail($this->_info->getEmail());
            if ($customersByEmail->getSize()) {
                $this->_helper->setCustomerLoggedIn($customersByEmail, $this->_info, $token);
                return $this;
            }

            $firstName = $this->_info->getFirstName();
            if (empty($firstName)) {
                $this->_helper->setException('firstname');
            }

            $lastName = $this->_info->getLastName();
            if (empty($lastName)) {
                $this->_helper->setException('lastname');
            }

            $gender = $this->_info->getGender();
            $gender = empty($gender) || ($gender == 'female') ? 2 : 1;
            $this->_helper->setSessionForRegister(
                $this->_info->getEmail(),
                $this->_info->getFirstName(),
                $this->_info->getLastName(),
                $this->_info->getId(),
                $this->_info->getBirthday(),
                $gender,
                $token,
                $this->_info->getPicture()->data->url
            );

            return $this;
        }
    }

    /**
     * @return $this
     * @throws Varien_Exception
     */
    public function cpfValidateAction()
    {
        if ($this->getRequest()->getParam('cpf') && $this->getRequest()->isAjax()) {
            $cpf = $this->_helper->cleanCpf($this->getRequest()->getParam('cpf'));

            $customer = $this->_helper->getCustomerByCpf($cpf);
            if ($customer->getId()) {
                $email = $this->_helper->getEmailEncrypted($customer->getEmail());
                $emailEncrypted = Mage::getModel('core/encryption')->encrypt($customer->getEmail());

                $this->getResponse()->setBody(
                    $this->loadLoginTemplate($email, $emailEncrypted)
                );
                return $this;
            }

            $this->getResponse()->setBody(0);
        }
    }

    /**
     * @todo Validate if current user is
     * owner of account
     * @return $this
     * @throws Mage_Core_Model_Store_Exception
     */
    public function validatePasswordAction()
    {
        if ($this->getRequest()->getParam('password')) {
            $email = Mage::getModel('core/encryption')->decrypt($this->getRequest()->getParam('encrypted_data'));
            $password = $this->getRequest()->getParam('password');

            $checkOwner = $this->_helper->checkAccountOwner($email, $password);

            if ($checkOwner) {
               $customer = $this->_helper->getCustomersByEmail($email);
               if ($customer->getSize()) {

                   $facebookToken = Mage::getSingleton('core/session')->getFacebookToken();
                   $facebookId = Mage::getSingleton('core/session')->getFacebookId();

                   $this->_helper->setCustomerLoggedIn($customer, $facebookId, $facebookToken);

                   $this->getResponse()->setBody(1);
                   return $this;
               }
           }
        }
        $this->getResponse()->setBody(0);
    }

    /**
     * @todo Load template to validate password
     * @param $email
     * @param $emailEncrypted
     * @return mixed
     */
    protected function loadLoginTemplate($email, $emailEncrypted)
    {
        $block = $this->getLayout()
            ->createBlock('core/template')
            ->setData(
                [
                    'email' => $email,
                    'encrypted' => $emailEncrypted
                    ]
            );

        $block->setTemplate('persistent/customer/form/login/cpfvalidate-password.phtml');

        return $block->toHtml();
    }

    /**
     * @todo Unset session of facebook login
     * @return void
     */
    public function removeSessionAction()
    {
        /** Unset session to access in template */
        Mage::getSingleton('core/session')->unsFacebookLogin();

        if ($this->getRequest()->getParam('login') == 1) {
            $loginUrl = Mage::getBaseUrl().'customer/account';

            $this->_redirectUrl($loginUrl);
        }
    }
}