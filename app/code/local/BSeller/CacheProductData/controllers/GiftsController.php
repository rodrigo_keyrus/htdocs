<?php
/**
 * BSeller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_CacheProductData
 *
 * @copyright Copyright (c) 2019 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Evandro Veloso Gomes <evandro.gomes@b2wdigital.com>
 */

class BSeller_CacheProductData_GiftsController extends Mage_Core_Controller_Front_Action
{
    /**
     * Extracts only the is_salable from product information array
     *
     * @param array $data
     * @return array
     */
    private function _extractIsSalableData($data)
    {
        $return = [];

        if (!$data) {
            return false;
        }

        foreach ($data as $key => $value) {
            if ($key == 'is_salable') {
                $return[$key] = $value;
            }
        }

        return $return;
    }

    /**
     * Get a list if gifts
     */
    public function getListAction()
    {
        $products = $this->getRequest()->getParam('products');
        if (!$products || !is_array($products)) {
            return;
        }


        $response = [];
        foreach($products as $productId) {
            $response[$productId] = $this->_extractIsSalableData(
                Mage::helper('bseller_cacheproductdata')->getGift($productId)
            );
        }

        $this->getResponse()->setBody(json_encode($response));
    }
}