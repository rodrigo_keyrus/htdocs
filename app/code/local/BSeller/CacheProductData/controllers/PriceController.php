<?php
/**
 * BSeller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_CacheProductData
 *
 * @copyright Copyright (c) 2017 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Luiz Fernando Alves <luiz.anjos@b2wdigital.com>
 */

class BSeller_CacheProductData_PriceController extends Mage_Core_Controller_Front_Action
{
    /**
     * Extracts only the prices data from product information array
     *
     * @param array $data
     * @return array
     */
    private function _extractPricesData($data)
    {
        $return = [];

        if (!$data) {
            return false;
        }

        if (!is_array(reset($data))) {
            foreach ($data as $key => $value) {
                if (in_array($key, ['price', 'current_price', 'show_discount', 'is_salable', 'free_shipping', 'status'])) {
                    $return[$key] = $value;
                }
            }
        } else {
            foreach ($data as $product) {
                $return[$product['entity_id']] = $this->_extractPricesData($product);
            }
        }

        return $return;
    }

    /**
     * Get prices data of a product
     */
    public function getPricesByParentAction()
    {
        $productId = $this->getRequest()->getParam('product');
        if (!$productId) {
            return;
        }

        $this->getResponse()->setBody(
            json_encode(
                $this->_extractPricesData(
                    Mage::helper('bseller_cacheproductdata')->getProductsByParent($productId)
                )
            )
        );
    }

    /**
     * Get prices data of a product
     */
    public function getPricesListAction()
    {
        $products = $this->getRequest()->getParam('products');
        if (!$products) {
            return;
        }

        $response = [];
        foreach($products as $productId) {
            $response[$productId] = $this->_extractPricesData(
                Mage::helper('bseller_cacheproductdata')->getProduct($productId)
            );
        }

        $this->getResponse()->setBody(json_encode($response));
    }
}