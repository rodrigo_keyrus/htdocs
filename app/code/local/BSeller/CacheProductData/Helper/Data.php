<?php
/**
 * BSeller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_CacheProductData
 *
 * @copyright Copyright (c) 2017 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Luiz Fernando Alves <luiz.anjos@b2wdigital.com>
 */

class BSeller_CacheProductData_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Verifies if a product is salable
     *
     * @param array $product
     * @return bool
     */
    private function _isSalable($product)
    {
        return ($product['is_in_stock'] && ($product['status'] == Mage_Catalog_Model_Product_Status::STATUS_ENABLED));
    }

    /**
     * Gets the minimum values of discount that shows in the site
     *
     * @return float
     */
    public function getMinimumDiscount()
    {
        return Mage::getModel('core/variable')
            ->loadByCode('minimum_discount')
            ->getValue('plain');
    }

    /**
     * Writes a message in the log
     *
     * @param $message
     */
    private function _writeLog($message)
    {
        Mage::helper('bseller_elasticlog/log')->logInfo('productcache-error', $message);
    }

    /**
     * Get the discount of the product, if it will be shown to the customer
     *
     * @param array $product
     * @return int
     */
    private function _getDiscount($product)
    {
        $show_discount = false;

        if ($product['current_price'] < $product['price']) {
            if ((1 - ($product['current_price'] / $product['price'])) * 100 > $this->getMinimumDiscount()) {
                $show_discount = (($product['price'] - $product['current_price']) / $product['price']) * 100;
                if (strripos($show_discount, '.')) {
                    $show_discount = floor($show_discount);
                }
            }
        }

        return $show_discount;
    }

    /**
     * Gets from cache (if in cache) or database the data of products sons using the parent product ID.
     * If not in cache it also adds the data in cache.
     *
     * @param int $parentProductId
     * @param bool $returnParent
     *
     * @return array|bool|null
     */
    public function getProductsByParent($parentProductId, $returnParent = false)
    {
        $childrenProducts = $this->_getChildrensOnCache($parentProductId);
        if (is_array($childrenProducts) && !empty($childrenProducts)) {
            return $childrenProducts;
        }

        $childrenProducts = Mage::getModel('bseller_cacheproductdata/product')
            ->getProductsByParent($parentProductId);

        /** @var Mage_Catalog_Model_Product $parentProduct */
        $parentProduct = json_decode(json_encode(
            Mage::getModel('catalog/product')->load($parentProductId)->getData()), true
        );

        if ($parentProduct && ($parentProduct['status'] != 1)) {
            return false;
        }

        // Setting the minor prices and is_salable for the parent product
        $parentProduct['price']         = false;
        $parentProduct['is_salable']    = false;
        $parentProduct['current_price'] = false;

        foreach ($childrenProducts as $product) {
            $index = $product['entity_id'];

            $childrenProducts[$index]['is_salable'] = $this->_isSalable($product);

            // For Neemu
            $months = $this->_getInstallments($product['current_price']);
            $childrenProducts[$index]['months']      = $months;
            $childrenProducts[$index]['installment'] = ($product['current_price'] / $months) . ' BRL';

            if ($childrenProducts[$index]['is_salable']) {
                $parentProduct['is_salable'] = true;

                if (!$parentProduct['current_price'] || ($product['current_price'] < $parentProduct['current_price'])) {
                    $parentProduct['current_price'] = $product['current_price'];
                    $parentProduct['price'] = $product['price'];
                }

                $childrenProducts[$index]['show_discount'] = $this->_getDiscount($product);
            }
        }

        $parentProduct['show_discount'] = $this->_getDiscount($parentProduct);

        $minProductAmount = Mage::helper('bseller_flag')->getStoreConfig('min_product_amount');

        $parentProduct['free_shipping'] = ($minProductAmount <= $parentProduct['current_price']);

        $this->_setChildrensOnCache($parentProductId, $childrenProducts);

        $this->_setProductOnCache($parentProduct);

        if (!$returnParent) {
            return $childrenProducts;
        } else {
            return $parentProduct;
        }
    }

    /**
     * Get from cache (if in cache) or database the data of the parent product.
     * If not in cache it also adds the data in cache.
     *
     * @param $productId
     *
     * @return mixed|null
     */
    public function getProduct($productId)
    {
        $product = $this->_getProductOnCache($productId);
        if ($product) {
            return $product;
        }

        // Generating all the caches
        return $this->getProductsByParent($productId, true);
    }

    /**
     * Uses the products data to select the correct lifetime to the product's cache
     *
     * @param array $product
     *
     * @return string|false
     */
    private function _getLifeTime($product)
    {
        $expire = false;

        if (($product['type_id'] == Mage_Catalog_Model_Product_Type::TYPE_SIMPLE) && $this->_isSalable($product)) {
            if (
                $product['special_from_date'] &&
                ($product['special_from_date'] >= date('Y-m-d H:i:s')) &&
                (!$expire || ($product['special_from_date'] <= $expire))
            ) {
                $expire = $product['special_from_date'];
            } elseif (
                $product['special_to_date'] &&
                ($product['special_to_date'] >= date('Y-m-d H:i:s')) &&
                (!$expire || ($product['special_to_date'] <= $expire))
            ) {
                $expire = $product['special_to_date'];
            }
        }

        if (!$expire) {
            return false;
        }

        return round(abs(strtotime($expire) - strtotime('now')));
    }

    /**
     * Writes on cache the product's data associated to the same parent product.
     *
     * @param int $parentProductId
     * @param array $products
     *
     * @return bool
     */
    private function _setChildrensOnCache($parentProductId, $products)
    {
        // Get the minor special_price date
        $lifeTime = false;

        /** @var Mage_Catalog_Model_Product $product */
        foreach ($products as $product) {
            $lifeTimeProduct = $this->_getLifeTime($product);
            if ($lifeTimeProduct && (!$lifeTime || ($lifeTimeProduct < $lifeTime))) {
                $lifeTime = $lifeTimeProduct;
            }
        }

        try {
            $this->_getCacheHelper()->saveCache(
                serialize($products),
                'parent_product:' . $parentProductId,
                array('product_parent'),
                $lifeTime
            );

            foreach ($products as $product) {
                $this->_setProductOnCache($product);
            }

            $this->_registerChildrenProducts($parentProductId, $products);
            $lastTenProducts = unserialize($this->_getCacheHelper()->loadCache('last_ten_products'));
            if (!$lastTenProducts) {
                $lastTenProducts = [];
            }

            $lastTenProducts = array_slice(array_merge(array_keys($products), $lastTenProducts), 0, 10);

            $this->_getCacheHelper()->saveCache(
                serialize($lastTenProducts),
                'last_ten_products'
            );

        } catch (Exception $e) {
            $this->_writeLog('Error caching parent product: ' . $e->getMessage());
        }

        return true;
    }

    /**
     * Writes on cache the association between parent and children products
     *
     * @param int $parentProductId
     * @param array $products
     */
    private function _registerChildrenProducts($parentProductId, $products)
    {
        foreach ($products as $product) {
            try {
                $this->_getCacheHelper()->saveCache(
                    $parentProductId,
                    'children_product:' . $product['entity_id'],
                    array('children_product')
                );
            } catch (Exception $e) {
                $this->_writeLog('Error caching children product: ' . $e->getMessage());
            }
        }
    }

    /**
     * Write the product data in cache as a single entry.
     * For parent products.
     *
     * @param array $product
     *
     * @return bool
     */
    private function _setProductOnCache($product)
    {
        try {
            $this->_getCacheHelper()->saveCache(
                serialize($product),
                'product:' . $product['entity_id'],
                ['product'],
                $this->_getLifeTime($product)
            );

            $this->_getCacheHelper()->saveCache(
                serialize(
                    [
                        'id'          => $product['entity_id'],
                        'type_id'     => $product['type_id'],
                        'parent_code' => $product['ax_parent_code']
                    ]
                ),
                'product_sku:' . $product['sku'],
                ['product_sku']
            );

            $this->_getCacheHelper()->saveCache(
                $product['sku'],
                'sku_product:' . $product['entity_id'],
                ['sku_product']
            );

            $this->_getCacheHelper()->removeCache('not_found_sku:' . $product['sku']);

        } catch (Exception $e) {
            $this->_writeLog('Error caching product: ' . $e->getMessage());
        }

        return true;
    }

    /**
     * Get the product data in cache.
     * For parent products.
     * Used in list pages
     *
     * @param int $productId
     *
     * @return array|false
     */
    private function _getProductOnCache($productId)
    {
        return unserialize($this->_getCacheHelper()->loadCache('product:' . $productId));
    }

    /**
     * Get the children product's data from the cache
     * Used in PDP's page
     *
     * @param int $parentProductId
     *
     * @return array|null
     */
    private function _getChildrensOnCache($parentProductId)
    {
        return unserialize($this->_getCacheHelper()->loadCache('parent_product:' . $parentProductId));
    }

    /**
     * Removes from cache the data associated from a product
     *
     * @param int $productId
     */
    public function removeProductFromCache($productId)
    {
        $this->_getCacheHelper()->removeCache('product:' . $productId);
        $this->_getCacheHelper()->removeCache('parent_product:' . $productId);

        $parentProductId = $this->_getCacheHelper()->loadCache('children_product:' . $productId);

        if ($parentProductId) {
            $this->_getCacheHelper()->removeCache('parent_product:' . $parentProductId);
            $this->_getCacheHelper()->removeCache('product:' . $parentProductId);
        }

        $sku = $this->_getCacheHelper()->loadCache('sku_product:' . $productId);
        if ($sku) {
            $this->_getCacheHelper()->removeCache('product_sku:' . $parentProductId);
        }
    }

    /**
     * Get the product data in cache.
     * For gift products.
     *
     * @param int $productId
     *
     * @return array|bool|false|mixed
     */
    public function getGift($productId)
    {
        $product = $this->_getProductOnCache($productId);
        if ($product) {
            return $product;
        }

        $product = json_decode(json_encode(
            Mage::getModel('catalog/product')->load($productId)->getData()), true
        );

        if ($product['status'] != 1) {
            return false;
        }

        $product['is_salable'] = $this->_isSalable($product);

        // For Neemu
        $months = $this->_getInstallments($product['current_price']);
        $product['months']      = $months;
        $product['installment'] = ($product['current_price'] / $months) . ' BRL';

        $this->_setProductOnCache($product);

        return $product;
    }

    /**
     * Get from cache (if in cache) or database the data of the products.
     * If not in cache it also adds the data in cache.
     *
     * @param array $skus
     *
     * @return mixed|null
     */
    public function getProductsBySkus($skus)
    {
        $products = [];
        foreach ($skus as $sku) {
            $product = $this->getProductBySku($sku);
            if ($product) {
                $products[] = $product;
            }
        }

        return $products;
    }

    /**
     * Get from cache (if in cache) or database the data of the product.
     * If not in cache it also adds the data in cache.
     *
     * @param string $sku
     * @param int|false $idChildren
     *
     * @return mixed|null
     */
    public function getProductBySku($sku, $idChildren = false)
    {
        if ($this->_getCacheHelper()->loadCache('not_found_sku:' . $sku)) {
            return false;
        }

        $data = unserialize($this->_getCacheHelper()->loadCache('product_sku:' . $sku));
        if (!$data) {
            $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
            if (!$product) {
                $this->_getCacheHelper()->saveCache(
                    true,
                    'not_found_sku:' . $sku,
                    array('not_found')
                );

                return false;
            }

            $data = [
                'id'          => $product->getId(),
                'type_id'     => $product->getTypeId(),
                'parent_code' => $product->getAxParentCode()
            ];
        }

        if ($data['type_id'] != 'simple') {
            $parent = $this->getProductsByParent($data['id']);

            if (!$idChildren) {
                return $parent;
            } elseif (array_key_exists($idChildren, $parent)) {
                return $parent[$idChildren];
            } else {
                return false;
            }

        } elseif ($data['parent_code'] != $sku) {
            return $this->getProductBySku($data['parent_code'], $data['id']);
        }

        return false;
    }

    public function getLastTenProducts()
    {
        $idProducts = unserialize($this->_getCacheHelper()->loadCache('last_ten_products'));

        if (!$idProducts) {
            return [];
        }

        $products = [];
        foreach ($idProducts as $idProduct) {
            $idParent = $this->_getCacheHelper()->loadCache('children_product:' . $idProduct);
            $parent   = unserialize($this->_getCacheHelper()->loadCache('parent_product:' . $idParent));

            if (array_key_exists($idProduct, $parent)) {
                $products[] = $parent[$idProduct];
            }
        }

        return $products;
    }

    /**
     * Return product installments
     *
     * @param float $price
     * @return int
     */
    private function _getInstallments($price)
    {
        /** @var Uecommerce_Mundipagg_Helper_Installments $helper */
        $helper = Mage::helper('mundipagg/installments');

        /** @var int $installments */
        $installments = $helper->getConfigValue($price);

        return ($installments > 0) ? $installments : 1;
    }

    /**
     * Get the cache's read/write helper
     *
     * @return BSeller_CacheProductData_Helper_Cache|Mage_Core_Helper_Abstract
     */
    private function _getCacheHelper()
    {
        return Mage::helper('bseller_cacheproductdata/cache');
    }
}