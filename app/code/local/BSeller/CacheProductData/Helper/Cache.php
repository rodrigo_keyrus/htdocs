<?php
/**
 * BSeller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_CacheProductData
 *
 * @copyright Copyright (c) 2017 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Luiz Fernando Alves <luiz.anjos@b2wdigital.com>
 */

class BSeller_CacheProductData_Helper_Cache extends Mage_Core_Helper_Abstract
{
    /**
     * Cache object
     *
     * @var Zend_Cache_Core
     */
    protected $_cache;

    /**
     * Initialize application cache instance
     *
     * @return Mage_Core_Model_App
     */
    public function __construct()
    {                    
        $config = Mage::getConfig()->getNode('global/productcachedata');
        
        if ($config) {
            $config  = $config->asArray();

            $class   = $config['class'];
            $options = $config['options'];
        } else {
            $config = Mage::getConfig()->getNode('global/cache')->asArray();

            $class   = $config['backend'];
            $options = $config['backend_options'];
        }

        try {
            $this->_cache = new $class($options);
        } catch (Exception $e) {

        }
    }

    /**
     * Loading cache data
     *
     * @param   string $id
     * @return  mixed
     */
    public function loadCache($id)
    {
        if (!$this->_cache) {
            return false;
        }

        return $this->_cache->load($id);
    }

    /**
     * Saving cache data
     *
     * @param   mixed $data
     * @param   string $id
     * @param   array $tags
     * @return  Mage_Core_Model_App
     */
    public function saveCache($data, $id, $tags = array(), $lifeTime = false)
    {
        if (!$this->_cache) {
            return false;
        }

        $this->_cache->save($data, $id, $tags, $lifeTime);

        return $this;
    }

    /**
     * Remove cache
     *
     * @param   string $id
     * @return  Mage_Core_Model_App
     */
    public function removeCache($id)
    {
        if (!$this->_cache) {
            return false;
        }

        $this->_cache->remove($id);

        return $this;
    }

    /**
     * Cleaning cache
     *
     * @param   array $tags
     * @return  Mage_Core_Model_App
     */
    public function cleanCache($tags = array())
    {
        if (!$this->_cache) {
            return false;
        }

        $this->_cache->clean('all', $tags);
    }
}