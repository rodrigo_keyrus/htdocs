<?php
/**
 * BSeller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_CacheProductData
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Evandro Veloso Gomes <evandro.gomes@e-smart.com.br>
 */

class BSeller_CacheProductData_Model_Observer_Stock
{
    /**
     * Remove from the cache the product, if it is sold out
     *
     * @param Varien_Event_Observer $observer
     */
    public function updatedStock(Varien_Event_Observer $observer)
    {
        $productId = $observer->getEvent()->getItem()->getProductId();

        /** @var Mage_CatalogInventory_Model_Stock_Item $stock */
        $stock = $observer->getEvent()->getItem();

        if ($stock->getOrigData('is_in_stock') != $stock->getData('is_in_stock')) {
            Mage::helper('bseller_cacheproductdata')->removeProductFromCache($productId);
        }
    }
}