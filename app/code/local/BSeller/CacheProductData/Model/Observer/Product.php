<?php
/**
 * BSeller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_CacheProductData
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Evandro Veloso Gomes <evandro.gomes@e-smart.com.br>
 */

class BSeller_CacheProductData_Model_Observer_Product
{
    /**
     * Whenever you change the simple product,
     * perform the redis key update with the new price.
     *
     * @param Varien_Event_Observer $observer
     *
     * @return Mage_Catalog_Model_Product
     *
     * @throws Varien_Exception
     */
    public function catalogProductSaveAfter(Varien_Event_Observer $observer)
    {
        $product = $observer->getEvent()->getProduct();

        Mage::getResourceModel('catalog/product_indexer_price')->reindexProductIds([$product->getId()]);

        // Updating the flat table
        Mage::getModel('catalog/product_flat_indexer')->updateProduct(array($product->getId()), null);

        return $product;
    }

    /**
     * Removes the keys of the product from the cache forcing to regenerate on the next access
     *
     * @param Varien_Event_Observer $observer
     */
    public function catalogProductFlatUpdateProduct(Varien_Event_Observer $observer)
    {
        $productIds = $observer->getEvent()->getProductIds();
        foreach ($productIds as $productId) {
            Mage::helper('bseller_cacheproductdata')->removeProductFromCache($productId);
        }
    }

    /**
     * Removes all product data in cache
     */
    public function cleanCache()
    {
        Mage::helper('bseller_cacheproductdata/cache')->cleanCache(
            ['product_parent', 'product', 'not_found', 'product_sku', 'sku_product']
        );
        Mage::helper('bseller_cacheproductdata/cache')->removeCache('stock_changed_products');
    }

    /**
     * Cleans the cache
     *
     * @param   Varien_Event_Observer $observer
     *
     * @return  BSeller_CacheProductData_Model_Observer_Product $this
     */
    public function catalogruleAfterApply($observer)
    {
        Mage::helper('bseller_cacheproductdata/cache')->cleanCache();

        return $this;
    }
}