<?php
/**
 * BSeller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_CacheProductData
 *
 * @copyright Copyright (c) 2017 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Luiz Fernando Alves <luiz.anjos@b2wdigital.com>
 */
class BSeller_CacheProductData_Model_Product extends Mage_Core_Model_Abstract
{
    /**
     * @param Mage_Catalog_Model_Product|int $parent
     * @return array
     */
    public function getProductsByParent($parent)
    {
        if (!is_object($parent)) {
            /** @var Mage_Catalog_Model_Product $parent */
            $parent = Mage::getModel('catalog/product')->load($parent);
        }

        if ($parent->getTypeId() == 'grouped') {
            $products = $parent->getTypeInstance(true)->getAssociatedProducts($parent);
        } else {
            $idsProducts = [];

            $options = $parent->getTypeInstance(true)->getChildrenIds($parent->getId(), false);
            foreach ($options as $ids) {
                $idsProducts = array_merge($idsProducts, $ids);
            }

            $products = Mage::getModel('catalog/product')->getCollection()
                ->addAttributeToSelect('*')
                ->setFlag('require_stock_items', true)
                ->addFieldToFilter('entity_id', array('in'=> $idsProducts))
                ->load()
                ->getItems();
        }

        $minimum  = Mage::getModel('core/variable')
            ->loadByCode('minimum_discount')
            ->getValue('plain');

        if (!is_array($products)) {
            return [];
        }

        $associatedProducts = array();

        /** @var Mage_Catalog_Model_Product $product */
        foreach ($products as $product) {

            $showDiscount = false;

            if ($product->getData('special_price')) {
                $currentPrice = $product->getData('special_price');

                if ((1 - ($currentPrice / $product->getData('price'))) * 100 > $minimum) {
                    $showDiscount = (($product->getData('price') - $currentPrice) / $product->getData('price')) * 100;
                    if (strripos($showDiscount, '.')) {
                        $showDiscount = floor($showDiscount);
                    }
                }
            }

            $associatedProducts[$product->getId()]['current_price'] = $product->getFinalPrice();
            $associatedProducts[$product->getId()]['show_discount'] = $showDiscount;
            $associatedProducts[$product->getId()] = array_merge(
                $associatedProducts[$product->getId()],
                json_decode(json_encode($product->getData()), true)
            );
        }

        return $associatedProducts;
    }

    /**
     * Return all parents IDs
     * @param $productId
     * @return array
     */
    public function getParentByChildId($productId)
    {
        $parentIds = Mage::getModel('catalog/product_type_grouped')->getParentIdsByChild($productId);

        return $parentIds;
    }
}