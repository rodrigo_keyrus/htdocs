<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_PromotionalFlag
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Evandro Veloso Gomes <evandro.gomes@e-smart.com.br>
 */

class BSeller_PromotionalFlag_Model_Resource_Flag_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    /**
     * Collection initialization
     */
    public function _construct()
    {
        $this->_init('bseller_promotionalflag/flag');
    }

    /**
     * Add status to filter collection
     *
     * @param true|bool $status
     * @return $this
     */
    public function addStatusFilter($status = true)
    {
        $this->addFieldToFilter('status', (bool) $status);

        return $this;
    }

    /**
     * Add all the conditional filters
     *
     * @param string $sku
     * @param int    $manufacturer
     * @param array  $categoryIds
     *
     * @return $this
     */
    public function addConditionalFilters($sku, $manufacturer, $categoryIds)
    {
        if (!$sku) {
            return $this;
        }

        $strCategory = '';
        if (is_array($categoryIds) && !empty($categoryIds)) {
            $strCategory = '(';
            $extra = '';
            foreach ($categoryIds as $category) {
                $strCategory .= $extra . "FIND_IN_SET('$category', category_view) OR FIND_IN_SET(' $category', category_view)";
                $extra = ' OR ';
            }
            $strCategory .= ')';
        }

        $this->getSelect()
            ->where("
                    (
                        (
                            (FIND_IN_SET('$sku', prd) OR FIND_IN_SET(' $sku', prd)) OR 
                            manufacturer_view = '$manufacturer'
                            " . ($strCategory ? " OR $strCategory" : '') . "
                        ) AND aggregator = 'any') OR 
                    (
                        (FIND_IN_SET('$sku', prd) OR FIND_IN_SET(' $sku', prd) OR prd = '') AND 
                        (manufacturer_view = '$manufacturer' OR manufacturer_view = '') AND 
                        " . ($strCategory ? "($strCategory  OR category_view = '') AND " : '') . "
                        aggregator = 'all'
                    )
                "
            );

        return $this;
    }

    /**
     * Add from_date and to_date to filter collection
     *
     * @return $this
     */
    public function addCheckDateFilter()
    {
        $this->addFieldToFilter(['rule_from_date', 'rule_from_date'],
                [
                    ['lteq' => Mage::getModel('core/date')->date('Y-m-d H:i:s')],
                    ['null' => true]
                ]
            )
            ->addFieldToFilter(['rule_to_date', 'rule_to_date'],
                [
                    ['gteq' => Mage::getModel('core/date')->date('Y-m-d H:i:s')],
                    ['null' => true]
                ]
            );

        return $this;
    }

    /**
     * Add flag type to order collection
     *
     * @param string $dir
     * @return $this
     */
    public function addFlagOrder($dir = Varien_Db_Select::SQL_ASC)
    {
        $this->addOrder('flag', $dir);

        return $this;
    }
}
