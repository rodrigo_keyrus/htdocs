<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_PromotionalFlag
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Evandro Veloso Gomes <evandro.gomes@e-smart.com.br>
 */

class BSeller_PromotionalFlag_Model_Observer
{
    /**
     *
     *
     * @param Varien_Event_Observer $observer
     *
     * @return $this
     *
     * @throws Varien_Exception
     */
    public function updatePromoQuoteTabMainForm(Varien_Event_Observer $observer)
    {
        $model = Mage::registry('current_promo_quote_rule');
        if ($model->getId()) {
            $model = Mage::getModel('bseller_promotionalflag/flag')
                ->load($model->getId(), 'rule_id');
        } else {
            $model = Mage::getModel('bseller_promotionalflag/flag');
        }

        $form = $observer->getEvent()->getForm();
        if (!$form) {
            return $this;
        }

        $form->addValues($model->getData());

        return $this;
    }

    /**
     * Saves a flag register in the database
     *
     * @param Varien_Event_Observer $observer
     * @return $this
     */
    public function saveFlag(Varien_Event_Observer $observer)
    {
        $rule             = $observer->getEvent()->getRule();
        $aggregator       = $rule['conditions']['1--1']['aggregator'];
        $gift             = Mage::getModel('catalog/product')->load($rule['gift_sku']);
        $prds             = '';
        $manufacturerView = '';
        $categoryView     = '';
        $promotionalFlag  = Mage::getModel('bseller_promotionalflag/flag')->load($rule['rule_id'], 'rule_id');

        if (is_null($promotionalFlag)) {
            $promotionalFlag = Mage::getModel('bseller_promotionalflag/flag');
        }

        foreach ($rule['conditions'] as $condition) {
            if ($condition['type'] == 'salesrule/rule_condition_product') {
                if ($condition['attribute'] == 'sku') {
                    $prds = $condition['value'];
                } elseif ($condition['attribute'] == 'manufacturer') {
                    if (!empty($manufacturerView) && !is_null($manufacturerView)) {
                        $manufacturerView .= ',' . $condition['value'];
                    } else {
                        $manufacturerView = $condition['value'];
                    }
                } elseif ($condition['attribute'] == 'category_ids') {
                    if (!empty($categoryView) && !is_null($categoryView)) {
                        $categoryView .= ',' . $condition['value'];
                    } else {
                        $categoryView = $condition['value'];
                    }
                }
            }
        }

        $promotionalFlag->setManufacturerId($gift->getManufacturer())
            ->setPrd($prds)
            ->setAggregator($aggregator)
            ->setManufacturerView($manufacturerView)
            ->setCategoryView($categoryView)
            ->setSku($rule['gift_sku'])
            ->setStatus($rule['is_active'])
            ->setRuleFromDate($rule['from_date'])
            ->setRuleToDate($rule['to_date'])
            ->setRuleId($rule['rule_id'])
            ->setFlag($rule['flag'])
            ->setFlagTitle($rule['flag_title'])
            ->setFlagDescription(array_key_exists('flag_description', $rule) ? $rule['flag_description'] : null)
            ->setFlagValue(array_key_exists('flag_value', $rule) ? $rule['flag_value'] : null);

        try {
            $promotionalFlag->save();
        } catch (Exception $e) {
            Mage::throwException($e);
        }

        return $this;
    }

    public function deleteFlag(Varien_Event_Observer $observer)
    {
        $rule  = $observer->getEvent()->getRule();
        $promotionalFlag = Mage::getModel('bseller_promotionalflag/flag')->load($rule['rule_id'], 'rule_id');

        if (!is_null($promotionalFlag)) {
            try {
                $promotionalFlag->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('salesrule')->__('The Promotional has been deleted.'));
            } catch (Exception $e) {
                Mage::throwException($e);
            }
        }

        return $this;
    }
}