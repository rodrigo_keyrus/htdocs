# Promotional Flags

Promotion customization to display some promotions information on the product page (similar to what was done in the BirthdayGift task).

The display rule will follow the same line allowing only the creation of these flags for cart promotions linked to:

    Category
    Brand
    SKU

------------------

It needs to select the Flag that will be displayed:

1) Flag

    Option 1 - Whitout Flag
    
    Option 2 - Basic Flag
    
    Option 3 - Promotional Flag


Each option will display a different flag on the product page (in terms of layout).

If Basic Flag is selected, it will enable a new field:

2) Flag Title

    Field type text, with limit of 50 characters

If you select Promotional Flag, you will enable the previous field (Flag Title, and two more new fields):

3) Flag Description

    Textfield field

4) Flag Value

    Input field (numeric), which will be the cut to display this flag only in SKUs with a value greater than this field.

## Dependencies

                BSeller_Core
                BSeller_GiftShowcase

### Changelog

