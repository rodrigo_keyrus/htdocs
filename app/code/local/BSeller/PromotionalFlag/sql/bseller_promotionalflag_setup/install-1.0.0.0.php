<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_PromotionalFlag
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Evandro Veloso Gomes <evandro.gomes@e-smart.com.br>
 */

/* @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;

$installer->startSetup();

/** @var Varien_Db_Adapter_Pdo_Mysql $connection */
$connection = $installer->getConnection();

/**
 * Create promotional_flag table
 */
$table = $connection->newTable($installer->getTable('bseller_promotionalflag/flag'))
    ->addColumn(
        'entity_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        11,
        [
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
            'primary'  => true
        ],
        'Entity ID'
    )
    ->addColumn(
        'manufacturer_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        11,
        [],
        'Manufacturer ID'
    )
    ->addColumn(
        'status',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        11,
        [
            'unsigned' => true,
            'nullable' => false,
            'default'  => 0
        ],
        'Status'
    )
    ->addColumn(
        'updated_at',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        [
            'nullable' => false,
            'default'  => '0000-00-00 00:00:00'
        ],
        'Updated At'
    )
    ->addColumn(
        'created_at',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        [
            'nullable' => false,
            'default'  => '0000-00-00 00:00:00'
        ],
        'Created At'
    )
    ->addColumn(
        'sku',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        255,
        [
            'nullable' => false,
        ],
        'Product SKU'
    )
    ->addColumn(
        'rule_from_date',
        Varien_Db_Ddl_Table::TYPE_DATETIME,
        255,
        [
            'nullable' => true,
        ],
        'A Partir da Data'
    )
    ->addColumn(
        'rule_to_date',
        Varien_Db_Ddl_Table::TYPE_DATETIME,
        255,
        [
            'nullable' => true,
        ],
        'Até a Data'
    )
    ->addColumn(
        'rule_id',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        255,
        [
            'nullable' => true,
        ],
        'Rule ID'
    )
    ->addColumn(
        'prd',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        255,
        [
            'nullable' => false,
        ],
        'Product (PRD)'
    )
    ->addColumn(
        'manufacturer_view',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        false,
        [
            'nullable' => false,
        ],
        'Manufacturer View'
    )
    ->addColumn(
        'flag',
        Varien_Db_Ddl_Table::TYPE_TINYINT,
        false,
        [
            'nullable' => false,
            'default'  => '0',
        ],
        'Flag'
    )
    ->addColumn(
        'flag_title',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        50,
        [
            'nullable' => true,
        ],
        'Flag Title'
    )
    ->addColumn(
        'flag_description',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        false,
        [
            'nullable' => true,
        ],
        'Flag Description'
    )
    ->addColumn(
        'flag_value',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        11,
        [
            'nullable' => true,
        ],
        'Flag Value'
    )
    ->setComment('BSeller Promotional Flag');

$connection->createTable($table);

$installer->endSetup();
