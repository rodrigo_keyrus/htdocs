<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_PromotionalFlag
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Evandro Veloso Gomes <evandro.gomes@e-smart.com.br>
 */

class BSeller_PromotionalFlag_Block_Product_View extends Mage_Catalog_Block_Product_View
{
    /**
     * Cache config values
     *
     * @var string
     */
    protected $_cacheKey = 'bseller_promotional_flag_view';
    protected $_cacheTag = 'bseller_promotional_flag_view';

    /**
     * Singleton key
     *
     * @var string
     */
    protected $_singletonKey = 'bseller_promotional_flag_view';

    /**
     * Return available flags on product page
     *
     * @param $type Type of the promotional flag
     *
     * @return BSeller_PromotionalFlag_Model_Resource_Flag_Collection
     */
    public function getFlags($type)
    {
        /** @var Mage_Catalog_Model_Product $product */
        $product = $this->getProduct();

        $flags = Mage::registry('bseller_promotionalflags_flags');
        if ($flags) {
            return (array_key_exists($type, $flags) ? $flags[$type] : new ArrayObject());
        }

        /** @var BSeller_PromotionalFlag_Model_Resource_Flag_Collection $collection */
        $collection = Mage::getResourceModel('bseller_promotionalflag/flag_collection')
            ->addStatusFilter()
            ->addCheckDateFilter()
            ->addFlagOrder()
            ->addConditionalFilters(
                $product->getSku(),
                $product->getManufacturer(),
                $product->getCategoryIds()
            );

        // Splitting the flags by type
        if ($collection->getSize()) {

            $flags = array();
            foreach ($collection as $flag) {
                $flags[ $flag->getData('flag') ][] = $flag;
            }

            Mage::register('bseller_promotionalflags_flags', $flags);
            return (array_key_exists($type, $flags) ? $flags[ $type ] : new ArrayObject());
        }

        return new ArrayObject();
    }
}