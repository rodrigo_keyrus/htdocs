<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_GiftShowcase
 *
 * @copyright Copyright (c) 2017 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Guilherme Miguelete <guilherme.miguelete@e-smart.com.br>
 */


/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

/** @var Varien_Db_Adapter_Pdo_Mysql $connection */
$connection = $installer->getConnection();

$connection->addColumn(
    $installer->getTable('bseller_giftshowcase/gift'),
    'rule_from_date',
    [
        'type'      => Varien_Db_Ddl_Table::TYPE_DATETIME,
        'after'     => 'status',
        'length'    => 255,
        'nullable' => true,
        'comment'   => 'A Partir da Data'
    ]
);

$connection->addColumn(
    $installer->getTable('bseller_giftshowcase/gift'),
    'rule_to_date',
    [
        'type'      => Varien_Db_Ddl_Table::TYPE_DATETIME,
        'after'     => 'rule_from_date',
        'length'    => 255,
        'nullable' => true,
        'comment'   => 'Até a Data'
    ]
);

$connection->addColumn(
    $installer->getTable('bseller_giftshowcase/gift'),
    'rule_id',
    [
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'length'    => 255,
        'nullable' => true,
        'comment'   => 'Rule ID'
    ]
);

$installer->endSetup();
