<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_GiftShowcase
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

/* @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;

$installer->startSetup();

/** @var Varien_Db_Adapter_Pdo_Mysql $connection */
$connection = $installer->getConnection();

/**
 * Create gift showcase table
 */
$table = $connection->newTable($installer->getTable('bseller_giftshowcase/gift'))
    ->addColumn(
        'entity_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        11,
        [
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
            'primary'  => true
        ],
        'Entity ID'
    )
    ->addColumn(
        'title',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        255,
        [
            'nullable' => false,
            'default'  => ''
        ],
        'Title'
    )
    ->addColumn(
        'description',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        null,
        [
            'nullable' => false,
            'default'  => ''
        ],
        'Description'
    )
    ->addColumn(
        'image',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        null,
        [
            'nullable' => false,
            'default'  => ''
        ],
        'Image'
    )
    ->addColumn(
        'link',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        null,
        [
            'nullable' => false,
            'default'  => ''
        ],
        'Link'
    )
    ->addColumn(
        'agreements',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        null,
        [
            'nullable' => false,
            'default'  => ''
        ],
        'Agreements'
    )
    ->addColumn(
        'position',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        11,
        [
            'unsigned' => true,
            'nullable' => false
        ],
        'Position'
    )
    ->addColumn(
        'status',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        11,
        [
            'unsigned' => true,
            'nullable' => false,
            'default'  => 0
        ],
        'Status'
    )
    ->addColumn(
        'updated_at',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        [
            'nullable' => false,
            'default'  => '0000-00-00 00:00:00'
        ],
        'Updated At'
    )
    ->addColumn(
        'created_at',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        [
            'nullable' => false,
            'default'  => '0000-00-00 00:00:00'
        ],
        'Created At'
    )
    ->setComment('BSeller Gift Showcase');

$connection->createTable($table);

$installer->endSetup();
