<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_GiftShowcase
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_GiftShowcase_Helper_Data extends BSeller_Core_Helper_Data
{
    use BSeller_Core_Trait_Data;

    /**
     * Register key prefix
     *
     * @var string
     */
    protected $_registerKeyPrefix = 'bseller_giftshowcase';

    /**
     * Register key suffix
     *
     * @var string
     */
    protected $_registerKeySuffix = BSeller_GiftShowcase_Model_Gift::ENTITY;

    /**
     * Check is module exists and enabled in config
     *
     * @return bool
     */
    public function isModuleEnabled($moduleName = null)
    {
        return Mage::getStoreConfigFlag('bseller_giftshowcase/settings/active')
            && parent::isModuleEnabled($this->_getModuleName());
    }

    /**
     * Save object data in register
     *
     * @param BSeller_GiftShowcase_Model_Gift $value
     * @param true|bool $graceful
     * @return $this
     */
    public function addObjectInfo($value, $graceful = true)
    {
        Mage::register($this->_registerKeyPrefix . '_' . $this->_registerKeySuffix, $value, $graceful);

        return $this;
    }

    /**
     * Return object data of register
     *
     * @return BSeller_GiftShowcase_Model_Gift|Varien_Object
     */
    public function getObjectInfo()
    {
        if ($object = Mage::registry($this->_registerKeyPrefix . '_' . $this->_registerKeySuffix)) {
            return $object;
        }

        return new Varien_Object();
    }

    /**
     * Check if exist image to upload
     *
     * @return bool
     */
    public function hasImageToUpload()
    {
        if (!isset($_FILES['image'])) {
            return false;
        }

        $image = $_FILES['image'];

        if (empty($image['name'])) {
            return false;
        }

        if ($image['size'] <= 0) {
            return false;
        }

        if ($image['error']) {
            return false;
        }

        return true;
    }

    /**
     * Check if exists option to delete image
     *
     * @return bool
     */
    public function hasImageToDelete()
    {
        $post = $this->getRequest()->getPost();

        return (bool) isset($post['image']['delete']);
    }

    /**
     * Return export filename
     *
     * @param string $ext
     * @return string
     */
    public function getExportFilename($ext)
    {
        return ('gift_' . time() . '.' . $ext);
    }

    /**
     * @param null $sku
     *
     * @return string
     */
    public function getImageProductBySku($sku = null)
    {
        $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);

        if (!$product) {
            return;
        }

        return (string) Mage::helper('catalog/image')->init($product, 'small_image')->resize(210);
    }
}
