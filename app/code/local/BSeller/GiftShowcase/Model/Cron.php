<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Core
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Diogo Baracho <diogo.baracho@e-smart.com.br>
 */

class BSeller_GiftShowcase_Model_Cron{

    /**
     * run
     */
    public function run(){

        if(!Mage::getStoreConfig('bseller_giftshowcase/cronjob/enable_cron')){
            return;
        }

        try{

            $cronDisable = Mage::getStoreConfig('bseller_giftshowcase/cronjob/enable_cron_disable');
            $cronEnable  = Mage::getStoreConfig('bseller_giftshowcase/cronjob/enable_cron_enable');

            $collection = Mage::getResourceModel('bseller_giftshowcase/gift_collection');

            if(!Mage::getStoreConfig('bseller_giftshowcase/cronjob/check_disabled_gifts')) {
                $collection->addFieldToFilter('status', array('eq' => '1'));
            }

            $collection->setPageSize(50);

            $collection->load();
            foreach($collection as $gift){
                $sku = $gift->getData('sku');
                if(empty($sku)) {
                    continue;
                }
                try {
                    /* will check sku availability*/
                    if(Mage::getModel('bseller_giftshowcase/gift')->productAvailability($sku)){
                        if($cronEnable) {
                            $gift->setData('status', 1);
                        }
                    }else{
                        if($cronDisable){
                            $gift->setData('status',0);
                        }
                    }
                    $gift->save();
                } catch (Exception $e) {
                    Mage::logException($e);

                    $this->_getSession()->addError($e->getMessage());
                }
            }
        }catch (Exception $e){
            Mage::throwException($e);
        }
    }

    /**
     * Retrieve adminhtml session model object
     *
     * @return Mage_Adminhtml_Model_Session
     */
    protected function _getSession()
    {
        return Mage::getSingleton('adminhtml/session');
    }
}