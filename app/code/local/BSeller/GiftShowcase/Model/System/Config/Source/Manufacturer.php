<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_GiftShowcase
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Guilherme Miguelete <guilherme.miguelete@e-smart.com.br>
 */

class BSeller_GiftShowcase_Model_System_Config_Source_Manufacturer
{
    use BSeller_Core_Trait_Data;

    /**
     * Return options in select format
     *
     * @return array
     */
    protected function getCollection()
    {
        return Mage::getResourceModel('zeon_manufacturer/manufacturer_collection')
            ->addFieldToFilter('status', true)
            ->setOrder('manufacturer_name', 'ASC');
    }

    /**
     * Return options in select format
     *
     * @return array
     */
    public function toOptionArray()
    {
        foreach ($this->toArray() as $value => $label) {
            $data[] = array(
                'value' => $value,
                'label' => $label
            );
        }

        return $data;
    }

    /**
     * Return options in key-value format
     *
     * @return array
     */
    public function toArray()
    {
        $data = [''];

        foreach ($this->getCollection() as $manufacturer) {
            $data[$manufacturer->getId()] = $this->_helper()->__($manufacturer->getManufacturerName());
        }

        return $data;
    }
}
