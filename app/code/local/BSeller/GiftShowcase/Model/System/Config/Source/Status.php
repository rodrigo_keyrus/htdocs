<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_GiftShowcase
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_GiftShowcase_Model_System_Config_Source_Status
{
    use BSeller_Core_Trait_Data;

    /**
     * Options in array
     *
     * @var array
     */
    protected $_options = [
        'Disabled',
        'Enabled'
    ];

    /**
     * Return options in select format
     *
     * @return array
     */
    public function toOptionArray()
    {
        $data[] = [];

        foreach ($this->toArray() as $value => $label) {
            $data[] = [
                'value' => $value,
                'label' => $label
            ];
        }

        return $data;
    }

    /**
     * Return options in key-value format
     *
     * @return array
     */
    public function toArray()
    {
        $data = [];

        foreach ($this->_options as $value => $label) {
            $data[$value] = $this->_helper()->__($label);
        }

        return $data;
    }
}
