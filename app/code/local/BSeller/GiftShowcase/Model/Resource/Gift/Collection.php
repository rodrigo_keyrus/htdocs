<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_GiftShowcase
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_GiftShowcase_Model_Resource_Gift_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    /**
     * Collection initialization
     */
    public function _construct()
    {
        $this->_init('bseller_giftshowcase/gift');
    }

    /**
     * Add status do filter collection
     *
     * @param true|bool $status
     * @return $this
     */
    public function addStatusFilter($status = true)
    {
        $this->addFieldToFilter('status', (bool) $status);

        return $this;
    }

    /**
     * Add all the conditional filters
     *
     * @param string $sku
     * @param int    $manufacturer
     * @param array  $categoryIds
     *
     * @return $this
     */
    public function addConditionalFilters($sku, $manufacturer, $categoryIds)
    {
        if (!$sku) {
            return $this;
        }

        $strCategory = '';
        if (is_array($categoryIds) && !empty($categoryIds)) {
            $strCategory = '(';
            $extra = '';
            foreach ($categoryIds as $category) {
                $strCategory .= $extra . "FIND_IN_SET('$category', category_view) OR FIND_IN_SET(' $category', category_view)";
                $extra = ' OR ';
            }
            $strCategory .= ')';
        }

        $this->getSelect()
            ->where("
                    (
                        (
                            (FIND_IN_SET('$sku', prd) OR FIND_IN_SET(' $sku', prd)) OR 
                            manufacturer_view = '$manufacturer'
                            " . ($strCategory ? " OR $strCategory" : '') . "
                        ) AND aggregator = 'any') OR 
                    (
                        (FIND_IN_SET('$sku', prd) OR FIND_IN_SET(' $sku', prd) OR prd = '') AND 
                        (manufacturer_view = '$manufacturer' OR manufacturer_view = '') AND 
                        " . ($strCategory ? "($strCategory  OR category_view = '') AND " : '') . "
                        aggregator = 'all'
                    )
                "
            );

        return $this;
    }

    /**
     * Add the product ID filter
     *
     * @param int $productId
     *
     * @return $this
     */
    public function addProductIdFilter($productId)
    {
        $resource = Mage::getSingleton('core/resource');

        $this->getSelect()->join(
            ['product' => $resource->getTableName('catalog/product')],
            'main_table.sku = product.sku',
            ['entity_id AS product_id']
        );

        $this->addFieldToFilter('product.entity_id', $productId);

        return $this;
    }

    /**
     * Add position to order collection
     *
     * @param string $dir
     * @return $this
     */
    public function addPositionOrder($dir = Varien_Db_Select::SQL_ASC)
    {
        $this->addOrder('position', $dir);

        return $this;
    }

    /**
     * Add filter rule_from_date and rule_to_date
     *
     * @return $this
     */
    public function addDateFilter()
    {
        $this->addFieldToFilter(
            'rule_from_date',
            array(
                array('lteq' => $this->getDate()),
                array('null' => true)
            )
        )
            ->addFieldToFilter(
                'rule_to_date',
                array(
                    array('gteq' => $this->getDate()),
                    array('null' => true)
                )
            );

        return $this;
    }

    public function addProductsToResult()
    {
        $resource = Mage::getSingleton('core/resource');

        $this->getSelect()->join(
            ['product' => $resource->getTableName('catalog/product')],
            'main_table.sku = product.sku',
            ['entity_id AS product_id']
        );

        $this->getSelect()->join(
            ['inve' => $resource->getTableName('cataloginventory/stock_status')],
            "(inve.product_id = product.entity_id)
                    AND (inve.website_id = " . Mage::app()->getStore()->getWebsiteId() . ")
                    AND (inve.stock_id = '1')
                    AND (inve.stock_status = '1')
                ",
            []
        );

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return Mage::getModel('core/date')->date('Y-m-d H:i:s');
    }
}
