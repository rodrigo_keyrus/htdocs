<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_GiftShowcase
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 * @author    Weverson Cachinsky <weverson.cachinsky@e-smart.com.br>
 */
class BSeller_GiftShowcase_Model_Image extends Varien_Object
{
    use BSeller_Core_Trait_Data;

    /**
     * File subdirectory
     *
     * @var string
     */
    protected $_fileSubdirectory = 'giftshowcase';

    /**
     * Allowed extensions
     *
     * @var array
     */
    protected $_allowedExtensions = [
        'jpg',
        'jpeg',
        'png'
    ];

    /**
     * Generate file in directory
     *
     * @return string
     * @throws Exception
     */
    public function upload()
    {
        $this->delete();

        $uploader = new Mage_Core_Model_File_Uploader('image');
        $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
        $uploader->addValidateCallback('catalog_product_image',
            Mage::helper('catalog/image'), 'validateUploadFile');
        $uploader->setAllowRenameFiles(true);
        $uploader->setFilesDispersion(true);
        $uploader->addValidateCallback(
            Mage_Core_Model_File_Validator_Image::NAME,
            Mage::getModel('core/file_validator_image'),
            'validate'
        );
        $result = $uploader->save(Mage::getBaseDir('media') . DS . $this->_fileSubdirectory);

        return $this->_fileSubdirectory . DS . $result['file'];
    }

    /**
     * Delete file in directory
     *
     * @param null|string $image
     * @return $this
     */
    public function delete($image = null)
    {
        if (is_null($image)) {
            $image = $this->getData('image');
        }

        if (isset($image['value'])) {
            $image = $image['value'];
        }

        /** @var Thai_S3_Model_Core_File_Storage_S3 $storageModel */
        $storageModel = Mage::getModel('thai_s3/core_file_storage_s3');
        $storageModel->deleteFile($image);

        return $this;
    }
}
