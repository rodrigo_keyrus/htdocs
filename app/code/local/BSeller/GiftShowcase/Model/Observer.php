<?php
/**
 * Created by PhpStorm.
 * User: danilo
 * Date: 22/12/17
 * Time: 11:13
 */

class BSeller_GiftShowcase_Model_Observer
{
    /**
     * @return Varien_Db_Adapter_Interface
     */
    private function getConnection()
    {
        /** @var Mage_Core_Model_Resource $resource */
        $resource = Mage::getSingleton('core/resource');
        return $resource->getConnection('default_write');
    }

    /**
     * Verify, parse and save the gift
     *
     * @param Varien_Event_Observer $observer
     * @return $this
     */
    public function saveGiftShowcase(Varien_Event_Observer $observer)
    {
        $rule               = $observer->getEvent()->getRule();
        $gift               = Mage::getModel('catalog/product')->loadByAttribute('sku', $rule['gift_sku']);
        $checkSkuDuplicated = $this->modelGiftShowcase()->getDuplicatedShowcase(
            $rule['rule_id'],
            $rule['gift_sku'],
            $rule['from_date'],
            $rule['to_date']
         );

        if ($checkSkuDuplicated) {
            Mage::throwException(Mage::helper('bseller_giftshowcase')
                ->__('Gift not created, there is already a gift rule registered in this period for the SKU:%s', $rule['gift_sku'])
            );
        }

        if (!$gift) {
            Mage::throwException(Mage::helper('bseller_giftshowcase')
                ->__('SKU is invalid or not found.')
            );
        }

        list($prds, $manufacturerView, $categoryView) = $this->_parseConditions($rule);

        $this->_registerGift($rule, $gift, $prds, $manufacturerView, $categoryView);

        return $this;
    }

    /**
     * Parses the rule's condition
     *
     * @param array $rule
     * @return array
     */
    private function _parseConditions($rule)
    {
        $prds               = '';
        $manufacturerView   = '';
        $categoryView       = '';

        foreach ($rule['conditions'] as $condition) {
            if ($condition['type'] == 'salesrule/rule_condition_product') {
                if ($condition['attribute'] == 'sku') {
                    $prds = $condition['value'];
                } elseif ($condition['attribute'] == 'manufacturer') {
                    if (!empty($manufacturerView) && !is_null($manufacturerView)) {
                        $manufacturerView .= ',' . $condition['value'];
                    } else {
                        $manufacturerView = $condition['value'];
                    }
                } elseif ($condition['attribute'] == 'category_ids') {
                    if (!empty($categoryView) && !is_null($categoryView)) {
                        $categoryView .= ',' . $condition['value'];
                    } else {
                        $categoryView = $condition['value'];
                    }
                }
            }
        }

        return [$prds, $manufacturerView, $categoryView];
    }

    /**
     * Saves the gift in the database
     *
     * @param array $rule
     * @param Mage_Catalog_Model_Product $gift
     * @param string $prds
     * @param string $manufacturerView
     * @param string $categoryView
     *
     * @throws Exception
     */
    private function _registerGift($rule, $gift, $prds, $manufacturerView, $categoryView)
    {
        $link           = $rule['link_gifts'];
        $idGiftShow     = $this->modelGiftShowcase()->getIdGiftShowcaseByRuleId($rule['rule_id']);
        $giftShowCase   = $this->modelGiftShowcase();
        $aggregator     = 'any';

        if (!is_null($idGiftShow)) {
            $giftShowCase = $this->modelGiftShowcase()->load($idGiftShow);
        }

        if (array_key_exists('conditions', $rule) && array_key_exists('1--1', $rule['conditions'])) {
            $aggregator = $rule['conditions']['1--1']['aggregator'];
        }

        $giftShowCase->setManufacturerId($gift->getManufacturer())
            ->setPrd($prds)
            ->setAggregator($aggregator)
            ->setManufacturerView($manufacturerView)
            ->setCategoryView($categoryView)
            ->setTitle($rule['name'])
            ->setSku($rule['gift_sku'])
            ->setDescription($rule['description'])
            ->setImage(Mage::getBaseUrl('media'))
            ->setLink($link)
            ->setStatus($rule['is_active'])
            ->setRuleFromDate($rule['from_date'])
            ->setRuleToDate($rule['to_date'])
            ->setRuleId($rule['rule_id']);

        $giftShowCase->save();
    }

    /**
     * @param Varien_Event_Observer $observer
     *
     * @return $this
     */
    public function deleteGiftShowcase(Varien_Event_Observer $observer)
    {
        $rule  = $observer->getEvent()->getRule();
        $idGiftShow   = $this->modelGiftShowcase()->getIdGiftShowcaseByRuleId($rule['rule_id']);

        if (!is_null($idGiftShow)) {
            try {
                $giftShowCase = $this->modelGiftShowcase();
                $giftShowCase->load($idGiftShow);
                $giftShowCase->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('salesrule')->__('The GiftShowcase has been deleted.'));
            } catch (Exception $e) {
                Mage::throwException($e);
            }
        }

        return $this;
    }

    /**
     * Inactivate the rule and the giftshowcase, if the gift is sold out
     *
     * @param Varien_Event_Observer $observer
     */
    public function checkStock(Varien_Event_Observer $observer)
    {
        /** @var Mage_CatalogInventory_Model_Stock_Item $stock */
        $stock = $observer->getEvent()->getItem();

        if ($stock->getData('is_in_stock') || ($stock->getOrigData('is_in_stock') == $stock->getData('is_in_stock'))) {
            return;
        }

        try {
            /** @var BSeller_GiftShowcase_Model_Resource_Gift_Collection $collection */
            $gifts = Mage::getResourceModel('bseller_giftshowcase/gift_collection')
                ->addStatusFilter()
                ->addDateFilter()
                ->addProductIdFilter($stock->getProductId());

            if (!count($gifts)) {
                return;
            }

            $this->getConnection()->beginTransaction();

            foreach ($gifts as $gift) {

                $gift->setStatus(0);
                $gift->save();

                $rule = Mage::getModel('salesrule/rule')->load($gift->getRuleId());
                $rule->setIsActive(0);
                $rule->save();
            }

            $this->getConnection()->commit();
        } catch (Exception $e) {
            Mage::log("Error on inactivate rule: " . $e->getMessage());
        }
    }

    /**
     * @return false|Mage_Core_Model_Abstract
     */
    public function modelGiftShowcase()
    {
        return Mage::getModel('bseller_giftshowcase/gift');
    }
}