<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_GiftShowcase
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_GiftShowcase_Model_Gift extends BSeller_Core_Model_Abstract
{
    use BSeller_Core_Trait_Data;

    /**
     * Resource entity
     *
     * @const string
     */
    const ENTITY = 'gift';

    /**
     * Status active
     *
     * @const string
     */
    const STATUS_ACTIVE = '1';

    /**
     * Image's URL
     *
     * @var string|bool
     */
    private $_imageURL = false;

    /**
     * Constructor initialization
     */
    public function _construct()
    {
        $this->_init('bseller_giftshowcase/gift');

        parent::_construct();
    }

    /**
     * Return image URL
     *
     * @return string
     */
    public function getImageUrl()
    {
        if ($this->_imageURL) {
            return $this->_imageURL;
        }

        $model = Mage::getResourceModel('catalog/product');
        $image = $model->getAttributeRawValue(
            $this->getProductId(),
            'image',
            Mage::app()->getStore()->getStoreId()
        );

        $product = Mage::getModel('catalog/product');

        $this->_imageURL = (string) Mage::helper('catalog/image')->init($product, 'image', $image)->resize(210);

        return $this->_imageURL;
    }

    /**
     * Return image factory object
     *
     * @return BSeller_GiftShowcase_Model_Image
     */
    public function getImageFactory()
    {
        $image = Mage::getModel('bseller_giftshowcase/image');
        $image->addData($this->getData());

        return $image;
    }

    /**
     * Processing object before save data
     *
     * @return Mage_Core_Model_Abstract
     */
    public function _beforeSave()
    {

        $data = $this->getRequest()->getPost();
        if (!empty($data)) {
            $object = Mage::helper('bseller_giftshowcase')->getObjectInfo();
            $object->addData($data);
            Mage::getSingleton('adminhtml/session')->setFormData($data);
        }

        if (!Zend_Validate::is($this->getData('image'), 'NotEmpty')) {
            Mage::throwException($this->_helper()->__('Image is a required field.'));
        }

        if (!Zend_Validate::is($this->getData('sku'), 'NotEmpty')) {
            Mage::throwException($this->_helper()->__('SKU is a required field.'));
        }

        $this->productAvailability($this->getData('sku'));

        return parent::_beforeSave();
    }

    public function _afterSave()
    {
        return parent::_afterSave();

        try {
            if(Mage::getStoreConfig('bseller_giftshowcase/sku/clear_magento_cache')){
                //clear magento cache
                $type = 'block_html';
                $tags = Mage::app()->getCacheInstance()->cleanType($type);
                Mage::dispatchEvent('adminhtml_cache_refresh_type', array('type' => $type));
            }

            if(Mage::getStoreConfig('bseller_giftshowcase/sku/clear_varnish_cache')) {
                $allStores = Mage::app()->getStores();
                foreach ($allStores as $_eachStoreId => $val) {
                    $this->_clearVarnishUrl(Mage::app()->getStore($_eachStoreId)->getId());
                }
            }

        }catch (Exception $e) {
            $this->_getSession()->addException(
                $e,
                Mage::helper('varnishcache')->__('An error occurred while clearing the Varnish cache.')
            );
        }
    }

    private function _clearVarnishUrl($storeId){
        try {
            //clear varnish url
            if (Mage::helper('varnishcache')->isEnabled()) {

                $url = Mage::getUrl('/brindes', array('_store' => $storeId));

                $domainList = Mage::helper('varnishcache/cache')->getStoreDomainList();
                extract(parse_url($url));
                if (!isset($host)) {
                    throw new Mage_Core_Exception(Mage::helper('varnishcache')->__('Invalid URL "%s".', $url));
                }
                if (!in_array($host, explode('|', $domainList))) {
                    throw new Mage_Core_Exception(Mage::helper('varnishcache')->__('Invalid domain "%s".', $host));
                }

                $uri = '';
                if (isset($path)) {
                    $uri .= $path;
                }
                if (isset($query)) {
                    $uri .= '\?';
                    $uri .= $query;
                }
                if (isset($fragment)) {
                    $uri .= '#';
                    $uri .= $fragment;
                }

                Mage::getModel('varnishcache/control')
                    ->clean($host, sprintf('^%s$', $uri));

                $this->_getSession()->addSuccess(
                    Mage::helper('varnishcache')->__('The URL\'s "%s" cache has been cleaned.', $url)
                );
            }
        }
        catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        }
        catch (Exception $e) {
            $this->_getSession()->addException(
                $e,
                Mage::helper('varnishcache')->__('An error occurred while clearing the Varnish cache.')
            );
        }
    }


    /**
     * validation produt on save gift showcase
     * @param null $sku
     * @return bool
     */
    public function productAvailability($sku = null)
    {

        if (!Mage::getStoreConfig('bseller_giftshowcase/sku/check_sku')) {
            return true;
        }

        if (!$sku) {
            Mage::throwException($this->_helper()->__('SKU is invalid on form submit.'));
        }

        $taxTreatment  = Mage::getStoreConfig('bseller_giftshowcase/sku/tax_treatment');

        $collection = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('status')
            ->addAttributeToFilter('sku', array('eq' => $sku))
            ->addAttributeToFilter('tax_treatment', ['eq' => $taxTreatment]);
        $item = $collection->fetchItem();

        if (!($item instanceof Mage_Catalog_Model_Product) || ($item->getData('sku') != $sku)) {
            Mage::throwException($this->_helper()->__('SKU is invalid or not found.'));
        }

        $product = Mage::getModel('catalog/product')->load($item->getData('entity_id'));

        $skuType = Mage::getStoreConfig('bseller_giftshowcase/sku/sku_type');
        if ($product->getData('type_id') != $skuType) {
            Mage::throwException($this->_helper()->__('Accepted only sku type: '.$skuType));
        }

        if (Mage::getStoreConfig('bseller_giftshowcase/sku/check_status')
            && $product->getData('status') != '1') {
            Mage::log('status negativo');
            return false;
        }

        $stock = $product->getData('stock_item');
        if ($stock->getData('manage_stock')) {
            if ($stock->getData('qty') < 1 || $stock->getData('is_in_stock') =='0') {
                return false;
            }
        }

        return true;
    }

    protected function _getSession()
    {
        return Mage::getSingleton('adminhtml/session');
    }

    /**
     * @param $ruleId
     *
     * @return mixed
     */
    public function getIdGiftShowcaseByRuleId($ruleId = null)
    {
        if (!$ruleId) {
            return false;
        }

        $idGiftShowcase = null;

        /** @var BSeller_GiftShowcase_Model_Resource_Gift_Collection $collection */
        $collection = Mage::getResourceModel('bseller_giftshowcase/gift_collection');
        $collection->addFieldToSelect('*');
        $collection->addFieldToFilter('rule_id', $ruleId);

        foreach ($collection->getItems() as $item) {
            if ($item->getRuleId() == $ruleId) {
                $idGiftShowcase = $item->getId();
            }
        }

        return $idGiftShowcase;
    }

    /**
     * Verifies if there is another gift's rule in analogous condition
     *
     * @param $ruleId   Rule ID
     * @param $sku      Product's SKU
     * @param $fromDate Initial date of the gift's rule
     * @param $toDate   Final date of the gift's rule
     *
     * @return bool
     */
    public function getDuplicatedShowcase($ruleId, $sku, $fromDate, $toDate)
    {
        /** @var BSeller_GiftShowcase_Model_Resource_Gift_Collection $collection */
        $collection = Mage::getResourceModel('bseller_giftshowcase/gift_collection')
            ->addFieldToFilter('sku', $sku)
            ->addFieldToFilter('status', self::STATUS_ACTIVE)
            ->addFieldToFilter('rule_id', ['neq' => $ruleId]);

        foreach ($collection->getItems() as $item) {

            if (($fromDate >= $item->getRuleFromDate()) && ($fromDate <= $item->getRuleToDate())) {
                return true;
            }

            if (($toDate <= $item->getRuleToDate()) && ($toDate >= $item->getRuleFromDate())) {
                return true;
            }

            if (($fromDate <= $item->getRuleFromDate() && $fromDate <= $item->getRuleToDate()) && ($toDate >= $item->getRuleToDate())) {
                return true;
            }
        }

        return false;
    }
}