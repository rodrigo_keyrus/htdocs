<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_GiftShowcase
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_GiftShowcase_Block_Adminhtml_Giftshowcase_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Prepare form before rendering HTML
     *
     * @return Mage_Adminhtml_Block_Widget_Form
     * @throws Exception
     */
    protected function _prepareForm($fieldSet = null)
    {
        $form = new Varien_Data_Form();

        /** @var BSeller_GiftShowcase_Model_Gift $object */
        $object = $this->helper('bseller_giftshowcase')->getObjectInfo();

        $fieldSet = $form->addFieldset('main_field_set', ['legend' => $this->__('General')]);

        if ($object->getId()) {
            $fieldSet->addField(
                'entity_id',
                'hidden',
                [
                    'name'     => 'id',
                    'required' => true
                ]
            );
        }

        $fieldSet->addField(
            'manufacturer_id',
            'select',
            [
                'name'     => 'manufacturer_id',
                'label'    => $this->__('Manufacturer'),
                'options'  => Mage::getSingleton('bseller_giftshowcase/system_config_source_manufacturer')->toArray(),
                'required' => true
            ]
        );

        $fieldSet->addField(
            'title',
            'text',
            [
                'name'     => 'title',
                'label'    => $this->__('Title'),
                'required' => true
            ]
        );

        $fieldSet->addField(
            'description',
            'textarea',
            [
                'name'     => 'description',
                'label'    => $this->__('Description'),
                'required' => true
            ]
        );

        $fieldSet->addField(
            'sku',
            'text',
            [
                'name'     => 'sku',
                'label'    => $this->__('SKU'),
                'required' => true,
                'note'     => 'Use only simple product'
            ]
        );

        $fieldSet->addField(
            'position',
            'text',
            [
                'name'     => 'position',
                'label'    => $this->__('Position'),
                'required' => false
            ]
        );

        $fieldSet->addField(
            'status',
            'select',
            [
                'name'     => 'status',
                'label'    => $this->__('Status'),
                'options'  => Mage::getSingleton('bseller_giftshowcase/system_config_source_status')->toArray(),
                'required' => true
            ]
        );

        $dateFormatIso = Mage::app()->getLocale()->getDateTimeFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
        $fieldSet->addField(
            'rule_from_date',
            'date',
            array(
                'name'   => 'rule_from_date',
                'label'  => Mage::helper('salesrule')->__('From Date'),
                'title'  => Mage::helper('salesrule')->__('From Date'),
                'image'  => $this->getSkinUrl('images/grid-cal.gif'),
                'input_format' => $dateFormatIso,
                'format'       => $dateFormatIso,
                'time' => true
            )
        );

        $fieldSet->addField(
            'rule_to_date',
            'date',
            array(
                'name'   => 'rule_to_date',
                'label'  => Mage::helper('salesrule')->__('To Date'),
                'title'  => Mage::helper('salesrule')->__('To Date'),
                'image'  => $this->getSkinUrl('images/grid-cal.gif'),
                'input_format' => $dateFormatIso,
                'format'       => $dateFormatIso,
                'time' => true
            )
        );

        $fieldSet = $form->addFieldset('agreements_field_set', ['legend' => $this->__('Regulation')]);

        $fieldSet->addField(
            'agreements',
            'textarea',
            [
                'name'     => 'agreements',
                'label'    => $this->__('Message'),
                'required' => false
            ]
        );

        $fieldSet = $form->addFieldset('image_field_set', ['legend' => $this->__('Image')]);

        $this->_addElementTypes($fieldSet);

        $fieldSet->addField(
            'image',
            'image',
            [
                'name'     => 'image',
                'label'    => $this->__('Upload'),
                'note'     => '(*.jpg, *.jpeg, *.png)',
                'required' => true
            ]
        );

        $fieldSet->addField(
            'link',
            'text',
            [
                'name'     => 'link',
                'label'    => $this->__('Link'),
                'required' => true
            ]
        );


        $fieldSet = $form->addFieldset('view_field_set', ['legend' => $this->__('View in')]);

        $fieldSet->addField(
            'prd',
            'text',
            [
                'name'     => 'prd',
                'label'    => $this->__('Product (PRD)')
            ]
        );

        $fieldSet->addField(
            'manufacturer_view',
            'multiselect',
            [
                'name'     => 'manufacturer_view',
                'label'    => $this->__('Manufacturer'),
                'values'  => Mage::getSingleton(
                    'bseller_giftshowcase/system_config_source_manufacturer'
                )->toOptionArray()
            ]
        );

        if (!$object->getId()) {
            $object->addData(
                [
                    'position' => 0,
                    'status'   => true
                ]
            );
        }


        if ($manufacturerView = $object->getData('manufacturer_view')) {
            $object->setData('manufacturer_view', explode(',', $manufacturerView));
        }

        $form->setValues($object->getData());
        $form->setData('action', $this->getUrl('*/*/save'));
        $form->setData('enctype', 'multipart/form-data');
        $form->setData('method', 'post');
        $form->setData('use_container', true);
        $form->setId('edit_form');

        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Add additional element types
     *
     * @return array
     */
    protected function _getAdditionalElementTypes()
    {
        return ['image' => 'BSeller_GiftShowcase_Block_Adminhtml_Giftshowcase_Helper_Form_Image'];
    }
}
