<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_GiftShowcase
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_GiftShowcase_Block_Adminhtml_Giftshowcase_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Initialize constructor
     */
    public function _construct()
    {
        parent::_construct();

        $this->setId('bseller_gift_showcase_grid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setData('use_ajax', true);
    }

    /**
     * Prepare grid collection
     *
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('bseller_giftshowcase/gift_collection');

        $this->setCollection($collection);
        
        parent::_prepareCollection();

        return $this;
    }

    /**
     * Prepare grid columns
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     * @throws Exception
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'entity_id',
            [
                'header' => $this->__('ID'),
                'align'  => 'left',
                'index'  => 'entity_id',
                'type'   => 'number'
            ]
        );

        $this->addColumn(
            'manufacturer_id',
            [
                'header'  => $this->__('Manufacturer'),
                'index'   => 'manufacturer_id',
                'type'    => 'options',
                'options' => Mage::getSingleton('bseller_giftshowcase/system_config_source_manufacturer')->toArray()
            ]
        );

        $this->addColumn(
            'title',
            [
                'header' => $this->__('Title'),
                'align'  => 'left',
                'index'  => 'title',
                'type'   => 'text'
            ]
        );

        $this->addColumn(
            'sku',
            [
                'header' => $this->__('sku'),
                'align'  => 'left',
                'index'  => 'sku',
                'type'   => 'text'
            ]
        );

        $this->addColumn(
            'updated_at',
            [
                'header'   => $this->__('Updated At'),
                'index'    => 'updated_at',
                'type'     => 'datetime',
                'renderer' => 'bseller_giftshowcase/adminhtml_widget_grid_column_renderer_datetime'
            ]
        );

        $this->addColumn(
            'created_at',
            [
                'header'   => $this->__('Created At'),
                'index'    => 'created_at',
                'type'     => 'datetime',
                'renderer' => 'bseller_giftshowcase/adminhtml_widget_grid_column_renderer_datetime'
            ]
        );

        $this->addColumn(
            'status',
            [
                'header'  => $this->__('Status'),
                'align'   => 'left',
                'index'   => 'status',
                'type'    => 'options',
                'options' => Mage::getSingleton('bseller_giftshowcase/system_config_source_status')->toArray()
            ]
        );

        $this->addColumn(
            'position',
            [
                'header' => $this->__('Position'),
                'align'  => 'left',
                'index'  => 'position',
                'type'   => 'number'
            ]
        );

        $this->addExportType('*/*/exportCsv', $this->__('CSV'));
        $this->addExportType('*/*/exportExcel', $this->__('Excel XML'));

        return parent::_prepareColumns();
    }

    /**
     * Prepare grid massaction block
     *
     * @return $this
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');

        $this->getMassactionBlock()->setData('form_field_name', 'entity_ids');
        $this->getMassactionBlock()->setUseSelectAll(false);

        $this->getMassactionBlock()->addItem(
            'availability_update',
            [
                'label' => $this->__('Check sku availability (update status)'),
                'url'   => $this->getUrl('*/*/massAvailabilityUpdate'),
            ]
        );

        $this->getMassactionBlock()->addItem(
            'delete',
            [
                'label' => $this->__('Delete'),
                'url'   => $this->getUrl('*/*/massDelete'),
            ]
        );

        return $this;
    }

    /**
     * Return grid url
     *
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', ['_current' => true]);
    }

    /**
     * Return row url
     *
     * @param Varien_Object $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', ['id' => $row->getId()]);
    }
}
