<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_GiftShowcase
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Guilherme Miguelete <guilherme.miguelete@e-smart.com.br>
 */

class BSeller_GiftShowcase_Block_Banner extends BSeller_GiftShowcase_Block_Abstract
{
    /**
     * Cache config values
     *
     * @var string
     */
    protected $_cacheKey = 'bseller_gift_banner';
    protected $_cacheTag = 'bseller_gift_banner';

    /**
     * Return page title
     *
     * @return string
     */
    public function getTitle()
    {
        /** @var Mage_Page_Block_Html_Head $head */
        $head = $this->getLayout()->getBlock('head');

        return $head->getTitle();
    }

    /**
     * Return if banner info exists
     *
     * @param string $name
     * @return bool
     */
    public function hasInfo($name)
    {
        return (Zend_Validate::is(Mage::getStoreConfig('bseller_giftshowcase/settings/' . $name), 'NotEmpty'));
    }

    /**
     * Return banner url
     *
     * @return string
     */
    public function getImageUrl()
    {
        return Mage::getBaseUrl('media')
            . 'giftshowcase/'
            . Mage::getStoreConfig('bseller_giftshowcase/settings/banner');
    }

    /**
     * Return banner description
     *
     * @return string
     */
    public function getDescription()
    {
        return Mage::getStoreConfig('bseller_giftshowcase/settings/banner_description');
    }

    /**
     * Return manufacturer identifier
     *
     * @return string
     */
    public function getIdentifier()
    {
        return self::DEFAULT_IDENTIFIER;
    }
}
