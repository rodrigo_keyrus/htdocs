<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_GiftShowcase
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Guilherme Miguelete <guilherme.miguelete@e-smart.com.br>
 */

abstract class BSeller_GiftShowcase_Block_Abstract extends BSeller_Core_Block_Template
{
    /**
     * Default identifier name
     *
     * @const string
     */
    const DEFAULT_IDENTIFIER = 'no_key';

    /**
     * Cache config values
     *
     * @var string
     */
    protected $_cacheKey      = 'bseller_gift';
    protected $_cacheTag      = 'bseller_gift';
    protected $_cacheLifetime = '86400';

    /**
     * Internal constructor, for add block cache
     */
    public function _construct()
    {
        parent::_construct();

        $this->addData(
            [
                'cache_tags'     => [$this->_cacheTag . '_' . $this->getIdentifier()],
                'cache_key'      => $this->_cacheKey . '_' . $this->getIdentifier(),
                'cache_lifetime' => $this->_cacheLifetime
            ]
        );
    }

    /**
     * Return manufacturer identifier
     *
     * @return string
     */
    public function getIdentifier()
    {
        $identifier = $this->getRequest()->getParam('manufacturer');

        if (empty($identifier)) {
            return self::DEFAULT_IDENTIFIER;
        }

        return trim(preg_replace('<\W+>', '_', $identifier));
    }

    /**
     * Return manufacturer ID by identifier
     *
     * @param string $identifier
     * @return int
     */
    public function getManufacturerIdByIdentifier($identifier)
    {
        $storeId = Mage::app()->getStore()->getId();

        /** @var Esmart_Manufacturer_Model_Manufacturer $manufacturer */
        $manufacturer   = Mage::getModel('esmart_manufacturer/manufacturer');
        $manufacturerId = $manufacturer->checkIdentifier($identifier, $storeId);

        return $manufacturerId;
    }

    /**
     * Return manufacturer name by identifier
     *
     * @param string $identifier
     * @return string
     */
    public function getManufacturerName($identifier)
    {
        /** @var Esmart_Manufacturer_Model_Manufacturer $manufacturer */
        $manufacturer = Mage::getModel('esmart_manufacturer/manufacturer')->load(
            $this->getManufacturerIdByIdentifier($identifier)
        );

        return $manufacturer->getManufacturerName();
    }

    /**
     * Render block HTML
     *
     * @return string
     */
    protected function _toHtml()
    {
        if (!$this->_helper()->isModuleEnabled()) {
            return '';
        }

        return parent::_toHtml();
    }
}
