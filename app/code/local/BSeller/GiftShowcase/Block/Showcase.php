<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_GiftShowcase
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Guilherme Miguelete <guilherme.miguelete@e-smart.com.br>
 */

class BSeller_GiftShowcase_Block_Showcase extends BSeller_GiftShowcase_Block_Abstract
{
    /**
    /**
     * Cache config values
     *
     * @var string
     */
    protected $_cacheKey = 'bseller_gift_showcase';
    protected $_cacheTag = 'bseller_gift_showcase';

    /**
     * Singleton key
     *
     * @var string
     */
    protected $_singletonKey = 'bseller_gift_showcase_collection';

    /**
     * Return available gift collection
     *
     * @return BSeller_GiftShowcase_Model_Resource_Gift_Collection
     */
    public function getCollection()
    {
        if ($this->hasData($this->_singletonKey)) {
            return $this->getData($this->_singletonKey);
        }

        /** @var BSeller_GiftShowcase_Model_Resource_Gift_Collection $collection */
        $collection = Mage::getResourceModel('bseller_giftshowcase/gift_collection')
            ->addFieldToSelect('*')
            ->addStatusFilter()
            ->addDateFilter()
            ->addProductsToResult()
            ->addPositionOrder();

        $manufacturerId = $this->getManufacturerIdByIdentifier($this->getRequest()->getParam('manufacturer'));

        if ($manufacturerId) {
            $collection->addFieldToFilter('manufacturer_id', $manufacturerId);
        }

        $this->setData($this->_singletonKey, $collection);

        return $this->getData($this->_singletonKey);
    }
}
