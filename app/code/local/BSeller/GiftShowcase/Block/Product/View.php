<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_GiftShowcase
 *
 * @copyright Copyright (c) 2017 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Guilherme Miguelete <guilherme.miguelete@e-smart.com.br>
 */

class BSeller_GiftShowcase_Block_Product_View extends Mage_Catalog_Block_Product_View
{

    /**
    /**
     * Cache config values
     *
     * @var string
     */
    protected $_cacheKey = 'bseller_gift_showcase_view';
    protected $_cacheTag = 'bseller_gift_showcase_view';

    /**
     * Singleton key
     *
     * @var string
     */
    protected $_singletonKey = 'bseller_gift_showcase_view';

    /**
     * Return available gift available on product page
     *
     * @return BSeller_GiftShowcase_Model_Resource_Gift_Collection
     */
    public function getGiftProduct()
    {
        /** @var Mage_Catalog_Model_Product $product */
        $product = $this->getProduct();

        if ($this->hasData($this->_singletonKey)) {
            return $this->getData($this->_singletonKey);
        }

        /** @var BSeller_GiftShowcase_Model_Resource_Gift_Collection $collection */
        $collection = Mage::getResourceModel('bseller_giftshowcase/gift_collection')
            ->addStatusFilter()
            ->addDateFilter()
            ->addConditionalFilters(
                $product->getSku(),
                $product->getManufacturer(),
                $product->getCategoryIds()
            )
            ->addPositionOrder()
            ->addProductsToResult();

        $this->setData($this->_singletonKey, $collection);
        return $this->getData($this->_singletonKey);

    }

    /**
     * @param null $from_date
     * @param null $to_date
     * @return bool
     */
    public function checkFromDateToDate($from_date = null, $to_date = null)
    {
        $return = false;

        if (!is_null($from_date) && !is_null($from_date)) {
            if (strtotime($from_date) <= strtotime(date('Y-m-d H:i:s')) && strtotime($to_date) >= strtotime(date('Y-m-d H:i:s'))) {
                $return = true;
            }
        }

        if (!is_null($from_date) && is_null($to_date)) {
            if (strtotime($from_date) <= strtotime(date('Y-m-d H:i:s'))) {
                $return = true;
            }
        }

        if (is_null($from_date) && !is_null($to_date)) {
            if (strtotime($to_date) >= strtotime(date('Y-m-d H:i:s'))) {
                $return = true;
            }
        }

        if (is_null($from_date) && is_null($to_date)) {
            $return = true;
        }

        return $return;
    }

    /**
     * @param $gift
     *
     * @return string
     */
    public function getImageGift($gift)
    {
        $image = $gift->getImageUrl();

        if (
            !stripos($gift->getImageUrl(),'.png')
            && !stripos($gift->getImageUrl(),'.jpg')
            && !stripos($gift->getImageUrl(),'.gif')
        ) {
            $image = Mage::helper('bseller_giftshowcase')->getImageProductBySku($gift->getSku());
        }

        return $image;
    }
}
