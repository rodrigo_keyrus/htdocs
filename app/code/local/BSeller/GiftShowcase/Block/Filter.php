<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_GiftShowcase
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Guilherme Miguelete <guilherme.miguelete@e-smart.com.br>
 */

class BSeller_GiftShowcase_Block_Filter extends BSeller_GiftShowcase_Block_Abstract
{
    /**
     * Cache config values
     *
     * @var string
     */
    protected $_cacheKey = 'bseller_gift_filter';
    protected $_cacheTag = 'bseller_gift_filter';

    /**
     * Singleton key
     *
     * @var string
     */
    protected $_singletonKey = 'bseller_gift_filter_collection';

    /**
     * Return available gift collection
     *
     * @return BSeller_GiftShowcase_Model_Resource_Gift_Collection
     */
    public function getCollection()
    {
        $resource = Mage::getSingleton('core/resource');

        if ($this->hasData($this->_singletonKey)) {
            return $this->getData($this->_singletonKey);
        }

        $collection = Mage::getResourceModel('zeon_manufacturer/manufacturer_collection')
            ->addFieldToFilter('status', true)
            ->setOrder('manufacturer_name', 'ASC');

        $collection->getSelect()->join(
            ['gift' => $resource->getTableName('bseller_giftshowcase/gift')],
            'main_table.manufacturer_id = gift.manufacturer_id',
            []
        );

        $collection->getSelect()->join(
            ['product' => $resource->getTableName('catalog/product')],
            'gift.sku = product.sku',
            []
        );

        $collection->getSelect()->join(
            ['inve' => $resource->getTableName('cataloginventory/stock_status')],
            "(inve.product_id = product.entity_id)
                    AND (inve.website_id = " . Mage::app()->getStore()->getWebsiteId() . ")
                    AND (inve.stock_id = '1')
                    AND (inve.stock_status = '1')
                ",
            []
        );

        $collection->getSelect()
            ->where('(gift.status = 1)')
            ->where('(((gift.rule_from_date <= ?) OR (gift.rule_from_date IS NULL)))', $this->getGmtDate())
            ->where('(((gift.rule_to_date >= ?) OR (gift . rule_to_date IS null)))', $this->getGmtDate());

        $collection->getSelect()->group('main_table.manufacturer_id');
        $collection->getSelect()->columns(new Zend_Db_Expr('count(main_table.manufacturer_id) as total'));

        $this->setData($this->_singletonKey, $collection);

        return $this->getData($this->_singletonKey);
    }

    /**
     * Return gmtDate
     * @return string
     */
    public function getGmtDate()
    {
        return Mage::getModel('core/date')->gmtDate();
    }
}
