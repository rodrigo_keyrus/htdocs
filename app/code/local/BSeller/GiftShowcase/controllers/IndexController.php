<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_GiftShowcase
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Guilherme Miguelete <guilherme.miguelete@e-smart.com.br>
 */

class BSeller_GiftShowcase_IndexController extends Mage_Core_Controller_Front_Action
{
    use BSeller_Core_Trait_Data;

    /**
     * Action pre dispatch
     * Should set layout area
     *
     * @return $this
     */
    public function preDispatch()
    {
        parent::preDispatch();

        if (!$this->_helper()->isModuleEnabled()) {
            $this->_forward('defaultNoRoute');
        }

        return $this;
    }

    /**
     * Render index page
     *
     * @return void
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }
}
