<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_GiftShowcase
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 * @author    Guilherme Miguelete <guilherme.miguelete@e-smart.com.br>
 */

/**
 * Class BSeller_GiftShowcase_Adminhtml_GiftshowcaseController
 *
 * @method BSeller_GiftShowcase_Helper_Data _helper()
 */
class BSeller_GiftShowcase_Adminhtml_GiftshowcaseController extends Mage_Adminhtml_Controller_Action
{
    use BSeller_Core_Trait_Data;
    
    /**
     * Render index page
     *
     * @return void
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Render grid block
     *
     * @return void
     */
    public function gridAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Render form new
     *
     * @return void
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     * Render form edit
     *
     * @return $this
     */
    public function editAction()
    {
        $id = $this->getRequest()->getParam('id');
        /** @var BSeller_GiftShowcase_Model_Gift $object */
        $model = Mage::getModel('bseller_giftshowcase/gift')->load($id);
        $this->_helper()->addObjectInfo($model);

        if (!$model->getId() && $id) {
            Mage::helper('bseller_giftshowcase')->__('This gift no longer exists.');
            $this->_redirect('*/*/');
            return;
        }

        $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
        if (!empty($data)) {
            $model->addData($data);
        }

        $this->loadLayout();
        $this->renderLayout();

        return $this;
    }

    /**
     * Save post data
     *
     * @return $this
     */
    public function saveAction()
    {
        if (!$this->getRequest()->isPost()) {
            $this->_redirect('*/*');

            return $this;
        }

        if (!$this->_validateFormKey()) {
            $this->_redirectReferer();

            return $this;
        }

        /** @var BSeller_GiftShowcase_Model_Gift $object */
        $object = Mage::getModel('bseller_giftshowcase/gift');

        $back = $this->getRequest()->getParam('back');
        $post = $this->getRequest()->getPost();
        $id   = $this->getRequest()->getPost('id');

        if ($id) {
            $object->load($id);
        }

        $post['manufacturer_view'] = (isset($post['manufacturer_view'])) ? implode(',', $post['manufacturer_view']) : '';

        //Convert Date
        if ($post['rule_from_date']) {
            $post['rule_from_date'] = date(
                'Y-m-d H:i:s',
                Mage::getModel('core/date')->gmtTimestamp(
                    strtotime(str_replace('/','-', $post['rule_from_date']))
                )
            );
        }

        if ($post['rule_to_date']) {
            $post['rule_to_date'] = date(
                'Y-m-d H:i:s',
                Mage::getModel('core/date')->gmtTimestamp(
                    strtotime(str_replace('/','-', $post['rule_to_date']))
                )
            );
        }

        $object->addData($post);

        if ($image = $object->getData('image/value')) {
            $object->setData('image', $image);
        }

        try {
            if ($this->_helper()->hasImageToDelete()) {
                $object->getImageFactory()->delete();

                $object->setData('image', '');
            }

            if ($this->_helper()->hasImageToUpload()) {
                $image = $object->getImageFactory()->upload();

                $object->setData('image', $image);
            }

            if ($object->getRuleId()) {
                $rule = Mage::getModel('salesrule/rule')->load($object->getRuleId());
                $object->setRuleToDate($rule->getToDate());
                $object->setRuleFromDate($rule->getFromDate());
            }

            $object->save();

            $id = $object->getId();

            $this->_getSession()->addSuccess($this->__('Successfully saved.'));
        } catch (Exception $e) {
            Mage::logException($e);

            $back = true;

            $this->_getSession()->addError($e->getMessage());
        }

        $this->_redirect('*/*/' . ($back ? 'edit' : 'index'), ($back ? ['id' => $id] : []));

        return $this;
    }

    /**
     * Delete row by id
     *
     * @return $this
     */
    public function deleteAction()
    {
        $id = $this->getRequest()->getParam('id');

        if (!$id) {
            $this->_redirect('*/*');

            return $this;
        }

        /** @var BSeller_GiftShowcase_Model_Gift $object */
        $object = Mage::getModel('bseller_giftshowcase/gift')->load($id);

        if (!$object->getId()) {
            $this->_redirect('*/*');

            return $this;
        }

        try {
            $object->getImageFactory()->delete();
            $object->delete();

            $this->_getSession()->addSuccess($this->__('Successfully deleted.'));
        } catch (Exception $e) {
            Mage::logException($e);

            $this->_getSession()->addError($e->getMessage());
        }

        $this->_redirect('*/*');

        return $this;
    }

    /**
     * Mass delete by ids
     *
     * @return $this
     */
    public function massDeleteAction()
    {
        $ids = (array) $this->getRequest()->getParam('entity_ids');

        foreach ($ids as $id) {
            /** @var BSeller_GiftShowcase_Model_Gift $object */
            $object = Mage::getModel('bseller_giftshowcase/gift')->load($id);

            if (!$object->getId()) {
                continue;
            }

            try {
                $object->getImageFactory()->delete();
                $object->delete();
            } catch (Exception $e) {
                Mage::logException($e);

                $this->_getSession()->addError($e->getMessage());
            }
        }

        $this->_getSession()->addSuccess($this->__('Successfully deleted.'));

        $this->_redirect('*/*');

        return $this;
    }


    /**
     * Mass massStatusUpdate by ids
     *
     * @return $this
     */
    public function massAvailabilityUpdateAction()
    {
        $ids = (array) $this->getRequest()->getParam('entity_ids');

        foreach ($ids as $id) {
            /** @var BSeller_GiftShowcase_Model_Gift $object */
            $object = Mage::getModel('bseller_giftshowcase/gift')->load($id);

            if (!$object->getId()) {
                continue;
            }

            try {
                /* will check sku availability*/
                if(Mage::getModel('bseller_giftshowcase/gift')->productAvailability($object->getData('sku'))){
                    $object->setData('status',1);
                }else{
                    $object->setData('status',0);
                }
                $object->save();
            } catch (Exception $e) {
                Mage::logException($e);

                $this->_getSession()->addError($e->getMessage());
            }
        }

        $this->_getSession()->addSuccess($this->__('Successfully updated.'));

        $this->_redirect('*/*');

        return $this;
    }

    /**
     * Export order grid to CSV format
     *
     * @return void
     */
    public function exportCsvAction()
    {
        /** @var BSeller_GiftShowcase_Block_Adminhtml_Giftshowcase_Grid $grid */
        $grid = $this->getLayout()->createBlock('bseller_giftshowcase/adminhtml_giftshowcase_grid');
        $this->_prepareDownloadResponse($this->_helper()->getExportFilename('csv'), $grid->getCsvFile());
    }

    /**
     * Export order grid to Excel XML format
     *
     * @return void
     */
    public function exportExcelAction()
    {
        $filename = $this->_helper()->getExportFilename('xml');

        /** @var BSeller_GiftShowcase_Block_Adminhtml_Giftshowcase_Grid $grid */
        $grid = $this->getLayout()->createBlock('bseller_giftshowcase/adminhtml_giftshowcase_grid');
        $this->_prepareDownloadResponse($filename, $grid->getExcelFile($filename));
    }

    /**
     * Is allowed to access
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('bseller/bseller_giftshowcase/gift');
    }
}
