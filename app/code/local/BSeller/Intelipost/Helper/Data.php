<?php
/**
 * B Seller Platform | B2W Digital
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Intelipost
 *
 * @copyright Copyright (c) 2019 B2W Digital - B Seller Platform (http://www.bseller.com.br)
 *
 * @author    Cristhian Andrade <cristhian.novaes@e-smart.com.br>
 */
class BSeller_Intelipost_Helper_Data extends Mage_Core_Helper_Abstract
{

    const URL = 'quote/available_scheduling_dates/';

    public function getSchedulingDates()
    {
        $cart = Mage::getSingleton('checkout/cart');
        $quote = $cart->getQuote();
        $shippingAddress = $quote->getShippingAddress();
        $zip = $shippingAddress->getPostcode();

        if(empty($zip)){
            return false;
        }

        $zipOrigin = Mage::getStoreConfig('shipping/origin/postcode');

        if(empty($zipOrigin)){
            return false;
        }

        $zipOriginDefaultConfig = Mage::getStoreConfig('intelipost_extra_config/delivery_config/active');

        if($zipOriginDefaultConfig) {
            $shippingMethodIdIntelipost = Mage::getStoreConfig('intelipost_extra_config/delivery_config/fix_id');

            if(!$shippingMethodIdIntelipost){
                return false;
            }
        }
        else {
            $shippingMethodIdIntelipost = explode("_", $quote->getShippingAddress()->getShippingMethod());
            $shippingMethodIdIntelipost = $shippingMethodIdIntelipost[1];

            if (!is_numeric($shippingMethodIdIntelipost)) {
                return false;
            }
        }

        $url = self::URL;
        $url = $url . $shippingMethodIdIntelipost.'/';
        $url = $url . $zipOrigin.'/'.$zip;

        $helper =  Mage::helper('basic');
        $helper->setVersionControlData($helper->getModuleName(), 'basic');

        $intelipost_api = Mage::getModel('basic/intelipost_api');
        $intelipost_api->apiRequest(Intelipost_Basic_Model_Intelipost_Api::GET, $url, false, $helper->getVersionControlModel());

        return $intelipost_api->apiResponseToObject();

    }

    public function getJsonSchedulingDates()
    {
        $response  = $this->getSchedulingDates();

        if(!$response){
            return false;
        }

        if($response->status != 'OK'){
            return false;
        }

        if(!$response->content->available_business_days){
            return 0;
        }

        $dates = null;

        foreach ($response->content->available_business_days as $index => $stringDate ){
            $dates[$index] = date("j-n-Y", strtotime($stringDate));
        }

        return json_encode($dates);
    }

    public function getMinAvailableDate($dates)
    {
        $dates = json_decode($dates);
        if(!is_array($dates) || !isset($dates[0])){
            return 0;
        }

        $arrayDates = date_parse_from_format("j-n-Y", $dates[0]);

        return $this->getDaysDiff(date('Y-m-d', strtotime($arrayDates['year']."-".$arrayDates['month']."-".$arrayDates['day'])));
    }

    public function getDaysDiff($newDate)
    {
        $date1 = new DateTime(date("Y-m-d"));
        $date2 = new DateTime($newDate);
        $interval = $date1->diff($date2);
        return $interval->days;
    }

    public function createSchedulingDateLog($response)
    {
        Mage::log($response, null, 'intelipost_schedulingDate.log', true);
    }

}