<?php
class BSeller_Intelipost_IndexController extends Mage_Core_Controller_Front_Action
{

    public function schedulingDatesAction()
    {
        $helperIntelipost = Mage::helper('bseller_intelipost');
        $result = $helperIntelipost->getJsonSchedulingDates();
        $helperIntelipost->createSchedulingDateLog($result);

        $minAvailableDate = Mage::helper('bseller_intelipost')->getMinAvailableDate($result);

        return $this->getResponse()->setBody(Mage::helper('core')->jsonEncode(
            array(
                "result"            => $result,
                "minAvailableDate"    => $minAvailableDate,
            )
        ));
    }
}
