<?php

$installer = $this;

$installer->startSetup();

$installer->run("CREATE INDEX IDX_SALES_FLAT_ORDER_ERP_STATUS ON {$installer->getTable('sales_flat_order')} (erp_status);");

$installer->endSetup(); 