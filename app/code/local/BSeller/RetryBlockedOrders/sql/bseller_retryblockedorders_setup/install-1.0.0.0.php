<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_PromotionalFlag
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Evandro Veloso Gomes <evandro.gomes@e-smart.com.br>
 */

/* @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;

$installer->startSetup();

/** @var Varien_Db_Adapter_Pdo_Mysql $connection */
$connection = $installer->getConnection();

/**
 * Create promotional_flag table
 */
$table = $connection->newTable($installer->getTable('bseller_retryblockedorders/retries'))
    ->addColumn(
        'entity_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        11,
        [
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
            'primary'  => true
        ],
        'Entity ID'
    )
    ->addColumn(
        'order_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        11,
        [],
        'Order ID'
    )
    ->addColumn(
        'qty_retries_erp',
        Varien_Db_Ddl_Table::TYPE_TINYINT,
        1,
        [
            'unsigned' => true,
            'nullable' => false,
            'default'  => 0
        ],
        'Quantity retries integration to ERP'
    )
    ->addColumn(
        'qty_retries_payment',
        Varien_Db_Ddl_Table::TYPE_TINYINT,
        1,
        [
            'unsigned' => true,
            'nullable' => false,
            'default'  => 0
        ],
        'Quantity retries integration to payment gateway'
    )
    ->addColumn(
        'blocked_at',
        Varien_Db_Ddl_Table::TYPE_DATETIME,
        null,
        [
            'nullable' => false,
            'default'  => '0000-00-00 00:00:00'
        ],
        'Order Block Date'
    )
    ->addColumn(
        'created_at',
        Varien_Db_Ddl_Table::TYPE_DATETIME,
        null,
        [
            'nullable' => false,
            'default'  => '0000-00-00 00:00:00'
        ],
        'Created At'
    )
    ->addColumn(
        'updated_at',
        Varien_Db_Ddl_Table::TYPE_DATETIME,
        null,
        [
            'nullable' => false,
            'default'  => '0000-00-00 00:00:00'
        ],
        'Updated At'
    )
    ->addColumn(
        'solved_erp',
        Varien_Db_Ddl_Table::TYPE_DATETIME,
        null,
        [
            'nullable' => true
        ],
        'Solved ERP'
    )
    ->addColumn(
        'solved_payment',
        Varien_Db_Ddl_Table::TYPE_DATETIME,
        null,
        [
            'nullable' => true
        ],
        'Solved ERP'
    )
    ->addIndex($installer->getIdxName('bseller_retryblockedorders/retries', array('order_id')),
        array('order_id'))
    ->setComment('BSeller Retry Blocked Orders');

$connection->createTable($table);

$installer->endSetup();
