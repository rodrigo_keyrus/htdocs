<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_RetryBlockedOrders
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Evandro Veloso Gomes <evandro.gomes@e-smart.com.br>
 */

class BSeller_RetryBlockedOrders_Model_Retries extends Mage_Core_Model_Abstract
{
    /**
     * Constructor initialization
     */
    public function _construct()
    {
        $this->_init('bseller_retryblockedorders/retries');

        parent::_construct();
    }
}
