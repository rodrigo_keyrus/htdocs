<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_RetryBlockedOrders
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Evandro Veloso Gomes <evandro.gomes@e-smart.com.br>
 * @author    Rafael Falcao <rafael.falcao@e-smart.com.br>
 *
 */

class BSeller_RetryBlockedOrders_Model_Cron extends Mage_Core_Model_Abstract
{
    /**
     * Helper module BSeller_RetryBlockedOrders
     *
     * @var BSeller_RetryBlockedOrders_Helper_Data
     */
    private $_helper;

    /**
     * Allowed Order Status
     *
     * @var array
     */
    private $_allowedStatus = array(
        BSellerERP_Order_Helper_Status::BLOCKED,
        BSellerERP_Order_Helper_Status::BLOCKED_APPROVE,
        BSellerERP_Order_Helper_Status::BLOCKED_DISAPPROVE
    );

    /**
     * Retries and their periods
     *
     * @var array
     */
    private $_retriesPeriods = array(
        0 => '-30 minutes',
        1 => '-3 hours',
        2 => '-12 hours'
    );

    /** @var string integration type */
    const INTEGRATION_ORDER = 'order';

    /** @var string integration type */
    const INTEGRATION_PAYMENT = 'payment';

    /**
     * Try number
     *
     * @var int
     */
    private $_retryNumber;

    /**
     * Called by the cron starts the process of resending blocked orders
     */
    public function retryBlockedOrders()
    {
        $this->_helper = Mage::helper('bseller_retryblockedorders');

        if(!$this->_helper->isModuleEnabled()){
            return;
        }

        foreach ($this->_retriesPeriods as $retry => $period){

            $this->_retryNumber = $retry;

            $date = $this->_helper->getTimestampDate($period);

            $orders = $this->_getCollection($date, self::INTEGRATION_ORDER);
            $this->_integrateOrdersERP($orders);

            $orders = $this->_getCollection($date, self::INTEGRATION_PAYMENT);
            $this->_integratePaymentsERP($orders);
        }
    }


    /**
     * First try to integrate the order to the ERP
     */
    private function _integrateOrdersERP($orders)
    {
        /** @var Mage_Sales_Model_Order $order */
        foreach ($orders as $order) {

            /** @var BSeller_RetryBlockedOrders_Model_Retries $retries */
            $retries = $this->_helper->getRetries($order);

            if($this->_retryNumber == 0){

                if(!$retries || $retries->getQtyRetriesErp() == 0){
                    $this->_flagOrderToIntegrate($order);
                }

            } else {

                if ($order->getErpStatus() == BSellerERP_Order_Helper_Status::BLOCKED) {
                    $this->_flagOrderToIntegrate($order);
                } elseif ($order->getErpStatus() != BSellerERP_Order_Helper_Status::PENDING) {
                    $this->_setSolvedERP($retries);
                }
            }

        }
    }

    /**
     * Changes the status of the order so it will reintegrated to the ERP
     * and updates the retries table.
     *
     * @param Mage_Sales_Model_Order $order
     *
     * @throws Exception
     */
    private function _flagOrderToIntegrate(Mage_Sales_Model_Order $order)
    {
        if (!in_array($order->getErpStatus(), $this->_allowedStatus)) {
            return;
        }

        if (!$this->_helper->validateOrder($order)) {
            return;
        }

        try{
            /** @var Mage_Sales_Model_Order $order */
            $order->setData('erp_status', 'pending');
            $order->getResource()->save($order);

        } catch (Exception $e){
            $this->_helper->errorLog('_flagOrderToIntegrate | Save Orders');
        }

        try{
            /** @var BSeller_RetryBlockedOrders_Model_Retries $retries */
            $retries = $this->_helper->getRetries($order);
            $retries->setQtyRetriesErp($retries->getQtyRetriesErp() + 1);
            $retries->save();

        } catch (Exception $e){
            $this->_helper->errorLog(' _flagOrderToIntegrate | Save Retries');
        }
    }

    /**
     * First try to integrate the payments to the ERP
     */
    private function _integratePaymentsERP($orders)
    {
        /** @var Mage_Sales_Model_Order $order */
        foreach ($orders as $order) {

            /** @var BSeller_RetryBlockedOrders_Model_Retries $retries */
            $retries = $this->_helper->getRetries($order);

            if($this->_retryNumber == 0){

                /** @var BSeller_RetryBlockedOrders_Model_Retries $retries */
                $retries = $this->_helper->getRetries($order);
                if (!$retries || ($retries->getQtyRetriesPayment() == 0)) {
                    $this->_flagPaymentToIntegrate($order);
                }

            } else {

                if ($order->getErpStatus() == BSellerERP_Order_Helper_Status::BLOCKED_APPROVE) {
                    $this->_flagPaymentToIntegrate($order);
                } elseif ($order->getErpStatus() != BSellerERP_Order_Helper_Status::SUCCESS) {
                    $this->_setSolvedPayment($retries);
                }
            }

        }
    }

    /**
     * Changes the status of the order so it will reintegrated to the ERP
     * and updates the retries table.
     *
     * @param Mage_Sales_Model_Order $order
     *
     * @throws Exception
     */
    private function _flagPaymentToIntegrate(Mage_Sales_Model_Order $order)
    {
        if (!in_array($order->getErpStatus(), $this->_allowedStatus)) {
            return;
        }

        if (!$this->_helper->validateOrder($order)) {
            return;
        }

        try{
            /** @var Mage_Sales_Model_Order $order */
            $order->setData('erp_status', BSellerERP_Order_Helper_Status::SUCCESS);
            $order->getResource()->save($order);

        } catch (Exception $e){
            $this->_helper->errorLog('_flagOrderToIntegrate | Save Orders');
        }

        try{
            /** @var BSeller_RetryBlockedOrders_Model_Retries $retries */
            $retries = $this->_helper->getRetries($order);
            $retries->setQtyRetriesPayment($retries->getQtyRetriesPayment() + 1);
            $retries->save();

        } catch (Exception $e){
            $this->_helper->errorLog(' _flagOrderToIntegrate | Save Retries');
        }
    }

    /**
     * Get Collection Orders or Payment to integrate
     *
     * @param string $date
     * @param string $integrationType
     *
     * @return Mage_Sales_Model_Resource_Order_Collection
     */
    private function _getCollection($date, $integrationType)
    {
        $ordersIds = array();

        if($this->_retryNumber > 0) {

            /** @var BSeller_RetryBlockedOrders_Model_Resource_Retries_Collection $orders */
            $retriesCollection = Mage::getResourceModel('bseller_retryblockedorders/retries_collection')
                                    ->addBlockedAtLessFilter($date);

            if($integrationType == self::INTEGRATION_ORDER){
                $retriesCollection->addNotSolvedERPFilter()
                                  ->addQuantityRetriesERPFilter($this->_retryNumber);
            } else {
                $retriesCollection->addNotSolvedPaymentFilter()
                                  ->addQuantityRetriesPaymentFilter($this->_retryNumber);
            }

            $retriesCollection->addOrderIdOrder();

            if(count($retriesCollection) == 0) {
                return [];
            }

            foreach ($retriesCollection as $retry){
                $ordersIds[] = $retry->getOrderId();
            }
        }

        /** @var Mage_Sales_Model_Resource_Order_Collection $orders */
        $orders = Mage::getResourceModel('sales/order_collection')
                ->addFieldToFilter('status', array('nin' => array('canceled')))
                ->addFieldToSelect(['increment_id', 'updated_at', 'erp_status']);

        if($integrationType == self::INTEGRATION_ORDER){
            $orders->addFieldToFilter('erp_status', BSellerERP_Order_Helper_Status::BLOCKED);
        } else {
            $orders->addFieldToFilter('erp_status', BSellerERP_Order_Helper_Status::BLOCKED_APPROVE);
        }

        if(count($ordersIds) > 0){
            $orders->addAttributeToFilter('entity_id', array('in' => $ordersIds));
        } else {
            $orders->addFieldToFilter('updated_at', array('lteq' => array($date)));
        }

        $orders->addOrder('entity_id', 'desc')
            ->setPage(0, 100);
        
        return $orders;
    }

    /**
     * Set solved ERP date
     *
     * @param BSeller_RetryBlockedOrders_Model_Retries $retries
     *
     * @throws Exception
     */
    private function _setSolvedERP($retries)
    {
        try {
            $retries->setSolvedErp(Mage::getModel('core/date')->date('Y-m-d H:i:s'));
            $retries->save();
        } catch (Exception $e){
            $this->_helper->errorLog('_setSolvedERP | Save Retries');
        }
    }

    /**
     * Set solved payment date
     *
     * @param BSeller_RetryBlockedOrders_Model_Retries $retries
     *
     * @throws Exception
     */
    private function _setSolvedPayment($retries)
    {
        try {
            $retries->setSolvedPayment(Mage::getModel('core/date')->date('Y-m-d H:i:s'));
            $retries->save();
        } catch (Exception $e){
            $this->_helper->errorLog('_setSolvedPayment | Save Retries');
        }
    }
}