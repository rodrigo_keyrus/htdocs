<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_RetryBlockedOrders
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Evandro Veloso Gomes <evandro.gomes@e-smart.com.br>
 */

class BSeller_RetryBlockedOrders_Model_Resource_Retries_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    /**
     * Collection initialization
     */
    public function _construct()
    {
        $this->_init('bseller_retryblockedorders/retries');
    }

    /**
     * Add quantity retries to ERP to filter collection
     *
     * @param int $quantity
     *
     * @return $this
     */
    public function addQuantityRetriesERPFilter($quantity)
    {
        $this->addFieldToFilter('qty_retries_erp', $quantity);

        return $this;
    }

    /**
     * Add quantity payment retries to filter collection
     *
     * @param int $quantity
     *
     * @return $this
     */
    public function addQuantityRetriesPaymentFilter($quantity)
    {
        $this->addFieldToFilter('qty_retries_payment', $quantity);

        return $this;
    }

    /**
     * Add blocked_at to filter collection
     *
     * @param DateTime $datetime
     *
     * @return $this
     */
    public function addBlockedAtLessFilter($datetime)
    {
        $this->addFieldToFilter(['blocked_at'],
            [
                ['lteq' => $datetime]
            ]
        );

        return $this;
    }

    /**
     * Add solved_erp to filter collection
     *
     * @return $this
     */
    public function addNotSolvedERPFilter()
    {
        $this->addFieldToFilter(['solved_erp'],
            [
                ['null' => true]
            ]
        );

        return $this;
    }

    /**
     * Add solved_payment to filter collection
     *
     * @return $this
     */
    public function addNotSolvedPaymentFilter()
    {
        $this->addFieldToFilter(['solved_payment'],
            [
                ['null' => true]
            ]
        );

        return $this;
    }

    /**
     * Add order_id type to order collection
     *
     * @param string $dir
     * @return $this
     */
    public function addOrderIdOrder($dir = Varien_Db_Select::SQL_DESC)
    {
        $this->addOrder('order_id', $dir);

        return $this;
    }
}
