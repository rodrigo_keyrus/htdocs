# Retry Blocked Orders

Aiming at the stability of the platform, we need to apply some rules of 
order integration retest for "Blocking" and "Non-Sent Payment Approval".

This module tries to send the integration of the order by up to 3x after 
it has been blocked.

     The first retentative, 30mins after the blockade
     The second retentative, 3h after the lock
     The third retentative, 12 hours after the blockade

The same logic should be applied when we have the status of 
"Non-Sent Payment Approval" (it should follow the same ranges as 
mentioned above).

To not overload the database the cron is scheduled to run every ten 
minutes so the reported periods are only approximate. 

The order data is validated and only the retry is done only if 
there is no problem in the order data. 

The reported period is applied in relation to the first block.
If cron is stopped for a long enough time the three attempts can be made 
with an interval of only ten minutes.

The cron not directly send the request but  mark the request again as 
'pending'. 

The new attempt is made by the existing cron responsible for the 
integration of the requests.


## Dependencies

                BSellerERP_Core
                BSellerERP_Order
                BSellerERP_Payment

### Changelog

