<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_CatalogRule
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Guilherme Miguelete <guilherme.miguelete@e-smart.com.br>
 */

class BSeller_CatalogRule_Model_Rule extends Mage_CatalogRule_Model_Rule
{

    /**
     * Set specified data to current rule
     * Set conditions and actions recursively
     * Convert dates into Zend_Date
     *
     * @param array $data
     * @return array
     */
    protected function _convertFlatToRecursive(array $data)
    {

        $arr = [];

        foreach ($data as $key => $value) {
            if (($key === 'conditions' || $key === 'actions') && is_array($value)) {
                foreach ($value as $id => $data) {
                    $path = explode('--', $id);
                    $node = &$arr;

                    for ($i = 0, $l = sizeof($path); $i < $l; $i++) {
                        if (!isset($node[$key][$path[$i]])) {
                            $node[$key][$path[$i]] = [];
                        }

                        $node = &$node[$key][$path[$i]];
                    }

                    foreach ($data as $k => $v) {
                        $node[$k] = $v;
                    }
                }

                continue;
            }

            /**
             * Convert datetime's into Zend_Date
             */
            if (in_array($key, ['from_date', 'to_date']) && $value) {

                $value = Mage::getModel('core/date')->gmtDate(null, $value);

            }

            $this->setData($key, $value);
        }

        return $arr;
    }

    /**
     * Validate rule data
     *
     * @param Varien_Object $object
     * @return bool|array
     */
    public function validateData(Varien_Object $object)
    {
        $result   = [];
        $fromDate = $toDate = null;

        if ($object->hasData('from_date') && $object->hasData('to_date')) {
            $fromDate = $object->getData('from_date');
            $toDate   = $object->getData('to_date');
        }

        if ($fromDate && $toDate) {
            $fromDate = new Zend_Date($fromDate, Varien_Date::DATETIME_INTERNAL_FORMAT);
            $toDate   = new Zend_Date($toDate, Varien_Date::DATETIME_INTERNAL_FORMAT);

            if ($fromDate->compare($toDate) === 1) {
                $result[] = Mage::helper('bseller_catalogrule')->__('End Date must be greater than Start Date.');
            }
        }

        if ($object->hasData('website_ids')) {
            $websiteIds = $object->getData('website_ids');

            if (empty($websiteIds)) {
                $result[] = Mage::helper('bseller_catalogrule')->__('Websites must be specified.');
            }
        }

        if ($object->hasData('customer_group_ids')) {
            $customerGroupIds = $object->getData('customer_group_ids');

            if (empty($customerGroupIds)) {
                $result[] = Mage::helper('bseller_catalogrule')->__('Customer Groups must be specified.');
            }
        }

        return !empty($result) ? $result : true;
    }

}
