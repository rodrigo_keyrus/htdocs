<?php
/**
 * BSeller Platform
 *
 * Não edite este arquivo caso você pretenda atualizar este módulo futuramente
 * para novas versões.
 *
 * @category  BSeller
 * @package   BSeller_CatalogRule
 *
 * @copyright Copyright (c) 2016 B2W Digital - BSeller Platform. (http://www.bseller.com.br)
 *
 * @author    Guilherme Miguelete <guilherme.miguelete@e-smart.com.br>
 */

class BSeller_CatalogRule_Model_Rules
{

    /**
     * Apply all Catalog rules
     */
    public function applyAllRules()
    {

        try {
            Mage::getModel('catalogrule/rule')->applyAll();
            Mage::app()->removeCache('catalog_rules_dirty');
        } catch (Exception $e) {
            Mage::logException($e);
        }

    }

    /**
     * @param $sku
     * @return $this
     */
    public function applyAllRulesOnProduct($sku)
    {

        $productId = Mage::getModel('catalog/product')->getIdBySku($sku);

        if (!$productId) {
            return $this;
        }

        $product = Mage::getModel('catalog/product')->load($productId);

        if (!$product->getId()) {
            return $this;
        }

        $rules = Mage::getModel('catalogrule/rule')->getCollection()
            ->addFieldToFilter('is_active', 1);

        foreach ($rules as $rule) {
            $websiteIds = array_intersect($product->getWebsiteIds(), $rule->getWebsiteIds());
            $rule->applyToProduct($product, $websiteIds);
        }

        return $this;
    }

}
