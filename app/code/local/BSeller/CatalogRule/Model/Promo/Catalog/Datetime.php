<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_CatalogRule
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Guilherme Miguelete <guilherme.miguelete@e-smart.com.br>
 */


/**
 * Class BSeller_CatalogRule_Model_Promo_Catalog_Datetime
 * 
 * @method Varien_Data_Form getForm()
 * @method Varien_Data_Form_Element_Fieldset getElement()
 * @method string getFormat()
 */
class BSeller_CatalogRule_Model_Promo_Catalog_Datetime extends Mage_Core_Model_Abstract
{
    /**
     * Insert date from field in form
     *
     * @return Varien_Data_Form_Element_Abstract
     */
    public function addDateFromField()
    {
        return $this->getElement()->addField(
            'from_date',
            'datetime',
            [
                'name'         => 'from_date',
                'label'        => Mage::helper('bseller_catalogrule')->__('From Date'),
                'title'        => Mage::helper('bseller_catalogrule')->__('From Date'),
                'image'        => Mage::getDesign()->getSkinUrl('images/grid-cal.gif'),
                'input_format' => Varien_Date::DATETIME_INTERNAL_FORMAT,
                'format'       => $this->getFormat(),
                'time'         => true
            ],
            'uses_per_customer'
        );
    }

    /**
     * Insert date from field in form
     *
     * @return Varien_Data_Form_Element_Abstract
     */
    public function addDateToField()
    {
        return $this->getElement()->addField(
            'to_date',
            'datetime',
            [
                'name'         => 'to_date',
                'label'        => Mage::helper('bseller_catalogrule')->__('To Date'),
                'title'        => Mage::helper('bseller_catalogrule')->__('To Date'),
                'image'        => Mage::getDesign()->getSkinUrl('images/grid-cal.gif'),
                'input_format' => Varien_Date::DATETIME_INTERNAL_FORMAT,
                'format'       => $this->getFormat(),
                'time'         => true
            ],
            'from_date'
        );
    }
}
