<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_CatalogRule
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Guilherme Miguelete <guilherme.miguelete@e-smart.com.br>
 */

class BSeller_CatalogRule_Model_Observer extends Mage_CatalogRule_Model_Observer
{
    /**
     * Update catalog rule tab main form
     * Update
     *
     * @param Varien_Event_Observer $observer
     * @return $this
     */
    public function updateCatalogRuleTabMainForm(Varien_Event_Observer $observer)
    {
        /** @var Varien_Data_Form $form */
        $form = $observer->getEvent()->getData('form');

        if (!$form) {
            return $this;
        }

        /** @var Varien_Data_Form_Element_Abstract $element */
        $element = $form->getElements()->searchById('base_fieldset');

        if (!$element) {
            return $this;
        }

        /**
         * Remove old field elements
         */
        $element->removeField('from_date');
        $element->removeField('to_date');

        /** @var BSeller_CatalogRule_Model_Promo_Catalog_Datetime $datetime */
        $datetime = Mage::getModel(
            'bseller_catalogrule/promo_catalog_datetime',
            [
                'form' => $form,
                'element' => $element,
                'format' => Mage::app()->getLocale()->getDateTimeFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT)
            ]
        );

        /** @var Mage_CatalogRule_Model_Rule $catalogRule */
        $catalogRule = Mage::registry('current_promo_catalog_rule');

        $datetime->addDateFromField()->setValue(
            $this->getLocaleDate($catalogRule->getData('from_date'))
        );

        $datetime->addDateToField()->setValue(
            $this->getLocaleDate($catalogRule->getData('to_date'))
        );

        return $this;
    }

    /**
     * Convert datetime to a locale timezone for edit in admin form
     *
     * @param $date
     * @return Zend_Date
     */
    public function getLocaleDate($date)
    {
        $date = Mage::app()->getLocale()->date(
            $date,
            Varien_Date::DATETIME_INTERNAL_FORMAT,
            null,
            true
        );

        return $date;
    }

    /**
     * Apply catalog price rules to product on frontend
     *
     * @param   Varien_Event_Observer $observer
     *
     * @return  Mage_CatalogRule_Model_Observer
     */
    public function processFrontFinalPrice($observer)
    {
        $product    = $observer->getEvent()->getProduct();
        $pId        = $product->getId();
        $storeId    = $product->getStoreId();

        if ($observer->hasDate()) {
            $date = $observer->getEvent()->getDate();
        } else {
            $date = Mage::app()->getLocale()->storeTimeStamp($storeId);
        }

        if ($observer->hasWebsiteId()) {
            $wId = $observer->getEvent()->getWebsiteId();
        } else {
            $wId = Mage::app()->getStore($storeId)->getWebsiteId();
        }

        if ($observer->hasCustomerGroupId()) {
            $gId = $observer->getEvent()->getCustomerGroupId();
        } elseif ($product->hasCustomerGroupId()) {
            $gId = $product->getCustomerGroupId();
        } else {
            $gId = Mage::getSingleton('customer/session')->getCustomerGroupId();
        }

        $key = $this->_getRulePricesKey(array($date, $wId, $gId, $pId));
        if (!isset($this->_rulePrices[$key])) {
            $rulePrice = Mage::getResourceModel('catalogrule/rule')
                ->getRulePrice($date, $wId, $gId, $pId);
            $this->_rulePrices[$key] = $rulePrice;
        }
        if ($this->_rulePrices[$key]!==false) {
            $finalPrice = min($product->getData('final_price'), $this->_rulePrices[$key]['rule_price']);

            if ($finalPrice == $this->_rulePrices[$key]['rule_price']) {
                $product->setFinalPrice($finalPrice);
                $product->setSpecialFromDate($this->_rulePrices[$key]['latest_start_date']);
                $product->setSpecialToDate($this->_rulePrices[$key]['earliest_end_date']);
            }
        }

        return $this;
    }
}
