<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_CatalogRule
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Guilherme Miguelete <guilherme.miguelete@e-smart.com.br>
 */

class BSeller_CatalogRule_Block_Adminhtml_Promo_Catalog extends Mage_Adminhtml_Block_Promo_Catalog
{

    /**
     * Remove "Save and Apply" button
     */
    public function __construct()
    {
        parent::__construct();
        //$this->_removeButton('apply_rules');
    }

}
