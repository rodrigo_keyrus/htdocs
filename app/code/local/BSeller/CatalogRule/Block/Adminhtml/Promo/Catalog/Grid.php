<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_CatalogRule
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Guilherme Miguelete <guilherme.miguelete@e-smart.com.br>
 */

class BSeller_CatalogRule_Block_Adminhtml_Promo_Catalog_Grid extends Mage_Adminhtml_Block_Promo_Catalog_Grid
{
    /**
     * Add grid columns
     *
     * @return Mage_Adminhtml_Block_Promo_Catalog_Grid
     */
    protected function _prepareColumns()
    {
        parent::_prepareColumns();

        $this->addColumn(
            'from_date',
            [
                'header'   => Mage::helper('catalogrule')->__('Date Start'),
                'align'    => 'left',
                'width'    => '120px',
                'type'     => 'datetime',
                'default'  => '--',
                'index'    => 'from_date'
            ]
        );

        $this->addColumn(
            'to_date',
            [
                'header'   => Mage::helper('catalogrule')->__('Date Expire'),
                'align'    => 'left',
                'width'    => '120px',
                'type'     => 'datetime',
                'default'  => '--',
                'index'    => 'to_date'
            ]
        );

        return $this;
    }

}
