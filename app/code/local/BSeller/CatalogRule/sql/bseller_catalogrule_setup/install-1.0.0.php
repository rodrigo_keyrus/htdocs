<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_CatalogRule
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Guilherme Miguelete <guilherme.miguelete@e-smart.com.br>
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer->getConnection()
    ->modifyColumn(
        $installer->getTable('catalogrule/rule'),
        'from_date',
        [
            'type'    => Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
            'default' => '0000-00-00 00:00:00'
        ]
    );

$installer->getConnection()
    ->modifyColumn(
        $installer->getTable('catalogrule/rule'),
        'to_date',
        [
            'type'    => Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
            'default' => '0000-00-00 00:00:00'
        ]
    );

$installer->endSetup();
