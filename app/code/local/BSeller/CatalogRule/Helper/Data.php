<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_CatalogRule
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Guilherme Miguelete <guilherme.miguelete@e-smart.com.br>
 */

class BSeller_CatalogRule_Helper_Data extends BSeller_Core_Helper_Data
{

    /**
     * Resque queue name
     *
     * @const string
     */
    const RESQUE_QUEUE_NAME = 'BSellerCatalogRule';

    /**
     * Resque job class
     *
     * @const string
     */
    const RESQUE_JOB_CLASS = 'BSeller_CatalogRule_Model_Job';

}
