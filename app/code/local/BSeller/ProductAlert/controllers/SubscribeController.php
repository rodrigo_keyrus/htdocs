<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_ProductAlert
 *
 * @copyright Copyright (c) 2017 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Luiz Fernando Alves <luiz.anjos@b2wdigital.com>
 */

class BSeller_ProductAlert_SubscribeController extends Mage_Core_Controller_Front_Action
{

    /**
     * Perform save to subscribe customer
     * @param $name
     * @param $email
     * @param $productId
     * @return bool
     */
    protected function save($name, $email, $productId)
    {
        /** @var BSeller_ProductAlert_Model_Guest $model */
        $model = Mage::getModel('bseller_productalert/guest');

        $subscriptionExists = $model->subscriptionExists($email, $productId);
        if ($subscriptionExists->hasData('email')) {
            return false;
        }

        /** @var Mage_Catalog_Model_Product $product */
        $product = Mage::getModel('catalog/product')->load($productId);

        try {
            $model->setName($name)
                ->setEmail($email)
                ->setProductId($product->getId())
                ->setPrice($product->getFinalPrice())
                ->setAddDate(Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s'))
                ->setWebsiteId(Mage::app()->getStore()->getWebsiteId());
            $model->save();
        }
        catch (Exception $e) {
            return false;
        }
        return true;
    }

    /**
     * @return Zend_Controller_Response_Abstract
     * @throws Zend_Controller_Response_Exception
     */
    public function newsAction()
    {
        /**
         * Get posted params
         */
        $name = $this->getRequest()->getParam('name');
        $email = $this->getRequest()->getParam('email');
        $productId  = (int) $this->getRequest()->getParam('product_id');

        $save = $this->save($name, $email, $productId);

        if ($save) {
            return $this->getResponse()
                ->setHttpResponseCode(200)
                ->setBody(1);
        } else {
            return $this->getResponse()
                ->setHttpResponseCode(200)
                ->setBody(0);
        }
    }
}