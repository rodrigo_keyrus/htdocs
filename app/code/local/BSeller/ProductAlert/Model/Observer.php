<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_ProductAlert
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Evandro Veloso Gomes <evandro.gomes@e-smart.com.br>
 */

class BSeller_ProductAlert_Model_Observer extends Mage_ProductAlert_Model_Observer
{
    /**
     * Process stock emails
     *
     * @return Mage_ProductAlert_Model_Observer
     */
    protected function _processStock()
    {
        $originalStore = Mage::app()->getStore();

        foreach ($this->_getWebsites() as $website) {
            /* @var $website Mage_Core_Model_Website */

            if (!$website->getDefaultGroup() || !$website->getDefaultGroup()->getDefaultStore()) {
                continue;
            }
            if (!Mage::getStoreConfig(
                self::XML_PATH_STOCK_ALLOW,
                $website->getDefaultGroup()->getDefaultStore()->getId()
            )) {
                continue;
            }
            try {
                $collection = Mage::getModel('bseller_productalert/guest')
                    ->getCollection()
                    ->addWebsiteFilter($website->getId())
                    ->addStatusFilter(0);
            }
            catch (Exception $e) {
                $this->_errors[] = $e->getMessage();
                return $this;
            }

            Mage::app()->setCurrentStore($website->getDefaultGroup()->getDefaultStore());
            foreach ($collection as $alert) {
                try {
                    $product = Mage::getModel('catalog/product')
                        ->setStoreId($website->getDefaultStore()->getId())
                        ->load($alert->getProductId());

                    /* @var $product Mage_Catalog_Model_Product */
                    if (!$product) {
                        continue;
                    }

                    if ($product->isSalable()) {
                        $alert->setSendDate(Mage::getModel('core/date')->gmtDate());
                        $alert->setSendCount($alert->getSendCount() + 1);
                        $alert->setStatus(1);
                        $alert->save();

                        /* @var $email Mage_ProductAlert_Model_Email */
                        $email = Mage::getModel('bseller_productalert/email');
                        $email->setWebsite($website);
                        $email->addStockProduct($product);
                        $email->setType('stock');
                        $email->setGuest($alert);
                        $email->send();
                    }
                }
                catch (Exception $e) {
                    $this->_errors[] = $e->getMessage();
                }
            }
        }
        Mage::app()->setCurrentStore($originalStore);

        return $this;
    }


    /**
     * Run process send product alerts
     *
     * @return Mage_ProductAlert_Model_Observer
     */
    public function process()
    {
        // $this->_processPrice($email);
        $this->_processStock();
        $this->_sendErrorEmail();

        return $this;
    }
}