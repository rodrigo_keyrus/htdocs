<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_ProductAlert
 *
 * @copyright Copyright (c) 2017 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Luiz Fernando Alves <luiz.anjos@b2wdigital.com>
 */

class BSeller_ProductAlert_Model_Guest extends BSeller_Core_Model_Abstract
{
    use BSeller_Core_Trait_Data;

    const ENTITY = 'guest';

    public function _construct()
    {
        $this->_init('bseller_productalert/guest');

        parent::_construct();
    }


    /**
     * Validate if user already subscribed
     * for the same product
     * @param $email
     * @param $productID
     * @return Varien_Object
     */
    public function subscriptionExists($email, $productID)
    {
        if ($email == null && $productID == null) {
            throw new Mage_Core_Exception("Blank email and productID");
        }

        $collection = Mage::getResourceModel('bseller_productalert/guest_collection');

        $collection->addFieldToSelect('*');
        $collection->addFieldToFilter('email', $email);
        $collection->addFieldToFilter('product_id', $productID);
        $collection->addFieldToFilter('status', 0);
        $collectionItems = $collection->getFirstItem();

        return $collectionItems;
    }
}