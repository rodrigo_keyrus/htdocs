<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_ProductAlert
 *
 * @copyright Copyright (c) 2017 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Luiz Fernando Alves <luiz.anjos@b2wdigital.com>
 */

class BSeller_ProductAlert_Model_Resource_Guest extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Initialize Resource
     * @return void
     */
    public function _construct()
    {
        $this->_init('bseller_productalert/guest', 'entity_id');
    }
}