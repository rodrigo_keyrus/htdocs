<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_ProductAlert
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Evandro Veloso Gomes <evandro.gomes@e-smart.com.br>
 */

class BSeller_ProductAlert_Model_Email extends Mage_ProductAlert_Model_Email
{
    /**
     * @var BSeller_ProductAlert_Model_Guest
     */
    private $_guest;

    /**
     * @return BSeller_ProductAlert_Model_Guest
     */
    public function getGuest()
    {
        return $this->_guest;
    }

    /**
     * @param BSeller_ProductAlert_Model_Guest $guest
     */
    public function setGuest($guest)
    {
        $this->_guest = $guest;
    }

    /**
     * Retrieve price block
     *
     * @return Mage_ProductAlert_Block_Email_Price
     */
    protected function _getPriceBlock()
    {
        if (is_null($this->_priceBlock)) {
            $this->_priceBlock = Mage::helper('bseller_productalert')
                ->createBlock('bseller_productalert/email_price');
        }
        return $this->_priceBlock;
    }

    /**
     * Retrieve stock block
     *
     * @return Mage_ProductAlert_Block_Email_Stock
     */
    protected function _getStockBlock()
    {
        if (is_null($this->_stockBlock)) {
            $this->_stockBlock = Mage::helper('bseller_productalert')
                ->createBlock('bseller_productalert/email_stock');
        }
        return $this->_stockBlock;
    }

    /**
     * Send customer email
     *
     * @return bool
     */
    public function send()
    {
        if (is_null($this->_website)) {
            return false;
        }
        if (($this->_type == 'price' && count($this->_priceProducts) == 0)
        ) {
            return false;
        }
        if (!$this->_website->getDefaultGroup() || !$this->_website->getDefaultGroup()->getDefaultStore()) {
            return false;
        }

        $storeId = null;
        if ($this->_customer) {
            $storeId = $this->_customer->getStoreId();
        } else {
            $storeId = $this->_website->getDefaultGroup()
                ->getDefaultStoreId();
        }

        $store = Mage::getModel('core/store')->load($storeId);

        if ($this->_type == 'price' && !Mage::getStoreConfig(self::XML_PATH_EMAIL_PRICE_TEMPLATE, $storeId)) {
            return false;
        } elseif ($this->_type == 'stock' && !Mage::getStoreConfig(self::XML_PATH_EMAIL_STOCK_TEMPLATE, $storeId)) {
            return false;
        }

        if ($this->_type != 'price' && $this->_type != 'stock') {
            return false;
        }

        $appEmulation = Mage::getSingleton('core/app_emulation');
        $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($storeId);
        Mage::app()->getTranslator()->init('frontend', true);

        if ($this->_type == 'price') {
            $this->_getPriceBlock()
                ->setStore($store)
                ->reset();
            foreach ($this->_priceProducts as $productSon) {

                $groupedParentsIds = Mage::getResourceSingleton('catalog/product_link')
                    ->getParentIdsByChild($productSon->getId(), Mage_Catalog_Model_Product_Link::LINK_TYPE_GROUPED);

                $product = Mage::getModel('catalog/product')->load($groupedParentsIds[0]);

                if ($this->_customer) {
                    $product->setCustomerGroupId($this->_customer->getGroupId());
                }
                $this->_getPriceBlock()->addProduct($product);
            }
            $block = $this->_getPriceBlock()->toHtml();
            $templateId = Mage::getStoreConfig(self::XML_PATH_EMAIL_PRICE_TEMPLATE, $storeId);
        } else {
            $this->_getStockBlock()
                ->setStore($store)
                ->reset();
            foreach ($this->_stockProducts as $productSon) {

                $groupedParentsIds = Mage::getResourceSingleton('catalog/product_link')
                    ->getParentIdsByChild($productSon->getId(), Mage_Catalog_Model_Product_Link::LINK_TYPE_GROUPED);

                $product = Mage::getModel('catalog/product')->load($groupedParentsIds[0]);

                if ($this->_customer) {
                    $product->setCustomerGroupId($this->_customer->getGroupId());
                }
                $this->_getStockBlock()->addProduct($product);
            }
            $block = $this->_getStockBlock()->toHtml();
            $templateId = Mage::getStoreConfig(self::XML_PATH_EMAIL_STOCK_TEMPLATE, $storeId);
        }

        $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);

        $customer_email = $this->_customer ? $this->_customer->getEmail() : $this->_guest->getEmail();
        $customer_name  = $this->_customer ? $this->_customer->getName()  : $this->_guest->getName();

        Mage::getModel('core/email_template')
            ->setDesignConfig(array(
                'area'  => 'frontend',
                'store' => $storeId
            ))->sendTransactional(
                $templateId,
                Mage::getStoreConfig(self::XML_PATH_EMAIL_IDENTITY, $storeId),
                $customer_email,
                $customer_name,
                array(
                    'customerName'  => $customer_name,
                    'alertGrid'     => $block
                )
            );

        return true;
    }
}
