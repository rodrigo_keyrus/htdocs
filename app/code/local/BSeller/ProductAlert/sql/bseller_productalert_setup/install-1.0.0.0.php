<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_ProductAlert
 *
 * @copyright Copyright (c) 2017 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Luiz Fernando Alves <luiz.anjos@b2wdigital.com>
 */


$installer = $this;

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer->startSetup();
/**
 * Create table 'productalert/guest'
 */

try {
    $table = $installer->getConnection()
        ->newTable($installer->getTable('bseller_productalert/guest'))
        ->addColumn('entity_id',
            Varien_Db_Ddl_Table::TYPE_INTEGER, null,
            [
                'identity' => true,
                'unsigned' => true,
                'nullable' => false,
                'primary' => true,
            ],
            'Table entity id'
        )
        ->addColumn('type',
            Varien_Db_Ddl_Table::TYPE_VARCHAR, 20,
            [
                'nullable' => false,
                'primary' => false
            ],
            'Product alert price id'
        )
        ->addColumn(
            'name',
            Varien_Db_Ddl_Table::TYPE_TEXT,
            200,
            [
                'nullable' => false,
                'default' => ''
            ],
            'Name'
        )
        ->addColumn(
            'email',
            Varien_Db_Ddl_Table::TYPE_TEXT,
            255,
            [
                'nullable' => false,
                'default' => ''
            ],
            'Email'
        )
        ->addColumn('product_id',
            Varien_Db_Ddl_Table::TYPE_INTEGER, null,
                [
                'unsigned' => true,
                'nullable' => false,
                'default' => '0',
            ],
            'Product id'
        )
        ->addColumn('price',
            Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,4',
            [
                'nullable' => false,
                'default' => '0.0000',
            ],
            'Price amount'
        )
        ->addColumn('website_id',
            Varien_Db_Ddl_Table::TYPE_SMALLINT, null,
            [
                'unsigned' => true,
                'nullable' => false,
                'default' => '0',
            ],
            'Website id'
        )
        ->addColumn('add_date',
            Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null,
           [
                'nullable' => false,
           ],
            'Product alert add date'
        )
        ->addColumn('last_send_date',
            Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, [], 'Product alert last send date')
        ->addColumn('send_count',
            Varien_Db_Ddl_Table::TYPE_SMALLINT, null,
            [
                'unsigned' => true,
                'nullable' => false,
                'default' => '0',
            ],
            'Product alert send count'
        )
        ->addColumn('status',
            Varien_Db_Ddl_Table::TYPE_SMALLINT, null,
            [
                'unsigned' => true,
                'nullable' => false,
                'default' => '0',
            ],
            'Product alert status'
        )
        ->addIndex($installer->getIdxName('bseller_productalert/guest', ['product_id']), ['product_id'])
        ->addIndex($installer->getIdxName('bseller_productalert/guest', ['website_id']), ['website_id'])
        ->addForeignKey($installer->getFkName('bseller_productalert/guest', 'product_id', 'catalog/product', 'entity_id'),
            'product_id', $installer->getTable('catalog/product'), 'entity_id',
            Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
        ->addForeignKey($installer->getFkName('bseller_productalert/guest', 'website_id', 'core/website', 'website_id'),
            'website_id', $installer->getTable('core/website'), 'website_id',
            Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
        ->setComment('Product Alert Price');
    $installer->getConnection()->createTable($table);
} catch (Zend_Db_Exception $exception) {
    echo $exception->getMessage();
}

$installer->endSetup();

