<?php
/**
 * B Seller | B2W - Companhia Digital
 *
 * DISCLAIMER
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category      BSeller
 * @package       BSeller_Monitoring
 *
 * @copyright     Copyright (c) 2016 B Seller | B2W - Companhia Digital. (http://www.bseller.com.br)
 *
 * @author        Guilherme Miguelete <guilherme.miguelete@e-smart.com.br>
 *
 */
class BSeller_Monitoring_Helper_Data extends Mage_Core_Helper_Abstract
{

    /**
     * Check is module exists and enabled in config
     *
     * @return bool
     */
    public function isModuleEnabled()
    {
        return Mage::getStoreConfigFlag('bseller_monitoring/settings/active')
        && parent::isModuleEnabled($this->_getModuleName());
    }

}
