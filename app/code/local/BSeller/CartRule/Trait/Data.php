<?php
/**
 * BSeller Platform
 *
 * Não edite este arquivo caso você pretenda atualizar este módulo futuramente
 * para novas versões.
 *
 * @category  BSeller
 * @package   BSeller_CartRule
 *
 * @copyright Copyright (c) 2016 B2W Digital - BSeller Platform. (http://www.bseller.com.br)
 *
 * @author    Tiago Sampaio <tiago.sampaio@e-smart.com.br>
 */

trait BSeller_CartRule_Trait_Data
{

    use BSeller_Core_Trait_Data;

}
