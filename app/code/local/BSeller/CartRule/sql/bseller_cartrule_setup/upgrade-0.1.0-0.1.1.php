<?php
/**
 * BSeller Platform
 *
 * Não edite este arquivo caso você pretenda atualizar este módulo futuramente
 * para novas versões.
 *
 * @category  BSeller
 * @package   BSeller_CartRule
 *
 * @copyright Copyright (c) 2016 B2W Digital - BSeller Platform. (http://www.bseller.com.br)
 *
 * @author    Tiago Sampaio <tiago.sampaio@e-smart.com.br>
 */

/**
 * @var Mage_Core_Model_Resource_Setup $installer
 * @var Magento_Db_Adapter_Pdo_Mysql   $conn
 */
$installer = $this;
$installer->startSetup();

$tables = array(
    $installer->getTable('sales/quote_item'),
    $installer->getTable('sales/order_item')
);

foreach ($tables as $table) {
    $conn->addColumn($table, 'skipped_discount', array (
        'type'    => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'length'  => 1,
        'default' => 0,
        'after'   => 'no_discount',
        'comment' => 'Indicates if the discount was skipped.'
    ));
}

$installer->endSetup();
