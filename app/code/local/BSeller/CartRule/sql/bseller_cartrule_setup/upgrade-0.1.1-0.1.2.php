<?php
/**
 * BSeller Platform
 *
 * Não edite este arquivo caso você pretenda atualizar este módulo futuramente
 * para novas versões.
 *
 * @category  BSeller
 * @package   BSeller_CartRule
 *
 * @copyright Copyright (c) 2016 B2W Digital - BSeller Platform. (http://www.bseller.com.br)
 *
 * @author    Guilherme Miguelete <guilherme.migueletee-smart.com.br>
 */

/**
 * @var Mage_Core_Model_Resource_Setup $installer
 * @var Magento_Db_Adapter_Pdo_Mysql   $conn
 */
$installer = $this;
$installer->startSetup();


$conn->addColumn(
    $installer->getTable('salesrule/rule'),
    'tracking_code',
    [
        'type'    => Varien_Db_Ddl_Table::TYPE_TEXT,
        'comment' => 'Used for identification promotional Rule on ERP.'
    ]
);

$tables = [
    $installer->getTable('sales/quote_item'),
    $installer->getTable('sales/order_item')
];


$installer->endSetup();
