<?php
/**
 * BSeller Platform
 *
 * Não edite este arquivo caso você pretenda atualizar este módulo futuramente
 * para novas versões.
 *
 * @category  BSeller
 * @package   BSeller_CartRule
 *
 * @copyright Copyright (c) 2016 B2W Digital - BSeller Platform. (http://www.bseller.com.br)
 *
 * @author    Tiago Sampaio <tiago.sampaio@e-smart.com.br>
 */

class BSeller_CartRule_Model_Observer
{

    use BSeller_CartRule_Trait_Data;

    public function customizePromoQuoteForm(Varien_Event_Observer $observer)
    {
        /** @var Varien_Data_Form $form */
        $form = $observer->getEvent()->getData('form');

        if (!$form || !($form instanceof Varien_Data_Form)) {
            return;
        }

        /** @var Varien_Data_Form_Element_Fieldset $fieldset */
        $fieldset = $form->getElement('base_fieldset');

        if (!$fieldset || !($fieldset instanceof Varien_Data_Form_Element_Fieldset)) {
            return;
        }

        $skipSpecialPricedProducts      = 0;
        $trackingCode                   = '';

        /** @var Mage_SalesRule_Model_Rule $currentRule */
        $currentRule = Mage::registry('current_promo_quote_rule');

        if ($currentRule) {
            $skipSpecialPricedProducts  = $currentRule->getData('skip_special_priced_products');
            $trackingCode               = $currentRule->getData('tracking_code');
        }

        try {
            $fieldset->addField('skip_special_priced_products', 'select', array(
                'label'    => $this->_helper()->__('Skip Discount for Special Priced Products'),
                'title'    => $this->_helper()->__('Skip Discount for Special Priced Products'),
                'name'     => 'skip_special_priced_products',
                'note'     => $this->_helper()->__(
                    'By enabling this option the discount will not be applied to products with special price.'
                ),
                'required' => false,
                'value'    => (int) $skipSpecialPricedProducts,
                'options'  => array (
                    1 => $this->_helper()->__('Yes'),
                    0 => $this->_helper()->__('No'),
                ),
            ));

            $fieldset->addField('tracking_code', 'text', [
                'label'    => $this->_helper()->__('Tracking Code'),
                'title'    => $this->_helper()->__('Tracking Code'),
                'name'     => 'tracking_code',
                'note'     => $this->_helper()->__(
                    'Used for identification promotional Rule on ERP.'
                ),
                'required' => false,
                'value'    => $trackingCode
            ]);

        } catch (Exception $e) {
            Mage::logException($e);
        }
    }


    /**
     * @param Varien_Event_Observer $observer
     */
    public function validateResultDiscount(Varien_Event_Observer $observer)
    {
        /**
         * @var Mage_Sales_Model_Quote_Item $item
         * @var Varien_Object               $result
         * @var Mage_SalesRule_Model_Rule   $rule
         */
        $item   = $observer->getEvent()->getData('item');
        $result = $observer->getEvent()->getData('result');
        $rule   = $observer->getEvent()->getData('rule');

        /** @var array $trackingCode */
        $trackingCode = array_merge(
            is_array($item->getData('tracking_code')) ? $item->getData('tracking_code') : [],
            $rule->getData('tracking_code') ? [$rule->getData('tracking_code')] : []
        );

        $item->setData('tracking_code', Zend_Serializer::serialize($trackingCode));

        if (!$this->shouldStopDiscountProcess($item, $rule)) {
            $item->setData('skipped_discount', false);
            return;
        }

        $result->setData('discount_amount',  0);
        $result->setData('base_discount_amount', 0);

        $item->setDiscountPercent(0);
        $item->setDiscountAmount(0);
        $item->setBaseDiscountAmount(0);
        
        $item->setData('skipped_discount', true);
    }


    /**
     * @param Mage_Sales_Model_Quote_Item $item
     * @param Mage_SalesRule_Model_Rule   $rule
     *
     * @return bool
     */
    protected function shouldStopDiscountProcess(Mage_Sales_Model_Quote_Item $item, Mage_SalesRule_Model_Rule $rule)
    {
        $skipSpecialPricedProducts = (bool) $rule->getData('skip_special_priced_products');

        if (!$skipSpecialPricedProducts) {
            return false;
        }

        if (!$this->isSpecialPriceActive($item)) {
            return false;
        }

        return true;
    }


    /**
     * @param Mage_Sales_Model_Quote_Item $item
     *
     * @return bool
     */
    protected function isSpecialPriceActive(Mage_Sales_Model_Quote_Item $item)
    {
        $specialPrice = (float) $item->getProduct()->getSpecialPrice();

        if (!$specialPrice) {
            return false;
        }

        $price = (float) $item->getProduct()->getPrice();

        if ($price <= $specialPrice) {
            return false;
        }

        $fromDate = strtotime($item->getProduct()->getSpecialFromDate());
        $toDate   = strtotime($item->getProduct()->getSpecialToDate());
        $now      = Mage::getSingleton('core/date')->timestamp();

        /**
         * Date validation.
         */
        if ($fromDate && ($now < $fromDate)) {
            return false;
        }

        if ($toDate && ($now > $toDate)) {
            return false;
        }

        return true;
    }

}
