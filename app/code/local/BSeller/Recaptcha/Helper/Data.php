<?php

/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Recaptcha
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Evandro Veloso Gomes <evandro.gomes@e-smart.com.br>
 */
class BSeller_Recaptcha_Helper_Data extends Magecomp_Recaptcha_Helper_Data
{
    const RECAPTCHA_WHITELIST = 'magecomp_captcha/recaptcha_config/whitelist_ips';

    /**
     * @return bool
     */
    public function showOnCustomerAddress()
    {
        if ($this->isEnabled()) {
            return self::getEnabledPage(7);
        }
        return false;
    }

    /**
     * @return bool
     */
    public function showOnCouponCode()
    {
        if ($this->isEnabled()) {
            return self::getEnabledPage(8);
        }
        return false;
    }

    /**
     * @return bool
     */
    public function showRecaptcha()
    {
        if (parent::isEnabled()) {
            return true;
        }
        return false;
    }

    /**
     * @return bool
     */
    public function isWhiteListed()
    {
        $clientIp = Mage::app()->getRequest()->getClientIp();
        $whitelistedIps = Mage::getStoreConfig(self::RECAPTCHA_WHITELIST);
        $whitelistedIpsAsArray = explode("\r\n", $whitelistedIps);

        foreach ($whitelistedIpsAsArray as $whitelistedIp) {
            if ($whitelistedIp == $clientIp) {
                return true;
            }
        }

        return false;
    }

    public function getWhitelistIps()
    {
        $clientIp = Mage::app()->getRequest()->getClientIp();
        return explode("\r\n", Mage::getStoreConfig(self::RECAPTCHA_WHITELIST));
    }
}