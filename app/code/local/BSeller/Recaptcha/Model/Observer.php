<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Recaptcha
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Evandro Veloso Gomes <evandro.gomes@e-smart.com.br>
 */

class BSeller_Recaptcha_Model_Observer extends Magecomp_Recaptcha_Model_Observer
{
    public function newsletterSubmit($observer)
    {
        $message = [];

        try {
            if (Mage::helper('recaptcha/data')->showOnNewsletter()) {
                $g_response = Mage::app()->getRequest()->getParam('g-recaptcha-response');
                if (isset($g_response) && !empty($g_response)) {
                    if (!(Mage::helper('recaptcha')->validateCaptcha($g_response))) {
                        $message['error'] = Mage::helper('recaptcha')->__('Please answer the captcha');
                    }
                } else {
                    $message['error'] = Mage::helper('recaptcha')->__('Please answer the captcha');
                }
            }
        } catch (Exception $e) {
        }

        if (array_key_exists('error', $message)) {
            $convertJson = Zend_Json::encode($message);

            Mage::app()->getResponse()->setHeader('Content-type', 'application/json');
            Mage::app()->getResponse()->setBody($convertJson);
            Mage::app()->getResponse()->sendResponse();

            exit;
        }
    }

    public function reviewSubmit()
    {
        $message = [];

        try {
            if (Mage::helper('recaptcha/data')->showOnReview()) {
                $g_response = Mage::app()->getRequest()->getParam('g-recaptcha-response');
                if (isset($g_response) && !empty($g_response)) {
                    if (!(Mage::helper('recaptcha')->validateCaptcha($g_response))) {
                        $message['error'] = Mage::helper('recaptcha')->__('Please answer the captcha');
                    }
                } else {
                    $message['error'] = Mage::helper('recaptcha')->__('Please answer the captcha');
                }
            }
        } catch (Exception $e) {
        }

        if (array_key_exists('error', $message)) {
            $convertJson = Zend_Json::encode($message);

            Mage::app()->getResponse()->setHeader('Content-type', 'application/json');
            Mage::app()->getResponse()->setBody($convertJson);
            Mage::app()->getResponse()->sendResponse();

            exit;
        }
    }

    public function saveOrder()
    {
        $error = false;

        try {
            if (Mage::helper('recaptcha/data')->showOnOnepage()) {
                $g_response = Mage::app()->getRequest()->getParam('g-recaptcha-response');
                if (isset($g_response) && !empty($g_response)) {
                    if (!(Mage::helper('recaptcha')->validateCaptcha($g_response))) {
                        $error = Mage::helper('recaptcha')->__('Please answer the captcha');
                    }
                } else {
                    $error = Mage::helper('recaptcha')->__('Please answer the captcha');
                }
            }
        } catch (Exception $e) {
        }

        if ($error) {
            Mage::app()->getResponse()->setBody(Mage::helper('core')->jsonEncode(array(
                'success' => false,
                'error' => true,
                'error_messages' => $error
            )));

            Mage::app()->getResponse()->sendResponse();

            exit;
        }
    }

    public function saveCustomerAddress(Varien_Event_Observer $observer)
    {
        $error = false;

        try {
            if (Mage::helper('recaptcha/data')->showOnCustomerAddress()) {
                $g_response = Mage::app()->getRequest()->getParam('g-recaptcha-response');
                if (isset($g_response) && !empty($g_response)) {
                    if (!(Mage::helper('recaptcha')->validateCaptcha($g_response))) {
                        $error = Mage::helper('recaptcha')->__('Please answer the captcha');
                    }
                } else {
                    $error = Mage::helper('recaptcha')->__('Please answer the captcha');
                }
            }
        } catch (Exception $e) {
        }

        if ($error) {

            $addressId = Mage::app()->getRequest()->getParam('id');

            Mage::getSingleton('core/session')->setAddressFormData(Mage::app()->getRequest()->getPost())
                ->addError($error);

            Mage::app()->getResponse()->setRedirect(Mage::getUrl('*/*/edit', array('id' => $addressId)));

            exit;
        }
    }

    public function couponPost(Varien_Event_Observer $observer)
    {
        $error = false;

        try {
            if (Mage::helper('recaptcha/data')->showOnCouponCode()) {
                $g_response = Mage::app()->getRequest()->getParam('g-recaptcha-response');
                if (isset($g_response) && !empty($g_response)) {
                    if (!(Mage::helper('recaptcha')->validateCaptcha($g_response))) {
                        $error = Mage::helper('recaptcha')->__('Please answer the captcha');
                    }
                } else {
                    $error = Mage::helper('recaptcha')->__('Please answer the captcha');
                }
            }
        } catch (Exception $e) {
        }

        if ($error) {

            $addressId = Mage::app()->getRequest()->getParam('id');

            Mage::getSingleton('core/session')->setAddressFormData(Mage::app()->getRequest()->getPost())
                ->addError($error);

            Mage::app()->getResponse()->setRedirect(Mage::getUrl('*/*', array('id' => $addressId)));

            exit;
        }
    }
}