<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Recaptcha
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Evandro Veloso Gomes <evandro.gomes@e-smart.com.br>
 */

class BSeller_Recaptcha_Model_Source_Pagessource
{
    public function toOptionArray()
    {
        return [
            // Don't use this option - Reserved - ['value'=>'0','label'=>'Contact Form'],
            ['value' => '1', 'label' => 'Review Form'],
            ['value' => '2', 'label' => 'Register Form'],
            ['value' => '3', 'label' => 'Onepage Checkout'],
            ['value' => '4', 'label' => 'Customer Login'],
            ['value' => '5', 'label' => 'Newsletter'],
            // Don't use this option - Reserved - ['value'=>'6','label'=>'Admin Login']
            ['value' => '7', 'label' => 'Customer Address'],
            ['value' => '8', 'label' => 'Coupon Code'],
        ];
    }
}