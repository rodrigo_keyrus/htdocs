<?php
/**
 * B Seller Platform | B2W Digital
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_CmsImage
 *
 * @copyright Copyright (c) 2017 B2W Digital - B Seller Platform (http://www.bseller.com.br)
 *
 * @author    Tiago Daniel <tiago.daniel@e-smart.com.br>
*/ 
class BSeller_CmsImage_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function formatBytes($bytes, $precision = 2)
    {
        $exp = floor(log($bytes, 1024)) | 0;
        return round($bytes / (pow(1024, $exp)), $precision);
    }

    public function checkAllowedSize()
    {
        $size           = $_FILES['image']['size'];
        $imgSize        = $this->formatBytes($size);
        $maximumSize    = Mage::getStoreConfig('bseller_cmsimage/settings/maximumsize');
        if ($imgSize > $maximumSize && $maximumSize != 0) {
            return false;
        }
        return  true;
    }
}