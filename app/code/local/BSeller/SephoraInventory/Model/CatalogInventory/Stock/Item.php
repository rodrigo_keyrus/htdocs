<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   Default (Template) Project
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Anderson Cardoso <anderson.cardoso@e-smart.com.br>
 */ 
class BSeller_SephoraInventory_Model_CatalogInventory_Stock_Item extends Mage_CatalogInventory_Model_Stock_Item {

    /**
     * Product validation
     * @return BSeller_SephoraInventory_Model_Product|false|Mage_Core_Model_Abstract
     */
    public function productValidation()
    {
        /** @var BSeller_SephoraInventory_Model_Product*/
        return Mage::getModel('bseller_sephorainventory/product');
    }

    /**
     * Product validation
     * @return BSeller_SephoraInventory_Model_Product|false|Mage_Core_Model_Abstract
     */
    public function reservedValidate()
    {
        /** @var BSeller_SephoraInventory_Model_Product*/
        return Mage::getModel('bseller_sephorainventory/reserved_reserved');
    }

    /**
     * This method returns the available quantity in stock of an item
     *
     * @return int
     */
    public function getQty()
    {
        return ((float) $this->getData('qty')) - ((float) $this->reservedValidate()
                ->getReservedQty($this->getProductId()));
    }

    /**
     * Check if is possible subtract value from item qty
     *
     * @return bool
     */
    public function canSubtractQty()
    {
        return false;
    }
    /**
     * Retrieve Stock Availability
     *
     * @return bool|int
     */
    public function getIsInStock()
    {
        $isInStock = (bool) $this->_getData('is_in_stock');
        $hasStock  = (bool) ($this->getQty() > 0);

        /** @var Mage_Catalog_Model_Product $product */
        $product = $this->getProduct();

        if (!$product) {
            return parent::getIsInStock();
        }

        if ($product->getTypeId() == Mage_Catalog_Model_Product_Type_Grouped::TYPE_CODE) {
            return (bool) $isInStock;
        }

        if ($product->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_BUNDLE) {
            return (bool) $isInStock;
        }

        return ($isInStock && $hasStock);
    }
    /**
     * Retrieve stock qty whether product is composite or no
     *
     * @return float
     */
    public function getStockQty()
    {
        if (!$this->hasStockQty()) {
            $this->setStockQty(0);  // prevent possible recursive loop
            $product = $this->_productInstance;
            if (!$product || !$product->isComposite()) {
                $stockQty = $this->getQty();
            } else {
                $stockQty = null;
                $productsByGroups = $product->getTypeInstance(true)->getProductsToPurchaseByReqGroups($product);
                foreach ($productsByGroups as $productsInGroup) {
                    $qty = 0;
                    foreach ($productsInGroup as $childProduct) {
                        if ($childProduct->hasStockItem()) {
                            $qty += $childProduct->getStockItem()->getStockQty();
                        }
                    }
                    if (is_null($stockQty) || $qty < $stockQty) {
                        $stockQty = $qty;
                    }
                }
            }
            $stockQty = (float) $stockQty;
            if ($stockQty < 0 || !$this->getManageStock()
                || !$this->getIsInStock() || ($product && !$product->isSaleable())
            ) {
                $stockQty = 0;
            }
            $this->setStockQty($stockQty);
        }
        return $this->getData('stock_qty');
    }

    /**
     * Reset model data
     * @return Mage_CatalogInventory_Model_Stock_Item
     */
    public function reset()
    {
        if ($this->_productInstance) {
            $this->_productInstance = null;
        }
        return $this;
    }

    /**
     * Set whether index events should be processed immediately
     *
     * @param bool $process
     * @return Mage_CatalogInventory_Model_Stock_Item
     */
    public function setProcessIndexEvents($process = true)
    {
        $this->_processIndexEvents = $process;
        return $this;
    }

    /**
     * Callback function which called after transaction commit in resource model
     *
     * @return Mage_CatalogInventory_Model_Stock_Item
     */
    public function afterCommitCallback()
    {
        parent::afterCommitCallback();

        /** @var \Mage_Index_Model_Indexer $indexer */
        $indexer = Mage::getSingleton('index/indexer');

        if ($this->_processIndexEvents) {
            $indexer->processEntityAction($this, self::ENTITY, Mage_Index_Model_Event::TYPE_SAVE);
        } else {
            $indexer->logEvent($this, self::ENTITY, Mage_Index_Model_Event::TYPE_SAVE);
        }
        return $this;
    }
}