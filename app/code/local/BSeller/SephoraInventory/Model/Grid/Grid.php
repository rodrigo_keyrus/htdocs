<?php

/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   sephora
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Anderson Cardoso <anderson.cardoso@e-smart.com.br>
 */

class BSeller_SephoraInventory_Model_Grid_Grid extends  Mage_CatalogInventory_Model_Observer
{
     /**
     * Method used to add new stock information to the admin product grid
     * It's an edit version of the code written by ProxiBlue, which can be accessed here:
     * @param Varien_Event_Observer $observer
     */
    public function addStockInfoToGrid(Varien_Event_Observer $observer)
    {
        /** @var Mage_Adminhtml_Block_Catalog_Product_Grid */
        $grid = $observer->getData('block');

        if ($grid instanceof Mage_Adminhtml_Block_Catalog_Product_Grid) {
            if (false === $this->_validateRequestAction()) {
                return;
            }

            try {
                /** @var Varien_Db_Select $reservedStockSelect */
                $reservedStockSelect = $this->getReadConnection()
                    ->select()
                    ->from(['sd' => 'esmart_axstock_detail'], [
                        'sd.product_id',
                        'reserved_qty' => new Zend_Db_Expr('SUM(sd.reserved_qty) + si.admin_reserved_qty')
                    ])
                    ->joinInner(['si' => 'cataloginventory_stock_item'], 'sd.product_id = si.product_id', [])
                    ->where('sd.is_released_reserve = 0')
                    ->group('sd.product_id');
                $grid->getCollection()
                    ->getSelect()
                    ->joinLeft(
                        ['detail_stock' => $reservedStockSelect],
                        'detail_stock.product_id = e.entity_id',
                        ['detail_stock.reserved_qty']
                    );

                $grid->getCollection()
                    ->getSelect()
                    ->joinLeft(
                        ['bsp' => 'bseller_position'],
                        'e.entity_id = bsp.product_id',
                        ['quantity']
                    );

                $grid->getCollection()->joinAttribute(
                    'seller_order_quantity',
                    'catalog_product/seller_order_quantity',
                    'entity_id',
                    null,
                    'left'
                );
                $grid->getCollection()->addAttributeToSelect('seller_order_quantity');

                /** @var Zend_Db_Expr $expression */
                $expression = new Zend_Db_Expr('(`at_qty`.`qty` - IFNULL(`detail_stock`.`reserved_qty`, 0))');
                $grid->getCollection()
                    ->getSelect()
                    ->columns(['available_qty' => $expression]);

                /** @var BSeller_SephoraInventory_Model_Grid_Grid $widgetHelper */
                $widgetHelper = Mage::helper('bseller_sephorainventory/widget_grid');
                /**
                 * Let's add the 'reserved_qty' column.
                 */
                $grid->addColumnAfter('reserved_qty', [
                    'header'                   => Mage::helper('bseller_sephorainventory')->__('Reserved Qty'),
                    'index'                    => 'reserved_qty',
                    'type'                     => 'number',
                    'width'                    => '100px',
                    'align'                    => 'right',
                    'renderer'                 => 'BSeller_SephoraInventory_Block_Adminhtml_Grid_Column_Renderer_Stock',
                    'filter_condition_callback'=> [$widgetHelper, 'filterReservedQty'],
                ], 'qty');
                /**
                 * Let's add the 'total_qty' column.
                 */
                $grid->addColumnAfter('total_qty', [
                    'header'                   => Mage::helper('bseller_sephorainventory')->__('Available Qty'),
                    'index'                    => 'available_qty',
                    'type'                     => 'number',
                    'width'                    => '100px',
                    'align'                    => 'right',
                    'renderer'                 => 'BSeller_SephoraInventory_Block_Adminhtml_Grid_Column_Renderer_Stock',
                    'filter_condition_callback'=> [$widgetHelper, 'filterAvailableQty'],
                ], 'reserved_qty');
                /**
                 * Let's add the 'total_qty' column.
                 */
                $grid->addColumnAfter('quantity', [
                    'header'                   => Mage::helper('bseller_sephorainventory')->__('Qty Bseller'),
                    'index'                    => 'quantity',
                    'type'                     => 'number',
                    'width'                    => '100px',
                    'align'                    => 'right',
                    'renderer'                 => 'BSeller_SephoraInventory_Block_Adminhtml_Grid_Column_Renderer_Stock',
                    'filter_condition_callback'=> [$widgetHelper, 'filterBsellerQty'],
                ], 'qty');

                $grid->addColumnAfter('seller_order_quantity', [
                    'header'                   => Mage::helper('bseller_sephorainventory')->__('Seller Order Quantity'),
                    'index'                    => 'seller_order_quantity',
                    'type'                     => 'number',
                    'width'                    => '100px',
                    'align'                    => 'right'
                ], 'quantity');

                $grid->sortColumnsByOrder();

                /** @var string $filter */
                $filter = $grid->getParam($grid->getVarNameFilter(), null);

                $grid->getCollection()->getSelect()->setPart('where', []);
                $grid->getCollection()->clear();
                if (is_string($filter)) {
                    $data = $grid->helper('adminhtml')->prepareFilterString($filter);
                    $widgetHelper->setFilterValues($data, $grid);
                } else {
                    if ($filter && is_array($filter)) {
                        $widgetHelper->setFilterValues($filter, $grid);
                    }
                }
                $grid->getCollection()->load();
            } catch (Exception $e) {
                Mage::log($e, '', 'product_grid');
            }
        }
    }

    /**
     * @return bool
     */
    protected function _validateRequestAction()
    {
        /** @var Mage_Adminhtml_Catalog_ProductController $controller */
        $controller = Mage::app()->getFrontController()->getData('action');

        if (!$controller || !($controller instanceof Mage_Adminhtml_Catalog_ProductController)) {
            return false;
        }

        switch ($controller->getRequest()
            ->getActionName()) {
            case 'index':
            case 'grid':
                return true;
        }

        return false;
    }

    /**
     * @return Magento_Db_Adapter_Pdo_Mysql
     */
    protected function getReadConnection()
    {
        return $this->getCoreResource()->getConnection('read');
    }


    /**
     * @return Mage_Core_Model_Resource
     */
    protected function getCoreResource()
    {
        return Mage::getModel('core/resource');
    }


}