<?php

/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_SephoraInventory_
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Anderson Cardoso <anderson.cardoso@e-smart.com.br>
 */

class BSeller_SephoraInventory_Model_Synchronize_Synchronize extends Mage_Core_Model_Abstract
{
    /**
     * Request Method
     * @var array
     */
    public $requestMethod = Zend_Http_Client::GET;

    /**
     * Service path order api
     * @var string
     */
    public $servicePath = 'api/itens';

    /**
     * Http Client
     *
     * @var Zend_Http_Client
     */
    protected $client;

    /**
     * Http Client
     *
     * @var Zend_Http_Client
     */
    protected $codSap;

    /**
     * Http Client
     *
     * @var Zend_Http_Client
     */
    protected $response;

    /**
     * Http Client
     *
     * @var Zend_Http_Client
     */
    public $productId;

    /**
     * @var
     */
    public $stockERP;

    /**
     * @var Mage_CatalogInventory_Model_Stock_Item
     */
    public $currentStock;

    /**
     * Query params for append in endpoint url
     *
     * @var array
     */
    public $queryParams = array();
    public $pathParams = array();

    /**
     * Connection timeout Zend Http Client (seconds)
     *
     * @const int
     */
    const HTTP_CLIENT_TIMEOUT = 30;

    /**
     * @return BSeller_SephoraInventory_Helper_Data|Mage_Core_Helper_Abstract
     */
    public function getHelper()
    {
        return Mage::helper('bseller_sephorainventory');
    }

    /**
     * @param $productId
     */
    public function synchronizeBseller($productId)
    {
        $this->productId = $productId;
        $product = Mage::getModel('catalog/product')->load($productId);
        $this->codSap = $product->getData('sku');
        $this->init();
    }

    /**
     * @return $this
     */
    public function init()
    {
        $header = $this->getHeader();
        try {
            $this->setClient($this->getFullEndpoint(), array('timeout' => self::HTTP_CLIENT_TIMEOUT));
            $this->client->setHeaders($header);
            $this->client->setUri($this->getFullEndpoint());
            $response = $this->client->request(Zend_Http_Client::GET);
            $this->response = Mage::helper('core')->jsonDecode($response->getBody(), Zend_Json::TYPE_OBJECT);
            Mage::log($response->getBody(), '', 'InventoryBsellerResponse.log');
            $this->validatePosition();
            $this->updateInventoryMagento();
        } catch (Exception $exception) {
            Mage::log($exception->getMessage(), '', 'SynchronizeBseller.log');
        }
        return $this;
    }

    /**
     * Validation for update or Create new position
     */
    public function validatePosition()
    {
        $position = Mage::getModel('bseller_sephorainventory/inventory_position')
            ->getCollection()
            ->addFieldToFilter('product_id', $this->productId)
            ->getData();

        if (!$position) {
            $this->createPosition();
        }

        $this->updatePosition();
    }

    /**
     * @return $this|void
     */
    public function createPosition()
    {
        $transactionSave = Mage::getModel('core/resource_transaction');
        $position = Mage::getModel('bseller_sephorainventory/inventory_position');
        $position->setProductId($this->productId);
        $position->setCreatedAt(date('Y-m-d H:i:s'));
        Mage::log($this->response->estoqueEstabelecimento[0]->quantidade, '', 'InventoryBsellerCreate.log');
        $position->setQuantity($this->response->estoqueEstabelecimento[0]->quantidade);
        $transactionSave->addObject($position);

        try {
            $transactionSave->save();
        } catch (Exception $e) {
            Mage::log($e->getMessage(), '', 'CreatePositionError.log');
        }
    }

    /**
     * Update current position
     */
    public function updatePosition()
    {
        $position = Mage::getModel('bseller_sephorainventory/inventory_position')
            ->getCollection()
            ->addFieldToFilter('product_id', $this->productId);

        foreach ($position as $item) {
            $item->setProductId($this->productId);
            $item->setCreatedAt(now());
            $item->setQuantity($this->response->estoqueEstabelecimento[0]->quantidade);
            $item->save();
        }
    }


    /**
     * @return array
     */
    public function getHeader()
    {
        return $header = array(
            'Content-Type' => 'application/json',
            'X-Auth-Token' => $this->getHelper()->getToken()
        );
    }

    /**
     * Get complete endpoint with the service path
     *
     * @return string
     */
    protected function getFullEndpoint()
    {
        $url = $this->getHelper()->getEndpoint(). $this->servicePath;

        /**
         * Prepare params
         */
        $pathParams  = $this->preparePathParams();
        $queryParams = $this->prepareQueryParams();

        /**
         * Create full endpoint
         */
        $url = $url . $pathParams . $queryParams;

        return $url;
    }

    /**
     * Prepare path params for create full endpoint
     *
     * @return string
     */
    protected function preparePathParams()
    {
        $this->getPathParams();
        $path = '/';

        foreach ($this->pathParams as $param) {
            $path .= $param;
        }

        return $path;
    }

    /**
     * @return string
     */
    public function prepareQueryParams()
    {
        $this->getQueryParams();
        if (!count($this->queryParams)) {
            return '';
        }

        $path = '?';

        foreach ($this->queryParams as $key => $value) {
            $path .= $key . '=' . $value;

            if ($value !== end($this->queryParams)) {
                $path .= '&';
            }
        }

        return $path;
    }

    /**
     * @return array
     */
    public function getQueryParams()
    {
        return $this->queryParams = [
            'tipoInterface' => 'SITE'
        ];
    }

    /**
     * @return array
     */
    public function getPathParams()
    {
        return $this->pathParams = [
            $this->codSap,
            '/estoque'
        ];
    }

    /**
     * Return a restful client according to URI.
     *
     * @param string $uri
     * @param array  $options
     *
     * @return $this
     * @throws Mage_Core_Exception
     */
    protected function setClient($uri, $options = array())
    {
        if (!$uri) {
            Mage::throwException($this->helper->__('Invalid URI for create new HTTP Client'));
        }

        $this->client = new Zend_Http_Client($uri, $options);

        return $this;
    }


    /**
     * update Stock Magento with response Bseller for single item
     * @return $this
     */
    public function updateInventoryMagento()
    {
        $this->stockERP     = $this->response;
        $productId          = Mage::helper('bseller_sephorainventory')
            ->getProductIdByAttribute('cod_sap', $this->stockERP->codigoItem);

        $this->currentStock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($productId);

        try {
            $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($productId);
            if ($this->currentStock->getId() > 0 and $stockItem->getManageStock()) {
                $qty = $this->response->estoqueEstabelecimento[0]->quantidade;
                $this->currentStock->setQty($qty);
                $this->currentStock->setIsInStock((int)($qty > 0));
                $this->currentStock->save();
            }
        } catch (Exception $e) {
            Mage::log($e->getMessage(), '', 'SynchronizeInventoryError.log');
        }


        return $this;
    }

}
