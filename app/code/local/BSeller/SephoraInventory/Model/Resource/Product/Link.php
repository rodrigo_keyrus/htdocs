<?php

class BSeller_SephoraInventory_Model_Resource_Product_Link extends Mage_Catalog_Model_Resource_Product_Link
{
    public function getChildrenIdsAvailables($parentId)
    {
        $adapter     = $this->_getReadAdapter();
        $statusId = $this->getAttributeIdByCode('status');

        $bind        = array(
            ':product_id'    => (int)$parentId,
            ':attribute_id'  => $statusId,
            ':type_id'       => Mage_Catalog_Model_Product_Link::LINK_TYPE_GROUPED
        );

        $select = $adapter->select()
            ->from(
                array('link' => $this->getMainTable()), array('linked_product_id')
            )
            ->join(
                array('children' => $this->getTable('catalog/product')),
                'children.entity_id = link.linked_product_id 
                        AND children.required_options = 0',
                array()
            )
            ->join(
                array('product_int' => $this->getTable(['catalog/product', 'int'])),
                'children.entity_id = product_int.entity_id
                        AND product_int.attribute_id = :attribute_id
                        AND product_int.value = 1',
                array()
            )
            ->join(
                array('product_stock' => $this->getTable('cataloginventory/stock_item')),
                'children.entity_id = product_stock.product_id
                        AND product_stock.qty > 0
                        AND product_stock.is_in_stock = 1',
                array()
            )
            ->joinLeft(
                array('ax' => $this->getTable('bseller_sephorainventory/inventory_detail')),
                    'children.entity_id = ax.product_id',
                array()
            )
            ->where('link.product_id = :product_id')
            ->where('link.link_type_id = :type_id')
            ->group('ax.product_id')
            ->columns(
                new Zend_Db_Expr ('
                    (product_stock.qty - COALESCE(SUM(ax.reserved_qty), 0) ) AS total_qty'
                )
            )
            ->having('total_qty > 0');

        $result = $adapter->fetchAll($select, $bind);

        if(count($result) > 0){
            return true;
        }

        return false;
    }

    public function getAttributeIdByCode($attributeCode)
    {
        $attribute = Mage::getModel('eav/config')->getAttribute('catalog_product', $attributeCode);
        if ($attribute){
            return $attribute->getId();
        }

        return false;
    }
}