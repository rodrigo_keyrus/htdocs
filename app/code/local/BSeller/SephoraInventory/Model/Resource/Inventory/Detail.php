<?php

/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   sephora
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Anderson Cardoso <anderson.cardoso@e-smart.com.br>
 */

class BSeller_SephoraInventory_Model_Resource_Inventory_Detail extends Mage_Core_Model_Resource_Db_Abstract
{
    public function _construct()
    {
        $this->_init('bseller_sephorainventory/inventory_detail', 'detail_id');
    }

    /**
     * Function used to load detail object from database
     * @param $detailModel
     * @param $orderId
     * @param $productId
     * @return $this
     */
    public function loadByOrderAndProduct($detailModel, $orderId, $productId)
    {
        $adapter = $this->_getReadAdapter();
        $bind    = array(
            'load_order_id' => $orderId,
            'load_product_id' => $productId
        );
        $select  = $adapter->select()
            ->from($this->getMainTable(), array($this->getIdFieldName()))
            ->where('order_id = :load_order_id')
            ->where('product_id = :load_product_id');

        $detailId = $adapter->fetchOne($select, $bind);

        if ($detailId) {
            $this->load($detailModel, $detailId);
        } else {
            $detailModel->setData([]);
        }

        return $this;
    }

    /**
     * Function used to
     * @param int $productId
     * @return int
     */
    public function getReservedQtySum($productId)
    {
        $adapter = $this->_getReadAdapter();
        $bind    = array(
            'load_product_id' => $productId,
        );

        $select  = $adapter->select()
            ->from($this->getMainTable(), array('SUM(reserved_qty)'))
            ->where('product_id = :load_product_id');

        return $adapter->fetchOne($select, $bind);
    }
}
