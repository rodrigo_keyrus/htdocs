<?php

/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   sephora
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Anderson Cardoso <anderson.cardoso@e-smart.com.br>
 */

class BSeller_SephoraInventory_Model_DetailStock_Detailstock extends BSeller_SephoraInventory_Model_Schema
{
    const INTEGRATED = 1;


    /**
     * Delete order stock reservation items for canceled orders
     *
     * @param Mage_Sales_Model_Order $order
     */
    public function releaseReservedItems($order)
    {
        if (!$order || !$order->getId()) {
            return;
        }

        /** @var Mage_Sales_Model_Entity_Order_Item_Collection $item */
        $items   = $order->getAllItems();
        $orderId = $order->getId();

        /** @var Mage_Sales_Model_Order_Item $item */
        foreach ($items as $item) {
            /** @var BSeller_SephoraInventory_Model_Inventory_Detail $stockDetail */
            $stockDetail = Mage::getModel('bseller_sephorainventory/inventory_detail');
            $stockDetail->loadByOrderAndProduct($orderId, $item->getProduct()->getId());

            if ($stockDetail->getId()) {
                $stockDetail->delete();
            }
        }
    }


    /**
     * Method used to release stock from specific order
     * and put the order in the queue
     * @param Mage_Sales_Model_Order $order
     * @return $this     *
     * @throws Exception
     */
    public function setOrderStockAsIntegrated($order)
    {
        if (!$order || !$order->getId()) {
            return $this;
        }

        /** @var array $items */
        $items   = $order->getAllItems();
        $orderId = $order->getId();

        foreach ($items as $item) {
            /** @var BSeller_SephoraInventory_Model_Inventory_Detail $stockDetail */
            $stockDetail = Mage::getModel('bseller_sephorainventory/inventory_detail');
            $stockDetail->loadByOrderAndProduct($orderId, $item->getProduct()->getId());

            if ($stockDetail->getId()) {
                $stockDetail->setIsIntegrated(1);
                $stockDetail->save();
            }
        }

        try {
            $this->processQueue();
        } catch (Exception $e) {
            Mage::helper('bseller_sephorainventory')->log($e->getMessage(), 'processQueue');
        }

        return $this;
    }

    /**
     * Method used to consume the stock queue
     * @return $this
     */
    public function processQueue()
    {
        /* @var $collection BSeller_SephoraInventory_Model_Resource_Inventory_Detail_Collection */
        $collection = Mage::getModel('bseller_sephorainventory/inventory_detail')
            ->getCollection()
            ->addFieldToFilter('is_integrated', self::INTEGRATED);
        foreach ($collection->getData() as $item) {
            try {
                $itemQty = $item['reserved_qty'];
                $itemId  = $item['product_id'];
                $order   = $item['order_id'];
                $this->UpdateStock($itemQty, $itemId, $order);
            } catch (Exception $e) {
                Mage::helper('bseller_sephorainventory')->log($e->getMessage(), 'processQueue');
            }
        }

        return $this;
    }

    /**
     * Update Stock with items of the orders success integrated
     * @param $itemQty
     * @param $itemId
     */
    public function updateStock($itemQty, $itemId, $order)
    {
        try {
            /**  @var Mage_CatalogInventory_Model_Stock_Item  */
            $stockItem     = Mage::getModel('cataloginventory/stock_item')->loadByProduct($itemId);
            $this->stockItem =  $stockItem->getData();
            $this->updateQty = $itemQty;
            $stockItem->addData($this->prepareSchema());
            $stockItem->save();
            $this->clearQueue($itemId, $order);
        } catch (Exception $e) {
            Mage::helper('bseller_sephorainventory')
                ->log('No possible update stock for item' . $itemId . $e->getMessage(), 'UpdateStock');
        }
    }

    /**
     * @param $item
     * @param $order
     * @throws Exception
     */
    public function clearQueue($item, $order)
    {
        $this->reindexStock();
        $queue = Mage::getModel('bseller_sephorainventory/inventory_detail');
        $queue->loadByOrderAndProduct($order, $item);

        if ($queue->getId()) {
            $queue->delete();
        }
    }

    /**
     * reindex stock
     */
    public function reindexStock()
    {
        $process = Mage::getModel('index/indexer')->getProcessByCode('cataloginventory_stock');
        $process->reindexAll();
    }
}
