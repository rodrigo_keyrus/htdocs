<?php

/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_SephoraInventory
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Anderson Cardoso <anderson.cardoso@e-smart.com.br>
 */

class BSeller_SephoraInventory_Model_Reserved_Reserved extends Mage_Core_Model_Abstract
{

    /**
     * This method returns the reserved quantity of an item considering order and admin reservations.
     *
     * @return float|bool
     */
    public function getReservedQty($product)
    {
        if (!$product) {
            return false;
        }

        if (!$this->getData('calculated_reserved_qty')) {
            /** @var BSeller_SephoraInventory_Model_Resource_Inventory_Detail $detail */
            $detail        = Mage::getResourceModel('bseller_sephorainventory/inventory_detail');
            $orderReserved = (float) $detail
                ->getReservedQtySum($product);
            $adminReserved = (float) $this->getData('admin_reserved_qty');

            $this->setData('calculated_reserved_qty', (float) ($orderReserved + $adminReserved));
        }

        return (float) $this->getData('calculated_reserved_qty');
    }

}