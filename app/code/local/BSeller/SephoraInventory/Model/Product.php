<?php

/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_SephoraInventory
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Anderson Cardoso <anderson.cardoso@e-smart.com.br>
 */

class BSeller_SephoraInventory_Model_Product extends Mage_Core_Model_Abstract
{
    /**
     * @param Mage_Catalog_Model_Product $product
     * @return bool
     */
    public function isGrouped(Mage_Catalog_Model_Product $product = null)
    {
        if ($product && $product->getTypeId() == Mage_Catalog_Model_Product_Type_Grouped::TYPE_CODE) {
            return true;
        }

        return false;
    }

    /**
     * Method used to reserve stock from specific order
     * @param $order
     * @return $this
     * @throws Exception
     */
    public function reserveOrderStock($order)
    {
        $items           = $order->getAllItems();
        $productIds      = [];
        $transactionSave = Mage::getModel('core/resource_transaction');

        foreach ($items as $item) {
            /** @var BSeller_SephoraInventory_Model_Inventory_Detail $stockDetail */
            $stockDetail = Mage::getModel('bseller_sephorainventory/detail');
            $stockDetail->setProductId($item->getProduct()->getId());
            $stockDetail->setOrderId($order->getId());
            $stockDetail->setReservedQty($item->getQtyOrdered());
            $stockDetail->setCreatedAt(date('Y-m-d H:i:s'));
            $stockDetail->setIsReleasedReserve(0);
            $transactionSave->addObject($stockDetail);
            $productIds[] = $item->getProduct()->getId();
        }

        try {
            $transactionSave->save();
            $stockItems = Mage::getModel('cataloginventory/stock_item')
                ->getCollection()
                ->addFieldToFilter('product_id', $productIds);
            $stockItems->save();
        } catch (Exception $e) {
            Mage::helper('bseller_sephorainventory')->log($e->getMessage(), 'add_reserve.log');
        }

        return $this;
    }

}
