<?php

/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_SephoraInventory
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Anderson Cardoso <anderson.cardoso@e-smart.com.br>
 */

class BSeller_SephoraInventory_Model_Observer extends Mage_CatalogInventory_Model_Observer
{

    /**
     * Product validation
     * @return BSeller_SephoraInventory_Model_Product|false|Mage_Core_Model_Abstract
     */
    public function productValidation()
    {
        /** @var BSeller_SephoraInventory_Model_Product*/
        return Mage::getModel('bseller_sephorainventory/product');
    }

    /**
     * Table detail manipulation
     * @return BSeller_SephoraInventory_Model_DetailStock_Detailstock|false|Mage_Core_Model_Abstract
     */
    public function detailStock()
    {
        /**
         * @var BSeller_SephoraInventory_Model_DetailStock_Detailstock
         */
        return Mage::getModel('bseller_sephorainventory/detailStock_detailstock');
    }

    /**
     * Event used to add new stock reserves when customer place an order
     * @param Varien_Event_Observer $observer
     * @throws Exception
     * @throws bool
     */
    public function reserveStock(Varien_Event_Observer $observer)
    {
        /** @var Mage_Sales_Model_Order $order */
        $order = $observer->getData('order');

        if ($order && $order->getId()) {
            $this->reserveOrderStock($order);
        }
    }

    /**
     * Event used to release stock reservation when order is canceled
     * @param Varien_Event_Observer $observer
     * @throws Exception
     */
    public function releaseStock(Varien_Event_Observer $observer)
    {
        /** @var Mage_Sales_Model_Order $order */
        $order = $observer->getData('order');

        if ($order && $order->getId()) {
            $this->detailStock()->releaseReservedItems($order);
        }
    }

    /**
     * Method used to reserve stock from specific order
     * @param $order
     * @return $this
     */
    public function reserveOrderStock($order)
    {
        $items           = $order->getAllItems();
        $productIds      = [];
        $transactionSave = Mage::getModel('core/resource_transaction');

        foreach ($items as $item) {
            /** @var BSeller_SephoraInventory_Model_Inventory_Detail*/
            $stockDetail = Mage::getModel('bseller_sephorainventory/inventory_detail');
            $stockDetail->setProductId($item->getProduct()->getId());
            $stockDetail->setOrderId($order->getId());
            $stockDetail->setReservedQty($item->getQtyOrdered());
            $stockDetail->setCreatedAt(date('Y-m-d H:i:s'));
            $stockDetail->setIsReleasedReserve(0);
            $transactionSave->addObject($stockDetail);
            $productIds[] = $item->getProduct()->getId();
        }

        try {
            $transactionSave->save();
            $stockItems = Mage::getModel('cataloginventory/stock_item')
                ->getCollection()
                ->addFieldToFilter('product_id', $productIds);
            $stockItems->save();
        } catch (Exception $e) {
            Mage::helper('bseller_sephorainventory')->log($e->getMessage(), 'add_reserve');
        }

        return $this;
    }

    /**
     * Considering the Stock Availability on product's isSalable() method.
     * @param Varien_Event_Observer $observer
     * @return $this
     */
    public function checkStockSaleableFlag(Varien_Event_Observer $observer)
    {
        /**
         * @var Mage_Catalog_Model_Product $product
         * @var Varien_Object              $salable
         */
        $product = $observer->getData('product');
        $salable = $observer->getData('salable');

        if (true === $this->productValidation()->isGrouped($product))
        {
            if (!$this->productIsSaleable($product)) {
                return $salable->setIsSalable(false);
            }

            return;
        }

        if (!$product || !$product->getId())
        {
            return $this;
        }

        $stockItem = $product->getStockItem();

        if (!$stockItem || !$stockItem->getIsInStock()) {
            $salable->setIsSalable(false);
        }
    }

    /**
     * Prepare stock item data for save
     * @param Mage_CatalogInventory_Model_Stock_Item $item
     * @param Mage_Catalog_Model_Product   $product
     * @return Mage_CatalogInventory_Model_Observer
     */
    protected function _prepareItemForSave($item, $product)
    {
        $item->addData($product->getStockData())
            ->setProduct($product)
            ->setProductId($product->getId())
            ->setStockId($item->getStockId());
        if (!is_null($product->getData('stock_data/min_qty'))
            && is_null($product->getData('stock_data/use_config_min_qty'))
        ) {
            $item->setData('use_config_min_qty', false);
        }

        if (!is_null($product->getData('stock_data/min_sale_qty'))
            && is_null($product->getData('stock_data/use_config_min_sale_qty'))
        ) {
            $item->setData('use_config_min_sale_qty', false);
        }

        if (!is_null($product->getData('stock_data/max_sale_qty'))
            && is_null($product->getData('stock_data/use_config_max_sale_qty'))
        ) {
            $item->setData('use_config_max_sale_qty', false);
        }

        if (!is_null($product->getData('stock_data/backorders'))
            && is_null($product->getData('stock_data/use_config_backorders'))
        ) {
            $item->setData('use_config_backorders', false);
        }

        if (!is_null($product->getData('stock_data/notify_stock_qty'))
            && is_null($product->getData('stock_data/use_config_notify_stock_qty'))
        ) {
            $item->setData('use_config_notify_stock_qty', false);
        }

        $originalQty = $product->getData('stock_data/original_inventory_qty');
        if (strlen($originalQty) > 0) {
            $item->setQtyCorrection($item->getData('qty') - $originalQty);
        }

        if (!is_null($product->getData('stock_data/enable_qty_increments'))
            && is_null($product->getData('stock_data/use_config_enable_qty_inc'))
        ) {
            $item->setData('use_config_enable_qty_inc', false);
        }

        if (!is_null($product->getData('stock_data/qty_increments'))
            && is_null($product->getData('stock_data/use_config_qty_increments'))
        ) {
            $item->setData('use_config_qty_increments', false);
        }

        return $this;
    }


    /**
     * @return bool
     */
    protected function _validateRequestAction()
    {
        /** @var Mage_Adminhtml_Catalog_ProductController $controller */
        $controller = Mage::app()->getFrontController()->getData('action');

        if (!$controller || !($controller instanceof Mage_Adminhtml_Catalog_ProductController)) {
            return false;
        }

        switch ($controller->getRequest()->getActionName()) {
            case 'index':
            case 'grid':
                return true;
        }

        return false;
    }

    /**
     * validate if product is saleable
     * @param $product
     * @return bool
     */
    protected function productIsSaleable($product)
    {
        return  Mage::getResourceModel('bseller_sephorainventory/product_link')
            ->getChildrenIdsAvailables($product->getId());
    }

    /**
     * Set status as integrated and put on the queue for low inventory,
     * if order was integrated with success
     * @param Varien_Event_Observer $observer
     */
    public function releaseBsellerOrderItemsStock(Varien_Event_Observer $observer)
    {
        /**  @var Mage_Sales_Model_Order $order */
        $order = $observer->getOrder();
        if ($order->getErpStatus() == BSellerERP_Order_Helper_Status::SUCCESS) {
            /** @var BSeller_SephoraInventory_Model_DetailStock_Detailstock $detail */
            $detail = Mage::getModel('bseller_sephorainventory/detailStock_detailstock');
            try {
                $detail->setOrderStockAsIntegrated($order);
            } catch (Exception $e) {
                Mage::helper('bseller_sephorainventory')->log($e->getMessage(), 'integratedOrder');
            }
        }
    }

    /**
     * @param Varien_Event_Observer $observer
     */
    public function addIntegrationButtonOrder(Varien_Event_Observer $observer)
    {
        $_block = $observer->getBlock();
        $_type = $_block->getType();
        if ($_type == 'adminhtml/catalog_product_edit') {
            $_deleteButton = $_block->getChild('delete_button');
            $_block->setChild('product_view_button', $_block->getLayout()
                ->createBlock('atwix_pvf/adminhtml_widget_button'));
            $_deleteButton->setBeforeHtml($_block->getChild('product_view_button')->toHtml());
        }
    }

    /**
     * Update column with inventory position from BsellerErp
     * @param Varien_Event_Observer $observer
     * @throws Mage_Core_Model_Store_Exception
     */
    public function productsFromGrid(Varien_Event_Observer $observer)
    {
        if (!Mage::app()->getStore()->isAdmin()) {
            return exit;
        }

        $productCollection = $observer->getEvent()->getCollection()->getData();

        foreach ($productCollection as $item) {
            $product = Mage::getModel('catalog/product')->load($item['entity_id']);

            if (!$product->isGrouped()) {
                Mage::getModel('bseller_sephorainventory/synchronize_synchronize')
                    ->synchronizeBseller($item['entity_id']);

            }

        }
    }
}
