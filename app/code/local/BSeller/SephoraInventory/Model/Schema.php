<?php

/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_SephoraInventory
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Anderson Cardoso <anderson.cardoso@e-smart.com.br>
 */

class BSeller_SephoraInventory_Model_Schema extends Mage_Core_Model_Abstract
{
    /**
     * Set class_tax_id in product
     *
     * @const int
     */
    const DEFAULT_CLASS_TAX_ID = 0;

    /**
     * Set attribute stock qty in product
     *
     * @const int
     */
    const DEFAULT_STOCK_QTY = 0;

    /**
     * @var Mage_CatalogInventory_Model_Stock_Item
     */
    public $currentStock;

    /**
     * @var object
     */
    public $stockItem;

    /**
     * @var object
     */
    public $updateQty;

    /**
     * Returns helper object
     *
     * @var BSellerERP_Stock_Helper_Data
     */
    protected $helper;

    /**
     * Load helper data
     *
     * @var string
     */
    public $currentHelper = 'stock';

    /**
     * Prepare schema and returns for adding in stock instance
     *
     * @return array
     */
    protected function prepareSchema()
    {

        $this->setGeneralData();

        return $this->getSchema();
    }

    /**
     * Returns schema
     *
     * @return array
     */
    protected function getSchema()
    {
        return $this->toArray();
    }

    /**
     * Setting general data for schema
     *
     * @return $this
     */
    protected function setGeneralData()
    {
        $available = Mage_CatalogInventory_Model_Stock::STOCK_OUT_OF_STOCK;

        if ($this->getQty()) {
            $available = Mage_CatalogInventory_Model_Stock::STOCK_IN_STOCK;
        }

        $this->addData(
            array(
                'qty' => $this->getQty(),
                'is_in_stock' => $available
            )
        );

        return $this;
    }

    /**
     * Get stock qty
     *
     * @return int
     */
    protected function getQty()
    {
        $qty = $this->stockItem['qty'] - $this->updateQty ;
        if ($qty < 0) {
            $qty = 0;
        }

        return $qty;
    }
}