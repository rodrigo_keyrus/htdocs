<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_SephoraInventory
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Anderson Cardoso <anderson.cardoso@e-smart.com.br>
 */

class BSeller_SephoraInventory_Adminhtml_SynchronizeController extends Mage_Adminhtml_Controller_Action
{
    /**
     * @return Mage_Adminhtml_Controller_Action
     */
    public function indexAction()
    {
        if (!Mage::helper('bseller_sephorainventory')->isActive()) {
            return $this;
        }

        $product_id = $this->getRequest()->getParam('product_id');

        if (!$product_id) {
            $this->_getSession()->addError($this->_getHelper()->__('It was not possible to synchronize the product'));
            return $this->_redirectReferer();
        }

        Mage::getModel('bseller_sephorainventory/synchronize_synchronize')
            ->synchronizeBseller($product_id);

        $this->_getSession()->addSuccess($this->_getHelper()->__('Order sent for synchronization'));

        return $this->_redirectReferer();
    }

}