<?php

/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   Default (Template) Project
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Anderson Cardoso <anderson.cardoso@e-smart.com.br>
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$tableName = $installer->getTable('bseller_sephorainventory/inventory_history');
if (!$installer->getConnection()->isTableExists($tableName)) {
    try {
        $newTable = $installer->getConnection()->newTable($tableName)
            ->addColumn(
                'id',
                Varien_Db_Ddl_Table::TYPE_INTEGER,
                null,
                [
                    'unsigned' => true,
                    'nullable' => false,
                    'primary'  => true,
                    'identity' => true,
                ],
                'Primary key - ID'
            )
            ->addColumn(
                'order_id',
                Varien_Db_Ddl_Table::TYPE_INTEGER,
                null,
                [
                    'unsigned' => true,
                    'nullable' => false,
                ],
                'Order ID'
            )
            ->addColumn(
                'created_at',
                Varien_Db_Ddl_Table::TYPE_DATETIME,
                null,
                [
                'nullable' => false,
                    ],
                'Date of history'
            )
            ->addColumn(
                'item_id',
                Varien_Db_Ddl_Table::TYPE_INTEGER,
                null,
                [
                    'unsigned' => true,
                    'nullable' => false,
                ],
                'item id'
            )
            ->addColumn(
                'quantity',
                Varien_Db_Ddl_Table::TYPE_VARCHAR,
                null,
                [
                    'unsigned' => true,
                    'nullable' => false,
                ],
                'original Value before low inventory'
            )
            ->addColumn(
                'original_qty',
                Varien_Db_Ddl_Table::TYPE_VARCHAR,
                null,
                [
                    'unsigned' => true,
                    'nullable' => false,
                ],
                'original Value before low inventory'
            )
            ->addColumn(
                'final_qty',
                Varien_Db_Ddl_Table::TYPE_VARCHAR,
                300,
                [
                    'unsigned' => true,
                    'nullable' => false,
                ],
                'Value after low inventory'
            );
        $installer->getConnection()->createTable($newTable);
    } catch (Zend_Db_Exception $e) {
        Mage::log($e->getMessage(), '', 'bsellerInventoryQueueHistory.log');

    }
}

$tableName = $installer->getTable('bseller_sephorainventory/inventory_position');
if (!$installer->getConnection()->isTableExists($tableName)) {
    try {
        $newTable = $installer->getConnection()->newTable($tableName)
            ->addColumn(
                'id',
                Varien_Db_Ddl_Table::TYPE_INTEGER,
                null,
                [
                    'unsigned' => true,
                    'nullable' => false,
                    'primary'  => true,
                    'identity' => true,
                ],
                'Primary key - ID'
            )
            ->addColumn(
                'product_id',
                Varien_Db_Ddl_Table::TYPE_INTEGER,
                null,
                [
                    'unsigned' => true,
                    'nullable' => false,
                ],
                'product ID'
            )
            ->addColumn(
                'created_at',
                Varien_Db_Ddl_Table::TYPE_DATETIME,
                null,
                [
                    'nullable' => false,
                ],
                'Date of history'
            )

            ->addColumn(
                'quantity',
                Varien_Db_Ddl_Table::TYPE_INTEGER,
                null,
                [
                    'unsigned' => true,
                    'nullable' => false,
                ],
                'original Value before low inventory'
            );
        $installer->getConnection()->createTable($newTable);
    } catch (Zend_Db_Exception $e) {
        Mage::log($e->getMessage(), '', 'bsellerInventoryQueuePosition.log');

    }
}
$installer->endSetup();