<?php

/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   Default (Template) Project
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Anderson Cardoso <anderson.cardoso@e-smart.com.br>
 */

$installer = $this;
$connection = $installer->getConnection();
 
$installer->startSetup();
 
$installer->getConnection()
          ->addColumn(
              $installer->getTable('bseller_sephorainventory/inventory_detail'),
              'bsellerPosition',
              array(
                  'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
                  'nullable' => true,
                  'default' => null,
                  'comment' => 'position Stock bseller'
              )
          );
 
$installer->endSetup();