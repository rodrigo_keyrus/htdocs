<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_SephoraInventory
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Anderson Cardoso <anderson.cardoso@e-smart.com.br>
 */

class BSeller_SephoraInventory_Helper_Data extends Mage_Core_Helper_Abstract {

    /** Method used to easily generate logs
     * @param $message
     * @param $method
     */
    public function log($message, $method)
    {
        Mage::log($message, null, 'sephoraInventory_'.$method.'.log');
    }

    /**
     * if module is active
     * @return bool
     */
    public function isActive()
    {
        return (bool)Mage::getStoreConfig('bseller_sephorainventory/settings/active');
    }

    /**
     * validate if service is active
     * @return bool
     */
    public function serviceIsActive()
    {
        return (bool)Mage::getStoreConfig('bseller_sephorainventory/settings/service_mode');
    }

    /**
     * return token
     * @return bool
     */
    public function getToken()
    {
        return Mage::getStoreConfig('bseller_sephorainventory/settings/token');
    }

    /**
     * return entpoint
     * @return bool
     */
    public function getEndpoint()
    {
        return Mage::getStoreConfig('bseller_sephorainventory/settings/endpoint');
    }


    /**
     * @param $attributeName
     * @param $value
     * @return string
     */
    public function getProductIdByAttribute($attributeName, $value)
    {
        $adapter = Mage::getSingleton('core/resource')->getConnection('core_read');
        $attribute = Mage::getSingleton('eav/config')
            ->getCollectionAttribute(Mage_Catalog_Model_Product::ENTITY, $attributeName);
        $attributeId = $attribute->getId();
        $backendTable = $attribute->getBackendTable();
        $select = $adapter->select()
            ->from($backendTable, 'entity_id')
            ->where("value = '{$value}' and attribute_id = {$attributeId} and entity_type_id = 4");
        return $adapter->fetchOne($select);
    }
}