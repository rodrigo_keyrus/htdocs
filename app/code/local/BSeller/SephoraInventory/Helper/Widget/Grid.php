<?php

/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   sephora
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Anderson Cardoso <anderson.cardoso@e-smart.com.br>
 */

class BSeller_SephoraInventory_Helper_Widget_Grid extends Mage_Core_Helper_Abstract
{

    /**
     * Method used when user filter the Reserved Qty column
     * @param Mage_Catalog_Model_Resource_Product_Collection $collection
     * @param Mage_Adminhtml_Block_Widget_Grid_Column $column
     * @return $this
     */
    public function filterReservedQty($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return $this;
        }

        $collection->getSelect()
            ->where("detail_stock.reserved_qty >= ?", $value['from']);
        $collection->getSelect()
            ->where("detail_stock.reserved_qty <= ?", $value['to']);
        return $this;
    }
    
   /**
     * Method used when user filter the Available Qty column
     * @param Mage_Catalog_Model_Resource_Product_Collection $collection
     * @param Mage_Adminhtml_Block_Widget_Grid_Column $column
     * @return $this
     */
    public function filterAvailableQty($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return $this;
        }

        $collection->getSelect()
            ->where("(`at_qty`.`qty` - IFNULL(`detail_stock`.`reserved_qty`, 0)) >= ?", $value['from']);
        $collection->getSelect()
            ->where("(`at_qty`.`qty` - IFNULL(`detail_stock`.`reserved_qty`, 0)) <= ?", $value['to']);

        return $this;
    }

    /**
     * Method used when user filter the Available Qty column
     * @param Mage_Catalog_Model_Resource_Product_Collection $collection
     * @param Mage_Adminhtml_Block_Widget_Grid_Column $column
     * @return $this
     */
    public function filterBsellerQty($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return $this;
        }

        $collection->getSelect()
            ->where("bsp.quantity >= ?", $value['from']);
        $collection->getSelect()
            ->where("bsp.quantity <= ?", $value['to']);
        return $this;
    }

    /**
     * Method used when user filter the Available Qty column
     * @param Mage_Catalog_Model_Resource_Product_Collection $collection
     * @param Mage_Adminhtml_Block_Widget_Grid_Column $column
     * @return $this
     */
    public function filterAvailableBseller($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return $this;
        }

        $collection->getSelect()
            ->where("(`at_qty`.`qty` - IFNULL(`detail_stock`.`reserved_qty`, 0)) >= ?", $value['from']);
        $collection->getSelect()
            ->where("(`at_qty`.`qty` - IFNULL(`detail_stock`.`reserved_qty`, 0)) <= ?", $value['to']);

        return $this;
    }

    public function setFilterValues($data, $grid)
    {
        foreach ($grid->getColumns() as $columnId => $column) {
            if (isset($data[$columnId]) && (!empty($data[$columnId]) || strlen($data[$columnId]) > 0)
                && $column->getFilter()
            ) {
                $column->getFilter()->setValue($data[$columnId]);
                $this->_addColumnFilterToCollection($column, $grid);
            }
        }
        return $this;
    }

    /**
     * Replicated method from Mage_Adminhtml_Block_Widget_Grid to a helper
     * Used to reset grid filters from specific grid
     * @param Mage_Adminhtml_Block_Widget_Grid_Column $column
     * @param Mage_Adminhtml_Block_Widget_Grid $grid
     * @return $this
     */
    protected function _addColumnFilterToCollection($column, $grid)
    {
        if ($grid->getCollection()) {
            $field = ($column->getFilterIndex()) ? $column->getFilterIndex() : $column->getIndex();
            if ($column->getFilterConditionCallback()) {
                call_user_func($column->getFilterConditionCallback(), $grid->getCollection(), $column);
            } else {
                $cond = $column->getFilter()->getCondition();
                if ($field && isset($cond)) {
                    $grid->getCollection()->addFieldToFilter($field, $cond);
                }
            }
        }

        return $this;
    }
}