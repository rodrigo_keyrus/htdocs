<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   sephora
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Anderson Cardoso <anderson.cardoso@e-smart.com.br>
 */

class BSeller_SephoraInventory_Block_Adminhtml_Widget_Button extends Mage_Adminhtml_Block_Widget_Button
{
    const CONTROLLER = 'synchronize';

    const ACTION = 'index';
    /**
     * @var Mage_Catalog_Model_Product Product instance
     */
    private $_product;

    /**
     * Block construct, setting data for button, getting current product
     */
    protected function _construct()
    {
        $this->_product = Mage::registry('current_product');
        parent::_construct();
        $message = Mage::helper('catalog')->__('Are you sure you want to do this?');
        $url     = Mage::helper('adminhtml')->getUrl("/synchronize/index", array(
            "key"       => $this->initKey(),
            "product_id"=> $this->_product->getData('entity_id')
        ));
        $this->setData(array(
            'label'     => Mage::helper('catalog')->__('Synchronize'),
            'onclick'   => "confirmSetLocation('{$message}', '{$url}')",

            'title' => (!$this->_isVisible())?
                Mage::helper('catalog')->__('Product is not visible on frontend'):
                Mage::helper('catalog')->__('Synchronize')
        ));
    }

    public function initKey()
    {
        return Mage::getSingleton('adminhtml/url')
            ->getSecretKey(self::CONTROLLER, self::ACTION);
    }

    /**
     * Checking product visibility
     *
     * @return bool
     */
    private function _isVisible()
    {
        return $this->_product->isVisibleInCatalog() && $this->_product->isVisibleInSiteVisibility();
    }
}