<?php

/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   sephora
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Anderson Cardoso <anderson.cardoso@e-smart.com.br>
 */

class BSeller_SephoraInventory_Block_Adminhtml_Catalog_Product_Edit_Tab_Inventory
    extends Mage_Adminhtml_Block_Catalog_Product_Edit_Tab_Inventory
{

    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('bseller/sephorainventory/catalog/product/tab/inventory.phtml');
    }

    /**
     * @param $field
     *
     * @return mixed
     */
    public function getFieldValue($field)
    {
        if ($field == 'qty' && $this->getStockItem()) {
            return $this->getStockItem()->getData('qty');
        }

        return parent::getFieldValue($field);
    }

    /**
     * @return string
     */
    public function getStockRequestUrl()
    {
        return $this->getUrl('*/catalog_product_stock/request', [
            'product_id' => $this->getProduct()->getId()
        ]);
    }
}