<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   sephora
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Anderson Cardoso <anderson.cardoso@e-smart.com.br>
 */

class BSeller_SephoraInventory_Block_Adminhtml_Catalog_Product_Edit_Edit extends Mage_Adminhtml_Block_Catalog_Product_Edit
{

    /**
     * @var Mage_Catalog_Model_Product Product instance
     */
    private $_product;

    /**
     * @return $this|Mage_Core_Block_Abstract
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $this->_product = $this->getProduct();
        $this->setChild('view_on_front', $this->getLayout()->createBlock('adminhtml/widget_button')
            ->setData(array(
                'label' => Mage::helper('catalog')->__('View Product Page'),
                'onclick'   => 'window.open(\''.Mage::getModel('core/url')->getUrl() . $this->_product->getUrlPath() .'\')',
                'disabled'  => !$this->_isVisible(),
                'title' => (!$this->_isVisible())?
                        Mage::helper('catalog')->__('Product is not visible on frontend'):
                        Mage::helper('catalog')->__('View Product Page')
                ))
        );

        return $this;
    }

    /**
     * Checking product visibility
     *
     * @return bool
     */
    private function _isVisible()
    {
        return $this->_product->isVisibleInCatalog() && $this->_product->isVisibleInSiteVisibility();
    }
}