<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Chaordic
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_Chaordic_Helper_Customer extends BSeller_Chaordic_Helper_Data
{
    /**
     * Is subscribed key
     *
     * @const string
     */
    const IS_SUBSCRIBED_KEY = 'bseller_chaordic_is_subscribed';

    /**
     * Gender options
     *
     * @var array
     */
    protected $_genders = [
        1 => 'M',
        2 => 'F',
        3 => 'O'
    ];

    /**
     * Return tag data
     *
     * @return array
     */
    public function getInfo()
    {
        if (!Mage::helper('bseller_chaordic')->isTagActive()) {
            return [];
        }

        if (!$this->getSession()->isLoggedIn()) {
            return [];
        }

        $data['user'] = [
            'id'                   => md5($this->getCustomer()->getData('email')),
            'name'                 => $this->getCustomer()->getName(),
            'email'                => $this->getCustomer()->getData('email'),
            'language'             => $this->getLang(),
            'allow_mail_marketing' => $this->getAllowMailMarketing(),
            'birthday'             => $this->getCustomerDob(),
            'gender'               => $this->getCustomerGender(),
            'document_id'          => $this->getCustomer()->getData('cpf')
        ];

        return $data;
    }

    /**
     * Return customer session
     *
     * @return Mage_Customer_Model_Session
     */
    protected function getSession()
    {
        return Mage::getSingleton('customer/session');
    }

    /**
     * Return customer object
     *
     * @return Mage_Customer_Model_Customer
     */
    protected function getCustomer()
    {
        return $this->getSession()->getCustomer();
    }

    /**
     * Return store language
     *
     * @return string
     */
    protected function getLang()
    {
        return str_replace('_', '-', Mage::app()->getLocale()->getLocaleCode());
    }

    /**
     * Return if mail marketing is enabled
     *
     * @return bool
     */
    protected function getAllowMailMarketing()
    {
        if ($this->getSession()->hasData(self::IS_SUBSCRIBED_KEY)) {
            return $this->getSession()->getData(self::IS_SUBSCRIBED_KEY);
        }

        /** @var Mage_Newsletter_Model_Subscriber $subscriber */
        $subscriber = Mage::getModel('newsletter/subscriber');
        $subscriber->loadByCustomer($this->getCustomer());

        $this->getSession()->setData(self::IS_SUBSCRIBED_KEY, $subscriber->isSubscribed());

        return $this->getSession()->getData(self::IS_SUBSCRIBED_KEY);
    }

    /**
     * Return dob at internal format
     *
     * @return string
     */
    protected function getCustomerDob()
    {
        $date = Mage::app()->getLocale()->date(
            $this->getCustomer()->getData('dob'),
            Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT),
            null,
            false
        );

        return $date->toString(Varien_Date::DATE_INTERNAL_FORMAT);
    }

    /**
     * Return gender
     *
     * @return string
     */
    protected function getCustomerGender()
    {
        $key = $this->getCustomer()->getData('gender');

        return (isset($this->_genders[$key])) ? $this->_genders[$key] : end($this->_genders);
    }
}
