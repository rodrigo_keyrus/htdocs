<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Chaordic
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_Chaordic_Helper_Data extends BSeller_Core_Helper_Data
{
    /**
     * Return if tag is active
     *
     * @return bool
     */
    public function isTagActive()
    {
        return Mage::getStoreConfigFlag('bseller_chaordic/tag/active');
    }
}
