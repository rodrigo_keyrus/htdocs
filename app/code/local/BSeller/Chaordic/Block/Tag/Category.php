<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Chaordic
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_Chaordic_Block_Tag_Category extends BSeller_Core_Block_Template
{
    /**
     * Categories page type
     *
     * @const string
     */
    const CATEGORY_PAGE_TYPE    = 'category';
    const SUBCATEGORY_PAGE_TYPE = 'subcategory';

    /**
     * Page breadcrumbs object
     *
     * @var Mage_Page_Block_Html_Breadcrumbs
     */
    protected $_breadcrumbs;

    /**
     * Initialize constructor
     * Set template
     */
    public function _construct()
    {
        parent::_construct();

        $this->setTemplate('bseller/chaordic/tag/category.phtml');
    }

    /**
     * Check if the current page is a subcategory
     *
     * @return bool
     */
    public function isSubcategory()
    {
        return (count($this->getBreadcrumbs()) > 1);
    }

    /**
     * Return page breadcrumbs
     *
     * @return array
     */
    public function getBreadcrumbs()
    {
        $info = $this->_breadcrumbs->getCacheKeyInfo();

        if (!isset($info['crumbs'])) {
            return [];
        }

        $crumbs = (array) unserialize(base64_decode($info['crumbs']));
        $result = [];
        foreach ($crumbs as $key => $crumb) {
            if (strpos($key, 'category') !== false) {
                $result[] = str_replace('category', '', $key);
                continue;
            }

            unset($crumbs[$key]);
        }

        return $result;
    }

    /**
     * Return block content
     *
     * @return string
     */
    protected function _toHtml()
    {
        if (!$this->_breadcrumbs = $this->getLayout()->getBlock('breadcrumbs')) {
            return '';
        }

        return parent::_toHtml();
    }
}
