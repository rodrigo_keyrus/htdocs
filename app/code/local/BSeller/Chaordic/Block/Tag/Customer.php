<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Chaordic
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_Chaordic_Block_Tag_Customer extends BSeller_Core_Block_Template
{
    /**
     * Customer info key
     *
     * @cont string
     */
    const CUSTOMER_INFO_KEY = 'customer_info';

    /**
     * Initialize constructor
     * Set template
     */
    public function _construct()
    {
        parent::_construct();

        $this->setTemplate('bseller/chaordic/tag/customer.phtml');
    }

    /**
     * Return customer info
     *
     * @return array
     */
    public function getInfo()
    {
        if (!$this->hasData(self::CUSTOMER_INFO_KEY)) {
            $this->setData(self::CUSTOMER_INFO_KEY, Mage::helper('bseller_chaordic/customer')->getInfo());
        }

        return $this->getData(self::CUSTOMER_INFO_KEY);
    }

    /**
     * Return block content
     *
     * @return string
     */
    protected function _toHtml()
    {
        if (!count($this->getInfo())) {
            return '';
        }

        return parent::_toHtml();
    }
}
