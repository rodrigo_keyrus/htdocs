<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Chaordic
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_Chaordic_Block_Tag_Product extends BSeller_Core_Block_Template
{
    /**
     * Status options
     *
     * @const string
     */
    const AVAILABLE_STATUS   = 'available';
    const UNAVAILABLE_STATUS = 'unavailable';

    /**
     * Initialize constructor
     * Set template
     */
    public function _construct()
    {
        parent::_construct();

        $this->setTemplate('bseller/chaordic/tag/product.phtml');
    }

    /**
     * Return product object
     *
     * @return Mage_Catalog_Model_Product
     */
    public function getProduct()
    {
        return Mage::registry('current_product');
    }

    /**
     * Return product status
     *
     * @param null|Mage_Catalog_Model_Product $product
     * @return string
     */
    public function getStatus($product = null)
    {
        if (is_null($product)) {
            $product = $this->getProduct();
        }

        return ($product->isSalable()) ? self::AVAILABLE_STATUS : self::UNAVAILABLE_STATUS;
    }

    /**
     * Return block content
     *
     * @return string
     */
    protected function _toHtml()
    {
        if (!$this->getProduct() instanceof Mage_Catalog_Model_Product && !$this->getProduct()->getId()) {
            return '';
        }

        return parent::_toHtml();
    }
}
