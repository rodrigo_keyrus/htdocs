<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Chaordic
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_Chaordic_Block_Tag_Transaction extends BSeller_Core_Block_Template
{
    /**
     * Initialize constructor
     * Set template
     */
    public function _construct()
    {
        parent::_construct();

        $this->setTemplate('bseller/chaordic/tag/transaction.phtml');
    }

    /**
     * Return all visible items in order
     *
     * @return array
     */
    public function getItems()
    {
        return $this->getOrder()->getAllVisibleItems();
    }

    /**
     * Return sales order
     *
     * @return Mage_Sales_Model_Order
     */
    public function getOrder()
    {
        if (Mage::registry('current_order') instanceof Mage_Sales_Model_Order) {
            return Mage::registry('current_order');
        }

        /** @var int $orderId */
        $orderId = Mage::getSingleton('checkout/session')->getData('last_order_id');

        /** @var Mage_Sales_Model_Order $order */
        $order = Mage::getModel('sales/order')->load($orderId);

        return $order;
    }

    /**
     * Return block content
     *
     * @return string
     */
    protected function _toHtml()
    {
        if (!$this->getOrder()->getId()) {
            return '';
        }

        return parent::_toHtml();
    }

    /**
     * Return json of items from order
     *
     * @return string
     */

    public function getChaordicOrderItems()
    {
        $data = array();

        foreach ($this->getItems() as $i => $item)
        {
            /**
             * Business Rule
             *
             * Only sends items that have tax_treatment equal to 1
             */
            if((int)$item->getProduct()->getTaxTreatment() !== 1)
            {
                continue;
            }

            $data[$i]['pid'] = $item->getProduct()->getData('ax_parent_code');
            $data[$i]['sku'] = $item->getSku();
            $data[$i]['price'] = (float) number_format($item->getPrice(), 2, '.', '');
            $data[$i]['quantity'] = (int) $item->getQtyOrdered();
        }

        return json_encode($data);
    }
}