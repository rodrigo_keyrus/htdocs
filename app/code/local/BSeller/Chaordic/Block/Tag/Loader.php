<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Chaordic
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_Chaordic_Block_Tag_Loader extends BSeller_Chaordic_Block_Tag
{
    /**
     * Initialize constructor
     * Set template
     */
    public function _construct()
    {
        parent::_construct();

        $this->setTemplate('bseller/chaordic/tag/loader.phtml');
    }

    /**
     * Return API url
     *
     * @return string
     */
    public function getApiUrl()
    {
        return Mage::getStoreConfig('bseller_chaordic/tag/api_url');
    }

    /**
     * Return API key
     *
     * @return string
     */
    public function getApiKey()
    {
        return Mage::getStoreConfig('bseller_chaordic/tag/api_key');
    }
}
