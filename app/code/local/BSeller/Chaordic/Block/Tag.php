<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Chaordic
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_Chaordic_Block_Tag extends BSeller_Core_Block_Template
{
    /**
     * Initialize constructor
     * Set template
     */
    public function _construct()
    {
        parent::_construct();

        $this->setTemplate('bseller/chaordic/tag.phtml');
    }

    /**
     * Return page type
     *
     * @return string
     */
    public function getPageType()
    {
        return $this->_getData('page_type');
    }

    /**
     * Return block content
     *
     * @return string
     */
    protected function _toHtml()
    {
        if (!Mage::helper('bseller_chaordic')->isTagActive()) {
            return '';
        }

        return parent::_toHtml();
    }
}
