<?php
/**
 * B Seller Platform | B2W Digital
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_QuoteCleaner
 *
 * @copyright Copyright (c) 2017 B2W Digital - B Seller Platform (http://www.bseller.com.br)
 *
 * @author    Tiago Daniel <tiago.daniel@e-smart.com.br>
*/ 
class BSeller_QuoteCleaner_Helper_Data extends Mage_Core_Helper_Abstract
{

}