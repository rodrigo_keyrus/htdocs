<?php
/**
 * B Seller Platform | B2W Digital
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_QuoteCleaner
 *
 * @copyright Copyright (c) 2017 B2W Digital - B Seller Platform (http://www.bseller.com.br)
 *
 * @author    Tiago Daniel <tiago.daniel@e-smart.com.br>
 */

class BSeller_QuoteCleaner_Model_Clean
{
    /**
     * Clean old quote entries.
     * This method will be called via a Magento crontab task.
     *
     * @param void
     * @return void
     */
    public function clean($olderThan)
    {
        /* @var $writeConnection Varien_Db_Adapter_Pdo_Mysql */
        $writeConnection = Mage::getSingleton('core/resource')->getConnection('core_write');
        $tableName = Mage::getSingleton('core/resource')->getTableName('sales/quote');
        $tableName = $writeConnection->quoteIdentifier($tableName, true);

        $sql = sprintf(
            'DELETE FROM %s WHERE updated_at < DATE_SUB(Now(), INTERVAL %s DAY)',
            $tableName,
            $olderThan
        );

        try{
            $writeConnection->query($sql);
        }catch (Mage_Exception $e) {
            Mage::logException($e);
        }
        return $this;
    }
}