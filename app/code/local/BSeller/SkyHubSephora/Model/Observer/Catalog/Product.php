<?php

/**
 * BIT Tools Platform | B2W - Companhia Digital
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category  BSeller
 * @package   BSeller
 *
 * @copyright Copyright (c) 2018 B2W Digital - BIT Tools Platform.
 *
 * @author    Julio Reis <julio.reis@b2wdigital.com>
 */
class BSeller_SkyHubSephora_Model_Observer_Catalog_Product extends BSeller_SkyHub_Model_Observer_Catalog_Product
{

    /**
     * @param Varien_Event_Observer $observer
     * @param Mage_Core_Model_Store $store
     */
    public function prepareIntegrationProduct(Varien_Event_Observer $observer, Mage_Core_Model_Store $store)
    {
        if (!$this->canRun($store->getId())) {
            return;
        }

        /** @var Mage_Catalog_Model_Product $product */
        $product = $observer->getData('product');

        if (!$product) {
            $item = $observer->getData('item');
            if ($item && $item->getId()) {
                $product = Mage::getModel('catalog/product')->load($item->getProductId());
            } else {
                return;
            }
        }

        $this->processIntegrationProduct($product);
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @param bool $forceQueue
     * @return void
     */
    protected function processIntegrationProduct(Mage_Catalog_Model_Product $product)
    {
        $parentIds = Mage::getModel('catalog/product_type_configurable')->getParentIdsByChild($product->getId());

        if (!$parentIds) {
            $parentIds = Mage::getModel('catalog/product_type_grouped')->getParentIdsByChild($product->getId());
        }

        foreach ($parentIds as $id) {
            $this->processIntegrationProduct(Mage::getModel('catalog/product')->load($id));
        }

        if (!$product->getData('is_salable')) {
            if ($product->hasData('is_salable') && $product->getData('is_salable') == null) {
                $product->unsetData('is_salable');
            }
        }

        if (!$this->canIntegrateProduct($product)) {
            return;
        }

        $forceIntegrate = true;
        if ($this->hasActiveIntegrateOnSaveFlag() && $this->hasStockOrPriceUpdate($product)) {
            try {
                /** Create or Update Product */
                $this->catalogProductIntegrator()->createOrUpdate($product);
                $forceIntegrate = false;

                // just to tell other "observers" to don't put the integration flag on these products;
                if ($recentIntegratedIds = Mage::registry('recent_integrated_product')) {
                    Mage::unregister('recent_integrated_product');
                    Mage::register('recent_integrated_product', array_merge($recentIntegratedIds, array($product->getId())));
                } else {
                    Mage::register('recent_integrated_product', array($product->getId()));
                }
                // end
            } catch (Exception $e) {
                Mage::logException($e);
            }
        }

        if ($forceIntegrate) {
            $this->flagEntityIntegrate($product->getId());
        }
    }
}