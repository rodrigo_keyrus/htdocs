<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_CatalogRule
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Guilherme Miguelete <guilherme.miguelete@e-smart.com.br>
 */

class BSeller_Dataflow_Model_Convert_Adapter_ProductAssociation extends Mage_Catalog_Model_Convert_Adapter_Product
{

    /**
     * @param array $data
     * @return $this
     */
    public function saveRow(array $data )
    {

        $groupProductId = Mage::getModel('catalog/product')->getIdBySku($data['familia']);
        $simpleProductId = Mage::getModel('catalog/product')->getIdBySku($data['sku']);

        if (!$groupProductId || !$simpleProductId) {
            return $this;
        }

        $action = Mage::getModel('catalog/resource_product_action');
        $action->updateAttributes(
            [$simpleProductId],
            [
                'ax_parent_code' => $data['familia']
            ],
            1
        );

        $productsLinks = Mage::getModel('catalog/product_link_api');
        $productsLinks->assign(Mage_Catalog_Model_Product_Type::TYPE_GROUPED, $groupProductId, $data['sku']);

    }

}
