<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_CatalogRule
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Guilherme Miguelete <guilherme.miguelete@e-smart.com.br>
 */

class BSeller_Dataflow_Model_Convert_Adapter_ProductImport extends Mage_Catalog_Model_Convert_Adapter_Product
{

    /**
     * @param array $data
     * @return $this
     */
    public function saveRow(array $data )
    {

        if (!is_array($data)) {
            return $this;
        }

        $productExists = Mage::getModel('catalog/product')->getIdBySku($data['sku']);
        if ($productExists) {
            return $this;
        }

        /** @var Mage_Catalog_Model_Product $product */
        $product = Mage::getModel('catalog/product');

        /** set defaults attributes */
        $product->setTypeId('grouped');
        $product->setAttributeSetId(Mage::getModel('catalog/product')->getDefaultAttributeSetId());
        $product->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH);
        $product->setStatus(Mage_Catalog_Model_Product_Status::STATUS_ENABLED);
        $product->setTaxClassId(0);
        $product->setStockData(
            [
                'is_in_stock'             => 1,
                'manage_stock'            => 0,
                'use_config_manage_stock' => 1
            ]
        );

        /** set category ids on product */
        $this->_associateCategories($data['bseller_subcategory'], $product);

        /** Adding Informed Atributes in csv to product data model */
        $product->addData($data);

        try {

            $product->save();
            $productsLinks = Mage::getModel('catalog/product_link_api');

            $childrenProducts = explode(',', $data['associados']);

            foreach ($childrenProducts as $sku) {
                $id = Mage::getModel('catalog/product')->getIdBySku($sku);
                $productsLinks->assign('grouped', $product->getId(), $id);
            }

        } catch (Exception $ex) {
            echo $ex->getMessage();
        }

        return $this;

    }



    /**
     *
     * @param string $bSellerCode
     * @param Mage_Catalog_Model_Product $groupedProduct
     * @return Mage_Catalog_Model_Product
     */
    protected function _associateCategories($bSellerCode, $groupedProduct)
    {
        /** @var Mage_Catalog_Model_Category $subCategory */
        $subCategory = Mage::getModel('catalog/category');
        $object      = $subCategory->loadByAttribute('bseller_code', $bSellerCode);

        if ($object instanceof Mage_Catalog_Model_Abstract) {
            $subCategory = $object;
        }

        $category   = $subCategory->getParentCategory();
        $categoryId = $subCategory->getParentId();

        $departmentId = $category->getParentId();

        $categoryIds = [
            (int) $subCategory->getId(),
            (int) $categoryId,
            (int) $departmentId
        ];

        $groupedProduct->setCategoryIds($categoryIds);
        return $groupedProduct;
    }

}
