<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_PersistentCustomerFirstName
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Evandro Veloso Gomes <evandro.gomes@e-smart.com.br>
 */

class BSeller_PersistentCustomerFirstName_Model_Observer
{
    const SYSTEM_CONFIG_ACTIVE_PATH   = 'bseller_persistentcustomerfirstname/settings/active';
    const SYSTEM_CONFIG_LIFETIME_PATH = 'bseller_persistentcustomerfirstname/settings/lifetime';

    /**
     * Captures the beginning of the session and verifies if the user is logged or it name is known
     *
     * @param Varien_Event_Observer $observer
     */
    public function customerSessionInit(Varien_Event_Observer $observer)
    {
        $active = Mage::getStoreConfig(self::SYSTEM_CONFIG_ACTIVE_PATH);
        if (!$active) {
            return;
        }

        /** @var Mage_Customer_Model_Session $session */
        $session = $observer->getEvent()->getCustomerSession();

        if ($session->isLoggedIn() || ($session->getCustomer()->getData('name'))) {
            return;
        }

        $customerName = Mage::getModel('core/cookie')->get('bseller_persistent_customer_firstname');
        if ($customerName) {
            $session->getCustomer()->setFirstname($customerName);
        }
    }

    /**
     * Captures the login of the user and register its firstname into a cookie
     *
     * @param Varien_Event_Observer $observer
     */
    public function customerLogin(Varien_Event_Observer $observer)
    {
        $active = Mage::getStoreConfig(self::SYSTEM_CONFIG_ACTIVE_PATH);
        if (!$active) {
            return;
        }

        /** @var Mage_Customer_Model_Customer $customer */
        $customer = $observer->getEvent()->getCustomer();

        if (!Mage::getSingleton('customer/session')->isLoggedIn() || (!$customer->getFirstname())) {
            return;
        }

        $lifetime = Mage::getStoreConfig(self::SYSTEM_CONFIG_LIFETIME_PATH);

        Mage::getModel('core/cookie')->set(
            'bseller_persistent_customer_firstname',
            $customer->getFirstname(),
            $lifetime * 24 * 60 * 60
        );
    }


    /**
     * Captures the logout of the user and deletes the cookie with its name
     *
     * @param Varien_Event_Observer $observer
     */
    public function customerLogout(Varien_Event_Observer $observer)
    {
        Mage::getModel('core/cookie')->delete('bseller_persistent_customer_firstname');
    }
}