<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_NewsletterPopUp
 *
 * @copyright Copyright (c) 2017 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Guilherme Miguelete <guilherme.miguelete@e-smart.com.br>
 */

class BSeller_NewsletterPopUp_Helper_Data extends Mage_Core_Helper_Abstract
{

    /**
     * Return store config data
     *
     * @param $key
     * @return string
     */
    public function getStoreConfig($key)
    {
        return Mage::getStoreConfig('bseller_newsletterpopup/settings/' . $key);
    }

    /**
     * Return parse Url
     *
     * @return mixed
     */
    public function getPathUrl()
    {
        $currentUrl = Mage::helper('core/url')->getCurrentUrl();
        $url = Mage::getSingleton('core/url')->parseUrl($currentUrl);
        return $url->getPath();
    }

    /**
     * Checks if url is similar
     *
     * @return bool
     */
    public function checkUrl()
    {
        $configValueSerialized = $this->getStoreConfig('disable_pages');
        $pages = @unserialize($configValueSerialized);

        if (!$pages)
            return true;

        $url = $this->getPathUrl();

        foreach ($pages as $page) {

            if ($ignore = strpos($page['value'], "*")) {
                $url = substr($url, 0, $ignore);
            }
            if ($url == rtrim($page['value'], "*")) {
                return false;
            }
        }

        return true;
    }

}
