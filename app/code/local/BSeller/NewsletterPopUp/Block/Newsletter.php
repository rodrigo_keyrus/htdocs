<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_NewsletterPopUp
 *
 * @copyright Copyright (c) 2017 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Guilherme Miguelete <guilherme.miguelete@e-smart.com.br>
 */

class BSeller_NewsletterPopUp_Block_Newsletter extends BSeller_Core_Block_Template
{
    /**
     * Cache config values
     *
     * @var string
     */
    protected $_cacheKey = 'bseller_gift_showcase';
    protected $_cacheTag = 'bseller_gift_showcase';


    /**
     * @return string
     */
    public function isActive()
    {
        return Mage::helper('bseller_newsletterpopup')->getStoreConfig('active');
    }

    /**
     * @return string
     */
    public function urlDisable()
    {
        return Mage::helper('bseller_newsletterpopup')->checkUrl();
    }

    /**
     * Return static block
     */
    public function getBlock()
    {

        if (!$this->isActive() or !$this->urlDisable()) {
            return "";
        }

        return $this->getLayout()->createBlock('cms/block')->setBlockId(
            Mage::helper('bseller_newsletterpopup')->getStoreConfig('static_block')
        )->toHtml();
    }

}
