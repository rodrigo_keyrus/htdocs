<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_NewsletterPopUp
 *
 * @copyright Copyright (c) 2017 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Guilherme Miguelete <guilherme.miguelete@e-smart.com.br>
 */

class BSeller_NewsletterPopUp_Model_System_Config_Source_Page
{
    /**
     * Return options in select format
     *
     * @return array
     */
    public function toOptionArray()
    {
        $data = array();

        foreach ($this->toArray() as $value => $label) {
            $data[] = array(
                'value' => $value,
                'label' => $label
            );
        }

        return $data;
    }

    /**
     * Return options in key-value format
     *
     * @return array
     */
    public function toArray()
    {
        $data = [];

        $blocks = Mage::getSingleton('cms/block')->getCollection();

        /** @var Mage_Cms_Model_Block $block */
        foreach ($blocks as $block) {
            $data[$block->getIdentifier()] = $this->helper()->__($block->getTitle());
        }

        return $data;
    }

    /**
     * Return data helper object
     *
     * @return BSeller_NewsletterPopUp_Helper_Data
     */
    protected function helper()
    {
        return Mage::helper('bseller_newsletterpopup');
    }
}
