<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_UrlRedirect
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_UrlRedirect_Helper_Data extends BSeller_Core_Helper_Data
{
    /**
     * Redirect types
     *
     * @const string
     */
    const REDIRECT_301 = 'RP';
    const REDIRECT_302 = 'R';

    /**
     * Allowed file extensions
     *
     * @var array
     */
    protected $_allowedExtensions = [
        'csv'
    ];

    /**
     * Columns file map
     *
     * @var array
     */
    protected $_columnsMap = [
        'identifier',
        'target_path'
    ];

    /**
     * Category attributes
     *
     * @var array
     */
    protected $_categoryAttributes = [
        'ax_category',
        'ax_department'
    ];

    /**
     * Return allowed extensions
     *
     * @return string
     */
    public function getAllowedExtensions()
    {
        return $this->_allowedExtensions;
    }

    /**
     * Return destination directory
     *
     * @return string
     */
    public function getDestinationDir()
    {
        return Mage::getBaseDir('upload');
    }

    /**
     * Return columns file map
     *
     * @return array
     */
    public function getColumnsMap()
    {
        return $this->_columnsMap;
    }

    /**
     * Load rewrite object by request path
     *
     * @param string $requestPath
     * @param int $storeId
     * @return Enterprise_UrlRewrite_Model_Redirect
     */
    public function loadByRequestPath($requestPath, $storeId)
    {
        Mage::log(
            'loadByRequestPath: ' . $requestPath,
            null,
            'url_rewrite_product.log'
        );


        /** @var Varien_Db_Adapter_Interface $readAdapter */
        $readAdapter = $this->getReadAdapter();

        $select = $readAdapter->select()
            ->from('enterprise_url_rewrite_redirect')
            ->where('identifier = ?', $requestPath)
            ->where('store_id = ?', $storeId);

        $result = $readAdapter->fetchRow($select);

        /** @var Enterprise_UrlRewrite_Model_Redirect $urlRewrite */
        $urlRewrite = Mage::getModel('enterprise_urlrewrite/redirect');

        if (!empty($result)) {
            $urlRewrite->setData($result);
        }

        return $urlRewrite;
    }

    /**
     * Return available category url
     *
     * @param Mage_Catalog_Model_Product $product
     * @param Mage_Core_Model_Store $store
     * @return null|string
     */
    public function getAvailableCategoryUrl($product, $store)
    {
        foreach ($this->_categoryAttributes as $attributeCode) {
            if (empty($code = $product->getData($attributeCode))) {
                continue;
            }

            /** @var Mage_Catalog_Model_Category $category */
            $category = Mage::getResourceModel('catalog/category_collection')
                ->setStore($store)
                ->addAttributeToFilter('ax_code', $code)
                ->setPageSize('1')
                ->getFirstItem()
                ->setData('store_id', $store->getId())
                ->setData('website_id', $store->getWebsiteId());

            if (!$category->getId() || empty($category->getUrl())) {
                continue;
            }

            return $this->formatUrl($category->getUrl());
        }

        return null;
    }

    /**
     * Validate file fields
     *
     * @param array $line
     * @return bool|string
     */
    public function validate($line)
    {
        $errors = [];

        if (!Zend_Validate::is($line['identifier'], 'NotEmpty')) {
            $errors[] = $this->__('The Identifier field can not be empty.');
        }

        if (!Zend_Validate::is($line['target_path'], 'NotEmpty')) {
            $errors[] = $this->__('The Target Path field can not be empty.');
        }

        if (trim($line['identifier']) == trim($line['target_path'])) {
            $errors[] = $this->__('The Identifier can not be the same as Target Path.');
        }

        if (!count($errors)) {
            return true;
        }

        return implode(nl2br(PHP_EOL), $errors);
    }

    /**
     * Return formatted url
     *
     * @param string $url
     * @return string
     */
    public function formatUrl($url)
    {
        $urlObject     = parse_url($url);
        $formattedUrl  = isset($urlObject['path']) ? $urlObject['path'] : '';
        $formattedUrl .= isset($urlObject['query']) ? '?' . $urlObject['query'] : '';

        $replacedUrl = str_replace(
            [
                Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK),
                Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB),
                'index.php'
            ],
            '',
            $formattedUrl
        );

        $replacedUrl = ltrim($replacedUrl, '/');

        return $replacedUrl;
    }

    /**
     * Return read adapter
     *
     * @return Varien_Db_Adapter_Interface
     */
    protected function getReadAdapter()
    {
        return Mage::getSingleton('core/resource')->getConnection('core_read');
    }
}
