<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_UrlRedirect
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_UrlRedirect_Model_Adminhtml_Observer
{
    use BSeller_Core_Trait_Data;

    /**
     * Add upload button to URL
     *
     * @param Varien_Event_Observer $observer
     * @return $this
     */
    public function addUploadButton(Varien_Event_Observer $observer)
    {
        $block = $observer->getEvent()->getBlock();

        if (!$block instanceof Enterprise_UrlRewrite_Block_Adminhtml_UrlRedirect) {
            return $this;
        }

        $block->addButton(
            'upload_url_redirect',
            [
                'label'   => $this->_helper()->__('Upload Redirect URLs'),
                'onclick' => 'setLocation(\'' . $block->getUrl('*/urlRedirect') . '\')',
                'class'   => 'add'
            ]
        );

        return $this;
    }
}
