<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_UrlRedirect
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

/**
 * Class BSeller_UrlRedirect_Model_Observer
 *
 * @method BSeller_UrlRedirect_Helper_Data _helper()
 */
class BSeller_UrlRedirect_Model_Observer
{
    use BSeller_Core_Trait_Data;

    /**
     * Product status changed to enabled/disabled
     *
     * @param Varien_Event_Observer $observer
     * @return $this
     */
    public function catalogProductSaveAfter(Varien_Event_Observer $observer)
    {
        /** @var Mage_Catalog_Model_Product $product */
        $product = clone $observer->getEvent()->getData('product');

        if (!$product->isGrouped()) {
            return $this;
        }

        /**
         * Check if the saved status is equal to the orig status
         */
        if ($product->getStatus() === $product->getOrigData('status')) {
            return $this;
        }

        try {

            $this->removeUrlRedirectOldProducts($product);

            /** @var Mage_Core_Model_Store $store */
            foreach (Mage::app()->getStores(true) as $store) {

                if ($store->getId() != 0) {
                    $product->setData('store_id', $store->getId());
                    $product->setData('website_id', $store->getWebsiteId());
                }

                /** @var string $productUrl */
                $productUrl = $this->_helper()->formatUrl($product->getProductUrl());

                if (empty($productUrl)) {
                    Mage::throwException($this->_helper()->__('An error has occurred'));
                }

                /** @var Enterprise_UrlRewrite_Model_Redirect $rewrite */
                $rewrite = $this->_helper()->loadByRequestPath(
                    $productUrl,
                    ($store->getId() == 0) ? '1' : $store->getId()
                );

                switch ($product->getStatus()) {
                    case Mage_Catalog_Model_Product_Status::STATUS_ENABLED:
                        Mage::log(
                            'Enable product(' . $store->getName() . '): ' . $product->getSku(),
                            null,
                            'url_rewrite_product.log'
                        );

                        if ($rewrite->getId()) {
                            Mage::log(
                                'Starting remove rewrite...',
                                null,
                                'url_rewrite_product.log'
                            );

                            Mage::log(
                                'url_product: ' . $productUrl,
                                null,
                                'url_rewrite_product.log'
                            );

                            Mage::log(
                                'remove rewrite: ',
                                null,
                                'url_rewrite_product.log'
                            );

                            Mage::log(
                                $rewrite->getData(),
                                null,
                                'url_rewrite_product.log'
                            );

                            $rewrite->delete();
                        }

                        break;
                    case Mage_Catalog_Model_Product_Status::STATUS_DISABLED:
                        Mage::log(
                            'Disable product(' . $store->getName() . '): ' . $product->getSku(),
                            null,
                            'url_rewrite_product.log'
                        );

                        $categoryUrl = $this->_helper()->getAvailableCategoryUrl(
                            $product,
                            ($store->getId() == 0) ? Mage::app()->getStore('1') : $store
                        );

                        if (empty($categoryUrl)) {
                            continue;
                        }

                        $rewrite->addData(
                            [
                                'identifier'  => $productUrl,
                                'target_path' => $categoryUrl,
                                'options'     => BSeller_UrlRedirect_Helper_Data::REDIRECT_301,
                                'description' => $product->getSku(),
                                'store_id'    => ($store->getId() == 0) ? '1' : $store->getId(),
                                'entity_type' => Mage_Core_Model_Url_Rewrite::TYPE_PRODUCT,
                                'product_id'  => $product->getId()
                            ]
                        );

                        $rewrite->save();

                        Mage::log(
                            'Starting create rewrite...',
                            null,
                            'url_rewrite_product.log'
                        );

                        Mage::log(
                            'url_product: ' . $productUrl,
                            null,
                            'url_rewrite_product.log'
                        );

                        Mage::log(
                            'create rewrite: ',
                            null,
                            'url_rewrite_product.log'
                        );

                        Mage::log(
                            $rewrite->getData(),
                            null,
                            'url_rewrite_product.log'
                        );

                        break;
                }
            }
        } catch (Exception $e) {
            Mage::logException($e);

            /**
             * Show error message in admin area
             */
            if (Mage::app()->getStore()->isAdmin()) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }

        return $this;
    }

    /**
     * @param $product
     *
     * @return $this
     */
    public function removeUrlRedirectOldProducts($product)
    {
        //Remove url rewrite product not set store view
        /** @var string $productUrl */
        $productUrl = $this->_helper()->formatUrl($product->getProductUrl());

        if (empty($productUrl)) {
            Mage::throwException($this->_helper()->__('An error has occurred'));
        }

        /** @var Enterprise_UrlRewrite_Model_Redirect $rewrite */
        $rewrite = $this->_helper()->loadByRequestPath($productUrl, '1');

        if (Mage_Catalog_Model_Product_Status::STATUS_ENABLED) {
            Mage::log(
                'Enable product(Default): ' . $product->getSku(),
                null,
                'url_rewrite_product.log'
            );

            if ($rewrite->getId()) {
                Mage::log(
                    'Starting remove rewrite...',
                    null,
                    'url_rewrite_product.log'
                );

                Mage::log(
                    'url_product: ' . $productUrl,
                    null,
                    'url_rewrite_product.log'
                );

                Mage::log(
                    'remove rewrite: ',
                    null,
                    'url_rewrite_product.log'
                );

                Mage::log(
                    $rewrite->getData(),
                    null,
                    'url_rewrite_product.log'
                );

                $rewrite->delete();
            }
        }
        //End

        return $this;
    }
}
