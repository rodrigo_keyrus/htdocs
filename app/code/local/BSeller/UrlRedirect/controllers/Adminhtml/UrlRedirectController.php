<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_UrlRedirect
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

/**
 * Class BSeller_UrlRedirect_Adminhtml_UrlRedirectController
 *
 * @method BSeller_UrlRedirect_Helper_Data _helper()
 */
class BSeller_UrlRedirect_Adminhtml_UrlRedirectController extends Mage_Adminhtml_Controller_Action
{
    use BSeller_Core_Trait_Data;

    /**
     * Render index page
     *
     * @return void
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Upload file and import to the system
     *
     * @return $this
     */
    public function uploadAction()
    {
        if (!$this->getRequest()->isPost()) {
            $this->_redirect('*/*');

            return $this;
        }

        if (!$this->_validateFormKey()) {
            $this->_redirectReferer();

            return $this;
        }

        /** @var string $destinationDir */
        $destinationDir = $this->_helper()->getDestinationDir();

        try {
            /** @var Varien_File_Uploader $uploader */
            $uploader = new Varien_File_Uploader('file');
            $uploader->setAllowedExtensions($this->_helper()->getAllowedExtensions());
            $uploader->setAllowRenameFiles(true);
            $uploader->setFilesDispersion(false);
            $uploader->save($destinationDir);

            $document = new Varien_File_Csv();
            $filename = $destinationDir . DS . $uploader->getUploadedFileName();
            $lines    = $document->getData($filename);

            /** @var Varien_Io_File $fileSystem */
            $fileSystem = new Varien_Io_File();
            $fileSystem->rm($filename);

            /**
             * Skip for header
             */
            array_shift($lines);

            foreach ($lines as $key => $line) {
                /** @var array $line */
                $line = array_combine($this->_helper()->getColumnsMap(), $line);

                /** @var bool|string $errorMessage */
                $errorMessage = $this->_helper()->validate($line);

                if ($errorMessage !== true) {
                    $errorMessage = $this->__('Error in line %s.', $key) . nl2br(PHP_EOL) . $errorMessage;

                    Mage::throwException($errorMessage);
                }

                /** @var string $identifier */
                $identifier = $line['identifier'];

                /**
                 * Validate url identifier
                 */
                Mage::helper('core/url_rewrite')->validateRequestPath($identifier);

                $storeId = $this->getRequest()->getParam('store_id');

                $line['options']     = $this->getRequest()->getParam('options');
                $line['store_id']    = $storeId;
                $line['description'] = $this->__('N/A');
                $line['entity_type'] = Mage_Core_Model_Url_Rewrite::TYPE_CUSTOM;

                /** @var Esmart_Catalog_Model_Resource_Router $router */
                $router = Mage::getResourceModel('esmart_catalog/router');

                if ($productId = $router->getProductId($identifier)) {
                    $product = array_shift(Mage::getResourceModel('catalog/product')->getProductsSku([$productId]));

                    $line['product_id']  = $productId;
                    $line['entity_type'] = Mage_Core_Model_Url_Rewrite::TYPE_PRODUCT;
                    $line['description'] = $product['sku'];
                }

                /** @var Mage_Core_Model_Url_Rewrite $rewrite */
                $rewrite = $this->_helper()->loadByRequestPath($identifier, $storeId);
                $rewrite->addData($line);
                $rewrite->save();
            }

            $this->_getSession()->addSuccess($this->__('The URLs Rewrite has been saved.'));
        } catch (Exception $e) {
            $this->_getSession()->addError($this->__($e->getMessage()));
        }

        $this->_redirect('*/urlrewrite');

        return $this;
    }

    /**
     * Check whether this controller is allowed in admin permissions
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('catalog/urlrewrite');
    }
}
