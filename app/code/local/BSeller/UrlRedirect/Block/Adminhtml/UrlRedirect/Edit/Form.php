<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_UrlRedirect
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_UrlRedirect_Block_Adminhtml_UrlRedirect_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Prepare form columns
     *
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();

        $fieldSet = $form->addFieldset('base_fieldset', ['legend' => $this->__('Information')]);

        if (!Mage::app()->isSingleStoreMode()) {
            /** @var Mage_Adminhtml_Model_System_Store $store */
            $store = Mage::getSingleton('adminhtml/system_store');

            $fieldSet->addField(
                'store_id',
                'select',
                [
                    'name'     => 'store_id',
                    'label'    => $this->__('Store View'),
                    'title'    => $this->__('Store View'),
                    'required' => true,
                    'values'   => $store->getStoreValuesForForm(false, true)
                ]
            );

            $fieldSet->setRenderer(
                $this->getLayout()->createBlock('adminhtml/store_switcher_form_renderer_fieldset_element')
            );
        } else {
            $fieldSet->addField(
                'store_id',
                'hidden',
                [
                    'name'  => 'store_id',
                    'value' => Mage::app()->getStore(true)->getId()
                ]
            );
        }

        $fieldSet->addField(
            'options',
            'select',
            [
                'label'   => $this->__('Redirect'),
                'title'   => $this->__('Redirect'),
                'name'    => 'options',
                'options' => [
                    ''   => $this->__('No'),
                    'R'  => $this->__('Temporary (302)'),
                    'RP' => $this->__('Permanent (301)')
                ]
            ]
        );

        $fieldSet->addField(
            'file',
            'file', [
                'name'     => 'file',
                'label'    => $this->__('File'),
                'title'    => $this->__('File'),
                'required' => true,
                'note'     => $this->__('Only CSV file.')
            ]
        );

        $form->setData('action', $this->getUrl('*/*/upload'));
        $form->setData('enctype', 'multipart/form-data');
        $form->setData('method', 'post');
        $form->setData('use_container', true);
        $form->setId('edit_form');

        $this->setForm($form);

        return parent::_prepareForm();
    }
}
