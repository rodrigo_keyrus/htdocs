<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_UrlRedirect
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_UrlRedirect_Block_Adminhtml_UrlRedirect_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * Constructor initialization
     */
    public function __construct()
    {
        parent::__construct();

        $this->_controller = 'adminhtml_urlRedirect';
        $this->_blockGroup = 'bseller_urlredirect';
        $this->_headerText = $this->__('Upload Redirect URLs');

        $this->removeButton('delete');
    }

    /**
     * Return url for back button
     *
     * @return string
     */
    public function getBackUrl()
    {
        return $this->getUrl('*/urlrewrite');
    }
}
