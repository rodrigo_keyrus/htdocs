<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Sitemap
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Bruno Gemelli <bruno.gemelli@e-smart.com.br>
 * @author    Evandro Veloso Gomes <evandro.gomes@e-smart.com.br>
 */

class BSeller_Sitemap_Model_ManufacturerSitemap
{
    private $_mageBaseUrl,
        $_xmlFileNameTmp,
        $_xmlFileName,
        $_bmChangefreq,
        $_bmPriority,
        $_date,
        $_mediaDir;


    /**
     * @return array
     */
    public function prepareSitemapArray()
    {
        $this->_setInitialVars();

        $manufacturers = array();

        /** @var Zeon_Manufacturer_Model_Manufacturer $item */
        foreach ($this->_getManufacturers() as $item) {
            if (!$manufacturerId = $item->getData('manufacturer_id')) {
                continue;
            }
            $manufacturers[$manufacturerId] = $this->_setManufacturerData($item);
        }

        /**
         * Convert manufacturer and your data into sitemap XML format
         */
        $manufacturersFull = $this->_prepareManufacturerDetails($manufacturers);

        return $manufacturersFull;
    }


    /**
     * @param Zeon_Manufacturer_Model_Manufacturer $manufacturer
     *
     * @return array
     */
    protected function _setManufacturerData($manufacturer)
    {
        $manufacturerId = $manufacturer->getData('manufacturer_id');

        $data = array(
            'manufacturer_id' => $manufacturerId,
            'ident'           => $manufacturer->getData('identifier'),
            'level'           => 0,
//            'pages'           => $this->_getPagesByManufacturer($manufacturerId),
        );

        if (!$manufacturerProducts = $this->_getProductsByManufacturer($manufacturerId)) {
            return $data;
        }

        /** @var Mage_Catalog_Model_Product $product */
        foreach ($manufacturerProducts as $product) {

            if (!($product instanceof Mage_Catalog_Model_Product)) {
                continue;
            }

            $categories = array();
            if (array_key_exists('categories', $data)) {
                $categories = array_keys($data['categories']);
            }

            if (count($product->getCategoryIds())) {
                $notFoundCategories = array_diff ($product->getCategoryIds() , $categories);
                if (count($notFoundCategories)) {
                    $categories = Mage::getModel ('catalog/category')
                        ->getCollection()
                        ->addFieldToFilter('is_active' , 1)
                        ->addAttributeToFilter('entity_id' , $notFoundCategories);

                    /** @var Mage_Catalog_Model_Category $category */
                    foreach ($categories as $category) {
                        $data['categories'][$category->getData('entity_id' )] = $category;
                    }
                }
            }
        }

        return $data;
    }


    /**
     * @param array $manufacturers
     *
     * @return array
     */
    protected function _prepareManufacturerDetails($manufacturers)
    {
        $items = array();

        foreach ($manufacturers as $manufacturerId => $manufacturer) {

            if (!isset($manufacturer['categories'])) {
                continue;
            }

            /**
             * create manufacturer node
             */
            $items[] = $this->_prepareXmlItemNode(
                null,
                $manufacturer['ident'],
                $manufacturer['level']
            );


            /**
             * create manufacturer page/category node
             */
            foreach ($manufacturer['categories'] as $category) {
                $items[] = $this->_prepareXmlItemNode(
                    $category->getData('entity_id'),
                    $manufacturer['ident'],
                    $category->getData('level')
                );
            }
        }

        return $items;
    }


    /**
     * @param int|null $categoryId
     * @param string $identifier
     * @param int $level
     *
     * @return array
     */
    protected function _prepareXmlItemNode($categoryId, $identifier, $level)
    {
        $path = $identifier;

        if ($categoryId) {
            /** @var Mage_Catalog_Model_Category $category */
            $category = Mage::getModel('catalog/category')->load($categoryId);
            $catUrl = str_replace(Mage::getUrl(), '', $category->getUrl());

            if (strpos($catUrl, 'catalog/category/')) {
                return array();
            }

            $path .= '/'.$catUrl;
        }

        switch ($level) {
            case 0:
                $priority = 0.8;
                break;
            case 1:
            case 2:
                $priority = 0.6;
                break;
            default:
                $priority = 0.5;
                break;
        }

        $entity = new Varien_Object();
        $entity->setUrl($path);
        $entity->setPriority($priority);
        return $entity;

    }


    /**
     * Set initial vars
     */
    private function _setInitialVars()
    {
        Mage::app()->setCurrentStore(Mage_Core_Model_App::DISTRO_STORE_ID);

        $this->_mediaDir = Mage::getBaseDir('media');

        $this->_mageBaseUrl = Mage::getStoreConfig('bseller_sitemap/settings/bm_base_url');
        if (!$this->_mageBaseUrl) {
            Mage::app()->loadArea(Mage_Core_Model_App_Area::AREA_FRONTEND);
            $this->_mageBaseUrl = Mage::getBaseUrl();
        }
        $this->_xmlFileNameTmp = $this->_mediaDir . '/tmp_sitemap_manufacturer.xml';
        $this->_xmlFileName    = $this->_mediaDir . '/' . Mage::getStoreConfig('bseller_sitemap/settings/bm_filename');
        $this->_bmChangefreq   = Mage::getStoreConfig('bseller_sitemap/settings/bm_changefreq');
        $this->_date           = Mage::getModel('core/date')->date('Y-m-d');
    }


    /**
     * Get all manufacturers for export
     *
     * @return Zeon_Manufacturer_Model_Mysql4_Manufacturer_Collection
     */
    private function _getManufacturers()
    {
        $limitManufacturer = Mage::getStoreConfig('bseller_sitemap/settings/bm_manufacturer_limit');

        return Mage::getModel('zeon_manufacturer/manufacturer')
            ->getCollection()
            ->addFieldToFilter('status', '1')
            ->addFieldToFilter('is_display_sitemap', '1')
            ->setPageSize($limitManufacturer)
            ->setOrder('manufacturer_id', 'DESC');
    }


    /**
     * Get the products of the manufacturer for export
     *
     * @param int $manufacturerId ID of the manufacturer
     *
     * @return Esmart_Catalog_Model_Catalog_Product
     */
    private function _getProductsByManufacturer($manufacturerId)
    {
        return Mage::getModel('catalog/product')
            ->getCollection()
            ->addFieldToFilter('manufacturer', $manufacturerId);
    }
}