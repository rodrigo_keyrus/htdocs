<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Core
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Diogo Baracho <diogo.baracho@e-smart.com.br>
 */ 
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

//$resource = Mage::getSingleton('core/resource');
$query="
UPDATE sales_flat_order AS o SET customer_cpf=(SELECT ev.value FROM 
customer_entity as ce,
eav_attribute as ea,
customer_entity_varchar as ev
WHERE 1=1
AND ce.entity_id=ev.entity_id
AND ea.attribute_id=ev.attribute_id
AND ea.attribute_code='cpf'
AND o.customer_email=ce.email
GROUP BY ce.email 
);
";


//OPTION 2 WHEN EMAILS ARE NOT UNIQUE
$query = "UPDATE sales_flat_order AS o SET 
customer_cpf=(SELECT ev.value FROM  
customer_entity as ce, eav_attribute as ea, customer_entity_varchar as ev 
WHERE 1=1 AND ce.entity_id=ev.entity_id AND ea.attribute_id=ev.attribute_id 
AND ea.attribute_code='cpf' AND o.customer_email=ce.email ) LIMIT 500000;
";
//$writeConnection = $resource->getConnection('core_write');
//$writeConnection->query($query);

$installer->endSetup();