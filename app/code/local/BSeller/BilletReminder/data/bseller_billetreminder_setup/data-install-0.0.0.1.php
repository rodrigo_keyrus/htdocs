<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_BilletReminder
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Evandro Veloso Gomes <evandro.gomes@e-smart.com.br>
 */

$installer = $this;
$installer->startSetup();

$newOrderEmail = Mage::getModel('core/email_template');
$newOrderEmail->setData('template_code', 'Lembrete de Boleto');
$newOrderEmail->setData('template_type', Mage_Core_Model_Email_Template::TYPE_HTML);
$newOrderEmail->setData('template_subject', '');
$newOrderEmail->setData('orig_template_code', 'bseller_billetreminder');
$newOrderEmail->setData('orig_template_variables', '');
$newOrderEmail->setData('template_text', <<<text
Nome: {{var customerName}}<br />
Nome Completo: {{var customerFullName}}<br />
Barcode: {{var barcode}}<br />
Data de vencimento: {{var dueDate}}<br />
Valor: {{var documentValue}}<br />
<br />
<a href="{{var BilletURL}}" target="_blank">Imprimir</a>
text
);
$newOrderEmail->save();

$installer->endSetup();