<?php

/**
 * BIT Tools Platform | B2W - Companhia Digital
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category  BitTools
 * @package   BitTools_ModuleName
 *
 * @copyright Copyright (c) 2018 B2W Digital - BIT Tools.
 *
 * @author    Julio Reis <julio.reis@b2wdigital.com>
 */
class BSeller_ErpOrder_Model_Observer
{

    /**
     * @param Varien_Event_Observer $observer
     */
    public function addApprovingPaymentJsonToInvoicePage(Varien_Event_Observer $observer)
    {
        $block = $observer->getBlock();
        // Invoice View Page
        $blockClass = Mage::getConfig()->getBlockClassName('adminhtml/sales_order_invoice_view');
        if ($blockClass == get_class($block)) {
            $transport = $observer->getTransport();
            $html = $transport->getHtml();
            $pos = strpos($html, 'head-shipping-method');
            $pos = strpos($html, 'class="clear"', $pos);
            $pos = strpos($html, '</div>', $pos);
            $insert = Mage::app()->getLayout()->createBlock('bseller_erporder/adminhtml_sales_order_invoice_view_integrationJson');
            $html = substr_replace($html, $insert->toHtml(), $pos + 6, 0);
            $transport->setHtml($html);
        }
    }
}