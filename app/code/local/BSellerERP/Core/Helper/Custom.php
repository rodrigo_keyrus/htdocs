<?php

class BSellerERP_Core_Helper_Custom extends Mage_Core_Helper_Abstract
{
    function getDeliveryDate($order)
    {
        return $order->getFirecheckoutDeliveryDate();
    }

    function getDeliveryTime($order)
    {
        if (!$order->getFirecheckoutDeliveryTimerange()) {
            return null;
        }

        $time = explode(':00 —', $order->getFirecheckoutDeliveryTimerange())[0];

        if ($time && $time < 11) {
            return 1;
        }

        if ($time && $time >= 11 && $time < 18) {
            return 2;
        }

        if ($time && $time >= 18) {
            return 3;
        }
        return null;
    }
}