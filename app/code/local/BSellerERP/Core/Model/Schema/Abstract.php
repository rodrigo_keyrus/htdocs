<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Core
 *
 * @copyright Copyright (c) 2019 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Bruno Gemelli <bruno.gemelli@e-smart.com.br>
 */
abstract class BSellerERP_Core_Model_Schema_Abstract extends Mage_Core_Model_Abstract
{
    /**
     * Returns helper object
     *
     * @var BSellerERP_Core_Helper_Data
     */
    protected $helper;

    /**
     * Load helper data
     *
     * @var string
     */
    public $currentHelper = 'core';

    /**
     * Returns current store
     * @var Mage_Core_Model_Store
     */
    protected $store;

    /**
     * Data variable types to be skipped in conversion
     * @var array
     */
    protected $_skippedTypes = array(
        'boolean',
        'double',
        'integer',
    );

    /**
     * Load helper model and get current store
     */
    public function __construct()
    {
        parent::__construct();

        $this->helper   = Mage::helper('bsellererp_' . $this->currentHelper);
        $this->store    = Mage::app()->getStore();
    }

    /**
     * Clear data and prepare schema for adding in instance
     *
     * @return $this
     */
    protected function prepareSchema()
    {
        $this->clearInstance();
        $this->unsetData();

        return $this;
    }


    /**
     * Returns schema
     *
     * @return array
     */
    protected function getSchema()
    {
        return $this->toArray();
    }


    /**
     * @param $data
     *
     * @return string
     */
    public function sanitizeData(&$data, $datakey = null)
    {
        if (!$data || in_array(gettype($data), $this->_skippedTypes)) {
            return;
        }

        if (!is_array($data)) {
            $data = trim($this->_getHelperUrl()->format($data));
            return;
        }

        foreach ($data as &$datum) {
            $this->sanitizeData($datum, $datakey);
        }
    }


    /**
     * @return Esmart_Catalog_Helper_Field_Url
     */
    protected function _getHelperUrl()
    {
        return Mage::helper('esmart_catalog/field_url');
    }


    /**
     * Setting general data for schema
     *
     * @return mixed
     */
    abstract protected function setGeneralData();
}
