<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Order
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Schema model
 *
 * @category   BSellerERP
 * @package    BSellerERP_Order
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Order_Model_Schema extends BSellerERP_Core_Model_Schema_Abstract
{
    /**
     * Available product type
     */
    const TYPE_DOWNLOADABLE = 'downloadable';

    /**
     * Config Channel Types
     */
    const CHANNEL_TYPE_CONFIG  = 'bsellererp_marketplace/mapping/channel_types';
    const CHANNEL_TYPE_DEFAULT = 'MKT';

    /**
     * Gift card product type
     */
    const TYPE_GIFTCARD = 'giftcard';

    /**
     * Product virtual type ERP
     */
    const ITEM_TYPE_VIRTUAL = 'N';

    /**
     * Product simple/configurable type ERP
     */
    const ITEM_TYPE_PRODUCT = 'P';

    /**
     * Content returned integration to import
     *
     * @var Mage_Sales_Model_Order
     */
    protected $order;

    /**
     * Customer by order
     *
     * @var Mage_Customer_Model_Customer
     */
    protected $customer;
    protected $carrier;
    protected $carrierContract;

    /**
     * Returns helper object
     *
     * @var BSellerERP_Order_Helper_Data
     */
    protected $helper;

    /**
     * Load helper data
     *
     * @var string
     */
    public $currentHelper = 'order';

    public $valeDiscountAmount;

    /**
     * Load helper data
     *
     * @var float
     */
    public $deductionAmount;
    public $giftAmount;
    public $orderFullPaidWithVale;

    protected $orderIncrementId;
    protected $orderExternalId;
    protected $orderSalesChannel;
    protected $orderPaymentCode;
    protected $orderShippingType;
    protected $intelipostIdCotacao;


    /**
     * Returns schema without special chars.
     * Special chars treatment done in sanitizeData method (ex: João -> Joao)
     *
     * @return array
     */
    protected function getSchema()
    {
        $data = $this->toArray();
        $this->sanitizeData($data);

        return $data;
    }


    /**
     * Prepare schema and returns for adding in order instance
     *
     * @return array
     */
    public function prepareSchema()
    {
        parent::prepareSchema();

        $this->setValues();

        if (!$this->order->getId()) {
            Mage::throwException('Order not exist for create schema');
        }

        $this->customer = Mage::getModel('customer/customer')->load($this->order->getCustomerId());

        $this->initCarrierAndContract();
        $this->setIntelipostIdCotacao();

        $this->setGeneralData();
        $this->setCustomerAddressData($this->order->getBillingAddress());
        $this->setCustomerAddressData($this->order->getShippingAddress() ? $this->order->getShippingAddress() : $this->order->getBillingAddress(), 'shipping');
        $this->setItemsData();
        $this->setShippingData();
        $this->setPaymentData();
        $this->setTotalsData();

        return $this->getSchema();
    }

    /**
     * Setting general data for schema
     *
     * @return $this
     */
    protected function setGeneralData()
    {
        $incrementId = $this->order->getIncrementId();

        $customerHelper = Mage::helper('bsellererp_core/customer');
        $customerHelper->setCustomer($this->customer);

        $this->addData(
            array(
                'canalVenda'            => $this->getSalesChannel(),
                'codigoVendedor'        => $this->order->getData('sac_operator'),
                'dataEmissao'           => $this->helper->timezoneConvert($this->order->getCreatedAt()),
                'dataInclusao'          => $this->helper->timezoneConvert($this->order->getCreatedAt()),
                'numeroPedido'          => $this->getOrderIncrementId(),
                'numeroPedidoExterno'   => $this->getOrderExternalId(),
                'numeroPedidoLoja'      => $this->getOrderIncrementId(),
                'origemPedido'          => $this->helper->getOrderStoreConfig('sale_origin'),
                'pedidoParaConsumo'     => (bool) $this->helper->getOrderStoreConfig('consumption'),
                'tipoPedido'            => $customerHelper->getTipoPedido(),
                'entregasQuebradas'     => false,
                'observacoesEtiqueta'   => null,
                'tipoFrete'             => 'C',
                'unidadeNegocio'        => $this->helper->getSettingStoreConfig('business_unit'),
                'idCotacao'             => $this->getIntelipostIdCotacao(),
            )
        );

        return $this;
    }

    /**
     * Setting customer address by type for schema
     *
     * @param Mage_Sales_Model_Order_Address $address
     * @return $this
     * @throws Mage_Core_Exception
     */
    protected function setCustomerAddressData(Mage_Sales_Model_Order_Address $address, $addressType = 'billing')
    {
        if (!$address) {
            Mage::throwException('Address not exist for create schema - Order: ' . $this->order->getIncrementId());
            return $this;
        }

        $customerHelper = Mage::helper('bsellererp_core/customer');
        $customerHelper->setCustomer($this->customer);
        $customerHelper->setAddress($address);

        $formatAddress = $customerHelper->formatAddress(
            $this->helper->getStreetByName('address_number', $address),
            $this->helper->getStreetByName('address_street', $address),
            $this->helper->getStreetByName('address_complement', $address)
        );

        $addressInfo = array(
            'bairro'            => $this->helper->truncate($this->getNeighborhood($address), 50),
            'logradouro'        => $this->helper->truncate($formatAddress['street'], 60),
            'numero'            => $this->helper->onlyNumbers($formatAddress['number']),
            'complemento'       => $this->helper->truncate($formatAddress['complement'], 50),
            'pontoReferencia'   => $this->helper->truncate($this->helper->getPredefinedAttributeValue('reference_point', $address), 100),
            'cep'               => $this->helper->onlyNumbers($address->getPostcode()),
            'cidade'            => $address->getCity(),
            'estado'            => $address->getRegionCode(),
            'pais'              => Mage::app()->getLocale()->getCountryTranslation($address->getCountry()),
            'zipCode'           => null,
        );

        $cellphone = $this->helper->getPredefinedAttributeValue('cellphone', $address);

        $generalInfo = array(
            'classificacao'         => $customerHelper->getClassificacao($this->customer),
            'dataNascimento'        => $this->helper->formatGMTDate($this->order->getCustomerDob()),
            'email'                 => $address->getEmail(),
            'endereco'              => $addressInfo,
            'fax'                   => $address->getFax(),
            'nome'                  => $this->helper->truncate($customerHelper->getName(), 40),
            'rg'                    => $this->helper->getPredefinedAttributeValue('rg', $this->customer),
            'telefoneResidencial'   => $this->helper->onlyNumbers($address->getTelephone()),
            'telefoneCelular'       => $this->helper->onlyNumbers($cellphone),
            'telefoneComercial'     => null,
            'id'                    => $customerHelper->getDocumentValue()
        );

        /**
         * Append person type info
         */
        $generalInfo = array_merge($generalInfo, $customerHelper->preparePersonType());

        /**
         * Configure type node customer address
         */
        $node = 'clienteFaturamento';
        if ($address->getAddressType() == Mage_Customer_Model_Address::TYPE_SHIPPING) {
            $node = 'clienteEntrega';
        }

        $this->addData(
            array(
                $node => $generalInfo
            )
        );

        return $this;
    }

    protected function initCarrierAndContract()
    {
        $carrier        = Mage::helper('bsellererp_shipping')->getCarrierByShipping($this->order->getShippingMethod());
        $contract       = Mage::helper('bsellererp_shipping')->getContractByShipping($this->order->getShippingMethod());

        $carrierContractId = null;

        if (!$carrier) {
            return null;
        }

        if ($carrier->getData('carrier_id')) {
            //if is flatrate_flatrate (admin configured - not intelipost service) then aways use default contract_id;
            if (Mage::getStoreConfig('bsellererp_shipping/carrier/use_default_carrier_codes') && $this->order->getShippingMethod() != 'flatrate_flatrate') {
                $this->carrierContract = substr($this->order->getShippingMethod(), (strrpos($this->order->getShippingMethod(), '_') ?: -1) + 1);
            } else {
                $this->carrierContract = $contract;
            }
        }
        $this->carrier = $carrier;
    }

    /**
     * Setting items data for schema
     *
     * @return $this
     */
    protected function setItemsData()
    {
        $orderItems     = $this->order->getAllVisibleItems();
        $items          = array();
        $shippingAmount = $this->order->getShippingAmount();
        $qtyItems       = count($orderItems);
        $transitTime    = $this->helper->onlyNumbers($this->order->getShippingDescription());
        $qtyGiftItems   = 0;

        /**
         * Set default transit time
         */
        if (!$transitTime) {
            $transitTime = (int) $this->helper->getOrderStoreConfig('transit_time');
        }

        /**
         * @var Mage_Sales_Model_Order_Item $orderItem
         */
        $totalItemsQty = 0;

        //Remove duplicate items on order. We are considering here that the second item is a
        //bugged gift was added due to an error;
        $this->removeDuplicatedItems($orderItems);

        //sometimes (employee discount for example) a discount is applied at order and not splitted
        //by the items. BSeller need all discount splitted by items.
        $this->splitOrderDiscountDiffIntoItems($orderItems);

        //Treating discounts by special price feature
        //maniuplating these data for sephora rules: they need the special price discount counting on orders export
        foreach ($orderItems as $key => $orderItem) {
            $unitFinalPrice = $orderItem->getPrice();

            //check if is gift
            if ($this->isGift($orderItem)) {
                continue;
            }

            $unitPriceWithoutAnyDiscount = $orderItem->getPriceWithoutAnyDiscount();
            if ($unitFinalPrice <= 0) {
                continue;
            }
            if (!$unitPriceWithoutAnyDiscount) {
                continue;
            }
            $unitSpecialPriceDiscount = $unitPriceWithoutAnyDiscount - $unitFinalPrice;
            if ($unitSpecialPriceDiscount <= 0) {
                continue;
            }

            $qtyOrdered = $orderItem->getQtyOrdered();
            $discountAmount = $orderItem->getData('discount_amount') + (($unitPriceWithoutAnyDiscount - $unitFinalPrice) * $qtyOrdered);
            $orderItem->setData('discount_amount', $discountAmount);
            $orderItem->setData('price', $unitPriceWithoutAnyDiscount);

            $this->order->setSubtotal($this->order->getSubtotal() + ($unitSpecialPriceDiscount * $qtyOrdered));
            $this->order->getGrandTotal($this->order->getGrandTotal() + ($unitSpecialPriceDiscount * $qtyOrdered));
        }

        foreach ($orderItems as $key => $orderItem) {
            $qtyOrdered = $orderItem->getQtyOrdered() * 1;
            $totalItemsQty += $qtyOrdered;

            if ($this->isGift($orderItem)) {
                $orderItem->setIsGift(true);
                $qtyGiftItems += $qtyOrdered;
            }

            if ($orderItem->getProductType() == Mage_Catalog_Model_Product_Type::TYPE_BUNDLE) {
                $children = $orderItem->getChildrenItems();

                $qtyItems += (count($children) - 1);

                $discountPerItem = (float) $this->round($orderItem->getDiscountAmount()/count($children));
                foreach ($children as $child) {
                    $attributes = (array) Zend_Serializer::unserialize($child->getProductOptions()['bundle_selection_attributes']);
                    $child->setQtyOrdered($attributes['qty']);
                    $child->setPrice((float)number_format($attributes['price']/$attributes['qty'], 2));
                    $child->setDiscountAmount($discountPerItem);
                    $child->setParentItemSku(explode('-', $orderItem->getSku())[0]);
                }
                unset($orderItems[$key]);
                $orderItems = array_merge($orderItems, $children);
            }
        }

        //carrier price per unit shouldn't take into account the gift item. Gifts shouldn't be charged.
        $shippableItems = $totalItemsQty - $qtyGiftItems;
        $unitShippingCost = $this->round($shippingAmount / ($shippableItems > 0 ? $shippableItems : 1));

        $index = 0;
        foreach ($orderItems as $key => $orderItem) {
            //update cod_sap attribute if it does not exist
            if (is_null($orderItem->getCodSap())) {
                $updateProduct = Mage::getModel('catalog/product')->load($orderItem->getProductId());
                $updateProduct->setCodSap($updateProduct->getSku())
                    ->getResource()->saveAttribute($updateProduct, 'cod_sap');
            }

            $qtyOrdered = $orderItem->getQtyOrdered() * 1;

            $giftUnitDiscount = $this->treatGift($orderItem);

            $discountAmount     = $orderItem->getDiscountAmount();
            $discountAmountItem = $discountAmount;

            $discountAmountItem += $giftUnitDiscount;

            /**
             * Calculate discount amount per item
             */
            if ((float)$discountAmount && $qtyOrdered > 1) {
                $discountAmountItem = $this->round($discountAmount/$qtyOrdered);
                $discountAmountForCheck = $discountAmountItem * $qtyOrdered;
                if ($discountAmountForCheck != $discountAmount) {
                    $discountDiff = $this->number_format($discountAmount - $discountAmountForCheck);

                    /**
                     * Increase deduction value
                     */
                    if ($discountDiff > 0) {
                        //temporaly we gonna deny all orders which has decimal problems
                        //It occurs because magento has 4 decimal cases and bseller just 2
                        $this->order->setHasDecimalProblems(true);
                        //end

                        $this->deductionAmount += $discountDiff;
                    }
                }
            }

            $orderItem->setCodSap(Mage::getModel('catalog/product')->load($orderItem->getProductId())->getCodSap());
            $isConditionalDiscount = $this->helper->getOrderStoreConfig('conditional_discount');
            $shippingCost = (!$orderItem->getIsGift() ? ($unitShippingCost * $qtyOrdered) : 0);

            $item = array(
                'cnpjFilial'                    => $this->helper->getSettingStoreConfig('cnpj_filial'),
                'codigoEstabelecimentoSaida'    => $this->helper->getSettingStoreConfig('output_establishment'),
                'codigoEstabelecimentoEstoque'  => $this->helper->getSettingStoreConfig('stock_establishment'),
                'descontoCondicionalUnitario'   => $isConditionalDiscount ? (string)$discountAmountItem : '0',
                'descontoIncondicionalUnitario' => !$isConditionalDiscount ? (string)$discountAmountItem : '0',
                'prazoCentroDistribuicao'       => (int) $this->helper->getOrderStoreConfig('lead_time'),
                'prazoFornecedor'               => 0,
                'prazoTransitTime'              => $transitTime,
                'idContratoTransportadora'      => $this->carrierContract ? $this->carrierContract->getContrato() : null,
                'idTransportadora'              => $this->carrier ? $this->carrier->getData('carrier_id') : null,
                'sequencial'                    => $index,
                'valorFrete'                    => $shippingCost,
                'codigoItem'                    => $orderItem->getCodSap() ? $orderItem->getCodSap() : $orderItem->getSku(),
                'tipoItem'                      => $this->getItemType($orderItem),
                'quantidade'                    => $qtyOrdered,
                'precoUnitario'                 => number_format(($orderItem->getGiftPrice() ? $orderItem->getGiftPrice() : $orderItem->getPrice()), 2, '.', ''),
                'valorDespesas'                 => 0,
                'itemBonificado'                => (($orderItem->getIsGift() && Mage::getStoreConfig('bsellererp_order/settings/gift_treatment')) ? true : false),
                'codigoPromocional'             => $this->_getPromotionalCode($orderItem),
            );

            if ($this->deductionAmount) {
                $index++;
                $extraItem = $item;
                $extraItem['valorFrete'] = $unitShippingCost;
                $extraItem['sequencial'] = $index;
                $extraItem['quantidade'] = 1;
                $extraItem['descontoCondicionalUnitario'] = $isConditionalDiscount ? (string)($discountAmountItem + $this->deductionAmount) : '0';
                $extraItem['descontoIncondicionalUnitario'] = !$isConditionalDiscount ? (string)($discountAmountItem + $this->deductionAmount) : '0';

                $item['quantidade'] = $item['quantidade'] - 1;

                $shippingCost = (!$orderItem->getIsGift() ? ($unitShippingCost * ($qtyOrdered - 1)) : 0);
                $item['valorFrete'] = $shippingCost;

                $items[] = $item;
                $items[] = $extraItem;
                $this->deductionAmount = 0;
            } else {
                $items[] = $item;
            }

            //if the item has parent defined, he's a kit
            if ($orderItem->getParentItemSku()) {
                $items[count($items)-1]['codigoItemKit'] = $orderItem->getParentItemSku();
            }

            $index++;
        }

        /**
         * Calculate shipping amount and correct prices
         */
        $totalAmount = ($totalItemsQty-$qtyGiftItems) * $unitShippingCost;
        $calculation = $shippingAmount - $totalAmount;

        if ($calculation) {
            $items[0]['valorFrete'] = $items[0]['valorFrete'] + $calculation;
        }

        $this->addData(
            array(
                'itens' => $items
            )
        );

        return $this;
    }

    public function treatGift(&$orderItem)
    {
        //gift treatment
        if ($this->isGift($orderItem)) {
            $orderItem->setIsGift(true);
            if (!Mage::helper('bsellererp_core')->getStoreConfig('bsellererp_order/settings/gift_value')) {
                $orderItem->setGiftPrice('0.01');
            } else {
                $orderItem->setGiftPrice(Mage::getModel('catalog/product')->load($orderItem->getProductId())->getPrice());
            }
            $total = ($orderItem->getGiftPrice() * $orderItem->getQtyOrdered());
            $this->giftAmount += $total;
            return $orderItem->getGiftPrice();
        }

        return null;
    }

    /**
     * Round price
     *
     * @param  $decimal
     * @param  $precision
     * @return float
     */
    public function round($decimal, $precision = 2)
    {
        return number_format(floor((string)$decimal*100)/100, $precision);
    }

    public function number_format($decimal, $precision = 4)
    {
        return number_format($decimal, $precision);
    }

    /**
     * Setting shipping method data for schema
     *
     * @return $this
     */
    protected function setShippingData()
    {
        $deliveryDate = Mage::helper('bsellererp_core/custom')->getDeliveryDate($this->order);
        $deliveryTime = Mage::helper('bsellererp_core/custom')->getDeliveryTime($this->order);

        $this->addData(
            array(
                'entrega' => array(
                    'dataEntregaAgendada'    => $deliveryDate,
                    'periodoEntregaAgendada' => $deliveryTime,
                    'tipoEntrega'            => $this->getOrderShippingType(),
                )
            )
        );

        return $this;
    }

    /**
     * Setting payment data for schema
     *
     * @return $this
     */
    protected function setPaymentData()
    {
        $paymentData   = new Varien_Object();
        $paymentHelper = Mage::helper('bsellererp_payment');
        $address       = $this->order->getBillingAddress();

        $formatAddress = Mage::helper('bsellererp_core/customer')->formatAddress(
            $this->helper->getStreetByName('address_number', $address),
            $this->helper->getStreetByName('address_street', $address),
            $this->helper->getStreetByName('address_complement', $address)
        );

        $address = array(
            'bairro'            => $this->helper->truncate($this->getNeighborhood($address), 20),
            'logradouro'        => $this->helper->truncate($formatAddress['street'], 60),
            'numero'            => $this->helper->onlyNumbers($formatAddress['number']),
            'complemento'       => $this->helper->truncate($formatAddress['complement'], 50),
            'pontoReferencia'   => $this->helper->truncate($this->helper->getPredefinedAttributeValue('reference_point', $address), 100),
            'cep'               => $this->helper->onlyNumbers($address->getPostcode()),
            'cidade'            => $address->getCity(),
            'estado'            => $address->getRegionCode(),
            'pais'              => Mage::app()->getLocale()->getCountryTranslation($address->getCountry()),
            'zipCode'           => null,
        );

        $value = number_format($this->order->getGrandTotal(), 2, '.', '');
        $generalData = array(
            'endereco' => $address,
            'valor'               => $value,
            'sequencial'          => 0,
            'codigoMeioPagamento' => $this->getOrderPaymentCode(),
            'codigoCondicaoPagamento' => $paymentHelper->getPaymentCondition($this->order),
        );

        if ($this->order->getData('bseller_skyhub') == 1) {
            $deliveryDate = $this->helper->formatDate(now());
            if (isset($this->poNumber['delivery_date'])) {
                $deliveryDate = $this->helper->formatDate($this->poNumber['delivery_date']);
            }

            $generalData['codigoBanco'] = $this->helper->getSettingStoreConfig('bank_code');
            $generalData['codigoAgencia'] = $this->helper->getSettingStoreConfig('number_agency');
            $generalData['numeroConta'] = $this->helper->getSettingStoreConfig('number_account');
            $generalData['dataVencimentoBoleto'] = $deliveryDate;
        }

        /**
         * Add general data
         */
        if ($value && ((float)$value) > 0) {
            $paymentData->addData(array(0 => $generalData));
            $this->orderFullPaidWithVale = false;
        } else {
            $this->orderFullPaidWithVale = true;
        }

        //vale treatment
        $this->setValePaymentData($generalData, $paymentData, $address);

        Mage::dispatchEvent(
            $this->helper->getPrefix() . '_payment',
            array(
                'payment' => $paymentData,
                'order'   => $this->order
            )
        );

        $this->addData(
            array(
                'pagamentos' =>
                    $paymentData->toArray()
            )
        );

        return $this;
    }

    private function setValePaymentData(&$generalData, &$paymentData, $address)
    {
        if (!(float)$this->order->getVoucherCreditAmountUsed()) {
            return;
        }

        //Module esmart_voucher required for vale integration works
        $this->valeDiscountAmount = $this->order->getVoucherCreditAmountUsed();

        $appliedVales = json_decode($this->order->getAppliedVales());

        $totalValue = number_format(($this->order->getSubtotal() + $this->order->getShippingAmount()), 2, '.', '');

        if (!$this->orderFullPaidWithVale) {
            $generalData['valor'] = $totalValue - ($this->valeDiscountAmount ? abs($this->valeDiscountAmount) : 0);
        }

        //if the total vale discount is greater than the total order amount, means the only payment will be "vale", so
        //we need to put the attribute "codigoVale" on the schema;
        $count = $firstItem = $this->orderFullPaidWithVale ? 0 : 1;
        foreach ($appliedVales as $valeCode => $appliedVoucherValue) {
            $valePaymentData = array(
                'endereco' => $address,
                'valor' => abs($appliedVoucherValue),
                'sequencial' => $count,
                'codigoMeioPagamento' => 4,
                'codigoVale' => $valeCode,
            );
            $paymentData->addData(array($count => $valePaymentData));
            $count++;
        }
    }

    /**
     * Setting totals data for schema
     *
     * @return $this
     */
    protected function setTotalsData()
    {
        $totals = array(
            'valorDespesasFinanceiras' => 0.0,
            'valorDespesasItem'        => 0.0,
            'valorFrete'               => number_format($this->order->getShippingAmount(), 2),
            'valorTotalProdutos'       => number_format($this->order->getSubtotal() + $this->giftAmount, 2, '.', ''),
        );

        $this->addData(
            array(
                'valores' => $totals
            )
        );

        return $this;
    }

    /**
     * Returns item type
     *
     * @param Mage_Sales_Model_Order_Item $item
     * @return string
     */
    protected function getItemType($item)
    {
        switch ($item->getProductType()) {
            case Mage_Catalog_Model_Product_Type::TYPE_VIRTUAL:
            case self::TYPE_GIFTCARD:
            case self::TYPE_DOWNLOADABLE:
                return self::ITEM_TYPE_VIRTUAL;

                break;

            default:
                return self::ITEM_TYPE_PRODUCT;

                break;
        }
    }

    protected function _getPromotionalCode($orderItem)
    {
        $loadedItem = Mage::getModel('catalog/product')->load($orderItem->getProductId());
        if ($loadedItem->getIsBc()) {
            return 'RESGATEBC';
        }

        if (!$orderItem->getAppliedRuleIds()) {
            return null;
        }

        $appliedRuleIds = explode(',', $orderItem->getAppliedRuleIds());
        foreach ($appliedRuleIds as $ruleId) {
            $rule = Mage::getModel('salesrule/rule')->load($ruleId);

            if ($rule && $rule->getId() && $rule->getTrackingCode()) {
                return $rule->getTrackingCode();
            }
        }

        return null;
    }

    protected function removeDuplicatedItems(&$orderItems) {
        $itemsTmp = array();
        foreach ($orderItems as $key => $orderItem) {
            if (!in_array($orderItem->getProductId(), $itemsTmp)) {
                $itemsTmp[] = $orderItem->getProductId();
                continue;
            }
            unset($orderItems[$key]);
        }
    }

    protected function splitOrderDiscountDiffIntoItems($orderItems)
    {
        if(!$this->order->getDiscountAmount()) {
            return $orderItems;
        }

        $totalDiscountItems = 0;
        foreach($orderItems as $orderItem) {
            $totalDiscountItems += $orderItem->getDiscountAmount();
        }

        $totalDiff = $this->number_format(abs($this->order->getDiscountAmount()) - abs($totalDiscountItems));

        $totalDiscountValueAux = 0;
        if($totalDiff && $totalDiff > 0) {
            foreach($orderItems as $orderItem) {
                //if is not gift
                if (!$this->isGift($orderItem)) {
                    $productPercentFromOrder = (($orderItem->getRowTotal() * 100) / $this->order->getSubtotal());
                    $newItemDiscountValue = $this->round($orderItem->getData('discount_amount') + (($productPercentFromOrder * $totalDiff)/100));
                    $orderItem->setData('discount_amount', $newItemDiscountValue);

                    $totalDiscountValueAux += $newItemDiscountValue;
                }
            }

            //treating if the splitted value has decimal cases
            if ($totalDiscountValueAux < $totalDiff) {
                $diffGeneratedWhenSplitting = $this->round($this->number_format($totalDiff - $totalDiscountValueAux));
                foreach($orderItems as $key => $orderItem) {
                    if (!$this->isGift($orderItem)) {
                        if ($this->number_format($orderItem->getPrice() - $orderItem->getDiscountAmount()) > $diffGeneratedWhenSplitting) {
                            $orderItem->setData('discount_amount', $orderItem->getData('discount_amount') + $diffGeneratedWhenSplitting);
                            break;
                        }
                    }
                }
            }
            //end
        }
        return $orderItems;
    }

    private function isGift($item)
    {
        if ($item->getIsGift()) {
            return true;
        }
        if (in_array($item->getPrice(), array('0.00')) || in_array($item->getRowTotal(), array('0.00'))) {
            $item->setIsGift(true);
            return true;
        }
        $loadedItem = Mage::getModel('catalog/product')->load($item->getProductId());
        if ($loadedItem->getIsBc()) {
            $item->setIsGift(true);
            return true;
        }
        return false;
    }

    /**
     * Set multiples values
     *
     * @return bool
     */
    private function setValues()
    {
        if ($this->order->getData('bseller_skyhub') == 1) {
            $this->setOrderExternalId(true);
            $this->setOrderIncrementId($this->order->getIncrementId());
            $this->setSalesChannel(true);
            $this->setOrderPaymentCode(true);
            $this->setOrderShippingType(true);
            return true;
        }

        $this->setOrderExternalId(false);
        $this->setOrderIncrementId($this->order->getIncrementId());
        $this->setSalesChannel(false);
        $this->setOrderPaymentCode(false);
        $this->setOrderShippingType(false);
        return false;
    }


    /**
     * @param $incrementId
     */
    private function setOrderIncrementId($incrementId)
    {
        $this->orderIncrementId = $incrementId;
    }

    /**
     * @return mixed
     */
    private function getOrderIncrementId()
    {
        return $this->orderIncrementId;
    }

    /**
     * @param bool $marketplace
     */
    private function setOrderExternalId($marketplace = true)
    {
        if ($marketplace == true) {
            $skyhubJson = json_decode($this->order->getData('bseller_skyhub_json'), true);
            if (isset($skyhubJson['import_info']['remote_code'])) {
                $this->orderExternalId = $skyhubJson['import_info']['remote_code'];
                return;
            }
        }

        $this->orderExternalId = null;
    }


    /**
     * @return mixed
     */
    private function getOrderExternalId()
    {
        return $this->orderExternalId;
    }


    /**
     * @param bool $marketplace
     */
    private function setSalesChannel($marketplace = true)
    {
        if ($marketplace == true) {
            $configValues = Mage::getStoreConfig(self::CHANNEL_TYPE_CONFIG);
            $configValues = unserialize($configValues);
            $this->orderSalesChannel = $this->getValueChannelType($configValues);
            return;
        }

        $this->orderSalesChannel = $this->helper->getOrderStoreConfig('sales_channel');
    }

    /**
     * Get channel value of order , default's self::CHANNEL_TYPE_DEFAULT
     * @param Array $configs
     * @return string
     */
    public function getValueChannelType($configs)
    {
        if(!$configs){
            return self::CHANNEL_TYPE_DEFAULT;
        }

        $channelType = $this->getOrderChannel();

        foreach ($configs as $config) {
            if(isset($config['channel_type']) && $config['channel_type'] == $channelType){
                return $config['channel_type_value'];
            }
        }

        return self::CHANNEL_TYPE_DEFAULT;
    }

    /**
     * Set Intelipost orders delivery_quote_id to bseller json , if exists
     */
    private function setIntelipostIdCotacao()
    {
        $intelipostOrders = Mage::getModel( "basic/orders")->load($this->order->getId(), 'order_id');

        if($intelipostOrders->getId()) {
            $this->intelipostIdCotacao = $intelipostOrders->getDeliveryQuoteId();
        }
    }

    /**
     * get Intelipost orders delivery_quote_id to bseller json , if exists
     */
    public function getIntelipostIdCotacao()
    {
        return $this->intelipostIdCotacao;
    }

    /**
     * @return mixed
     */
    private function getOrderChannel(){
        $chanell = $this->order->getBsellerSkyhubChannel();
        if($chanell){
            $chanell = strtolower(str_replace(' ', '_', trim($chanell)));
        }
        return $chanell;
    }


    /**
     * @return mixed
     */
    private function getSalesChannel()
    {
        return $this->orderSalesChannel;
    }


    /**
     * @param bool $marketplace
     */
    private function setOrderPaymentCode($marketplace = true)
    {
        $paymentHelper = Mage::helper('bsellererp_payment');
        $this->orderPaymentCode = $paymentHelper->getPaymentByCode($this->order->getPayment()->getMethod());
    }

    /**
     * @return mixed
     */
    private function getOrderPaymentCode()
    {
        return $this->orderPaymentCode;
    }

    private function setOrderShippingType($marketplace = true)
    {
        if ($marketplace == true) {
            $this->orderShippingType = 1;
            return;
        }

        $this->initCarrierAndContract();
        $this->orderShippingType = $this->carrierContract ? $this->carrierContract->getTipoEntrega() : null;
    }

    private function getOrderShippingType()
    {
        return $this->orderShippingType;
    }

    /**
     * Get the correct neighborhood according if
     * is a SkyHub order or not
     *
     * @param array $address
     *
     * @return string
     */
    private function getNeighborhood($address)
    {
        $json = json_decode($this->order->getData('bseller_skyhub_json'));

        if ($json) {
            if (($address->getAddressType() == 'billing') || !$json->shipping_address->neighborhood) {
                return $json->billing_address->neighborhood;
            }

            return $json->shipping_address->neighborhood;
        }

        return $this->helper->getStreetByName('address_neighborhood', $address);
    }
}