<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Order
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Observer model
 *
 * @category   BSellerERP
 * @package    BSellerERP_Order
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Order_Model_Observer
{
    /**
     * @var BSellerERP_Order_Helper_Data
     */
    protected $helper;

    /**
     * Load helper model
     */
    public function __construct()
    {
        $this->helper = Mage::helper('bsellererp_order');
    }

    /**
     * Starting integrate order
     *
     * @param Varien_Event_Observer $observer
     * @return $this
     * @throws Mage_Core_Exception
     */
    public function salesOrderPlaceBefore(Varien_Event_Observer $observer)
    {
        /**
         * @var Mage_Sales_Model_Order $order
         */
        $order = $observer->getEvent()->getOrder();
        $order->setData('erp_status', BSellerERP_Order_Helper_Status::PENDING);

        return $this;
    }

    /**
     * Add button for resend integration in order view
     *
     * @param Varien_Event_Observer $observer
     * @return $this
     */
    public function addIntegrationButtonOrder(Varien_Event_Observer $observer)
    {
        $statusButtons = array(BSellerERP_Order_Helper_Status::BLOCKED, BSellerERP_Order_Helper_Status::INTERNAL_BLOCKED);

        if (!$this->helper->moduleIsActive()) {
            return $this;
        }

        $block = $observer->getBlock();

        if (!$block instanceof Mage_Adminhtml_Block_Sales_Order_View) {
            return $this;
        }

        $erpStatus = $block->getOrder()->getData('erp_status');
        if (!in_array($erpStatus, $statusButtons)) {
            return $this;
        }

        if($erpStatus == BSellerERP_Order_Helper_Status::BLOCKED) {
            $message = $this->helper->__('Are you sure you want to do this?');
            $url = $block->getUrl('bsellererp_order/adminhtml_index/resend');
            $block->addButton(
                'resend_order_integration',
                array(
                    'label' => $this->helper->__('Resend Integration'),
                    'onclick' => "confirmSetLocation('{$message}', '{$url}')"
                )
            );
        }

        if($erpStatus == BSellerERP_Order_Helper_Status::INTERNAL_BLOCKED) {
            $message = $this->helper->__('Are you sure you want to do this?');
            $url = $block->getUrl('bsellererp_order/adminhtml_index/approveinternally');
            $block->addButton(
                'approve_order_internally',
                array(
                    'label' => $this->helper->__('Approve Internally'),
                    'onclick' => "confirmSetLocation('{$message}', '{$url}')"
                )
            );
        }

        return $this;
    }

    //Adding mass action options to sales_grid page
    //Adding erp_status column to sales_grid page
    public function beforeBlockToHtml(Varien_Event_Observer $observer)
    {
        $block = $observer->getBlock();

        if (Mage::getStoreConfig('bsellererp_order/settings/order_grid_mass_actions')) {
            if ($block instanceOf Enterprise_SalesArchive_Block_Adminhtml_Sales_Order_Grid_Massaction
                && $block->getRequest()->getControllerName() == 'sales_order'
            ) {
                $block->addItem('bselererp_order_resendorders', array(
                    'label' => $this->helper->__('[BsellerERP] Resend orders'),
                    'url' => Mage::app()->getStore()->getUrl('bsellererp_order/adminhtml_index/resendmass'),
                ));

                $block->addItem('bselererp_order_resendpayments', array(
                    'label' => $this->helper->__('[BsellerERP] Resend payments'),
                    'url' => Mage::app()->getStore()->getUrl('bsellererp_order/adminhtml_index/resendpaymentmass'),
                ));
            }
        }

//        if (Mage::getStoreConfig('bsellererp_order/settings/order_grid_add_statuserp_column')) {
//            if ($block instanceof Mage_Adminhtml_Block_Sales_Order_Grid) {
//                $block->addColumnAfter(
//                    'erp_status',
//                    array(
//                        'header' => Mage::helper('bsellererp_order')->__('ERP Status'),
//                        'index' => 'sfo.erp_status',
//                        'filter' => 'bsellererp_order/adminhtml_sales_order_filter_erpstatuscolumn',
//                        'renderer' => 'bsellererp_order/adminhtml_sales_order_grid_erpstatuscolumn',
//                        'type' => 'text'
//                    ),
//                    'status'
//                );
//            }
//        }
    }

    public function beforeCollectionLoad(Varien_Event_Observer $observer)
    {
        if(!Mage::getStoreConfig('bsellererp_order/settings/order_grid_add_statuserp_column')) {
            return;
        }

        $collection = $observer->getOrderGridCollection();

        if (!isset($collection)) {
            return;
        }

        if (!$collection instanceof Mage_Sales_Model_Resource_Order_Grid_Collection) {
            return;
        }

        if ($this->collectionHasErpStatusSubQuery($collection)) {
            return;
        }

        if (!$this->hasOrderJoin($collection)) {
            return $this;
        }

        $collection->getSelect()->columns('order.erp_status');
    }

    /**
     * @param Mage_Sales_Model_Resource_Order_Grid_Collection $collection
     * @return bool
     */
    private function collectionHasErpStatusSubQuery($collection)
    {
        return array_reduce($collection->getSelect()->getPart('columns'), function ($result, $columnData) {
            $tableAlias = reset($columnData);
            if ($tableAlias == 'sfo') {
                $result = true;
            }

            return $result;
        }, false);
    }

    /**
     * Verifies if the collection's query has the orders join
     *
     * @param Mage_Sales_Model_Resource_Order_Grid_Collection $collection
     * @return bool
     */
    private function hasOrderJoin($collection) {
        $from = $collection->getSelect()->getPart(Zend_Db_Select::FROM);
        if (array_key_exists('order', $from)) {
            return true;
        }

        return false;
    }
}
