<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Order
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * BSeller order module helper
 *
 * @category   BSellerERP
 * @package    BSellerERP_Order
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Order_Helper_Data extends BSellerERP_Core_Helper_Data
{
    /**
     * General prefix for using in logs and events
     *
     * @var string
     */
    protected $prefix = 'order';

    const RESQUE_QUEUE_NAME = 'bsellererp_order';
    const PRODUCT_QUEUE_LIMIT = '100';
    const RESQUE_JOB_CLASS = 'BSellerERP_Order_Model_Job';

    /**
     * Get order settings store config by key
     *
     * @param $key
     * @return mixed
     */
    public function getOrderStoreConfig($key)
    {
        return Mage::getStoreConfig('bsellererp_order/settings/' . $key);
    }

    function getErpStatusFilterOptions()
    {
        $select = array(
            array('label' => '', 'value' => ''),
            array('label' => '[NI] Aguardando integração', 'value' => BSellerERP_Order_Helper_Status::PENDING),
            array('label' => '[I] Integrado', 'value' => BSellerERP_Order_Helper_Status::SUCCESS),
            array('label' => '[NI] Enfileirado', 'value' => BSellerERP_Order_Helper_Status::ENQUEUED),
            array('label' => '[I] Finalizado', 'value' => BSellerERP_Order_Helper_Status::SUCCESS_APPROVE),
            array('label' => '[NI] Bloqueado', 'value' => BSellerERP_Order_Helper_Status::BLOCKED),
            array('label' => '[I] Aprovação de pagamento não enviada', 'value' => BSellerERP_Order_Helper_Status::BLOCKED_APPROVE),
            array('label' => '[I] Desaprovação de pagamento não enviada', 'value' => BSellerERP_Order_Helper_Status::BLOCKED_DISAPPROVE),
            array('label' => '[BI] Bloqueado Internamente', 'value' => BSellerERP_Order_Helper_Status::INTERNAL_BLOCKED),
            array('label' => '[AI] Aprovado Internamente', 'value' => BSellerERP_Order_Helper_Status::INTERNAL_APPROVED)
        );

        return $select;
    }

    function getErpStatusLabel($value)
    {
        $array = $this->getErpStatusFilterOptions();

        foreach ($array as $statusItem) {
            if ($statusItem['value'] == $value) {
                return $statusItem['label'];
            }
        }

        return $value;
    }

    public function getAquirerName($payment)
    {
        $additionalInformation = $payment->getAdditionalInformation();

        if (isset($additionalInformation['1_Acquirer'])) {
            return $additionalInformation['1_Acquirer'];
        }

        if (isset($additionalInformation['1_AcquirerName'])) {
            return $additionalInformation['1_AcquirerName'];
        }
        return '';
    }

    public function getIdAutorizacao($payment)
    {
        $additionalInformation = $payment->getAdditionalInformation();
        if (isset($additionalInformation['1_AuthorizationCode']) && !empty($additionalInformation['1_AuthorizationCode'])) {
            return $additionalInformation['1_AuthorizationCode'];
        }
        if (isset($additionalInformation['1_AcquirerAuthorizationCode']) && !empty($additionalInformation['1_AcquirerAuthorizationCode'])) {
            return $additionalInformation['1_AcquirerAuthorizationCode'];
        }
        return '';
    }

    public function getNsuAutorizadora($payment)
    {
        $additionalInformation = $payment->getAdditionalInformation();

        if (isset($additionalInformation['mundipagg_creditcard_payment_capture_nsu'])) {
            return $additionalInformation['mundipagg_creditcard_payment_capture_nsu'];
        }
        return '';
    }

    function getStatusForThisReturningErpMessage($returningErpMessage, $event)
    {
        if (strpos($returningErpMessage, 'DT_EMISSAO deve ser menor ou igual a hoje') !== false) {
            return BSellerERP_Order_Helper_Status::PENDING;
        }
        if ($event == BSellerERP_Order_Helper_Event::INSERT_ORDER) {
            if (strpos($returningErpMessage, 'ja foi incluido no sistema') !== false || strpos($returningErpMessage, 'Pedido Duplicado') !== false) {
                return BSellerERP_Order_Helper_Status::SUCCESS;
            }
            return BSellerERP_Order_Helper_Status::BLOCKED;
        }
        if ($event == BSellerERP_Order_Helper_Event::APPROVE_PAYMENT) {
            if (strpos($returningErpMessage, 'Ja existe resultado de analise do MP') !== false) {
                return BSellerERP_Order_Helper_Status::SUCCESS_APPROVE;
            }
            if (strpos($returningErpMessage, 'Nao deve existir analise para o MP pedcli/seq') !== false) {
                return BSellerERP_Order_Helper_Status::PENDING;
            }
            return BSellerERP_Order_Helper_Status::BLOCKED_APPROVE;
        }
        if ($event == BSellerERP_Order_Helper_Event::DISAPPROVE_PAYMENT) {
            if (strpos($returningErpMessage, 'Ja existe resultado de analise do MP') !== false) {
                return BSellerERP_Order_Helper_Status::SUCCESS_DISAPPROVE;
            }
            return BSellerERP_Order_Helper_Status::BLOCKED_DISAPPROVE;
        }
        return null;
    }

    public function reproccessingModuleIsActive() {
        return $this->getStoreConfig('bsellererp_order/settings/reproccess_orders');
    }

    public function getAllowedNotificationIpList()
    {
        return explode(';', $this->getStoreConfig('bsellererp_order/settings/notification_ip'));
    }
}
