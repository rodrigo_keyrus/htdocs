<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Delivery
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Julio Reis <julio.reis@e-smart.com.br>
 */

/* @var $installer BSellerERP_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

//updating config for bsellererp

$configsF2 = 'default|0|bsellererp_core/settings/active|1
default|0|bsellererp_core/settings/mode|production
default|0|bsellererp_core/settings/queue|1
default|0|bsellererp_core/settings/homologation_endpoint|http://backstg.bseller.com.br/api/
default|0|bsellererp_core/settings/homologation_token|3D08698C8B196E22E0536AF3A8C0ED13
default|0|bsellererp_core/settings/debug_mode|1
default|0|bsellererp_core/settings/cnpj_filial|05753951000155
default|0|bsellererp_core/settings/output_establishment|1
default|0|bsellererp_core/settings/stock_establishment|1
default|0|bsellererp_core/settings/business_unit|1
default|0|bsellererp_core/settings/interface_name|SITE
default|0|bsellererp_core/settings/bank_code|001
default|0|bsellererp_core/settings/number_agency|1603
default|0|bsellererp_core/settings/number_account|322601
default|0|bsellererp_core/mapping/rg|
default|0|bsellererp_core/mapping/cpf|cpf
default|0|bsellererp_core/mapping/cnpj|
default|0|bsellererp_core/mapping/contributor|
default|0|bsellererp_core/mapping/state_registration|
default|0|bsellererp_core/mapping/company_name|
default|0|bsellererp_core/mapping/person_type|
default|0|bsellererp_core/mapping/legal_person|
default|0|bsellererp_core/mapping/natural_person|
default|0|bsellererp_core/mapping/tax_regime|
default|0|bsellererp_core/mapping/tax_regime_standard|
default|0|bsellererp_core/mapping/tax_regime_simple|taxvat
default|0|bsellererp_core/mapping/address_street|1
default|0|bsellererp_core/mapping/address_neighborhood|4
default|0|bsellererp_core/mapping/address_number|2
default|0|bsellererp_core/mapping/address_complement|3
default|0|bsellererp_core/mapping/reference_point|
default|0|bsellererp_core/mapping/cellphone|cellphone
default|0|bsellererp_channel/settings/active|1
default|0|bsellererp_channel/schedule/expression|0 3 * * *
default|0|bsellererp_delivery/settings/active|1
default|0|bsellererp_delivery/schedule/expression|0 3 * * *
default|0|bsellererp_tracking/settings/active|1
default|0|bsellererp_tracking/schedule/expression|*/10 * * * *
default|0|bsellererp_payment/settings/active|1
default|0|bsellererp_payment/schedule/expression|0 3 * * *
default|0|bsellererp_payment/mapping/payment|a:3:{s:18:"_1480696388871_871";a:2:{s:12:"payments_erp";s:1:"2";s:8:"payments";s:16:"mundipagg_boleto";}s:18:"_1480696419887_887";a:2:{s:12:"payments_erp";s:1:"1";s:8:"payments";s:20:"mundipagg_creditcard";}s:18:"_1481897981229_229";a:2:{s:12:"payments_erp";s:2:"10";s:8:"payments";s:4:"free";}}
default|0|bsellererp_shipping/settings/active|1
default|0|bsellererp_shipping/carrier/cron_expression|15 3 * * *
default|0|bsellererp_shipping/carrier/use_default_carrier_codes|0
default|0|bsellererp_shipping/carrier/mapping|a:6:{s:18:"_1480704453388_388";a:2:{s:12:"carriers_erp";s:16:"73939449000193-1";s:8:"carriers";s:8:"texpress";}s:18:"_1482154330428_428";a:2:{s:12:"carriers_erp";s:16:"73939449000193-1";s:8:"carriers";s:25:"matrixrate_matrixrate_rod";}s:18:"_1483984488139_139";a:2:{s:12:"carriers_erp";s:16:"73939449000193-2";s:8:"carriers";s:36:"matrixrate_free_matrixrate_scheduled";}s:18:"_1484069579407_407";a:2:{s:12:"carriers_erp";s:16:"73939449000193-3";s:8:"carriers";s:25:"matrixrate_matrixrate_exp";}s:18:"_1484309170320_320";a:2:{s:12:"carriers_erp";s:16:"73939449000193-1";s:8:"carriers";s:25:"freeshipping_freeshipping";}s:18:"_1484760953378_378";a:2:{s:12:"carriers_erp";s:16:"73939449000193-2";s:8:"carriers";s:31:"matrixrate_matrixrate_scheduled";}}
default|0|bsellererp_payment_brands/settings/active|1
default|0|bsellererp_payment_brands/schedule/expression|35 3 * * *
default|0|bsellererp_payment_brands/mapping/brands|a:6:{s:18:"_1480696332135_135";a:2:{s:3:"erp";s:1:"1";s:11:"application";s:2:"VI";}s:18:"_1480696338400_400";a:2:{s:3:"erp";s:1:"2";s:11:"application";s:2:"MC";}s:18:"_1480696345102_102";a:2:{s:3:"erp";s:1:"7";s:11:"application";s:2:"HI";}s:17:"_1480696352094_94";a:2:{s:3:"erp";s:1:"3";s:11:"application";s:2:"AE";}s:18:"_1480696357703_703";a:2:{s:3:"erp";s:1:"4";s:11:"application";s:2:"DI";}s:18:"_1480696372302_302";a:2:{s:3:"erp";s:1:"5";s:11:"application";s:2:"EL";}}
default|0|bsellererp_order/schedule/expression|*/2 * * * *
default|0|bsellererp_order/schedule/invoice_expression|*/2 * * * *
default|0|bsellererp_order/settings/active|0
default|0|bsellererp_order/settings/sale_origin|LJ
default|0|bsellererp_order/settings/sales_channel|SITE
default|0|bsellererp_order/settings/lead_time|7
default|0|bsellererp_order/settings/transit_time|5
default|0|bsellererp_order/settings/order_type|0
default|0|bsellererp_order/settings/gift_treatment|0
default|0|bsellererp_order/settings/consumption|0
default|0|bsellererp_order/settings/invoice_active|1
default|0|bsellererp_order/settings/cancel_active|1
default|0|bsellererp_order/settings/order_grid_add_statuserp_column|1
default|0|bsellererp_order/settings/order_grid_mass_actions|1
default|0|bsellererp_product/settings/active|0
default|0|bsellererp_product/settings/weight|gross
default|0|bsellererp_product/schedule/expression|*/10 * * * *
default|0|bsellererp_stock/settings/active|0
default|0|bsellererp_stock/schedule/expression|*/10 * * * *
default|0|bsellererp_attribute/settings/active|0
default|0|bsellererp_attribute/schedule/expression|* * * * *
default|0|bsellererp_attribute/mapping/groups|a:0:{}
default|0|bsellererp_attribute/mapping/attributes|a:3:{s:18:"_1481643186844_844";a:2:{s:14:"attributes_erp";s:5:"brand";s:10:"attributes";s:12:"manufacturer";}s:17:"_1481643196088_88";a:2:{s:14:"attributes_erp";s:3:"ean";s:10:"attributes";s:8:"bar_code";}s:18:"_1482093753712_712";a:2:{s:14:"attributes_erp";s:3:"ncm";s:10:"attributes";s:3:"ncm";}}
default|0|bsellererp_order/settings/gift_value|1
default|0|bsellererp_vale/settings/active|1
default|0|bsellererp_vale/schedule/expression|*/1 * * * *
default|0|bsellererp_log/settings/active|1
default|0|bsellererp_log/schedule/expression|25 3 * * *
default|0|bsellererp_log/schedule/time_alive|90
default|0|bsellererp_order/schedule/reproccess_expression|0 */1 * * *
default|0|bsellererp_order/settings/conditional_discount|0
default|0|bsellererp_order/settings/reproccess_orders|0
default|0|bsellererp_order/settings/blocked_orders_expiration_interval|1
default|0|bsellererp_core/settings/production_endpoint|http://back.bseller.com.br/api/
default|0|bsellererp_core/settings/production_token|4A632E332E7176B4E05324F3A8C089B1';

$lines = explode("\n", $configsF2);

foreach ($lines as $line) {
    $cols = explode('|', $line);
    Mage::getConfig()->saveConfig($cols[2], $cols[3], $cols[0], $cols[1]);
}

//end updating config for bsellererp

$installer->endSetup();