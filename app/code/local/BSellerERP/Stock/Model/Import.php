<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Stock
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Initialize importing stocks in application
 *
 * @category   BSellerERP
 * @package    BSellerERP_Stock
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Stock_Model_Import extends BSellerERP_Stock_Model_Schema
{
    /**
     * Content returned integration to import
     * @var object
     */
    protected $stocks;

    /**
     * Init importing stocks
     */
    public function init()
    {
        Mage::dispatchEvent($this->helper->getPrefix() . '_import_init', array('stocks' => $this->stocks));

        foreach ($this->stocks as $stock) {
            if (!$this->validate($stock)) {
                continue;
            }
            try {
                $this->createOrUpdate($stock);
            } catch (Exception $ex) {
                $this->helper->log('Error at product (sap code): ' . $stock->codigoItem . ' (' . $ex->getMessage() . ')', null, BSellerERP_Log_Model_Item_Attribute_Source_Status::ERROR, json_encode($stock));
            }
        }

        Mage::dispatchEvent($this->helper->getPrefix() . '_import_complete', array('stocks' => $this->stocks));

        return $this;
    }

    /**
     * Set data stocks
     *
     * @param $stocks
     * @return $this
     */
    public function setStocks($stocks)
    {
        $this->stocks = $stocks;

        return $this;
    }

    /**
     * Create/Update stock by response ERP
     *
     * @param $stockERP
     * @return $this
     * @throws Exception
     */
    public function createOrUpdate($stockERP)
    {
        $this->stockERP     = $stockERP;
        $productId          = Mage::helper('bsellererp_product')->getProductIdByAttribute('cod_sap', $this->stockERP->codigoItem);

        if(!$productId) {
            $this->helper->log('Impossible update stock, item not exist - SKU: ' . $this->stockERP->codigoItem);
            return $this;
        }

        $this->currentStock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($productId);

        if (!$this->currentStock->getId()) {
            //if there's no stock created for this item, create it
            $this->currentStock = Mage::getModel('catalog/product')->load($productId)->getStockItem();
            $this->currentStock->setData('stock_id', BSellerERP_Product_Model_Schema::DEFAULT_STOCK_ID);
            $this->currentStock->setData('use_config_manage_stock', 1);
            $this->currentStock->setData('manage_stock', 1);
        }

        /**
         * Adding data in schema
         */
        $this->currentStock->addData($this->prepareSchema());

        Mage::dispatchEvent($this->helper->getPrefix() . '_item_save_before', array('stock' => $this->currentStock));
        $this->currentStock->save();
        $this->helper->log('Stock updated: ' . $this->stockERP->codigoItem . ' Magento entity_id: ' . $productId, null, BSellerERP_Log_Model_Item_Attribute_Source_Status::SUCCESS, json_encode($stockERP));
        Mage::dispatchEvent($this->helper->getPrefix() . '_item_save_after', array('stock' => $this->currentStock));

        return $this;
    }

    /**
     * Validate stock for create in application
     *
     * @param object $stock
     * @return bool
     */
    protected function validate($stock)
    {
        if (!$stock->codigoItem || $stock->estoqueEstabelecimento[0]->quantidade === false) {
            $this->helper->log('Stock is not valid for importing, missing codigoItem/quantidade param');
            $this->helper->log($stock);

            return false;
        }

        return true;
    }

    public function updateStockParents()
    {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection('core_write');

        $this->selectParentsOutOfStock($readConnection);
        $this->updateStockStatus($readConnection, $writeConnection);
        $this->updateIsInStock($readConnection, $writeConnection);
    }

    public function selectParentsOutOfStock($readConnection)
    {
        $query = "select
            entity_id
        from
            (
                select
                    cpe.sku,
                    cpe.entity_id,
                    css.stock_status,
                    (
                        select
                            sum( css2.stock_status )
                        from
                            cataloginventory_stock_status css2
                        where
                            css2.product_id in(
                                select
                                    cpr.child_id
                                from
                                    catalog_product_relation cpr
                                where
                                    cpr.parent_id = cpe.entity_id
                            )
                    ) as qtdChildStatusOk
                from
                    catalog_product_entity cpe join cataloginventory_stock_status css on
                    css.product_id = cpe.entity_id join catalog_product_entity_int cpei on
                    cpei.entity_id = cpe.entity_id
                    and cpei.entity_type_id = 4
                    and cpei.attribute_id =(
                        select
                            attribute_id
                        from
                            eav_attribute
                        where
                            attribute_code = 'status'
                            and entity_type_id = 4
                    )
                    and cpei.value = 1
                where
                    cpe.type_id = 'grouped'
                    and css.stock_status = 0
            ) as tmp
        where
            tmp.stock_status = 0
            and tmp.qtdChildStatusOk > 0";

        $lines = $readConnection->fetchAll($query);

        Mage::log(json_encode($lines), null, 'saneamento.log');
    }

    public function updateStockStatus($readConnection, $writeConnection)
    {
        //UPDATE STOCK STATUS
        $query = "UPDATE cataloginventory_stock_status
                    SET stock_status = 1
                    WHERE product_id IN (
                      SELECT
                        tmp.entity_id
                      FROM
                        (
                          SELECT
                            cpe.sku,
                            cpe.entity_id,
                            css.stock_status,
                            (
                              SELECT
                                sum(css2.stock_status)
                              FROM
                                cataloginventory_stock_status css2
                              WHERE
                                css2.product_id IN (
                                  SELECT
                                    cpr.child_id
                                  FROM
                                    catalog_product_relation cpr
                                  WHERE
                                    cpr.parent_id = cpe.entity_id
                                )
                            ) AS qtdChildStatusOk,
                            csi.is_in_stock
                          FROM
                            catalog_product_entity cpe
                            JOIN cataloginventory_stock_status css
                              ON css.product_id = cpe.entity_id
                            JOIN cataloginventory_stock_item csi
                              ON csi.product_id = cpe.entity_id
                            JOIN catalog_product_entity_int cpei
                              ON cpei.entity_id = cpe.entity_id
                                 AND cpei.entity_type_id = 4
                                 AND cpei.attribute_id = (
                              SELECT
                                attribute_id
                              FROM eav_attribute
                              WHERE
                                attribute_code = 'status'
                                AND entity_type_id = 4
                            )
                                 AND cpei.value = 1
                          WHERE
                            cpe.type_id = 'grouped'
                            AND css.stock_status = 0
                        ) AS tmp
                      WHERE
                        tmp.stock_status = 0
                        AND tmp.qtdChildStatusOk > 0
                    )";
        $writeConnection->query($query);
    }

    public function updateIsInStock($readConnection, $writeConnection)
    {
        //UPDATE STOCK STATUS ITEM
        $queryItem = "UPDATE cataloginventory_stock_item
                    SET is_in_stock = 1
                    WHERE product_id IN (
                      SELECT
                        tmp.entity_id
                      FROM
                        (
                          SELECT
                            cpe.sku,
                            cpe.entity_id,
                            css.stock_status,
                            (
                              SELECT
                                sum(css2.stock_status)
                              FROM
                                cataloginventory_stock_status css2
                              WHERE
                                css2.product_id IN (
                                  SELECT
                                    cpr.child_id
                                  FROM
                                    catalog_product_relation cpr
                                  WHERE
                                    cpr.parent_id = cpe.entity_id
                                )
                            ) AS qtdChildStatusOk,
                            csi.is_in_stock
                          FROM
                            catalog_product_entity cpe
                            JOIN cataloginventory_stock_status css
                              ON css.product_id = cpe.entity_id
                            JOIN cataloginventory_stock_item csi
                              ON csi.product_id = cpe.entity_id
                            JOIN catalog_product_entity_int cpei
                              ON cpei.entity_id = cpe.entity_id
                                 AND cpei.entity_type_id = 4
                                 AND cpei.attribute_id = (
                              SELECT
                                attribute_id
                              FROM eav_attribute
                              WHERE
                                attribute_code = 'status'
                                AND entity_type_id = 4
                            )
                                 AND cpei.value = 1
                          WHERE
                            cpe.type_id = 'grouped'
                            AND csi.is_in_stock  = 0
                        ) AS tmp
                      WHERE
                        tmp.is_in_stock = 0
                        AND tmp.qtdChildStatusOk > 0
                    )";

        $writeConnection->query($queryItem);
    }
}
