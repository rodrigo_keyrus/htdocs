<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 *
 * @category   BSellerERP
 * @package    BSellerERP_Stock
 * @author     Rafael Falcão <rafael.falcao@e-smart.com.br>
 */
class BSellerERP_Stock_Model_Observer
{
    /**
     * Update stock item to parent
     *
     * @param $observer
     * @throws Mage_Core_Model_Store_Exception
     */
    public function updateParentStock($observer)
    {
        $stock = $observer->getStock();
        $parentId = $this->_getParentId($stock->getProductId());

        /** Check if parent product exists */
        if(!$parentId){
            return;
        }

        /** Check if the child product has stock */
        if(!$stock->getIsInStock()){
            return;
        }

        $writeConnection = $this->_getResource()->getConnection('core_write');;

        $table = $this->_getResource()->getTableName('cataloginventory/stock_item');
        $bind  = ['is_in_stock' => $stock->getIsInStock()];
        $where = "product_id = {$parentId}";

        try{
            $writeConnection->update($table, $bind, $where);
        } catch (Exception $e){
            Mage::log('BSellerERP Stock Observer error: '.$e->getMessage());
        }

        return;
    }

    /**
     * Get parent id from children product
     *
     * @param int $childId
     *
     * @return int
     */
    protected function _getParentId($childId)
    {
        /** @var Mage_Core_Model_Abstract $readConnection */
        $readConnection = $this->_getResource()->getConnection('core_read');

        $catalogProductRelation = $readConnection->select()
            ->from(
                $this->_getResource()->getTableName('catalog/product_relation'), array('parent_id')
            )
            ->where('child_id=?', $childId);

        $parentId = $readConnection->fetchOne($catalogProductRelation);

        return $parentId;
    }

    /**
     * @return Mage_Core_Model_Abstract
     */
    protected function _getResource()
    {
        return Mage::getSingleton('core/resource');
    }
}