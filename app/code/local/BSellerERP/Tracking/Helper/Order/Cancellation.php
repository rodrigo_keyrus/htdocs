<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Core
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Bruno Gemelli <bruno.gemelli@e-smart.com.br>
 */
class BSellerERP_Tracking_Helper_Order_Cancellation extends Mage_Core_Helper_Abstract
{
    /**
     * Force order cancellation in Magento
     *
     * @param Mage_Sales_Model_Order $order
     */
    public function cancelOrder(Mage_Sales_Model_Order $order)
    {
        if ($order->hasInvoices()) {
            /** @var \Mage_Sales_Model_Order_Invoice $invoice */
            foreach ($order->getInvoiceCollection() as $invoice) {
                $this->_cancelInvoice($invoice);
            }
        }

        if ($order->getState() === Mage_Sales_Model_Order::STATE_HOLDED) {
            $order->unhold();
        }


        if (!$order->canCancel()) {
            Mage::throwException($this->__('Order is canceled in BSeller ERP but could not be canceled in Magento.'));
        }

        $order->cancel();
    }


    /**
     * @param Mage_Sales_Model_Order_Invoice $invoice
     * @return $this
     */
    protected function _cancelInvoice(Mage_Sales_Model_Order_Invoice $invoice)
    {
        if ($invoice->isCanceled()) {
            return $this;
        }

        $invoice->cancel();

        Mage::getModel('core/resource_transaction')
            ->addObject($invoice)
            ->save();
    }
}