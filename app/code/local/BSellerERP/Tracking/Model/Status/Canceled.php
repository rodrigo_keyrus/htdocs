    <?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Tracking
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Bruno Gemelli <bruno.gemelli@e-smart.com.br>
 */
class BSellerERP_Tracking_Model_Status_Canceled extends BSellerERP_Tracking_Model_Status_Abstract
{
    const DEFAULT_STATUS = 'canceled';

    /**
     * Initialize status import
     */
    public function prepare()
    {
        try {
            $this->_getCancellationHelper()->cancelOrder($this->order);
        } catch (Mage_Exception $exception) {
            $this->order->addStatusHistoryComment($exception->getMessage());
            return $this;
        }

        /**
         * Set status
         */
        $this->order->setStatus(self::DEFAULT_STATUS);

        return $this;
    }


    /**
     * @return BSellerERP_Tracking_Helper_Order_Cancellation
     */
    protected function _getCancellationHelper()
    {
        return Mage::helper('bsellererp_tracking/order_cancellation');
    }
}
