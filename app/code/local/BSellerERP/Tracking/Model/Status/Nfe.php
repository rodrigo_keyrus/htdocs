<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Tracking
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Status model
 *
 * @category   BSellerERP
 * @package    BSellerERP_Tracking
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Tracking_Model_Status_Nfe extends BSellerERP_Tracking_Model_Status_Abstract
{
    const DEFAULT_STATUS = 'nfe';

    const XML_PATH_EMAIL_TEMPLATE = 'sales_email/nfe_gerada/template';

    const XML_PATH_EMAIL_IDENTITY = 'sales_email/nfe_gerada/identity';

    /**
     * Initialize status import
     */
    public function prepare()
    {
        $this->order->setData('erp_nfe_key', $this->tracking->notaFiscal->id);
        $this->order->setData('erp_nfe_id', $this->tracking->notaFiscal->chaveAcesso);
        $this->order->setData('erp_nfe_link', $this->tracking->notaFiscal->linkNfe);

        /**
         * add NFE data in comment (for SkyHub NFE integration)
         */
        $message = 'Chave NFE: '.$this->tracking->notaFiscal->chaveAcesso;
        $this->order->addStatusHistoryComment($message);

        /**
         * Set status
         */
        if (in_array($this->order->getStatus(), array('pending', 'processing'))) {
            $this->order->setStatus(self::DEFAULT_STATUS);
        }

        if($this->order->getBsellerSkyhub()){
            return;
        }

        try {
            $this->sendTransactionEmail();
            Mage::log($this->order->getStatus(), 'NF_transactionNew.log');
        } catch (exception $e) {
            Mage::log($e->getMessage(), 'NF_transaction_error.log');
        }

        return $this;
    }

    /**
     * Send email transaction
     * @throws Exception
     */
    public function sendTransactionEmail()
    {
        $order = $this->order;
        $storeId = $order->getStore()->getId();

        $templateId = Mage::getStoreConfig(self::XML_PATH_EMAIL_TEMPLATE, $storeId);
        $customerName = $order->getCustomerName();

        // Start store emulation process
        $appEmulation = Mage::getSingleton('core/app_emulation');
        $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($storeId);

        try {
            // Retrieve specified view block from appropriate design package (depends on emulated store)
            $paymentBlock = Mage::helper('payment')->getInfoBlock($order->getPayment())
                ->setIsSecureMode(true);
            $paymentBlock->getMethod()->setStore($storeId);
            $paymentBlockHtml = $paymentBlock->toHtml();
        } catch (Exception $exception) {
            // Stop store emulation process
            $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
            throw $exception;
        }

        // Stop store emulation process
        $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);

        $mailer = Mage::getModel('core/email_template_mailer');
        $emailInfo = Mage::getModel('core/email_info');
        $emailInfo->addTo($order->getCustomerEmail(), $customerName);
        $mailer->addEmailInfo($emailInfo);

        $mailer->setSender(Mage::getStoreConfig(self::XML_PATH_EMAIL_IDENTITY, $storeId));
        $mailer->setStoreId($storeId);
        $mailer->setTemplateId($templateId);
        $mailer->setTemplateParams([
            'order'        => $order,
            'comment'      => '',
            'billing'      => $order->getBillingAddress(),
            'payment_html' => $paymentBlockHtml
        ]);

        $mailer->send();
    }
}
