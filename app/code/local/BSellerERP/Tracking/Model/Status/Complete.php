<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Tracking
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Anderson Cardoso <anderson.cardoso@e-smart.com.br>
 */

/**
 * Status model
 *
 * @category   BSellerERP
 * @package    BSellerERP_Tracking
 * @author     Anderson Cardoso <anderson.cardoso@e-smart.com.br>
 */
class BSellerERP_Tracking_Model_Status_Complete extends BSellerERP_Tracking_Model_Status_Abstract
{
    const DEFAULT_STATUS = 'complete';

    const XML_PATH_EMAIL_TEMPLATE = 'sales_email/delivered/template';

    const XML_PATH_EMAIL_IDENTITY = 'sales_email/delivered/identity';

    public function prepare()
    {
        /**
         * Set status
         */
        $this->order->setStatus(self::DEFAULT_STATUS);

        return $this;
    }
}
