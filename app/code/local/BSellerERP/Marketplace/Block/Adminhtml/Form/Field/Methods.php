<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Marketplace
 *
 * @copyright Copyright (c) 2019 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Cristhian Andrade <cristhian.novaes@e-smart.com.br>
 */

class BSellerERP_Marketplace_Block_Adminhtml_Form_Field_Methods
    extends BSellerERP_Core_Block_Adminhtml_Form_Field_Abstract
{
    /**
     * Column payment groups ERP
     *
     * @const string
     */
    const CHANNEL_TYPE = 'channel_type';

    /**
     * Column payment groups application
     *
     * @const string
     */
    const CHANNEL_TYPE_VALUE = 'channel_type_value';

    /**
     * Renderer types
     *
     * @var array
     */
    protected $renderer = array();

    /**
     * Prepare to render
     *
     * @return void
     */
    protected function _prepareToRender()
    {
        $helper = $this->helper('bsellererp_marketplace');

        $this->addColumn(
            self::CHANNEL_TYPE,
            array(
                'label'    => $helper->__('Channel Type'),
                'class'    => 'input-text required-entry',
            )
        );

        $this->addColumn(
            self::CHANNEL_TYPE_VALUE,
            array(
                'label'    => $helper->__('Channel Type Value'),
                'class'    => 'input-text required-entry'
            )
        );

        $this->_addAfter = false;
        $this->_addButtonLabel = $helper->__('Add new');
    }
}
