<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs plelse {
                print_r('Empty Param order_id');
            }ease refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Debug
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Anderson Cardoso <anderson.cardoso@e-smart.com.br>
 */

class BSellerERP_Debug_IndexController extends Mage_Core_Controller_Front_Action {

    /**
     * Controller used unique for Debug , for security,  please keep commented code for .
     */
    public function indexAction()
    {
    }

}