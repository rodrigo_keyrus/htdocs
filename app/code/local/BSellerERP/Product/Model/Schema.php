<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Product
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Schema model
 *
 * @category   BSellerERP
 * @package    BSellerERP_Product
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Product_Model_Schema extends BSellerERP_Core_Model_Schema_Abstract
{
    /**
     * Set class_tax_id in product
     *
     * @const int
     */
    const DEFAULT_CLASS_TAX_ID = 0;

    /**
     * Set attribute stock qty in product
     *
     * @const int
     */
    const DEFAULT_STOCK_QTY = 0;

    /**
     * Set attribute stock id in product
     *
     * @const int
     */
    const DEFAULT_STOCK_ID = 1;

    /**
     * Set attribute weight qty
     *
     * @const int
     */
    const DEFAULT_WEIGHT_QTY = 0;

    /**
     * Path store config weight type
     *
     * @const string
     */
    const WEIGHT_TYPE = 'bsellererp_product/settings/weight';

    /**
     * Status active category in product
     *
     * @const string
     */
    const CATEGORY_STATUS_ACTIVE  = 'A';

    /**
     * Status disable category in product
     *
     * @const string
     */
    const CATEGORY_STATUS_DISABLE = 'I';

    /**
     * Default price if not exist in ERP
     *
     * @const decimal
     */
    const DEFAULT_PRICE = 0.00;

    /**
     * Default special price if not exist in ERP
     *
     * @const decimal
     */
    const DEFAULT_SPECIAL_PRICE = null;

    /**
     * Current product for create/update
     *
     * @var Mage_Catalog_Model_Product
     */
    public $currentProduct;

    /**
     * Product object returned by ERP
     *
     * @var object
     */
    public $productERP;

    /**
     * Returns helper object
     * @var BSellerERP_Product_Helper_Data
     */
    protected $helper;

    /**
     * Load helper data
     *
     * @var string
     */
    public $currentHelper = 'product';

    /**
     * Prepare schema and returns for adding in product instance
     *
     * @return array
     */
    protected function prepareSchema()
    {
        parent::prepareSchema();

        if (!$this->currentProduct->getId()) {
            $this->setDefaultData();
        }

        $this->setGeneralData();
        //$this->setStockData();
        $this->setMetaInformationData();
        $this->setDataSheet();
        $this->setVariations();

        /**
         * Setting predefined attributes data
         */
        $this->setPredefinedAttributes();

        return $this->getSchema();
    }

    /**
     * Add default attributes only creating new product
     *
     * @return $this
     */
    protected function setDefaultData()
    {
        $attributeSetId = false;

        /**
         * Group ID to compare with attribute set ID
         */
        if (!is_null($this->productERP->agrupamentoFichaTecnica)) {
            $groupId = $this->productERP->agrupamentoFichaTecnica->id;

            /** @var BSellerERP_Attribute_Helper_Data $attributeHelper */
            $attributeHelper = Mage::helper('bsellererp_attribute');

            $attributeSetId = $attributeHelper->getAttributeSetId($groupId);
        }

        if (!$attributeSetId) {
            $attributeSetId = $this->currentProduct->getDefaultAttributeSetId();
        }

        /**
         * Attributes updated only on creating
         */
        $this->addData(
            array(
                'name' => $this->productERP->nome,
                'type_id'           => $this->helper->getTypeId($this->productERP),
                'attribute_set_id'  => $attributeSetId,
                'tax_class_id'      => self::DEFAULT_CLASS_TAX_ID
            )
        );

        return $this;
    }

    /**
     * Setting general data for schema
     *
     * @return $this
     */
    protected function setGeneralData()
    {
        $visibility = Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE;

//        if ($this->helper->isChild($this->productERP)) {
//            $visibility = Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE;
//        }

        $data = array(
            'sku' => $this->getSku(),
            'price' => $this->getPrices()->getData('price'),
            //'special_price' => $this->getPrices()->getData('special_price'),
            'weight' => $this->getWeight(),
            'website_ids' => $this->getWebsiteIds(),
            'visibility' => $visibility,
            'cod_sap' => $this->getCodSap(),
            'tax_treatment' => $this->formatTaxTreatment(),
        );

//        if ($data['tax_treatment'] == 2) {
//            $data['visibility'] = Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE;
//        }
//
//        if ($data['tax_treatment'] == 3) {
//            $data['visibility'] = Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE;
//        }

        $itemStatus = $this->getStatus();
        if($itemStatus) {
            $data['status'] = $itemStatus;
        }

        $this->addData(
            $data
        );

        $categories = $this->getCategories();

        if (!empty($categories)) {
            $this->setData('category_ids', $categories);
        }

        return $this;
    }

    /**
     * Set stock data for schema
     *
     * @return $this
     */
    protected function setStockData()
    {
        //if is a update, dont need to set stock
        if ($this->currentProduct->getId()) {
            return $this;
        }
        $extraData   = array();

        //if configurable, grouped or bundle, don't manage stock
        if ($this->currentProduct->getTypeId() != Mage_Catalog_Model_Product_Type::TYPE_SIMPLE) {
            $generalData = array(
                'use_config_manage_stock' => 0,
                'manage_stock' => 0,
            );
        } else {
            $generalData = array(
                'use_config_manage_stock' => $this->productERP->controlaVenda,
                'manage_stock' => $this->productERP->controlaVenda,
            );
        }

        if (!$this->currentProduct->getId()) {
            $extraData = array(
                'is_in_stock' => true,
                'qty'         => self::DEFAULT_STOCK_QTY,
                'stock_id'    => self::DEFAULT_STOCK_ID,
            );
        }

        $this->addData(
            array(
                'stock_data' => array(array_merge($generalData, $extraData))
            )
        );

        return $this;
    }

    /**
     * Set Meta Information data for schema
     *
     * @return $this
     */
    protected function setMetaInformationData()
    {
        $this->addData(
            array(
                'meta_title'        => $this->productERP->nome,
                'meta_keyword'      => $this->productERP->tags,
                'meta_description'  => $this->productERP->titulo,
            )
        );

        return $this;
    }

    /**
     * Add predefined attributes in schema, attributes configured in administration
     *
     * @return $this
     */
    protected function setPredefinedAttributes()
    {
        $attributes = array(
            'ean' => $this->getEan(),
            'gtin' => $this->getEan(),
            'ncm' => $this->getNcm(),
            'dimension_height' => $this->getDimension('altura'),
            'dimension_length' => $this->getDimension('comprimento'),
            'dimension_width' => $this->getDimension('largura')
        );

        $brand = $this->getBrand();
        if ($brand) {
            $attributes['brand'] = $brand;
        }

        foreach ($attributes as $code => $value) {
            $attributeId = null;
            if (!$attribute = Mage::helper('bsellererp_attribute')->getAttribute($code)) {
                continue;
            }

            $attribute = $this->currentProduct->getResource()->getAttribute($attribute);

            //if the attribute has source model (select / multiselect), get the option ID;
            if ($attribute->getSourceModel()) {
                $attributeId = $this->getIdOptionAttribute($attribute, $value);
            }
            $this->setData($attribute->getAttributeCode(), $attributeId ? $attributeId : $value);
        }

        return $this;
    }

    /**
     * Starting association simple product in configurable
     *
     * @return $this
     */
    public function associateChildren()
    {
        $parentId = Mage::getModel('catalog/product')->getIdBySku($this->productERP->codigoItemPai);

        /**
         * @var Mage_Catalog_Model_Product $parent
         */
        $parent = Mage::getModel('catalog/product')->load($parentId);

        if (!$parent->getId()) {
            $this->helper->log('Impossible load parent product for importing children: ' . $this->productERP->codigoItemPai);

            return $this;
        }

        if ($parent->getTypeId() != Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE) {
            $this->helper->log('Impossible associate products that are not configurable: ' . $this->productERP->codigoItemPai);

            return $this;
        } else {
            if ($parent->getAttributeSetId() != $this->currentProduct->getAttributeSetId()) {
                $this->helper->log('Parent and child must have the same Group ID (agrupamentoFichaTecnica): ' . $parent->getSku() . ' -> ' . $this->currentProduct->getSku());

                return $this;
            }
        }

        $instance = Mage::getModel('bsellererp_product/type_configurable');
        $instance->setParent($parent);
        $instance->setChild($this->currentProduct);
        $instance->setProductERP($this->productERP);
        $instance->init();

        return $this;
    }

    public function associateBundleChildren() {
        $kitComponents = $this->productERP->componentesKit;
        $parentId = $this->productERP->codigoTerceiro;
        /**
         * @var Mage_Catalog_Model_Product $parent
         */
        $parent = Mage::getModel('catalog/product')->loadByAttribute('sku', $parentId);

        if (!$parent->getId()) {
            $this->helper->log('Impossible load parent product for group children: ' . $parentId);

            return $this;
        }

        if ($parent->getTypeId() != Mage_Catalog_Model_Product_Type::TYPE_BUNDLE) {
            $this->helper->log('Impossible associate products that the parent is not bundle: ' . $parentId);

            return $this;
        }

        $instance = Mage::getModel('bsellererp_product/type_bundle');
        $instance->setParent($parent);
        $instance->setChilds($kitComponents);
        $instance->setProductERP($this->productERP);
        $instance->init();

        return $this;
    }

    public function associateGroupedChildren()
    {
        $kitComponents = $this->productERP->componentesKit;
        $parentId = $this->productERP->codigoTerceiro;
        /**
         * @var Mage_Catalog_Model_Product $parent
         */
        $parent = Mage::getModel('catalog/product')->loadByAttribute('sku', $parentId);

        if (!$parent->getId()) {
            $this->helper->log('Impossible load parent product for group children: ' . $parentId);

            return $this;
        }

        if ($parent->getTypeId() != Mage_Catalog_Model_Product_Type::TYPE_GROUPED) {
            $this->helper->log('Impossible associate products that the parent is not grouped: ' . $parentId);

            return $this;
        }

        $instance = Mage::getModel('bsellererp_product/type_grouped');
        $instance->setParent($parent);
        $instance->setChilds($kitComponents);
        $instance->setProductERP($this->productERP);
        $instance->init();

        return $this;
    }

    /**
     * Prepare and insert attributes by data sheet
     *
     * @return $this
     */
    protected function setDataSheet()
    {
        $dataSheet = $this->productERP->fichaTecnica;

        if (!count($dataSheet)) {
            return $this;
        }

        foreach ($dataSheet as $data) {
            if (!$data->chave->id || !$data->chave->valor) {
                continue;
            }

            /**
             * Get attribute code
             */
            $attributeCode = Mage::helper('bsellererp_attribute')->getAttribute($data->chave->id);

            if (!$attributeCode) {
                continue;
            }

            /**
             * @var Mage_Eav_Model_Config $attribute
             */
            $attribute = $this->currentProduct->getResource()->getAttribute($attributeCode);

            /**
             * Validate attribute type
             */
            $validate = Mage::getModel('bsellererp_product/validation_attribute')
                ->validate($attribute->getFrontendInput(), $data->chave->valor);

            if (!$validate) {
                continue;
            }

            $value = $data->chave->valor;

            /**
             * Get option id by label
             */
            if ($attribute->getFrontendInput() == 'select') {
                $value = $this->getIdOptionAttribute($attribute, $this->helper->labelFilter($data->chave->valor));
            }

            $this->setData($attributeCode, $value);
        }


        return $this;
    }

    /**
     * Setting variations
     *
     * @return $this
     * @throws Mage_Core_Exception
     */
    protected function setVariations()
    {
        $variations = $this->productERP->variacoes;

        if (!count($variations)) {
            return $this;
        }

        foreach ($variations as $variation) {
            if (!$variation->idTipoVariacao || !$variation->variacao) {
                continue;
            }

            /**
             * Get attribute code
             */
            $attributeCode = Mage::helper('bsellererp_variation')->getAttribute($variation->idTipoVariacao);

            if (!$attributeCode) {
                continue;
            }

            /**
             * @var Mage_Eav_Model_Config $attribute
             */
            $attribute = $this->currentProduct->getResource()->getAttribute($attributeCode);
            $value     = $variation->variacao;

            /**
             * Get option id by label
             */
            if ($attribute->getFrontendInput() == 'select') {
                $value = $this->getIdOptionAttribute($attribute, $variation->variacao);

                if (!$value) {
                    $value = Mage::helper('bsellererp_variation')->saveVariation($attribute, $variation->variacao);
                    if(!$value) {
                        Mage::throwException('The variation "' . $variation->variacao . '" of the attribute "' . $variation->tipoVariacao . '" is not registered on system.');
                    }
                }
            }

            $this->setData($attributeCode, $value);
        }


        return $this;
    }

    /**
     * Get option ID by label
     *
     * @param  $attribute
     * @param  $optionLabel
     * @return bool|null
     * @throws Mage_Core_Exception
     */
    protected function getIdOptionAttribute($attribute, $optionLabel)
    {
        $attribute_model = Mage::getModel('catalog/resource_eav_attribute');
        $attribute = $attribute_model->load($attribute->getId());

        if (!$attribute) {
            return false;
        }

        if (!$attribute || !$attribute->usesSource()) {
            $this->helper->log('Impossible importing attribute: ' . $attribute->getFrontendLabel());
            return false;
        }

        return $attribute->getSource()->getOptionId($optionLabel);
    }

    /**
     * Set EAN code
     *
     * @return string
     */
    protected function getEan()
    {
        if (!isset($this->productERP->ean[0])) {
            return '';
        }

        if (!$code = $this->productERP->ean[0]->codEan) {
            return '';
        }

        return $code;
    }

    /**
     * Set NCM code
     *
     * @return string
     */
    protected function getNcm()
    {
        if (!$code = $this->productERP->ncm->codigoNcm) {
            return '';
        }

        return $code;
    }

    /**
     * Get dimension by type
     *
     * @param string $type
     * @return bool|string
     */
    protected function getDimension($type)
    {
        if (!$code = $this->productERP->dimensoes->$type) {
            return '';
        }

        return $code;
    }

    /**
     * Return status
     *
     * @return bool
     */
    protected function getStatus()
    {
        if(!$this->currentProduct->getId()) {
            return Mage_Catalog_Model_Product_Status::STATUS_DISABLED;
        }

        //comentando código para não atualizar status na integração quando bseller informa que ele não está ativo; Magento deve ser mandatório;
//        if (!($this->productERP->ativo == 'true')) {
//            return Mage_Catalog_Model_Product_Status::STATUS_DISABLED;
//        }
        //fim

        return null;
    }

    /**
     * Return price and special price
     *
     * @return Varien_Object
     */
    protected function getPrices()
    {
        $prices = new Varien_Object();

        /**
         * Set default price
         */
        $prices->addData(
            array(
                'price'         => self::DEFAULT_PRICE,
                'special_price' => self::DEFAULT_SPECIAL_PRICE
            )
        );

        if (!isset($this->productERP->preco[0])) {
            return $prices;
        }

        if ($price = $this->productERP->preco[0]->precoDe) {
            $prices->setData('price', $price);
        }

        if ($specialPrice = $this->productERP->preco[0]->precoPor) {
            $prices->setData('special_price', $specialPrice);
        }

        return $prices;
    }

    /**
     * Returns all categories ids
     *
     * @return array
     */
    protected function getCategories()
    {
        $estruturaMercadologica = $this->productERP->estruturaMercadologica;
        $departamentoErpId = $estruturaMercadologica->departamento->id;
        $setorErpId = $estruturaMercadologica->setor->id;

        $productCatIds = array();

        $departamentoMagento = Mage::getResourceModel('catalog/category_collection')
            ->addAttributeToFilter('bseller_code', $departamentoErpId)
            ->addFieldToFilter('level', 2)
            ->getFirstItem();

        if ($departamentoMagento && $departamentoMagento->getId()) {
            $productCatIds[] = $departamentoMagento->getId();
        } else {
            return array();
        }

        $setorMagento = Mage::getResourceModel('catalog/category_collection')
            ->addAttributeToFilter('bseller_code', $setorErpId)
            ->addFieldToFilter('level', $departamentoMagento->getId())
            ->getFirstItem();

        if ($setorMagento && $setorMagento->getId()) {
            $productCatIds[] = $setorMagento->getId();
        }

        return $productCatIds;
    }

    /**
     * Return all website ids
     *
     * @return array
     */
    protected function getWebsiteIds()
    {
        $currentWebsite = array($this->store->getWebsiteId());

        if (!$this->currentProduct->getId()) {
            return $currentWebsite;
        }

        $websiteIds = array_unique(
            array_merge($currentWebsite, $this->currentProduct->getWebsiteIds())
        );

        return $websiteIds;
    }

    /**
     * Returns wight by type defined in administrator
     *
     * @return int
     */
    protected function getWeight()
    {
        if (!count($this->productERP->peso)) {
            return self::DEFAULT_WEIGHT_QTY;
        }

        switch (Mage::getStoreConfig(self::WEIGHT_TYPE)) {
            case 'gross':
                return $this->productERP->peso->bruto;

                break;
            case 'unitary':
                return $this->productERP->peso->unitario;

                break;
        }

        return self::DEFAULT_WEIGHT_QTY;
    }

    /**
     * Returns SKU code
     *
     * @return string
     */
    protected function getSku()
    {
        if ($sku = ($this->productERP->codigoFornecedor ? $this->productERP->codigoFornecedor : $this->productERP->codigoTerceiro)) {
            return $sku;
        }

        return $this->productERP->codigoItem;
    }

    protected function getCodSap()
    {
        return $this->productERP->codigoTerceiro;
    }

    protected function formatTaxTreatment()
    {
        $dePara = array(1 => array(1),
            2 => array(19, 20),
            3 => array(12, 18));

        foreach ($dePara as $key => $item) {
            if (in_array($this->productERP->controleItem->id, $item)) {
                return $key;
            }
        }

        Mage::throwException('Invalid attribute "controleItem" (codFornecedor: ' . $this->productERP->codigoFornecedor . ', codTerceiro . ' . $this->productERP->codigoTerceiro . ')');
    }

    protected function getBrand()
    {
        if (!Mage::getStoreConfig('bsellererp_product/settings/integrate_brands')) {
            return null;
        }
        $bsellerBrandCode = $this->productERP->marca->id;
        if ($bsellerBrandCode) {
            $brand = Mage::getModel('zeon_manufacturer/manufacturer')->getCollection()
                ->addFieldToFilter('bseller_code', $bsellerBrandCode)
                ->getFirstItem();
            if ($brand && $brand->getManufacturerId()) {
                return $brand->getManufacturerId();
            } else {
                //create brand
                $brand = Mage::getModel('zeon_manufacturer/manufacturer');
                $brand->setManufacturerName($this->productERP->marca->nome);
                $brand->setStatus(0);
                $brand->setIsDisplayHome(0);
                $brand->setIdentifier(Mage::getModel('catalog/category')->formatUrlKey($this->productERP->marca->nome));
                $brand->setData('bseller_code', $this->productERP->marca->id);
                $brand->setCorporateName($this->productERP->marca->nome);
                try {
                    $brand->save();
                    return $brand->getManufacturerId();
                } catch (Exception $ex) {
                    $this->helper->log("Brand couldn't be created: " . $this->productERP->marca->nome);
                }
                return null;
            }
        }
        return null;
    }
}
