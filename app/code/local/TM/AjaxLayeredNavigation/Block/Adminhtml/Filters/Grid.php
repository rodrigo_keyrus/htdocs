<?php
class TM_AjaxLayeredNavigation_Block_Adminhtml_Filters_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('filtersGrid');
        // This is the primary key of the database
        $this->setDefaultSort('filters_id');
        $this->setDefaultDir('ASC');
        $this->setUseAjax(true);
        $this->setSaveParametersInSession(true);
    }

    protected function _getStore()
    {
        $storeId = (int) $this->getRequest()->getParam('store', 0);
        return Mage::app()->getStore($storeId);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('ajaxlayerednavigation/filters')->getCollection();
        $collection->getSelect()
            ->join(array('tap1' => Mage::getResourceModel('catalog/product_attribute_collection')->getMainTable()),
                'tap1.attribute_id = main_table.attribute_id',
                 array('frontend_label' => 'frontend_label'))
            ->reset(Zend_Db_Select::COLUMNS)
            ->columns(array('filters_id', 'f_position'))
            ->columns(array('frontend_label'), 'tap1')
        ;

        $this->setCollection($collection);
        return parent::_prepareCollection();;
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('filters_id');
        $this->getMassactionBlock()->setFormFieldName('filters');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('ajaxlayerednavigation')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('ajaxlayerednavigation')->__('Are you sure?')
        ));

        return $this;
    }

    protected function _prepareColumns()
    {
        $this->addColumn('filters_id', array(
            'header'        => Mage::helper('ajaxlayerednavigation')->__('ID'),
            'type'          => 'number',
            'align'         => 'right',
            'index'         => 'filters_id',
            'filter_index'  => 'main_table.filters_id'
        ));

        $this->addColumn('frontend_label', array(
            'header'        => Mage::helper('ajaxlayerednavigation')->__('Attribute Name'),
            'align'         => 'left',
            'index'         => 'frontend_label',
            'filter_index'  => 'tap1.frontend_label'
        ));

        $this->addColumn('f_position', array(
            'header'        => Mage::helper('ajaxlayerednavigation')->__('Position'),
            'align'         => 'left',
            'width'			=> '50px',
            'index'         => 'f_position',
            'filter_index'  => 'main_table.position'
        ));

        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getFiltersId()));
    }
}
