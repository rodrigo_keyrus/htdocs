<?php
class TM_AjaxLayeredNavigation_Block_Adminhtml_Filters_Edit_Tab_Options
    extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Set grid params
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('ajaxlayerednavigation_options');
        $this->setDefaultSort('foption_id');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    /**
     * Retirve currently edited product model
     *
     * @return TM_AffiliateSuite_Model_Proframe
     */

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('ajaxlayerednavigation/options')->getCollection()
            ->addFieldToFilter('filters_id', Mage::registry('ajaxlayerednavigation_filters')->getId());;
        $collection->getSelect()
            ->join(array('eaov' => Mage::getSingleton('core/resource')->getTableName('eav/attribute_option_value')),
                'eaov.option_id = main_table.option_id',
                 array('value' => 'value'))
            ->where('eaov.store_id = ?', 0)
            ->reset(Zend_Db_Select::COLUMNS)
            ->columns(array('foption_id', 'option_id', 'position'))
            ->columns(array('value'), 'eaov');

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * Add columns to grid
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareColumns()
    {
        $this->addColumn('foption_id', array(
            'header'    => Mage::helper('ajaxlayerednavigation')->__('ID'),
            'sortable'  => true,
            'width'     => 60,
            'index'     => 'foption_id'
        ));
        $this->addColumn('value', array(
            'header'    => Mage::helper('ajaxlayerednavigation')->__('Option Name'),
            'index'     => 'value'
        ));

        $this->addColumn('position', array(
            'header'            => Mage::helper('ajaxlayerednavigation')->__('Position'),
            'name'              => 'position',
            'index'             => 'position',
            'width'             => 60
        ));

        return parent::_prepareColumns();
    }

    /**
     * Rerieve grid URL
     *
     * @return string
     */
    public function getGridUrl()
    {
        $id = Mage::registry('ajaxlayerednavigation_filters')->getId();

        return $this->getUrl('*/*/optionsGrid', array('id' => $id));
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/option', array('id' => $row->getFoptionId()));
    }

}
