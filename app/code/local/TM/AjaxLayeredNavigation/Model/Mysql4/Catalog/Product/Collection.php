<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  TM
 * @package   TM_AjaxLayeredNavigation
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Evandro Veloso Gomes <evandro.gomes@e-smart.com.br>
 */

class TM_AjaxLayeredNavigation_Model_Mysql4_Catalog_Product_Collection extends Mage_Catalog_Model_Resource_Product_Collection
{
    public function _initSelect()
    {
        $resource = Mage::getSingleton('core/resource');

        /** @var $conn Varien_Db_Adapter_Pdo_Mysql */
        $conn = $resource->getConnection('core_read');

        $this->_select = new TM_AjaxLayeredNavigation_Model_Mysql4_Catalog_Product_Select($conn);

        $fieldsToSelect = array(
            "e.entity_id as entity_id",
            "e.type_id as type_id",
            "e.name AS name",
            "e.created_at as created_at",
            "e.sku as sku",
            "e.seller_order_quantity"
        );

        $resource = Mage::getSingleton('core/resource');

        /** @var $conn Varien_Db_Adapter_Pdo_Mysql */
        $conn = $resource->getConnection('core_read');

        $this->_select = new TM_AjaxLayeredNavigation_Model_Mysql4_Catalog_Product_Select($conn);

        if ($this->isEnabledFlat()) {
            $this->getSelect()->from(array(self::MAIN_TABLE_ALIAS => $this->getEntity()->getFlatTableName()), $fieldsToSelect);
        } else {
            $this->getSelect()->from(array(self::MAIN_TABLE_ALIAS => $this->getEntity()->getEntityTable()), $fieldsToSelect);
        }

        $this->_addJoins();

        $this->getSelect()
            ->where("e.status = ?", Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
            ->group("e.entity_id");
    }

    private function _addJoins()
    {
        $resource = Mage::getSingleton('core/resource');

        if ($this->isEnabledFlat()) {
            $tableProduct = $this->getEntity()->getFlatTableName();
        } else {
            $tableProduct  = $this->getEntity()->getEntityTable();
        }

        $this->addAttributeToSelect('small_image');

        $this->getSelect()->join(array('linked' => $resource->getTableName('catalog_product_link')),
                'linked.`product_id` = e.`entity_id`',
                array()
            )
            ->joinLeft(array('son' => $tableProduct),
                "(son.entity_id = linked.linked_product_id)
                    AND son.status = " . Mage_Catalog_Model_Product_Status::STATUS_ENABLED . "
                ",
                array()
            );
    }

    /**
     * Join Product Price Table with left-join possibility
     *
     * @see Mage_Catalog_Model_Resource_Product_Collection::_productLimitationJoinPrice()
     * @return Esmart_Catalog_Model_Resource_Product_Collection
     */
    protected function _productLimitationPrice($joinLeft = false)
    {
        return $this;
    }

    /**
     * Add attribute to entities in collection
     * If $attribute=='*' select all attributes
     *
     * @param array|string|integer|Mage_Core_Model_Config_Element $attribute
     * @param false|string $joinType
     * @return Esmart_Catalog_Model_Resource_Product_Collection
     */
    public function addAttributeToSelect($attribute, $joinType = false)
    {
        $cleanAttributes = $attribute;
        if (is_array($attribute)) {
            $deleteKeys = [
                'name',
                'price',
                'special_price',
                'price_type',
                'price_view',
                'price_discount_percent',
                'msrp_display_actual_price_type',
                'seller_order_quantity'
            ];
            $cleanAttributes = array_diff($attribute, $deleteKeys);
        }

        return parent::addAttributeToSelect($cleanAttributes, $joinType);
    }

    /**
     * Get SQL for get record count
     *
     * @param bool $resetLeftJoins
     * @return Varien_Db_Select
     */
    protected function _getSelectCountSql($select = null, $resetLeftJoins = true)
    {
        $this->_renderFilters();
        $countSelect = (is_null($select)) ?
            $this->_getClearSelect() :
            $this->_buildClearSelect($select);
        // Clear GROUP condition for count method
        $countSelect->reset(Zend_Db_Select::GROUP);
        $countSelect->columns('COUNT(DISTINCT e.entity_id)');

        return $countSelect;
    }
}