<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  TM
 * @package   TM_AjaxLayeredNavigation
 *
 * @copyright Copyright (c) 2018 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Evandro Veloso Gomes <evandro.gomes@e-smart.com.br>
 */

class TM_AjaxLayeredNavigation_Model_Mysql4_Catalog_Product_Select extends Varien_Db_Select
{
    /**
     * Adds a WHERE condition to the query by AND.
     *
     * If a value is passed as the second param, it will be quoted
     * and replaced into the condition wherever a question-mark
     * appears. Array values are quoted and comma-separated.
     *
     * <code>
     * // simplest but non-secure
     * $select->where("id = $id");
     *
     * // secure (ID is quoted but matched anyway)
     * $select->where('id = ?', $id);
     *
     * // alternatively, with named binding
     * $select->where('id = :id');
     * </code>
     *
     * Note that it is more correct to use named bindings in your
     * queries for values other than strings. When you use named
     * bindings, don't forget to pass the values when actually
     * making a query:
     *
     * <code>
     * $db->fetchAll($select, array('id' => 5));
     * </code>
     *
     * @param string $cond The WHERE condition.
     * @param string $value OPTIONAL A single value to quote into the condition.
     * @param constant $type OPTIONAL The type of the given value
     * @return Varien_Db_Select This Zend_Db_Select object.
     */
    public function where($cond, $value = null, $type = null)
    {
        if (strpos($cond, 'e.min_price') !== false) {
            $cond = str_replace('e.min_price', 'final_price', $cond);

            $this->addPriceJoins();
        }

        $columns = $this->getPart(Zend_Db_Select::COLUMNS);

        // Is a 'corrupt' query for price range or category quantity
        if (count($columns) <= 2) {
            if (array_key_exists(0, $columns) && ($columns[0][0] == 'count_table')) {
                $this->group($columns[0][0] . '.' . $columns[0][1]);
            } elseif (!array_key_exists(0, $columns) || ($columns[0][0] != 'manufacturer_idx1')) {
                $this->reset(Zend_Db_Select::GROUP);
            }
        }

        return parent::where($cond, $value, $type);
    }

    /**
     * Adds a WHERE condition to the query by OR.
     *
     * Otherwise identical to where().
     *
     * @param string   $cond  The WHERE condition.
     * @param mixed    $value OPTIONAL The value to quote into the condition.
     * @param int      $type  OPTIONAL The type of the given value
     * @return Zend_Db_Select This Zend_Db_Select object.
     *
     * @see where()
     */
    public function orWhere($cond, $value = null, $type = null)
    {
        if (strpos($cond, 'e.min_price') !== false) {
            $cond = str_replace('e.min_price', 'final_price', $cond);

            $this->addPriceJoins();
        }

        return parent::orWhere($cond, $value, $type);
    }

    public function columns($columns)
    {
        if (is_array($columns)) {
            foreach ($columns as $i => $column) {
                if (strpos($column, 'price') !== false) {
                    $this->addPriceJoins();

                    $columns[$i] = str_replace('e.', '', $column);
                }
            }
        }

        parent::columns($columns);
    }

    private function _addManufacturerNameField()
    {
        $from = $this->getPart(Zend_Db_Select::FROM);
        if (array_key_exists('manufacturer', $from)) {
            return false;
        }

        $resource = Mage::getSingleton('core/resource');

        $this->join(
            array("manufacturer" => $resource->getTableName('zeon_manufacturer/manufacturer')),
            "manufacturer.manufacturer_id = e.manufacturer",
            array()
        );

        $columns = $this->getPart(Zend_Db_Select::COLUMNS);

        $columns[] = array(
            'manufacturer',
            'manufacturer_name',
            'manufacturer_name'
        );

        $this->setPart(Zend_Db_Select::COLUMNS, $columns);

        return true;
    }

    public function addPriceJoins()
    {
        $resource = Mage::getSingleton('core/resource');

        $from = $this->getPart(Zend_Db_Select::FROM);
        if (array_key_exists('price_index', $from)) {
            return false;
        }

        if ($from['e']['tableName'] == $resource->getTableName('catalog_product_index_price')) {
            return false;
        }

        if (!array_key_exists('inve', $from)) {
            $this->joinLeft(array('inve' => $resource->getTableName('cataloginventory/stock_status')),
                "(inve.product_id = son.entity_id)
                        AND (inve.website_id = " . Mage::app()->getStore()->getWebsiteId() . ")
                        AND (inve.stock_id = '1')
                    ",
                []
            );
        }

        $customer = Mage::getSingleton('customer/session');

        if (!array_key_exists('price_index', $from)) {
            $this->joinLeft(array('price_index' => $resource->getTableName('catalog_product_index_price')),
                "price_index.entity_id = son.entity_id
                        AND price_index.website_id = " . Mage::app()->getStore()->getWebsiteId() . "
                        AND price_index.customer_group_id = " . $customer->getCustomerGroupId() . "
                        AND inve.stock_status = " . Mage_CatalogInventory_Model_Stock_Status::STATUS_IN_STOCK . "
                    ",
                []
            );
        }

        return true;
    }

    /**
     * Adds a row order to the query.
     *
     * @param mixed $spec The column(s) and direction to order by.
     * @return Zend_Db_Select This Zend_Db_Select object.
     */
    public function order($spec)
    {
        $specClean = str_replace('e.', '', $spec);
        $specClean = str_replace('seller_order_quantity_value', 'e.seller_order_quantity', $specClean);
        if (strpos($specClean, 'manufacturer') !== false) {
            if (!$this->_addManufacturerNameField()) {
                return $this;
            }

            $specClean = str_replace('manufacturer', 'manufacturer.manufacturer_name', $specClean);
        } elseif (strpos($specClean, 'price_index.') !== false) {
            $this->addPriceJoins();

            $specClean = str_replace('price_index.', 'MIN(', $specClean);
            $specClean = str_replace('_price', '_price)', $specClean);
        } elseif (strpos($specClean, 'price_discount_percent') !== false) {
            $this->addPriceJoins();

            $specClean = str_replace('price_discount_percent', '(MIN(price_index.price) - MIN(price_index.final_price)) / MIN(price_index.price)', $specClean);
        }

        return parent::order($specClean);
    }
}