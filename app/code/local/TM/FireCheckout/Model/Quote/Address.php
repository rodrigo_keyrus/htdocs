<?php

class TM_FireCheckout_Model_Quote_Address extends Mage_Sales_Model_Quote_Address
// Unirgy_DropshipSplit_Model_Quote_Address
{
    /**
     * Validate address attribute values
     *
     * @return bool
     */
    public function validate()
    {
        $errors = array();
        $helper = Mage::helper('customer');
        $this->implodeStreetAddress();
        $formConfig = Mage::getStoreConfig('firecheckout/address_form_status');

        if (!Zend_Validate::is($this->getFirstname(), 'NotEmpty')) {
            $errors[] = $helper->__('Please enter the first name.');
        }
        if (!Zend_Validate::is($this->getLastname(), 'NotEmpty')) {
            $errors[] = $helper->__('Please enter the last name.');
        }

        if ('required' === $formConfig['company']
            && !Zend_Validate::is($this->getCompany(), 'NotEmpty')) {

            $errors[] = $helper->__('Please enter the company.'); // translate
        }

        if ('required' === $formConfig['street1']
            && !Zend_Validate::is($this->getStreet(1), 'NotEmpty')) {

            $errors[] = $helper->__('Please enter the street.');
        }

        if ('required' === $formConfig['city']
            && !Zend_Validate::is($this->getCity(), 'NotEmpty')) {

            $errors[] = $helper->__('Please enter the city.');
        }

        if ('required' === $formConfig['telephone']
            && !Zend_Validate::is($this->getTelephone(), 'NotEmpty')) {

            $errors[] = $helper->__('Please enter the telephone number.');
        }

        if ('required' === $formConfig['fax']
            && !Zend_Validate::is($this->getFax(), 'NotEmpty')) {

            $errors[] = $helper->__('Please enter the fax.'); // translate
        }

        $_havingOptionalZip = Mage::helper('directory')->getCountriesWithOptionalZip();
        if ('required' === $formConfig['postcode']
            && !in_array($this->getCountryId(), $_havingOptionalZip)
            && !Zend_Validate::is($this->getPostcode(), 'NotEmpty')) {

            $errors[] = $helper->__('Please enter the zip/postal code.');
        }

        if ('required' === $formConfig['country_id']
            && !Zend_Validate::is($this->getCountryId(), 'NotEmpty')) {

            $errors[] = $helper->__('Please enter the country.');
        }

        if ('required' === $formConfig['region']
            && $this->getCountryModel()->getRegionCollection()->getSize()
            && !Zend_Validate::is($this->getRegionId(), 'NotEmpty')) {

            $errors[] = $helper->__('Please enter the state/province.');
        }

        if (empty($errors) || $this->getShouldIgnoreValidation()) {
            return true;
        }
        return $errors;
    }


    /**
     * Rewrite
     *
     * @return array|mixed
     */
    public function getAllItems()
    {
        // We calculate item list once and cache it in three arrays - all items, nominal, non-nominal
        $cachedItems = $this->_nominalOnly ? 'nominal' : ($this->_nominalOnly === false ? 'nonnominal' : 'all');
        $key = 'cached_items_' . $cachedItems;
        if (!$this->hasData($key)) {
            // For compatibility  we will use $this->_filterNominal to divide nominal items from non-nominal
            // (because it can be overloaded)
            // So keep current flag $this->_nominalOnly and restore it after cycle
            $wasNominal = $this->_nominalOnly;
            $this->_nominalOnly = true; // Now $this->_filterNominal() will return positive values for nominal items

            $quoteItems = $this->getQuote()->getItemsCollection();
            $addressItems = $this->getItemsCollection();

            $items = array();
            $nominalItems = array();
            $nonNominalItems = array();
            if ($this->getQuote()->getIsMultiShipping() && $addressItems->count() > 0) {
                foreach ($addressItems as $aItem) {
                    if ($aItem->isDeleted()) {
                        continue;
                    }

                    if (!$aItem->getQuoteItemImported()) {
                        $qItem = $this->getQuote()->getItemById($aItem->getQuoteItemId());
                        if ($qItem) {
                            $aItem->importQuoteItem($qItem);
                        }
                    }
                    $items[] = $aItem;
                    if ($this->_filterNominal($aItem)) {
                        $nominalItems[] = $aItem;
                    } else {
                        $nonNominalItems[] = $aItem;
                    }
                }
            } else {
                /*
                * For virtual quote we assign items only to billing address, otherwise - only to shipping address
                */
                $addressType = $this->getAddressType();
                $canAddItems = $this->getQuote()->isVirtual()
                    ? ($addressType == self::TYPE_BILLING)
                    : ($addressType == self::TYPE_SHIPPING);

                if ($canAddItems) {
                    foreach ($quoteItems as $qItem) {
                        // Jumps Beauty Club Items
                        if ($qItem->getProduct()->getTaxTreatment() == Mage::helper('esmart_bc')->taxTreatment()) {
                            continue;
                        }

                        if ($qItem->isDeleted()) {
                            continue;
                        }
                        $items[] = $qItem;
                        if ($this->_filterNominal($qItem)) {
                            $nominalItems[] = $qItem;
                        } else {
                            $nonNominalItems[] = $qItem;
                        }
                    }
                }
            }

            // Cache calculated lists
            $this->setData('cached_items_all', $items);
            $this->setData('cached_items_nominal', $nominalItems);
            $this->setData('cached_items_nonnominal', $nonNominalItems);

            $this->_nominalOnly = $wasNominal; // Restore original value before we changed it
        }

        $items = $this->getData($key);
        return $items;
    }
}
