<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright   Copyright (c) 2013 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Order information tab
 *
 * @category   Mage
 * @package    Mage_Adminhtml
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Intelipost_Basic_Block_Adminhtml_Sales_Order_View_Tab_Info
    extends Mage_Adminhtml_Block_Sales_Order_View_Tab_Info
{
    public function getTemplate()
    {
        $order = $this->getOrder();
        $basicOrders = Mage::getModel("basic/orders")->load($order->getId(), 'order_id');  
        if (count($basicOrders->getData()))
        {
            $this->setQuoteId($basicOrders->getDeliveryQuoteId());
            $this->setDeliveryMethodId($basicOrders->getDeliveryMethodId());

            return 'intelipost/sales/order/view/tab/info.phtml';
        }

        if ($order->getShippingMethod() == Mage::helper('quote')->getFallbackMethod())
        {

            return 'intelipost/sales/order/view/tab/info.phtml';
        }

        return parent::getTemplate();     
        
    }

    public function getIntelipostQuoteUrl()
    {
        return 'https://secure.intelipost.com.br/quote-detail/' . $this->getQuoteId();
    }

}
