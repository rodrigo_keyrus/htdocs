<?php

$installer = $this;

$installer->startSetup();

$installer->getConnection()
    ->addColumn('shipping_matrixrate', 'method_type', array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'comment' => 'Method type'
    ));

$installer->endSetup();


