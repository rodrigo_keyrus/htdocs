<?php


class Webshopapps_Matrixrate_Block_Adminhtml_Shipping_Carrier_Matrixrate_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	
    /**
     * Prepare table columns
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'dest_country_id',
            [
                'header'  => 'dest_country_id',
                'index'   => 'dest_country_id',
                'default' => 'BR'
            ]
        );

        $this->addColumn(
            'dest_region_id',
            [
                'header'  => 'dest_region_id',
                'index'   => 'dest_region',
                'default' => '0'
            ]
        );
        
        $this->addColumn(
            'dest_city',
            [
                'header'  => 'dest_city',
                'index'   => 'dest_city',
                'default' => ''
            ]
        );

        $this->addColumn(
            'dest_zip',
            [
                'header' => 'dest_zip',
                'index'  => 'dest_zip'
            ]
        );

        $this->addColumn(
            'dest_zip_to',
            [
                'header' => 'dest_zip_to',
                'index'  => 'dest_zip_to'
            ]
        );

        $this->addColumn(
            'condition_from_value',
            [
                'header' => 'condition_from_value',
                'index'  => 'condition_from_value'
            ]
        );

        $this->addColumn(
            'condition_to_value',
            [
                'header' => 'condition_to_value',
                'index'  => 'condition_to_value'
            ]
        );

        $this->addColumn(
            'price',
            [
                'header' => 'price',
                'index'  => 'price'
            ]
        );

        $this->addColumn(
            'cost',
            [
                'header' => 'cost',
                'index'  => 'cost'
            ]
        );
        
        $this->addColumn(
            'delivery_type',
            [
                'header' => 'delivery_type',
                'index'  => 'delivery_type'
            ]
        );

        $this->addColumn(
            'method_type',
            [
                'header' => 'method_type',
                'index'  => 'method_type'
            ]
        );

        return Mage_Adminhtml_Block_Widget_Grid::_prepareColumns();
    }
    
   protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('matrixrate_shipping/carrier_matrixrate_collection');
        $collection->setConditionFilter($this->getConditionName())
            ->setWebsiteFilter($this->getWebsiteId());

        $this->setCollection($collection);

        return Mage_Adminhtml_Block_Widget_Grid::_prepareCollection();
    }
    
      /**
         * Website filter
         *
         * @var int
         */
        protected $_websiteId;
    
        /**
         * Condition filter
         *
         * @var string
         */
        protected $_conditionName;
    
        /**
         * Define grid properties
         *
         * @return void
         */
        public function __construct()
        {
            parent::__construct();
            $this->setId('shippingTablerateGrid');
            $this->_exportPageSize = 10000;
        }
    
        /**
         * Set current website
         *
         * @param int $websiteId
         * @return Mage_Adminhtml_Block_Shipping_Carrier_Tablerate_Grid
         */
        public function setWebsiteId($websiteId)
        {
            $this->_websiteId = Mage::app()->getWebsite($websiteId)->getId();
            return $this;
        }
    
        /**
         * Retrieve current website id
         *
         * @return int
         */
        public function getWebsiteId()
        {
            if (is_null($this->_websiteId)) {
                $this->_websiteId = Mage::app()->getWebsite()->getId();
            }
            return $this->_websiteId;
        }
    
        /**
         * Set current website
         *
         * @param int $websiteId
         * @return Mage_Adminhtml_Block_Shipping_Carrier_Tablerate_Grid
         */
        public function setConditionName($name)
        {
            $this->_conditionName = $name;
            return $this;
        }
    
        /**
         * Retrieve current website id
         *
         * @return int
         */
        public function getConditionName()
        {
            return $this->_conditionName;
    }
	
}