<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright   Copyright (c) 2010 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Delete button for shipping table rates
 *
 * @category   Mage
 * @package    Mage_Adminhtml
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Webshopapps_Matrixrate_Block_Adminhtml_System_Config_Form_Field_Deletematrix
    extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    /**
     * Return element html
     *
     * @param Varien_Data_Form_Element_Abstract $element
     * @return string
     */
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $this->setData('element', $element);

        /** @var Mage_Core_Model_Website $website */
        $website = Mage::app()->getWebsite($this->getRequest()->getParam('website'));

        /** @var Mage_Adminhtml_Block_Widget_Button $buttonBlock */
        $buttonBlock = $this->getLayout()->createBlock('adminhtml/widget_button');
        $buttonBlock->setData(
            [
                'label'   => $this->__('Delete from %s', $website->getName()),
                'onclick' => 'setLocation(\'' . $this->getUrl('*/*/deletematrix', ['id' => $website->getId()]) . '\')',
                'class'   => ''
            ]
        );

        return $buttonBlock->toHtml();
    }
}
