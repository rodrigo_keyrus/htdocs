<?php
/**
 * Webshopapps Shipping Module
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * Shipping MatrixRates
 *
 * @category   Webshopapps
 * @package    Webshopapps_Matrixrate
 * @copyright  Copyright (c) 2010 Zowta Ltd (http://www.webshopapps.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Karen Baker <sales@webshopapps.com>
*/
class Webshopapps_Matrixrate_Model_Mysql4_Carrier_Matrixrate extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init('shipping/matrixrate', 'pk');
    }

    public function getNewRate(Mage_Shipping_Model_Rate_Request $request,$zipRangeSet=0)
    {
        $read = $this->_getReadAdapter();
        $write = $this->_getWriteAdapter();

		$postcode = $request->getDestPostcode();
        $zendFilter = new Zend_Filter_Digits();
        $postcode = $zendFilter->filter($postcode);

        $table = Mage::getSingleton('core/resource')->getTableName('matrixrate_shipping/matrixrate');

		if ($zipRangeSet && is_numeric($postcode)) {
			#  Want to search for postcodes within a range
			$zipSearchString = ' AND '.$postcode.' BETWEEN dest_zip AND dest_zip_to )';
		} else {
			$zipSearchString = $read->quoteInto(" AND ? LIKE dest_zip )", $postcode);
		}

		for ($j=0;$j<10;$j++)
		{

			$select = $read->select()->from($table);
			switch($j) {
				case 0:
					$select->where(
						$read->quoteInto(" (dest_country_id=? ", $request->getDestCountryId()).
							$read->quoteInto(" AND dest_region_id=? ", $request->getDestRegionId()).
							$read->quoteInto(" AND STRCMP(LOWER(dest_city),LOWER(?)) = 0  ", $request->getDestCity()).
							$zipSearchString
						);
					break;
				case 1:
					$select->where(
						$read->quoteInto(" (dest_country_id=? ", $request->getDestCountryId()).
							$read->quoteInto(" AND dest_region_id=?  AND dest_city=''", $request->getDestRegionId()).
							$zipSearchString
						);
					break;
				case 2:
					$select->where(
						$read->quoteInto(" (dest_country_id=? ", $request->getDestCountryId()).
							$read->quoteInto(" AND dest_region_id=? ", $request->getDestRegionId()).
							$read->quoteInto(" AND STRCMP(LOWER(dest_city),LOWER(?)) = 0  AND dest_zip='')", $request->getDestCity())
						);
					break;
				case 3:
					$select->where(
					   $read->quoteInto("  (dest_country_id=? ", $request->getDestCountryId()).
							$read->quoteInto(" AND STRCMP(LOWER(dest_city),LOWER(?)) = 0  AND dest_region_id='0'", $request->getDestCity()).
							$zipSearchString
					   );
					break;
				case 4:
					$select->where(
					   $read->quoteInto("  (dest_country_id=? ", $request->getDestCountryId()).
							$read->quoteInto(" AND STRCMP(LOWER(dest_city),LOWER(?)) = 0  AND dest_region_id='0' AND dest_zip='') ", $request->getDestCity())
					   );
					break;
				case 5:
					$select->where(
						$read->quoteInto("  (dest_country_id=? AND dest_region_id='0' AND dest_city='' ", $request->getDestCountryId()).
							$zipSearchString
						);
					break;
				case 6:
					$select->where(
					   $read->quoteInto("  (dest_country_id=? ", $request->getDestCountryId()).
							$read->quoteInto(" AND dest_region_id=? AND dest_city='' AND dest_zip='') ", $request->getDestRegionId())
					   );
					break;

				case 7:
					$select->where(
					   $read->quoteInto("  (dest_country_id=? AND dest_region_id='0' AND dest_city='' AND dest_zip='') ", $request->getDestCountryId())
					);
					break;
				case 8:
						$select->where(
						   "  (dest_country_id='0' AND dest_region_id='0'".
						   $zipSearchString
						);
					break;

				case 9:
					$select->where(
							"  (dest_country_id='0' AND dest_region_id='0' AND dest_zip='')"
				);
					break;
			}


			if (is_array($request->getMRConditionName())) {
				$i = 0;
				foreach ($request->getMRConditionName() as $conditionName) {
					if ($i == 0) {
						$select->where('condition_name=?', $conditionName);
					} else {
						$select->orWhere('condition_name=?', $conditionName);
					}
					$select->where('condition_from_value<=?', $request->getData($conditionName));


					$i++;
				}
			} else {
				$select->where('condition_name=?', $request->getMRConditionName());
				$select->where('condition_from_value<=?', $request->getData($request->getMRConditionName()));
				$select->where('condition_to_value>=?', $request->getData($request->getMRConditionName()));
			}

			$select->where('website_id=?', $request->getWebsiteId());

			$select->order('dest_country_id DESC');
			$select->order('dest_region_id DESC');
			$select->order('dest_zip DESC');
			$select->order('condition_from_value DESC');
			/*
			pdo has an issue. we cannot use bind
			*/

			$newdata=array();
			$row = $read->fetchAll($select);
			if (!empty($row))
			{
				// have found a result or found nothing and at end of list!
				foreach ($row as $data) {
					$newdata[]=$data;
				}
				break;
			}
		}
		return $newdata;

    }

    /**
     * Upload and import shipping rates
     *
     * @param Varien_Object $object
     * @return $this
     * @throws Exception
     */
    public function uploadAndImport(Varien_Object $object)
    {
        $destinationDir = Mage::getBaseDir('upload');

        $file             = [];
        $tmpName          = $_FILES['groups']['tmp_name'];
        $file['tmp_name'] = $tmpName[$object->getData('group_id')]['fields'][$object->getData('field')]['value'];
        $name             = $_FILES['groups']['name'];
        $file['name']     = $name[$object->getData('group_id')]['fields'][$object->getData('field')]['value'];

        /** @var Mage_Core_Model_File_Uploader $uploader */
        $uploader = new Mage_Core_Model_File_Uploader($file);
        $uploader->setAllowedExtensions(['csv']);
        $uploader->setAllowRenameFiles(true);
        $uploader->setFilesDispersion(false);
        $uploader->save($destinationDir);

        $document = new Varien_File_Csv();
        $filename = $destinationDir . DS . $uploader->getUploadedFileName();
        $lines    = $document->getData($filename);

        /** @var Varien_Io_File $fileSystem */
        $fileSystem = new Varien_Io_File();
        $fileSystem->rm($filename);

        /** @var Webshopapps_Matrixrate_Helper_Data $helper */
        $helper = Mage::helper('matrixrate');

        /**
         * Skip for header
         */
        $columnsMap = array_shift($lines);

        $requiredColumns = [
            'website_id', 'dest_country_id', 'dest_zip', 'dest_zip_to', 'condition_name',
            'condition_from_value', 'price', 'cost', 'delivery_type', 'method_type'
        ];

        $data = [];

        foreach ($lines as $key => $line) {
            $line                   = array_combine($columnsMap, $line);
            $line['dest_zip']       = str_pad(preg_replace('/[^0-9]/', '', $line['dest_zip']), 8, '0', STR_PAD_LEFT);
            $line['dest_zip_to']    = str_pad(preg_replace('/[^0-9]/', '', $line['dest_zip_to']), 8, '0', STR_PAD_LEFT);
            $line['website_id']     = $object->getData('scope_id');
            $line['condition_name'] = $object->getData('groups/matrixrate/fields/condition_name/inherit')
                ? Mage::getStoreConfig('carriers/matrixrate/condition_name')
                : $object->getData('groups/matrixrate/fields/condition_name/value');

            $errors = [];

            foreach ($requiredColumns as $requiredColumn) {
                if (isset($line[$requiredColumn])) {
                    continue;
                }

                $errors[] = $helper->__('The "%s" column is required.', $requiredColumn);
            }

            if ($line['condition_from_value'] < 0 && $line['condition_from_value'] > $line['condition_to_value']) {
                $errors[] = $helper->__('The "condition_from_value" must be less than the "condition_to_value".');
            }

            if ($line['condition_to_value'] < 0 && $line['condition_to_value'] < $line['condition_from_value']) {
                $errors[] = $helper->__('The "condition_to_value" must be greater than the "condition_from_value".');
            }

            if (!empty($errors)) {
                Mage::throwException(implode(nl2br(PHP_EOL), $errors));
            }

            $data[$key] = $line;
        }

        $connection = $this->_getWriteAdapter();
        $table      = Mage::getSingleton('core/resource')->getTableName('matrixrate_shipping/matrixrate');

        foreach (array_chunk($data, '500') as $items) {
            $connection->insertMultiple($table, $items);
        }

        return $this;
    }

    /**
     * Delete shipping rates from website
     *
     * @param int $websiteId
     * @return $this
     */
    public function deleteFromWebsite($websiteId)
    {
        $connection = $this->_getWriteAdapter();
        $table      = Mage::getSingleton('core/resource')->getTableName('matrixrate_shipping/matrixrate');

        $connection->delete($table, ['website_id' => $websiteId]);

        return $this;
    }
}
