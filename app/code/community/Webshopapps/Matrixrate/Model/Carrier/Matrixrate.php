<?php
/**
 * Webshopapps Shipping Module
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * Shipping MatrixRates
 *
 * @category   Webshopapps
 * @package    Webshopapps_Matrixrate
 * @copyright  Copyright (c) 2011 Zowta Ltd (http://www.webshopapps.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Karen Baker <sales@webshopapps.com>
 */

class Webshopapps_Matrixrate_Model_Carrier_Matrixrate
    extends Mage_Shipping_Model_Carrier_Abstract
    implements Mage_Shipping_Model_Carrier_Interface
{
    /**
     * Schedule method type
     *
     * @const string
     */
    CONST SCHEDULED_METHOD_TYPE = 'SCHEDULED';

    /**
     * Method code
     *
     * @var string
     */
    protected $_code = 'matrixrate';

    /**
     * Default condition name
     *
     * @var string
     */
    protected $_defaultConditionName = 'package_weight';

    /**
     * Collect shipping rates
     *
     * @param Mage_Shipping_Model_Rate_Request $request
     * @return Mage_Shipping_Model_Rate_Result
     */
    public function collectRates(Mage_Shipping_Model_Rate_Request $request)
    {
        if (!$this->getConfigFlag('active')) {
            return false;
        }

        /**
         * Exclude Virtual products price from Package value if pre-configured
         */
        if (!$this->getConfigFlag('include_virtual_price') && $request->getAllItems()) {
            foreach ($request->getAllItems() as $item) {
                if ($item->getParentItem()) {
                    continue;
                }

                if ($item->getHasChildren() && $item->isShipSeparately()) {
                    foreach ($item->getChildren() as $child) {
                        if ($child->getProduct()->isVirtual() || $item->getProductType() == 'downloadable') {
                            $request->setPackageValue($request->getPackageValue() - $child->getBaseRowTotal());
                        }
                    }
                } elseif ($item->getProduct()->isVirtual() || $item->getProductType() == 'downloadable') {
                    $request->setPackageValue($request->getPackageValue() - $item->getBaseRowTotal());
                }
            }
        }

        /**
         * Free shipping by qty
         */
        $freeQty = 0;

        if ($request->getAllItems()) {
            /** @var Mage_Sales_Model_Quote_Item $item */
            foreach ($request->getAllItems() as $item) {
                if ($item->getProduct()->isVirtual() || $item->getParentItem()) {
                    continue;
                }

                if ($item->getHasChildren() && $item->isShipSeparately()) {
                    foreach ($item->getChildren() as $child) {
                        if ($child->getFreeShipping() && !$child->getProduct()->isVirtual()) {
                            $freeShippingQty = (is_numeric($child->getFreeShipping()) ? $child->getFreeShipping() : 0);
                            $freeQty         += ($item->getQty() * ($child->getQty() - $freeShippingQty));
                        }
                    }
                } elseif ($item->getFreeShipping()) {
                    $freeShippingQty = (is_numeric($item->getFreeShipping()) ? $item->getFreeShipping() : 0);
                    $freeQty         += ($item->getQty() - $freeShippingQty);
                }
            }
        }

        if (!$request->getMRConditionName()) {
            $request->setMRConditionName(
                $this->getConfigData('condition_name')
                    ? $this->getConfigData('condition_name')
                    : $this->_defaultConditionName
            );
        }

        /**
         * Package weight and qty free shipping
         */
        $oldQty = $request->getPackageQty();

        if ($this->getConfigData('allow_free_shipping_promotions')
            && !$this->getConfigData('include_free_ship_items')) {
            $request->setPackageWeight($request->getFreeMethodWeight());
            $request->setPackageQty($oldQty - $freeQty);
        }

        /** @var Mage_Shipping_Model_Rate_Result $result */
        $result = Mage::getModel('shipping/rate_result');

        $rateArray    = $this->getRate($request);
        $freeShipping = false;

        if (is_numeric($this->getConfigData('free_shipping_threshold'))
            && $this->getConfigData('free_shipping_threshold') > 0
            && $request->getPackageValue() > $this->getConfigData('free_shipping_threshold')
        ) {
            $freeShipping = true;
        }

        if ($this->getConfigData('allow_free_shipping_promotions')
            && ($request->getFreeShipping() === true
                || $request->getPackageQty() == $this->getFreeBoxes())
        ) {
            $freeShipping = true;
        }

        foreach ($rateArray as $rate) {
            if (empty($rate) || $rate['price'] < 0) {
                continue;
            }

            $methodCode = 'matrixrate_' . strtolower($rate['method_type']);

            /** @var Mage_Shipping_Model_Rate_Result_Method $method */
            $method = Mage::getModel('shipping/rate_result_method');
            $method->setCarrier($this->_code);
            $method->setCarrierTitle($this->getCarrierTitle($methodCode));
            $method->setMethod($methodCode);
            $method->setMethodTitle(Mage::helper('matrixrate')->__($rate['delivery_type']));

            $shippingPrice = $this->getFinalPriceWithHandlingFee($rate['price']);

            $method->setCost($rate['cost']);
            $method->setDeliveryType($rate['delivery_type']);
            $method->setPrice($shippingPrice);

            if ($rate['method_type'] == self::SCHEDULED_METHOD_TYPE) {
                $method->setCarrierTitle(Mage::helper('matrixrate')->__('Scheduled Delivery'));
                $method->setMethod('matrixrate_scheduled');
                $method->setMethodTitle(Mage::helper('matrixrate')->__($rate['delivery_type']));

                if ($freeShipping) {
                    $method->setMethod('free_matrixrate_scheduled');
                }
            }

            if ($freeShipping) {
                $method->setPrice('0.0000');
            }

            $result->append($method);
        }

        return $result;
    }

    /**
     * Return carrier rates
     *
     * @param Mage_Shipping_Model_Rate_Request $request
     * @return array
     */
    public function getRate(Mage_Shipping_Model_Rate_Request $request)
    {
        return Mage::getResourceModel('matrixrate_shipping/carrier_matrixrate')->getNewRate(
            $request,
            $this->getConfigFlag('zip_range')
        );
    }

    /**
     * Return allowed shipping methods
     *
     * @return array
     */
    public function getAllowedMethods()
    {
        return Mage::getSingleton('matrixrate/system_config_source_carrier')->toArray();
    }

    /**
     * Return carrier title
     *
     * @param string $methodCode
     * @return string
     */
    public function getCarrierTitle($methodCode)
    {
        $carrierTitle   = $this->getConfigData('title');
        $allowedMethods = $this->getAllowedMethods();

        foreach ($allowedMethods as $code => $title) {
            if ($code != $methodCode) {
                continue;
            }

            $carrierTitle = $title;
        }

        return $carrierTitle;
    }

    /**
     * Return condition code
     *
     * @param string $type
     * @param string $code
     * @return array
     * @throws Mage_Core_Exception
     */
    public function getCode($type, $code = '')
    {
        $codes = [
            'condition_name' => [
                'package_weight' => Mage::helper('shipping')->__('Weight vs. Destination'),
                'package_value'  => Mage::helper('shipping')->__('Price vs. Destination'),
                'package_qty'    => Mage::helper('shipping')->__('# of Items vs. Destination')
            ],
            'condition_name_short' => [
                'package_weight' => Mage::helper('shipping')->__('Weight'),
                'package_value'  => Mage::helper('shipping')->__('Order Subtotal'),
                'package_qty'    => Mage::helper('shipping')->__('# of Items')
            ],
        ];

        if (!isset($codes[$type])) {
            throw Mage::exception(
                'Mage_Shipping',
                Mage::helper('shipping')->__('Invalid Matrix Rate code type: %s', $type)
            );
        }

        if ('' === $code) {
            return $codes[$type];
        }

        if (!isset($codes[$type][$code])) {
            throw Mage::exception(
                'Mage_Shipping',
                Mage::helper('shipping')->__('Invalid Matrix Rate code for type %s: %s', $type, $code)
            );
        }

        return $codes[$type][$code];
    }
}
