<?php
 
class Pagespeed_Image_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Configuration paths
     */
    const PAGESPEED_IMAGE_ENABLED = 'pagespeed/compress_images/enabled_lazyload';
    const PAGESPEED_IMAGES_COMPRESS_ENABLED = 'pagespeed/compress_images/enabled';
    const PAGESPEED_COMPRESS_EXCLUDE_DIR = 'pagespeed/compress_images/exclude_list';

    public function isEnabled()
    {
        return Mage::getStoreConfigFlag(self::PAGESPEED_IMAGE_ENABLED);
    }

    public function compressImagesIsEnabled()
    {
        return Mage::getStoreConfigFlag(self::PAGESPEED_IMAGES_COMPRESS_ENABLED);
    }

    public function excludeListDir()
    {
        $list = Mage::getStoreConfig(self::PAGESPEED_COMPRESS_EXCLUDE_DIR);
        $list = str_replace("\r", '', $list);
        $list = str_replace("\n", '', $list);

        $listArray = array($list);
        if (stripos($list, ',')) {
            $listArray = explode(',', $list);
        }

        return $listArray;
    }        

    public function getQualityImage($type)
    {
        return Mage::getStoreConfig('pagespeed/compress_images/quality_'.$type);
    }
}