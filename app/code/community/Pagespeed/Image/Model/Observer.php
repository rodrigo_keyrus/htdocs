<?php

class Pagespeed_Image_Model_Observer extends Mage_Core_Model_Design_Package
{
    public function processImagesOptimize(Varien_Event_Observer $observer)
    {
        if (!$this->getHelper()->isEnabled()) {
            return;
        }

        $response = $observer->getFront()->getResponse();
        $html = $response->getBody();

        //skin
        $html = str_replace(
            ' src="' . $this->getSkinUrl('images'),
            ' data-src="' . $this->getSkinUrl('images'),
            $html
        );

        //bannerslider
        $html = str_replace(
            ' src="' . Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'bannerslider/m/', 
            ' data-src="' . Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'bannerslider/m/', 
            $html
        );
        $html = str_replace(
            ' src="' . Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'bannerslider/1/', 
            ' data-src="' . Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'bannerslider/1/', 
            $html
        );        

        //brand
        $html = str_replace(
            ' src="' . Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'brand',
            ' data-src="' . Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'brand',
            $html
        );

        //cache
        $html = str_replace(
            ' src="' . Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'cache',
            ' data-src="' . Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'cache',
            $html
        );

        //catalog
        $html = str_replace(
            ' src="' . Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'catalog',
            ' data-src="' . Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'catalog',
            $html
        );

        //manufacturer
        $html = str_replace(
            ' src="' . Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'manufacturer',
            ' data-src="' . Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'manufacturer',
            $html
        );

        //wysiwyg
        $html = str_replace(
            ' src="' . Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'wysiwyg',
            ' data-src="' . Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'wysiwyg',
            $html
        );

        /* ==========================================================================================
         * URL Secure
         * ========================================================================================== */
        //skin
        $html = str_replace(
            ' src="' . $this->getSkinUrl('images', array('_secure' => true)),
            ' data-src="' . $this->getSkinUrl('images', array('_secure' => true)),
            $html
        );

        //bannerslider
        $html = str_replace(
            ' src="' . Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA, 'true') . 'bannerslider/m/',
            ' data-src="' . Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA, 'true') . 'bannerslider/m/',
            $html
        );
        $html = str_replace(
            ' src="' . Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA, 'true') . 'bannerslider/1/',
            ' data-src="' . Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA, 'true') . 'bannerslider/1/',
            $html
        );             

        //brand
        $html = str_replace(
            ' src="' . Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA, 'true') . 'brand',
            ' data-src="' . Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA, 'true') . 'brand',
            $html
        );

        //cache
        $html = str_replace(
            ' src="' . Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA, 'true') . 'cache',
            ' data-src="' . Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA, 'true') . 'cache',
            $html
        );

        //catalog
        $html = str_replace(
            ' src="' . Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA, 'true') . 'catalog',
            ' data-src="' . Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA, 'true') . 'catalog',
            $html
        );

        //menu
        $html = str_replace(
            ' src="' . Mage::getUrl('media', array('_secure' => true)) . 'catalog/category/',
            ' data-src="' . Mage::getUrl('media', array('_secure' => true)) . 'catalog/category/',
            $html
        );

        //manufacturer
        $html = str_replace(
            ' src="' . Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA, 'true') . 'manufacturer',
            ' data-src="' . Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA, 'true') . 'manufacturer',
            $html
        );

        //wysiwyg
        $html = str_replace(
            ' src="' . Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA, 'true') . 'wysiwyg',
            ' data-src="' . Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA, 'true') . 'wysiwyg',
            $html
        );


        //Ignore logo
        $html = str_replace(
            ' data-src="' . $this->getSkinUrl('images', array('_secure' => true)) . '/media/logo.png',
            ' src="' . $this->getSkinUrl('images', array('_secure' => true)) . '/media/logo.png',
            $html
        );
        $html = str_replace(
            ' data-src="' . $this->getSkinUrl('images', array('_secure' => true)) . '/media/logo.svg',
            ' src="' . $this->getSkinUrl('images', array('_secure' => true)) . '/media/logo.svg',
            $html
        );

        if (Mage::getStoreConfigFlag('pagespeed/compress_images/use_ignore_folder_patterns')) {
            //ignore few initial images
            $patterns = explode(',', Mage::getStoreConfig('pagespeed/compress_images/ignore_folder_patterns'));
            $qtyImagesToIgnore = Mage::getStoreConfig('pagespeed/compress_images/ignore_folder_patterns_image_qtys');
            foreach ($patterns as $pattern) {
                if (strpos($pattern, 'media') !== false) {
                    $url = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA, 'true');
                    $pattern = str_replace(array('media/', 'media'), '', $pattern);
                } elseif (strpos($pattern, 'skin') !== false) {
                    $url = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_SKIN, 'true') . str_replace(array('skin/', 'skin'), '', $pattern);
                    $pattern = str_replace(array('skin/', 'skin'), '', $pattern);
                } elseif (strpos($pattern, 'js') !== false) {
                    $url = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_JS, 'true') . str_replace(array('js/', 'js'), '', $pattern);
                    $pattern = str_replace(array('js/', 'js'), '', $pattern);
                } else {
                    $url = Mage::getBaseUrl('', 'true') . $pattern;
                }

                $html = preg_replace(
                    '/ data-src="' . str_replace('/', '\/', $url . $pattern) . '/',
                    ' src="' . $url . $pattern,
                    $html,
                    $qtyImagesToIgnore
                );
            }
        }

        //Ignore logo
        $html = str_replace(
            ' data-src="' . $this->getSkinUrl('images', array('_secure' => true)) . '/media/logo.png', 
            ' src="' . $this->getSkinUrl('images', array('_secure' => true)) . '/media/logo.png', 
            $html
        );        

        $response->setBody($html);
    }

    public function compressImages()
    {
        $excludeList = $this->getHelper()->excludeListDir();
        $di  = new RecursiveDirectoryIterator(getcwd());
        $iter  = new RecursiveIteratorIterator($di);
        $regexIter = new RegexIterator($iter, '/^.+\.(jpg|png)$/i', RecursiveRegexIterator::GET_MATCH);
        $count = 0;

        if (!$this->getHelper()->compressImagesIsEnabled()) {
            Mage::log('Module disabled, compress aborted!', null, 'compressImages.log');
            return;
        }

        foreach ($regexIter as $fileInfo) {
            $continue = false;

            /* Exclude List */
            foreach ($excludeList as $excludeDir) {
                if(strripos($fileInfo[0], $excludeDir)){
                    $continue = true;
                }
            }

            if ($continue) {
                continue;
            }

            if (strripos($fileInfo[0], '.png')) {
                $img = imagecreatefrompng($fileInfo[0]);
                if ($img) {
                    $compressImage = imagepng($img,$fileInfo[0], $this->getHelper()->getQualityImage('png'));
                    if ($compressImage) {
                        Mage::log('Success compress image: ' . $fileInfo[0], null, 'compressImages.log');
                        $count++;
                    }
                }
            }
            if (strripos($fileInfo[0], '.jpg')) {
                $img = imagecreatefromjpeg($fileInfo[0]);
                if ($img) {
                    $compressImage = imagejpeg($img,$fileInfo[0], $this->getHelper()->getQualityImage('jpg'));
                    if ($compressImage) {
                        Mage::log('Success compress image: ' . $fileInfo[0], null, 'compressImages.log');
                        $count++;
                    }
                }
            }
        }

        Mage::log('Total of  '. $count . ' images compress!', null, 'compressImages.log');

        return $this;
    }

    public function getHelper()
    {
        return Mage::helper('pagespeed_image');
    }

    public function getGifLoader()
    {
        return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_SKIN).'frontend/sephora/default/images/opc-ajax-loader-black.gif';
    }
}