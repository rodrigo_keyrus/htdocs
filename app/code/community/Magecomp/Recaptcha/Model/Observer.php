<?php

class Magecomp_Recaptcha_Model_Observer
{
    public function customerCreate($observer)
    {
        try {
            if (Mage::helper('recaptcha/data')->showOnRegister()) {
                $g_response = Mage::app()->getRequest()->getParam('g-recaptcha-response');
                if (isset($g_response) && !empty($g_response)) {
                    if (!(Mage::helper('recaptcha')->validateCaptcha($g_response))) {
                        Mage::getSingleton('core/session')->addError('Please click on the reCAPTCHA box.123');
                        $url = Mage::helper('core/http')->getHttpReferer() ? Mage::helper('core/http')->getHttpReferer() : Mage::getUrl();
                        Mage::app()->getFrontController()->getResponse()->setRedirect($url);
                        Mage::app()->getResponse()->sendResponse();
                        exit;
                    }
                } else {
                    $observer->getEvent()->setData(null);
                    Mage::getSingleton('core/session')->addError('Please click on the reCAPTCHA box.456');
                    $url = Mage::helper('core/http')->getHttpReferer() ? Mage::helper('core/http')->getHttpReferer() : Mage::getUrl();
                    Mage::app()->getFrontController()->getResponse()->setRedirect($url);
                    Mage::app()->getResponse()->sendResponse();
                    exit;
                }
            }
        } catch (Exception $e) {
            Mage::log("Captcha Error :" . $e->getMessage(), null, "recaptcha.log");
        }
    }

    public function login($observer)
    {
        try {
            if (Mage::helper('recaptcha/data')->showOnLogin()) {
                $g_response = Mage::app()->getRequest()->getParam('g-recaptcha-response');
                if (isset($g_response) && !empty($g_response)) {
                    if (!(Mage::helper('recaptcha')->validateCaptcha($g_response))) {
                        Mage::getSingleton('core/session')->addError('Please click on the reCAPTCHA box.123 ');
                        $url = Mage::helper('core/http')->getHttpReferer() ? Mage::helper('core/http')->getHttpReferer() : Mage::getUrl();
                        Mage::app()->getFrontController()->getResponse()->setRedirect($url);
                        Mage::app()->getResponse()->sendResponse();
                        exit;
                    }
                } else {
                    $observer->getEvent()->setData(null);
                    Mage::getSingleton('core/session')->addError('Please click on the reCAPTCHA box.456 ');
                    $url = Mage::helper('core/http')->getHttpReferer() ? Mage::helper('core/http')->getHttpReferer() : Mage::getUrl();
                    Mage::app()->getFrontController()->getResponse()->setRedirect($url);
                    Mage::app()->getResponse()->sendResponse();
                    exit;
                }
            }
        } catch (Exception $e) {
            Mage::log("Captcha Error :" . $e->getMessage(), null, "recaptcha.log");
        }
    }

    public function newsletterSubmit($observer)
    {
        try {
            if (Mage::helper('recaptcha/data')->showOnNewsletter()) {
                $g_response = Mage::app()->getRequest()->getParam('g-recaptcha-response');
                if (isset($g_response) && !empty($g_response)) {
                    if (!(Mage::helper('recaptcha')->validateCaptcha($g_response))) {
                        Mage::getSingleton('core/session')->addError('Please click on the reCAPTCHA box.123');
                        $url = Mage::helper('core/http')->getHttpReferer() ? Mage::helper('core/http')->getHttpReferer() : Mage::getUrl();
                        Mage::app()->getFrontController()->getResponse()->setRedirect($url);
                        Mage::app()->getResponse()->sendResponse();
                        exit;
                    }
                } else {
                    $observer->getEvent()->setData(null);
                    Mage::getSingleton('core/session')->addError('Please click on the reCAPTCHA box.456');
                    $url = Mage::helper('core/http')->getHttpReferer() ? Mage::helper('core/http')->getHttpReferer() : Mage::getUrl();
                    Mage::app()->getFrontController()->getResponse()->setRedirect($url);
                    Mage::app()->getResponse()->sendResponse();
                    exit;
                }
            }
        } catch (Exception $e) {
            Mage::log("Captcha Error :" . $e->getMessage(), null, "recaptcha.log");
        }
    }

    public function adminLoginSubmit($observer)
    {
        try {
            if (Mage::helper('recaptcha/data')->showAdminLogin()) {
                $g_response = Mage::app()->getRequest()->getParam('g-recaptcha-response');
                if (isset($g_response) && !empty($g_response)) {
                    if (!(Mage::helper('recaptcha')->validateCaptcha($g_response))) {
                        Mage::getSingleton('core/session')->addError('Please click on the reCAPTCHA box.123 ');
                        $url = Mage::helper('core/http')->getHttpReferer() ? Mage::helper('core/http')->getHttpReferer() : Mage::getUrl();
                        Mage::app()->getFrontController()->getResponse()->setRedirect($url);
                        Mage::app()->getResponse()->sendResponse();
                        exit;
                    }
                } else {
                    $observer->getEvent()->setData(null);
                    Mage::getSingleton('core/session')->addError('Please click on the reCAPTCHA box.456');
                    $url = Mage::helper('core/http')->getHttpReferer() ? Mage::helper('core/http')->getHttpReferer() : Mage::getUrl();
                    Mage::app()->getFrontController()->getResponse()->setRedirect($url);
                    Mage::app()->getResponse()->sendResponse();
                    exit;
                }
            }
        } catch (Exception $e) {
            Mage::log("Captcha Error :" . $e->getMessage(), null, "recaptcha.log");
        }
    }

    public function reviewSubmit()
    {
        try {
            if (Mage::helper('recaptcha/data')->showOnReview()) {
                $g_response = Mage::app()->getRequest()->getParam('g-recaptcha-response');
                if (isset($g_response) && !empty($g_response)) {
                    if (!(Mage::helper('recaptcha')->validateCaptcha($g_response))) {
                        Mage::getSingleton('core/session')->addError('Please click on the reCAPTCHA box');
                        $url = Mage::helper('core/http')->getHttpReferer() ? Mage::helper('core/http')->getHttpReferer() : Mage::getUrl();
                        Mage::app()->getFrontController()->getResponse()->setRedirect($url);
                        Mage::app()->getResponse()->sendResponse();
                        exit;
                    }
                } else {
                    Mage::getSingleton('core/session')->addError('Please click on the reCAPTCHA box.');
                    $url = Mage::helper('core/http')->getHttpReferer() ? Mage::helper('core/http')->getHttpReferer() : Mage::getUrl();
                    Mage::app()->getFrontController()->getResponse()->setRedirect($url);
                    Mage::app()->getResponse()->sendResponse();
                    exit;
                }
            }
        } catch (Exception $e) {
            Mage::log("Captcha Error :" . $e->getMessage(), null, "recaptcha.log");
        }
    }

    public function saveBilling()
    {
        try {
            if (Mage::helper('recaptcha/data')->showOnOnepage()) {
                $g_response = Mage::app()->getRequest()->getParam('g-recaptcha-response');
                if (isset($g_response) && !empty($g_response)) {
                    if (!(Mage::helper('recaptcha')->validateCaptcha($g_response))) {
                        Mage::getSingleton('core/session')->addError('Please click on the reCAPTCHA box');
                        $url = Mage::helper('core/http')->getHttpReferer() ? Mage::helper('core/http')->getHttpReferer() : Mage::getUrl();
                        Mage::app()->getFrontController()->getResponse()->setRedirect($url);
                        Mage::app()->getResponse()->sendResponse();
                        exit;
                    }
                } else {
                    Mage::getSingleton('core/session')->addError('Please click on the reCAPTCHA box.');
                    $url = Mage::helper('core/http')->getHttpReferer() ? Mage::helper('core/http')->getHttpReferer() : Mage::getUrl();
                    Mage::app()->getFrontController()->getResponse()->setRedirect($url);
                    Mage::app()->getResponse()->sendResponse();
                    exit;
                }
            }
        } catch (Exception $e) {
            Mage::log("Captcha Error :" . $e->getMessage(), null, "recaptcha.log");
        }
    }
}