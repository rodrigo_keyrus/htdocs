<?php
$installer = $this;
$installer->startSetup();

Mage::getSingleton('eav/config')
    ->getAttribute('customer', 'inchoo_socialconnect_fid')
    ->setData('used_in_forms', ['customer_account_create'])
    ->save();
Mage::getSingleton('eav/config')
    ->getAttribute('customer', 'inchoo_socialconnect_ftoken')
    ->setData('used_in_forms', ['customer_account_create'])
    ->save();

Mage::getSingleton('eav/config')
    ->getAttribute('customer', 'inchoo_socialconnect_gid')
    ->setData('used_in_forms', ['customer_account_create'])
    ->save();

Mage::getSingleton('eav/config')
    ->getAttribute('customer', 'inchoo_socialconnect_gtoken')
    ->setData('used_in_forms', ['customer_account_create'])
    ->save();

$installer->endSetup();