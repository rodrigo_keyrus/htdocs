

# Mundipagg - Sephora customizations

### Customization to save the NSU

`app/code/community/Uecommerce/Mundipagg/Model/Api.php`

>:1434
```php
/**
 * Sephora customization to save NSU
 */
$this->saveNsu($data, $orderReference);
```
>:2725
```php
/**
 * @todo MUNDIPAGG TEAM SHOULD MOVE IT INTO OFFICIAL MODULE
 */
$cardonfile->setCcOwner($data['payment'][1]['HolderName']);
```
>:2776
```php
/**
 * @todo MUNDIPAGG TEAM SHOULD MOVE IT INTO OFFICIAL MODULE
 *
 * Save updated NSU order
 *
 * @param array $data
 * @param array $orderReference
 */
private function saveNsu($data, $orderReference)
{
    if (isset($data['CreditCardTransaction']) && isset($data['CreditCardTransaction']['UniqueSequentialNumber'])) {
        try {
            $nsu     = $data['CreditCardTransaction']['UniqueSequentialNumber'];
            $order   = Mage::getModel('sales/order')->loadByIncrementId($orderReference);
            $payment = $order->getPayment();
            $payment->setAdditionalInformation('mundipagg_creditcard_payment_capture_nsu', $nsu);
            $payment->save();
        } catch (Exception $exception){

        }
    }
}
```
### Customization to dispatch event to add transaction data

`app/code/community/Uecommerce/Mundipagg/Model/Standard.php`

>:2309
```php
/**
 * @todo MUNDIPAGG TEAM SHOULD MOVE IT INTO OFFICIAL MODULE
*/
Mage::dispatchEvent(
    'uecommerce_mundipagg_add_transaction_data_after',
        array(
            'payment'       => $payment,
            'transaction'   => $transactionAdditionalInfo
        )
);
```
