<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Stock
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Cron schedule model
 *
 * @category   BSellerERP
 * @package    BSellerERP_Stock
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Stock_Model_Cron
{
    /**
     * Returns helper object
     * 
     * @var BSellerERP_Stock_Helper_Data
     */
    public $helper;

    /**
     * Initialize helper and factory model
     */
    public function __construct()
    {
        $this->helper = Mage::helper('bsellererp_stock');
    }

    /**
     * Init import stocks
     *
     * @return $this
     */
    public function import()
    {
        if (!$this->helper->moduleIsActive()) {
            return $this;
        }
        
        try {
            Mage::getModel('bsellererp_stock/integrator')->startIntegratorAllStores();
            $this->helper->log('Integration stocks success');
        } catch (Exception $e) {
            $this->helper->log($e->getMessage());
            Mage::throwException($e->getMessage());
        }

        return $this;
    }
}
