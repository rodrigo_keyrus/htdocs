<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Variation
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * BSeller core module helper
 *
 * @category   BSellerERP
 * @package    BSellerERP_Variation
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Variation_Helper_Data extends BSellerERP_Core_Helper_Data
{
    /**
     * General prefix for using in logs and events
     *
     * @var string
     */
    protected $prefix = 'variation';

    /**
     * Get attribute by ERP variation
     *
     * @param string $code
     * @return bool
     */
    public function getAttribute($code)
    {
        $attributesSerialized = Mage::getStoreConfig('bsellererp_variation/mapping/variations');
        $attributes           = (array) Zend_Serializer::unserialize($attributesSerialized);

        if (!count($attributes)) {
            return false;
        }

        foreach ($attributes as $attribute) {
            if ($attribute['variations_erp'] != $code) {
                continue;
            }

            return $attribute['variations'];
        }

        return false;
    }

    public function saveVariation($attribute, $variation) {
        $arg_attribute = $attribute->getAttributeCode();
        $arg_value = $variation;

        $attr_model = Mage::getModel('catalog/resource_eav_attribute');
        $attr = $attr_model->loadByCode('catalog_product', $arg_attribute);
        $attr_id = $attr->getAttributeId();

        $option['attribute_id'] = $attr_id;
        $option['value']['any_option_name'][0] = $arg_value;

        $setup = new Mage_Eav_Model_Entity_Setup('core_setup');
        $setup->addAttributeOption($option);

        $attr = $attr_model->loadByCode('catalog_product', $arg_attribute);
        return $attr->getSource()->getOptionId($arg_value);
    }
}
