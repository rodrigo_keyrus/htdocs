<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Delivery
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/* @var $installer BSellerERP_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$setup = Mage::getResourceModel('sales/setup', 'default_setup');

/**
 * Create attribute sales flat item
 */
$setup->addAttribute(
    'order_item',
    'id_delivery',
    array(
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'required'      => false,
    )
);

$installer->endSetup();