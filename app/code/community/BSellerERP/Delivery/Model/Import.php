<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Delivery
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Initialize importing deliverys in application
 *
 * @category   BSellerERP
 * @package    BSellerERP_Delivery
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Delivery_Model_Import
{
    /**
     * Content returned integration to import
     *
     * @var object
     */
    protected $responseObject;

    /**
     * Helper model
     *
     * @var BSellerERP_Delivery_Helper_Data
     */
    protected $helper;

    public function __construct()
    {
        $this->helper = Mage::helper('bsellererp_delivery');
    }

    /**
     * Set response object data
     *
     * @param object $responseObject
     * @return $this
     */
    public function setResponse($responseObject)
    {
        $this->responseObject = $responseObject;

        return $this;
    }

    /**
     * Init importing data
     */
    public function init()
    {
        Mage::dispatchEvent($this->helper->getPrefix() . '_import_init', array('deliveries' => $this->responseObject));

        foreach ($this->responseObject as $responseObjectItem) {
            if (!$this->validate($responseObjectItem)) {
                continue;
            }

            try {
                $this->import($responseObjectItem);
            } catch (Exception $ex) {
                $this->helper->log('Error updating (order code): ' . $responseObjectItem->numeroPedido . ' (' . $ex->getMessage() . ')', null, BSellerERP_Log_Model_Item_Attribute_Source_Status::ERROR, json_encode($responseObjectItem));
            }
        }

        Mage::dispatchEvent($this->helper->getPrefix() . '_import_complete', array('deliveries' => $this->responseObject));

        return $this;
    }

    protected function import($responseObjectItem)
    {
        $deliveries = $responseObjectItem->entregas;

        if (!count($deliveries)) {
            $this->helper->log('Not exist deliveries for importing for order ' . $responseObjectItem->numeroPedido);

            return false;
        }
        $result = $this->importDeliveriesToOrder($responseObjectItem->numeroPedido, $deliveries);
        $this->helper->log('Deliveries imported to order: ' . $responseObjectItem->numeroPedido, null, BSellerERP_Log_Model_Item_Attribute_Source_Status::SUCCESS, json_encode($responseObjectItem));

        return $result;
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @param $deliveries
     * @return bool
     */
    protected function importDeliveriesToOrder($incrementId, $deliveries)
    {
        if (!count($deliveries)) {
            return false;
        }

        /** @var Varien_Db_Adapter_Interface $conn */
        $conn = Mage::getSingleton('core/resource')->getConnection('core_write');

        foreach ($deliveries as $delivery) {
            $idDelivery = $delivery->idEntrega;
            $items = $this->getItemsByDelivery($delivery);

            /**
             * @var Mage_Sales_Model_Order_Item $item
             */
            $conn->update(
                'sales_flat_order_item',
                array('id_delivery' => $idDelivery),
                array('order_id = (select entity_id from sales_flat_order where increment_id = ?)' => $incrementId, 'sku' => array('in' => $items))
            );
        }

        return true;
    }

    /**
     * Get and format items by delivery
     *
     * @param object $delivery
     * @return array
     */
    protected function getItemsByDelivery($delivery)
    {
        $items = $delivery->itens;
        $data  = array();

        if (!count($items)) {
            return $data;
        }

        foreach ($items as $item) {
            $data[] = $item->codigoItem;
        }

        return $data;
    }

    /**
     * Validate delivery for create in application
     *
     * @param object $product
     * @return bool
     */
    protected function validate($product)
    {
        $requiredParams = array('numeroPedido', 'entregas');

        foreach ($requiredParams as $param) {
            if ($product->$param === false) {
                $this->helper->log('Delivery is not valid for importing, missing param: ' . $param);
                $this->helper->log($product);

                return false;
            }
        }

        return true;
    }
}
