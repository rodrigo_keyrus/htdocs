<?php
/**
 * BSellerERP_Log extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @package        BSellerERP_Log
 * @copyright      Copyright (c) 2016
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Log Item admin controller
 *
 * @category    BSellerERP
 * @package     BSellerERP_Log
 * @author      Ultimate Module Creator
 */
class BSellerERP_Log_Adminhtml_Log_ItemController extends BSellerERP_Log_Controller_Adminhtml_Log
{
    /**
     * init the log item
     *
     * @access protected
     * @return BSellerERP_Log_Model_Item
     */
    protected function _initItem()
    {
        $itemId  = (int) $this->getRequest()->getParam('id');
        $item    = Mage::getModel('bsellererp_log/item');
        if ($itemId) {
            $item->load($itemId);
        }
        Mage::register('current_item', $item);
        return $item;
    }

    /**
     * default action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->_title(Mage::helper('bsellererp_log')->__('BSellerERP Log'))
             ->_title(Mage::helper('bsellererp_log')->__('Log Items'));
        $this->renderLayout();
    }

    /**
     * grid action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function gridAction()
    {
        $this->loadLayout()->renderLayout();
    }

    /**
     * edit log item - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function editAction()
    {
        $itemId    = $this->getRequest()->getParam('id');
        $item      = $this->_initItem();
        if ($itemId && !$item->getId()) {
            $this->_getSession()->addError(
                Mage::helper('bsellererp_log')->__('This log item no longer exists.')
            );
            $this->_redirect('*/*/');
            return;
        }
        $data = Mage::getSingleton('adminhtml/session')->getItemData(true);
        if (!empty($data)) {
            $item->setData($data);
        }
        Mage::register('item_data', $item);
        $this->loadLayout();
        $this->_title(Mage::helper('bsellererp_log')->__('BSellerERP Log'))
             ->_title(Mage::helper('bsellererp_log')->__('Log Items'));
        if ($item->getId()) {
            $this->_title($item->getEntityCode());
        } else {
            $this->_title(Mage::helper('bsellererp_log')->__('Add log item'));
        }
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
        $this->renderLayout();
    }

    /**
     * new log item action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     * save log item - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost('item')) {
            try {
                $item = $this->_initItem();
                $item->addData($data);
                $item->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('bsellererp_log')->__('Log Item was successfully saved')
                );
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $item->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setItemData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            } catch (Exception $e) {
                Mage::logException($e);
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('bsellererp_log')->__('There was a problem saving the log item.')
                );
                Mage::getSingleton('adminhtml/session')->setItemData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('bsellererp_log')->__('Unable to find log item to save.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * delete log item - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function deleteAction()
    {
        if ( $this->getRequest()->getParam('id') > 0) {
            try {
                $item = Mage::getModel('bsellererp_log/item');
                $item->setId($this->getRequest()->getParam('id'))->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('bsellererp_log')->__('Log Item was successfully deleted.')
                );
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('bsellererp_log')->__('There was an error deleting log item.')
                );
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                Mage::logException($e);
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('bsellererp_log')->__('Could not find log item to delete.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * mass delete log item - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massDeleteAction()
    {
        $itemIds = $this->getRequest()->getParam('item');
        if (!is_array($itemIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('bsellererp_log')->__('Please select log items to delete.')
            );
        } else {
            try {
                foreach ($itemIds as $itemId) {
                    $item = Mage::getModel('bsellererp_log/item');
                    $item->setId($itemId)->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('bsellererp_log')->__('Total of %d log items were successfully deleted.', count($itemIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('bsellererp_log')->__('There was an error deleting log items.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massContextAction()
    {
        $itemIds = $this->getRequest()->getParam('item');
        if (!is_array($itemIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('bsellererp_log')->__('Please select log items.')
            );
        } else {
            try {
                foreach ($itemIds as $itemId) {
                $item = Mage::getSingleton('bsellererp_log/item')->load($itemId)
                    ->setContext($this->getRequest()->getParam('flag_context'))
                    ->setIsMassupdate(true)
                    ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d log items were successfully updated.', count($itemIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('bsellererp_log')->__('There was an error updating log items.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * export as csv - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportCsvAction()
    {
        $fileName   = 'item.csv';
        $content    = $this->getLayout()->createBlock('bsellererp_log/adminhtml_item_grid')
            ->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as MsExcel - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportExcelAction()
    {
        $fileName   = 'item.xls';
        $content    = $this->getLayout()->createBlock('bsellererp_log/adminhtml_item_grid')
            ->getExcelFile();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as xml - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportXmlAction()
    {
        $fileName   = 'item.xml';
        $content    = $this->getLayout()->createBlock('bsellererp_log/adminhtml_item_grid')
            ->getXml();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Check if admin has permissions to visit related pages
     *
     * @access protected
     * @return boolean
     * @author Ultimate Module Creator
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('bseller/bsellererp_log/item');
    }
}
