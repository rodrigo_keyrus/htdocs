<?php
/**
 * BSellerERP_Log extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @package        BSellerERP_Log
 * @copyright      Copyright (c) 2016
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Log Item admin grid block
 *
 * @category    BSellerERP
 * @package     BSellerERP_Log
 * @author      Ultimate Module Creator
 */
class BSellerERP_Log_Block_Adminhtml_Item_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * constructor
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('itemGrid');
        $this->setDefaultSort('created_at');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    /**
     * prepare collection
     *
     * @access protected
     * @return BSellerERP_Log_Block_Adminhtml_Item_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('bsellererp_log/item')
            ->getCollection();
        
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * prepare grid collection
     *
     * @access protected
     * @return BSellerERP_Log_Block_Adminhtml_Item_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'entity_id',
            array(
                'header' => Mage::helper('bsellererp_log')->__('Id'),
                'index'  => 'entity_id',
                'type'   => 'number'
            )
        );
        $this->addColumn(
            'context',
            array(
                'header' => Mage::helper('bsellererp_log')->__('Context'),
                'index'  => 'context',
                'type'  => 'options',
                'options' => Mage::helper('bsellererp_log')->convertOptions(
                    Mage::getModel('bsellererp_log/item_attribute_source_context')->getAllOptions(false)
                )

            )
        );
        $this->addColumn(
            'entity_code',
            array(
                'header'    => Mage::helper('bsellererp_log')->__('Entity Code'),
                'align'     => 'left',
                'index'     => 'entity_code',
            )
        );
        $this->addColumn(
            'log_data',
            array(
                'header' => Mage::helper('bsellererp_log')->__('Log Data'),
                'index'  => 'log_data',
            )
        );
        $this->addColumn(
            'created_at',
            array(
                'header' => Mage::helper('bsellererp_log')->__('Created at'),
                'index'  => 'created_at',
                'width'  => '120px',
                'type'   => 'datetime',
            )
        );
        $this->addColumn(
            'status',
            array(
                'header'         => $this->__('Status'),
                'index'          => 'status',
                'frame_callback' => array($this, 'decorateStatus'),
                'width'  => '20px',
                'type' => 'options',
                'options' => Mage::helper('bsellererp_log')->convertOptions(
                    Mage::getModel('bsellererp_log/item_attribute_source_status')->getAllOptions(false)
                )
            )
        );

        $this->addColumn(
            'action',
            array(
                'header'  =>  Mage::helper('bsellererp_log')->__('Action'),
                'width'   => '100',
                'type'    => 'action',
                'getter'  => 'getId',
                'actions' => array(
                    array(
                        'caption' => Mage::helper('bsellererp_log')->__('View'),
                        'url'     => array('base'=> '*/*/edit'),
                        'field'   => 'id'
                    )
                ),
                'filter'    => false,
                'is_system' => true,
                'sortable'  => false,
            )
        );
        $this->addExportType('*/*/exportCsv', Mage::helper('bsellererp_log')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('bsellererp_log')->__('Excel'));
        $this->addExportType('*/*/exportXml', Mage::helper('bsellererp_log')->__('XML'));
        return parent::_prepareColumns();
    }

    /**
     * prepare mass action
     *
     * @access protected
     * @return BSellerERP_Log_Block_Adminhtml_Item_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('item');
        $this->getMassactionBlock()->addItem(
            'delete',
            array(
                'label'=> Mage::helper('bsellererp_log')->__('Delete'),
                'url'  => $this->getUrl('*/*/massDelete'),
                'confirm'  => Mage::helper('bsellererp_log')->__('Are you sure?')
            )
        );
        $this->getMassactionBlock()->addItem(
            'context',
            array(
                'label'      => Mage::helper('bsellererp_log')->__('Change Context'),
                'url'        => $this->getUrl('*/*/massContext', array('_current'=>true)),
                'additional' => array(
                    'flag_context' => array(
                        'name'   => 'flag_context',
                        'type'   => 'select',
                        'class'  => 'required-entry',
                        'label'  => Mage::helper('bsellererp_log')->__('Context'),
                        'values' => Mage::getModel('bsellererp_log/item_attribute_source_context')
                            ->getAllOptions(true),

                    )
                )
            )
        );
        return $this;
    }

    /**
     * get the row url
     *
     * @access public
     * @param BSellerERP_Log_Model_Item
     * @return string
     * @author Ultimate Module Creator
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    /**
     * get the grid url
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

    /**
     * after collection load
     *
     * @access protected
     * @return BSellerERP_Log_Block_Adminhtml_Item_Grid
     * @author Ultimate Module Creator
     */
    protected function _afterLoadCollection()
    {
        $this->getCollection()->walk('afterLoad');
        parent::_afterLoadCollection();
    }

    public function decorateStatus($value, Varien_Object $row)
    {
        switch ($value) {
            case Mage::getModel('bsellererp_log/item_attribute_source_status')->getOptionText(1) :
                return '<div alt="Sucesso" style="width: 20px; height: 20px; -webkit-border-radius: 25px; -moz-border-radius: 25px; border-radius: 25px; background: green;"></div>';
            case Mage::getModel('bsellererp_log/item_attribute_source_status')->getOptionText(2) :
                return '<div alt="Erro" style="width: 20px; height: 20px; -webkit-border-radius: 25px; -moz-border-radius: 25px; border-radius: 25px; background: red;"></div>';
            default :
                return '<div alt="Info" style="width: 20px; height: 20px; -webkit-border-radius: 25px; -moz-border-radius: 25px; border-radius: 25px; background: grey;"></div>';
        }
    }
}
