<?php
/**
 * BSellerERP_Log extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @package        BSellerERP_Log
 * @copyright      Copyright (c) 2016
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Log Item edit form tab
 *
 * @category    BSellerERP
 * @package     BSellerERP_Log
 * @author      Ultimate Module Creator
 */
class BSellerERP_Log_Block_Adminhtml_Item_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * prepare the form
     *
     * @access protected
     * @return BSellerERP_Log_Block_Adminhtml_Item_Edit_Tab_Form
     * @author Ultimate Module Creator
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('item_');
        $form->setFieldNameSuffix('item');
        $this->setForm($form);
        $fieldset = $form->addFieldset(
            'item_form',
            array('legend' => Mage::helper('bsellererp_log')->__('Log Item'))
        );

        $fieldset->addField(
            'context',
            'select',
            array(
                'label' => Mage::helper('bsellererp_log')->__('Context'),
                'name'  => 'context',
                'required'  => true,
                'class' => 'required-entry',
                'disabled' => true,
                'values'=> Mage::getModel('bsellererp_log/item_attribute_source_context')->getAllOptions(true),
           )
        );

        $fieldset->addField(
            'entity_code',
            'text',
            array(
                'label' => Mage::helper('bsellererp_log')->__('Entity Code'),
                'name'  => 'entity_code',
                'note'	=> $this->__('magento entity_id / sku / custom code'),
                'required'  => true,
                'disabled' => true,
                'class' => 'required-entry',

           )
        );

        $fieldset->addField(
            'log_data',
            'textarea',
            array(
                'label' => Mage::helper('bsellererp_log')->__('Log Data'),
                'name'  => 'log_data',
                'required'  => true,
                'disabled' => true,
                'class' => 'required-entry',

            )
        );

        $fieldset->addField(
            'log_detail',
            'textarea',
            array(
                'label' => Mage::helper('bsellererp_log')->__('Log Detail'),
                'name'  => 'log_detail',
                'required'  => true,
                'disabled' => true,
                'class' => 'required-entry',

           )
        );
        $formValues = Mage::registry('current_item')->getDefaultValues();
        if (!is_array($formValues)) {
            $formValues = array();
        }
        if (Mage::getSingleton('adminhtml/session')->getItemData()) {
            $formValues = array_merge($formValues, Mage::getSingleton('adminhtml/session')->getItemData());
            Mage::getSingleton('adminhtml/session')->setItemData(null);
        } elseif (Mage::registry('current_item')) {
            $formValues = array_merge($formValues, Mage::registry('current_item')->getData());
        }
        $form->setValues($formValues);
        return parent::_prepareForm();
    }
}
