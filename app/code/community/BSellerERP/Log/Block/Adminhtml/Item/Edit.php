<?php
/**
 * BSellerERP_Log extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @package        BSellerERP_Log
 * @copyright      Copyright (c) 2016
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Log Item admin edit form
 *
 * @category    BSellerERP
 * @package     BSellerERP_Log
 * @author      Ultimate Module Creator
 */
class BSellerERP_Log_Block_Adminhtml_Item_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->_blockGroup = 'bsellererp_log';
        $this->_controller = 'adminhtml_item';
        $this->_removeButton('save');
        $this->_removeButton('delete');
        $this->_removeButton('reset');
    }

    /**
     * get the edit form header
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getHeaderText()
    {
        if (Mage::registry('current_item') && Mage::registry('current_item')->getId()) {
            return Mage::helper('bsellererp_log')->__(
                "Edit Log Item '%s'",
                $this->escapeHtml(Mage::registry('current_item')->getEntityCode())
            );
        } else {
            return Mage::helper('bsellererp_log')->__('Add Log Item');
        }
    }
}
