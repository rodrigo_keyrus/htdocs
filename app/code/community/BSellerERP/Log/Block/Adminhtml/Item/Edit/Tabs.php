<?php
/**
 * BSellerERP_Log extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @package        BSellerERP_Log
 * @copyright      Copyright (c) 2016
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Log Item admin edit tabs
 *
 * @category    BSellerERP
 * @package     BSellerERP_Log
 * @author      Ultimate Module Creator
 */
class BSellerERP_Log_Block_Adminhtml_Item_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * Initialize Tabs
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('item_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('bsellererp_log')->__('Log Item'));
    }

    /**
     * before render html
     *
     * @access protected
     * @return BSellerERP_Log_Block_Adminhtml_Item_Edit_Tabs
     * @author Ultimate Module Creator
     */
    protected function _beforeToHtml()
    {
        $this->addTab(
            'form_item',
            array(
                'label'   => Mage::helper('bsellererp_log')->__('Log Item'),
                'title'   => Mage::helper('bsellererp_log')->__('Log Item'),
                'content' => $this->getLayout()->createBlock(
                    'bsellererp_log/adminhtml_item_edit_tab_form'
                )
                ->toHtml(),
            )
        );
        return parent::_beforeToHtml();
    }

    /**
     * Retrieve log item entity
     *
     * @access public
     * @return BSellerERP_Log_Model_Item
     * @author Ultimate Module Creator
     */
    public function getItem()
    {
        return Mage::registry('current_item');
    }
}
