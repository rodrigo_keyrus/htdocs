<?php
/**
 * BSellerERP_Log extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @package        BSellerERP_Log
 * @copyright      Copyright (c) 2016
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Log Item admin block
 *
 * @category    BSellerERP
 * @package     BSellerERP_Log
 * @author      Ultimate Module Creator
 */
class BSellerERP_Log_Block_Adminhtml_Item extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        $this->_controller         = 'adminhtml_item';
        $this->_blockGroup         = 'bsellererp_log';
        parent::__construct();
        $this->_headerText         = Mage::helper('bsellererp_log')->__('Log Item');
        $this->_removeButton('add');
    }
}
