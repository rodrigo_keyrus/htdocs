<?php
/**
 * BSellerERP_Log extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @package        BSellerERP_Log
 * @copyright      Copyright (c) 2016
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Log Item model
 *
 * @category    BSellerERP
 * @package     BSellerERP_Log
 * @author      Ultimate Module Creator
 */
class BSellerERP_Log_Model_Item extends Mage_Core_Model_Abstract
{
    /**
     * Entity code.
     * Can be used as part of method name for entity processing
     */
    const ENTITY    = 'bsellererp_log_item';
    const CACHE_TAG = 'bsellererp_log_item';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'bsellererp_log_item';

    /**
     * Parameter name in event
     *
     * @var string
     */
    protected $_eventObject = 'item';

    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('bsellererp_log/item');
    }

    /**
     * before save log item
     *
     * @access protected
     * @return BSellerERP_Log_Model_Item
     * @author Ultimate Module Creator
     */
    protected function _beforeSave()
    {
        parent::_beforeSave();
        $now = Mage::getSingleton('core/date')->gmtDate();
        if ($this->isObjectNew()) {
            $this->setCreatedAt($now);
        }
        return $this;
    }

    /**
     * save log item relation
     *
     * @access public
     * @return BSellerERP_Log_Model_Item
     * @author Ultimate Module Creator
     */
    protected function _afterSave()
    {
        $this->_getResource()->saveLogDetail($this);
        return parent::_afterSave();
    }

    /**
     * get default values
     *
     * @access public
     * @return array
     * @author Ultimate Module Creator
     */
    public function getDefaultValues()
    {
        $values = array();
        return $values;
    }

    protected function _afterLoad()
    {
        parent::_afterLoad();

        $logDetail = $this->_getResource()->getLogDetail($this->getId());
        if($logDetail) {
            $this->setLogDetail($logDetail);
        }

        return $this;
    }
    
}
