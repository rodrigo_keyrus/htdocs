<?php

/**
 * Created by PhpStorm.
 * User: julio
 * Date: 21/12/16
 * Time: 14:14
 */
class BSellerERP_Log_Model_Cron extends Mage_Core_Model_Abstract
{
    public $helper;

    /**
     * Initialize helper and factory model
     */
    public function __construct()
    {
        $this->helper = Mage::helper('bsellererp_log');
    }

    public function cleanLogs()
    {
        $timeAlive = Mage::getStoreConfig('bsellererp_log/schedule/time_alive');
        if (!$timeAlive) {
            return $this;
        }

        $this->helper->log('Start cleaning bseller_erp logs...');

        $toDate = date('Y-m-d H:i:s', strtotime('-' . $timeAlive . ' day'));
        $logHistory = Mage::getModel('bsellererp_log/item')->getCollection()
            ->addFieldToFilter('created_at', array(
                'to' => $toDate,
                'date' => true,
            ));

        foreach ($logHistory as $log) {
            try {
                $log->delete();
            } catch (Exception $ex) {
                $this->helper->log($ex->getMessage());
            }
        }
        $this->helper->log('Finished cleaning bseller_erp logs...');
    }
}