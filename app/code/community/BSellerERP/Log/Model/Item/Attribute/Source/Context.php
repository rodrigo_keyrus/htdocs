<?php
/**
 * BSellerERP_Log extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @package        BSellerERP_Log
 * @copyright      Copyright (c) 2016
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Admin source model for Context
 *
 * @category    BSellerERP
 * @package     BSellerERP_Log
 * @author      Ultimate Module Creator
 */
class BSellerERP_Log_Model_Item_Attribute_Source_Context extends Mage_Eav_Model_Entity_Attribute_Source_Table
{
    /**
     * get possible values
     *
     * @access public
     * @param bool $withEmpty
     * @param bool $defaultValues
     * @return array
     * @author Ultimate Module Creator
     */
    public function getAllOptions($withEmpty = true, $defaultValues = false)
    {
        $options =  array(
            array(
                'label' => 'Attribute',
                'value' => 1
            ),
            array(
                'label' => 'Category',
                'value' => 2
            ),
            array(
                'label' => 'Channel',
                'value' => 3
            ),
            array(
                'label' => 'Delivery',
                'value' => 4
            ),
            array(
                'label' => 'Log',
                'value' => 14
            ),
            array(
                'label' => 'Order',
                'value' => 5
            ),
            array(
                'label' => 'Payment',
                'value' => 6
            ),
            array(
                'label' => 'Payment Brands',
                'value' => 7
            ),
            array(
                'label' => 'Payment Conditions',
                'value' => 8
            ),
            array(
                'label' => 'Product',
                'value' => 9
            ),
            array(
                'label' => 'Shipping',
                'value' => 10
            ),
            array(
                'label' => 'Stock',
                'value' => 11
            ),
            array(
                'label' => 'Tracking',
                'value' => 12
            ),
            array(
                'label' => 'Vale',
                'value' => 13
            ),
            array(
                'label' => 'Geral',
                'value' => 100
            ),
        );
        if ($withEmpty) {
            array_unshift($options, array('label'=>'', 'value'=>''));
        }
        return $options;

    }

    /**
     * get options as array
     *
     * @access public
     * @param bool $withEmpty
     * @return string
     * @author Ultimate Module Creator
     */
    public function getOptionsArray($withEmpty = true)
    {
        $options = array();
        foreach ($this->getAllOptions($withEmpty) as $option) {
            $options[$option['value']] = $option['label'];
        }
        return $options;
    }

    /**
     * get option text
     *
     * @access public
     * @param mixed $value
     * @return string
     * @author Ultimate Module Creator
     */
    public function getOptionText($value)
    {
        $options = $this->getOptionsArray();
        if (!is_array($value)) {
            $value = explode(',', $value);
        }
        $texts = array();
        foreach ($value as $v) {
            if (isset($options[$v])) {
                $texts[] = $options[$v];
            }
        }
        return implode(', ', $texts);
    }
}
