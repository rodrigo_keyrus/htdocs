<?php
/**
 * BSellerERP_Log extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @package        BSellerERP_Log
 * @copyright      Copyright (c) 2016
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */

/**
 * Log Item resource model
 *
 * @category    BSellerERP
 * @package     BSellerERP_Log
 * @author      Ultimate Module Creator
 */
class BSellerERP_Log_Model_Resource_Item extends Mage_Core_Model_Resource_Db_Abstract
{

    /**
     * constructor
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function _construct()
    {
        $this->_init('bsellererp_log/item', 'entity_id');
    }

    public function saveLogDetail(Mage_Core_Model_Abstract $object)
    {
        if ($object->getLogDetail()) {
            $logDetailData = array(
                'log_id' => $object->getId(),
                'detail' => $object->getLogDetail()
            );
            $this->_getWriteAdapter()->insert($this->getTable('bsellererp_log/item_detail'), $logDetailData);
        }
    }

    public function getLogDetail($logId)
    {
        $collection = $this->_getReadAdapter()->fetchAll($this->_getReadAdapter()->select()->from($this->getTable('bsellererp_log/item_detail'))->where('log_id = ' . $logId));
        $logDetail = '';
        foreach ($collection as $item) {
            $logDetail = $item['detail'];
        }
        return $logDetail;
    }
}
