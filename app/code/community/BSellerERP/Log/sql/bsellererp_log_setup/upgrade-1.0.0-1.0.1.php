<?php

$this->startSetup();
$table = $this->getConnection()
    ->newTable($this->getTable('bsellererp_log/item_detail'))
    ->addColumn(
        'entity_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'identity' => true,
            'nullable' => false,
            'primary' => true,
        ),
        'Entity ID'
    )
    ->addColumn(
        'log_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'nullable' => false
        ),
        'Log Item ID'
    )
    ->addColumn(
        'detail',
        Varien_Db_Ddl_Table::TYPE_TEXT, '64k',
        array(
            'nullable' => false,
        ),
        'Log Detail'
    )
    ->setComment('Log Item Table');
$this->getConnection()->createTable($table);

$this->getConnection()->addForeignKey(
    $this->getConnection()->getForeignKeyName(
        $this->getTable('bsellererp_log/item_detail'),
        'log_id',
        $this->getTable('bsellererp_log/item'),
        'entity_id'),
    $this->getTable('bsellererp_log/item_detail'),
    'log_id',
    $this->getTable('bsellererp_log/item'),
    'entity_id'
);

$this->getConnection()
    ->addColumn($this->getTable('bsellererp_log/item'),
        'status',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'nullable' => false,
        ),
        'Status');

$this->endSetup();