<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Core
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * BSeller core module helper
 *
 * @category   BSellerERP
 * @package    BSellerERP_Core
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Core_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Path store config debugging mode
     *
     * @const string
     */
    const DEBUGGING_ACTIVE = 'bsellererp_core/settings/debug_mode';
    const BSELLERERP_LOG_MODULE_ACTIVE       = 'bsellererp_log/settings/active';

    const RESQUE_QUEUE_NAME = 'bsellererp';
    const PRODUCT_QUEUE_LIMIT = '100';
    const RESQUE_JOB_CLASS = 'BSellerERP_Core_Model_Job';

    /**
     * General prefix for using in logs and events
     *
     * @var string
     */
    protected $prefix = 'core';

    /**
     * Default prefix for merge in general prefix
     *
     * @var string
     */
    private $defaultPrefix = 'bsellererp';

    /**
     * Save logs by prefix file
     *
     * @param  $message
     * @return $this
     */
    public function log($message, $entityCode = null, $status = BSellerERP_Log_Model_Item_Attribute_Source_Status::INFO, $detail = null)
    {
        if (!Mage::getStoreConfig(self::DEBUGGING_ACTIVE)) {
            return $this;
        }

        Mage::log($message, null, $this->getPrefix() . '.log', true);

        if (Mage::getStoreConfig(SELF::BSELLERERP_LOG_MODULE_ACTIVE)) {
            try {
                $this->saveBSellerERPLog($message, $entityCode, $status, $detail);
            } catch (Exception $ex) {
                Mage::log($ex->getMessage(), null, $this->getPrefix() . '.log', true);
            }
        }

        return $this;
    }

    private function saveBSellerERPLog($message, $entityCode, $status, $detail) {
        $model = Mage::getModel('bsellererp_log/item');

        $contextSource = array_search($this->prefix ? ucfirst($this->prefix) : 0, Mage::getModel('bsellererp_log/item_attribute_source_context')->getOptionsArray());

        $model->setEntityCode($entityCode);
        $model->setContext($contextSource ? $contextSource : 100);
        $model->setLogData($message);
        $model->setStatus($status);
        $model->setLogDetail($detail);
        $model->save();
    }

    /**
     * Return prefix formatted to the module
     *
     * @return string
     */
    public function getPrefix()
    {
        return $this->defaultPrefix . '_' . $this->prefix;
    }

    /**
     * Converts input date into date of default ERP
     *
     * @param  string $date
     * @return string
     */
    public function formatDate($date)
    {
        return Mage::getModel('core/date')->date('Y-m-d\TH:i:s.000\Z', $date);
    }

    /**
     * Converts Magento datetime (default GMT 0 in database)
     * into BSeller ERP (actually GMT 0) expected GMT
     *
     * This method should be used in all date conversions while mounting
     * BSeller ERP JSON's integrations (ex: order, payment, etc...)
     *
     * @param  string $datetime
     * @return string
     */
    public function timezoneConvert($datetime)
    {
        /** @var string $erpTimezone */
        $erpTimezone = $this->_getErpTimezone();

        /** @var string $erpDateTime */
        $erpDateTime = date('Y-m-d\TH:i:s.000\Z', strtotime($erpTimezone, strtotime($datetime)));

        return $erpDateTime;
    }

    /**
     * Get street by name
     *
     * @param  string $name
     * @param  mixed $instance
     * @return $this
     */
    public function getStreetByName($name, $instance)
    {
        if (!$code = Mage::getStoreConfig('bsellererp_core/mapping/' . $name)) {
            return '';
        }

        /**
         * @var Mage_Customer_Model_Address $instance
         */
        return $instance->getStreet($code);
    }

    /**
     * Get setting store config by key
     *
     * @param  $key
     * @return mixed
     */
    public function getSettingStoreConfig($key)
    {
        return Mage::getStoreConfig('bsellererp_core/settings/' . $key);
    }

    /**
     * Verify if module is active
     *
     * @return mixed
     */
    public function moduleIsActive()
    {
        return Mage::getStoreConfig($this->getPrefix() . '/settings/active');
    }

    /**
     * Get attribute in store config by name
     *
     * @param  string $name
     * @param  mixed $instance
     * @return $this
     */
    public function getPredefinedAttributeValue($name, $instance)
    {
        if (!$attribute = Mage::getStoreConfig('bsellererp_core/mapping/' . $name)) {
            return '';
        }

        /**
         * @var Varien_Object $instance
         */
        return $instance->getData($attribute);
    }

    /**
     * Returns only numbers
     *
     * @param  $string
     * @return mixed
     */
    public function onlyNumbers($string)
    {
        return preg_replace('/[^0-9]/', '', $string);
    }

    /**
     * Truncate a string to a certain length
     *
     * @param  string $string
     * @param  int $size
     * @return string
     */
    public function truncate($string, $size)
    {
        return Mage::helper('core/string')->truncate($string, $size, '');
    }

    public function getStoreConfig($path, $store = null)
    {
        if ($store) {
            return Mage::getStoreConfig($path, $store);
        }
        if (Mage::app()->getStore()->isAdmin()) {
            if(Mage::app()->getRequest()->getParam('order_id')) {
                $storeId = Mage::getModel('sales/order')->load(Mage::app()->getRequest()->getParam('order_id'))->getStoreId();
            } else {
                $checkout = Mage::getSingleton('adminhtml/session_quote');
                $storeId = $checkout->getStoreId();
            }
        } else {
            $storeId = Mage::app()->getStore()->getStoreId();
        }
        if ($storeId) {
            return Mage::getStoreConfig($path, $storeId);
        }
        return Mage::getStoreConfig($path);
    }


    /**
     * Get BSeller ERP date timezone
     *
     * @return string
     */
    protected function _getErpTimezone()
    {
        /** @var string $erpTimezone */
        $erpTimezone = $this->getSettingStoreConfig('timezone');

        if ($erpTimezone == '') {
            return '0 hour';
        }

        if ((float) $erpTimezone <= 0) {
            return (string) $erpTimezone.' hour';
        }

        return '+'.$erpTimezone.' hour';
    }
}
