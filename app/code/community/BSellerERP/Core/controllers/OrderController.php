<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Core
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

class BSellerERP_Core_OrderController extends Mage_Core_Controller_Front_Action
{
    /**
     * Init integration
     */
    public function initAction()
    {
        try {
            $export = Mage::getModel('bsellererp_order/export');
            $export->setOrder(200000075);
            $export->init();
        } catch (Exception $e) {
            Mage::throwException($e->getMessage());
        }
    }

    public function approveAction()
    {
        $invoice = Mage::getModel('sales/order_invoice')->loadByIncrementId(100000068);

        if (!$invoice->getId()) {
            die('Invoice not exist');
        }

        Mage::getModel('bsellererp_order/payment_approve_export')
            ->setInvoice($invoice)
            ->init();
    }

    public function disapproveAction()
    {
        $order = Mage::getModel('sales/order')->loadByIncrementId($this->getRequest()->getParam('id'));

        if (!$order->getId()) {
            die('Order not exist');
        }

        Mage::getModel('bsellererp_order/payment_disapprove_export')
            ->setOrder($order)
            ->init();
    }
}
