<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Representative
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Observerl model
 *
 * @category   BSellerERP
 * @package    BSellerERP_Representative
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Representative_Model_Observer
{
    /**
     * @var BSellerERP_Representative_Helper_Data
     */
    protected $helper;

    /**
     * Load helper model
     */
    public function __construct()
    {
        $this->helper = Mage::helper('bsellererp_representative');
    }

    /**
     * Add button for resend integration in customer view
     *
     * @param Varien_Event_Observer $observer
     * @return $this
     */
    public function addIntegrationButtonCustomer(Varien_Event_Observer $observer)
    {
        if (!$this->helper->moduleIsActive()) {
            return $this;
        }

        $block = $observer->getBlock();

        if (!$block instanceof Mage_Adminhtml_Block_Customer_Edit) {
            return $this;
        }

        if ($this->getCustomer()->getData('erp_status') != BSellerERP_Representative_Helper_Data::BLOCKED) {
            return $this;
        }

        $message = $this->helper->__('Are you sure you want to do this?');

        $url = $block->getUrl(
            'bsellererp_representative/adminhtml_index/resend',
            array('customer_id' => $block->getCustomerId())
        );

        $block->addButton(
            'resend_customer_integration',
            array(
                'label'     => $this->helper->__('Resend Representative'),
                'onclick'   => "confirmSetLocation('{$message}', '{$url}')"
            )
        );

        return $this;
    }

    /**
     * Retrieve customer model object
     *
     * @return Mage_Customer_Model_Customer
     */
    protected function getCustomer()
    {
        return Mage::registry('current_customer');
    }
}
