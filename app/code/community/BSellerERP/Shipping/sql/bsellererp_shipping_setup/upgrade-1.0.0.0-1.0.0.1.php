<?php

$installer = $this;

$installer->startSetup();

$installer->getConnection()->dropColumn($installer->getTable('bsellererp_shipping/carrier_items'), 'contract_id');

$table = $installer->getConnection()
    ->newTable($installer->getTable('bsellererp_shipping/contract_items'))
    ->addColumn(
        'entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
        array(
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
            'primary' => true
        ), 'Entity ID'
    )
    ->addColumn(
        'contrato', Varien_Db_Ddl_Table::TYPE_TEXT, 30,
        array(
            'unsigned' => true,
            'nullable' => false,
        ), 'Contract'
    )
    ->addColumn(
        'descricao', Varien_Db_Ddl_Table::TYPE_TEXT, 30,
        array(
            'unsigned' => true,
            'nullable' => false,
        ), 'Contract Description'
    )
    ->addColumn(
        'id_erp', Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(
            'unsigned' => true,
            'nullable' => false,
        ), 'ERP Id'
    )
    ->addColumn(
        'carrier_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
        array(
            'unsigned' => true,
            'nullable' => false
        ), 'Carrier ID'
    )
    ->setComment('BSeller ERP - Shipping Contract Integration');

$installer->getConnection()->createTable($table);
$installer->getConnection()
    ->addForeignKey(
        'FK_BSELLERERP_CONTRACT_BSELLERERP_CARRIER',
        $installer->getTable('bsellererp_shipping/contract_items'),
        'carrier_id',
        $installer->getTable('bsellererp_shipping/carrier_items'),
        'entity_id'
    );

$installer->endSetup();