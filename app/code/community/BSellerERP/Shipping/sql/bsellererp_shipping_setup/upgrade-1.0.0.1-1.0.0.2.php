<?php

$installer = $this;

$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('bsellererp_shipping/contract_items'),
        'tipo_entrega',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
        ), 'Tipo Entrega');

$installer->endSetup();