<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Shipping
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Getting contracts by carrier
 *
 * @category   BSellerERP
 * @package    BSellerERP_Shipping
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Shipping_Model_Contract_Import extends BSellerERP_Shipping_Model_Contract_Schema
{
    /**
     * Init importing contracts
     */
    public function init()
    {
        Mage::dispatchEvent($this->helper->getPrefix() . '_import_init', array('contracts' => $this->contracts));

        foreach ($this->contracts as $contract) {
            if (!$this->validate($contract)) {
                continue;
            }

            $this->create($contract);
        }

        Mage::dispatchEvent($this->helper->getPrefix() . '_import_complete', array('contracts' => $this->contracts));

        return $this;
    }

    public function setContracts($contracts)
    {
        $this->contracts = $contracts;

        return $this;
    }

    public function setCarrier($carrier) {
        $this->currentCarrier = $carrier;
    }

    protected function create($contractERP)
    {
        $this->contractERP     = $contractERP;
        $this->currentContract = Mage::getModel('bsellererp_shipping/contract_items');

        /**
         * Adding data as scheme
         */
        $this->currentContract->addData($this->prepareSchema());

        Mage::dispatchEvent($this->helper->getPrefix() . '_contract_item_save_before', array('contract' => $this->currentContract));
        $this->currentContract->save();
        Mage::dispatchEvent($this->helper->getPrefix() . '_contract_item_save_after', array('contract' => $this->currentContract));

        return $this;
    }

    /**
     * Validate contract for create in application
     *
     * @param object $product
     * @return bool
     */
    protected function validate($product)
    {
        $requiredParams = array('contrato', 'descricao', 'id');

        foreach ($requiredParams as $param) {
            if ($product->$param === false) {
                $this->helper->log('[Contract] Contract is not valid for importing, missing param: ' . $param);
                $this->helper->log($product);

                return false;
            }
        }

        return true;
    }
}
