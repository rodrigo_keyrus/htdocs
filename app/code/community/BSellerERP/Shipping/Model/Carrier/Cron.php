<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Shipping
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Cron schedule model
 *
 * @category   BSellerERP
 * @package    BSellerERP_Shipping
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Shipping_Model_Carrier_Cron
{
    /**
     * Returns helper object
     *
     * @var BSellerERP_Shipping_Helper_Data
     */
    public $helper;

    /**
     * Initialize helper and factory model
     */
    public function __construct()
    {
        $this->helper = Mage::helper('bsellererp_shipping');
    }

    /**
     * Init import carriers
     *
     * @return $this
     */
    public function import()
    {
        if (!$this->helper->moduleIsActive()) {
            return $this;
        }
        
        try {
            Mage::getModel('bsellererp_shipping/carrier_integrator')->init();
            $this->helper->log('[Carrier] Integration carriers success');
        } catch (Exception $e) {
            $this->helper->log('[Carrier] ' . $e->getMessage());
            Mage::throwException($e->getMessage());
        }

        return $this;
    }
}
