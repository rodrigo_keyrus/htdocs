<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Shipping
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * BSeller core module helper
 *
 * @category   BSellerERP
 * @package    BSellerERP_Shipping
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Shipping_Helper_Data extends BSellerERP_Core_Helper_Data
{
    /**
     * General prefix for using in logs and events
     *
     * @var string
     */
    protected $prefix = 'shipping';

    /**
     * Get shipping method by ERP code, attributes mapping in administrator.
     *
     * @param string $code
     * @return bool
     */
    public function getCarrierByShipping($code)
    {
        $carriers = (array) unserialize(Mage::getStoreConfig('bsellererp_shipping/carrier/mapping'));

        if (!count($carriers)) {
            return false;
        }

        foreach ($carriers as $carrier) {
            if ($carrier['carriers'] != $code) {
                continue;
            }

            $carrier = Mage::getModel('bsellererp_shipping/carrier_items')->load(explode('-', $carrier['carriers_erp'])[0], 'carrier_id');

            if(!$carrier || !$carrier->getEntityId()) {
                continue;
            }

            return $carrier;
        }

        return false;
    }

    public function getContractByShipping($code)
    {
        $carriers = (array) unserialize(Mage::getStoreConfig('bsellererp_shipping/carrier/mapping'));

        if (!count($carriers)) {
            return false;
        }

        foreach ($carriers as $carrier) {
            if ($carrier['carriers'] != $code) {
                continue;
            }

            $carrierTmp = Mage::getModel('bsellererp_shipping/carrier_items')->load(explode('-', $carrier['carriers_erp'])[0], 'carrier_id');

            $contract = Mage::getModel('bsellererp_shipping/contract_items')->getCollection()
                ->addFieldToFilter('carrier_id', $carrierTmp->getEntityId())
                ->addFieldToFilter('id_erp', explode('-', $carrier['carriers_erp'])[1])
                ->getFirstItem();

            if(!$contract || !$contract->getEntityId()) {
                continue;
            }

            return $contract;
        }

        return false;
    }
}