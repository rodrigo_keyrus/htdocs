<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Category
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/* @var $installer BSellerERP_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

/**
 * Create category attribute 'erp_id' for mapping categories by ERP
 */
$installer->addAttribute(
    'catalog_category',
    'erp_id',
    array(
        'type'          => 'int',
        'user_defined'  => false,
        'visible'       => false,
        'unique'        => true,
        'required'      => false,
    )
);

$installer->endSetup();