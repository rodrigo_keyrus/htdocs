<?php

$installer = $this;

$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('bsellererp_vale/items'), 'saldo',
        array(
            'type' => Varien_Db_Ddl_Table::TYPE_DECIMAL,
            'length' => '12,4',
            'unsigned' => true,
            'nullable' => false,
            'comment'  => 'Credit remains'
        ));

$setup = Mage::getResourceModel('sales/setup', 'default_setup');
$setup->addAttribute(
    'order',
    'applied_vales',
    array(
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'required'      => false,
    )
);

$installer->endSetup();