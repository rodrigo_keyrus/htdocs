<?php
/**
 * BSeller vale module helper
 *
 * @category   BSellerERP
 * @package    BSellerERP_Vale
 * @author     Julio Reis <julio.reis@e-smart.com.br>
 */
/* @var $installer BSellerERP_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

/**
 * Create 'Customers Vales' table
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('bsellererp_vale/items'))
    ->addColumn(
        'entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
        array(
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
            'primary' => true
        ), 'Entity ID'
    )
    ->addColumn(
        'cpfCliente', Varien_Db_Ddl_Table::TYPE_TEXT, 30,
        array(
            'unsigned' => true,
            'nullable' => false,
        ), 'CPF Cliente'
    )
    ->addColumn(
        'idVale', Varien_Db_Ddl_Table::TYPE_TEXT, 100,
        array(
            'unsigned' => true,
            'nullable' => false,
        ), 'ID Vale'
    )
    ->addColumn(
        'situacao', Varien_Db_Ddl_Table::TYPE_TEXT, 30,
        array(
            'nullable' => true,
        ), 'Situação'
    )
    ->addColumn(
        'validade', Varien_Db_Ddl_Table::TYPE_TEXT, 50,
        array(
            'nullable' => true,
        ), 'Validade'
    )
    ->addColumn(
        'valor', Varien_Db_Ddl_Table::TYPE_DECIMAL, null,
        array(
            'scale'     => 4,
            'precision' => 12,
            'nullable' => false,
        ), 'Valor'
    )
    ->setComment('BSeller ERP - Vale Integration');

$installer->getConnection()->createTable($table);

$installer->endSetup();