<?php

/**
 * BSeller vale module helper
 *
 * @category   BSellerERP
 * @package    BSellerERP_Vale
 * @author     Julio Reis <julio.reis@e-smart.com.br>
 */
class BSellerERP_Vale_Model_Schema extends BSellerERP_Core_Model_Schema_Abstract
{
    /**
     * Default value param status
     *
     * @const string
     */
    const DEFAULT_STATUS_ACTIVE = 'A';

    const INTEGRATION_SOURCE = 'integration';

    /**
     * @var Mage_SalesRule_Model_Vale
     */
    public $objectModel;

    /**
     * @var object
     */
    public $responseObjectItem;

    /**
     * Load helper data
     *
     * @var string
     */
    public $currentHelper = 'vale';

    /**
     * Prepare schema and returns for adding in vale instance
     *
     * @return array
     */
    protected function prepareSchema()
    {
        parent::prepareSchema();

        $this->setGeneralData();

        return $this->getSchema();
    }

    /**
     * Setting general data for schema
     *
     * @return $this
     */
    protected function setGeneralData()
    {
        $this->addData(
            array(
                'cpfCliente' => str_pad($this->responseObjectItem->cpfCliente, 11, "0", STR_PAD_LEFT),
                'idVale' => $this->responseObjectItem->idVale,
                'situacao' => $this->responseObjectItem->situacao,
                'validade' => $this->responseObjectItem->validade,
                'valor' => $this->responseObjectItem->valor,
                'saldo' => $this->responseObjectItem->valor,
            )
        );
        return $this;
    }

    /**
     * Get and format To Date
     *
     * @return string
     */
    protected function getToDate()
    {
        $date = $this->responseObjectItem->validade;

        if (!$date) {
            return '';
        }

        $formatDate = Mage::getModel('core/date')->date('Y-m-d', $date);

        return $formatDate;
    }

    /**
     * Verify if vale is active
     *
     * @return bool
     */
    protected function isAllowed()
    {
        $active = $this->responseObjectItem->situacao;

        if ($active === self::DEFAULT_STATUS_ACTIVE) {
            return true;
        }

        return false;
    }

    /**
     * Get default label and change param by vale code
     *
     * @return string
     */
    protected function getLabel()
    {
        $label = Mage::getStoreConfig(self::DEFAULT_LABEL);

        return $this->helper->__($label, $this->getValeCode());
    }

    /**
     * Get vale code by response ERP
     *
     * @return string
     */
    protected function getValeCode()
    {
        if (!$code = $this->helper->decryptValeKey($this->responseObjectItem->idVale)) {
            return '';
        }

        return $code;
    }
}
