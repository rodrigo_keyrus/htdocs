<?php
/**
 * BSeller vale module
 *
 * @category   BSellerERP
 * @package    BSellerERP_Vale
 * @author     Julio Reis <julio.reis@e-smart.com.br>
 */
class BSellerERP_Vale_Model_Batch extends BSellerERP_Core_Model_Integrator_Rest
{
    /**
     * Service path api
     *
     * @var string
     */
    public $servicePath = 'formas-pagamento/vales/massivo';

    /**
     * Request Method
     *
     * @var array
     */
    public $requestMethod = Zend_Http_Client::PUT;

    /**
     * Load helper data
     *
     * @var string
     */
    public $currentHelper = 'vale';

    /**
     * BSellerERP_Tracking_Model_Batch constructor
     *
     * @param null|int $batchNumber
     */
    public function __construct($batchNumber = null)
    {
        if (!is_null($batchNumber)) {
            $this->setPathParams(array($batchNumber));
        }

        return parent::__construct();
    }

    /**
     * Flag batch as success
     *
     * @return $this
     * @throws Mage_Core_Exception
     */
    protected function success()
    {
        $body = Mage::helper('core')->jsonDecode($this->response->getBody(), Zend_Json::TYPE_OBJECT);

        if (!$body->message) {
            Mage::throwException($this->helper->__('Body response not exist data for confirm batch'));

            return $this;
        }

        $this->helper->log('The batch was successfully confirmed: #' . $this->paramsPath);

        return $this;
    }

    /**
     * Flag batch as failed
     *
     * @return $this
     */
    protected function failed()
    {
        $this->helper->log($this->response->getBody());

        return $this;
    }
}