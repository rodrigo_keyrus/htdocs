<?php
/**
 * BSeller vale module helper
 *
 * @category   BSellerERP
 * @package    BSellerERP_Vale
 * @author     Julio Reis <julio.reis@e-smart.com.br>
 */
class BSellerERP_Vale_Model_Cron
{
    /**
     * Returns helper object
     * 
     * @var BSellerERP_Vale_Helper_Data
     */
    public $helper;

    /**
     * Initialize helper and factory model
     */
    public function __construct()
    {
        $this->helper = Mage::helper('bsellererp_vale');
    }

    /**
     * Init import vales
     *
     * @return $this
     */
    public function import()
    {
        if (!$this->helper->moduleIsActive()) {
            return $this;
        }
        
        try {
            Mage::getModel('bsellererp_vale/integrator')->init();
            $this->helper->log('Integration vales success');
        } catch (Exception $e) {
            $this->helper->log($e->getMessage());
        }

        return $this;
    }
}
