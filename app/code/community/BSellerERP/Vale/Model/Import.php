<?php

/**
 * BSeller vale module helper
 *
 * @category   BSellerERP
 * @package    BSellerERP_Vale
 * @author     Julio Reis <julio.reis@e-smart.com.br>
 */
class BSellerERP_Vale_Model_Import extends BSellerERP_Vale_Model_Schema
{
    /**
     * Content returned integration to import
     * @var object
     */
    protected $vales;

    /**
     * Init importing vales
     */
    public function init()
    {
        Mage::dispatchEvent($this->helper->getPrefix() . '_import_init', array('vales' => $this->vales));

        foreach ($this->vales as $vale) {
            if (!$this->validate($vale)) {
                continue;
            }

            try {
                $this->create($vale);
            } catch (Exception $ex) {
                $this->helper->log('Error at vale (cpf Customer): ' . $vale->cpfCliente . ' (' . $ex->getMessage() . ')', null, BSellerERP_Log_Model_Item_Attribute_Source_Status::ERROR, json_encode($vale));
            }
        }

        Mage::dispatchEvent($this->helper->getPrefix() . '_import_complete', array('vales' => $this->vales));

        return $this;
    }

    /**
     * Set data vales
     *
     * @param $vales
     * @return $this
     */
    public function setVales($vales)
    {
        $this->vales = $vales;

        return $this;
    }

    /**
     * Create/Update vale by response ERP
     *
     * @param $valeERP
     * @return $this
     * @throws Exception
     */
    public function create($responseObjectItem)
    {
        $this->responseObjectItem = $responseObjectItem;
        $this->objectModel = Mage::getModel('bsellererp_vale/items')->loadByField('idVale', $this->responseObjectItem->idVale);

        /**
         * Adding data as scheme
         */
        $this->objectModel->addData($this->prepareSchema());

        Mage::dispatchEvent($this->helper->getPrefix() . '_item_save_before', array('vale' => $this->objectModel));

        $this->objectModel->save();

        //set free the orders paid by vale credit
        $idVale = $this->objectModel->getData('idVale');
        $orders = Mage::getModel('sales/order')->getCollection()
            ->addFieldToFilter('applied_vales', array('like' => '%' . $idVale . '%'));
        if ($orders && count($orders) > 0) {
            foreach ($orders as $order) {
                $orderHistory = Mage::getModel('esmart_voucher/history')->load($order->getId(), 'order_id');
                if ($orderHistory && $orderHistory->getId()) {
                    $orderHistory->update(Esmart_Voucher_Model_History::STATUS_PAYED)->save();
                }
            }
        }
        //end

        $this->helper->log('Vale imported (cpf Customer): ' . $responseObjectItem->cpfCliente, null, BSellerERP_Log_Model_Item_Attribute_Source_Status::SUCCESS, json_encode($responseObjectItem));

        Mage::dispatchEvent($this->helper->getPrefix() . '_item_save_after', array('vale' => $this->objectModel));

        return $this;
    }

    /**
     * Validate vale for create in application
     *
     * @param object $vale
     * @return bool
     */
    protected function validate($vale)
    {
        $requiredParams = array(
            'cpfCliente', 'idVale', 'situacao', 'valor'
        );

        foreach ($requiredParams as $param) {
            if ($vale->$param === false) {
                $this->helper->log('Vale is not valid for importing, missing param: ' . $param);
                $this->helper->log($vale);

                return false;
            }
        }

        return true;
    }
}
