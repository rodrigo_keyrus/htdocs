<?php
/**
 * BSeller vale module helper
 *
 * @category   BSellerERP
 * @package    BSellerERP_Vale
 * @author     Julio Reis <julio.reis@e-smart.com.br>
 */
class BSellerERP_Vale_Model_Integrator extends BSellerERP_Core_Model_Integrator_Rest
{
    /**
     * Service path category api
     *
     * @var string
     */
    public $servicePath = 'formas-pagamento/vales/massivo';

    /**
     * Load helper data
     *
     * @var string
     */
    public $currentHelper = 'vale';

    public function _beforeInit()
    {
        /**
         * Adding query params
         */
        $params = array(
            'unidadeNegocio' => Mage::getStoreConfig('bsellererp_core/settings/business_unit')
        );

        $this->setQueryParams($params);
    }

    /**
     * Flag integration as success
     *
     * @return $this
     * @throws Mage_Core_Exception
     */
    protected function success()
    {
        $body = Mage::helper('core')->jsonDecode($this->response->getBody(), Zend_Json::TYPE_OBJECT);

        if (!$body || !count($body->content)) {
            Mage::throwException($this->helper->__('Body response not exist data for importing'));
            return $this;
        }

        $import = Mage::getModel('bsellererp_vale/import');
        $import->setVales($body->content);
        $import->init();

        if ($batchNumber = $body->batchNumber) {
            Mage::getModel('bsellererp_vale/batch', $batchNumber)->init();
        }

        return $this;
    }

    /**
     * Flag integration as failed
     *
     * @return $this
     */
    protected function failed()
    {
        $this->helper->log($this->response->getBody());

        return $this;
    }
}
