<?php
/**
 * BSeller vale module helper
 *
 * @category   BSellerERP
 * @package    BSellerERP_Vale
 * @author     Julio Reis <julio.reis@e-smart.com.br>
 */
class BSellerERP_Vale_Model_Observer
{
    /**
     * @var BSellerERP_Order_Helper_Data
     */
    protected $helper;

    /**
     * Load helper model
     */
    public function __construct()
    {
        $this->helper = Mage::helper('bsellererp_vale');
    }

    public function salesOrderPlaceBefore(Varien_Event_Observer $observer) {
        if (!$this->helper->moduleIsActive()) {
            return $this;
        }

        /**
         * @var Mage_Sales_Model_Order $order
         */
        $order = $observer->getEvent()->getOrder();

        $usedValue = $order->getVoucherCreditAmountUsed();

        if(!floatval($usedValue)) {
            return $this;
        }

        $customer = Mage::getModel('customer/customer')->load($order->getCustomerId());

        $cpfCnpj = $customer->getCpf() ? $customer->getCpf() : $customer->getCnpj();

        $availableVales = Mage::helper('bsellererp_vale')->getAvailableVales($cpfCnpj);

        $arrayReturn = array();

        foreach ($availableVales as $availableVale) {
            if ($usedValue <= $availableVale->getSaldo()) {
                $arrayReturn[$availableVale->getData('idVale')] = number_format($usedValue, 2, '.', '');
                $availableVale->setSaldo(number_format($availableVale->getSaldo() - $usedValue, 2, '.', ''));
                $availableVale->save();
                break;
            } else {
                $arrayReturn[$availableVale->getData('idVale')] = number_format($availableVale->getSaldo(), 2, '.', '');
                $usedValue = number_format($usedValue - $availableVale->getSaldo(), 2, '.', '');
                $availableVale->setSaldo(0);
                $availableVale->save();
            }
        }
        $order->setData('applied_vales', json_encode($arrayReturn));

        return $this;
    }
}