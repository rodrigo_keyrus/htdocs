<?php
/**
 * BSeller vale module helper
 *
 * @category   BSellerERP
 * @package    BSellerERP_Vale
 * @author     Julio Reis <julio.reis@e-smart.com.br>
 */
class BSellerERP_Vale_Model_Items_Status
{
    const VALE_ITEMS_ATIVO = 'A';
    const VALE_ITEMS_CANCELADO = 'C';
    const VALE_ITEMS_LIQUIDADO = 'C';
}