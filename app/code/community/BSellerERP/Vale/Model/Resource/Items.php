<?php
/**
 * BSeller vale module helper
 *
 * @category   BSellerERP
 * @package    BSellerERP_Vale
 * @author     Julio Reis <julio.reis@e-smart.com.br>
 */
class BSellerERP_Vale_Model_Resource_Items extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Resource model initialization
     */
    protected function _construct()
    {
        $this->_init('bsellererp_vale/items', 'entity_id');
    }
}
