<?php
/**
 * BSeller vale module
 *
 * @category   BSellerERP
 * @package    BSellerERP_Vale
 * @author     Julio Reis <julio.reis@e-smart.com.br>
 */
class BSellerERP_Vale_Model_Items extends Mage_Core_Model_Abstract
{
    /**
     * Initialization items model
     */
    protected function _construct()
    {
        $this->_init('bsellererp_vale/items');
    }

    public function loadByField($field, $value) {
        return $this->getCollection()
            ->addFieldToFilter($field, $value)
            ->getFirstItem();
    }
}
