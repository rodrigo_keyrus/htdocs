<?php
/**
 * BSeller vale module helper
 *
 * @category   BSellerERP
 * @package    BSellerERP_Vale
 * @author     Julio Reis <julio.reis@e-smart.com.br>
 */
class BSellerERP_Vale_Helper_Data extends BSellerERP_Core_Helper_Data
{
    /**
     * General prefix for using in logs and events
     *
     * @var string
     */
    protected $prefix = 'vale';

    public function getAllWebsiteIds() {
        $websitesIds = array();

        foreach (Mage::app()->getWebsites() as $website) {
            $websitesIds[] = $website->getId();
        }

        return $websitesIds;
    }

    public function decryptValeKey($data)
    {
        $key = "131037638026587213103763";
        $iv = "68743802";
        $data = hex2bin($data);
        $decrypt = mcrypt_decrypt(MCRYPT_3DES, $key, $data, MCRYPT_MODE_CBC, $iv);
        return strtoupper($decrypt);
    }

    public function encryptValeKey($data)
    {
        $key = "131037638026587213103763";
        $iv = "68743802";
        $encrypt = mcrypt_encrypt(MCRYPT_3DES, $key, $data, MCRYPT_MODE_CBC, $iv);
        $encrypt = bin2hex($encrypt);
        return strtoupper($encrypt);
    }

    public function getValeAmount($customerCpf) {
        /** @var Esmart_Voucher_Model_Resource_History $resource */
        $resource = Mage::getResourceModel('bsellererp_vale/items');

        /** @var Varien_Db_Adapter_Interface $readAdapter */
        $readAdapter = $resource->getReadConnection();

        $select = $readAdapter->select()
            ->from($resource->getMainTable(), new Zend_Db_Expr('SUM(valor)'))
            ->where('cpfCliente = ?', $customerCpf)
            ->where('situacao = ?', BSellerERP_Vale_Model_Items_Status::VALE_ITEMS_ATIVO)
            ->where('DATE_FORMAT(validade,"%Y-%m-%d") >= DATE_FORMAT(now(),"%Y-%m-%d")');

        /** @var array $histories */
        $amount = $readAdapter->fetchOne($select);
        return $amount;
    }

    public function getAvailableVales($customerCpf)
    {
        $availableVales = Mage::getModel('bsellererp_vale/items')->getCollection()
            ->addFieldToFilter('cpfCliente', $customerCpf)
            ->addFieldToFilter('situacao', BSellerERP_Vale_Model_Items_Status::VALE_ITEMS_ATIVO)
            ->addFieldToFilter('saldo', array('gt' => 0));
        $availableVales->getSelect()
            ->where('DATE_FORMAT(validade,"%Y-%m-%d") >= DATE_FORMAT(now(),"%Y-%m-%d")')
            ->order('entity_id asc');

        return $availableVales;
    }

    public function getValueAvailableVales($customerCpf)
    {
        /** @var Esmart_Voucher_Model_Resource_History $resource */
        $resource = Mage::getResourceModel('bsellererp_vale/items');

        /** @var Varien_Db_Adapter_Interface $readAdapter */
        $readAdapter = $resource->getReadConnection();

        $select = $readAdapter->select()
            ->from($resource->getMainTable(), new Zend_Db_Expr('SUM(saldo)'))
            ->where('cpfCliente = ?', $customerCpf)
            ->where('situacao = ?', BSellerERP_Vale_Model_Items_Status::VALE_ITEMS_ATIVO)
            ->where('DATE_FORMAT(validade,"%Y-%m-%d") >= DATE_FORMAT(now(),"%Y-%m-%d")');

        /** @var array $histories */
        $amount = $readAdapter->fetchOne($select);
        return $amount;
    }

    public function decrementVales($vales) {
        $vales = json_decode($vales);
        foreach($vales as $key => $item) {
            $model = Mage::getModel('bsellererp_vale/items')->getCollection()
                ->addFieldToFilter('idVale', $key)
                ->getFirstItem();

            $newValue = $model->getValor() - $item;
            if ($newValue < 0) {
                $newValue = 0;
            }
            $model->setValor($newValue);
            $model->save();
        }
    }
}
