<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_PaymentBrands
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * BSeller module helper
 *
 * @category   BSellerERP
 * @package    BSellerERP_PaymentBrands
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_PaymentBrands_Helper_Data extends BSellerERP_Core_Helper_Data
{

    /**
     * General prefix for using in logs and events
     *
     * @var string
     */
    protected $prefix = 'payment_brands';

    /**
     * Get brand code by code
     *
     * @param  string $code
     * @return bool
     */
    public function getBrandCode($code)
    {
        $optionsSerialized = Mage::getStoreConfig('bsellererp_payment_brands/mapping/brands');

        if (!$optionsSerialized) {
            return false;
        }

        $options = (array) unserialize($optionsSerialized);

        if (!count($options)) {
            return false;
        }

        foreach ($options as $option) {
            if ($option['application'] != $code) {
                continue;
            }

            return $option['erp'];
        }

        $this->log('Impossible find the brand in mapping, brand: ' . $code);

        return false;
    }

}
