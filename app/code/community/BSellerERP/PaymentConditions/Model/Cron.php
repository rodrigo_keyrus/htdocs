<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Order
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Returns all sales channel
 *
 * @category   BSellerERP
 * @package    BSellerERP_Order
 * @author     Julio Reis <julio.reis@e-smart.com.br>
 */
class BSellerERP_PaymentConditions_Model_Cron
{
    /**
     * Returns helper object
     *
     * @var BSellerERP_PaymentConditions_Helper_Data
     */
    public $helper;

    /**
     * Initialize helper and factory model
     */
    public function __construct()
    {
        $this->helper = Mage::helper('bsellererp_payment_conditions');
    }

    /**
     * Init import
     *
     * @return $this
     */
    public function import()
    {
        if (!$this->helper->moduleIsActive()) {
            return $this;
        }

        try {
            Mage::getModel('bsellererp_payment_conditions/integrator')->init();
            $this->helper->log('Integration conditions success');
        } catch (Exception $e) {
            $this->helper->log($e->getMessage());
        }

        return $this;
    }
}
