<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_PaymentConditions
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/* @var $installer BSellerERP_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

/**
 * Create 'Credit Card Conditions' table
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('bsellererp_payment_conditions/items'))
    ->addColumn(
        'entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
        array(
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
            'primary'  => true
        ), 'Entity ID'
    )
    ->addColumn(
        'condition_id', Varien_Db_Ddl_Table::TYPE_TEXT, 30,
        array(
            'unsigned' => true,
            'nullable' => false,
        ), 'Condition ID'
    )
    ->addColumn(
        'condition_name', Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(
            'unsigned' => true,
            'nullable' => false,
        ), 'Condition Name'
    )
    ->setComment('BSeller ERP - Payment Conditions Integration');

$installer->getConnection()->createTable($table);

$installer->endSetup();