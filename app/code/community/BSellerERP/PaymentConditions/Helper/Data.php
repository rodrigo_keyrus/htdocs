<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Order
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Returns all sales channel
 *
 * @category   BSellerERP
 * @package    BSellerERP_Order
 * @author     Julio Reis <julio.reis@e-smart.com.br>
 */
class BSellerERP_PaymentConditions_Helper_Data extends BSellerERP_Core_Helper_Data
{

    /**
     * General prefix for using in logs and events
     *
     * @var string
     */
    protected $prefix = 'payment_conditions';

    /**
     * Get condition code by code
     *
     * @param  string $code
     * @return bool
     */
    public function getConditionCode($code)
    {
        $optionsSerialized = Mage::getStoreConfig('bsellererp_payment_conditions/mapping/conditions');

        if (!$optionsSerialized) {
            return false;
        }

        $options = (array) unserialize($optionsSerialized);

        if (!count($options)) {
            return false;
        }

        foreach ($options as $option) {
            if ($option['application'] != $code) {
                continue;
            }

            return $option['erp'];
        }

        $this->log('Impossible find the condition in mapping, condition: ' . $code);

        return false;
    }

}
