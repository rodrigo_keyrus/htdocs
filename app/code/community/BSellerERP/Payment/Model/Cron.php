<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Payment
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Cron schedule model
 *
 * @category   BSellerERP
 * @package    BSellerERP_Payment
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Payment_Model_Cron
{
    /**
     * Init import data
     *
     * @return $this
     */
    public function import()
    {
        if (!$this->helper()->moduleIsActive()) {
            return $this;
        }

        try {
            Mage::getModel('bsellererp_payment/integrator')->init();

            $this->helper()->log('Integration payments success');
        } catch (Exception $e) {
            $this->helper()->log($e->getMessage());

            Mage::throwException($e->getMessage());
        }

        return $this;
    }

    /**
     * Return data helper object
     *
     * @return BSellerERP_Payment_Helper_Data
     */
    protected function helper()
    {
        return Mage::helper('bsellererp_payment');
    }
}
