<?php

class BSellerERP_Order_Block_Adminhtml_Sales_Order_View_Tab_IntegrationJson
    extends Mage_Adminhtml_Block_Sales_Order_Abstract
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    /**
     * Internal constructor, that is called from real constructor
     *
     */
    public function _construct()
    {
        parent::_construct();
        $this->setTemplate('bsellererp_order/order/view/tab/integration_json.phtml');
    }

    /**
     * Return Tab label
     *
     * @return string
     */
    public function getTabLabel() {
        return $this->_helper()->__('BSeller Integration (Json)');
    }

    /**
     * Return Tab title
     *
     * @return string
     */
    public function getTabTitle() {
        return $this->_helper()->__('BSeller Integration (Json)');
    }

    /**
     * Can show tab in tabs
     *
     * @return boolean
     */
    public function canShowTab() {
        return true;
    }

    /**
     * Tab is hidden
     *
     * @return boolean
     */
    public function isHidden() {
        return false;
    }

    protected function _helper()
    {
        return Mage::helper('bsellererp_order');
    }

    /**
     * Get Integration Json
     *
     * @return string
     */
    public function getIntegrationJson()
    {
        $order = Mage::registry('current_order');

        $export = Mage::getModel('bsellererp_order/export');
        $export->setOrder($order->getIncrementId());
        $schema = $export->prepareSchema();

        $jsonFormat = json_encode($schema, JSON_PRETTY_PRINT);

        return $jsonFormat;
    }
}