<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 08/09/16
 * Time: 18:45
 */

class BSellerERP_Order_Block_Adminhtml_Sales_Order_Grid_Erpstatuscolumn extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        return Mage::helper('bsellererp_order')->getErpStatusLabel($row->getErpStatus());
    }

}