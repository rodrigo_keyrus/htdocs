<?php

class BSellerERP_Order_Model_Reproccess_Cron
{
    /**
     * Returns helper object
     *
     * @var BSellerERP_Order_Helper_Data
     */
    public $helper;

    /**
     * Initialize helper and factory model
     */
    public function __construct()
    {
        $this->helper = Mage::helper('bsellererp_order');
    }

    /**
     * Init import orders
     *
     * @return $this
     */
    public function export()
    {
        if (!$this->helper->reproccessingModuleIsActive()) {
            return $this;
        }

        if ($this->helper->getSettingStoreConfig('queue')) {
            $this->_exportWithResqueService();
            return $this;
        }

        $orders = $this->_getCollection();

        try {
            /**
             * Prepare schema and init integration for all orders
             */
            foreach ($orders as $order) {
                try {
                    $export = Mage::getModel('bsellererp_order/export');
                    $export->setOrder($order->getIncrementId());
                    $export->init();
                } catch (Exception $e) {
                    $this->helper->log($e->getMessage());
                }
            }
            $this->helper->log('Integration order success');
        } catch (Exception $e) {
            $this->helper->log($e->getMessage());
        }

        return $this;
    }

    private function _exportWithResqueService()
    {
        /**
         * Get all pending orders
         */
        $orders = $this->_getCollection();

        $ordersIds = array();

        foreach ($orders as $order) {
            $ordersIds[] = $order->getId();
        }

        $chunkData = array_chunk(
            $ordersIds,
            BSellerERP_Order_Helper_Data::PRODUCT_QUEUE_LIMIT
        );

        for ($i = 0; $i < $length = count($chunkData); $i++) {
            $args['orders_ids'] = $chunkData[$i];

            $resque = Mage::getModel('bseller_resque/resque');

            $queueName = BSellerERP_Order_Helper_Data::RESQUE_QUEUE_NAME;
            $this->helper->log('Sending to bseller_resque module. Queue name: ' . $queueName . ' <> Args: ' . json_encode($args));
            $resque->enqueue(
                $queueName,
                BSellerERP_Order_Helper_Data::RESQUE_JOB_CLASS,
                $args
            );
        }

        return $this;
    }

    private function _getCollection()
    {
        $expireDays = 10;
        $time = time();
        $to = date('Y-m-d H:i:s', $time);
        $lastTime = $time - (86400 * $expireDays);
        $from = date('Y-m-d H:i:s', $lastTime);

        $orders = Mage::getResourceModel('sales/order_collection')
            ->addFieldToFilter('erp_status', 'blocked');

        if ($expireDays && $from) {
            $orders->addFieldToFilter('created_at', array('from' => $from, 'to' => $to));
        }

        return $orders;
    }
}
