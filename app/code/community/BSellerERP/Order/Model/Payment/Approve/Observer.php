<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Order
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Observer model
 *
 * @category   BSellerERP
 * @package    BSellerERP_Order
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Order_Model_Payment_Approve_Observer
{
    /**
     * @var BSellerERP_Order_Helper_Data
     */
    protected $helper;

    /**
     * Load helper model
     */
    public function __construct()
    {
        $this->helper = Mage::helper('bsellererp_order');
    }

    /**
     * Add approve payment in invoice
     *
     * @param  Varien_Event_Observer $observer
     * @return $this
     */
    public function addApprovePaymentButton(Varien_Event_Observer $observer)
    {
        if (!$this->helper->moduleIsActive()) {
            return $this;
        }

        $block = $observer->getBlock();

        if (!$block instanceof Mage_Adminhtml_Block_Sales_Order_Invoice_View) {
            return $this;
        }

        $invoiceId = Mage::app()->getRequest()->getParam('invoice_id');
        $message   = $this->helper->__('Are you sure you want to do this?');
        $url       = $block->getUrl('bsellererp_order/adminhtml_index/resendPayment', array('invoice_id' => $invoiceId));

        $block->addButton(
            'resend_order_integration',
            array(
                'label'     => $this->helper->__('ERP - Resend Payment'),
                'onclick'   => "confirmSetLocation('{$message}', '{$url}')"
            )
        );

        return $this;
    }

    public function electOrderForErpPaymentApprove($observer) {
        $invoice = $observer->getInvoice();
        $order = $invoice->getOrder();

        if ($order->getErpStatus() == BSellerERP_Order_Helper_Status::BLOCKED_APPROVE) {
            //re-enabling erp payment integration
            $order->setData('erp_status', BSellerERP_Order_Helper_Status::SUCCESS);
            $order->save();
        }
    }
}
