<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Order
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Cron schedule model
 *
 * @category   BSellerERP
 * @package    BSellerERP_Order
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Order_Model_Payment_Approve_Cron
{
    const RECORDS_LIMIT = 100;

    /**
     * Returns helper object
     * 
     * @var BSellerERP_Order_Helper_Data
     */
    public $helper;

    /**
     * Initialize helper and factory model
     */
    public function __construct()
    {
        $this->helper = Mage::helper('bsellererp_order');
    }

    /**
     * Init import orders
     *
     * @return $this
     */
    public function export()
    {
        /**
         * Validate if module is active
         */
        if (!$this->helper->moduleIsActive()) {
            return $this;
        }

        /**
         * Validate if invoice is active
         */
        if (!$this->helper->getOrderStoreConfig('invoice_active')) {
            return $this;
        }

        /**
         * Get all pending orders
         */
        $orders = Mage::getModel('sales/order')
            ->getCollection()
            ->addFieldToFilter('erp_status', BSellerERP_Order_Helper_Status::SUCCESS)
            ->addFieldToFilter('status', Mage_Sales_Model_Order::STATE_PROCESSING)
            ->setPageSize(self::RECORDS_LIMIT);
        
        try {
            /**
             * Prepare schema and init integration for all orders
             * @var Mage_Sales_Model_Order $order
             */
            foreach ($orders as $order) {
                $this->helper->log('Starting approve payment for to order: ' . $order->getIncrementId());
                
                $export = Mage::getModel('bsellererp_order/payment_approve_export');

                if ($export->setOrder($order)) {
                    $export->init();
                }
            }

            $this->helper->log('Integration order success');
        } catch (Exception $e) {
            $this->helper->log($e->getMessage());
        }

        return $this;
    }
}
