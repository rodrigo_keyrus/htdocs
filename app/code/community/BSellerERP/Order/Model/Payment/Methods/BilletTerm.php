<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Order
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Billet Term payment model
 *
 * @category   BSellerERP
 * @package    BSellerERP_Order
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Order_Model_Payment_Methods_BilletTerm extends Varien_Object
{
    /**
     * Returns helper object
     *
     * @var BSellerERP_Core_Helper_Data
     */
    protected $helper;

    /**
     * Load helper model and get current store
     */
    public function __construct()
    {
        parent::__construct();

        $this->helper = Mage::helper('bsellererp_order');
    }

    /**
     * Starting payment schema
     *
     * @return $this
     */
    public function init()
    {
        $this->addBilletData();
        
        return $this;
    }

    /**
     * Adding information billet payment
     *
     * @return Varien_Object
     */
    public function addBilletData()
    {
        $data = array(
            'codigoBanco'             => $this->helper->getSettingStoreConfig('bank_code'),
            'codigoAgencia'           => $this->helper->getSettingStoreConfig('number_agency'),
            'numeroConta'             => $this->helper->getSettingStoreConfig('number_account'),
        );

        return $this->addData($data);
    }

    /**
     * Returns schema
     *
     * @return array
     */
    public function getSchema()
    {
        $this->init();

        return $this->toArray();
    }
}
