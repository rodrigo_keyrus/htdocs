<?php

class BSellerERP_Order_Model_Job extends BSeller_Resque_Model_Job_Abstract
{
    public $helper;

    public function __construct()
    {
        parent::_construct();
        $this->helper = Mage::helper('bsellererp_order');
    }

    public function perform()
    {
        $args = $this->getArgs();
        $ordersIds = $args['orders_ids'];
        try {
            foreach ($ordersIds as $orderId) {
                try {
                    /** @var Mage_Sales_Model_Order $order */
                    $order = Mage::getModel('sales/order')->load($orderId);
                    /** The order may be canceled after it goes to integration queue */
                    if ($order->isCanceled()) {
                        $order->setData('erp_status', BSellerERP_Order_Helper_Status::INTERNAL_BLOCKED);
                        $order->save();
                        continue;
                    }
                    $export = Mage::getModel('bsellererp_order/export');
                    $export->setOrder($order->getIncrementId());
                    $export->init();
                } catch (Exception $e) {
                    $order->setData('erp_status', BSellerERP_Order_Helper_Status::PENDING);
                    $order->save();
                    $this->helper->log($e->getMessage());
                }
            }
            $this->helper->log('Integration order success');
        } catch (Exception $e) {
            $this->helper->log($e->getMessage());
        }
        return $this;
    }
}