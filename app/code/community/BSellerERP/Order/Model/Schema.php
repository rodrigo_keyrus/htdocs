<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Order
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Schema model
 *
 * @category   BSellerERP
 * @package    BSellerERP_Order
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Order_Model_Schema extends BSellerERP_Core_Model_Schema_Abstract
{
    /**
     * Available product type
     */
    const TYPE_DOWNLOADABLE = 'downloadable';

    /**
     * Gift card product type
     */
    const TYPE_GIFTCARD = 'giftcard';

    /**
     * Product virtual type ERP
     */
    const ITEM_TYPE_VIRTUAL = 'N';

    /**
     * Product simple/configurable type ERP
     */
    const ITEM_TYPE_PRODUCT = 'P';

    /**
     * Content returned integration to import
     *
     * @var Mage_Sales_Model_Order
     */
    protected $order;

    /**
     * Customer by order
     *
     * @var Mage_Customer_Model_Customer
     */
    protected $customer;
    protected $carrier;
    protected $carrierContract;

    /**
     * Returns helper object
     *
     * @var BSellerERP_Order_Helper_Data
     */
    protected $helper;

    /**
     * Load helper data
     *
     * @var string
     */
    public $currentHelper = 'order';

    public $valeDiscountAmount;

    /**
     * Load helper data
     *
     * @var float
     */
    public $deductionAmount;
    public $giftAmount;

    /**
     * Prepare schema and returns for adding in order instance
     *
     * @return array
     */
    public function prepareSchema()
    {
        parent::prepareSchema();

        if (!$this->order->getId()) {
            Mage::throwException('Order not exist for create schema');
        }

        $this->customer = Mage::getModel('customer/customer')->load($this->order->getCustomerId());

        $this->initCarrierAndContract();

        $this->setGeneralData();
        $this->setCustomerAddressData($this->order->getBillingAddress());
        $this->setCustomerAddressData($this->order->getShippingAddress() ? $this->order->getShippingAddress() : $this->order->getBillingAddress(), 'shipping');
        $this->setItemsData();
        $this->setShippingData();
        $this->setPaymentData();
        $this->setTotalsData();

        return $this->getSchema();
    }

    /**
     * Setting general data for schema
     *
     * @return $this
     */
    protected function setGeneralData()
    {
        $incrementId = $this->order->getIncrementId();

        $customerHelper = Mage::helper('bsellererp_core/customer');
        $customerHelper->setCustomer($this->customer);
        $this->addData(
            array(
                'canalVenda'            => $this->helper->getOrderStoreConfig('sales_channel'),
                'codigoVendedor'        => $this->order->getData('sac_operator'),
                'dataEmissao'           => $this->helper->formatDate($this->order->getCreatedAt()),
                'dataInclusao'          => $this->helper->formatDate($this->order->getCreatedAt()),
                'numeroPedido'          => $incrementId,
                'numeroPedidoExterno'   => $incrementId,
                'numeroPedidoLoja'      => $incrementId,
                'origemPedido'          => $this->helper->getOrderStoreConfig('sale_origin'),
                'pedidoParaConsumo'     => (bool) $this->helper->getOrderStoreConfig('consumption'),
                'tipoPedido'            => $customerHelper->getTipoPedido(),
                'entregasQuebradas'     => false,
                'observacoesEtiqueta'   => null,
                'tipoFrete'             => 'C',
                'unidadeNegocio'        => $this->helper->getSettingStoreConfig('business_unit'),
            )
        );

        return $this;
    }

    /**
     * Setting customer address by type for schema
     *
     * @param Mage_Sales_Model_Order_Address $address
     * @return $this
     * @throws Mage_Core_Exception
     */
    protected function setCustomerAddressData(Mage_Sales_Model_Order_Address $address, $addressType = 'billing')
    {
        if (!$address) {
            Mage::throwException('Address not exist for create schema - Order: ' . $this->order->getIncrementId());
            return $this;
        }

        $customerHelper = Mage::helper('bsellererp_core/customer');
        $customerHelper->setCustomer($this->customer);
        $customerHelper->setAddress($address);

        $formatAddress = $customerHelper->formatAddress(
            $this->helper->getStreetByName('address_number', $address),
            $this->helper->getStreetByName('address_street', $address),
            $this->helper->getStreetByName('address_complement', $address)
        );

        $addressInfo = array(
            'bairro'            => $this->helper->truncate($this->helper->getStreetByName('address_neighborhood', $address), 50),
            'logradouro'        => $this->helper->truncate($formatAddress['street'], 60),
            'numero'            => $this->helper->onlyNumbers($formatAddress['number']),
            'complemento'       => $this->helper->truncate($formatAddress['complement'], 50),
            'pontoReferencia'   => $this->helper->truncate($this->helper->getPredefinedAttributeValue('reference_point', $address), 100),
            'cep'               => $this->helper->onlyNumbers($address->getPostcode()),
            'cidade'            => $address->getCity(),
            'estado'            => $address->getRegionCode(),
            'pais'              => Mage::app()->getLocale()->getCountryTranslation($address->getCountry()),
            'zipCode'           => null,
        );

        $cellphone = $this->helper->getPredefinedAttributeValue('cellphone', $address);

        $generalInfo = array(
            'classificacao'         => $customerHelper->getClassificacao($this->customer),
            'dataNascimento'        => $this->helper->formatDate($this->order->getCustomerDob()),
            'email'                 => $address->getEmail(),
            'endereco'              => $addressInfo,
            'fax'                   => $address->getFax(),
            'nome'                  => $this->helper->truncate($customerHelper->getName(), 40),
            'rg'                    => $this->helper->getPredefinedAttributeValue('rg', $this->customer),
            'telefoneResidencial'   => $this->helper->onlyNumbers($address->getTelephone()),
            'telefoneCelular'       => $this->helper->onlyNumbers($cellphone),
            'telefoneComercial'     => null,
            'id'                    => $customerHelper->getDocumentValue()
        );

        /**
         * Append person type info
         */
        $generalInfo = array_merge($generalInfo, $customerHelper->preparePersonType());

        /**
         * Configure type node customer address
         */
        $node = 'clienteFaturamento';
        if ($addressType == Mage_Customer_Model_Address::TYPE_SHIPPING) {
            $node = 'clienteEntrega';
        }

        $this->addData(
            array(
                $node => $generalInfo
            )
        );

        return $this;
    }

    protected function initCarrierAndContract()
    {
        $carrier        = Mage::helper('bsellererp_shipping')->getCarrierByShipping($this->order->getShippingMethod());
        $contract       = Mage::helper('bsellererp_shipping')->getContractByShipping($this->order->getShippingMethod());

        $carrierContractId = null;

        if ($carrier->getData('carrier_id')) {
            //if is flatrate_flatrate (admin configured - not intelipost service) then aways use default contract_id;
            if (Mage::getStoreConfig('bsellererp_shipping/carrier/use_default_carrier_codes') && $this->order->getShippingMethod() != 'flatrate_flatrate') {
                $this->carrierContract = substr($this->order->getShippingMethod(), (strrpos($this->order->getShippingMethod(), '_') ?: -1) + 1);
            } else {
                $this->carrierContract = $contract;
            }
        }
        $this->carrier = $carrier;
    }

    /**
     * Setting items data for schema
     *
     * @return $this
     */
    protected function setItemsData()
    {
        $orderItems     = $this->order->getAllVisibleItems();
        $items          = array();
        $shippingAmount = $this->order->getShippingAmount();
        $qtyItems       = count($orderItems);
        $amountItem     = $this->roundDown(($shippingAmount / $qtyItems), 2);
        $transitTime    = $this->helper->onlyNumbers($this->order->getShippingDescription());

        /**
         * Set default transit time
         */
        if (!$transitTime) {
            $transitTime = (int) $this->helper->getOrderStoreConfig('transit_time');
        }

        /**
         * @var Mage_Sales_Model_Order_Item $orderItem
         */
        $totalItemsAmountDiscount = 0;
        $totalItemsQty = 0;

        //Module esmart_voucher required for vale integration works
        $this->valeDiscountAmount = $this->order->getVoucherCreditAmountUsed();

        foreach ($orderItems as $key => $orderItem) {
            if ($orderItem->getProductType() == Mage_Catalog_Model_Product_Type::TYPE_BUNDLE) {
                $children = $orderItem->getChildrenItems();

                $qtyItems += (count($children) - 1);
                $amountItem = $this->roundDown(($shippingAmount / $qtyItems), 2);

                $discountPerItem = (float) number_format($orderItem->getDiscountAmount()/count($children), 2);

                foreach ($children as $child) {
                    $attributes = (array) Zend_Serializer::unserialize($child->getProductOptions()['bundle_selection_attributes']);
                    $child->setQtyOrdered($attributes['qty']);
                    $child->setPrice((float)number_format($attributes['price']/$attributes['qty'], 2));
                    $child->setDiscountAmount($discountPerItem);
                    $child->setParentItemSku(explode('-', $orderItem->getSku())[0]);
                }
                unset($orderItems[$key]);
                $orderItems = array_merge($orderItems, $children);
            }
        }

        $index = 0;
        foreach ($orderItems as $key => $orderItem) {
            $qtyOrdered         = $orderItem->getQtyOrdered() * 1;

            $giftDiscount = $this->treatGift($orderItem);

            $discountAmount     = round($orderItem->getDiscountAmount(), 2, PHP_ROUND_HALF_ODD);
            $discountAmountItem = $discountAmount;

            $totalItemsAmountDiscount += $discountAmountItem;
            $discountAmountItem += $giftDiscount;

            $totalItemsQty += $qtyOrdered;

            /**
             * Calculate discount amount per item
             */
            if ($discountAmount && $qtyOrdered > 1) {
                $calcDiscount = $this->roundDown($discountAmount/$qtyOrdered, 2);
                $discountAmountItem = $calcDiscount;
                if (($calcDiscount * $qtyOrdered) != $discountAmount) {
                    $discountAmountItem = $this->roundDown($discountAmountItem, 2);
                    $discountDiff = round($discountAmount - ($discountAmountItem * $qtyOrdered), 2);

                    /**
                     * Increase deduction value
                     */
                    if ($discountDiff > 0) {
                        $this->deductionAmount += $discountDiff;
                    }
                }
            }

            $isConditionalDiscount = $this->helper->getOrderStoreConfig('conditional_discount');

            $item = array(
                'cnpjFilial'                    => $this->helper->getSettingStoreConfig('cnpj_filial'),
                'codigoEstabelecimentoSaida'    => $this->helper->getSettingStoreConfig('output_establishment'),
                'codigoEstabelecimentoEstoque'  => $this->helper->getSettingStoreConfig('stock_establishment'),
                'descontoCondicionalUnitario'   => $isConditionalDiscount ? (string)$discountAmountItem : '0',
                'descontoIncondicionalUnitario' => !$isConditionalDiscount ? (string)$discountAmountItem : '0',
                'prazoCentroDistribuicao'       => (int) $this->helper->getOrderStoreConfig('lead_time'),
                'prazoFornecedor'               => 0,
                'prazoTransitTime'              => $transitTime,
                'idContratoTransportadora'      => $this->carrierContract->getContrato(),
                'idTransportadora'              => $this->carrier->getData('carrier_id'),
                'sequencial'                    => $index,
                'valorFrete'                    => $amountItem,
                'codigoItem'                    => $orderItem->getSku(),
                'tipoItem'                      => $this->getItemType($orderItem),
                'quantidade'                    => $qtyOrdered,
                'precoUnitario'                 => number_format($orderItem->getPrice() + $orderItem->getGiftPrice(), 2, '.', ''),
                'valorDespesas'                 => 0,
                'itemBonificado'                => (($orderItem->getIsGift() && Mage::getStoreConfig('bsellererp_order/settings/gift_treatment')) ? true : false)
            );

            if ($this->deductionAmount) {
                $index++;
                $extraItem = $item;
                $extraItem['valorFrete'] = 0;
                $extraItem['sequencial'] = $index;
                $extraItem['quantidade'] = 1;
                $extraItem['descontoCondicionalUnitario'] = $isConditionalDiscount ? (string)($discountAmountItem + $this->deductionAmount) : '0';
                $extraItem['descontoIncondicionalUnitario'] = !$isConditionalDiscount ? (string)($discountAmountItem + $this->deductionAmount) : '0';

                $item['quantidade'] = $item['quantidade'] - 1;

                $items[] = $item;
                $items[] = $extraItem;
                $this->deductionAmount = 0;
            } else {
                $items[] = $item;
            }

            //if the item has parent defined, he's a kit
            if($orderItem->getParentItemSku()) {
               $items[count($items)-1]['codigoItemKit'] = $orderItem->getParentItemSku();
            }

            $index++;
        }

        if($totalItemsAmountDiscount != $this->order->getDiscountAmount()) {
            $this->deductionAmount += (float) number_format((($this->order->getDiscountAmount()*-1) - $totalItemsAmountDiscount), 2);
        }

        /**
         * Calculate shipping amount and correct prices
         */
        $totalAmount = count($orderItems) * $amountItem;
        $calculation = $shippingAmount - $totalAmount;

        if ($calculation) {
            $items[0]['valorFrete'] = $items[0]['valorFrete'] + $calculation;
        }

        $this->addData(
            array(
                'itens' => $items
            )
        );

        return $this;
    }

    public function treatGift(&$orderItem) {
        //gift treatment
        if($orderItem->getPrice() == '0.00') {
            $orderItem->setIsGift(true);
            if(!Mage::helper('bsellererp_core')->getStoreConfig('bsellererp_order/settings/gift_value')) {
                $orderItem->setGiftPrice('0.01');
            } else {
                $orderItem->setGiftPrice(Mage::getModel('catalog/product')->load($orderItem->getProductId())->getPrice());
            }
            $total = ($orderItem->getGiftPrice() * $orderItem->getQtyOrdered());
            $this->giftAmount += $total;
            return $total;
        }

        return null;
    }

    /**
     * Round price
     *
     * @param  $decimal
     * @param  $precision
     * @return float
     */
    public function roundDown($decimal, $precision)
    {
        $fraction = substr($decimal - floor($decimal), 2, $precision);
        $newDecimal = floor($decimal). '.' .$fraction;

        return floatval($newDecimal);
    }

    /**
     * Setting shipping method data for schema
     *
     * @return $this
     */
    protected function setShippingData()
    {
        $deliveryDate = Mage::helper('bsellererp_core/custom')->getDeliveryDate($this->order);
        $deliveryTime = Mage::helper('bsellererp_core/custom')->getDeliveryTime($this->order);

        $this->addData(
            array(
                'entrega' => array(
                    'dataEntregaAgendada'    => $deliveryDate,
                    'periodoEntregaAgendada' => $deliveryTime,
                    'tipoEntrega'            => $this->carrierContract->getTipoEntrega(),
                )
            )
        );

        return $this;
    }

    /**
     * Setting payment data for schema
     *
     * @return $this
     */
    protected function setPaymentData()
    {
        $paymentData   = new Varien_Object();
        $paymentHelper = Mage::helper('bsellererp_payment');
        $address       = $this->order->getBillingAddress();

        $formatAddress = Mage::helper('bsellererp_core/customer')->formatAddress(
            $this->helper->getStreetByName('address_number', $address),
            $this->helper->getStreetByName('address_street', $address),
            $this->helper->getStreetByName('address_complement', $address)
        );

        $address = array(
            'bairro'            => $this->helper->truncate($this->helper->getStreetByName('address_neighborhood', $address), 20),
            'logradouro'        => $this->helper->truncate($formatAddress['street'], 60),
            'numero'            => $this->helper->onlyNumbers($formatAddress['number']),
            'complemento'       => $this->helper->truncate($formatAddress['complement'], 50),
            'pontoReferencia'   => $this->helper->truncate($this->helper->getPredefinedAttributeValue('reference_point', $address), 100),
            'cep'               => $this->helper->onlyNumbers($address->getPostcode()),
            'cidade'            => $address->getCity(),
            'estado'            => $address->getRegionCode(),
            'pais'              => Mage::app()->getLocale()->getCountryTranslation($address->getCountry()),
            'zipCode'           => null,
        );

        $value = number_format($this->order->getGrandTotal(), 2, '.', '');
        $generalData = array(
            'endereco' => $address,
            'valor'               => $value,
            'sequencial'          => 0,
            'codigoMeioPagamento' => $paymentHelper->getPaymentByCode($this->order->getPayment()->getMethod()),
            'codigoCondicaoPagamento' => $paymentHelper->getPaymentCondition($this->order),
        );

        /**
         * Add general data
         */
        if ($value && ((float)$value) > 0) {
            $paymentData->addData(array(0 => $generalData));
            $this->setOrderFullPaidWithVale(false);
        } else {
            $this->setOrderFullPaidWithVale(true);
        }

        //vale treatment
        $this->setValePaymentData($generalData, $paymentData, $address);

        Mage::dispatchEvent(
            $this->helper->getPrefix() . '_payment',
            array(
                'payment' => $paymentData,
                'order'   => $this->order
            )
        );

        $this->addData(
            array(
                'pagamentos' =>
                    $paymentData->toArray()
            )
        );

        return $this;
    }

    private function setValePaymentData(&$generalData, &$paymentData, $address)
    {
        if(!(float)$this->order->getVoucherCreditAmountUsed()) {
            return;
        }

        //Module esmart_voucher required for vale integration works
        $this->valeDiscountAmount = $this->order->getVoucherCreditAmountUsed();

        $totalValue = number_format(($this->order->getSubtotal() + $this->order->getShippingAmount()), 2, '.', '');
        $paymentHelper = Mage::helper('bsellererp_payment');

        //if the total vale discount is greater than the total order amount, means the only payment will be "vale", so
        //we need to put the attribute "codigoVale" on the schema;
        if ($this->valeDiscountAmount && abs($this->valeDiscountAmount) >= $this->order->getGrandTotal()) {
            $generalData['valor'] = abs($this->valeDiscountAmount);
            $generalData['codigoVale'] = $paymentHelper->getValeCode($this->order);
        } else {
            $generalData['valor'] = $totalValue - ($this->valeDiscountAmount ? abs($this->valeDiscountAmount) : 0);
        }

        if ($this->valeDiscountAmount) {
            $valeCode = $paymentHelper->getValeCode($this->order);
            $valePaymentData = array(
                'endereco' => $address,
                'valor' => abs($this->valeDiscountAmount),
                'sequencial' => $this->getOrderFullPaidWithVale() ? 0 : 1,
                'codigoMeioPagamento' => 4,
                'codigoVale' => $valeCode,
            );
            $paymentData->addData(array($this->getOrderFullPaidWithVale() ? 0 : 1 => $valePaymentData));
        }
    }

    /**
     * Setting totals data for schema
     *
     * @return $this
     */
    protected function setTotalsData()
    {
        $totals = array(
            'valorDespesasFinanceiras' => 0.0,
            'valorDespesasItem'        => 0.0,
            'valorFrete'               => number_format($this->order->getShippingAmount(), 2),
            'valorTotalProdutos'       => number_format($this->order->getSubtotal() + $this->giftAmount, 2, '.', ''),
        );

        $this->addData(
            array(
                'valores' => $totals
            )
        );

        return $this;
    }

    /**
     * Returns item type
     *
     * @param Mage_Sales_Model_Order_Item $item
     * @return string
     */
    protected function getItemType($item)
    {
        switch ($item->getProductType()) {
            case Mage_Catalog_Model_Product_Type::TYPE_VIRTUAL:
            case self::TYPE_GIFTCARD:
            case self::TYPE_DOWNLOADABLE:
                return self::ITEM_TYPE_VIRTUAL;

                break;

            default:
                return self::ITEM_TYPE_PRODUCT;

                break;
        }
    }
}