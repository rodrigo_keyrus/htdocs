<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Product
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * BSeller core module helper
 *
 * @category   BSellerERP
 * @package    BSellerERP_Product
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Product_Helper_Data extends BSellerERP_Core_Helper_Data
{
    /**
     * Item virtual in ERP
     *
     * @const string
     */
    const ERP_TYPE_VIRTUAL = 'N';

    /**
     * Item download in ERP
     *
     * @const string
     */
    const ERP_TYPE_DOWNLOAD = 'D';

    /**
     * General prefix for using in logs and events
     *
     * @var string
     */
    protected $prefix = 'product';

    /**
     * Label filters
     *
     * @var array
     */
    protected $labelFilters = array(
        'Sim' => 'Yes',
        'Não' => 'No'
    );

    /**
     * Get type id product by ERP object
     *
     * @param object $product
     * @return string
     */
    public function getTypeId($product)
    {
        /**
         * Configurable Product
         */
        if ($product->itemPai) {
            return Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE;
        }

        /**
         * Download Product
         */
        if ($product->tipoItem == self::ERP_TYPE_VIRTUAL
            && $product->itemVirtual->tipoItemVirtual == self::ERP_TYPE_DOWNLOAD) {
            return Mage_Catalog_Model_Product_Type::TYPE_VIRTUAL;
        }

        /**
         * Virtual Product
         */
        if ($product->tipoItem == self::ERP_TYPE_VIRTUAL) {
            return Mage_Catalog_Model_Product_Type::TYPE_VIRTUAL;
        }

        /**
         * Kit (bundle) Product
         */
        if ($this->isKitProduct($product)) {
            return Mage_Catalog_Model_Product_Type::TYPE_BUNDLE;
        }

        return Mage_Catalog_Model_Product_Type::TYPE_SIMPLE;
    }

    /**
     * Verify whether the product is a child to associate the relative product
     *
     * @param $productERP
     * @return bool
     */
    public function isChild($productERP)
    {
        /**
         * Product is a child
         */
        if (!$productERP->itemPai && $productERP->codigoItemPai && count($productERP->variacoes)) {
            return true;
        }

        return false;
    }

    public function isKitProduct($productERP) {
        if(count($productERP->componentesKit)) {
            return true;
        }
        return false;
    }

    public function hasFatherRegistered($productERP) {
        $productFather = Mage::getModel('catalog/product')->loadByAttribute('sku', $productERP->codigoItemPai);
        if($productFather && $productFather->getId()) {
            return true;
        }
        return false;
    }

    public function hasSonNotRegistered($productERP)
    {
        $kitComponents = $productERP->componentesKit;
        foreach ($kitComponents as $kitComponent) {
            $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $kitComponent->idItemComponente);
            if (!$product || !$product->getId()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get all  attributes ids by variations
     *
     * @param $productERP
     * @return array|bool
     * @throws Mage_Core_Exception
     */
    public function getVariationsId($productERP)
    {
        if (!count($productERP->variacoes)) {
            return false;
        }

        $allIds = array();

        foreach ($productERP->variacoes as $variation) {
            $attributeCode = $this->getAttributeByVariation($variation->idTipoVariacao);

            if(!$attributeCode) {
                Mage::throwException('The product attribute ' . $variation->tipoVariacao . ' is not registered on system "From" -> "To"');
            }

            $attributeModel = Mage::getModel('eav/entity_attribute')->loadByCode('catalog_product', $attributeCode);

            $allIds[$attributeModel->getId()] = $attributeCode;
        }

        return $allIds;
    }

    /**
     * Get attribute by ERP variation, attributes mapping in administrator.
     *
     * @param $variationId
     * @return bool
     */
    public function getAttributeByVariation($variationId)
    {
        $attributes = (array) unserialize(Mage::getStoreConfig('bsellererp_variation/mapping/variations'));

        if (!count($attributes)) {
            return false;
        }

        foreach ($attributes as $attribute) {
            if ($attribute['variations_erp'] != $variationId) {
                continue;
            }

            return $attribute['variations'];
        }

        return false;
    }

    /**
     * Returns type product instance
     *
     * @param string $type
     * @return mixed
     * @throws Mage_Core_Exception
     */
    public function getTypeInstance($type)
    {
        $storeConfigModel = 'bsellererp_product/type_' . $type;

        if (!$model = Mage::getSingleton($storeConfigModel)) {
            $this->log('Integrator class not found: ' . $storeConfigModel);
        }

        return $model;
    }

    /**
     * Label filter
     *
     * @param  string $label
     * @return string
     */
    public function labelFilter($label)
    {
        if (array_key_exists($label, $this->labelFilters)) {
            return $this->labelFilters[$label];
        }

        return $label;
    }

    public function getProductIdByAttribute($attributeName, $value)
    {
        $adapter = Mage::getSingleton('core/resource')->getConnection('core_read');
        $attribute = Mage::getSingleton('eav/config')->getCollectionAttribute(Mage_Catalog_Model_Product::ENTITY, $attributeName);
        $attributeId = $attribute->getId();
        $backendTable = $attribute->getBackendTable();
        $select = $adapter->select()
            ->from($backendTable, 'entity_id')
            ->where("value = '{$value}' and attribute_id = {$attributeId} and entity_type_id = 4");
        return $adapter->fetchOne($select);
    }
}