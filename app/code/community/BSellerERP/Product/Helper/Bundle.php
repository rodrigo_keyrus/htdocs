<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Product
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Configurable product module helper, associate children in parent product.
 * Class developed by Jreinke.
 *
 * @author     Jreinke (https://github.com/jreinke/magento-improve-api)
 */
class BSellerERP_Product_Helper_Grouped extends BSellerERP_Core_Helper_Data
{

}
