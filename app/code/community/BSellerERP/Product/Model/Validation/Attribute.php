<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Product
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Validation value attribute
 *
 * @category   BSellerERP
 * @package    BSellerERP_Product
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Product_Model_Validation_Attribute
{
    /**
     * Validate attributes type
     *
     * @param  string $attributeType
     * @param  string $value
     * @return bool
     */
    public function validate($attributeType, $value)
    {
        switch ($attributeType) {
            case 'date':
                $validator = new Zend_Validate_Date(array('format' => 'd/m/Y'));

                if (!$validator->isValid($value)) {
                    return false;
                }

                break;
        }

        return true;
    }
}
