<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Product
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Initialize importing products in application
 *
 * @category   BSellerERP
 * @package    BSellerERP_Product
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Product_Model_Import extends BSellerERP_Product_Model_Schema
{
    /**
     * Content returned integration to import
     * @var object
     */
    protected $products;

    /**
     * Init importing categories
     */
    public function init()
    {
        Mage::dispatchEvent($this->helper->getPrefix() . '_import_init', array('products' => $this->products));

        $this->helper->log('Numers of products: ' . count($this->products));

        foreach ($this->products as $product) {
            if (!$this->validate($product)) {
                $this->helper->log('Item id not valid: ' . $product->codigoTerceiro);
                continue;
            }

            try {
                $this->createOrUpdate($product);
            } catch (Exception $ex) {
                $this->helper->log('Error at product (sap code): ' . $product->codigoTerceiro . ' (' . $ex->getMessage() . ')', null, BSellerERP_Log_Model_Item_Attribute_Source_Status::ERROR, json_encode($product));
            }
        }

        Mage::dispatchEvent($this->helper->getPrefix() . '_import_complete', array('products' => $this->products));

        return $this;
    }

    /**
     * Set data categories
     *
     * @param $products
     * @return $this
     */
    public function setProducts($products)
    {
        $arrayFathers = array();
        $arraySons = array();

        foreach($products as $key => $product) {
            if($product->itemPai) {
                array_push($arrayFathers, $product);
            } else {
                array_push($arraySons, $product);
            }
        }

        $this->products = array_merge($arrayFathers, $arraySons);

        return $this;
    }

    /**
     * Create/Update by response ERP
     *
     * @param $productERP
     * @return $this
     * @throws Exception
     */
    protected function createOrUpdate($productERP)
    {
        $this->productERP     = $productERP;
        $idProduct            = Mage::getModel('catalog/product')->getIdBySku($this->getSku());
        $this->currentProduct = Mage::getModel('catalog/product')->load($idProduct);

        if (!$this->currentProduct->getId()) {
            $this->currentProduct = Mage::getModel('catalog/product');
        }

        Mage::dispatchEvent($this->helper->getPrefix() . '_item_save_before', array('product' => $this->currentProduct));

        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

        $isChild = $this->helper->isChild($this->productERP);

        if($isChild && !$this->helper->hasFatherRegistered($this->productERP)) {
            //the child has no father
            $this->helper->log('The child product ' . $this->productERP->codigoTerceiro . ' has no father registered. Trying to import the father product: ' . $this->productERP->codigoItemPai);
            //try to create the father with an unique product webserver request
            $result = $this->importUnique($this->productERP->codigoItemPai);
            if(!$result) {
                //if could not register the father, then don't register the son
                return $this;
            }
        }

        // IMPORTING KIT PRODUCTS
        $isKitProduct = $this->helper->isKitProduct($this->productERP);
        if ($isKitProduct && $this->helper->hasSonNotRegistered($this->productERP)) {
            $this->helper->log('The KIT product ' . $this->productERP->codigoTerceiro . ' has not all sons registered. Trying to import them.');
            $this->importMultiple($this->productERP);
        }

        /**
         * Adding data as schema
         */
        $this->currentProduct->addData($this->prepareSchema());

        $this->currentProduct->save();

        $this->helper->log(($this->currentProduct->getId() ? 'Product updated: ' : 'Product created:') . '(Magento SKU: ' . $this->currentProduct->getSku() . ' Cód. SAP: ' . $this->productERP->codigoTerceiro . ')', null, BSellerERP_Log_Model_Item_Attribute_Source_Status::SUCCESS, json_encode($productERP));

        Mage::dispatchEvent($this->helper->getPrefix() . '_item_save_after', array('product' => $this->currentProduct));

        /**
         * Configurable products
         */
        if ($isChild) {
            $this->helper->log('Start association children');

            try {
                $this->associateChildren();
            } catch (Exception $e) {
                $this->helper->log($e->getMessage());
            }
        }

        /**
         * Grouped products
         */
        $isGroupedProduct = false;
        if($isGroupedProduct) {
            $this->helper->log('Start assoiating grouped children');

            try {
                $this->associateGroupedChildren();
            } catch (Exception $e) {
                $this->helper->log($e->getMessage());
            }
        }

        /**
         * Bundle (KIT) products
         */

        if($isKitProduct) {
            $this->helper->log('Start grouping bundle children');

            try {
                $this->associateBundleChildren();
            } catch (Exception $e) {
                $this->helper->log($e->getMessage());
            }
        }

        return $this;
    }

    /**
     * Validate product for create in application
     *
     * @param object $product
     * @return bool
     */
    protected function validate($product)
    {
        $requiredParams = array(
            'codigoItem', 'codigoTerceiro'
        );

        foreach ($requiredParams as $param) {
            if (empty($product->$param)) {
                $this->helper->log('Product is not valid for importing, missing param: ' . $param);
                $this->helper->log($product);

                return false;
            }
        }

        return true;
    }

    protected function importUnique($productSku) {
        Mage::getModel('bsellererp_product/integrator_unique')
            ->setProductSku($productSku)
            ->startIntegratorAllStores();

        $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $productSku);
        if($product && $product->getId()) {
            return true;
        }

        return false;
    }

    protected function importMultiple($productErp) {
        foreach($productErp->componentesKit as $item) {
            Mage::getModel('bsellererp_product/integrator_unique')
                ->setProductSku($item->idItemComponente)
                ->startIntegratorAllStores();
        }
    }
}