<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Product
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Integrator product model
 *
 * @category   BSellerERP
 * @package    BSellerERP_Product
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Product_Model_Integrator_Unique extends BSellerERP_Core_Model_Integrator_Rest
{
    /**
     * Service path api
     * 
     * @var string
     */
    public $servicePath = 'itens';

    /**
     * Load helper data
     *
     * @var string
     */
    public $currentHelper = 'product';

    /**
     * Query Params
     *
     * @var array
     */
    public $queryParams = array(
        'tipoInterface' => 'SITE'
    );

    /**
     * Flag integration as success
     *
     * @return $this
     * @throws Mage_Core_Exception
     */
    protected function success()
    {
        $body = Mage::helper('core')->jsonDecode($this->response->getBody(), Zend_Json::TYPE_OBJECT);

        if (!$body) {
            $this->helper->__('Body response not exist data for importing');

            return $this;
        }

        /**
         * Init importation
         */
        $import = Mage::getModel('bsellererp_product/import');
        $import->setProducts(array($body));
        $import->init();

        $this->helper->log('Importing success');

        return $this;
    }

    /**
     * Flag integration as failed
     *
     * @return $this
     */
    protected function failed()
    {
        $this->helper->log($this->response->getBody());

        return $this;
    }

    public function setProductSku($productSku)
    {
        $this->pathParams[] = $productSku;
        return $this;
    }
}
