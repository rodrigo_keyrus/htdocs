<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Product
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Configurable product type implementation
 *
 * This type builds in product attributes and simple products
 *
 * @category   BSellerERP
 * @package    BSellerERP_Product
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Product_Model_Type_Grouped extends Varien_Object
{
    /**
     * Parent product
     *
     * @var Mage_Catalog_Model_Product
     */
    public $parent;

    /**
     * Current Product Child
     *
     * @var Mage_Catalog_Model_Product
     */
    public $childs;

    /**
     * Product ERp
     *
     * @var object
     */
    public $productERP;

    /**
     * Product Helper
     *
     * @var BSellerERP_Product_Helper_Data
     */
    public $helper;

    public function __construct()
    {
        parent::__construct();

        $this->helper = Mage::helper('bsellererp_product');
    }

    /**
     * Child relationship starting product
     *
     * @return $this
     * @throws Mage_Core_Exception
     */
    public function init()
    {
        try {
            $relation_data = array();
            foreach($this->childs as $child){
                $temp = Mage::getModel('catalog/product');
                $temp_id = $temp->getIdBySku($child->idItemComponente);
                $relation_data[$temp_id] = array('qty' => $child->quantidade,
                    'position' => 0
                );
            }

            $this->parent->setGroupedLinkData($relation_data);
            $this->parent->save();
        } catch (Exception $e) {
            $this->helper->log($e->getMessage());
            Mage::throwException($e->getMessage());
        }

        return $this;
    }

    /**
     * Set parent product
     *
     * @param $parent
     * @return $this
     */
    public function setParent($parent)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Set child product
     *
     * @param $child
     * @return $this
     */
    public function setChilds($childs)
    {
        $this->childs = $childs;

        return $this;
    }

    /**
     * Set product object ERP
     *
     * @param $productERP
     * @return $this
     */
    public function setProductERP($productERP)
    {
        $this->productERP = $productERP;

        return $this;
    }
}
