<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Tracking
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/* @var $installer BSellerERP_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

/**
 * Insert new status and assign for state
 */

$shipping = array(
    'status' => 'shipping',
    'label'  => 'Pedido em Transporte',
    'state'  => 'processing'
);

/**
 * @var Mage_Sales_Model_Order_Status $status
 */
$status = Mage::getModel('sales/order_status');

/**
 * Add status
 */
$status->addData(
    array(
        'status' => $shipping['status'],
        'label'  => $shipping['label']
    )
);

$status->save();

/**
 * Assign status for the state
 */
$status->setStatus($shipping['status'])
    ->assignState($shipping['state'])
    ->save();

$installer->endSetup();