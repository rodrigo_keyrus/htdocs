<?php

/**
 * Created by PhpStorm.
 * User: julio
 * Date: 22/11/16
 * Time: 11:19
 */
class BSellerERP_Credit_Model_Observer
{
    /**
     * @var BSellerERP_Order_Helper_Data
     */
    protected $helper;

    /**
     * Load helper model
     */
    public function __construct()
    {
        $this->helper = Mage::helper('bsellererp_order');
    }

    public function insertAdminBlock(Varien_Event_Observer $observer)
    {
        $giftOptionsBlock = $observer->getBlock();
        if ($giftOptionsBlock->getNameInLayout() !== 'order_info') {
            return;
        }

        $giftOptionsHtml = $observer->getTransport()->getHtml();

        $order = Mage::registry('current_order');

        if(!$order) {
            return;
        }

        $show = (Mage::getStoreConfigFlag('bsellererp_credit/settings/active', $order->getStoreId()) && Mage::getStoreConfigFlag('bsellererp_credit/settings/show_on_salesview', $order->getStoreId()));
        if(!$show) {
            return;
        }

        $customer = Mage::getModel('customer/customer')->load($order->getCustomerId());

        $cpfCnpj = '';
        if ($customer->getCpf()) {
            $cpfCnpj = trim($customer->getCpf());
        } elseif ($customer->getCnpj()) {
            $cpfCnpj = trim($customer->getCnpj());
        }

        $customerCreditApiResult = Mage::helper('bsellererp_credit')->getCreditLimit($cpfCnpj);
        $customerCredit = null;
        if ($customerCreditApiResult && is_array($customerCreditApiResult)) {
            if (isset($customerCreditApiResult['limit_available'])) {
                $customerCredit = $customerCreditApiResult['limit_available'];
            }
        }

        $html = "<div class=\"entry-edit\">
    <div class=\"entry-edit-head\">
        <h4>Informações de crédito de cliente</h4>
    </div>
    <fieldset>
        <tr>
            <td class=\"label\"><label><strong>Limite disponível:</strong></label></td>
            <td class=\"value\">" . (($customerCredit || $customerCredit == 0) ? Mage::helper('core')->currency($customerCredit, true, false) : 'Não foi possível consultar') . "</td>
        </tr>
    </fieldset>
</div>";
        $observer->getTransport()->setHtml($giftOptionsHtml . $html);
    }
}