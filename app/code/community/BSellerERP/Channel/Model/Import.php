<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Channel
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Initialize importing channels in application
 *
 * @category   BSellerERP
 * @package    BSellerERP_Channel
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Channel_Model_Import extends BSellerERP_Channel_Model_Schema
{
    /**
     * Content returned integration to import
     * @var object
     */
    protected $channels;

    /**
     * Init importing channels
     */
    public function init()
    {
        Mage::dispatchEvent($this->helper->getPrefix() . '_import_init', array('channels' => $this->channels));

        foreach ($this->channels as $channel) {
            if (!$this->validate($channel)) {
                continue;
            }

            $this->create($channel);
        }

        Mage::dispatchEvent($this->helper->getPrefix() . '_import_complete', array('channels' => $this->channels));

        return $this;
    }

    /**
     * Set data channels
     *
     * @param $channels
     * @return $this
     */
    public function setChannels($channels)
    {
        $this->channels = $channels;

        return $this;
    }

    /**
     * Create channel by response ERP
     *
     * @param $channelERP
     * @return $this
     * @throws Exception
     */
    protected function create($channelERP)
    {
        $this->channelERP     = $channelERP;
        $this->currentChannel = Mage::getModel('bsellererp_channel/items');

        /**
         * Adding data as scheme
         */
        $this->currentChannel->addData($this->prepareSchema());

        Mage::dispatchEvent($this->helper->getPrefix() . '_item_save_before', array('channel' => $this->currentChannel));
        $this->currentChannel->save();
        Mage::dispatchEvent($this->helper->getPrefix() . '_item_save_after', array('channel' => $this->currentChannel));

        return $this;
    }

    /**
     * Validate channel for create in application
     *
     * @param object $product
     * @return bool
     */
    protected function validate($product)
    {
        $requiredParams = array('id', 'nome');

        foreach ($requiredParams as $param) {
            if ($product->$param === false) {
                $this->helper->log('Channel is not valid for importing, missing param: ' . $param);
                $this->helper->log($product);

                return false;
            }
        }

        return true;
    }
}
