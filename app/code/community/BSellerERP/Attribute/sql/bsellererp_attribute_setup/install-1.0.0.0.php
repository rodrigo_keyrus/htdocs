<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Attribute
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

/* @var $installer BSellerERP_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

/**
 * Create 'Catalog Attribute' table
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('bsellererp_attribute/attribute'))
    ->addColumn(
        'entity_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
            'primary'  => true
        ),
        'Entity ID'
    )
    ->addColumn(
        'attribute_id',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        30,
        array(
            'unsigned' => true,
            'nullable' => false,
        ),
        'Attribute ID'
    )
    ->addColumn(
        'attribute_name',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        255,
        array(
            'unsigned' => true,
            'nullable' => false,
        ),
        'Attribute Name'
    )
    ->setComment('BSeller ERP - Catalog Attribute Integration');

$installer->getConnection()->createTable($table);

/**
 * Create 'Catalog Group' table
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('bsellererp_attribute/group'))
    ->addColumn(
        'entity_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
            'primary'  => true
        ),
        'Entity ID'
    )
    ->addColumn(
        'group_id',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        30,
        array(
            'unsigned' => true,
            'nullable' => false,
        ),
        'Group ID'
    )
    ->addColumn(
        'group_name',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        255,
        array(
            'unsigned' => true,
            'nullable' => false,
        ),
        'Group Name'
    )
    ->setComment('BSeller ERP - Catalog Attribute Group Integration');

$installer->getConnection()->createTable($table);

$installer->endSetup();
