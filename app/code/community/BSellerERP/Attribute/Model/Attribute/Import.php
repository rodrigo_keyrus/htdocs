<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Attribute
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

/**
 * Initialize importing attributes in application
 *
 * @category   BSellerERP
 * @package    BSellerERP_Attribute
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 * @author     Alan Carvalho <alan.carvalho@e-smart.com.br>
 */
class BSellerERP_Attribute_Model_Attribute_Import extends BSellerERP_Attribute_Model_Attribute_Schema
{
    /**
     * Content returned integration to import
     *
     * @var object
     */
    protected $responseObject;

    /**
     * Set response object data
     *
     * @param object $responseObject
     * @return $this
     */
    public function setResponse($responseObject)
    {
        $this->responseObject = $responseObject;

        return $this;
    }

    /**
     * Init importing data
     */
    public function init()
    {
        Mage::dispatchEvent($this->helper->getPrefix() . '_import_init', array('attributes' => $this->responseObject));

        foreach ($this->responseObject as $responseObjectItem) {
            if (!$this->validate($responseObjectItem)) {
                continue;
            }

            $this->create($responseObjectItem);
        }

        Mage::dispatchEvent($this->helper->getPrefix() . '_import_complete', array('attributes' => $this->responseObject));

        return $this;
    }

    /**
     * Create attribute by response item
     *
     * @param object $responseObjectItem
     * @return $this
     * @throws Exception
     */
    protected function create($responseObjectItem)
    {
        $this->responseObjectItem = $responseObjectItem;
        $this->objectModel        = Mage::getModel('bsellererp_attribute/attribute');

        /**
         * Adding data as scheme
         */
        $this->objectModel->addData($this->prepareSchema());

        Mage::dispatchEvent($this->helper->getPrefix() . '_item_save_before', array('attribute' => $this->objectModel));

        $this->objectModel->save();

        Mage::dispatchEvent($this->helper->getPrefix() . '_item_save_after', array('attribute' => $this->objectModel));

        return $this;
    }

    /**
     * Validate variation for create in application
     *
     * @param object $responseObjectItem
     * @return bool
     */
    protected function validate($responseObjectItem)
    {
        $requiredParams = array('id', 'descricao');

        foreach ($requiredParams as $param) {
            if ($responseObjectItem->$param === false) {
                $this->helper->log('Variation is not valid for importing, missing param: ' . $param);
                $this->helper->log($responseObjectItem);

                return false;
            }
        }

        return true;
    }
}
