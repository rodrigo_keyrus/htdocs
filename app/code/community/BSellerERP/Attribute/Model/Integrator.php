<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Attribute
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

/**
 * Integrator rest
 *
 * @category   BSellerERP
 * @package    BSellerERP_Attribute
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 * @author     Alan Carvalho <alan.carvalho@e-smart.com.br>
 */
class BSellerERP_Attribute_Model_Integrator extends BSellerERP_Core_Model_Integrator_Rest
{
    /**
     * Service path api
     *
     * @var string
     */
    public $servicePath = '/itens/tipos-ficha-tecnica';

    /**
     * Load helper data
     *
     * @var string
     */
    public $currentHelper = 'attribute';

    /**
     * Flag integration as success
     *
     * @return $this
     * @throws Mage_Core_Exception
     */
    protected function success()
    {
        $response = Mage::helper('core')->jsonDecode($this->response->getBody(), Zend_Json::TYPE_OBJECT);

        if (!$response || !count($response)) {
            Mage::throwException($this->helper->__('Body response not exist data for importing'));
        }

        $this->onSuccessAttribute($response);
        $this->onSuccessGroup($response);

        return $this;
    }

    /**
     * Flag integration as failed
     *
     * @return $this
     */
    protected function failed()
    {
        $this->helper->log($this->response->getBody());

        return $this;
    }

    /**
     * Add data in attribute table
     *
     * @param object $response
     * @return $this
     */
    protected function onSuccessAttribute($response)
    {
        Mage::getResourceModel('bsellererp_attribute/attribute')
            ->truncate();

        Mage::getModel('bsellererp_attribute/attribute_import')
            ->setResponse($response)
            ->init();

        return $this;
    }

    /**
     * Add data in group table
     *
     * @param object $response
     * @return $this
     */
    protected function onSuccessGroup($response)
    {
        Mage::getResourceModel('bsellererp_attribute/group')
            ->truncate();

        Mage::getModel('bsellererp_attribute/group_import')
            ->setResponse($response)
            ->init();

        return $this;
    }
}
