<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Attribute
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSellerERP_Attribute_Block_Adminhtml_Form_Field_Group
    extends BSellerERP_Core_Block_Adminhtml_Form_Field_Abstract
{
    /**
     * Column attribute groups ERP
     *
     * @const string
     */
    const COLUMN_CODE_CUSTOM = 'groups_erp';

    /**
     * Column attribute groups application
     *
     * @const string
     */
    const COLUMN_CODE_APP = 'groups';

    /**
     * Renderer types
     *
     * @var array
     */
    protected $renderer = array();

    /**
     * Prepare to render
     *
     * @return void
     */
    protected function _prepareToRender()
    {
        $helper = $this->helper('bsellererp_attribute');

        $this->addColumn(
            self::COLUMN_CODE_CUSTOM,
            array(
                'label'    => $helper->__('ERP Attribute Groups'),
                'class'    => 'input-text required-entry',
                'renderer' => $this->getRenderer(self::COLUMN_CODE_CUSTOM)
            )
        );

        $this->addColumn(
            self::COLUMN_CODE_APP,
            array(
                'label'    => $helper->__('Magento Attribute Groups'),
                'class'    => 'input-text required-entry',
                'renderer' => $this->getRenderer(self::COLUMN_CODE_APP)
            )
        );

        $this->_addAfter = false;
        $this->_addButtonLabel = $helper->__('Add new mapping');
    }

    /**
     * Return renderer by type
     *
     * @param string $column
     * @return array
     */
    protected function getRenderer($column)
    {
        if (isset($this->renderer[$column])) {
            return $this->renderer[$column];
        }

        $options = array();

        if ($column === self::COLUMN_CODE_CUSTOM) {
            $options = $this->getCustomGroupOptions();
        }

        if ($column === self::COLUMN_CODE_APP) {
            $options = $this->getGroupOptions();
        }

        $this->renderer[$column] = $this->getLayout()
            ->createBlock('core/html_select')
            ->setIsRenderToJsTemplate(true)
            ->setOptions($options);

        return $this->renderer[$column];
    }

    /**
     * Prepare row and set option selected
     *
     * @param Varien_Object $row
     * @return void
     */
    protected function _prepareArrayRow(Varien_Object $row)
    {
        $row->setData(
            'option_extra_attr_' . $this->getRenderer(self::COLUMN_CODE_CUSTOM)
                ->calcOptionHash($row->getData(self::COLUMN_CODE_CUSTOM)),
            'selected="selected"'
        );

        $row->setData(
            'option_extra_attr_' . $this->getRenderer(self::COLUMN_CODE_APP)
                ->calcOptionHash($row->getData(self::COLUMN_CODE_APP)),
            'selected="selected"'
        );
    }

    /**
     * Return groups options in array
     *
     * @return array
     */
    protected function getGroupOptions()
    {
        $options[] = array(
            'label' => $this->helper('bsellererp_attribute')->__('-- Please Select --'),
            'value' => ''
        );

        $typeId = Mage::getModel('catalog/product')
            ->getResource()
            ->getTypeId();

        /** @var Mage_Eav_Model_Resource_Entity_Attribute_Set_Collection $collection */
        $collection = Mage::getResourceSingleton('eav/entity_attribute_set_collection')
            ->setEntityTypeFilter($typeId);

        /** @var Mage_Eav_Model_Entity_Attribute_Set $object */
        foreach ($collection as $object) {
            $options[$object->getData('attribute_set_id')] = $object->getAttributeSetName();
        }

        return $options;
    }

    /**
     * Return custom group options in array
     *
     * @return array
     */
    protected function getCustomGroupOptions()
    {
        $options[] = array(
            'label' => $this->helper('bsellererp_attribute')->__('-- Please Select --'),
            'value' => ''
        );

        $collection = Mage::getResourceSingleton('bsellererp_attribute/group_collection');

        /** @var BSellerERP_Attribute_Model_Group $object */
        foreach ($collection as $object) {
            $options[$object->getData('group_id')] = $object->getData('group_name');
        }

        return $options;
    }
}
