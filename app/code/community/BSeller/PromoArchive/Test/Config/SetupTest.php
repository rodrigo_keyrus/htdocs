<?php
/**
 * BSeller Platform | B2W - Companhia Digital
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category  BSeller
 * @package   BSeller_PromoArchive
 *
 * @copyright Copyright (c) 2017 B2W Digital - BSeller Platform. (http://www.bseller.com.br)
 *
 * @author   Weverson Cachinsky <weverson.cachinsky@e-smart.com.br>
 */
class BSeller_PromoArchive_Test_Config_SetupTest extends EcomDev_PHPUnit_Test_Case_Config
{
    public function testIsArchivedColumnIsAddedToSalesruleTable()
    {
        $resource = Mage::getModel('core/resource');
        $table = $resource->getTableName('salesrule/rule');

        $this->assertTrue(
            $resource->getConnection('core/read')->tableColumnExists($table, 'is_archived')
        );
    }
}
