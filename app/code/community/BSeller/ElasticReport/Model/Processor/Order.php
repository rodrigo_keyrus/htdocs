<?php

class BSeller_ElasticReport_Model_Processor_Order extends BSeller_ElasticReport_Model_Processor
{

    use BSeller_ElasticReport_Trait_Resource_Query;
    
    
    const DATA_TYPE_INTEGER = 'integer';
    const DATA_TYPE_DATE    = 'date';
    const DATA_TYPE_TEXT    = 'text';
    const DATA_TYPE_FLOAT   = 'float';


    protected $_initialized = false;


    /**
     * @param array $orderIds
     *
     * @return array
     */
    public function updateOrdersByIds($orderIds = [])
    {
        $orderIds = $this->filterOrderIds((array) $orderIds);

        if (empty($orderIds)) {
            return [];
        }

        /** @var Mage_Sales_Model_Resource_Order_Collection $collection */
        $collection = Mage::getResourceModel('sales/order_collection');
        $collection->addFieldToFilter('entity_id', ['in' => (array) $orderIds]);

        return $this->updateOrdersByCollection($collection);
    }


    /**
     * @param Mage_Sales_Model_Resource_Order_Collection $collection
     *
     * @return array
     */
    public function updateOrdersByCollection(Mage_Sales_Model_Resource_Order_Collection $collection)
    {
        $orders   = $this->fetchCollection($collection);
        $orderIds = [
            'error'   => [],
            'success' => []
        ];

        /** @var array $order */
        foreach ($orders as $order) {
            $result = $this->updateOrder((array) $order);

            if (!$result) {
                $orderIds['error'][] = $order['entity_id'];
                continue;
            }

            $orderIds['success'][] = $order['entity_id'];
        }

        return (array) $orderIds;
    }



    /**
     * @param Mage_Sales_Model_Order $order
     *
     * @return bool|BSeller_ElasticReport_Model_Processor_Order
     */
    public function updateOrderByObject(Mage_Sales_Model_Order $order)
    {
        $payment   = $order->getPayment()->toArray();
        $addresses = $order->getAddressesCollection()->toArray();
        $items     = $order->getItemsCollection()->toArray();

        $order->addData([
            '_payments'  => [$payment],
            '_addresses' => $addresses['items'],
            '_items'     => $items['items'],
        ]);

        return $this->updateOrder($order->getData());
    }


    /**
     * @param array $orderData
     *
     * @return $this|bool
     */
    public function updateOrder(array $orderData)
    {
        $orderId = (int) $this->extractIndex('increment_id', $orderData);

        if (!$orderId) {
            return false;
        }
        
        $createdAt = $orderData['created_at'];
        
        /** @var Mage_Core_Model_Date $date */
        $date     = Mage::getSingleton('core/date');
        $suffix   = $date->date('Y.m.d', strtotime($createdAt));
        
        $index    = $this->getOrdersIndexName($suffix);
        $template = $this->getOrdersIndexName($suffix);
        $type     = $this->getOrdersIndexTypeName();

//        if (!$this->_initialized) {
        $this->init($index, $template, $type, $this->getIndexTemplateOrderMapping());
//            $this->_initialized = true;
//        }

        /** @var BSeller_ElasticReport_Model_Request_Body_Sales_Order $requestBody */
        $requestBody = Mage::getModel('bseller_elasticreport/request_body_sales_order');
        $data        = $requestBody->prepareData($orderData);

        if (false === $data) {
            return false;
        }

        if (empty($data)) {
            return false;
        }

        $result = $this->getElasticSearchApi()->documentCreate($index, $type, $orderId, $data);

        return $result;
    }


    /**
     * @return array
     */
    protected function getIndexTemplateOrderMapping()
    {
        $mapping = [
            'entity_id' => [
                'type' => self::DATA_TYPE_INTEGER
            ],
            'increment_id' => [
                'type'  => self::DATA_TYPE_TEXT
//                'index' => 'not_analyzed',
            ],
            'store_id' => [
                'type' => self::DATA_TYPE_INTEGER
            ],
            'store_code' => [
                'type' => self::DATA_TYPE_TEXT
            ],
            'updated_at' => [
                'type'   => self::DATA_TYPE_DATE,
                'format' => 'yyyy-MM-dd HH:mm:ss',
            ],
            'created_at' => [
                'type'   => self::DATA_TYPE_DATE,
                'format' => 'yyyy-MM-dd HH:mm:ss',
            ],
            'event_datetime' => [
                'type'   => self::DATA_TYPE_DATE,
                'format' => 'yyyy-MM-dd HH:mm:ss',
            ],
            'subtotal' => [
                'type'   => self::DATA_TYPE_FLOAT,
            ],
            'grand_total' => [
                'type'   => self::DATA_TYPE_FLOAT,
            ],
            'total_invoiced' => [
                'type'   => self::DATA_TYPE_FLOAT,
            ],
            'total_paid' => [
                'type'   => self::DATA_TYPE_FLOAT,
            ],
            'shipping_amount' => [
                'type'   => self::DATA_TYPE_FLOAT,
            ],
            'shipping_method' => [
                'type'   => self::DATA_TYPE_TEXT,
            ],
            'remote_ip' => [
                'type'   => self::DATA_TYPE_TEXT,
            ],
            'customer' => [
                'properties' => [
                    'id' => [
                        'type' => self::DATA_TYPE_INTEGER,
                    ],
                    'email' => [
                        'type' => self::DATA_TYPE_TEXT,
                    ],
                    'firstname' => [
                        'type' => self::DATA_TYPE_TEXT,
                    ],
                    'lastname' => [
                        'type' => self::DATA_TYPE_TEXT,
                    ],
                    'taxvat' => [
                        'type' => self::DATA_TYPE_TEXT,
                    ],
                ]
            ],
            'payment' => [
                'properties' => [
                    'method' => [
                        'type' => self::DATA_TYPE_TEXT,
                    ],
                ]
            ],
            'items' => [
                'properties' => [
                    'id' => [
                        'type' => self::DATA_TYPE_INTEGER,
                    ],
                    'parent_item_id' => [
                        'type' => self::DATA_TYPE_INTEGER,
                    ],
                    'product_name' => [
                        'type'  => self::DATA_TYPE_TEXT
//                        'index' => 'not_analyzed',
                    ],
                    'product_id' => [
                        'type' => self::DATA_TYPE_INTEGER,
                    ],
                    'product_sku' => [
                        'type' => self::DATA_TYPE_TEXT
//                        'index' => 'not_analyzed',
                    ],
                    'product_type' => [
                        'type' => self::DATA_TYPE_TEXT,
                    ],
                ]
            ]
        ];

        return $mapping;
    }

}
