<?php

class BSeller_ElasticReport_Model_Cron
{

    use BSeller_ElasticReport_Trait_Data,
        BSeller_ElasticReport_Trait_Config;


    /**
     * @var Mage_Cron_Model_Schedule $schedule
     */
    public function collectOrdersToQueue(Mage_Cron_Model_Schedule $schedule)
    {
        if (!$this->isEnabled()) {
            $schedule->setMessages($this->__('Module disabled.'));
            return;
        }
        
        if (!$this->isCronEnabled()) {
            $schedule->setMessages($this->__('Cron is disabled.'));
            return;
        }
        
        $limit = (int) $schedule->getData('limit');
        
        if (empty($limit)) {
            $limit = (int) $this->getMaximumOrdersToProcessQty();
        }

        try {
            /** @var array|bool $orderIds */
            $order    = 'created_at DESC';
            $orderIds = $this->getOrdersQueueResource()->collectOrdersToQueue($limit, null, $order);
        } catch (Exception $e) {
            $schedule->setMessages($e->getMessage());
            return;
        }

        if (empty($orderIds)) {
            $schedule->setMessages($this->__('No orders queued.'));
            return;
        }

        $schedule->setMessages($this->__('Queued Order IDs: %s.', implode(',', (array) $orderIds)));
    }


    /**
     * @param Mage_Cron_Model_Schedule $schedule
     */
    public function processOrderQueue(Mage_Cron_Model_Schedule $schedule)
    {
        if (!$this->isEnabled()) {
            $schedule->setMessages($this->__('Module disabled.'));
            return;
        }
    
        if (!$this->isCronEnabled()) {
            $schedule->setMessages($this->__('Cron is disabled.'));
            return;
        }
    
        if (!$this->getElasticSearchApi()->hasConnection()) {
            $schedule->setMessages($this->__('Connection with ElasticSearch is unavailable in the moment.'));
            return;
        }
    
        $limit = (int) $schedule->getData('limit');
    
        if (empty($limit)) {
            $limit = (int) $this->getMaximumOrdersToProcessQty();
        }
    
        try {
            $allIds = (array) $this->getOrdersQueueResource()->getNonProcessedOrderIds($limit);
        } catch (Exception $e) {
            return;
        }
    
        /**
         * Let's process 500 at time.
         */
        $sets    = array_chunk($allIds, 500);
        $message = '';
        $result  = [
            'error'   => [],
            'success' => []
        ];
        
        foreach ($sets as $orderIds) {
            try {
                $_result = (array) $this->getOrderProcessor()->updateOrdersByIds($orderIds);

                $result['success'] = array_merge($result['success'], $_result['success']);
                $result['error']   = array_merge($result['error'],   $_result['error']);
            } catch (Exception $e) {
                $message .= $e->getMessage();
                continue;
            }
        }
    
        if ($message) {
            $schedule->setMessages($message);
            return;
        }

        if (empty($result['success']) && empty($result['error'])) {
            $schedule->setMessages($this->__('No orders processed.'));
            return;
        }

        $messagePattern  = null;
        $succeededOrders = [];

        if (isset($result['success']) && !empty($result['success'])) {
            $succeededOrders = (array) $result['success'];
            $this->getOrdersQueueResource()->setOrdersAsProcessed($succeededOrders);

            $messagePattern  .= 'Processed Order IDs: %s.';
            $succeededOrders  = implode(',', $succeededOrders);
        }

        $errorOrders = [];

        if (isset($result['error']) && !empty($result['error'])) {
            if (!empty($messagePattern)) {
                $messagePattern .= ' ';
            }

            $messagePattern .= 'Errors: %s.';
            $errorOrders     = implode(',', (array) $result['error']);
        }

        $schedule->setMessages($this->__($messagePattern, $succeededOrders, $errorOrders));
    }

}
