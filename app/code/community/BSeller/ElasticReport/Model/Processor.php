<?php
/**
 * BSeller Platform | B2W - Companhia Digital
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category  BSeller
 * @package   BSeller_ElasticReport
 *
 * @copyright Copyright (c) 2017 B2W Digital - BSeller Platform. (http://www.bseller.com.br)
 *
 * @author    Tiago Sampaio <tiago.sampaio@e-smart.com.br>
 */
abstract class BSeller_ElasticReport_Model_Processor
{

    use BSeller_ElasticReport_Trait_Data,
        BSeller_ElasticReport_Trait_Config,
        BSeller_ElasticReport_Trait_Array,
        BSeller_ElasticSearch_Trait_Api_Connection;


    /**
     * @param string $index
     * @param string $type
     * @param array  $indexTemplateMapping
     *
     * @return $this
     */
    protected function init($index, $templateName, $type, array $indexTemplateMapping)
    {
        if (!$this->getElasticSearchApi()->hasConnection()) {
            Mage::throwException($this->getConnectionErrorMessage());
        }
        
        $this->initIndexTemplate($templateName, $type, $indexTemplateMapping);
        $this->initIndex($index);

        return $this;
    }


    /**
     * @param string $index
     * @param string $type
     * @param array  $indexTemplateMapping
     *
     * @return $this
     */
    protected function initIndexTemplate($index, $type, array $indexTemplateMapping)
    {
        /**
         * Checks if the template exists.
         * In negative case creates the required template.
         */
        $templateName = sprintf('%s_%s', $index, $type);
        if (!$this->getElasticSearchApi()->indexTemplateExists($templateName)) {
            $result = $this->getElasticSearchApi()
                ->indexTemplateCreate($index, $templateName, $type, $indexTemplateMapping);
            
            if (false === $result) {
                Mage::throwException($this->__('Unable to create the index template.'));
            }
        }

        return $this;
    }


    /**
     * @param string $index
     *
     * @return $this
     */
    protected function initIndex($index)
    {
        /**
         * Checks if the index exists.
         * In negative case, then create the required index.
         */
        if (!$this->getElasticSearchApi()->indexExists($index)) {
            $result = $this->getElasticSearchApi()->indexCreate($index, 1, 1);
    
            if (false === $result) {
                Mage::throwException($this->__('Unable to create the index.'));
            }
        };

        return $this;
    }

}
