<?php

class BSeller_ElasticReport_Model_Resource_Orders_Queue extends BSeller_Core_Model_Resource_Abstract
{

    use BSeller_ElasticReport_Trait_Array;


    protected function _construct()
    {
        $this->_init('bseller_elasticreport/orders_queue', 'id');
    }


    /**
     * @param int|null $limit
     *
     * @return array|bool
     */
    public function collectOrdersToQueue($limit = 0, $where = null, $order = null)
    {
        /** @var Varien_Db_Select $select */
        $select = $this->getSalesOrderSelect('entity_id');
        
        if (!empty($where)) {
            $select->where($where);
        }
        
        if (!empty($order)) {
            $select->order($order);
        }
        
        $queuedOrderIds = $this->_getReadAdapter()->fetchCol($this->getOrdersQueueSelect()->columns('order_id'));

        if (!empty($queuedOrderIds)) {
            $select->where('entity_id NOT IN (?)', $queuedOrderIds);
        }
    
        /**
         * Apply limitations.
         */
        if ($this->isLimitValid($limit)) {
            $select->limit($limit);
        }

        $orderIds = (array) $this->getReadConnection()->fetchCol($select);

        if (empty($orderIds)) {
            return false;
        }

        $this->queueOrders($orderIds);

        return $orderIds;
    }


    /**
     * @param array $orderIds
     *
     * @return int
     */
    public function queueOrders($orderIds = [])
    {
        $sets = array_chunk((array) $orderIds, 100);
        $qty  = 0;

        foreach ($sets as $orderIds) {
            $data = [];

            foreach ($orderIds as $orderId) {
                $data[] = [
                    'order_id'   => $orderId,
                    'created_at' => date('Y-m-d H:i:s'),
                ];
            }

            if (!empty($data)) {
                $qty =+ $this->_getWriteAdapter()->insertMultiple($this->getMainTable(), $data);
            }
        }

        return $qty;
    }


    /**
     * @return array
     */
    public function getNonProcessedOrderIds($limit = null, $order = null)
    {
        $bind = [
            ':is_processed' => 0
        ];

        /** @var Varien_Db_Select $select */
        $select = $this->getReadConnection()
            ->select()
            ->from($this->getMainTable(), 'order_id')
            ->where('is_processed = :is_processed');
        
        if (!empty($order)) {
            $select->order($order);
        }
    
        /**
         * Apply limitations.
         */
        if ($this->isLimitValid($limit)) {
            $select->limit($limit);
        }

        $orderIds = $this->getReadConnection()->fetchCol($select, $bind);

        $orderIds = array_map(function ($value) {
            return (int) $value;
        }, $orderIds);

        return $orderIds;
    }


    /**
     * @return Varien_Db_Select
     */
    public function getOrdersQueueSelect()
    {
        /** @var Varien_Db_Select $select */
        $select = $this->getReadConnection()
            ->select()
            ->from($this->getMainTable(), 'order_id');

        return $select;
    }


    /**
     * @param array $orderIds
     *
     * @return array
     */
    public function setOrdersAsProcessed($orderIds = [])
    {
        $orderIds = $this->filterOrderIds($orderIds);
        $updated  = [];

        foreach ($orderIds as $orderId) {
            $updated[$orderId] = $this->_getWriteAdapter()
                ->update(
                    $this->getMainTable(),
                    [
                        'is_processed' => 1,
                        'processed_at' => date('Y-m-d H:i:s'),
                        'updated_at'   => date('Y-m-d H:i:s'),
                    ],
                    new Zend_Db_Expr("order_id = {$orderId}")
                );
        }

        return $updated;
    }


    /**
     * @var string|array $columns
     *
     * @return Varien_Db_Select
     */
    protected function getSalesOrderSelect($columns = '*')
    {
        /** @var Varien_Db_Select $select */
        $select = $this->getReadConnection()
            ->select()
            ->from($this->getSalesOrderTableName(), $columns);

        return $select;
    }


    /**
     * @return string
     */
    protected function getSalesOrderTableName()
    {
        return $this->getTable('sales/order');
    }
    
    
    /**
     * @param int $limit
     *
     * @return bool
     */
    protected function isLimitValid($limit)
    {
        if (!is_int($limit)) {
            return false;
        }
        
        if (empty($limit)) {
            return false;
        }
        
        if ($limit <= 0) {
            return false;
        }
        
        return true;
    }

}
