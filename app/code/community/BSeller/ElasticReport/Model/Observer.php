<?php

class BSeller_ElasticReport_Model_Observer
{

    use BSeller_ElasticReport_Trait_Data,
        BSeller_ElasticReport_Trait_Config,
        BSeller_ElasticReport_Trait_Date;


    /** @var array */
    protected $_processedOrders = [];


    /**
     * @param Varien_Event_Observer $observer
     */
    public function updateElasticSearchOrderDocument(Varien_Event_Observer $observer)
    {
        if (!$this->isEnabled()) {
            return;
        }

        /** @var Mage_Sales_Model_Order $order */
        $order = $observer->getEvent()->getData('order');

        if (!$this->validateOrder($order)) {
            return;
        }

        if (isset($this->_processedOrders[$order->getId()])) {
            return;
        }

        try {
            $this->voidDateConversion(true);
            $this->getOrderProcessor()->updateOrderByObject($order);
            $this->_processedOrders[$order->getId()] = true;
        } catch (Exception $e) {
            Mage::logException($e);
        }
    }
    
    
    /**
     * @param Varien_Event_Observer $observer
     */
    public function appendMassactionOptions(Varien_Event_Observer $observer)
    {
        if (!$this->isEnabled()) {
            return;
        }

        /** @var Mage_Adminhtml_Block_Sales_Order_Grid $block */
        $block = $observer->getData('block');
    
        if (!($block instanceof Mage_Adminhtml_Block_Sales_Order_Grid)) {
            return;
        }
        
        /** @var Mage_Adminhtml_Helper_Data $adminhtmlHelper */
        $adminhtmlHelper = Mage::helper('adminhtml');
    
        $block->getMassactionBlock()->addItem('bseller_elasticreport_integrate', array(
            'label' => $this->__('Integrate to ElasticSearch'),
            'url'   => $adminhtmlHelper->getUrl('adminhtml/elasticreport_orders/integrate'),
        ));
    }


    /**
     * @param Mage_Sales_Model_Order $order
     *
     * @return bool
     */
    protected function validateOrder($order)
    {
        if (!$order || !$order->getId()) {
            return false;
        }

        if (!$order->getState() && !$order->getStatus()) {
            return false;
        }

        return true;
    }

}
