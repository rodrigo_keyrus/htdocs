<?php
/**
 * @var Magento_Db_Adapter_Pdo_Mysql               $conn
 * @var BSeller_ElasticReport_Model_Resource_Setup $this
 */
$this->startSetup();

$table = $this->newTable('bseller_elasticreport/orders_queue')
    ->addColumn('order_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, [
        'primary'  => true,
        'nullable' => false,
    ])
    ->addColumn('is_processed', Varien_Db_Ddl_Table::TYPE_SMALLINT, 1, [
        'nullable' => false,
        'default'  => 0,
    ]);
$this->addDatetimeColumn($table, 'processed_at');
$this->addTimestamps($table);

$conn->createTable($table);

/**
 * Create indexes.
 */
$this->addIndexes([
    'order_id',
    'is_processed'
], $table, Varien_Db_Adapter_Interface::INDEX_TYPE_INDEX);

/**
 * Add required foreign keys.
 */
$this->addForeignKey('bseller_elasticreport/orders_queue', 'order_id', 'sales/order', 'entity_id');

$this->endSetup();
