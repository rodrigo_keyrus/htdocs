<?php

trait BSeller_ElasticReport_Trait_Data
{

    use BSeller_Core_Trait_Data;


    /**
     * @return BSeller_ElasticReport_Model_Processor_Order
     */
    protected function getOrderProcessor()
    {
        return Mage::getSingleton('bseller_elasticreport/processor_order');
    }


    /**
     * @return BSeller_ElasticReport_Model_Resource_Orders_Queue
     */
    protected function getOrdersQueueResource()
    {
        return Mage::getResourceModel('bseller_elasticreport/orders_queue');
    }


    /**
     * @return BSeller_ElasticSearch_Model_Api
     */
    protected function getElasticSearchApi()
    {
        return Mage::getSingleton('bseller_elasticsearch/api');
    }

}
