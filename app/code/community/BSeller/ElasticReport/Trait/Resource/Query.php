<?php

trait BSeller_ElasticReport_Trait_Resource_Query
{

    /**
     * @param Mage_Core_Model_Resource_Db_Collection_Abstract $collection
     *
     * @return array
     */
    protected function fetchCollection(Mage_Core_Model_Resource_Db_Collection_Abstract $collection)
    {
        return $this->fetchAll($collection->getSelect());
    }


    /**
     * @param Varien_Db_Select $select
     *
     * @return array
     */
    protected function fetchAll(Varien_Db_Select $select)
    {
        return (array) $this->getReadConnection()->fetchAll($select);
    }


    /**
     * @return Varien_Db_Adapter_Interface|Magento_Db_Adapter_Pdo_Mysql
     */
    protected function getReadConnection()
    {
        return $this->getResource()->getConnection('read');
    }


    /**
     * @return Mage_Core_Model_Resource
     */
    protected function getResource()
    {
        return Mage::getModel('core/resource');
    }

}
