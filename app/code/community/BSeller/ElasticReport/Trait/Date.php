<?php

trait BSeller_ElasticReport_Trait_Date
{

    /**
     * @return Mage_Core_Model_Date|Mage_Core_Model_Abstract
     */
    protected function getDateModel()
    {
        return Mage::getSingleton('core/date');
    }


    /**
     * @param string      $input
     * @param null|string $format
     * @return string
     *
     * @throws Mage_Core_Exception
     */
    protected function date($input, $format = null)
    {
        if (true === $this->voidDateConversion()) {
            return $input;
        }

        return $this->getDateModel()->date($format, $input);
    }


    /**
     * This flag controls the date conversion in this class.
     *
     * When the order is processed by an observer, on save_after event, the date is with the correct timezone already.
     *
     * That's because the Observer process integrates the order object itself and the object is already ...
     * ... with the timezone calculated correctly. If the order data is fetched directly from database it won't.
     *
     * If we apply the conversion here again then the date and time will be wrongly converted.
     *
     * @param bool $flag
     *
     * @return $this|bool
     *
     * @throws Mage_Core_Exception
     */
    protected function voidDateConversion($flag = null)
    {
        $key = 'void_date_conversion';

        if (is_null($flag)) {
            return (bool) Mage::registry($key);
        }

        Mage::register($key, (bool) $flag, true);

        return $this;
    }

}
