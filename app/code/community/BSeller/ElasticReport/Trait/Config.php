<?php

trait BSeller_ElasticReport_Trait_Config
{

    use BSeller_Core_Trait_Config;


    /**
     * @param string $field
     * @param string $group
     * @param string $section
     * @param Mage_Core_Model_Store $store
     *
     * @return mixed
     */
    protected function getModuleConfig($field, $group, $section = 'bseller_elasticreport', Mage_Core_Model_Store $store = null)
    {
        return $this->getStoreConfig($field, $group, $section, $store);
    }


    /**
     * @param string $field
     *
     * @return mixed
     */
    protected function getGeneralConfig($field)
    {
        return $this->getModuleConfig($field, 'general');
    }


    /**
     * @param string $field
     *
     * @return mixed
     */
    protected function getCronConfig($field)
    {
        return $this->getModuleConfig($field, 'cron');
    }


    /**
     * @param string $field
     *
     * @return mixed
     */
    protected function getOrdersIndexConfig($field)
    {
        return $this->getModuleConfig($field, 'order_index');
    }


    /**
     * @return bool
     */
    protected function isEnabled()
    {
        return (bool) $this->getGeneralConfig('enabled');
    }


    /**
     * @return bool
     */
    protected function isCronEnabled()
    {
        return (bool) $this->getCronConfig('enabled');
    }


    /**
     * @return int
     */
    protected function getMaximumOrdersToProcessQty()
    {
        $qty = (int) $this->getCronConfig('queued_orders_qty');
        
        if (empty($qty) || $qty<0) {
            return null;
        }
        
        return $qty;
    }


    /**
     * @return string
     */
    protected function getOrdersIndexName($suffix = null)
    {
        $index = (string) $this->getOrdersIndexConfig('index');
        
        if (!empty($suffix)) {
            $index = $this->prepareIndexName($index, $suffix);
        }
        
        return $index;
    }
    
    
    /**
     * @param string $index
     *
     * @return string
     */
    protected function prepareIndexName($index, $suffix = null)
    {
        /**
         * Mariano's team told me that we got to prepare the index name appending the date as a suffix to the name.
         * It helps the backup and maintenance of the cluster.
         */
        return sprintf('%s-%s', $index, $this->getIndexNameSuffix($suffix));
    }
    
    
    /**
     * @return string
     */
    protected function getIndexNameSuffix($suffix = null)
    {
        if (!empty($suffix)) {
            return $suffix;
        }
        
        /** @var Mage_Core_Model_Date $model */
        $model = Mage::getSingleton('core/date');
        $date  = $model->date('Y.m.d');
        
        return $date;
    }


    /**
     * @return string
     */
    protected function getOrdersIndexTypeName()
    {
        return (string) $this->getOrdersIndexConfig('type');
    }

}
