<?php

trait BSeller_ElasticReport_Trait_Array
{

    /**
     * @param string $index
     * @param array  $data
     *
     * @return null|string|int|float
     */
    protected function extractIndex($index, $data)
    {
        return isset($data[$index]) ? $data[$index] : null;
    }


    /**
     * @param array $data
     *
     * @return array
     */
    protected function convertToInteger(array $data)
    {
        $data = array_map(function ($value) {
            return (int) $value;
        }, $data);

        return $data;
    }


    /**
     * @param array $orderIds
     *
     * @return array
     */
    protected function filterOrderIds(array $orderIds)
    {
        $orderIds = $this->convertToInteger($orderIds);
        $orderIds = array_filter($orderIds);
        $orderIds = array_unique($orderIds);

        return $orderIds;
    }

}
