<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Core
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Guilherme Miguelete <guilherme.miguelete@e-smart.com.br>
 */
/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer->removeAttribute('catalog_product', 'seller_order_quantity');

$installer->addAttribute(
    'catalog_product', 'seller_order_quantity',
    [
        'label'             => 'Seller Order Quantity',
        'type'              => Varien_Db_Ddl_Table::TYPE_TEXT,
        'position'          => 1,
        'backend'           => '',
        'frontend'          => '',
        'class'             => '',
        'visible'           => false,
        'required'          => false,
        'user_defined'      => false,
        'default'           => '0',
        'searchable'        => false,
        'filterable'        => true,
        'comparable'        => false,
        'visible_on_front'  => false,
        'unique'            => false,
        'used_for_sort_by'  => true,
        'used_in_product_listing' => true
    ]
);

$installer->endSetup();
