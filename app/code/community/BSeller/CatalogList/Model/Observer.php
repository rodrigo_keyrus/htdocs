<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Core
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Diogo Baracho <diogo.baracho@e-smart.com.br>
 */
class BSeller_CatalogList_Model_Observer
{

    /**
     * catalogProductMediaSaveBefore
     *
     * @param Varien_Event_Observer $observer
     * @return $this
     */
    public function catalogProductMediaSaveBefore(Varien_Event_Observer $observer)
    {

        $product  = $observer->getEvent()->getProduct();

        if ($product->getData('tax_treatment') != 1) {
            return $this;
        }

        $discount = Mage::getModel('bseller_cataloglist/product_discount')->getProductDiscount($product);
        if (!$discount) {
            return $this;
        }

        $product->setData('price_discount_percent', $discount);

        return $this;

    }
}
