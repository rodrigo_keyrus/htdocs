<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Core
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Diogo Baracho <diogo.baracho@e-smart.com.br>
 * @author    Guilherme Miguelete <guilherme.miguelete@e-smart.com.br>
 */
class BSeller_CatalogList_Model_Product_Discount extends Mage_Core_Model_Abstract
{

    const DEFAULT_BATCH_SIZE = 250;

    /**
     * getProductDiscount
     *
     * @param Mage_Catalog_Model_Product $product
     * @return int|mixed
     */
    public function getProductDiscount(Mage_Catalog_Model_Product $product)
    {

        /** @var Mage_Catalog_Model_Product $product */
        $product = Mage::getModel('catalog/product')->load($product->getId());

        if (!$product->getStockItem()->getIsInStock()) {
            return 0;
        }

        $price        = $product->getPrice();
        $specialPrice = $product->getFinalPrice();

        if (empty($specialPrice) || $specialPrice <= 0) {
            return 0;
        }

        $discount = $price - $specialPrice;
        if ($price == ($discount+$specialPrice)) {
            /* will return the percent value */
            return ((($price-$specialPrice) / $price)*100);
        }

        return 0;
    }

    /**
     * updateDiscountAttribute
     *
     * @param Mage_Catalog_Model_Product $product
     */
    public function updateDiscountAttribute(Mage_Catalog_Model_Product $product)
    {

        $discount = 0;
        $finalPrice = 0;

        try{

            foreach ($product->getTypeInstance(true)->getAssociatedProducts($product) as $productChild) {
                /** @var Mage_Catalog_Model_Product $productChild */
                if ($productChild->getData('status') != 1 || $productChild->getData('tax_treatment') != 1) {
                    continue;
                }

                if ($finalPrice == 0) {
                    $finalPrice = $productChild->getFinalPrice();
                }

                $childDiscount = $this->getProductDiscount($productChild);
                if ($childDiscount > $discount && $productChild->getFinalPrice() <= $finalPrice) {
                    $discount = $childDiscount;
                    $finalPrice = $productChild->getFinalPrice();
                }
            }

            if ($discount <= 0) {
                return;
            }

            /** @var Mage_Catalog_Model_Resource_Product_Action $action */
            $action = Mage::getModel('catalog/resource_product_action');
            $action->updateAttributes(
                [$product->getId()],
                ['price_discount_percent' => $discount],
                Mage_Core_Model_App::ADMIN_STORE_ID
            );

        } catch (Exception $e) {
            Mage::exception($e);
        }
    }


    /**
     * @param null $pageSize
     */
    public function batchUpdate($pageSize = null)
    {

        /** @var Mage_Catalog_Model_Resource_Product_Collection $collection */
        $collection = Mage::getModel('catalog/product')->getCollection();

//        $collection->addAttributeToFilter('sku', ['eq'=> 'PRD17672']);
        $collection->addAttributeToFilter('status', ['eq'=>'1']);
        $collection->addAttributeToFilter('tax_treatment', ['eq'=>'1']);
        $collection->addAttributeToFilter('type_id', Mage_Catalog_Model_Product_Type::TYPE_GROUPED);

        $startTime = microtime(true);

        $this->reset();
        $this->walk($collection, $pageSize);

        echo "Ended in Time: " . (microtime(true) - $startTime) . " seconds\n";
    }


    /**
     * walk
     *
     * @param Mage_Catalog_Model_Resource_Product_Collection $collection
     * @param null $batchSize
     */
    public function walk($collection, $batchSize = null)
    {
        if (!$batchSize) {
            $batchSize = self::DEFAULT_BATCH_SIZE;
        }

        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

        $collection->setPageSize($batchSize);

        $total = $collection->getSize();

        $currentPage = 1;
        $pages = $collection->getLastPageNumber();
        do {
            $collection->setCurPage($currentPage);
            $collection->load();
            foreach ($collection as $product) {
                $this->updateDiscountAttribute($product);
            }

            echo 'pages: '.$currentPage.'/'.$pages.' - pagesize: '.$batchSize. ' - total: '.$total."\n";

            $currentPage++;
            $collection->clear();

        } while ($currentPage <= $pages);
    }


    /**
     * wrong
     */
    public function reset()
    {
        $resource = Mage::getSingleton('core/resource');
        $query="
        update catalog_product_entity_decimal as edec set edec.value='0.00'
        where 1=1
        and (edec.value IS NULL || edec.value <> '')
        and edec.attribute_id=(select ea.attribute_id from
        catalog_eav_attribute as ea,
        eav_attribute as eat
        where 1=1
        AND eat.attribute_id=ea.attribute_id
        AND eat.attribute_code='price_discount_percent'
        AND edec.attribute_id=ea.attribute_id
        limit 1)
        limit 999999999;
        ";
        $writeConnection = $resource->getConnection('core_write');
        $writeConnection->query($query);
    }

}
