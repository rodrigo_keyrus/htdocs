<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Core
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Guilherme Miguelete <guilherme.miguelete@e-smart.com.br>
 */
class BSeller_CatalogList_Model_Product_Bestsellers extends Mage_Core_Model_Abstract
{

    const DEFAULT_BATCH_SIZE = 250;


    /**
     * @param $associatedProductIds
     * @return Mage_Sales_Model_Resource_Order_Item_Collection
     */
    private function getProductsBestSellers($associatedProductIds)
    {

        /** @var Mage_Sales_Model_Resource_Order_Item_Collection $collection */
        $collection = Mage::getResourceModel('sales/order_item_collection');
        $collection->getSelect()->reset(Zend_Db_Select::COLUMNS)
        ->columns(['product_id', 'SUM(qty_ordered) as order_quantity']);
        //$collection->getSelect()->where('product_id in (64884, 32043, 8227)');
        $collection->addFieldToFilter('product_id', ['in'=> $associatedProductIds]);
        $collection->getSelect()->group('product_id');
        $collection->getSelect()->order('order_quantity DESC');

        $orderQuantity = 0;
        foreach ($collection as $item) {
            $orderQuantity += $item->getData('order_quantity');
        }

        return $orderQuantity;

    }


    /**
     * getAttributeValue
     *
     * @param Mage_Catalog_Model_Product $product
     * @return int|mixed
     */
    public function getAttributeValue(Mage_Catalog_Model_Product $product)
    {
        $associatedProductIds = Mage::getModel('catalog/product_type_grouped')->getAssociatedProductIds($product);
        $orderQuantity = $this->getProductsBestSellers($associatedProductIds);
        return $orderQuantity;
    }

    /**
     * updateDiscountAttribute
     *
     * @param Mage_Catalog_Model_Product $product
     */
    public function updateAttribute(Mage_Catalog_Model_Product $product)
    {

        $attributeValue = 0;

        try{

            /** @var int $attributeValue */
            $attributeValue = $this->getAttributeValue($product);

            if ($attributeValue <= 0) {
                return;
            }

            /** @var Mage_Catalog_Model_Resource_Product_Action $action */
            $action = Mage::getModel('catalog/resource_product_action');
            $action->updateAttributes(
                [$product->getId()],
                ['seller_order_quantity' => $attributeValue],
                Mage_Core_Model_App::ADMIN_STORE_ID
            );

        } catch (Exception $e) {
            Mage::exception($e);
        }
    }


    /**
     * @param null $pageSize
     */
    public function batchUpdate($pageSize = null)
    {

        /** @var Mage_Catalog_Model_Resource_Product_Collection $collection */
        $collection = Mage::getModel('catalog/product')->getCollection();

        //$collection->addAttributeToFilter('sku', ['eq'=> 'PRD19320']);
        $collection->addAttributeToFilter('status', ['eq'=>'1']);
        $collection->addAttributeToFilter('tax_treatment', ['eq'=>'1']);
        $collection->addAttributeToFilter('type_id', Mage_Catalog_Model_Product_Type::TYPE_GROUPED);

        $startTime = microtime(true);

        $this->reset();
        $this->walk($collection, $pageSize);

        echo "Ended in Time: " . (microtime(true) - $startTime) . " seconds\n";
    }


    /**
     * walk
     *
     * @param Mage_Catalog_Model_Resource_Product_Collection $collection
     * @param null $batchSize
     */
    public function walk($collection, $batchSize = null)
    {
        if (!$batchSize) {
            $batchSize = self::DEFAULT_BATCH_SIZE;
        }

        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

        $collection->setPageSize($batchSize);

        $total = $collection->getSize();

        $currentPage = 1;
        $pages = $collection->getLastPageNumber();
        do {
            $collection->setCurPage($currentPage);
            $collection->load();
            foreach ($collection as $product) {
                $this->updateAttribute($product);
            }

            echo 'pages: '.$currentPage.'/'.$pages.' - pagesize: '.$batchSize. ' - total: '.$total."\n";

            $currentPage++;
            $collection->clear();

        } while ($currentPage <= $pages);
    }


    /**
     * wrong
     */
    public function reset()
    {
        $resource = Mage::getSingleton('core/resource');
        $query="
        update catalog_product_entity_text as edec set edec.value='0.00'
        where 1=1
        and (edec.value IS NULL || edec.value <> '')
        and edec.attribute_id=(select ea.attribute_id from
        catalog_eav_attribute as ea,
        eav_attribute as eat
        where 1=1
        AND eat.attribute_id=ea.attribute_id
        AND eat.attribute_code='seller_order_quantity'
        AND edec.attribute_id=ea.attribute_id
        limit 1)
        limit 999999999;
        ";
        $writeConnection = $resource->getConnection('core_write');
        $writeConnection->query($query);
    }

}
