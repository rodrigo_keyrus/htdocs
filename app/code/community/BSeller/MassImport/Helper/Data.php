<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_MassImport
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Diogo Santiago <diogo.santiago@b2wdigital.com>
 */
class BSeller_MassImport_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Attribute Type ID
     * @var int
     */
    protected $attributeTypeId;

    /**
     * Clear invalid char begin line
     *
     * @param string $filename
     * @param bool $removeSpecialChars
     *
     * @return string
     */
    public function csvToArray($filename, $removeSpecialChars = true)
    {
        $line = 0;
        $combinedData = $keys = [];

        ini_set('auto_detect_line_endings', true);
        $fileContent = fopen($filename, 'r');

        while ($row = fgetcsv($fileContent, 1024)) {
            $line++;

            if (!$row) {
                continue;
            }

            if ($line == 1) {
                $keys = array_map('trim', $row);
                continue;
            }

            if ($removeSpecialChars) {
                $row = array_map(function($value) {
                    return preg_replace('/^[\pZ\p{Cc}\x{feff}]+|[\pZ\p{Cc}\x{feff}]+$/ux', '', $value);
                }, $row);
            }

            $row = array_combine($keys, $row);
            $combinedData[] = $row;
        }

        fclose($fileContent);
        ini_set('auto_detect_line_endings', false);

        return $combinedData;
    }

    
    /**
     * Generate CSV by Collection
     *
     * @param mixed $line
     * @param string $filename
     *
     * @return void
     */
    public function generateCsv(Varien_Object $line, $filename = 'export')
    {
        header("Content-Type: text/csv; charset=utf-8");
        header("Content-Disposition: attachment; filename={$filename}.csv");

        // create a file pointer connected to the output stream
        $output = fopen('php://output', 'w');
        $contentLine = $line->getData();

        fputcsv($output, array_keys($contentLine));
        fputcsv($output, array_values($contentLine));
    }


    /**
     * @param array $skus
     * @return array
     */
    public function loadIdsBySkus(array $skus)
    {
        $skus = array_map('trim', $skus);

        return Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect('entity_id')
            ->addFieldToFilter('sku', ['in' => $skus])
            ->getColumnValues('entity_id');
    }


    /**
     * Get attributes related a AttributeSet
     *
     * @param int $attributeSetId
     *
     * @return array
     */
    public function getAttributesByAttributeSet($attributeSetId)
    {
        $attributesIds = Mage::getResourceModel('eav/entity_attribute_collection')
            ->addFieldToSelect('attribute_id')
            ->setAttributeSetFilter($attributeSetId)
            ->getColumnValues('attribute_id');

        $attributeCodes = [];

        array_walk($attributesIds, function($attributeId) use (&$attributeCodes){
            $attr = Mage::getResourceModel('catalog/eav_attribute')->load($attributeId);

            // Just the needed attributes to the store
            $isNecessary = $attr->getIsVisible() && ($attr->getIsRequired() || $attr->getIsUserDefined());

            if ($isNecessary) {
                $attributeCodes[] = $attr->getAttributeCode();
            }
        });

        return $attributeCodes;
    }

    
    /**
     * Get Options from attribute
     *
     * @param string $attributeCode
     * @param int $optionId
     * @param string $optionLabel
     * @param Esmart_MassImport_Model_Content_AbstractContent $object
     *
     * @return array|bool
     */
    public function getOptionsByAttrCode($attributeCode, $optionId = null, $optionLabel = null, $object = null)
    {
        $sourceModel = 'eav/entity_attribute_source_table';

        $attribute = Mage::getModel('eav/entity_attribute')->loadByCode(4, $attributeCode);

        if ($attribute->getData('source_model')) {
            $sourceModel = $attribute->getData('source_model');
        }

        /** @var Mage_Eav_Model_Entity_Attribute_Source_Table $attrOptionsModel */
        $attrOptionsModel = Mage::getModel($sourceModel)->setAttribute($attribute);

        $options = $attrOptionsModel->getAllOptions(false);

        if (!$options && $object) {
            /** @var Esmart_MassImport_Model_Content_AttributeOptionInterface $object */
            $options = $object->getOptionsFromAttribute($attributeCode);
        }

        if (!is_null($optionId)) {
            $key = array_search($optionId, array_column($options, 'value'));
            return $key >= 0 ? $options[$key] : false;
        }

        if (!is_null($optionLabel)) {
            $options = array_map('strtolower', $options);
            $key = array_search(strtolower($optionLabel), array_column($options, 'label'));
            return $key >= 0 ? $options[$key] : false;
        }

        return $options;
    }
}
