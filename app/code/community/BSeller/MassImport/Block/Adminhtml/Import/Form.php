<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_MassImport
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Diogo Santiago <diogo.santiago@b2wdigital.com>
 */
class BSeller_MassImport_Block_Adminhtml_Import_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Prepare form before rendering HTML
     *
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $form   = new Varien_Data_Form(array(
            'id'        => 'edit_form',
            'action'    => $this->getUrl('*/*/save'),
            'method'    => 'post',
            'enctype' 	=> 'multipart/form-data',
        ));

        $fieldSet   = $form->addFieldset('base_fieldset', array(
            'legend'    => $this->__("Information"),
            'class'     => 'fieldset-wide',
        ));

        $fieldSet->addField('description', 'text', array(
            'name'      => 'description',
            'label'     => $this->__('Description'),
            'title'     => $this->__('Description of importation'),
            'required'  => true,
            'maxlength' => 64,
        ));

        $fieldSet->addField('file', 'file', array(
            'name'      => 'file',
            'label'     => $this->__('File'),
            'title'     => $this->__('File'),
            'required'  => true,
        ));

        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}
