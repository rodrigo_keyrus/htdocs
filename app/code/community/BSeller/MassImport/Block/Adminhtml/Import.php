<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_MassImport
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Diogo Santiago <diogo.santiago@b2wdigital.com>
 */
class BSeller_MassImport_Block_Adminhtml_Import extends Mage_Adminhtml_Block_Widget_Form_Container
{
    var $_blockGroup  = 'bseller_massimport';
    var $_controller  = 'adminhtml';
    var $_mode        = 'import';


    /**
     * Internal constructor, that is called from real constructor
     */
    public function __construct()
    {
        parent::__construct();

        $this->_headerText  = $this->__('Mass Import - Import New File');

        $this->_removeButton('save');
        $this->_removeButton('reset');

        $this->addButton('template', array(
            'label'     => $this->__('Build Template CSV'),
            'onclick'   => $this->dialogJs(),
            'class'     => 'scalable',
        ));

        $this->addButton('mass_import', array(
            'label'   => Mage::helper('bseller_massimport')->__('Import'),
            'onclick' => 'editForm.submit()',
            'class'   => 'save'
        ));
    }


    /**
     * Get URL for back (reset) button
     *
     * @return string
     */
    public function getBackUrl()
    {
        return $this->getUrl('*/catalog_product/index');
    }

    /**
     * Get form save URL
     *
     * @deprecated
     * @see getFormActionUrl()
     * @return string
     */
    public function getSaveUrl()
    {
        $this->setData('form_action_url', 'save');
        return $this->getFormActionUrl();
    }


    /**
     *
     * @return string
     */
    protected function dialogJs()
    {
        $title = $this->__('Build Template CSV');
        $url = $this->getUrl('*/*/buildCsv');

        return "Dialog.info(null, {
            closable: true,
            resizable: false,
            draggable: true,
            scroll: false,
            className: 'magento',
            windowClassName: 'popup-window',
            title: '{$title}',
            top: 50,
            width: 780,
            height: 600,
            zIndex: 1000,
            recenterAuto: false,
            hideEffect: Element.hide,
            showEffect: Element.show,
            id: 'browser_window',
            url: '{$url}'
        });";
    }
}
