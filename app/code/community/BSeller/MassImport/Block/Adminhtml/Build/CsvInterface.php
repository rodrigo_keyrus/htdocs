<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_MassImport
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Diogo Santiago <diogo.santiago@b2wdigital.com>
 */
class BSeller_MassImport_Block_Adminhtml_Build_CsvInterface extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();

        $this->_removeButton('save');
        $this->_removeButton('reset');
        $this->_removeButton('back');

        $this->_headerText  = $this->__('Mass Import - Build Template CSV');

        $this->addButton('build', array(
            'label'   => Mage::helper('bseller_massimport')->__('Build'),
            'onclick' => 'editForm.submit()',
            'class'   => 'save'
        ));
    }


    /**
     * Get form save URL
     *
     * @deprecated
     * @see getFormActionUrl()
     * @return string
     */
    public function getSaveUrl()
    {
        $this->setData('form_action_url', 'save');
        return $this->getFormActionUrl();
    }


    /**
     *
     */
    protected function _prepareLayout()
    {
        $block = $this->getLayout()->createBlock('adminhtml/widget_form');

        $form   = new Varien_Data_Form([
            'id'        => 'edit_form',
            'action'    => $this->getUrl('*/*/generateCsv'),
            'method'    => 'post'
        ]);

        $fieldSet   = $form->addFieldset('base_fieldset', [
            'legend'    => $this->__('Choose Options'),
            'class'     => 'fieldset-wide'
        ]);

        $fieldSet->addField('attribute_set', 'select', [
            'label'     => $this->__('Attribute Set'),
            'options'   => Mage::getModel('bseller_massimport/source_attributeSet')->toOptionArray(),
            'name'      => 'attribute_set',
            'class'     => 'validate-select'
        ]);

        $optionsAvailable = Mage::getModel('bseller_massimport/template_product')->optionsAvailable(true);

        $func = function($value, $key) use ($fieldSet) {
            $options = [
                'required' => false,
                'label'    => $value->label(),
                'onclick'  => 'this.value = this.checked ? 1 : 0;',
                'name'     => "options[{$key}]"
            ];

            $mandatory = false;
            if (in_array($key, ['general_information', 'availability', 'price'])) {
                $options['checked'] = true;
                $mandatory = true;
            }

            $options['note'] = $this->getAttributeNote($value->interfaceNote(), $mandatory);
            $fieldSet->addField($key, 'checkbox', $options);
        };
        
        array_walk($optionsAvailable, $func);

        $form->setUseContainer(true);
        $block->setForm($form);
        $this->setChild('form', $block);
        $this->_blockGroup = null;
        parent::_prepareLayout();
    }


    /**
     * Get attributes to show in 'notes' form input
     *
     * @param array $attributes
     * @param bool $required
     *
     * @return string
     */
    protected function getAttributeNote(array $attributes, $required = false)
    {
        $extra = $required ? '<b>MANDATORY*</b>' : '';
        $notes = $this->__('Append into template columns: %s <hr /><br />', $extra);

        array_walk($attributes, function($value, $key) use (&$notes) {
            $attribute = Mage::getModel('eav/entity_attribute')->loadByCode(4, $key);

            if (!$attribute->getId() && $attribute->getFrontendLabel()) {
                $value = $attribute->getFrontendLabel();
                $key = $attribute->getAttributeCode();
            }

            $notes .= sprintf('<b>%s :</b> %s <br />', $key, $value);
        });

        return $notes;
    }
}
