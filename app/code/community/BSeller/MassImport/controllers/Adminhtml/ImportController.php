<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_MassImport
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Diogo Santiago <diogo.santiago@b2wdigital.com>
 */
class BSeller_MassImport_Adminhtml_ImportController extends Mage_Adminhtml_Controller_Action
{
    use BSeller_MassImport_Model_Trait;


    var $expectedKeys = [
        'sku',
        'name',
        'type_id',
        'status',
        'tax_treatment',
        'visibility',
        'manufacturer',
    ];


    /**
     * History FILE action
     *
     * Show NEW import form
     */
    public function formAction()
    {
        $this->loadLayout();

        $this->getLayout()
            ->getBlock('head')
            ->addItem('js_css', 'prototype/windows/themes/default.css')
            ->addCss('lib/prototype/windows/themes/magento.css');

        $this->_addContent($this->getLayout()->createBlock('bseller_massimport/adminhtml_import'));
        $this->renderLayout();
    }


    /**
     * Save action
     *
     * Action used when save new file
     */
    public function saveAction()
    {
        if (!$data = $this->getRequest()->getPost()) {
            return $this->_redirect('*/*/form');
        }

        $description = strtolower(Mage::helper('core')->removeAccents($data['description']));
        $helper = Mage::helper('bseller_massimport');
        $hasErrors = false;

        if (isset($_FILES['file']['tmp_name']) && !file_exists($_FILES['file']['tmp_name'])) {
            $this->_getSession()->addError('File uploaded not found. Upload file again.');
            return $this->_redirect('*/*/form');
        }

        ini_set('auto_detect_line_endings', true);

        $fileContent = fopen($_FILES['file']['tmp_name'], 'r');

        if(!$this->validateFields($fileContent)){
            $this->_getSession()->addError($this->__('CSV file not imported.'));

            return $this->_redirect('*/*/form');
        }

        fclose($fileContent);

        $fileContent = fopen($_FILES['file']['tmp_name'], 'r');

        $line = 0;
        $imported = 0;

        while ($row = fgetcsv($fileContent, null, ';')) {
            $line++;

            if (!$row) { continue; }

            // Guarantees the UTF-8 encoding from fils
            $row = array_map(function($value) {
                $value = trim($value);
                return (mb_detect_encoding($value, 'UTF-8', true) === false) ? utf8_encode($value) : $value;
            }, $row);

            if ($line == 1) {
                if (!$this->validateKeys($row)) {
                    $this->_getSession()->addError('Your header appear to be wrong! ' .
                        'Look if ti have at least the fields: ' . implode(', ', $this->expectedKeys));
                    return $this->_redirect('*/*/form');
                }
                continue;
            }           

            try {
                $status = 2;
 
                Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
                $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $row[0]);
                
                if($product){
                    continue;
                }else{
                    $product = Mage::getModel('catalog/product');
                }

                $product
                    ->setWebsiteIds(array(1))
                    ->setAttributeSetId(4)
                    ->setTypeId($row[2])
                    ->setCreatedAt(strtotime('now'))
                    ->setSku($row[0])
                    ->setName($row[1])
                    ->setStatus($status)
                    ->setTaxTreatment($row[4])
                    ->setVisibility($row[5])
                    ->setManufacturer($this->getOptionIdByAttributeManufacturer($row[6]))
                    ->save();
                    $imported++;

                Mage::log('Product: '.$row[0].' imported', null, 'mass_import.log');                    

            } catch (Exception $e) {
                Mage::log($e->getMessage());
                $this->_getSession()->addError('Error importing products! Total of '.$imported.' products imported.');
                return $this->_redirect('*/*/form');                    
            }
        }

        fclose($fileContent);
        ini_set('auto_detect_line_endings', false);

        if (!$hasErrors) {
            $this->_getSession()->addSuccess($helper->__('Success file imported! Total of '.$imported.' products imported.'));
        }

        return $this->_redirect('*/*/form');
    }


    /**
     * Show Build Template CSV
     */
    public function buildCsvAction()
    {
        $this->loadLayout('popup');

        $this->getLayout()->getBlock('root')->unsetChild('footer');

        $formBlock = $this->getLayout()->createBlock('bseller_massimport/adminhtml_build_csvInterface');
        $this->_addContent($formBlock);

        $this->renderLayout();
    }

    /**
     * @param $fileContent
     * @return bool
     */
    public function validateFields($fileContent)
    {
        $line = 0;
        $valid = true;

        while ($row = fgetcsv($fileContent, null, ';')) {

            $line++;

            if ($line == 1) {
                continue;
            }

            foreach ($this->expectedKeys as $key => $field){

                if(empty($row[$key])){

                    if($field == 'sku'){
                        $this->_getSession()->addError($this->__("Product line '%s' has the field '%s' empty.", $line, $field));
                    } else {
                        $this->_getSession()->addError($this->__("Import was not performed because the '%s' attribute was not filled in SKU: '%s'.", $field, $row[0]));
                    }

                    $valid = false;
                }
            }
        }

        return $valid;
    }

    /**
     * Generate CSV
     */
    public function generateCsvAction()
    {
        $params = $this->getRequest()->getParams();

        $collection = Mage::getModel('bseller_massimport/template_product')->getTemplateCsv($params);
        $this->helper()->generateCsv($collection, 'template');
    }


    /**
     * @param array $keys
     * @return bool
     */
    protected function validateKeys(array $keys)
    {
        $intersection = array_intersect($this->expectedKeys, $keys);
        return count($intersection) == count($this->expectedKeys);
    }

    public function getOptionIdByAttributeManufacturer($optionName){
        $name = 'manufacturer';
        $attributeInfo = Mage::getResourceModel('eav/entity_attribute_collection')->setCodeFilter($name)->getFirstItem();
        $attributeId = $attributeInfo->getAttributeId();
        $attribute = Mage::getModel('catalog/resource_eav_attribute')->load($attributeId);
        $attributeOptions = $attribute ->getSource()->getAllOptions(false);

        foreach ($attributeOptions as $_option){
            if($_option['label'] == $optionName){
                return (int) $_option['value'];
            }
        }  
    }       
}
