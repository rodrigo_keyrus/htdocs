<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_MassImport
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Diogo Santiago <diogo.santiago@b2wdigital.com>
 */
trait BSeller_MassImport_Model_Trait
{
    use Esmart_Core_Trait;

    /**
     * @return BSeller_MassImport_Helper_Data
     */
    public function helper()
    {
        return Mage::helper('bseller_massimport');
    }


    /**
     * Get array from string with common delimiter character
     *
     * @param string $inputString
     * @param string $delimiter
     *
     * @return array
     */
    public function getArrayFromString($inputString, $delimiter = ',')
    {
        $array = explode($delimiter, $inputString);
        return array_map('trim', $array);
    }


    /**
    * Logs messages in a certain file
    *
    * @param string|Exception $msg
    * @param string $filename
    * @param int $type
    */
    public function log($msg, $type = Zend_Log::ALERT, $filename = 'massimport.log')
    {
        if ($msg instanceof Exception) {
            $msg = $msg->getMessage();
        }

        if ($type == Zend_Log::ERR && $filename == 'massimport.log') {
            $filename = 'massimport-error.log';
        }

        Mage::log($msg, $type, $filename, true);
    }
}
