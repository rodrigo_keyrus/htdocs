<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_MassImport
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Diogo Santiago <diogo.santiago@b2wdigital.com>
 */
class BSeller_MassImport_Model_Source_AttributeSet
{
    /**
     * Get AttributesSets
     *
     * @return array
     */
    public function toOptionArray()
    {
        $attributesSets = Mage::getModel('catalog/product_attribute_set_api')->items();

        $options = ['none' => ''];

        array_walk($attributesSets, function ($value) use (&$options) {
            $options[$value['set_id']] = $value['name'];
        });

        return $options;
    }
}
