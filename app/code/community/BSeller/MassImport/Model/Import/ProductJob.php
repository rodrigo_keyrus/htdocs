<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_MassImport
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Diogo Santiago <diogo.santiago@b2wdigital.com>
 */
class BSeller_MassImport_Model_Import_ProductJob extends BSeller_Resque_Model_Job_Abstract
{
    use BSeller_MassImport_Model_Trait;
    
    protected static $parts = [
        'website',
        'attributes',
        'category',
        'configurable',
        'grouped',
        'images',
        'stock'
    ];


    /**
     * Process data content
     *
     */
    public function perform()
    {
        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID); 
        Mage::getSingleton('catalog/product')->clearInstance();
        $args = $this->getArgs();
        $data = $args['combined_data'];
        $line = $args['line'];

        if (!isset($data['sku'])) {
            $this->log('Sku not found on line: ' . $line, Zend_Log::ERR);
            return;
        }

        if (!isset($data['attribute_set_id'])) {
            $this->log('"Attribute Set" not found on line: ' . $line, Zend_Log::ERR);
            return;
        }

        /** @var Mage_Catalog_Model_Product $product */
        $product = Mage::getSingleton('catalog/product')->loadByAttribute('sku', $data['sku']);

        if (!$product) {
            $product = Mage::getModel('catalog/product');
        }

        $result = [];
        $product->addData($data);

        if (!$product->getUrlKey()) {
            $product->setUrlKey(null);
        }

        array_walk(self::$parts, function ($value) use ($product, &$result) {
            try {
                $result[$value] = Mage::getModel("bseller_massimport/import_parts_{$value}")->process($product);

                $successArr = array_filter($result, function($value) {
                    return stripos($value, 'success') !== false;
                });

                if (count($successArr) == 0) {
                    $this->log('Sku: ' . $product->getSku(), Zend_Log::INFO, 'massimport-skus_not_imported.log');
                }

            } catch (Exception $e) {
                $this->log($e->getMessage(), Zend_Log::ERR);
            }
        });

        $result = array_map(function ($key, $value) {
            return "{$key}: $value";
        }, array_keys($result), $result);

        $result = $data['sku'] . ' ==> ' . implode(', ', $result);
        $this->log($result, Zend_Log::INFO);
    }
}
