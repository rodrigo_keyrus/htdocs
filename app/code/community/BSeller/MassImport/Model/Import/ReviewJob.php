<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_MassImport
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Gabriel da Gama <gabriel.gama@e-smart.com.br>
 */
class BSeller_MassImport_Model_Import_ReviewJob extends BSeller_Resque_Model_Job_Abstract
{
    use BSeller_MassImport_Model_Trait;
   
    const DEFAULT_STORE_ID = 1;
    
    const APROVED_STATUS_ID = 1;
  
    /**
     * Process data content
     *
     */
    public function perform()
    {
        Mage::app()->setCurrentStore(self::DEFAULT_STORE_ID);
        $args = $this->getArgs();
        
        $data = $args['combined_data'];
        $line = $args['line'];

        if (!isset($data['sku'])) {
            $this->log('Sku not found on line: ' . $line, Zend_Log::ERR);
            return;
        }
        try {
            /** @var Mage_Catalog_Model_Product $product */
            $productId = Mage::getSingleton('catalog/product')->getIdBySku($data['sku']);

            if (!$productId) {
                return;
            }

            /** @var Mage_Review_Model_Review $review */
            $review = Mage::getModel('review/review');
            $review->setEntityPkValue($productId);
            $review->setStatusId(self::APROVED_STATUS_ID);
            $review->setTitle($data['email']);
            $review->setDetail($data['detail']);
            $review->setEntityId(1);                                      
            $review->setStoreId(Mage::app()->getStore()->getId());                    
            $review->setCustomerId(null);
            $review->setNickname($data['nickname']);
            $review->setReviewId($review->getId());
            $review->setStores(array(Mage::app()->getStore()->getId()));
            $review->save();

            // Add review Ratting
            $rating_options = array(
                    1 => array(1,2,3,4,5),
            );				 
            // Now save the ratings
            foreach($rating_options as $rating_id => $option_ids):
                $rating = Mage::getModel('rating/rating')
                    ->setRatingId($rating_id)
                    ->setReviewId($review->getId())
                    ->addOptionVote($option_ids[$data['rating']-1], $productId);
            endforeach;

            /** @var Mage_Review_Model_Review $updateReview */
            $updateReview = Mage::getModel('review/review')->load($review->getId());
            $updateReview->setCreatedAt($data['created_at']);
            $updateReview->save();
        } catch (Exception $e) {
            Mage::logException($e->getMessage());
        }
    }
}
