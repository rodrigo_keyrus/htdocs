<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_MassImport
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Diogo Santiago <diogo.santiago@b2wdigital.com>
 */
class BSeller_MassImport_Model_Import_Parts_Website
{
    use BSeller_MassImport_Model_Trait;

    /**
     * Process data content
     *
     * @param $product
     * @return $this
     */
    public function process(Mage_Catalog_Model_Product $product)
    {
        try {
            $websiteIds = $product->getData('website_ids');

            if (!is_array($websiteIds)) {
                $websiteIds = array_filter($this->getArrayFromString($websiteIds));
            }

            $websiteNames = array_filter($this->getArrayFromString($product->getData('website_names')));

            if ($websiteNames) {
                $mageWebsites = Mage::app()->getWebsites();

                array_walk($mageWebsites, function($mageWebsite) use (&$websiteIds, $websiteNames) {
                    if (in_array($mageWebsite->getName(), $websiteNames)) {
                        $websiteIds[] = $mageWebsite->getId();
                    }
                });
            }

            if(!$websiteIds) {
                $websiteIds = array_keys(Mage::app()->getWebsites());
            }

            $product->setData('website_ids', array_unique($websiteIds));
            $product->save();
        } catch (Exception $exp) {
            return $exp->getMessage();
        }

        return 'Success';
    }


}
