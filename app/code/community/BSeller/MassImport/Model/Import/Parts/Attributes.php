<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_MassImport
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Diogo Santiago <diogo.santiago@b2wdigital.com>
 */
class BSeller_MassImport_Model_Import_Parts_Attributes
{
    use BSeller_MassImport_Model_Trait;


    /**
     * Process data content
     *
     * @param Mage_Catalog_Model_Product $product
     * @return Mage_Catalog_Model_Product
     */
    public function process(Mage_Catalog_Model_Product $product)
    {
        try {
            array_walk($product->getData(), function($value, $key) use ($product){
                $this->setAttributeInProduct($product, $key, $value);
            });

            $product->save();
        } catch (Exception $exp) {
            return $exp->getMessage();
        }

        return 'Success';
    }


    /**
     * Persist attribute in Product
     *
     * @param $product
     * @param string $key
     * @param mixed $value
     *
     * @return Varien_Object
     */
    protected function setAttributeInProduct($product, $key, $value)
    {
        $attribute = Mage::getModel('eav/entity_attribute')->loadByCode(4, $key);

        if (!$attribute->usesSource() || is_numeric($value) || $value == '') {
            return;
        }

        $selectedOption = null;
        $attributeWithSource = false;
        $value = $value ? $value : 0;

        try {
//            $source = @$attribute->getSource();
            $selectedOption = $attribute->getSource()->getOptionId($value);

            if (is_null($selectedOption)) {
                $attributeWithSource = true;
                throw new Exception('Error on getSource()');
            }

        } catch (Exception $exp) {
            $option = $this->helper()->getOptionsByAttrCode($key, null, $value);

            if ($option) {
                $selectedOption = $option['value'];
            }
        }

        if (is_null($selectedOption)
            && Mage::getStoreConfigFlag('bseller/massimport/create_option_value_automatically')
            && !$attributeWithSource) {

            $newOptionValue['attribute_id'] = $attribute->getId();
            $newOptionValue['value']['any_option_name'][0] = $value;
            Mage::getResourceModel('catalog/setup', 'catalog_setup')->addAttributeOption($newOptionValue);

            return $this->setAttributeInProduct($key, $value, $product);
        }
    }
}
