<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_MassImport
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Diogo Santiago <diogo.santiago@b2wdigital.com>
 */
class BSeller_MassImport_Model_Import_Parts_Category
{
    use BSeller_MassImport_Model_Trait;

    /**
     * Process data content
     *
     * @param $product
     * @return string
     */
    public function process(Mage_Catalog_Model_Product $product)
    {
        if (!$product->getData('category_ids') && !$product->getData('category_names')
            && !$product->getData('category_axcodes')) {
            return 'No data to process for this type content.';
        }

        $validIds = [];

        // For Category Ids
        if ($product->getData('category_ids')) {

            $validIds = explode(',', $product->getData('category_ids'));

            $validIds = Mage::getResourceModel('catalog/category_collection')
                ->addAttributeToFilter('entity_id', ['in' => $validIds])
                ->getColumnValues('entity_id');


            if (count($validIds) == 0) {
                return 'The item "category_ids" passed but no valid ids on database.';
            }
        }

        // For Category Names
        if (!$validIds && $product->getData('category_names')) {

            $validIds = explode(',', $product->getData('category_names'));

            $validIds = Mage::getResourceModel('catalog/category_collection')
                ->addAttributeToFilter('name', ['in' => $validIds])
                ->getColumnValues('entity_id');

            if (count($validIds) == 0) {
                return 'The item "category_ids" passed but no valid ids on database.';
            }
        }

        // For Sephora Ax Codes
        if (!$validIds && $product->getData('category_axcodes')) {

            $validIds = explode(',', $product->getData('category_axcodes'));

            $validIds = Mage::getResourceModel('catalog/category_collection')
                ->addAttributeToFilter('ax_code', ['in' => $validIds])
                ->getColumnValues('entity_id');

            if (count($validIds) == 0) {
                return 'The item "category_ids" passed but no valid ids on database.';
            }
        }

        try {
            $product->setData('category_ids', $validIds);
            $product->save();
        } catch (Exception $exp) {
            return $exp->getMessage();
        }

        return 'Success';
    }
}
