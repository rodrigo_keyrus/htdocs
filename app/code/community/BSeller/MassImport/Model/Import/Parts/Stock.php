<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_MassImport
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Diogo Santiago <diogo.santiago@b2wdigital.com>
 */
class BSeller_MassImport_Model_Import_Parts_Stock
{
    use BSeller_MassImport_Model_Trait;

    /**
     * Process data content
     *
     * @return $this
     */
    public function process(Mage_Catalog_Model_Product $product)
    {
        try {
            $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product->getId());

            if (!$stockItem->getId()) {
                $stockItem->setData('product_id', $product->getId());
                $stockItem->setData('stock_id', Mage_CatalogInventory_Model_Stock::DEFAULT_STOCK_ID);
                $stockItem->setData('type_id', $product->getTypeId());
            }

            array_walk($product->getData(), function($value, $key) use ($stockItem) {
                if (strpos($key, 'stock.') !== false) {
                    $attr = explode('.', $key)[1];
                    $stockItem->setData($attr, $value);
                }
            });

            $stockItem->save();
            $product->save();
            Mage::getSingleton('cataloginventory/stock_status')->updateStatus($product->getId(), $product->getTypeId());
        } catch (Exception $exp) {
            return $exp->getMessage();
        }

        return 'Success';
    }


    /**
     * Internal construct
     * @return void
     */
    protected function _construct()
    {
        $basicStock = $this->getAvailableAttributes(true);

        $advancedStock = $this->getAvailableAttributesAdvanced(true);

        $merged = array_unique(array_merge($basicStock, $advancedStock));

        $this->validAttributes = $merged;
    }


    /**
     * Get options from attributes
     *
     * This method returns the options for unmapped attributes in the 'eav_attributes'
     *
     * @param string $attributeCode
     *
     * @return array
     */
    public function getOptionsFromAttribute($attributeCode)
    {
        switch ($attributeCode) {
            case 'is_in_stock':
                /** @var Mage_CatalogInventory_Model_Source_Stock $object */
                $object = Mage::getModel('cataloginventory/source_stock');
                return $object->toOptionArray();
                break;
            case 'backorders':
                /** @var Mage_CatalogInventory_Model_Source_Backorders $object */
                $object = Mage::getModel('cataloginventory/source_backorders');
                return $object->toOptionArray();
                break;
            default:
                /** @var Mage_Eav_Model_Entity_Attribute_Source_Boolean $object */
                $object = Mage::getModel('eav/entity_attribute_source_boolean');
                return $object->getAllOptions();
        }
    }
}
