<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_MassImport
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Diogo Santiago <diogo.santiago@b2wdigital.com>
 */
class BSeller_MassImport_Model_Import_Parts_Images
{
    /**
     * Path to images that must be imported.
     *
     * @var string
     */
    protected $path;

    /**
     * Import image type
     *
     * @var array
     */
    protected $type = ['image', 'small_image', 'thumbnail'];


    /**
     * Process data content
     *
     * @return $this
     */
    public function process(Mage_Catalog_Model_Product $product)
    {
        if (!$product->getData('images')) {
            return 'No data to process for this type content.';
        }
        
        $path = Mage::getBaseDir('media') . DS . Mage::getStoreConfig('bseller/massimport/image_path');

        $images = explode(',', $product->getData('images'));
        $errors = [];

        array_walk($images, function ($image) use ($product, $path, &$errors) {

            // This is the format image.jpg|ist|Image Label
            $parts = array_pad(explode('|', $image), 3, '');
            $parts = array_map('trim', $parts);

            $imagePath = $path . DS . $parts[0];
            $imageType = $this->getType($parts[1]);

            try {
                $product->addImageToMediaGallery($imagePath, $imageType, false, false);

                // If have label to set
                if ($parts[2]) {
                    $gallery   = $product->getMediaGallery();
                    $lastImage = array_pop($gallery['images']);
                    $lastImage['label'] = $parts[2];

                    $gallery['images'][0] = $lastImage;
                    $product->setData('media_gallery', $gallery);
                }

                $product->save();
            } catch (Exception $e) {
                $errors[] = sprintf('Could not upload image: %s  Error: %s', $parts[0], $e->getMessage());
            }
        });

        return count($errors > 0) ? implode(',', $errors) : 'Success';
    }


    /**
     * @param $min
     * @return array|null
     */
    protected function getType($min)
    {
        $types = [];

        if(strpos($min, 'i') !== false) {
            $types[] = 'image';
        }

        if(strpos($min, 's') !== false) {
            $types[] = 'small_image';
        }

        if(strpos($min, 't') !== false) {
            $types[] = 'thumbnail';
        }

        return count($types) > 0 ? $types : null;
    }
}
