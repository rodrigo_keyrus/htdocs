<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_MassImport
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Diogo Santiago <diogo.santiago@b2wdigital.com>
 */
class BSeller_MassImport_Model_Import_Parts_Grouped
{
    use BSeller_MassImport_Model_Trait;


    /**
     * Process data content
     *
     * @param $product
     * @return $this
     */
    public function process(Mage_Catalog_Model_Product $product)
    {
        if (!$product->getData('grouped.associates_sku')) {
            return 'No data to process for this type content.';
        }

        if ($product->getTypeId() != Mage_Catalog_Model_Product_Type::TYPE_GROUPED) {
            return 'Product type must be grouped for this operation';
        }

        try {
            $skus = $this->getArrayFromString($product->getData('grouped.associates_sku'));
            $ids = $this->helper()->loadIdsBySkus($skus);

            array_walk($ids, function($id) use ($product) {
                Mage::getModel('catalog/product_link_api')->assign('grouped', $product->getId(), $id,
                    ['qty' => '0.00', 'position' => '0']);
            });

            $product->save();
        } catch (Exception $exp) {
            return $exp->getMessage();
        }

        return 'Success';
    }
}
