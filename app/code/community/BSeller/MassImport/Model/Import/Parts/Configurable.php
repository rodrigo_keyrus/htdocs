<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_MassImport
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Diogo Santiago <diogo.santiago@b2wdigital.com>
 */
class BSeller_MassImport_Model_Import_Parts_Configurable
{
    use BSeller_MassImport_Model_Trait;


    /**
     * Process data content
     *
     * @param $product
     * @return $this
     */
    public function process(Mage_Catalog_Model_Product $product)
    {
        if (!$product->getData('configurable.attributes') && ! $product->getData('configurable.associates_sku')) {
            return 'No data to process for this type content.';
        }

        if ($product->getTypeId() != Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE) {
            return 'Only cofigurable products can link to other products';
        }

        try {
            $attributesCode = $this->getArrayFromString($product->getData('configurable.attributes'));

            $attributes = array_reduce($attributesCode, function($acc, $code) {
                $attribute = Mage::getModel('eav/entity_attribute')->loadByCode(4, $code);

                if ($attribute && $attribute->getId()) {
                    $acc[$attribute->getId()] = $attribute->getAttributeCode();
                }
                return $acc;
            }, []);

            $type = $product->getTypeInstance();
            $type->setUsedProductAttributeIds(array_keys($attributes));

            $existsSimpleProducts = $type->getUsedProductIds();
            $configProductsData  = count($existsSimpleProducts) ? array_flip($existsSimpleProducts) : [];

            $configAttributeData = array_map(function($attrData) {
                return  ['attribute_id' => $attrData];
            }, $type->getConfigurableAttributesAsArray());

            $productsSku = $this->getArrayFromString($product->getData('configurable.associates_sku'));
            array_walk($productsSku, function($sku) use (&$configProductsData, &$configAttributeData, $attributes) {
                if (!$product = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku)) {
                    return;
                }

                foreach ($attributes as $id => $code) {
                    $option = $this->helper()->getOptionsByAttrCode($code, $product->getData($code));

                    $productData = [
                        'label'         => $option['label'],
                        'attribute_id'  => $id,
                        'value_index'   => $option['value'],
                        'is_percent'    => '0',
                        'pricing_value' => $product->getPrice(),
                    ];

                    $configProductsData[$product->getId()][]  = $productData;
                    $configAttributeData[$id]['values'][] = $productData;
                }
            });

            $product->setConfigurableProductsData($configProductsData);
            $product->setConfigurableAttributesData($configAttributeData);
            $product->setCanSaveConfigurableAttributes(true);

            $product->save();
        } catch (Exception $exp) {
            return $exp->getMessage();
        }

        return 'Success';
    }
}
