<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_MassImport
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Diogo Santiago <diogo.santiago@b2wdigital.com>
 */
class BSeller_MassImport_Model_Template_Options_GeneralInformation
    extends BSeller_MassImport_Model_Template_Options_Container_Abstract
{
    const LABEL = 'General Information';

    const KEY = 'general_information';

    const SORT_ORDER = 1;

    
    /**
     * Get attributes for General Information
     *
     * @return array
     */
    public function interfaceNote()
    {
        return [
            'sku'  => 'Required',
            'name' => 'Required',
            'type_id' => 'Use: simple, configurable or grouped',
            'description' => 'Required',
            'short_description' => 'Required',
            'weight' => 'Required',
            'news_from_date' => '',
            'news_to_date' => '',
            'url_key' => '',
            'country_of_manufacture' => ''
        ];
    }
}
