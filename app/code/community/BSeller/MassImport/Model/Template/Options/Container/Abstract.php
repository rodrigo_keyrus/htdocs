<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_MassImport
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Diogo Santiago <diogo.santiago@b2wdigital.com>
 */
abstract class BSeller_MassImport_Model_Template_Options_Container_Abstract
{
    const LABEL = '';

    const KEY = '';

    const SORT_ORDER = 99;

    
    /**
     * Get attributes for content Type
     *
     * @return array
     */
    public function interfaceNote ()
    {
        return [];
    }


    public function label ()
    {
        return static::LABEL;
    }


    public function key ()
    {
        return static::KEY;
    }


    public function order()
    {
        return static::SORT_ORDER;
    }
}
