<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_MassImport
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Diogo Santiago <diogo.santiago@b2wdigital.com>
 */
class BSeller_MassImport_Model_Template_Options_MetaInformation
    extends BSeller_MassImport_Model_Template_Options_Container_Abstract
{
    const LABEL = 'Meta Information';

    const KEY = 'meta_information';

    const SORT_ORDER = 9;

    
    /**
     * Get attributes for Meta Information
     *
     * @return array
     */
    public function interfaceNote()
    {
        return [
            'meta_title' => '',
            'meta_keyword' => '',
            'meta_description' => ''
        ];
    }
}
