<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_MassImport
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Diogo Santiago <diogo.santiago@b2wdigital.com>
 */
class BSeller_MassImport_Model_Template_Options_StockAdvanced
    extends BSeller_MassImport_Model_Template_Options_Container_Abstract
{
    const LABEL = 'Stock (Advanced)';

    const KEY = 'stock_advanced';

    const SORT_ORDER = 11;

    
    /**
     * Get Stock Attributes (Advanced)
     *
     * @return array
     */
    public function interfaceNote()
    {
        return [
            'stock.use_config_min_qty' => 'Use Config Settings for Qty for Item\'s Status to Become Out of Stock',
            'stock.min_qty' => 'Qty for Item\'s Status to Become Out of Stock',
            'stock.use_config_min_sale_qty' => 'Use Config Settings for Minimum Qty Allowed in Shopping Cart',
            'stock.min_sale_qty' => 'Minimum Qty Allowed in Shopping Cart',
            'stock.use_config_max_sale_qty' => 'Use Config Settings for Maximum Qty Allowed in Shopping Cart',
            'stock.max_sale_qty' => 'Maximum Qty Allowed in Shopping Cart',
            'stock.is_decimal_divided' => 'Can Be Divided into Multiple Boxes for Shipping',
            'stock.use_config_backorders' => 'Use Config Settings for Backorders',
            'stock.backorders' => 'Backorders',
            'stock.use_config_notify_stock_qty' => 'Use Config Settings for Notify for Quantity Below',
            'stock.notify_stock_qty' => 'Notify for Quantity Below',
            'stock.qty_increments' => 'Qty Increments',
            'stock.use_config_qty_increments' => 'Use Config Settings for Enable Qty Increments',
            'stock.enable_qty_increments' => 'Enable Qty Increments',
            'stock.use_config_enable_qty_inc' => 'Use Config Settings for Enable Qty Increments'
        ];
    }
}
