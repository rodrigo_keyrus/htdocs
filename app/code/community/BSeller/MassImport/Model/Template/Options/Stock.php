<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_MassImport
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Diogo Santiago <diogo.santiago@b2wdigital.com>
 */
class BSeller_MassImport_Model_Template_Options_Stock
    extends BSeller_MassImport_Model_Template_Options_Container_Abstract
{
    const LABEL = 'Stock (Basic)';

    const KEY = 'stock';

    const SORT_ORDER = 10;


    /**
     * Get attributes for content Type- Get Stock Attributes (Basic)
     *
     * @return array
     */
    public function interfaceNote()
    {
        return [
            'stock.manage_stock' => 'Required. Manage Stock (Ignored if "use_config_manage_stock" is enabled).',
            'stock.use_config_manage_stock' => 'Use Config Settings for Manage Stock (Use 0 or 1)',
            'stock.qty' => 'Quantity In Stock',
            'stock.is_qty_decimal' => 'Qty Uses Decimals'
        ];
    }
}
