<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_MassImport
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Diogo Santiago <diogo.santiago@b2wdigital.com>
 */
class BSeller_MassImport_Model_Template_Options_Images
    extends BSeller_MassImport_Model_Template_Options_Container_Abstract
{
    const LABEL = 'Import Images';

    const KEY = 'images';

    const SORT_ORDER = 6;

    
    /**
     * Get attributes for content Type
     *
     * @return array
     */
    public function interfaceNote()
    {
        $msg = 'Images must respect the following example: </br>' .
        '<b>image_path|image_types|label.</b>. </br>' .
        'Images files must be in the specified folder in the modules admin configuration. <br/>' .
        'The types are i for "image", s for "small_image" and t for "thumbnail". <br/>' .
        'The labels can be ommited. Ex: <br/><br/>' .
        '<b>img1.jpg|i|My image 1</b> <br/>' .
        '<b>img2.jpg|ist</b>';

        return [
            'images' => $msg
        ];
    }
}
