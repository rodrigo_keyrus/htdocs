<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_MassImport
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Diogo Santiago <diogo.santiago@b2wdigital.com>
 */
class BSeller_MassImport_Model_Template_Options_Availability 
    extends BSeller_MassImport_Model_Template_Options_Container_Abstract
{
    const LABEL = 'Availability';

    const KEY = 'availability';

    const SORT_ORDER = 2;



    /**
     * Get attributes for content Type
     *
     * @return array
     */
    public function interfaceNote()
    {
        return [
            'status' => '(1: Enabled, 2: Disabled)',
            'visibility' => '(1: Not Visible, 2: Catalog, 3: Search, 4: Catalog/Search)',
            'stock.is_in_stock' => 'Required. Stock Availability',
            'stock.qty' => 'Quantity In Stock'
        ];
    }
}
