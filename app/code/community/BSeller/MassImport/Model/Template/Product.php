<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_MassImport
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Diogo Santiago <diogo.santiago@b2wdigital.com>
 */
class BSeller_MassImport_Model_Template_Product
{
    use BSeller_MassImport_Model_Trait;

    /**
     * Get attributes list
     *
     * @param array $param
     *
     * @return Varien_Object
     */
    public function getTemplateCsv($param = [])
    {
        $optionsAvailable = $this->optionsAvailable(true);
        $returnMethod = [];

        array_walk($optionsAvailable, function($option, $optionId)  use (&$returnMethod, $param){
            if (isset($param['options'][$optionId])) {
                $returnMethod += $option->interfaceNote();
            }
        });

        if ($param['attribute_set'] !== 'none') {
            $returnMethod['attribute_set'] = $param['attribute_set'];
            $result = $this->helper()->getAttributesByAttributeSet($param['attribute_set']);
            $returnMethod += array_flip($result);
        }

        $setName = $returnMethod['attribute_set'];
        $returnMethod = array_map(function() {}, $returnMethod);
        $returnMethod['attribute_set'] = $setName;

        return new Varien_Object($returnMethod);
    }


    /**
     * Get Product Interface Options
     *
     * @param bool $sorted
     * @return array
     */
    public function optionsAvailable($sorted = false)
    {
        $options = scandir(Mage::getModuleDir('', 'BSeller_MassImport') . '/Model/Template/Options/');
        $newOptions = [];

        array_walk($options, function($value) use (&$newOptions) {
            if (!in_array($value, ['.', '..', 'Container'])) {
                $modelName = str_replace('.php', '', $value); // Remove php extension
                $modelName[0] = strtolower($modelName[0]);
                $model = Mage::getModel("bseller_massimport/template_options_{$modelName}");
                $newOptions[$model->key()] = $model;
            }
        });

        if ($sorted) {
            $newOptions = $this->sortOptions($newOptions);
        }
        return $newOptions;
    }


    /**
     * Sort available options
     *
     * @param array $options
     * @return array
     */
    protected function sortOptions($options)
    {
        $size = count($options);
        $sortedOptions = [];

        for ($i = 0; $i < $size; $i++) {
            $minKey = array_reduce($options, function ($acc, $value) {
                return ($value->order() < $acc[1]) ? [$value->key(), $value->order()] : $acc;
            }, ['', 99]);

            $minKey = $minKey[0];
            $sortedOptions[$minKey] = $options[$minKey];
            unset($options[$minKey]);
        }

        return $sortedOptions;
    }
}
