<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category BSeller
 * @package  BSeller_BuscapeEbit
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author Diogo Baracho <diogo.baracho@e-smart.com.br>
 */

class BSeller_BuscapeEbit_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function isAvailable($store = null)
    {
        $accountId = Mage::getStoreConfig(BSeller_BuscapeEbit_Model_Config::XML_PATH_ACCOUNT, $store);
        return $accountId && Mage::getStoreConfigFlag(BSeller_BuscapeEbit_Model_Config::XML_PATH_ACTIVE, $store);
    }
    
    public function isSealAvailable($store = null)
    {
        $accountId = Mage::getStoreConfig(BSeller_BuscapeEbit_Model_Config::XML_PATH_STORE_NAME, $store);
        return $accountId && Mage::getStoreConfigFlag(BSeller_BuscapeEbit_Model_Config::XML_PATH_ACTIVE, $store);
    }

    public function getEbitId()
    {
        return Mage::getStoreConfig('sales/ebit/ebit_id', Mage::app()->getStore());
    }

    public function getBuscapeId()
    {
        return Mage::getStoreConfig('sales/ebit/buscape_id', Mage::app()->getStore());
    }

    public function useLightBox($store = null)
    {
        $useLightBox = (bool)Mage::getStoreConfig(
            BSeller_BuscapeEbit_Model_Config::XML_PATH_BANNER_USE_LIGHTBOX,
            $store
        );
        return $useLightBox && Mage::getStoreConfigFlag(BSeller_BuscapeEbit_Model_Config::XML_PATH_ACTIVE, $store);
    }
}