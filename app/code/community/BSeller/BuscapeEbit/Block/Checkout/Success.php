<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category BSeller
 * @package  BSeller_BuscapeEbit
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author Diogo Baracho <diogo.baracho@e-smart.com.br>
 */

class BSeller_BuscapeEbit_Block_Checkout_Success extends Mage_Core_Block_Template
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('bseller/buscape/ebit/success.phtml');
    }

    /**
     * @return Mage_Sales_Model_Order
     */
    public function getOrder()
    {
        return Mage::registry('current_order');
    }

    public function getDeliveryTime()
    {
        /** @var Mage_Sales_Model_Quote $quote */
        $quote = Mage::getModel('sales/quote')->load($this->getOrder()->getQuoteId());
        $str = str_replace(' ', '', $quote->getShippingAddress()->getShippingDescription());

        if (strstr($str, 'até')) {
            $pos0 = strpos($str, 'até');
            $pos1 = strpos($str, 'dia');
            return substr($str, $pos0 + 4, $pos1 - $pos0 - 4);
        }

        return 0;
    }

    public function getPaymentType()
    {
        $paymentMethod = $this->getOrder()->getPayment()->getMethod();

        if ($paymentMethod == 'mundipagg_boleto') {
            return '08';
        }

        if ($paymentMethod == 'mundipagg_creditcard') {
            return '05';
        }

        return '14';
    }

    public function getCardFlag()
    {
        $brandMap = [
            'MC' => 5,
            'VI' => 6,
            'AE' => 1,
            'DI' => 3,
            'HI' => 4,
            'EL' => 8
        ];

        $paymentBrand = $this->getOrder()->getPayment()->getCcType();

        if (!isset($brandMap[$paymentBrand])) {
            return 7;
        }

        return $brandMap[$paymentBrand];
    }

    public function getPlatform()
    {
        return '';
    }

}