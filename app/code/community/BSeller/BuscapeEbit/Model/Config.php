<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category BSeller
 * @package  BSeller_BuscapeEbit
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author Diogo Baracho <diogo.baracho@e-smart.com.br>
 */

class BSeller_BuscapeEbit_Model_Config extends Varien_Object
{
    const XML_PATH              = 'sales/ebit/';
    
    const XML_PATH_ACTIVE       = 'sales/ebit/active';
    
    const XML_PATH_ACCOUNT      = 'sales/ebit/ebit_id';
    
    const XML_PATH_STORE_NAME   = 'sales/ebit/store';

    const XML_PATH_BANNER_USE_LIGHTBOX = 'sales/ebit/checkout_banner_is_lightbox';

    protected $_urlAction = "https://www.ebitempresa.com.br/bitrate/pesquisa1.asp";
    
    protected $_srcAction = "https://www.ebitempresa.com.br/bitrate/banners/b%s.gif";
    
    protected $_config = array();
    
    public function getConfigData($key, $storeId = null)
    {
        if (!isset($this->_config[$key][$storeId])) {
            $value = Mage::getStoreConfig(self::XML_PATH . $key, $storeId);
            $this->_config[$key][$storeId] = $value;
        }
        return $this->_config[$key][$storeId];
    }
    
    public function getAccount($store = null)
    {
        if (!$this->hasData('ebit_account')) {
            $this->setData('ebit_account', $this->getConfigData('account', $store));
        }
        
        return $this->getData('ebit_account');
    }
    
    public function getStore($store = null)
    {
        if (!$this->hasData('ebit_store')) {
            $this->setData('ebit_store', $this->getConfigData('store', $store));
        }
        
        return $this->getData('ebit_store');
    }
    
    public function getUrlAction()
    {
        if (!$this->hasData('ebit_url_action')) {
            $this->setData('ebit_url_action', $this->_urlAction);
        }
        
        return $this->getData('ebit_url_action');
    }
    
    public function getSrc()
    {
        if (!$this->hasData('ebit_src')) {
            $this->setData('ebit_src', sprintf($this->_srcAction, '1' . $this->getAccount() . '5'));
        }
        
        return $this->getData('ebit_src');
    }
}