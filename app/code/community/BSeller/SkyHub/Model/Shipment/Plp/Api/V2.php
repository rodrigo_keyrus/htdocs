<?php

/**
 * BIT Tools Platform | B2W - Companhia Digital
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category  BitTools
 * @package   BitTools_ModuleName
 *
 * @copyright Copyright (c) 2018 B2W Digital - BIT Tools.
 *
 * @author    Julio Reis <julio.reis@b2wdigital.com>
 */
class BSeller_SkyHub_Model_Shipment_Plp_Api_V2 extends BSeller_SkyHub_Model_Shipment_Plp_Api
{
}