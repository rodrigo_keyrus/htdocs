<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category BSeller
 * @package  BSeller_Core
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author Diogo Baracho <diogo.baracho@e-smart.com.br>
 */

class BSeller_Serpini_Model_Report
    extends Serpini_Sqlreport_Model_Report
{
    protected $_helper = null;


    public function exportReport($codeReport)
    {
        $id             = $codeReport;

        $helper         = $this->_getHelper();

        $csvSeparator   = $this->setupmanager->getValue("exp_col_delimiter");

        $tmpFilename    = $helper->getFileName($id).'.tmp';

        $filename       = $helper->getFileName($id).'.csv';

        $dir            = $helper->getDir().$helper->getFilePath();

        if (file_exists($dir.$tmpFilename)) {
            return 'running';
        }

        $this->id = $id;
        $this->combosSeted=false;




        /** @var Serpini_Sqlreport_Model_Setup setupmanager */
        $this->setupmanager= Mage::getModel('sqlreport/setup');

        try{

            $coreResource = Mage::getSingleton('core/resource');

            $connection = $coreResource->getConnection('core_read');

            $select = $connection->select()
                ->from(
                    array(
                        'a' => $this->gtn('sqlrpt_report')
                    ),
                    array(
                        'a.title', 'a.report_sql', 'a.group_id'
                    )
                )
                ->join(
                    array('b' => $this->gtn('sqlrpt_group')
                    ),
                    'a.group_id = b.group_id',
                    array(
                        'group_des' => 'b.description', 'group_orden' => 'b.orden')
                )
                ->where('report_id = ?', $id);

            $readResult = $connection->fetchAll($select);

            $this->sql = '';

            foreach ($readResult as $fila) {
                $this->title = $fila['title'];
                $this->sql = $fila['report_sql'];
                $this->group->loadMe($fila['group_id']);
            }

            $this->loadReport($this->id);

            $comboValues = Mage::app()->getRequest()->getParam('filter');
            if (!empty($comboValues)) {
                $this->setComboValues($comboValues);
                $data = array();
                // GROUP
                $data[0] = array("object_type" => "group",
                    "group_id" => $this->getGroup()->getId(),
                    "description" =>$this->getGroup()->getDescription(),
                    "orden" => $this->getGroup()->getOrden());

                // FILTERS
                foreach ($this->getComboList() as $combo) {
                    array_push($data, $combo->toArrayex());
                }
            }


            $connection = Mage::getSingleton('core/resource')->getConnection('reports');

            $this->sql = $this->_setQueryParams($this->sql);

            $fp = fopen($helper->getDir().$helper->getFilePath().$tmpFilename, 'w');
            fclose($fp);

            $increment  = 500;
            $total      = 100000;
            $query      = '';


            for ($x=0; $x < $total; $x = ($x + $increment)) {

                $limit  = " LIMIT $increment";
                if ($x > 0) {
                    $offset = " OFFSET $x";
                    $limit .= $offset;
                } else {


                    if ($this->setupmanager->getValue("svn_print_header")) {

                        $readResult   = $connection->query($this->sql.' LIMIT 1');
                        $this->result = $readResult->fetchAll();
                        $this->resultSum = array();
                        foreach ($this->result as $fila) {
                            foreach ($fila as $key => $value) {
                                array_push($this->columnsName, $key);
                                array_push($this->resultSum, 0);
                            }
                            break 1;
                        }

                        $result = "";

                        $isFirst = true;

                        foreach ($this->getColumnsName() as $_column) {
                            if ($isFirst) {
                                $result.= $_column;
                                $isFirst = false;
                            } else {
                                $result.= $csvSeparator.$_column;
                            }
                        }

                        if (!empty($result)) {
                            $result.="\r\n";
                            file_put_contents($dir.$tmpFilename, $result);
                        }
                    }

                }

                //has any limit at the end of query
                $len = strlen($this->sql);
                $pos = strpos(strtolower($this->sql), 'limit');
                if (empty($pos)
                    || ($len - 10) < $pos
                ) {
                    if ($query == $this->sql.$limit) {
                        break;
                    }
                    $query = $this->sql.$limit;
                    $readResult = $connection->query($query);
                } else {
                    if ($query == $this->sql) {
                        break;
                    }
                    $query = $this->sql;
                    $readResult = $connection->query($this->sql);
                }

                $this->result    = $readResult->fetchAll();

                if (empty($this->result)) {
                    break;
                }

                foreach ($this->result as $linha) {
                    $fp = fopen($dir.$tmpFilename, 'a+');
                    fputcsv($fp, $linha, $csvSeparator);
                    fclose($fp);
                }
            }

        }catch (Exception  $e){
            Mage::throwException($e);
        }

        rename($dir.$tmpFilename, $dir.$filename);

        return $filename;
    }

    protected function _getHelper()
    {
        if ($this->_helper) {
            return $this->_helper;
        }
        return $this->_helper = Mage::helper('bseller_serpini');
    }


    protected function _setQueryParams($sql)
    {
        $this->sql = $sql;
        $salida = $this->sql;
        $salida = str_replace(
            $this->setupmanager->getValue('prefix_table'),
            Mage::getConfig()->getTablePrefix(),
            $salida
        );

        foreach ($this->getComboList() as $combo) {
            if ($combo->getType()=="date") {

                $valor = $combo->getValueSet();
                $valor = @$valor[0];

                $salida = str_replace(
                    $this->setupmanager->getValue('prefix_parameter').$combo->getParameter(),
                    "STR_TO_DATE('".$valor."','".$this->setupmanager->getValue('date_mask')."')",
                    $salida
                );

            } else if ($combo->getType()=="evaluated") {
                $valor    = "";
                $sentence = $combo->getAtribute("sql");
                //TODO escapar todo
                try{
                    eval("\$valor = ".$sentence.";");
                    $salida = str_replace(
                        $this->setupmanager->getValue('prefix_parameter').$combo->getParameter(),
                        $valor,
                        $salida
                    );
                }catch (Exception  $err){
                    echo "error";
                }
            } else if ($combo->getType()=="text") {

                $valor  = $combo->getValueSet();
                $salida = str_replace(
                    $this->setupmanager->getValue('prefix_parameter').$combo->getParameter(),
                    $valor,
                    $salida
                );
            } else {
                $valor = "";
                if ("checkbox-multiple"==$combo->getAtribute("selectType")) {
                    $valor ="(";
                    foreach ($combo->getValueSet() as $valueSet) {
                        $valor.="'".$valueSet."',";
                    }
                    $valor = substr($valor, 0, strlen($valor)-1);
                    $valor .=")";
                } else {
                    $valor = $combo->getValueSet();
                    $valor=@$valor[0];
                }
                $salida = str_replace(
                    $this->setupmanager->getValue('prefix_parameter').$combo->getParameter(),
                    $valor,
                    $salida
                );
            }
        }

        //extra replaces if necessary
        $storeId = Mage::app()->getStore()->getStoreId();

        $salida = str_replace(
            ':STORE_ID',
            'ALL',
            $salida
        );

        /** @var Mage_Core_Model_Date $now */
//        $now = Mage::getModel('core/date')->date();
//
//        $datetime = new DateTime($now);
//        $datetime->modify('-120 day');
//        $startAt = $datetime->format('Y-m-d');
//
//        $salida = str_replace(
//            ':STARTDATE',
//            "'".$startAt."'",
//            $salida
//        );
//
//        $salida = str_replace(
//            ':ENDDATE',
//            "'".$now."'",
//            $salida
//        );

        return $salida;
    }
}