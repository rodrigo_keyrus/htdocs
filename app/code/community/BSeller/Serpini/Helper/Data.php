<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category BSeller
 * @package  BSeller_Core
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author Diogo Baracho <diogo.baracho@e-smart.com.br>
 */ 
class BSeller_Serpini_Helper_Data
    extends Mage_Core_Helper_Abstract
{

    const REPORT_MKDIR_CHMOD = 775;

    protected $_dir = null;

    protected $_filePath = null;


    public function isActive()
    {
        return (bool)Mage::getStoreConfig('reports/bseller_serpini/active');
    }

    public function showOnlyHisBtn()
    {
        return (bool)Mage::getStoreConfig('reports/bseller_serpini/show_only_his_btn');
    }

    public function customExportDir()
    {
        return Mage::getStoreConfig('reports/bseller_serpini/custom_export_dir');
    }

    public function downloadListExtensions()
    {
        return Mage::getStoreConfig('reports/bseller_serpini/download_list_extensions');
    }

    public function customExportDirChmod()
    {
        return Mage::getStoreConfig('reports/bseller_serpini/custom_export_dir_chmod');
    }


    public function getDir()
    {
        if ($this->_dir) {
            return $this->_dir;
        }

        $this->_dir = Mage::getBaseDir();

        return $this->_dir;
    }

    public function getFilePath()
    {
        if ($this->_filePath) {
            return $this->_filePath;
        }

        $dir = $this->getDir();

        $this->_filePath = $this->customExportDir();

        if (empty($this->_filePath)) {
            $this->_filePath = DIRECTORY_SEPARATOR.'var'. DIRECTORY_SEPARATOR.'report'.DIRECTORY_SEPARATOR;
        }

        if (!is_dir($dir . $this->_filePath)) {
            $chmod = $this->customExportDirChmod();
            if (empty($chmod)) {
                $chmod = self::REPORT_MKDIR_CHMOD;
            }
            mkdir($dir . $this->_filePath, $chmod, true);
        }

        return $this->_filePath;
    }

    public function compress($file, $outputName, $fileName = null)
    {
        try {
            $zipName = $outputName . '.zip';
            $zip = new ZipArchive();
            $dir = $this->getDir() . $this->getFilePath();
            $res = $zip->open($dir . $zipName, ZIPARCHIVE::CREATE | ZIPARCHIVE::OVERWRITE);
            if ($res === true) {
                $zip->addFile($file, $fileName);
                $zip->close();

                return $zipName;
            } else {
                Mage::throwException('[ERROR: bseller_serpini_helper_data] Sorry, can not create zip archive file');
            }
        }catch (Exception $e) {
            Mage::throwException($e);
        }
    }

    public function getFileSize($file)
    {
        return filesize($file);
    }

    public function fileUpdatedAtInfo($file)
    {
        if (!file_exists($file)) {
            return ' - ';
        }

        return Mage::getModel('core/date')->date("F d Y H:i:s.", filemtime($file));
    }

    public function humanFileSize($size, $precision = 2)
    {
        $units = array('Bytes','kB','MB','GB','TB','PB','EB','ZB','YB');
        $step = 1024;
        $i = 0;
        while (($size / $step) > 0.9) {
            $size = $size / $step;
            $i++;
        }
        return round($size, $precision).' '.$units[$i];
    }

    public function getFileName($codeReport)
    {
        return 'report_'.trim(strtolower($codeReport));
    }
}