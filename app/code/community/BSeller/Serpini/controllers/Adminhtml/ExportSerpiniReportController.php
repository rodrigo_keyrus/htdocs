<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category BSeller
 * @package  BSeller_Core
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author Diogo Baracho <diogo.baracho@e-smart.com.br>
 */

class BSeller_Serpini_Adminhtml_ExportSerpiniReportController
    extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('bseller_serpini/downloadReport'));
        $this->renderLayout();
    }

    public function exportAction()
    {
        $helper = Mage::helper('bseller_serpini');

        $return = array();

        if ($this->getRequest()->isAjax()) {

            $codeReport = Mage::app()->getRequest()->getParam('codeReport');

            /** @var BSeller_Serpini_Model_Report $report */
            $report = Mage::getModel('bseller_serpini/report');

            $fileName = $report->exportReport($codeReport);

            if ($fileName == 'running') {
                $return['exportedHTML'] = $helper->__('This report running, please check report download list.');
            } else {

                $outputZipName = str_replace('.csv', '', $fileName);

                $zipFile = $helper->compress(
                    $helper->getDir() . $helper->getFilePath() . $fileName,
                    $outputZipName,
                    $fileName
                );

                unlink($helper->getDir() . $helper->getFilePath() . $fileName);

                $fileInfo = $helper->fileUpdatedAtInfo($helper->getDir() . $helper->getFilePath() . $zipFile);

                $link = Mage::helper("adminhtml")->getUrl("*/downloadReport/index") . '?file=' . $zipFile;


                $return['exportFileName'] = $link;
                $return['exportedHTML'] = '<p>' . $helper->__('Report') . ': <strong>' . $codeReport . '</strong>: <span class="button"><a href="' . $link . '" target="_blank">'. $helper->__('download here').'</a></span></p>';
                $return['exportedHTML'] .= '<p>' . $helper->__('Created At') . ': <em>' . $fileInfo . '</em></p>';
                $return['exportedHTML'] .= '<p>'.$helper->__('File Size').': <em>' . $helper->humanFileSize($helper->getFileSize($helper->getDir() . $helper->getFilePath() . $zipFile)) . '</em></p>';
            }
        }

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($return));
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('report/bseller_serpini');
    }
}