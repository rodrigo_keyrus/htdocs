<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category BSeller
 * @package  BSeller_Core
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author Diogo Baracho <diogo.baracho@e-smart.com.br>
 */

class BSeller_Serpini_Adminhtml_DownloadReportController
    extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $deleteFile = $this->getRequest()->getParam('deleteFile');
        if (!empty($deleteFile)) {
            $this->deleteFileAction($deleteFile);
            $this->getResponse()->setRedirect(Mage::getUrl('*/index', array('_current'=>true)));
            return;
        }

        $file = $this->getRequest()->getParam('file');
        if (!empty($file)) {
            $this->download($file);
            return;
        }

        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('bseller_serpini/downloadReport'));
        $this->renderLayout();
    }

    public function download($file)
    {
        $helper = Mage::helper('bseller_serpini');

        $file = $helper->getDir().$helper->getFilePath().$file;

        if (file_exists($file)) {
            return $this->_download($file);
        }

        return $this->_fileNotFound();
    }

    protected function _download($file)
    {
        header("Pragma: public", true);
        header("Expires: 0"); // set expiration time
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment; filename=".basename($file));
        header("Content-Transfer-Encoding: binary");
        header("Content-Length: ".filesize($file));
        @readfile($file);
        return;
    }

    protected function _fileNotFound()
    {
        $this->getResponse()->setHeader('HTTP/1.1', '404 Not Found');
        $this->getResponse()->setHeader('Status', '404 File not found');
        $this->loadLayout();
        $this->renderLayout();
        return;
    }

    public function deleteFileAction($file)
    {
        $helper = Mage::helper('bseller_serpini');
        $file = $helper->getDir().$helper->getFilePath().$file;
        if (!file_exists($file)) {
            return false;
        }
        return unlink($file);
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('report/bseller_serpini');
    }
}