<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Neemu
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_Neemu_Model_Job extends BSeller_Resque_Model_Job_Abstract
{
    /**
     * Delete old file
     * Insert header content
     *
     * @return $this
     */
    public function setUp()
    {
        $args = $this->getArgs();

        if (!$args['is_first']) {
            return $this;
        }

        /**
         * Set current store before file content
         */
        Mage::app()->setCurrentStore($args['store_id']);

        /** @var BSeller_Neemu_Model_File $file */
        $file = Mage::getModel('bseller_neemu/file');
        $file->setPrefix($args['type'] . '_temp');
        $file->delete();
        $file->insert('header');

        return $this;
    }

    /**
     * Run job
     *
     * @return $this
     */
    public function perform()
    {
        $args = $this->getArgs();

        /**
         * Set current store before file content
         */
        Mage::app()->setCurrentStore($args['store_id']);

        /** @var string $type */
        $type = $args['type'];

        /** @var BSeller_Neemu_Model_Product_Abstract $instance */
        $instance = Mage::getModel('bseller_neemu/product_' . $type);

        /** @var BSeller_Neemu_Model_File $file */
        $file = Mage::getModel('bseller_neemu/file');
        $file->setPrefix($type . '_temp');
        $file->create($instance, $args['product_ids']);

        return $this;
    }

    /**
     * Insert footer content
     *
     * @return $this
     */
    public function tearDown()
    {
        $args = $this->getArgs();

        if (!$args['is_last']) {
            return $this;
        }

        /**
         * Set current store before file content
         */
        Mage::app()->setCurrentStore($args['store_id']);

        /** @var BSeller_Neemu_Model_File $file */
        $file = Mage::getModel('bseller_neemu/file');
        $file->setPrefix($args['type'] . '_temp');
        $file->insert('footer');

        /**
         * Rename temporally file to final name
         */
        $io = new Varien_Io_File();
        $io->mv(
            $file->getFilename(),
            $file->setPrefix($args['type'])->getFilename()
        );

        return $this;
    }
}
