<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Neemu
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_Neemu_Model_Observer
{
    /**
     * Enqueue partial load of products
     *
     * @return $this
     */
    public function enqueuePartial()
    {
        if ($this->isProcessRunning(BSeller_Neemu_Helper_Data::PRODUCT_PARTIAL_TYPE)) {
            $this->logBackTrace(' Job(' . BSeller_Neemu_Helper_Data::PRODUCT_PARTIAL_TYPE . ') already is running...');
            return;
        }

        $this->logBackTrace('Starting job (' . BSeller_Neemu_Helper_Data::PRODUCT_PARTIAL_TYPE . ')');

        if (!Mage::helper('bseller_neemu')->isPartialEnabled()) {
            return $this;
        }

        $this->enqueue(
            BSeller_Neemu_Helper_Data::PRODUCT_PARTIAL_TYPE,
            Mage::app()->getStore(1)
        );

        $this->logBackTrace('End job (' . BSeller_Neemu_Helper_Data::PRODUCT_PARTIAL_TYPE . ')');

        return $this;
    }


    /**
     * Enqueue full load of products
     *
     * @return $this
     */
    public function enqueueFull()
    {
        if ($this->isProcessRunning(BSeller_Neemu_Helper_Data::PRODUCT_FULL_TYPE)) {
            $this->logBackTrace(' Job(' . BSeller_Neemu_Helper_Data::PRODUCT_FULL_TYPE . ') already is running...');
            return;
        }

        $this->logBackTrace('Starting job (' . BSeller_Neemu_Helper_Data::PRODUCT_FULL_TYPE . ')');

        if (!Mage::helper('bseller_neemu')->isFullEnabled()) {
            return $this;
        }

        /** @var Mage_Core_Model_Store $store */
        $this->enqueue(
            BSeller_Neemu_Helper_Data::PRODUCT_FULL_TYPE,
            Mage::app()->getStore(1)
        );

        $this->logBackTrace('End job (' . BSeller_Neemu_Helper_Data::PRODUCT_FULL_TYPE . ')');

        return $this;
    }


    /**
     * @param string $type
     * @param Mage_Core_Model_Store $store
     * @return $this
     */
    protected function enqueue($type, $store)
    {
        /** @var BSeller_Neemu_Model_Product_Abstract $instance */
        $instance = Mage::getModel('bseller_neemu/product_' . $type);

        $args['store_id']    = $store->getId();
        $args['product_ids'] = $instance->getAvailableIds();
        $args['type']        = $type;

        $this->createFlatItemsToXml($args);

        return $this;
    }


    /**
     * Create items in Neemu flat table
     *
     * @param array $args
     *
     * @return $this
     */
    protected function createFlatItemsToXml($args)
    {
        /** @var BSeller_Neemu_Model_Product_Abstract $instance */
        $instance = Mage::getModel('bseller_neemu/product_' . $args['type']);

        $template   = new Varien_Filter_Template();
        $content    = $this->neemuFile()->getContent('item');
        $variables  = $this->getTemplateVariables($template, $content);

        /** @var Mage_Catalog_Model_Resource_Product_Collection $products */
        $products   = $instance->getAvailableCollection($args['product_ids'], $variables, 1);

        $this->logBackTrace('Available ids: ('. count($args['product_ids']) .')');

        if (!$products->getSize()) {
            return $this;
        }

        try {

            $writeConnection = $this->_getWriteConnection();
            $writeConnection->beginTransaction();

            foreach ($products as $product) {

                $data = array(
                    'sku'           => $product->getSku(),
                    'type'          => $args['type'],
                    'data'          => serialize($product->getData()),
                );

                $this->_getResourceModel()->insertItem($data);
            }

            $writeConnection->commit();

        } catch (Mage_Exception $e) {
            $this->logBackTrace('Error adding items to Neemu flat items table.');
            Mage::logException($e);
        }

        return $this;
    }


    /**
     * @return BSeller_Neemu_Model_File
     */
    public function neemuFile()
    {
        return Mage::getModel('bseller_neemu/file');
    }


    /**
     * @param $template
     * @param $content
     *
     * @return array
     */
    protected function getTemplateVariables($template, $content)
    {
        if (!preg_match_all($template::CONSTRUCTION_PATTERN, $content, $constructions, PREG_SET_ORDER)) {
            return [];
        }

        $variables = [];

        foreach ($constructions as $construction) {
            if (!isset($construction[2])) {
                continue;
            }

            $vars = explode('.', trim($construction[2]));

            if (count($vars) < 2) {
                continue;
            }

            list(, $var) = $vars;

            if (!ctype_lower($var) && !strpos($var, '_')) {
                $var = str_replace(['get', 'set', 'uns', 'has'], '', $var);
                $var = preg_replace('/(?<!^)([A-Z])/', '_\\1', $var);
                $var = preg_replace('/([^a-zA-Z0-9_\s])/', '', $var);;
                $var = strtolower($var);
            }

            $variables[] = $var;
        }

        $variables = array_filter($variables);
        $variables = array_unique($variables);

        return $variables;
    }


    /**
     *  Run Job Full
     */
    public function createXmlFull()
    {
        $this->logBackTrace('Start createXmlFull...');

        $this->setUp('full');
        $this->perform('full');
        $this->tearDown('full');

        $this->logBackTrace('End createXmlFull...');
    }


    /**
     *  Run Job Partial
     */
    public function createXmlPartial()
    {
        $this->logBackTrace('Start createXmlPartial...');

        $this->setUp('partial');
        $this->perform('partial');
        $this->tearDown('partial');

        $this->logBackTrace('End createXmlPartial...');
    }


    /**
     * Delete old file
     * Insert header content
     *
     * @return $this
     */
    public function setUp($type)
    {
        /** @var BSeller_Neemu_Model_File $file */
        $file = Mage::getModel('bseller_neemu/file');
        $file->setPrefix($type . '_temp');
        $file->delete();

        return $this;
    }


    /**
     * Run job
     *
     * @return $this
     */
    public function perform($type)
    {
        /** @var BSeller_Neemu_Model_Product_Abstract $instance */
        $instance = Mage::getModel('bseller_neemu/product_' . $type);

        /** @var BSeller_Neemu_Model_File $file */
        $file = Mage::getModel('bseller_neemu/file');
        $file->setPrefix($type . '_temp');
        $file->create($instance, $type);

        return $this;
    }


    /**
     * Insert footer content
     *
     * @return $this
     */
    public function tearDown($type)
    {
        $directory = Mage::getBaseDir('media') . DS . 'neemu';

        /** @var BSeller_Neemu_Model_File $file */
        $file = Mage::getModel('bseller_neemu/file');
        $file->setPrefix($type . '_temp');

        /**
         * Rename temporally file to final name
         */
        $io = new Varien_Io_File();
        $io->mv(
            $directory . DS . 'store_default_' . $type . '_temp.xml',
            $directory . DS . 'store_default_' . $type . '.xml'
        );

        return $this;
    }


    /**
     * @todo add log to xdebug
     * @param $message
     *
     * @return $this
     */
    public function logBackTrace($message)
    {
        Mage::log($message, null, 'xdebug_neemu_job.log');

        return $this;
    }


    /**
     * @return Esmart_Neemu_Model_Resource_Flat
     */
    protected function _getResourceModel()
    {
        return Mage::getResourceSingleton('esmart_neemu/flat');
    }


    /**
     * @return Varien_Db_Adapter_Interface
     */
    protected function _getWriteConnection()
    {
        return Mage::getSingleton('core/resource')->getConnection('core_write');
    }


    /**
     * @param string $type
     *
     * @return bool
     */
    public function isProcessRunning($type)
    {
        if (count($this->_getResourceModel()->getFlatItems($type))) {
            return true;
        }

        return false;
    }
}
