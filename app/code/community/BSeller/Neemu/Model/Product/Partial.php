<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Neemu
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_Neemu_Model_Product_Partial extends BSeller_Neemu_Model_Product_Abstract
{
    /**
     * Return available product ids
     *
     * @return array
     */
    public function getAvailableIds()
    {
        $adapter = $this->getReadAdapter();

        $queryNew    = $this->getQuery('e.created_at');
        $queryUpdate = $this->getQuery('e.updated_at');

        $select = $adapter->select()->union([$queryNew, $queryUpdate]);

        /**
         * Dispatch event to customize in another module
         */
        Mage::dispatchEvent($this->_eventPrefix . '_available_partial_ids', ['object' => $this, 'select' => $select]);

        return $adapter->fetchCol($select);
    }

    /**
     * Return query select
     *
     * @param string $field
     * @param null|array $joinTableData
     * @return Varien_Db_Select
     */
    public function getQuery($field, $joinTableData = null)
    {
        $adapter = $this->getReadAdapter();

        /**
         * Base table - Catalog Product
         */
        $select = $adapter->select()
            ->from(
                ['e' => 'catalog_product_entity'],
                ['entity_id']
            );

        $this->addStatusToFilter($select);

        /**
         * Join to other table (left only)
         */
        if (is_array($joinTableData)) {
            list($name, $cond, $cols) = $joinTableData;

            $select->joinLeft($name, $cond, $cols);
        }

        /** @var int $timestamp */
        $timestamp = time();

        /** @var int $interval */
        $interval = ((int) Mage::getStoreConfig(BSeller_Neemu_Helper_Data::XML_NODE_PARTIAL_INTERVAL)) * 60;

        /**
         * Filter by datetime
         */
        $select->where($field . ' >= ?', date(Varien_Date::DATETIME_PHP_FORMAT, ($timestamp - $interval)));
        $select->where($field . ' <= ?', date(Varien_Date::DATETIME_PHP_FORMAT, $timestamp));

        /**
         * Dispatch event to customize in another module
         */
        Mage::dispatchEvent($this->_eventPrefix . '_available_ids', ['object' => $this, 'select' => $select]);

        return $select;
    }
}
