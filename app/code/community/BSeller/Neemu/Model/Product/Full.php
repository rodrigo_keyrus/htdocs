<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Neemu
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_Neemu_Model_Product_Full extends BSeller_Neemu_Model_Product_Abstract
{
    /**
     * Return available product ids
     *
     * @return array
     */
    public function getAvailableIds()
    {
        $adapter = $this->getReadAdapter();

        /**
         * Base table - Catalog Product
         */
        $select = $adapter->select()
            ->from(
                ['e' => 'catalog_product_entity'],
                ['entity_id']
            );

        $this->addStatusToFilter($select);

        /**
         * Dispatch event to customize in another module
         */
        Mage::dispatchEvent($this->_eventPrefix . '_available_ids', ['object' => $this, 'select' => $select]);

        return $adapter->fetchCol($select);
    }
}
