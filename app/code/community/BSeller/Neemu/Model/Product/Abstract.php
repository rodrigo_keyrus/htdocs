<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Neemu
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

abstract class BSeller_Neemu_Model_Product_Abstract extends Varien_Object
{
    /**
     * Event prefix
     *
     * @var string
     */
    protected $_eventPrefix = 'bseller_neemu_product';

    /**
     * Return available product ids
     *
     * @return array
     */
    public abstract function getAvailableIds();

    /**
     * Return available product collection
     *
     * @param array $productIds
     * @param array $variables
     * @param int $storeId
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    public function getAvailableCollection($productIds, $variables, $storeId)
    {
        /** @var array $attributes */
        $attributes = $this->getAvailableAttributeCodes($variables);

        /** @var Mage_Catalog_Model_Resource_Product_Collection $collection */
        $collection = Mage::getResourceModel('catalog/product_collection');

        /**
         * Add attributes to collection
         */
        $collection->addAttributeToSelect($attributes);

        /**
         * Add product filter to collection
         */
        $collection->addFieldToFilter('entity_id', ['in' => $productIds]);

        /**
         * Add price data to collection
         */
        $collection->addPriceData(null, $storeId);

        $collection->addTaxPercents();

        /**
         * Add store filter to collection
         */
        $collection->addStoreFilter($storeId);
        $collection->setStore($storeId);

        /**
         * Add catalog inventory object to collection
         */
        Mage::getModel('cataloginventory/stock')->addItemsToProducts($collection);

        /**
         * Dispatch event to customize in another module
         */
        Mage::dispatchEvent(
            $this->_eventPrefix . '_available_collection',
            ['object' => $this, 'collection' => $collection]
        );

        return $collection;
    }

    /**
     * Prepare values for product object
     *
     * @param Mage_Catalog_Model_Product $product
     * @param array $variables
     * @return $this
     */
    public function prepareValues($product, $variables)
    {
        /**
         * Initialize variable to false
         */
        $product->setData('skip_product', false);

        foreach ($variables as $variable) {
            switch ($variable) {
                case 'image':
                case 'thumbnail':
                case 'small_image':
                    $value = (string) Mage::helper('catalog/image')->init($product, $variable);
                    break;
                case 'thumb_variant':

                    if ($this->mediaAttributeIsEmpty($product->getData('thumb_variant'))) {
                        $value = '';

                        break;
                    }

                    $value = (string) Mage::helper('catalog/image')->init($product, $variable);
                    break;

                default:
                    $value = $this->getAttributeValue($product, $variable);
                    break;
            }

            $product->setData($variable, $value);
        }

        /**
         * Dispatch event to customize in another module
         */
        Mage::dispatchEvent(
            $this->_eventPrefix . '_prepare_values',
            ['object' => $this, 'product' => $product, 'variables' => $variables]
        );

        return $this;
    }

    /**
     * @param string $value
     *
     * @return boolean
     */
    public function mediaAttributeIsEmpty($value){
        if(!$value || ($value == 'no_selection')){
            return true;
        }

        return false;
    }

    /**
     * Return product image url
     *
     * @param Mage_Catalog_Model_Product $product
     * @param string $attributeName
     * @return string
     */
    protected function getImage($product, $attributeName)
    {
        /**
         * Resize product image
         */
        if (Mage::getStoreConfigFlag(BSeller_Neemu_Helper_Data::XML_NODE_IMAGE_RESIZE)) {
            return (string) Mage::helper('catalog/image')->init($product, $attributeName)->resize(
                (int) Mage::getStoreConfig(BSeller_Neemu_Helper_Data::XML_NODE_IMAGE_WIDTH),
                (int) Mage::getStoreConfig(BSeller_Neemu_Helper_Data::XML_NODE_IMAGE_HEIGHT)
            );
        }

        /** @var Mage_Catalog_Model_Product_Media_Config $productMedia */
        $productMedia = Mage::getSingleton('catalog/product_media_config');

        return (!empty($product->getData($attributeName)))
            ? $productMedia->getMediaUrl($product->getData($attributeName))
            : '';
    }

    /**
     * Return available product attribute codes
     *
     * @param array $variables
     * @return array
     * @throws Mage_Core_Exception
     */
    public function getAvailableAttributeCodes($variables)
    {
        if (!count($variables)) {
            return [];
        }

        $adapter = $this->getReadAdapter();

        $entityTypeId = Mage::getResourceModel('catalog/product')
            ->getEntityType()
            ->getData('entity_type_id');

        $select = $adapter->select()
            ->from(['eav_attr' => 'eav_attribute'], ['attribute_code', 'frontend_input', 'source_model'])
            ->where('eav_attr.entity_type_id = ?', $entityTypeId)
            ->where('eav_attr.attribute_code IN (' . $adapter->quote($variables) . ')');

        $this->setData('attributes', $adapter->fetchAll($select));

        $codes = [];

        foreach ($this->getData('attributes') as $attribute) {
            $codes[] = $attribute['attribute_code'];
        }

        return $codes;
    }

    /**
     * Return product any attribute value
     *
     * @param Mage_Catalog_Model_Product $product
     * @param string $attributeCode
     * @param string $separator
     * @return string
     */
    protected function getAttributeValue($product, $attributeCode, $separator = ',')
    {
        /** @var array $attributes */
        $attributes = $this->getData('attributes');

        $column = (!is_null($attributes)) ? array_column($attributes, 'attribute_code') : [];
        $key    = array_search($attributeCode, $column);

        if (!empty($key)) {
            $attribute = $attributes[$key];

            /**
             * Product attribute boolean type
             */
            if ($attribute['source_model'] === 'eav/entity_attribute_source_boolean') {
                return ((bool) $product->getData($attributeCode)) ? 1 : '';
            }

            /**
             * Product attribute options type
             */
            if (in_array($attribute['frontend_input'], ['select', 'multiselect'])) {
                $attributeText = $product->getAttributeText($attribute['attribute_code']);

                return implode($separator, (array) $attributeText);
            }
        }

        return $product->getData($attributeCode);
    }

    /**
     * Add status to filter select data
     *
     * @param Varien_Db_Select $select
     * @return $this
     */
    protected function addStatusToFilter($select)
    {
        /** @var Mage_Eav_Model_Config $config */
        $config = Mage::getModel('eav/config');

        /**
         * Status attribute - Catalog Product Int
         */
        $status = $config->getAttribute(Mage_Catalog_Model_Product::ENTITY, 'status');

        $select->joinLeft(
            ['status' => 'catalog_product_entity_int'],
            'status.entity_id = e.entity_id AND status.attribute_id = ' . $status->getAttributeId(),
            null
        );

        $select->where('status.value = ?', 1);

        return $this;
    }

    /**
     * Return read adapter
     *
     * @return Varien_Db_Adapter_Interface
     */
    protected function getReadAdapter()
    {
        return Mage::getSingleton('core/resource')->getConnection('core_read');
    }
}
