<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Neemu
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 * @author    Bruno Gemelli <bruno.gemelli@e-smart.com.br>
 */
class BSeller_Neemu_Model_File extends Varien_Object
{
    /** @var string */
    public $xmlContent = '';


    /**
     * @param BSeller_Neemu_Model_Product_Abstract $instance
     * @param string                               $type
     *
     * @return $this
     */
    public function create($instance, $type)
    {
        $template  = new Varien_Filter_Template();
        $content   = $this->getContent('item');
        $variables = $this->getTemplateVariables($template, $content);

        /** @var Esmart_Neemu_Model_Resource_Flat_Collection $flatItems */
        $flatItems = Mage::getResourceModel('esmart_neemu/flat_collection');

        $iteratorCallbacks = array(
            $this,
            'createItemNode',
        );

        $iteratorParams = array(
            'instance'      => $instance,
            'type'          => $type,
            'template'      => $template,
            'content'       => $content,
            'variables'     => $variables,
        );

        Mage::getSingleton('core/resource_iterator')->walk(
            $flatItems->getSelect(),
            array($iteratorCallbacks),
            $iteratorParams
        );

        $xmlContent = $this->getContent('header');
        $xmlContent .= $this->xmlContent;
        $xmlContent .= $this->getContent('footer');

        if (!$this->validateXml($xmlContent)) {
            return $this;
        }

        $this->saveFile($this->getFilename(), $xmlContent);
        $this->_getResourceModel()->deleteAll($type);

        return $this;
    }


    /**
     * @param $args
     */
    public function createItemNode($args)
    {
        /**
         * @var BSeller_Neemu_Model_Product_Abstract $instance
         * @var string                               $type
         * @var Varien_Filter_Template               $template
         * @var string                               $content
         * @var array                                $variables
         */
        $instance   = $args['instance'];
        $type       = $args['type'];
        $template   = $args['template'];
        $content    = $args['content'];
        $variables  = $args['variables'];

        /** @var array $item */
        $item = $args['row'];

        /** @var Mage_Catalog_Model_Product $product */
        $product = Mage::getSingleton('catalog/product');
        $product->setData(unserialize($item['data']));

        /**
         * Prepare product values
         */
        $instance->prepareValues($product, $variables);

        /**
         * Not generate xml for this product
         */
        if ($product->getData('skip_product') === true) {
            Mage::log(
                'Skip product (SKU): ' . $product->getSku(),
                null,
                'neemu_skip_product.log'
            );
            return;
        }

        /**
         * Insert variables in template
         */
        $template->setVariables(['product' => $product]);

        $item = $template->filter($content);
        $item = preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;', $item);

        $this->xmlContent .= $item;
    }


    /**
     * @param $filepath
     * @param $content
     *
     * @return $this
     */
    public function saveFile($filepath, $content)
    {
        try {
            if (!file_exists($filepath)) {
                fopen($filepath, 'w');
            }

            file_put_contents($filepath, $content);
        } catch (Exception $e) {
            Mage::log('Error: ' . $e->getMessage(), null, 'xdebug_neemu_job.log');
        }

        return $this;
    }


    /**
     * Insert content in file by type
     *
     * @param string $contentType
     * @return $this
     */
    public function insert($contentType)
    {
        $filePath = $this->getFilename();
        $openMode = file_exists($filePath) ? 'a' : 'w';
        $handle   = fopen($filePath, $openMode);
        
        fwrite($handle, $this->getContent($contentType));
        fclose($handle);

        return $this;
    }

    /**
     * Delete file from path
     *
     * @return $this
     */
    public function delete()
    {
        $filePath = $this->getFilename();
        
        if (!file_exists($filePath)) {
            return $this;
        }

        unlink($filePath);

        return $this;
    }

    /**
     * Return filename location
     *
     * @return string
     */
    public function getFilename()
    {
        $storeCode = 'default';
        $directory = Mage::getBaseDir('media') . DS . 'neemu';

        $options = Mage::getConfig()->getOptions();
        $options->createDirIfNotExists($directory);

        return ($directory . DS . 'store_' . $storeCode . '_' . $this->getPrefix() . '.xml');
    }

    /**
     * Return filename prefix
     *
     * @return string
     */
    public function getPrefix()
    {
        return (!is_null($this->_getData('prefix'))) ? $this->_getData('prefix') : 'temp';
    }

    /**
     * Return data content
     *
     * @param string $type
     * @return string
     */
    public function getContent($type)
    {
        return Mage::getStoreConfig('bseller_neemu/content/' . $type);
    }

    /**
     * Return template variables
     *
     * @param Varien_Filter_Template $template
     * @param string $content
     * @return array
     */
    protected function getTemplateVariables($template, $content)
    {
        if (!preg_match_all($template::CONSTRUCTION_PATTERN, $content, $constructions, PREG_SET_ORDER)) {
            return [];
        }

        $variables = [];

        foreach ($constructions as $construction) {
            if (!isset($construction[2])) {
                continue;
            }

            $vars = explode('.', trim($construction[2]));
            
            if (count($vars) < 2) {
                continue;
            }

            list(, $var) = $vars;

            if (!ctype_lower($var) && !strpos($var, '_')) {
                $var = str_replace(['get', 'set', 'uns', 'has'], '', $var);
                $var = preg_replace('/(?<!^)([A-Z])/', '_\\1', $var);
                $var = preg_replace('/([^a-zA-Z0-9_\s])/', '', $var);;
                $var = strtolower($var);
            }

            $variables[] = $var;
        }

        $variables = array_filter($variables);
        $variables = array_unique($variables);

        return $variables;
    }

    /**
     * @return Esmart_Neemu_Model_Resource_Flat
     */
    protected function _getResourceModel()
    {
        return Mage::getResourceSingleton('esmart_neemu/flat');
    }

    /**
     * @param $xml
     */
    public function validateXml($xml)
    {
        try {
            new simpleXmlElement($xml);
        } catch (Exception $e) {
            Mage::log('Error create xml: ' . $e->getMessage(), null, 'xdebug_neemu_job.log');
            return false;
        }

        return true;
    }
}
