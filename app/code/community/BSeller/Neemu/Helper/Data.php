<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Neemu
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_Neemu_Helper_Data extends BSeller_Core_Helper_Data
{
    /**
     * XML node module is active (partial load)
     *
     * @const string
     */
    const XML_NODE_PARTIAL_IS_ACTIVE = 'bseller_neemu/partial_load/active';

    /**
     * XML node partial load interval (partial load)
     *
     * @const string
     */
    const XML_NODE_PARTIAL_INTERVAL = 'bseller_neemu/partial_load/interval';

    /**
     * XML node module is active (full load)
     *
     * @const string
     */
    const XML_NODE_FULL_IS_ACTIVE = 'bseller_neemu/full_load/active';

    /**
     * XML node image resize
     *
     * @const string
     */
    const XML_NODE_IMAGE_RESIZE = 'bseller_neemu/image/resize';

    /**
     * XML node image width
     *
     * @const string
     */
    const XML_NODE_IMAGE_WIDTH = 'bseller_neemu/image/width';

    /**
     * XML node image height
     *
     * @const string
     */
    const XML_NODE_IMAGE_HEIGHT = 'bseller_neemu/image/height';

    /**
     * Product partial type
     *
     * @const string
     */
    const PRODUCT_PARTIAL_TYPE = 'partial';

    /**
     * Product full type
     *
     * @const string
     */
    const PRODUCT_FULL_TYPE = 'full';

    /**
     * Product queue limit
     *
     * @var int
     */
    const PRODUCT_QUEUE_LIMIT = 50;

    /**
     * Resque queue name
     *
     * @const string
     */
    const RESQUE_QUEUE_NAME = 'BSellerNeemuXML';

    /**
     * Resque job class
     *
     * @const string
     */
    const RESQUE_JOB_CLASS = 'BSeller_Neemu_Model_Job';

    /**
     * Return if module is active (partial load)
     *
     * @return bool
     */
    public function isPartialEnabled()
    {
        return Mage::getStoreConfigFlag(self::XML_NODE_PARTIAL_IS_ACTIVE);
    }

    /**
     * Return if module is active (full load)
     *
     * @return bool
     */
    public function isFullEnabled()
    {
        return Mage::getStoreConfigFlag(self::XML_NODE_FULL_IS_ACTIVE);
    }
}
