<?php

/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category    BSeller
 * @package     BSeller_Page
 *
 * @copyright   Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author      Julio Reis <julio.reis@e-smart.com.br>
 */
class BSeller_Page_Block_Html_Breadcrumbs extends Mage_Page_Block_Html_Breadcrumbs
{
    /**
     * @var null
     */
    protected $_mobileCrumbs = null;

    /**
     * @return array|null
     */
    public function getMobileCrumbs()
    {
        if (!$this->_mobileCrumbs) {
            $this->_mobileCrumbs = $this->_crumbs;

            if (isset($this->_mobileCrumbs['home'])) {
                unset($this->_mobileCrumbs['home']);
                if (isset($this->_mobileCrumbs[array_keys($this->_mobileCrumbs)[0]])) {
                    $this->_mobileCrumbs[array_keys($this->_mobileCrumbs)[0]]['first'] = true;
                }
            }
            if (isset($this->_mobileCrumbs['product'])) {
                unset($this->_mobileCrumbs['product']);
                $totalCrumbs = count($this->_mobileCrumbs);
                if ($totalCrumbs > 0 && isset($this->_mobileCrumbs[array_keys($this->_mobileCrumbs)[$totalCrumbs - 1]])) {
                    $this->_mobileCrumbs[array_keys($this->_mobileCrumbs)[$totalCrumbs - 1]]['last'] = true;
                }
            }
            $paramCatId = $this->getRequest()->getParam('cat');
            if ($paramCatId != null) {
                $category = Mage::getModel('catalog/category')->load($paramCatId);
                if ($category && $category->getId()) {
                    $totalCrumbs = count($this->_mobileCrumbs);
                    $this->_mobileCrumbs[array_keys($this->_mobileCrumbs)[$totalCrumbs - 1]]['last'] = false;

                    $newCrumb = array('label' => $category->getName(),
                        'title' => $category->getTitle(),
                        'link' => $category->getUrl(),
                        'first' => false,
                        'last' => 'true',
                        'readonly' => null);
                    $this->_mobileCrumbs['category' . $category->getId()] = $newCrumb;
                }
            }
        }
        return $this->_mobileCrumbs;
    }

    /**
     * @return mixed|null
     */
    public function getCustomBreadcrumbs()
    {
        $_helper = Mage::helper('catalog/category');
        $product = Mage::registry('current_product');
        $breadcrumbsBlock = $this->getLayout()->getBlock('breadcrumbs');

        if (!$product) {
            return $breadcrumbsBlock->_crumbs;
        }

        $breadcrumbsBlock->_crumbs = null;

        //Add crumbs Home
        $breadcrumbsBlock->addCrumb(
            'home',
            array(
                'label'=>Mage::helper('catalog')->__('Home'),
                'title'=>Mage::helper('catalog')->__('Go to Home Page'),
                'link'=>Mage::getBaseUrl()
            )
        );

        //Add crumbs Category
        if (count($product->getCategoryIds())) {
            $categories = Mage::getModel('catalog/category')->getCollection()
                ->addFieldToSelect(array('include_in_menu', 'level', 'name', 'path'))
                ->addAttributeToFilter('entity_id', $product->getCategoryIds())
                ->addAttributeToFilter('is_active', 1)
                ->addAttributeToFilter('include_in_menu', 1);

            $level = 0;

            foreach ($categories->getItems() as $category){
                if ($category->getLevel() > 1 && $category->getLevel() > $level) {
                    $level = $category->getLevel();
                    $url = $_helper->getCategoryUrl($category);
                    $breadcrumbsBlock->addCrumb(
                        'category' . $category->getId(),
                        array(
                            'label'=>Mage::helper('catalog')->__($category->getName()),
                            'title'=>Mage::helper('catalog')->__($category->getName()),
                            'link'=>$url
                        )
                    );
                }
            }
        }

        //Add crumbs product
        $breadcrumbsBlock->addCrumb(
            'product',
            array(
                'label'=>Mage::helper('catalog')->__($product->getName()),
                'title'=>Mage::helper('catalog')->__($product->getName()),
                'last' => true
            )
        );

        return $breadcrumbsBlock->_crumbs;
    }
}
