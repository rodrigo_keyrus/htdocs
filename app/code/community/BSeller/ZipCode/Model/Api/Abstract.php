<?php

/**
 * BSeller Platform | B2W - Companhia Digital
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category  
 * @package   
 *
 * @copyright Copyright (c) 2016 B2W Digital - BSeller Platform. (http://www.bseller.com.br)
 *
 * @author    BSeller Core Team <dev@e-smart.com.br>
 */
abstract class BSeller_ZipCode_Model_Api_Abstract
{

    /**
     * 
     * @var string
     */
    private $_zipCode;
 
    /**
     * @param $zipCode
     */
    public function setZipCode($zipCode)
    {
        $this->_zipCode = $zipCode;
    }

    /**
     * @return mixed
     */
    public function getZipCode()
    {
        return $this->_zipCode;
    }
    
}
