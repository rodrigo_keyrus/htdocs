<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_ZipCode
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_ZipCode_Model_Api_Adapter_Republica extends BSeller_ZipCode_Model_Api_Adapter_Abstract
{
    /**
     * Adapter fields map
     *
     * @var array
     */
    protected $_adapterMap = [
        'street'       => [
            'tipo_logradouro',
            'logradouro'
        ],
        'neighborhood' => 'bairro',
        'city'         => 'cidade',
        'region'       => 'uf'
    ];

    /**
     * Autocomplete provider
     *
     * @var int
     */
    protected $_provider = Esmart_Customer_Helper_Provider::REPUBLICA_VIRTUAL;
}
