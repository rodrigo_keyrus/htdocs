<?php

/**
 * BSeller Platform | B2W - Companhia Digital
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category  
 * @package   
 *
 * @copyright Copyright (c) 2016 B2W Digital - BSeller Platform. (http://www.bseller.com.br)
 *
 * @author    BSeller Core Team <dev@e-smart.com.br>
 */
interface BSeller_ZipCode_Model_Api_Interface
{
    
    /**
     * Function responsible to make the request to webservice
     */
    public function request();
    
    /**
     * Function responsible to treat the response
     * @param array $response
     */
    public function responseTreatment($response);
}
