<?php

/**
 * BSeller Platform | B2W - Companhia Digital
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category  
 * @package   
 *
 * @copyright Copyright (c) 2016 B2W Digital - BSeller Platform. (http://www.bseller.com.br)
 *
 * @author    BSeller Core Team <dev@e-smart.com.br>
 */
class BSeller_ZipCode_Model_Api_Republica
    extends BSeller_ZipCode_Model_Api_Abstract 
        implements BSeller_ZipCode_Model_Api_Interface
{
    const XML_PATH_ZIPCODE_WS_URL       = 'bseller_zipcode/api_settings/ws_url_republica';
    
    const ADAPTER = 'republica';
        
    /**
     * Function responsible to make the request to webservice
     * @return mixed
     * @throws Zend_Http_Client_Exception
     * @throws Zend_Json_Exception
     */
    public function request()
    {
        $url = sprintf(Mage::getStoreConfig(self::XML_PATH_ZIPCODE_WS_URL), $this->getZipCode());
        
        $client = new Zend_Http_Client($url);
        
        $response = Zend_Json::decode($client->request('GET')->getBody(), true);
        
        return $this->responseTreatment($response);
    }
    
    /**
     * Function responsible to treat the response
     * @param array $response
     */
    public function responseTreatment($response)
    {
        if (!$response['resultado']) {
            return json_encode(array('error' => 1));
        }
        
        $adapter = Mage::getModel('bseller_zipcode/api_adapter');
        
        return $adapter->handle($response, self::ADAPTER);
    }
}
