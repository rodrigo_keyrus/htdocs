<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_ZipCode
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_ZipCode_Model_Api_Findcep
    extends BSeller_ZipCode_Model_Api_Abstract
        implements BSeller_ZipCode_Model_Api_Interface
{
    /**
     * XML config paths
     *
     * @const string
     */
    const XML_PATH_ZIPCODE_WS_URL     = 'bseller_zipcode/api_settings/ws_url_findcep';
    const XML_PATH_ZIPCODE_WS_REFERER = 'bseller_zipcode/api_settings/ws_referer_findcep';
    const XML_PATH_ALTERNATIVE_ACTIVE = 'bseller_zipcode/api_settings/ws_active_americanas';

    /**
     * Adapter name
     *
     * @const string
     */
    const ADAPTER = 'findcep';
        
    /**
     * Function responsible to make the request to webservice
     *
     * @return string|array
     * @throws Zend_Http_Client_Exception
     * @throws Zend_Json_Exception
     */
    public function request()
    {
        $requestUrl = sprintf(Mage::getStoreConfig(self::XML_PATH_ZIPCODE_WS_URL), $this->getZipCode());
        $refererUrl = Mage::getStoreConfig('XML_PATH_ZIPCODE_WS_REFERER') ?: Mage::getBaseUrl();

        $client = new Zend_Http_Client($requestUrl);
        $client->setAdapter('Zend_Http_Client_Adapter_Curl');
        $client->getAdapter()->setCurlOption(CURLOPT_REFERER, $refererUrl);

        $response = Zend_Json::decode($client->request('GET')->getBody(), true);
        
        return $this->responseTreatment($response);
    }
    
    /**
     * Function responsible to treat the response
     *
     * @param array $response
     * @return string|array
     */
    public function responseTreatment($response)
    {
        if ($response['status'] == 'cep_invalido') {

            if (!Mage::getStoreConfig(self::XML_PATH_ALTERNATIVE_ACTIVE)) {
                return json_encode(['error' => 1]);
            }

            /** @var BSeller_ZipCode_Model_Api_Americanas $americanasApi */
            $americanasApi = Mage::getModel('bseller_zipcode/api_americanas');
            $americanasApi->setZipCode($this->getZipCode());

            return $americanasApi->request();
        }

        /** @var BSeller_ZipCode_Model_Api_Adapter $adapter */
        $adapter = Mage::getModel('bseller_zipcode/api_adapter');

        return $adapter->handle($response, self::ADAPTER);
    }
}
