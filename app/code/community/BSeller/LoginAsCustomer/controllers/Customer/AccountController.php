<?php
/**
 * BSeller Platform | B2W - Companhia Digital
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category  BSeller
 * @package   BSeller_LoginAsCustomer
 *
 * @copyright Copyright (c) 2016 B2W Digital - BSeller Platform. (http://www.bseller.com.br)
 *
 * @author    Tiago Sampaio <tiago.sampaio@e-smart.com.br>
 * @author    Bruno Gemelli <bruno.gemelli@e-smart.com.br>
 */
class BSeller_LoginAsCustomer_Customer_AccountController extends Mage_Core_Controller_Front_Action
{

    use BSeller_LoginAsCustomer_Trait_Data,
        BSeller_LoginAsCustomer_Trait_Config;


    public function forceLoginAction()
    {
        /* parse the 'loginAsCustomer' param */
        $info = unserialize(
            Mage::helper('core')->decrypt( /* important step; use Magento encryption key to decrypt/extract info */
                base64_decode($this->getRequest()->getParam('info'))
            )
        );

        $customerId = isset($info['customer_id']) ? (int) $info['customer_id'] : null;
        $websiteId  = isset($info['website_id'])  ? (int) $info['website_id']  : null;
        $timestamp  = isset($info['timestamp'])   ? (int) $info['timestamp']   : null;

        if ($this->validateTimestamp() && $timestamp) {
            $isValid = time() < ($info['timestamp'] + 15);
            /** @todo Add timestamp validation */
        }

        if ($customerId) {
            /** @var Mage_Customer_Model_Session $customerSession */
            $customerSession = Mage::getSingleton('customer/session');

            /* Share Customer Accounts is set to "Per Website" */
            if (Mage::getSingleton('customer/config_share')->isWebsiteScope()) {
                if (Mage::app()->getWebsite()->getId() != $websiteId) {
                    $message = '<i>Share Customer Accounts</i> option is set to <i>Per Website</i>. You are trying to login as customer from website %d into website %s. This action is not allowed.';
                    Mage::getSingleton('customer/session')->addNotice(
                        $this->__($message, $websiteId, Mage::app()->getWebsite()->getId())
                    );

                    $this->_redirect('customer/account');
                    return;
                }
            }

            if ($customerSession->getCustomerId() != $customerId) {
                /* Logout any currently logged in customer */
                if ($customerSession->isLoggedIn()) {
                    $customerSession->logout();
                }

                /* Login new customer as requested on the admin interface */
                $customerSession->loginById($customerId);

                $loginAsCustomerSession = Mage::getSingleton('bseller_loginascustomer/session');
                /* set admin user logged as customer in session */
                $loginAsCustomerSession->setLacAdminUserId($info['admin_user_id']);
                $loginAsCustomerSession->setLacAdminUsername($info['admin_username']);
                $loginAsCustomerSession->setLacAdminUserLogdate($info['admin_user_logdate']);
                $loginAsCustomerSession->setLacAdminUserTimestamp($info['timestamp']);
            }
        }

        $this->_redirect('customer/account');
    }

}
