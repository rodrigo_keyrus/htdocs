<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_ImageResize
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

/**
 * Class BSeller_ImageResize_Model_Processor
 *
 * Getters
 *
 * @method int|float getWidth()
 * @method int|float getHeight()
 *
 * Setters
 *
 * @method $this setWidth(int|float $with)
 * @method $this setHeight(int|float $height)
 */
class BSeller_ImageResize_Model_Processor extends Varien_Object
{
    /**
     * Base image processor object
     *
     * @var Varien_Image
     */
    protected $_processor;

    /**
     * Base image path
     *
     * @var string
     */
    protected $_baseFilePath;

    /**
     * Cache image path
     *
     * @var string
     */
    protected $_cacheFilePath;

    /**
     * Base cache image path
     *
     * @var string
     */
    protected $_baseCacheFilePath = 'cache';

    /**
     * Image quality
     *
     * @var int
     */
    protected $_quality;

    /**
     * Keep aspect ratio
     *
     * @var bool
     */
    protected $_keepAspectRatio = true;

    /**
     * Keep frame
     *
     * @var bool
     */
    protected $_keepFrame = true;

    /**
     * Keep transparency
     *
     * @var bool
     */
    protected $_keepTransparency = true;

    /**
     * Constrain only
     *
     * @var bool
     */
    protected $_constrainOnly = false;

    /**
     * Background color
     *
     * @var array
     */
    protected $_backgroundColor = [255, 255, 255];

    /**
     * Image is cached
     *
     * @var bool
     */
    protected $_fileIsCached = false;

    /**
     * Set filename for base file and new file
     *
     * @param string $baseFilePath
     * @param string $cacheFilePath
     * @param int $quality
     * @return $this
     * @throws Exception
     */
    public function init($baseFilePath, $cacheFilePath, $quality)
    {
        $this->_baseFilePath  = $baseFilePath;
        $this->_cacheFilePath = Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA)
            . DS . $this->_baseCacheFilePath
            . DS . $cacheFilePath
            . DS . $this->getWidth() . 'x' . $this->getHeight()
            . DS . basename($this->_baseFilePath);

        if ($this->fileExists($this->_cacheFilePath)) {
            $this->_baseFilePath = $this->_cacheFilePath;
            $this->_fileIsCached = true;
        }

        $this->_quality = $quality;

        return $this;
    }

    /**
     * Return image processor object
     *
     * @return Varien_Image
     */
    public function getImageProcessor()
    {
        if (!$this->_processor instanceof Varien_Image) {
            $this->_processor = new Varien_Image($this->_baseFilePath);
        }

        $this->_processor->keepAspectRatio($this->_keepAspectRatio);
        $this->_processor->keepFrame($this->_keepFrame);
        $this->_processor->keepTransparency($this->_keepTransparency);
        $this->_processor->constrainOnly($this->_constrainOnly);
        $this->_processor->backgroundColor($this->_backgroundColor);
        $this->_processor->quality($this->_quality);

        return $this->_processor;
    }

    /**
     * Return image storage object
     *
     * @return Mage_Core_Helper_File_Storage_Database
     */
    public function getImageStorage()
    {
        return Mage::helper('core/file_storage_database');
    }

    /**
     * Resize image file
     *
     * @return $this
     */
    public function resize()
    {
        if (is_null($this->getWidth()) && is_null($this->getHeight())) {
            return $this;
        }

        $this->getImageProcessor()->resize($this->getWidth(), $this->getHeight());

        return $this;
    }

    /**
     * Save image file in directory
     *
     * @return $this
     */
    public function save()
    {
        $this->getImageProcessor()->save($this->_cacheFilePath);
        $this->getImageStorage()->saveFile($this->_cacheFilePath);

        return $this;
    }

    /**
     * Return image url
     *
     * @param bool $isSecure
     * @param null|string $filePath
     * @return string
     */
    public function getUrl($isSecure, $filePath = null)
    {
        if (!is_null($filePath)) {
            $this->_cacheFilePath = $filePath;
        }

        $directory = Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA);
        $filename  = str_replace($directory . DS, '', $this->_cacheFilePath);

        return Mage::getBaseUrl(
            Mage_Core_Model_Store::URL_TYPE_MEDIA,
            $isSecure
        ) . str_replace(DS, '/', $filename);
    }

    /**
     * Clear images cached
     *
     * @return $this
     */
    public function clearCache()
    {
        $directory = Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . $this->_baseCacheFilePath;

        $io = new Varien_Io_File();
        $io->rmdir($directory, true);

        $this->getImageStorage()->deleteFolder($directory);

        return $this;
    }

    /**
     * Return if image was cached
     *
     * @return bool
     */
    public function isCached()
    {
        return $this->_fileIsCached;
    }

    /**
     * Check if image exists
     *
     * @param string $filename
     * @return bool
     */
    protected function fileExists($filename)
    {
        if (file_exists($filename)) {
            return true;
        }

        return $this->getImageStorage()->saveFileToFilesystem($filename);
    }
}
