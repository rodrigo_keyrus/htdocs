<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_ImageResize
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_ImageResize_Model_Varnish extends Varien_Object
{
    /**
     * Clear images cache
     *
     * @return $this
     */
    public function clearCache()
    {
        if (!Mage::helper('bseller_imageresize')->isModuleEnabled('Phoenix_VarnishCache')) {
            return $this;
        }

        if (!Mage::helper('varnishcache')->isEnabled()) {
            return $this;
        }

        /** @var Phoenix_VarnishCache_Model_Control $cacheControl */
        $cacheControl = Mage::getSingleton('varnishcache/control');

        /** @var string $domainList */
        $domainList = Mage::helper('varnishcache/cache')->getStoreDomainList();

        $cacheControl->clean(
            $domainList,
            '^/media/cache/',
            Phoenix_VarnishCache_Model_Control::CONTENT_TYPE_IMAGE
        );

        /**
         * Also clean HTML files
         */
        $cacheControl->clean($domainList, '.*', Phoenix_VarnishCache_Model_Control::CONTENT_TYPE_HTML);

        Mage::getSingleton('adminhtml/session')->addSuccess(
            Mage::helper('bseller_imageresize')->__('The image cache has been cleaned on the Varnish servers.')
        );

        return $this;
    }
}
