<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_ImageResize
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

require_once Mage::getModuleDir('controllers', 'Mage_Adminhtml') . DS . 'CacheController.php';

class BSeller_ImageResize_Adminhtml_CacheController extends Mage_Adminhtml_CacheController
{
    /**
     * Clean custom files cache
     *
     * @return void
     */
    public function cleanCustomAction()
    {
        try {
            /**
             * Clear images from filesystem or database storage
             */
            Mage::getModel('bseller_imageresize/processor')->clearCache();

            /**
             * Clear images from varnish storage
             */
            Mage::getModel('bseller_imageresize/varnish')->clearCache();

            /**
             * Dispatch event after delete images
             */
            Mage::dispatchEvent('clean_custom_images_cache_after');

            $this->_getSession()->addSuccess($this->__('The image cache was cleaned.'));
        } catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Exception $e) {
            $this->_getSession()->addException(
                $e,
                $this->__('An error occurred while clearing the image cache.')
            );
        }

        $this->_redirect('*/*');
    }
}
