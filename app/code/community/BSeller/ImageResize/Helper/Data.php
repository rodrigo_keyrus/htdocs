<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_ImageResize
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_ImageResize_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Image processor object
     *
     * @var BSeller_ImageResize_Model_Processor
     */
    protected $_processor;

    /**
     * Image path
     *
     * @var null|string
     */
    protected $_filePath;

    /**
     * Image cache path
     *
     * @var null|string
     */
    protected $_fileCachePath;

    /**
     * Image quality percentage
     *
     * @var int
     */
    protected $_quality = 100;

    /**
     * Is secure image path
     *
     * @var false|bool
     */
    protected $_isSecure = false;

    /**
     * Scheduled for resize image
     *
     * @var bool
     */
    protected $_scheduleResize = false;

    /**
     * Initialize process
     *
     * @param string $filePath
     * @param string $fileCachePath
     * @param null|int $quality
     * @param false|bool $isSecure
     * @return $this
     */
    public function init($filePath, $fileCachePath, $quality = null, $isSecure = null)
    {
        $this->reset();

        $this->_processor     = Mage::getModel('bseller_imageresize/processor');
        $this->_filePath      = $filePath;
        $this->_fileCachePath = $fileCachePath;

        if (is_int($quality)) {
            $this->_quality = $quality;
        }

        if (is_bool($isSecure)) {
            $this->_isSecure = $isSecure;
        }

        return $this;
    }

    /**
     * Resize image file
     *
     * @param int|float $width
     * @param null|int|float $height
     * @return $this
     */
    public function resize($width, $height = null)
    {
        if (is_null($height)) {
            $height = $width;
        }

        $this->_processor->setWidth($width);
        $this->_processor->setHeight($height);

        $this->_scheduleResize = true;

        return $this;
    }

    /**
     * Return image url
     *
     * @return string
     */
    public function __toString()
    {
        try {
            $this->addOriginalSize();

            $this->_processor->init($this->_filePath, $this->_fileCachePath, $this->_quality);

            if ($this->_processor->isCached()) {
                return $this->_processor->getUrl($this->_isSecure);
            }

            if ($this->_scheduleResize) {
                $this->_processor->resize();
            }

            $this->_processor->save();

            $url = $this->_processor->getUrl($this->_isSecure);
        } catch (Exception $e) {
            $url = $this->_processor->getUrl($this->_isSecure, $this->_filePath);
        }

        return $url;
    }

    /**
     * Add original image size if don't use resize
     *
     * @return $this
     */
    protected function addOriginalSize()
    {
        $originalImageProcessor = new Varien_Image($this->_filePath);

        if (is_null($this->_processor->getWidth())) {
            $this->_processor->setWidth($originalImageProcessor->getOriginalWidth());
        }

        if (is_null($this->_processor->getHeight())) {
            $this->_processor->setHeight($originalImageProcessor->getOriginalHeight());
        }

        return $this;
    }

    /**
     * Reset all previous data
     *
     * @return $this
     */
    protected function reset()
    {
        $this->_processor      = null;
        $this->_filePath       = null;
        $this->_fileCachePath  = null;
        $this->_quality        = 100;
        $this->_isSecure       = false;
        $this->_scheduleResize = false;

        return $this;
    }
}
