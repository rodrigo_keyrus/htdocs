<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Sitemap
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_Sitemap_Model_Observer
{
    use BSeller_Core_Trait_Data;

    /**
     * Show new field in cms page dit form
     *
     * @param Varien_Event_Observer $observer
     * @return $this
     */
    public function adminhtmlCmsPageEditTabMainPrepareForm(Varien_Event_Observer $observer)
    {
        /** @var Varien_Data_Form $form */
        $form = $observer->getEvent()->getData('form');

        /* @var Varien_Data_Form_Element_Fieldset $baseFieldset */
        $fieldset = $form->getElement('base_fieldset');

        $isElementDisabled = !Mage::getSingleton('admin/session')->isAllowed('cms/page/save');

        $fieldset->addField(
            'show_in_google_sitemap',
            'select',
            [
                'label'    => $this->_helper()->__('Show in Google Sitemap'),
                'title'    => $this->_helper()->__('Show in Google Sitemap'),
                'name'     => 'show_in_google_sitemap',
                'required' => true,
                'options'  => Mage::getSingleton('adminhtml/system_config_source_yesno')->toArray(),
                'disabled' => $isElementDisabled
            ]
        );

        return $this;
    }


    /**
     * Prepare brand collection with sitemap/SEO manual instructions
     * This sitemap XML should contains Brand URL homepage and your children categories
     *
     * @param Varien_Event_Observer $observer
     *
     * @return $this
     */
    public function prepareManufacturerSitemapCollection(Varien_Event_Observer $observer)
    {
        $collection = $observer->getCollection();
        $newCollection = Mage::getModel('bseller_sitemap/manufacturerSitemap')->prepareSitemapArray();
        $collection->setItems($newCollection);
        return $this;
    }
}
