<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Sitemap
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

/**
 * Class BSeller_Sitemap_Model_Sitemap
 *
 * @method BSeller_Sitemap_Helper_Data _helper()
 * @method $this setLimit(int $limit)
 * @method int getLimit()
 * @method $this setBaseUrl(string $url)
 * @method string getBaseUrl()
 * @method $this setDate(string $limit)
 * @method string getDate()
 * @method $this setMainIo(Varien_Io_File $io)
 * @method Varien_Io_File getMainIo()
 */
class BSeller_Sitemap_Model_Sitemap extends Mage_Sitemap_Model_Sitemap
{
    use BSeller_Core_Trait_Data;

    /**
     * Maximum of items in file
     *
     * @var int
     */
    const MAX_LIMIT = 50000;

    /**
     * Sitemap options
     *
     * @var array
     */
    protected $_options = [
        [
            'catalog_category',
            'category',
            'sitemap_categories_generating_before',
            'sitemap'
        ],
        [
            'catalog_product',
            'product',
            'sitemap_products_generating_before',
            'sitemap'
        ],
        [
            'cms_page',
            'page',
            'sitemap_pages_generating_before',
            'bseller_sitemap'
        ],
        [
            'manufacturer',
            'manufacturer',
            'sitemap_manufacturers_generating_before',
            'bseller_sitemap'
        ]
    ];

    /**
     * Generate XML files
     *
     * @return BSeller_Sitemap_Model_Sitemap|Mage_Sitemap_Model_Sitemap
     */
    public function generateXml()
    {
        if (!$this->_helper()->isModuleEnabled()) {
            return parent::generateXml();
        }

        $limit = (int) Mage::getStoreConfig('bseller_sitemap/settings/limit');

        $this->setLimit(($limit > 0) ? $limit : self::MAX_LIMIT);
        $this->setBaseUrl(Mage::app()->getStore($this->getStoreId())->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK));
        $this->setDate(Mage::getSingleton('core/date')->gmtDate(Varien_Date::DATE_PHP_FORMAT));
        $this->setMainIo($this->getIoFile());

        $this->getMainIo()->streamWrite('<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">');

        foreach ($this->_options as $option) {
            list ($resourceName, $configName, $eventName, $moduleName) = $option;

            $this->prepareInstanceXml($resourceName, $configName, $eventName, $moduleName);
        }

        $this->getMainIo()->streamWrite('</sitemapindex>');
        $this->getMainIo()->streamClose();

        $this->setSitemapTime(Mage::getSingleton('core/date')->gmtDate(Varien_Date::DATETIME_PHP_FORMAT));
        $this->save();

        return $this;
    }

    /**
     * Generate instance sitemap
     *
     * @param string $resourceName
     * @param string $configName
     * @param string $eventName
     * @param string $moduleName
     * @return $this
     */
    protected function prepareInstanceXml($resourceName, $configName, $eventName, $moduleName)
    {
        $storeId = (int) $this->getStoreId();
        $limit   = (int) $this->getLimit();

        $changefreq = (string) Mage::getStoreConfig('sitemap/' . $configName . '/changefreq', $storeId);
        $priority   = (string) Mage::getStoreConfig('sitemap/' . $configName . '/priority', $storeId);

        /** @var array $collection */
        $collection = Mage::getResourceModel($moduleName . '/' . $resourceName)->getCollection($storeId);

        /**
         * Dispatch instance event observer
         */
        $collection = $this->dispatchEvent($eventName, $collection);

        $pages    = ceil(count($collection) / $limit);
        $iterator = 0;

        while ($iterator < $pages) {
            $name = ('_' . $resourceName . '_' . $iterator . '.xml');

            $io = $this->getIoFile($name);
            $io->streamWrite('<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">');

            $items = array_slice($collection, $iterator * $limit, $limit);

            /** @var Varien_Object $item */
            foreach ($items as $item) {

                $itemPriority = ($item->getData('priority')) ? $item->getData('priority') : $priority;

                $xml = sprintf(
                    '<url><loc>%s</loc><lastmod>%s</lastmod><changefreq>%s</changefreq><priority>%.1f</priority></url>',
                    htmlspecialchars($this->getBaseUrl() . $item->getData('url')),
                    $this->getDate(),
                    $changefreq,
                    $itemPriority
                );

                $io->streamWrite($xml);
            }

            $io->streamWrite('</urlset>');
            $io->streamClose();

            /**
             * Adding link of the sub-file to the main file
             */
            $this->prepareInstanceLinkXml($name);

            $iterator++;
        }

        return $this;
    }

    /**
     * Adding link of the sub-file to the main file
     *
     * @param string $name
     * @return $this
     */
    protected function prepareInstanceLinkXml($name)
    {
        $xml = sprintf(
            '<sitemap><loc>%s</loc><lastmod>%s</lastmod></sitemap>',
            htmlspecialchars($this->getFileUrl($name)),
            $this->getDate()
        );

        $this->getMainIo()->streamWrite($xml);

        return $this;
    }

    /**
     * Dispatch instance event observer
     *
     * @param string $name
     * @param array $collection
     *
     * @return array
     */
    protected function dispatchEvent($name, $collection)
    {
        $object = new Varien_Object();
        $object->setData('items', $collection);

        Mage::dispatchEvent($name, ['collection' => $object, 'store_id' => $this->getStoreId()]);

        return $object->getItems();
    }

    /**
     * Return IO file instance
     *
     * @param null|string $suffix
     * @return Varien_Io_File
     */
    protected function getIoFile($suffix = null)
    {
        $filename = $this->getSitemapFilename();

        if (!is_null($suffix)) {
            $filename = substr($filename, 0, strpos($filename, '.xml')) . $suffix;
        }

        $filePath = $this->getPath() . DS . $filename;

        $io = new Varien_Io_File();

        if ($io->fileExists($filePath)) {
            $io->rm($filePath);
        }

        $io->setAllowCreateFolders(true);
        $io->open(['path' => $this->getPath()]);
        $io->streamOpen($filename);
        $io->streamWrite('<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL);

        return $io;
    }

    /**
     * Return URL of sitemap sub-file by the name
     *
     * @param $name
     * @return string
     */
    protected function getFileUrl($name)
    {
        $fileName = substr($this->getSitemapFilename(), 0, strpos($this->getSitemapFilename(), '.xml')) . $name;

        $filePath = $this->getBaseUrl() . $this->getSitemapPath();
        $filePath = str_replace('//', '/', $filePath);
        $filePath = str_replace(':/', '://', $filePath);

        return $filePath . $fileName;
    }
}
