<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Sitemap
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_Sitemap_Model_Resource_Cms_Page extends Mage_Sitemap_Model_Resource_Cms_Page
{
    /**
     * Retrieve cms page collection array
     *
     * @param int $storeId
     * @return array
     */
    public function getCollection($storeId)
    {
        $pages = [];

        $select = $this->_getWriteAdapter()->select()
            ->from(['main_table' => $this->getMainTable()], [$this->getIdFieldName(), 'identifier AS url'])
            ->join(
                ['store_table' => $this->getTable('cms/page_store')],
                'main_table.page_id = store_table.page_id',
                []
            )
            ->where('main_table.is_active = ?', true)
            ->where('main_table.show_in_google_sitemap = ?', true)
            ->where('store_table.store_id IN (?)', [0, $storeId]);

        $query = $this->_getWriteAdapter()->query($select);

        while ($row = $query->fetch()) {
            if ($row['url'] === Mage_Cms_Model_Page::NOROUTE_PAGE_ID) {
                continue;
            }

            $page = $this->_prepareObject($row);

            $pages[$page->getId()] = $page;
        }

        return $pages;
    }
}
