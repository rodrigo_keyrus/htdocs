<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Sitemap
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_Sitemap_Model_Resource_Manufacturer extends Zeon_Manufacturer_Model_Mysql4_Manufacturer
{
    /**
     * Retrieve manufacturer collection array
     *
     * @param int $storeId
     * @return array
     */
    public function getCollection($storeId)
    {
        $manufacturers = [];

        $select = $this->_getWriteAdapter()->select()
            ->from(['main_table' => $this->getMainTable()], [$this->getIdFieldName(), 'identifier AS url'])
            ->join(
                ['store_table' => $this->getTable('zeon_manufacturer/store')],
                'main_table.' . $this->getIdFieldName() . ' = store_table.' . $this->getIdFieldName(),
                ['store_id']
            )
            ->where('main_table.status = ?', true)
            ->where('store_table.store_id IN (?)', [0, $storeId])
            ->order('sort_order ASC');

        $query = $this->_getWriteAdapter()->query($select);

        while ($row = $query->fetch()) {
            if (empty($row['url'])) {
                continue;
            }

            $manufacturer = $this->prepareObject($row);

            $manufacturers[$manufacturer->getId()] = $manufacturer;
        }

        return $manufacturers;
    }

    /**
     * Prepare manufacturer object
     *
     * @param array $data
     * @return Varien_Object
     */
    protected function prepareObject(array $data)
    {
        $object = new Varien_Object(
            [
                'id'  => $data[$this->getIdFieldName()],
                'url' => $data['url']
            ]
        );

        return $object;
    }
}
