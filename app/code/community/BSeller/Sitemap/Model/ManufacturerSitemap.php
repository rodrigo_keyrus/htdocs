<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Core
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Diogo Baracho <diogo.baracho@e-smart.com.br>
 */

class BSeller_Sitemap_Model_ManufacturerSitemap{


    private $_mageBaseUrl,
        $_xmlFileNameTmp,
        $_xmlFileName,
        $_bmChangefreq,
        $_bmPriority,
        $_date,
        $_mediaDir,
        $_sitePaginationCount = 0;

    /**
     * run
     */
    public function run()
    {
        if(!Mage::getStoreConfig('bseller_sitemap/settings/enable_manufacture_sitemap')){
            return;
        }

        Mage::app()->setCurrentStore(Mage_Core_Model_App::DISTRO_STORE_ID);

        $this->_mediaDir         = Mage::getBaseDir('media');

        $this->_mageBaseUrl = Mage::getStoreConfig('bseller_sitemap/settings/bm_base_url');
        if (!$this->_mageBaseUrl) {
            Mage::app()->loadArea(Mage_Core_Model_App_Area::AREA_FRONTEND);
            $this->_mageBaseUrl      = Mage::getBaseUrl();
        }
        $this->_xmlFileNameTmp   = $this->_mediaDir.'/tmp_sitemap_manufacturer.xml';
        $this->_xmlFileName      = $this->_mediaDir.'/'.Mage::getStoreConfig('bseller_sitemap/settings/bm_filename');
        $this->_bmChangefreq     = Mage::getStoreConfig('bseller_sitemap/settings/bm_changefreq');
        $this->_date             = Mage::getModel('core/date')->date('Y-m-d');

        $limitManufacturer = Mage::getStoreConfig('bseller_sitemap/settings/bm_manufacturer_limit');

        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');

        $array = array();

        try {
            $results = $readConnection->fetchAll(
                "SELECT manufacturer_id,identifier FROM zeon_manufacturer WHERE status=1 ORDER BY manufacturer_id DESC limit {$limitManufacturer} ");

            foreach ($results as $manufacturer) {
                if (!isset($manufacturer['manufacturer_id'])) {
                    return;
                }
                $manufacturId = $manufacturer['manufacturer_id'];

                $collection = Mage::getModel('catalog/product')->getCollection();
                $collection->addAttributeToSelect(array('manufacturer,category_ids'));


                $collection->addFieldToFilter( array(
                    array('attribute' => 'manufacturer', 'eq' => $manufacturId),
                ));

                $collection->addFieldToFilter('status', array('eq' => '1'));

                $collection->load();

                $array[$manufacturId] = array();
                $array[$manufacturId]['ident'] = $manufacturer['identifier'];

                foreach ($collection as $product) {

                    if (!($product instanceof Mage_Catalog_Model_Product)) {
                        continue;
                    }
                    $categoryIds = $product->getCategoryIds();
                    foreach ($categoryIds as $id) {
                        $array[$manufacturId]['categories'][$id] = $id;
                    }
                }
            }

            $this->_setXMLheadSitemapIndex();
            $this->_setXMlhead();

            $paginationCount = 1;

            foreach ($array as $a) {

                if (!isset($a['categories'])) {
                    continue;
                }

                $cats = implode(',', $a['categories']);
                $query = "SELECT * FROM catalog_category_entity WHERE entity_id in ({$cats}) ORDER BY level DESC";
                $results = $readConnection->fetchAll($query);

                $loopCount = 1;

                $bm_just_one_category_link = (bool)Mage::getStoreConfig('bseller_sitemap/settings/bm_just_one_category_link');

                $mb_sitemap_pagination_limit = 0;
                if (Mage::getStoreConfig('bseller_sitemap/settings/bm_paginate_sitemap')) {
                    $mb_sitemap_pagination_limit = (int)Mage::getStoreConfig('bseller_sitemap/settings/mb_sitemap_pagination_limit');
                }

                foreach ($results as $r) {

                    if ($mb_sitemap_pagination_limit > 0) {
                        if ($paginationCount >= $mb_sitemap_pagination_limit) {
                            $this->_setXMlfooter($this->_sitePaginationCount);
                            $this->_setXMlhead();
                            $this->_sitePaginationCount++;
                            $paginationCount = 1;
                        }
                    }

                    if ($loopCount == 1) {
                        $this->_insertXMLcontent(null, $a['ident'], 0);
                        $loopCount++;
                    }

                    if ($bm_just_one_category_link) {
                        if ($loopCount > 2) {
                            continue;
                        }//just once for manufacturer
                    }
                    $this->_insertXMLcontent($r['entity_id'], $a['ident'], $r['level']);

                    $loopCount++;
                    $paginationCount++;
                }
            }

            $this->_setXMlfooter($this->_sitePaginationCount);
            $this->_setXMLfooterSitemapIndex();

        }catch(Exception $e){
            Mage::throwException($e);
        }
    }

    /**
     * _setXMLheadSitemapIndex
     */
    private function _setXMLheadSitemapIndex()
    {
        $content = <<< EOF
<?xml version="1.0" encoding="UTF-8"?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
EOF;

        $fileXML = file_put_contents($this->_xmlFileName.'_tmp_index.xml', $content);
        if (!$fileXML) {
            Mage::throwException('BSeller_Sitemap - Erro trying create manufacturer sitemap');
        }
    }

    /**
     * _setXMlhead
     */
    private function _setXMlhead()
    {

        $content = <<< EOF
<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
EOF;

        $fileXML = file_put_contents($this->_xmlFileNameTmp,$content);
        if(!$fileXML){
            Mage::throwException('BSeller_Sitemap - Erro trying create manufacturer sitemap');
        }
    }


    /**
     * _insertXMLcontent
     *
     * @param $catId
     * @param $identify
     * @param $level
     */
    private function _insertXMLcontent($catId,$identify,$level)
    {

        $path = $this->_mageBaseUrl.$identify;

        if($catId){
            $category = Mage::getModel('catalog/category')->load($catId);
            $catUrl   =  str_replace($this->_mageBaseUrl,'',$category->getUrl());

            if(strpos($catUrl,'catalog/category/')){
                return;
            }

            $path    .= '/'.$catUrl;
        }

        switch ($level){
            case 0:
                $this->_bmPriority = 0.8;
                break;
            case 1:
            case 2:
                $this->_bmPriority = 0.6;
                break;
            default:
                $this->_bmPriority = 0.5;
                break;
        }

        $xml = <<< EOF
<url>
    <loc>{$path}</loc>
    <lastmod>{$this->_date}</lastmod>
    <changefreq>{$this->_bmChangefreq}</changefreq>
    <priority>{$this->_bmPriority}</priority>
</url>
EOF;

        $fileXML = file_put_contents($this->_xmlFileNameTmp,$xml,FILE_APPEND);
        if(!$fileXML){
            Mage::throwException('BSeller_Sitemap - Erro trying create manufacturer sitemap');
        }

    }

    /**
     * _setXMlfooter
     * @param int $page
     */
    private function _setXMlfooter($page = 0)
    {
        if($page > 0){
            $this->_xmlFileNameTmp.'_'.$page;
        }


        $fileXML = file_put_contents($this->_xmlFileNameTmp,'</urlset>',FILE_APPEND);
        if(!$fileXML){
            Mage::throwException('BSeller_Sitemap - Erro trying create manufacturer sitemap');
        }
        rename($this->_xmlFileNameTmp,$this->_xmlFileName.'_'.$page.'.xml');

        $this->setXMLContentSitemapIndex($page);
    }

    private function setXMLContentSitemapIndex($page)
    {

        $xmlFile  = $this->_mageBaseUrl;
        $xmlFile .= 'media'.DIRECTORY_SEPARATOR;
        $xmlFile .= Mage::getStoreConfig('bseller_sitemap/settings/bm_filename');
        $xmlFile .= '_'.$page.'.xml';

        $xml = <<< EOF
<sitemap>
  <loc>{$xmlFile}</loc>
  <lastmod>{$this->_date}</lastmod>
</sitemap>
EOF;

        $fileXML = file_put_contents($this->_xmlFileName.'_tmp_index.xml',$xml,FILE_APPEND);
        if(!$fileXML){
            Mage::throwException('BSeller_Sitemap - Erro trying create manufacturer sitemap');
        }
    }

    /**
     * _setXMLfooterSitemapIndex
     */
    private function _setXMLfooterSitemapIndex()
    {

        $fileXML = file_put_contents($this->_xmlFileName.'_tmp_index.xml', '</sitemapindex>',FILE_APPEND);
        if(!$fileXML){
            Mage::throwException('BSeller_Sitemap - Erro trying create manufacturer sitemap');
        }
        rename($this->_xmlFileName.'_tmp_index.xml', $this->_xmlFileName.'.xml');
    }
}