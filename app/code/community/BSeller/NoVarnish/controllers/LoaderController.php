<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_NoVarnish
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_NoVarnish_LoaderController extends Mage_Core_Controller_Front_Action
{
    /**
     * Load blocks asynchronously
     *
     * @return $this
     */
    public function blocksAction()
    {
        /*if (!$this->getRequest()->isAjax()) {
            $this->_forward('denied');

            return $this;
        }*/

        $this->loadLayout();

        $response['error'] = false;
        
        $containers = $this->getRequest()->getParam('containers');
        $containers = (array) Zend_Json::decode($containers);
        
        try {
            foreach ($containers as $containerId => $container) {
                /** @var Mage_Core_Block_Abstract $block */
                $block = $this->getLayout()->getBlock($container['name']);

                /**
                 * Check if block exists
                 */
                if (!$block) {
                    continue;
                }

                /**
                 * Store loaded in ajax in block object
                 */
                $block->setData(BSeller_NoVarnish_Helper_Data::BLOCK_LOADED_AFTER_KEY, true);

                /**
                 * Append block html data
                 */
                $response['blocks'][$containerId] = $block->toHtml();
            }
        } catch (Exception $e) {
            Mage::logException($e);

            $response['message'] = $e->getMessage();
            $response['error']   = true;
        }

        $this->getResponse()->clearHeaders();
        $this->getResponse()->setBody(Zend_Json::encode($response));

        return $this;
    }
}
