<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_NoVarnish
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_NoVarnish_Helper_Data extends BSeller_Core_Helper_Data
{
    /**
     * Cookie key
     *
     * @const string
     */
    const COOKIE_KEY = 'bseller_novarnish';

    /**
     * Block loaded after key
     *
     * @const string
     */
    const BLOCK_LOADED_AFTER_KEY = 'loaded_after';

    /**
     * Block load after key
     *
     * @const string
     */
    const BLOCK_LOAD_AFTER_KEY = 'load_after';

    /**
     * Return if module is active
     *
     * @return bool
     */
    public function isActive()
    {
        return Mage::getStoreConfigFlag('bseller_novarnish/settings/active');
    }
}
