<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_NoVarnish
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_NoVarnish_Model_Observer
{
    /**
     * Prepare to remove cookie
     *
     * @return $this
     */
    public function prepareToRemoveCookie()
    {
        /**
         * Check if module is active
         */
        if (!Mage::helper('bseller_novarnish')->isActive()) {
            return $this;
        }

        /** @var array $cookies */
        $cookies = Mage::getModel('core/cookie')->get();

        foreach ($cookies as $key => $value) {
            if (strpos($key, BSeller_NoVarnish_Helper_Data::COOKIE_KEY . '_') === false) {
                continue;
            }

            /**
             * Remove cookie information
             */
            Mage::getModel('core/cookie')->delete($key, '/', null, '', '');
        }

        return $this;
    }

    /**
     * Prepare to load after
     *
     * @param Varien_Event_Observer $observer
     * @return $this
     */
    public function prepareToLoadAfter(Varien_Event_Observer $observer)
    {
        /**
         * Check if module is active
         */
        if (!Mage::getStoreConfigFlag('bseller_novarnish/settings/active')) {
            return $this;
        }
        
        /** @var Mage_Core_Block_Template $block */
        $block = $observer->getEvent()->getBlock();

        /**
         * It's informed in loader controller
         */
        if ($block->getData(BSeller_NoVarnish_Helper_Data::BLOCK_LOADED_AFTER_KEY)) {
            return $this;
        }

        /**
         * It's informed on layout xml
         */
        if (!$block->getData(BSeller_NoVarnish_Helper_Data::BLOCK_LOAD_AFTER_KEY)) {
            return $this;
        }

        /** @var BSeller_NoVarnish_Block_Container $container */
        $container = Mage::app()->getLayout()->createBlock('bseller_novarnish/container');
        $container->setBlockId(str_replace(['.', '_'], '-', $block->getNameInLayout()));
        $container->setBlockName($block->getNameInLayout());

        $observer->getData('transport')->setHtml($container->toHtml());

        return $this;
    }
}
