<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category BSeller
 * @package  BSeller_Core
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author Diogo Baracho <diogo.baracho@e-smart.com.br>
 */ 
class BSeller_CMSCategory_Model_Categorycms extends Mage_Core_Model_Abstract
{

    protected function _construct()
    {
        $this->_init('bseller_cmscategory/categorycms');
    }

}