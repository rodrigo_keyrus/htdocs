<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_CategoryCMS
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Diogo Baracho <diogo.baracho@e-smart.com.br>
 */
class BSeller_CMSCategory_Model_Config_Source_Category extends Varien_Object{

    static public function toOptionArray()
    {
        $return = array();

        $rootCat = Mage::app()->getStore()->getRootCategoryId();
        if(!$rootCat){$rootCat=2;}
        $categories = explode(',',Mage::getModel('catalog/category')->load($rootCat)->getChildren());
        foreach($categories as $cat){
            $category = Mage::getModel('catalog/category')->load($cat);
            $return[$category->getId()] = Mage::helper('adminhtml')->__($category->getName());

        }
        return $return;
    }

}