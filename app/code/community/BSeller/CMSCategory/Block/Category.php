<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category BSeller
 * @package  BSeller_Core
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author Diogo Baracho <diogo.baracho@e-smart.com.br>
 */
class BSeller_CMSCategory_Block_Category extends Mage_Core_Block_Template
{
    /**
     * Get category cms collection
     *
     * @return BSeller_CMSCategory_Model_Resource_Categorycms_Collection|null|object
     */
    public function getCategoryCMSCollection()
    {
        $category = Mage::registry('current_category');
        if (!$category) {
            return null;
        }

        $categoryId = $category->getId();
        if (!$categoryId) {
            return null;
        }
        $collection = Mage::getModel('bseller_cmscategory/categorycms')
            ->getCollection();
        $collection->addFieldToFilter('identifier', $categoryId.'_1');
        $collection->addFieldToFilter('status', 1);
        $collection->setOrder('sort_order', 'asc');

        return $collection;
    }

    /**
     * Get content rows and columns
     *
     * @return array
     */
    public function getCategoryCMSContent()
    {
        $collection = $this->getCategoryCMSCollection();
        $row       = 0;
        $content   = array();

        foreach ($collection as $c) {

            for ($x = 1; $x <= 4; $x++) {
                $box = trim($c->getData('content_box' . $x));
                if (!empty($box)) {
                    $content[$row]['columns'][$x] = $box;
                    $content[$row]['countColumns'] = $x;
                }
            }

            $row++;
        }

        return $content;
    }

    /**
     * Prepare HTML content
     *
     * @return string
     */
    public function getHTMLProcessor()
    {
        /* @var $helper Mage_Cms_Helper_Data */
        $helper = Mage::helper('cms');
        $processor = $helper->getPageTemplateProcessor();
        return $processor;
    }
}