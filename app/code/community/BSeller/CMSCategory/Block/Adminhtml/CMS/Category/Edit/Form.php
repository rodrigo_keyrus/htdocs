<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category BSeller
 * @package  BSeller_Core
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author Diogo Baracho <diogo.baracho@e-smart.com.br>
 */
class BSeller_CMSCategory_Block_Adminhtml_CMS_Category_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{

    protected function _getModel(){
        return Mage::registry('current_model');
    }

    protected function _getHelper(){
        return Mage::helper('bseller_cmscategory');
    }

    protected function _getModelTitle(){
        return 'Categorycms';
    }

    protected function _prepareForm()
    {
        $model  = $this->_getModel();
        $modelTitle = $this->_getModelTitle();
        $form   = new Varien_Data_Form(array(
            'id'        => 'edit_form',
            'action'    => $this->getUrl('*/*/save'),
            'method'    => 'post'
        ));

        $fieldset   = $form->addFieldset('base_fieldset', array(
            'legend'    => $this->_getHelper()->__("$modelTitle Information"),
            'class'     => 'fieldset-wide',
        ));

        if ($model && $model->getId()) {
            $modelPk = $model->getResource()->getIdFieldName();
            $fieldset->addField($modelPk, 'hidden', array(
                'name' => $modelPk,
            ));
        }


        $fieldset->addField('category_id', 'select', array(
            'label' => Mage::helper('bseller_cmscategory')->__('Category'),
            'name' => 'category_id',
            'required'  => true,
            'options' => Mage::getModel('bseller_cmscategory/config_source_category')->toOptionArray()
        ));

//        $fieldset->addField('position', 'select', array(
//            'name'      => 'position',
//            'label'     => $this->_getHelper()->__('Position'),
//            'title'     => $this->_getHelper()->__('Position'),
//            'required'  => true,
//            'options'   => array( 'after'=>'After Content','before' => 'Before Content' ),
//            'style'     => 'css rules',
//            'class'     => 'css classes',
//        ));

        $fieldset->addField('status', 'select', array(
            'name'      => 'status',
            'label'     => $this->_getHelper()->__('Status'),
            'title'     => $this->_getHelper()->__('Status'),
            'required'  => true,
            'options'   => array( '1'=>'Enabled','0' => 'Disabled' ),
            'style'     => 'css rules',
            'class'     => 'css classes',
        ));
        $fieldset->addField('sort_order', 'text', array(
            'label' => Mage::helper('bseller_cmscategory')->__('Sort Order'),
            'name' => 'sort_order',
            'required'  => true
        ));


        $fieldset->addField('content_box1', 'editor', array(
            'name'      => 'content_box1',
            'label'     => Mage::helper('bseller_cmscategory')->__('Column Content Box 1'),
            'title'     => Mage::helper('bseller_cmscategory')->__('Column Content Box 1'),
            'style'     => 'height:15em',
            'config'    => Mage::getSingleton('cms/wysiwyg_config')->getConfig(array('hidden'=>true)),
            'wysiwyg'   => true,
            'required'  => true
        ));
        $fieldset->addField('content_box2', 'editor', array(
            'name'      => 'content_box2',
            'label'     => Mage::helper('bseller_cmscategory')->__('Column Content Box 2'),
            'title'     => Mage::helper('bseller_cmscategory')->__('Column Content Box 2'),
            'style'     => 'height:15em',
            'config'    => Mage::getSingleton('cms/wysiwyg_config')->getConfig(array('hidden'=>true)),
            'wysiwyg'   => true,
            'required'  => false,
        ));
        $fieldset->addField('content_box3', 'editor', array(
            'name'      => 'content_box3',
            'label'     => Mage::helper('bseller_cmscategory')->__('Column Content Box 3'),
            'title'     => Mage::helper('bseller_cmscategory')->__('Column Content Box 3'),
            'style'     => 'height:15em',
            'config'    => Mage::getSingleton('cms/wysiwyg_config')->getConfig(array('hidden'=>true)),
            'wysiwyg'   => true,
            'required'  => false,
        ));
        $fieldset->addField('content_box4', 'editor', array(
            'name'      => 'content_box4',
            'label'     => Mage::helper('bseller_cmscategory')->__('Column Content Box 4'),
            'title'     => Mage::helper('bseller_cmscategory')->__('Column Content Box 4'),
            'style'     => 'height:15em',
            'config'    => Mage::getSingleton('cms/wysiwyg_config')->getConfig(array('hidden'=>true)),
            'wysiwyg'   => true,
            'required'  => false,
        ));

//        $fieldset->addField('name', 'text' /* select | multiselect | hidden | password | ...  */, array(
//            'name'      => 'name',
//            'label'     => $this->_getHelper()->__('Label here'),
//            'title'     => $this->_getHelper()->__('Tooltip text here'),
//            'required'  => true,
//            'options'   => array( OPTION_VALUE => OPTION_TEXT, ),                 // used when type = "select"
//            'values'    => array(array('label' => LABEL, 'value' => VALUE), ),    // used when type = "multiselect"
//            'style'     => 'css rules',
//            'class'     => 'css classes',
//        ));
//          // custom renderer (optional)
//          $renderer = $this->getLayout()->createBlock('Block implementing Varien_Data_Form_Element_Renderer_Interface');
//          $field->setRenderer($renderer);

//      // New Form type element (extends Varien_Data_Form_Element_Abstract)
//        $fieldset->addType('custom_element','MyCompany_MyModule_Block_Form_Element_Custom');  // you can use "custom_element" as the type now in ::addField([name], [HERE], ...)


        if($model){
            $form->setValues($model->getData());
        }
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }

}
