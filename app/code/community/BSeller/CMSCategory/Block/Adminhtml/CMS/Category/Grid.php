<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category BSeller
 * @package  BSeller_Core
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author Diogo Baracho <diogo.baracho@e-smart.com.br>
 */
class BSeller_CMSCategory_Block_Adminhtml_CMS_Category_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct()
    {
        parent::__construct();
        $this->setId('grid_id');
//         $this->setDefaultSort('sort_order');
        $this->setDefaultDir('asc');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('bseller_cmscategory/categorycms')->getCollection();
        $collection->setOrder('category_id','asc');
        $collection->setOrder('sort_order','asc');
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {

        $this->addColumn('category_id',
            array(
                'header'=> $this->__('Category Id'),
                'width' => '5px',
                'index' => 'category_id',
                'type'    => 'options',
                'options' => Mage::getModel('bseller_cmscategory/config_source_category')
                    ->toOptionArray()

            )
        );
        $this->addColumn('sort_order',
            array(
                'header'=> $this->__('sort order'),
                'width' => '50px',
                'index' => 'sort_order'
            )
        );
        $this->addColumn('status',
            array(
                'header'=> $this->__('Status'),
                'width' => '50px',
                'index' => 'status',
                'type'    => 'options',
                'options'   => array( '1'=>$this->__('Enabled'),'0' => $this->__('Disabled'))
            )
        );

        $this->addColumn('content_box1',
            array(
                'header'=> $this->__('Column 1'),
                'width' => '50px',
                'index' => 'content_box1',
                'frame_callback' => array($this, 'decorateTrim'),
            )
        );
        $this->addColumn('content_box2',
            array(
                'header'=> $this->__('Column 2'),
                'width' => '50px',
                'index' => 'content_box2',
                'frame_callback' => array($this, 'decorateTrim'),
            )
        );
        $this->addColumn('content_box3',
            array(
                'header'=> $this->__('Column 3'),
                'width' => '50px',
                'index' => 'content_box3',
                'frame_callback' => array($this, 'decorateTrim'),
            )
        );
        $this->addColumn('content_box4',
            array(
                'header'=> $this->__('Column 4'),
                'width' => '50px',
                'index' => 'content_box4',
                'frame_callback' => array($this, 'decorateTrim'),
            )
        );

        $this->addExportType('*/*/exportCsv', $this->__('CSV'));

        $this->addExportType('*/*/exportExcel', $this->__('Excel XML'));
        
        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
       return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

        protected function _prepareMassaction()
    {
        $modelPk = Mage::getModel('bseller_cmscategory/categorycms')->getResource()->getIdFieldName();
        $this->setMassactionIdField($modelPk);
        $this->getMassactionBlock()->setFormFieldName('ids');
        // $this->getMassactionBlock()->setUseSelectAll(false);
        $this->getMassactionBlock()->addItem('delete', array(
             'label'=> $this->__('Delete'),
             'url'  => $this->getUrl('*/*/massDelete'),
        ));
        return $this;
    }

    /**
     * Decorate content value
     *
     * @param $value
     *
     * @return string
     */
    public function decorateTrim($value)
    {
        return sprintf('<span title="%s">%s</span>', $value, mb_strimwidth($value, 0, 40, "..."));
    }

    public function decorateStatus($value){
        if ($value == 1) {
            return $this->__('Enabled');
        }
        return $this->__('Disabled');
    }
}