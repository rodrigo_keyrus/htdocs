<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category BSeller
 * @package  BSeller_Core
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author Diogo Baracho <diogo.baracho@e-smart.com.br>
 */ 
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();


$installer->run("DROP TABLE IF EXISTS {$this->getTable('bseller_cmscategory/bseller_categorycms')}");
$table = $installer->getConnection()
    ->newTable($this->getTable('bseller_cmscategory/bseller_categorycms'))
    ->addColumn('categorycms_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true
    ), 'categorycms_id')
    ->addColumn('category_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false
    ), 'category_id')
    ->addColumn('identifier', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'unsigned'  => true,
        'nullable'  => false,
        'default'   => '',
    ), 'Identifier')
    ->addColumn('position', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'unsigned'  => true,
        'nullable'  => false,
        'default'   => '',
    ), 'position')

    ->addColumn('sort_order', Varien_Db_Ddl_Table::TYPE_SMALLINT, 6, array(
        'unsigned'  => true,
        'nullable'  => true,
        'default'   => 0,
    ), 'sort_order')

    ->addColumn('content_box1', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'unsigned'  => true,
        'nullable'  => true,
        'default'   => '',
    ), 'content_box1')
    ->addColumn('content_box2', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'unsigned'  => true,
        'nullable'  => true,
        'default'   => '',
    ), 'content_box2')
    ->addColumn('content_box3', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'unsigned'  => true,
        'nullable'  => true,
        'default'   => '',
    ), 'content_box3')
    ->addColumn('content_box4', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'unsigned'  => true,
        'nullable'  => true,
        'default'   => '',
    ), 'content_box4')

    ->addColumn('status', Varien_Db_Ddl_Table::TYPE_TINYINT, null, array(
        'unsigned'  => true,
        'nullable'  => true,
        'default'   => 0,
    ), 'Status')

    ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
    ), 'Creation Time')
    ->addColumn('updated_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
    ), 'Update Time')

    ->addIndex($installer->getIdxName($this->getTable('bseller_cmscategory/bseller_categorycms'), array('identifier')),
        array('identifier'))

    ->setComment($this->getTable('bseller_cmscategory/bseller_categorycms'));
$installer->getConnection()->createTable($table);


$installer->endSetup();