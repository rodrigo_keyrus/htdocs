<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category    BSeller
 * @package     BSeller_Resque
 *
 * @copyright   Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author      Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

/**
 * Resque model
 *
 * @category    BSeller
 * @package     BSeller_Resque
 * @author      Alan Carvalho <alan.carvalho@e-smart.com.br>
 */
class BSeller_Resque_Model_Resque extends BSeller_Resque_Model_Abstract
{
    /**
     * Return resque object
     *
     * @return Resque
     */
    public function getParent()
    {
        return $this->_resque;
    }

    /**
     * Set data in queue
     *
     * @param string $queue
     * @param string $class
     * @param array $args
     * @param bool $trackStatus
     * @return bool|string
     */
    public function enqueue($queue, $class, $args, $trackStatus = true)
    {
        return $this->getParent()->enqueue($queue, $class, $args, $trackStatus);
    }

    /**
     * Set multiple data in queue and slice then
     *
     * @param string $queue
     * @param string $class
     * @param array $args
     * @param int $workers
     * @param bool $trackStatus
     * @return array
     */
    public function enqueueMultiple($queue, $class, $args, $workers, $trackStatus = true)
    {
        $trackIds  = [];
        $argsCount = count($args);
        $queueSize = floor($argsCount / $workers);
        $diff      = 0;

        if (!($argsCount % $workers) === 0) {
            $diff = $argsCount - ($queueSize * $workers);
        }

        for ($i = 0; $i < $workers; $i++) {
            $currentQueue = $this->getHash($queue);

            if ((($i + 1) === $workers) && ($diff > 0)) {
                $queueSize = $queueSize + $diff;
            }

            $queueSizeCount = 0;

            foreach ($args as $key => $arg) {
                $trackIds[] = $this->enqueue($currentQueue, $class, $arg, $trackStatus);

                unset($args[$key]);

                $queueSizeCount++;

                if ($queueSizeCount == $queueSize) {
                    break;
                }
            }
        }

        return $trackIds;
    }

    /**
     * Return queue status
     *
     * @param string $trackingToken
     * @return string
     */
    public function status($trackingToken)
    {
        $status = new Resque_Job_Status($trackingToken);

        return $status->get();
    }

    /**
     * Return worker objects
     *
     * @return array
     */
    public function workers()
    {
        return Resque_Worker::all();
    }

    /**
     * Return worker object
     *
     * @param $queue
     * @return bool|Resque_Worker
     */
    public function worker($queue)
    {
        /** @var Resque_Worker $worker */
        foreach ($this->workers() as $worker) {
            $workerQueue = $this->workerInfo($worker, self::INFO_QUEUE);

            if ($workerQueue === $queue) {
                return $worker;
            }
        }

        return false;
    }

    /**
     * Return an array of process ids for all of the Resque workers currently running on this machine
     *
     * @return array
     */
    public function workerPids()
    {
        $workerPids = [];

        foreach ($this->workers() as $worker) {
            $workerPids[] = $this->workerInfo($worker, self::INFO_PID);
        }

        return $workerPids;
    }

    /**
     * Return worker pid
     *
     * @param $queue
     * @return bool|int
     */
    public function workerPid($queue)
    {
        /** @var bool|Resque_Worker $worker */
        $worker = $this->worker($queue);

        if (!$worker) {
            return false;
        }

        return $this->workerInfo($worker, self::INFO_PID);
    }

    /**
     * Return worker child pid
     *
     * @param string $queue
     * @return int
     */
    public function workerChildPid($queue)
    {
        $workerChildPids = [];

        exec(sprintf(self::WORKER_SEARCH_SCRIPT, '[r]esque-' . Resque::VERSION . ': Processing ' . $queue), $lines);

        foreach ($lines as $line) {
            list($workerChildPids[]) = explode(' ', trim($line), 2);
        }

        return array_shift($workerChildPids);
    }

    /**
     * Return worker info based on id
     *
     * @param Resque_Worker $worker
     * @param string $infoKey
     * @return int|null|string
     */
    public function workerInfo($worker, $infoKey)
    {
        list($hostname, $pid, $queues) = explode(':', (string) $worker, 3);

        switch ($infoKey) {
            case self::INFO_HOSTNAME:
                return (string) $hostname;
            case self::INFO_PID:
                return (int) $pid;
            case self::INFO_QUEUE:
            case self::INFO_QUEUES:
                $queues = explode(',', $queues);
                $queue  = array_shift($queues);

                return (string) $queue;
        }

        return null;
    }

    /**
     * Return worker collection
     *
     * @return Varien_Data_Collection
     */
    public function getWorkerCollection()
    {
        $collection = new Varien_Data_Collection();

        /** @var Resque_Worker $worker */
        foreach ($this->workers() as $worker) {
            $object = new Varien_Object();

            $queue = $this->workerInfo($worker, self::INFO_QUEUE);

            $workerChildPid = $this->workerChildPid($queue);

            if (is_null($workerChildPid)) {
                $workerChildPid = '--';
            }

            $object->setData('queue', $queue);
            $object->setData('queue_pending', $this->getParent()->size($queue));
            $object->setData('queue_processed', $worker->getStat('processed'));
            $object->setData('queue_failed', $worker->getStat('failed'));
            $object->setData('worker_pid', $this->workerInfo($worker, self::INFO_PID));
            $object->setData('worker_child_pid', $workerChildPid);

            $collection->addItem($object);
        }

        return $collection;
    }

    /**
     * Return unique hash
     *
     * @param string $text
     * @return string
     */
    protected function getHash($text)
    {
        $uniqueId = uniqid(mt_rand(), true);
        $crypt    = md5($uniqueId);
        $hash     = ($text . '_' . $crypt);

        return $hash;
    }
}
