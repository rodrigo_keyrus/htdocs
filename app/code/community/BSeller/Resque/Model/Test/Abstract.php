<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category    BSeller
 * @package     BSeller_Resque
 *
 * @copyright   Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author      Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

/**
 * Abstract test model
 *
 * @category    BSeller
 * @package     BSeller_Resque
 * @author      Alan Carvalho <alan.carvalho@e-smart.com.br>
 */
abstract class BSeller_Resque_Model_Test_Abstract
{
    /**
     * Default queue
     *
     * @var string
     */
    protected $_queue = 'test';
    
    /**
     * Default class
     *
     * @var string
     */
    protected $_class = 'BSeller_Resque_Model_Test_Job';

    /**
     * Default arguments
     *
     * @var array
     */
    protected $_args = [
        'name'  => 'John Doe',
        'email' => 'john.doe@example.com'
    ];

    /**
     * Create queue
     *
     * @return array
     */
    abstract public function create();

    /**
     * Return resque object model
     *
     * @return BSeller_Resque_Model_Resque
     */
    protected function getResque()
    {
        return Mage::getSingleton('bseller_resque/resque');
    }
}
