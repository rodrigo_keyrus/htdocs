<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category    BSeller
 * @package     BSeller_Resque
 *
 * @copyright   Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author      Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

/**
 * Test multiple model
 *
 * @category   BSeller
 * @package    BSeller_Resque
 * @author     Alan Carvalho <alan.carvalho@e-smart.com.br>
 */
class BSeller_Resque_Model_Test_Multiple extends BSeller_Resque_Model_Test_Abstract
{
    /**
     * Default slice number
     *
     * @var int
     */
    protected $_slice = 10;

    /**
     * Create multiple queues
     *
     * @return array
     */
    public function create()
    {
        $args = [];

        for ($i = 0; $i < 1000; $i++) {
            $args[] = $this->_args;
        }

        return $this->getResque()->enqueueMultiple($this->_queue, $this->_class, $args, $this->_slice);
    }
}
