<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category    BSeller
 * @package     BSeller_Resque
 *
 * @copyright   Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author      Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

/**
 * Config model
 *
 * @category   BSeller
 * @package    BSeller_Resque
 * @author     Alan Carvalho <alan.carvalho@e-smart.com.br>
 */
class BSeller_Resque_Model_Config extends BSeller_Core_Model_Abstract
{
    /**
     * App to include
     *
     * @var string
     */
    protected $_appFile = 'app/Mage.php';

    /**
     * Shell file path
     *
     * @var string
     */
    protected $_shellFile = 'shell/bseller/resque.php';

    /**
     * Library shell file path
     *
     * @var string
     */
    protected $_libShellFile = 'lib/Resque/bin/resque';

    /**
     * Log file path
     *
     * @var string
     */
    protected $_logFile = 'resque.log';

    /**
     * Required PHP extension
     *
     * @var array
     */
    protected $_requiredExtensions = ['posix', 'redis'];

    /**
     * Return store config data
     *
     * @param $name
     * @return string
     */
    public function get($name)
    {
        return Mage::getStoreConfig('bseller_resque/settings/' . $name);
    }

    /**
     * Return app to include
     *
     * @return string
     */
    public function getAppFile()
    {
        return Mage::getBaseDir('base') . DS . $this->_appFile;
    }

    /**
     * Return shell file path
     *
     * @return string
     */
    public function getShellFile()
    {
        return Mage::getBaseDir('base') . DS . $this->_shellFile;
    }

    /**
     * Return library shell file path
     *
     * @return string
     */
    public function getLibShellFile()
    {
        return Mage::getBaseDir('base') . DS . $this->_libShellFile;
    }

    /**
     * Return PID file path
     *
     * @return string
     */
    public function getLogFile()
    {
        return Mage::getBaseDir('log') . DS . $this->_logFile;
    }

    /**
     * Return required extensions
     *
     * @return array
     */
    public function getRequiredExtensions()
    {
        return $this->_requiredExtensions;
    }
}
