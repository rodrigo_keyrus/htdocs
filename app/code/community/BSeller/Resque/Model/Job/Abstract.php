<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category    BSeller
 * @package     BSeller_Resque
 *
 * @copyright   Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author      Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

/**
 * Job abstract model
 *
 * @category    BSeller
 * @package     BSeller_Resque
 * @author      Alan Carvalho <alan.carvalho@e-smart.com.br>
 *
 * @method      string getQueue()
 * @method      array getArgs()
 * @method      Resque_Job getJob()
 */
abstract class BSeller_Resque_Model_Job_Abstract extends Varien_Object
{
    /**
     * Initialize application with code (store, website code)
     *
     * @var string
     */
    protected $_appCode = 'admin';

    /**
     * Initialize application code type (store, website, store_group)
     *
     * @var string
     */
    protected $_appType = 'store';

    /**
     * Constructor initialization
     */
    public function _construct()
    {
        Mage::app($this->_appCode, $this->_appType);
        
        parent::_construct();
    }

    /**
     * Set up environment for this job
     *
     * @return $this
     */
    public function setUp()
    {
        return $this;
    }

    /**
     * Run job
     *
     * @return $this
     */
    abstract public function perform();

    /**
     * Remove environment for this job
     *
     * @return $this
     */
    public function tearDown()
    {
        return $this;
    }
}
