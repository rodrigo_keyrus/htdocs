<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category    BSeller
 * @package     BSeller_Resque
 *
 * @copyright   Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author      Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

/**
 * Include third-party extension
 */
require_once Mage::getBaseDir('lib') . DS . 'Resque' . DS . 'vendor' . DS . 'autoload.php';

/**
 * Abstract model
 *
 * @category   BSeller
 * @package    BSeller_Resque
 * @author     Alan Carvalho <alan.carvalho@e-smart.com.br>
 */
abstract class BSeller_Resque_Model_Abstract extends Varien_Object
{
    /**
     * Shell script to search process in execution
     *
     * @cont string
     */
    const WORKER_SEARCH_SCRIPT = 'ps -A -o pid,command | grep \'%s\'';

    /**
     * Shell script to search manager process in execution
     *
     * @cont string
     */
    const MANAGER_SEARCH_SCRIPT = 'ps -A -o pid,command | grep \'[php] %s/%s %s %s\'';

    /**
     * Shell script to start manager process
     *
     * @const string
     */
    const MANAGER_START_SCRIPT = 'php %s/%s %s %s > /dev/null 2>&1 &';

    /**
     * Worker info hostname
     *
     * @cont string
     */
    const INFO_HOSTNAME = 'hostname';

    /**
     * Worker info pid
     *
     * @cont string
     */
    const INFO_PID = 'pid';

    /**
     * Worker info queue
     *
     * @cont string
     */
    const INFO_QUEUE = 'queue';

    /**
     * Worker info queues
     *
     * @cont string
     */
    const INFO_QUEUES = self::INFO_QUEUE;

    /**
     * Config object
     *
     * @var BSeller_Resque_Model_Config
     */
    protected $_config;

    /**
     * Resque object
     *
     * @var Resque
     */
    protected $_resque;
    
    /**
     * Constructor initialization
     * Connect with Redis server
     */
    public function _construct()
    {
        parent::_construct();

        /**
         * Set config in attribute
         */
        $this->_config = Mage::getModel('bseller_resque/config');

        /**
         * Start connection with Redis server
         */
        $this->connect();
    }

    /**
     * Connect with Redis server
     *
     * @return $this
     */
    public function connect()
    {
        if ($this->_resque) {
            return $this;
        }

        $host     = (string) $this->getConfig()->get('host');
        $port     = (string) $this->getConfig()->get('port');
        $prefix   = (string) $this->getConfig()->get('prefix');
        $database = (int)    $this->getConfig()->get('database');

        $this->_resque = new Resque();
        $this->_resque->setBackend($host . ':' . $port, $database);
        $this->_resque->redis()->prefix($prefix);

        return $this;
    }

    /**
     * Return config object
     *
     * @return BSeller_Resque_Model_Config
     */
    public function getConfig()
    {
        return $this->_config;
    }
}
