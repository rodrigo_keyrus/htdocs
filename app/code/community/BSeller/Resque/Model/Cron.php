<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category    BSeller
 * @package     BSeller_Resque
 *
 * @copyright   Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author      Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

/**
 * Cron model
 *
 * @category    BSeller
 * @package     BSeller_Resque
 * @author      Alan Carvalho <alan.carvalho@e-smart.com.br>
 */
class BSeller_Resque_Model_Cron
{
    /**
     * Start manager process if is not running
     *
     * @return $this
     */
    public function start()
    {
        /** @var BSeller_Resque_Model_Config $config */
        $config = Mage::getModel('bseller_resque/config');

        $shellFile = $config->getShellFile();
        $logFile   = $config->getLogFile();

        shell_exec("php {$shellFile} start >> {$logFile} 2>&1 &");

        return $this;
    }
}
