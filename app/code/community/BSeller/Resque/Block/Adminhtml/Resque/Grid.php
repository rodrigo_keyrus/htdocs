<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category    BSeller
 * @package     BSeller_Resque
 *
 * @copyright   Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author      Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

/**
 * Adminhtml grid widget block
 *
 * @category    BSeller
 * @package     BSeller_Resque
 * @author      Alan Carvalho <alan.carvalho@e-smart.com.br>
 */
class BSeller_Resque_Block_Adminhtml_Resque_Grid extends BSeller_Core_Block_Adminhtml_Widget_Grid
{
    /**
     * Constructor initialization
     */
    public function __construct()
    {
        parent::__construct();

        $this->setId('resque_grid');
        $this->setSaveParametersInSession(true);
        $this->setFilterVisibility(false);
        $this->setPagerVisibility(false);
    }

    /**
     * Prepare grid collection
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        /** @var Varien_Data_Collection $collection */
        $collection = Mage::getModel('bseller_resque/resque')->getWorkerCollection();

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * Prepare columns of grid
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     * @throws Exception
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'queue',
            [
                'header'   => $this->__('Queue'),
                'type'     => 'text',
                'index'    => 'queue',
                'sortable' => false,
                'filter'   => false
            ]
        );

        $this->addColumn(
            'queue_pending',
            [
                'header'   => $this->__('Pending'),
                'type'     => 'number',
                'index'    => 'queue_pending',
                'sortable' => false,
                'filter'   => false
            ]
        );

        $this->addColumn(
            'queue_processed',
            [
                'header'   => $this->__('Processed'),
                'type'     => 'number',
                'index'    => 'queue_processed',
                'sortable' => false,
                'filter'   => false
            ]
        );

        $this->addColumn(
            'queue_failed',
            [
                'header'   => $this->__('Failed'),
                'type'     => 'number',
                'index'    => 'queue_failed',
                'sortable' => false,
                'filter'   => false
            ]
        );

        $this->addColumn(
            'worker_pid',
            [
                'header'   => $this->__('Worker PID'),
                'type'     => 'text',
                'index'    => 'worker_pid',
                'sortable' => false,
                'filter'   => false
            ]
        );

        $this->addColumn(
            'worker_child_pid',
            [
                'header'   => $this->__('Worker Child PID'),
                'type'     => 'text',
                'index'    => 'worker_child_pid',
                'sortable' => false,
                'filter'   => false
            ]
        );

        $this->addColumn(
            'action',
            [
                'header'  => $this->__('Action'),
                'width'   => '50',
                'type'    => 'action',
                'getter'  => 'getQueue',
                'actions' => [
                    [
                        'caption' => $this->__('Clear'),
                        'url'     => [
                            'base' => '*/*/clear'
                        ],
                        'field' => 'id'
                    ]
                ],
                'filter'   => false,
                'sortable' => false,
                'index'    => 'stores'
            ]
        );

        return parent::_prepareColumns();
    }
}
