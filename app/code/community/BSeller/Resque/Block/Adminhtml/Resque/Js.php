<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category    BSeller
 * @package     BSeller_Resque
 *
 * @copyright   Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author      Alan Carvalho <alan.carvalho@e-smart.com.br>
 * @author      Gabriel da Gama <gabriel.gama@e-smart.com.br>
 */

/**
 * Adminhtml javascript block
 *
 * @category    BSeller
 * @package     BSeller_Resque
 * @author      Alan Carvalho <alan.carvalho@e-smart.com.br>
 */
class BSeller_Resque_Block_Adminhtml_Resque_Js extends Mage_Core_Block_Text
{
    /**
     * Constructor initialization
     */
    public function _construct()
    {
        $gridActionUrl = Mage::helper('adminhtml')->getUrl('adminhtml/resque/ajax');

        $javaScript = <<<TEXT
<script type='text/javascript'>
//<![CDATA[
function refreshGridContent(){new Ajax.Request('{$gridActionUrl}',{method:'GET',onSuccess:function(e){
if(200!=e.status)return this;var n=$('resque_grid'),t=e.responseText;n.update(t)}})}
document.observe('dom:loaded',function(){setInterval(function(){refreshGridContent()},5e3)});
//]]>
</script>
TEXT;
        
        $this->setData('text', $javaScript);

        parent::_construct();
    }
}
