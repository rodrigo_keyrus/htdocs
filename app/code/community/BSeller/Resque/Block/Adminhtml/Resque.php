<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category    BSeller
 * @package     BSeller_Resque
 *
 * @copyright   Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author      Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

/**
 * Adminhtml grid container block
 *
 * @category    BSeller
 * @package     BSeller_Resque
 * @author      Alan Carvalho <alan.carvalho@e-smart.com.br>
 */
class BSeller_Resque_Block_Adminhtml_Resque extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * Constructor initialization
     */
    public function __construct()
    {
        parent::__construct();

        $this->_controller = 'adminhtml_resque';
        $this->_blockGroup = 'bseller_resque';
        $this->_headerText = $this->__('Panel');

        $this->_addButton(
            'clear_all',
            [
                'label'   => $this->__('Clear All'),
                'onclick' => 'setLocation(\'' . $this->getUrl('*/*/clearAll') . '\')',
                'class'   => 'del'
            ]
        );

        $this->removeButton('add');
    }
}
