<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category    BSeller
 * @package     BSeller_Resque
 *
 * @copyright   Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author      Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

/**
 * Adminhtml controller
 *
 * @category    BSeller
 * @package     BSeller_Resque
 * @author      Alan Carvalho <alan.carvalho@e-smart.com.br>
 */
class BSeller_Resque_Adminhtml_ResqueController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Index action
     *
     * @return void
     */
    public function indexAction()
    {
        $this->_redirect('*/*/dashboard');
    }

    /**
     * Dashboard action
     *
     * @return void
     */
    public function dashboardAction()
    {
        /** @var BSeller_Resque_Model_Config $config */
        $config = Mage::getModel('bseller_resque/config');

        /**
         * Check if required extensions was installed
         */
        foreach ($config->getRequiredExtensions() as $extension) {
            if (extension_loaded($extension)) {
                continue;
            }

            $this->_getSession()->addWarning($this->__('The php-%s extension is required.', $extension));
        }

        /**
         * Check if module is enabled
         */
        if (!$config->get('active')) {
            $this->_getSession()->addWarning($this->__('Please, active on System > Configuration > BSeller / Resque.'));
        }

        /** @var BSeller_Resque_Block_Adminhtml_Resque $block */
        $block = $this->getLayout()->createBlock('bseller_resque/adminhtml_resque');

        /** @var BSeller_Resque_Block_Adminhtml_Resque_Js $jsBlock */
        $jsBlock = $this->getLayout()->createBlock('bseller_resque/adminhtml_resque_js');

        $this->loadLayout()
            ->_addContent($block)
            ->_addContent($jsBlock)
            ->_setActiveMenu('bseller/bseller_resque')
            ->_title($this->__('Panel'));

        $this->renderLayout();
    }

    /**
     * Ajax load of resque grid action
     *
     * @return void
     */
    public function ajaxAction()
    {
        /** @var BSeller_Resque_Block_Adminhtml_Resque $block */
        $block = $this->getLayout()->createBlock('bseller_resque/adminhtml_resque_grid');

        $this->getResponse()->setBody($block->toHtml());
    }

    /**
     * Clear queue action
     *
     * @return void
     */
    public function clearAction()
    {
        $queue = $this->getRequest()->getParam('id');

        /** @var BSeller_Resque_Model_Resque $resque */
        $resque = Mage::getModel('bseller_resque/resque');

        try {
            /**
             * Remove all items from queue
             *
             * @var int $items
             */
            $items = $resque->getParent()->removeQueue($queue);

            $this->_getSession()->addSuccess($this->__('%s items were removed from the %s queue.', $items, $queue));
        } catch (Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        }

        $this->_redirectReferer();
    }

    /**
     * Clear all queues action
     *
     * @return void
     */
    public function clearAllAction()
    {
        /** @var BSeller_Resque_Model_Resque $resque */
        $resque = Mage::getModel('bseller_resque/resque');

        try {
            /** @var Resque_Worker $worker */
            foreach ($resque->workers() as $worker) {
                $queue = $resque->workerInfo($worker, $resque::INFO_QUEUE);

                /**
                 * Remove all items from queue
                 *
                 * @var int $items
                 */
                $items = $resque->getParent()->removeQueue($queue);

                $this->_getSession()->addSuccess(
                    $this->__('%s item(s) removed from the %s queue.', $items, $queue)
                );
            }
        } catch (Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        }

        $this->_redirectReferer();
    }

    /**
     * Is allowed to access
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('bseller/bseller_resque/resque');
    }
}
