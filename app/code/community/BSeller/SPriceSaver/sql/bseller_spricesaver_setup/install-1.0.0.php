<?php

$installer = $this;

$installer->startSetup();

$setup = Mage::getResourceModel('sales/setup', 'default_setup');

/**
 * Create attribute sales flat item
 */
$setup->addAttribute(
    'order_item',
    'price_without_any_discount',
    array(
        'type'          => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'lenght'        => '12,4',
        'required'      => false,
    )
);

$installer->endSetup();