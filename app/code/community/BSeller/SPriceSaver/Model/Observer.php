<?php

class BSeller_SPriceSaver_Model_Observer
{

    public function salesOrderPlaceBefore(Varien_Event_Observer $observer)
    {
        $order = $observer->getEvent()->getOrder();
        $items = $order->getItemsCollection();
        foreach ($items as $item) {
            $unitFinalPrice = $item->getPrice();
            $unitPriceWithoutAnyDiscount = Mage::getModel('catalog/product')->load($item->getProductId())->getPrice();
            $item->setData('price_without_any_discount', $unitPriceWithoutAnyDiscount);

//            if ($unitFinalPrice <= 0) {
//                continuer;
//            }
//            $unitSpecialPriceDiscount = $unitPriceWithoutAnyDiscount - $unitFinalPrice;
//            if ($unitSpecialPriceDiscount <= 0) {
//                continue;
//            }
//
//            $qtyOrdered = $item->getQtyOrdered();
//            $discountAmount = $item->getData('discount_amount') + (($unitPriceWithoutAnyDiscount - $unitFinalPrice) * $qtyOrdered);
//            $item->setData('discount_amount', $discountAmount);
//            $item->setData('base_discount_amount', $discountAmount);
//            $item->setData('discount_percent', (100 - ((100 * (($unitPriceWithoutAnyDiscount * $qtyOrdered) - $discountAmount)) / ($unitPriceWithoutAnyDiscount * $qtyOrdered))));
        }

        return $this;
    }
}
