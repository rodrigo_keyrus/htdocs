<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Flag
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

/**
 * Class BSeller_Flag_Model_Flag
 *
 * Getters
 *
 * @method string getTitle()
 * @method bool getStatus()
 * @method int getDisplay()
 * @method string getImage()
 * @method string getText()
 * @method string getAction()
 * @method string getConditionJson()
 * @method int getPosition()
 * @method bool getIsStamp()
 * @method string getAlignList()
 * @method string getAlignView()
 * @method BSeller_Flag_Helper_Data _helper()
 *
 * Setters
 *
 * @method setTitle($title)
 * @method setStatus($status)
 * @method setDisplay($display)
 * @method setImage($image)
 * @method setText($text)
 * @method setAction($action)
 * @method setConditionJson($conditionJson)
 * @method setPosition($position)
 * @method setIsStamp($isStamp)
 * @method setAlignList($alignList)
 * @method setAlignView($alignView)
 */
class BSeller_Flag_Model_Flag extends BSeller_Core_Model_Abstract
{
    /**
     * Resource entity table
     *
     * @var string
     */
    const ENTITY = 'flag';

    /**
     * Initialize constructor
     */
    public function _construct()
    {
        parent::_construct();

        $this->_init('bseller_flag/flag');
    }

    /**
     * Return conditions in array
     *
     * @return array
     */
    public function getConditions()
    {
        return (array) Mage::helper('core')->jsonDecode($this->getConditionJson());
    }

    /**
     * Return image factory object
     *
     * @return BSeller_Flag_Model_Image
     */
    public function getImageFactory()
    {
        return Mage::getSingleton('bseller_flag/image')->addData($this->getData());
    }

    /**
     * Before save data
     *
     * @return Mage_Core_Model_Abstract
     */
    protected function _beforeSave()
    {
        $this->_helper()->convertToJson($this, 'condition');

        return parent::_beforeSave();
    }
}
