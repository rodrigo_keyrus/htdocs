<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Flag
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_Flag_Model_Validator extends Varien_Object
{
    /**
     * Check if conditions is valid
     *
     * @param BSeller_Flag_Model_Flag $object
     * @param Mage_Catalog_Model_Product $product
     * @return bool
     */
    public function validate($object, $product)
    {
        $conditions = $object->getConditions();

        foreach ($conditions as $condition) {
            $attribute = $condition['attribute'];
            $operator  = $condition['operator'];
            $value     = $condition['value'];

            switch ($operator) {
                case BSeller_Flag_Model_System_Config_Source_Operator::EQUAL:
                    $isApproved = $product->getData($attribute) === $value;

                    break;
                case BSeller_Flag_Model_System_Config_Source_Operator::NOT_EQUAL:
                    $isApproved = $product->getData($attribute) !== $value;

                    break;
                case BSeller_Flag_Model_System_Config_Source_Operator::GREATER_THAN:
                    $isApproved = $product->getData($attribute) > $value;

                    break;
                case BSeller_Flag_Model_System_Config_Source_Operator::GREATER_THAN_OR_EQUAL:
                    $isApproved = $product->getData($attribute) >= $value;

                    break;
                case BSeller_Flag_Model_System_Config_Source_Operator::LESS_THAN:
                    $isApproved = $product->getData($attribute) < $value;

                    break;
                case BSeller_Flag_Model_System_Config_Source_Operator::LESS_THAN_OR_EQUAL:
                    $isApproved = $product->getData($attribute) <= $value;

                    break;
                default:
                    $isApproved = false;

                    break;
            }

            if ($isApproved === false) {
                return false;
            }
        }

        return true;
    }
}
