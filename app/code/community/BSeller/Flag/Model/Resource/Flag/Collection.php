<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Flag
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_Flag_Model_Resource_Flag_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    /**
     * Collection constructor
     */
    public function _construct()
    {
        $this->_init('bseller_flag/flag');
    }

    /**
     * Add status to filter collection
     *
     * @param bool $value
     * @return $this
     */
    public function addStatusToFilter($value = true)
    {
        $this->addFieldToFilter('status', (bool) $value);

        return $this;
    }

    /**
     * Add is stamp to filter collection
     *
     * @param bool $value
     * @return $this
     */
    public function addIsStampToFilter($value = false)
    {
        $this->addFieldToFilter('is_stamp', (bool) $value);

        return $this;
    }

    /**
     * Add position to order collection
     *
     * @param string $direction
     * @return $this
     */
    public function addPositionToOrder($direction = Varien_Data_Collection_Db::SORT_ORDER_ASC)
    {
        $this->addOrder('position', $direction);

        return $this;
    }
}
