<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Flag
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

/**
 * Class BSeller_Flag_Model_Action_Abstract
 *
 * Getters
 *
 * @method Mage_Catalog_Model_Product getProduct()
 * @method BSeller_Flag_Model_Flag getFlag()
 */
abstract class BSeller_Flag_Model_Action_Abstract extends Varien_Object
{
    use BSeller_Core_Trait_Data;

    /**
     * Object singleton key
     *
     * @var string
     */
    protected $_singletonKey = 'bseller_flag_id_';

    /**
     * Initialize constructor
     */
    public function _construct()
    {
        parent::_construct();

        $this->init();
    }

    /**
     * Initialize custom config keys
     *
     * @return $this
     */
    public function init()
    {
        $this->_singletonKey .= $this->getFlag()->getId();

        return $this;
    }

    /**
     * Return if text action is valid
     *
     * @return bool
     */
    abstract public function validate();

    /**
     * Return text action argument
     *
     * @return string
     */
    abstract public function getArgument();
}
