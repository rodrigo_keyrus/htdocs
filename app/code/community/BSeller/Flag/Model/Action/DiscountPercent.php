<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Flag
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_Flag_Model_Action_DiscountPercent extends BSeller_Flag_Model_Action_Abstract
{
    /**
     * Return if text action is valid
     *
     * @return bool
     */
    public function validate()
    {
        return (bool) !($this->getArgument() <= 0);
    }

    /**
     * Return text action argument
     *
     * @return string
     */
    public function getArgument()
    {
        if ($this->hasData($this->_singletonKey)) {
            return $this->getData($this->_singletonKey);
        }

        $this->setData($this->_singletonKey, $this->getDiscount());

        return $this->getData($this->_singletonKey);
    }

    /**
     * Return product discount percent
     *
     * @return float
     */
    protected function getDiscount()
    {
        /** @var Mage_Catalog_Model_Product $product */
        $product = $this->getProduct();

        $price      = $product->getPrice();
        $finalPrice = $product->getFinalPrice();

        return floor((($price - $finalPrice) / $price) * 100);
    }
}
