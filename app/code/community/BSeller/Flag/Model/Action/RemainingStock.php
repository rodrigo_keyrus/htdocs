<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Flag
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_Flag_Model_Action_RemainingStock extends BSeller_Flag_Model_Action_Abstract
{
    /**
     * Return if text action is valid
     *
     * @return bool
     */
    public function validate()
    {
        return (bool) !($this->getArgument() <= 0);
    }

    /**
     * Return text action argument
     *
     * @return string
     */
    public function getArgument()
    {
        if ($this->hasData($this->_singletonKey)) {
            return $this->getData($this->_singletonKey);
        }

        $this->setData($this->_singletonKey, $this->getInventoryStockQty($this->getProduct()));

        return $this->getData($this->_singletonKey);
    }

    /**
     * Return product inventory stock qty
     *
     * @param Mage_Catalog_Model_Product $product
     * @return int
     */
    public function getInventoryStockQty($product)
    {
        $qty = 0;

        switch ($product->getTypeId()) {
            case Mage_Catalog_Model_Product_Type::TYPE_SIMPLE:
                $qty = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product)->getQty();

                break;
            case Mage_Catalog_Model_Product_Type::TYPE_GROUPED:
                /** @var Mage_Catalog_Model_Product_Type_Grouped $productTypeInstance */
                $productTypeInstance = $product->getTypeInstance(true);

                $children = $productTypeInstance->getAssociatedProductCollection($product);

                foreach ($children as $child) {
                    $qty += $this->getInventoryStockQty($child);
                }

                break;
            case Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE:
                $children = Mage::getModel('catalog/product_type_configurable')->getUsedProducts(null, $product);

                foreach ($children as $child) {
                    $qty += $this->getInventoryStockQty($child);
                }

                break;
            default:
                $qty = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product)->getQty();

                break;
        }

        return (int) $qty;
    }
}
