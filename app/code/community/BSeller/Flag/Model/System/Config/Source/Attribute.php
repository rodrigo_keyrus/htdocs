<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Flag
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_Flag_Model_System_Config_Source_Attribute
{
    use BSeller_Core_Trait_Data;

    /**
     * Return options in select format
     *
     * @return array
     */
    public function toOptionArray()
    {
        $data[] = array();

        foreach ($this->toArray() as $value => $label) {
            $data[] = array(
                'value' => $value,
                'label' => $label
            );
        }

        return $data;
    }

    /**
     * Return options in key-value format
     *
     * @return array
     */
    public function toArray()
    {
        $data = array();

        $attributes = Mage::getSingleton('catalog/product')->getAttributes();

        /** @var Mage_Catalog_Model_Resource_Eav_Attribute $attribute */
        foreach ($attributes as $attribute) {
            if (!$attribute->getIsVisible()) {
                continue;
            }

            $data[$attribute->getAttributeCode()] = $attribute->getFrontendLabel();
        }

        return $data;
    }
}
