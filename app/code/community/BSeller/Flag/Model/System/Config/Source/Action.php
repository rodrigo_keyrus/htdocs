<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Flag
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_Flag_Model_System_Config_Source_Action
{
    use BSeller_Core_Trait_Data;

    /**
     * Action labels
     *
     * @var array
     */
    protected $_actionLabels = array(
        BSeller_Flag_Model_Factory::DISCOUNT_PERCENT_KEY => 'Discount percent',
        BSeller_Flag_Model_Factory::REMAINING_STOCK_KEY  => 'Remaining stock'
    );

    /**
     * Return options in select format
     *
     * @return array
     */
    public function toOptionArray()
    {
        $data[] = array();

        foreach ($this->toArray() as $value => $label) {
            $data[] = array(
                'value' => $value,
                'label' => $label
            );
        }

        return $data;
    }

    /**
     * Return options in key-value format
     *
     * @return array
     */
    public function toArray()
    {
        /** @var BSeller_Flag_Model_Factory $factory */
        $factory = Mage::getSingleton('bseller_flag/factory');

        $data[''] = $this->_helper()->__('None');

        foreach ($factory->getAction() as $value => $class) {
            $data[$value] = $this->_helper()->__($this->_actionLabels[$value]);
        }

        return $data;
    }
}
