<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Flag
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_Flag_Model_System_Config_Source_Operator
{
    use BSeller_Core_Trait_Data;

    /**
     * Conditional operator values
     */
    const EQUAL                 = '=';
    const NOT_EQUAL             = '!=';
    const GREATER_THAN          = '>';
    const GREATER_THAN_OR_EQUAL = '>=';
    const LESS_THAN             = '<';
    const LESS_THAN_OR_EQUAL    = '<=';

    /**
     * Conditional operators
     *
     * @var array
     */
    protected $_operators = array(
        self::EQUAL                 => 'Equal',
        self::NOT_EQUAL             => 'Not equal',
        self::GREATER_THAN          => 'Greater than',
        self::GREATER_THAN_OR_EQUAL => 'Greater than or equal',
        self::LESS_THAN             => 'Less than',
        self::LESS_THAN_OR_EQUAL    => 'Less than or equal'
    );

    /**
     * Return options in select format
     *
     * @return array
     */
    public function toOptionArray()
    {
        $data[] = array();

        foreach ($this->toArray() as $value => $label) {
            $data[] = array(
                'value' => $value,
                'label' => $label
            );
        }

        return $data;
    }

    /**
     * Return options in key-value format
     *
     * @return array
     */
    public function toArray()
    {
        $data = array();

        foreach ($this->_operators as $value => $label) {
            $data[$value] = $this->_helper()->__($label);
        }

        return $data;
    }
}
