<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Flag
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_Flag_Model_System_Config_Source_Status extends Mage_Adminhtml_Model_System_Config_Source_Enabledisable
{
    /**
     * Return option only array
     *
     * @return array
     */
    public function toArray()
    {
        $statuses = $this->toOptionArray();

        $options = array();
        foreach ($statuses as $status) {
            $options[$status['value']] = $status['label'];
        }

        return $options;
    }
}
