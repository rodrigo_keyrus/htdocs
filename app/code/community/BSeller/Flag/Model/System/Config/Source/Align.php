<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Flag
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_Flag_Model_System_Config_Source_Align
{
    use BSeller_Core_Trait_Data;

    /**
     * Align position values
     */
    const TOP_LEFT      = 't-left';
    const TOP_MIDDLE    = 't-middle';
    const TOP_RIGHT     = 't-right';
    const MIDDLE_LEFT   = 'm-left';
    const MIDDLE_MIDDLE = 'm-middle';
    const MIDDLE_RIGHT  = 'm-right';
    const BOTTOM_LEFT   = 'b-left';
    const BOTTOM_MIDDLE = 'b-middle';
    const BOTTOM_RIGHT  = 'b-right';

    /**
     * Align positions
     *
     * @var array
     */
    protected $_aligns = array(
        self::TOP_LEFT      => 'Top / Left',
        self::TOP_MIDDLE    => 'Top / Middle',
        self::TOP_RIGHT     => 'Top / Right',
        self::MIDDLE_LEFT   => 'Middle / Left',
        self::MIDDLE_MIDDLE => 'Middle / Middle',
        self::MIDDLE_RIGHT  => 'Middle / Right',
        self::BOTTOM_LEFT   => 'Bottom / Left',
        self::BOTTOM_MIDDLE => 'Bottom / Middle',
        self::BOTTOM_RIGHT  => 'Bottom / Right'
    );

    /**
     * Return options in select format
     *
     * @return array
     */
    public function toOptionArray()
    {
        $data[] = array();

        foreach ($this->toArray() as $value => $label) {
            $data[] = array(
                'value' => $value,
                'label' => $label
            );
        }

        return $data;
    }

    /**
     * Return options in key-value format
     *
     * @return array
     */
    public function toArray()
    {
        $data[''] = $this->_helper()->__('Select');

        foreach ($this->_aligns as $value => $label) {
            $data[$value] = $this->_helper()->__($label);
        }

        return $data;
    }
}
