<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Flag
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_Flag_Model_Process extends Varien_Object
{
    /**
     * Process conditions
     *
     * @param Mage_Catalog_Model_Product $product
     * @param BSeller_Flag_Model_Flag $flag
     * @return bool
     */
    public function validate($product, $flag)
    {
        /** @var BSeller_Flag_Model_Validator $validator */
        $validator = Mage::getModel('bseller_flag/validator');

        if (!$validator->validate($flag, $product)) {
            return false;
        }

        /** @var BSeller_Flag_Model_Factory $factory */
        $factory = Mage::getModel('bseller_flag/factory');

        $factoryObject = $factory->getActionObject($flag->getAction(), array('flag' => $flag, 'product' => $product));

        if (!$factoryObject instanceof BSeller_Flag_Model_Action_Abstract) {
            return true;
        }

        if (!$factoryObject->validate()) {
            return false;
        }

        $flag->setText(sprintf($flag->getText(), $factoryObject->getArgument()));

        return true;
    }
}
