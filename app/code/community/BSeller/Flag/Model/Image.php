<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Flag
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_Flag_Model_Image extends Varien_Object
{
    use BSeller_Core_Trait_Data;

    /**
     * File subdirectory
     *
     * @var string
     */
    protected $_fileSubdirectory = 'flag';

    /**
     * Allowed extensions
     *
     * @var array
     */
    protected $_allowedExtensions = array(
        'jpg',
        'jpeg',
        'png'
    );

    /**
     * Generate file in directory
     *
     * @return $this
     * @throws Exception
     */
    public function upload()
    {
        $this->delete();

        $directoryFile = Mage::getBaseDir('media') . DS . $this->_fileSubdirectory;

        /** @var Varien_File_Uploader $uploader */
        $uploader = new Varien_File_Uploader('image');
        $uploader->setAllowedExtensions($this->_allowedExtensions);
        $uploader->setAllowRenameFiles(true);
        $uploader->setFilesDispersion(false);
        $uploader->save($directoryFile);

        return $this->_fileSubdirectory . DS . $uploader->getUploadedFileName();
    }

    /**
     * Delete file in directory
     *
     * @param null|string $image
     * @return $this
     */
    public function delete($image = null)
    {
        if (is_null($image)) {
            $image = $this->getData('image');
        }

        if (isset($image['value'])) {
            $image = $image['value'];
        }

        /** @var Varien_Io_File $fileSystem */
        $fileSystem = new Varien_Io_File();
        $fileSystem->rm(Mage::getBaseDir('media') . DS . implode(DS, explode(DS, $image)));

        return $this;
    }
}
