<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Flag
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_Flag_Model_Factory extends Varien_Object
{
    /**
     * Text action keys
     */
    const DISCOUNT_PERCENT_KEY = 'discount_percent';
    const REMAINING_STOCK_KEY  = 'remaining_stock';

    /**
     * Text actions
     *
     * @var array
     */
    protected $_actions = array(
        self::DISCOUNT_PERCENT_KEY => 'bseller_flag/action_discountPercent',
        self::REMAINING_STOCK_KEY  => 'bseller_flag/action_remainingStock'
    );

    /**
     * Create a new action
     *
     * @param string $key
     * @param string $value
     * @return $this
     */
    public function setAction($key, $value)
    {
        $this->_actions[$key] = $value;

        return $this;
    }

    /**
     * Return action class
     *
     * @param null|string $key
     * @return array|string
     */
    public function getAction($key = null)
    {
        if (is_null($key)) {
            return $this->_actions;
        }

        if (!isset($this->_actions[$key])) {
            return '';
        }

        return $this->_actions[$key];
    }

    /**
     * Return action object
     *
     * @param string $code
     * @param array $arguments
     * @return BSeller_Flag_Model_Action_Abstract
     */
    public function getActionObject($code, $arguments = array())
    {
        $action = $this->getAction($code);

        return Mage::getModel($action, $arguments);
    }
}
