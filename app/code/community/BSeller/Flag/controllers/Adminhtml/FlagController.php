<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Flag
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_Flag_Adminhtml_FlagController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Render index page
     *
     * @return void
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Render grid block
     *
     * @return void
     */
    public function gridAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Render form new
     *
     * @return void
     */
    public function newAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Render form edit
     *
     * @return $this
     */
    public function editAction()
    {
        $id = $this->getRequest()->getParam('id');

        if (!$id) {
            $this->_redirect('*/*/new');

            return $this;
        }

        /** @var BSeller_Flag_Model_Flag $object */
        $object = Mage::getModel('bseller_flag/flag')->load($id);

        if (!$object->getId()) {
            $this->_redirect('*/*/new');

            return $this;
        }

        Mage::helper('bseller_flag/flag')->addObjectInfo($object);

        $this->loadLayout();
        $this->renderLayout();

        return $this;
    }

    /**
     * Save post data
     *
     * @return $this
     */
    public function saveAction()
    {
        if (!$this->getRequest()->isPost()) {
            $this->_redirect('*/*');

            return $this;
        }

        if (!$this->_validateFormKey()) {
            $this->_redirectReferer();

            return $this;
        }

        /** @var BSeller_Flag_Model_Flag $object */
        $object = Mage::getModel('bseller_flag/flag');

        $back = $this->getRequest()->getParam('back');
        $post = $this->getRequest()->getPost();
        $id   = $this->getRequest()->getPost('id');

        if ($id) {
            $object->load($id);
        }

        $object->addData($post);

        if ($image = $object->getData('image/value')) {
            $object->setData('image', $image);
        }

        try {
            if ($this->helper()->hasImageToDelete()) {
                $object->getImageFactory()->delete();

                $object->setData('image', '');
            }

            if ($this->helper()->hasImageToUpload()) {
                $image = $object->getImageFactory()->upload();

                $object->setData('image', $image);
            }

            $object->save();

            $id = $object->getId();

            $this->_getSession()->addSuccess($this->__('Successfully saved.'));
        } catch (Exception $e) {
            Mage::logException($e);

            $this->_getSession()->addError($e->getMessage());
        }

        $this->_redirect('*/*/' . ($back ? 'edit' : 'index'), ($back ? array('id' => $id) : array()));

        return $this;
    }

    /**
     * Delete row by entity id
     *
     * @return $this
     */
    public function deleteAction()
    {
        $id = $this->getRequest()->getParam('id');

        if (!$id) {
            $this->_redirect('*/*');

            return $this;
        }

        /** @var BSeller_Flag_Model_Flag $object */
        $object = Mage::getModel('bseller_flag/flag')->load($id);

        if (!$object->getId()) {
            $this->_redirect('*/*');

            return $this;
        }

        try {
            $object->getImageFactory()->delete();
            $object->delete();

            $this->_getSession()->addSuccess($this->__('Successfully deleted.'));
        } catch (Exception $e) {
            Mage::logException($e);

            $this->_getSession()->addError($e->getMessage());
        }

        $this->_redirect('*/*/index');

        return $this;
    }

    /**
     * Is allowed to access
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('bseller/bseller_flag/flag');
    }

    /**
     * Return data helper object
     *
     * @return BSeller_Flag_Helper_Flag
     */
    protected function helper()
    {
        return Mage::helper('bseller_flag/flag');
    }
}
