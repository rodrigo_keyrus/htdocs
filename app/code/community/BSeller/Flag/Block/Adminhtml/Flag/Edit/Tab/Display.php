<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Flag
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_Flag_Block_Adminhtml_Flag_Edit_Tab_Display extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Prepare form before rendering html
     *
     * @return Mage_Adminhtml_Block_Widget_Form
     * @throws Exception
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();

        $fieldSet = $form->addFieldset('display_field_set_general', array('legend' => $this->__('General')));

        $object = $this->helper('bseller_flag/flag')->getObjectInfo();

        /** @var Mage_Adminhtml_Block_Widget_Form_Element_Dependence $dependence */
        $dependence = $this->getLayout()->createBlock('adminhtml/widget_form_element_dependence');

        $fieldSet->addField(
            'is_stamp',
            'select',
            array(
                'name'     => 'is_stamp',
                'label'    => $this->__('Is Stamp'),
                'options'  => Mage::getSingleton('adminhtml/system_config_source_yesno')->toArray(),
                'required' => true
            )
        );

        $dependence->addFieldMap('is_stamp', 'is_stamp');

        $fieldSet->addField(
            'align_list',
            'select',
            array(
                'name'     => 'align_list',
                'label'    => $this->__('Align on Product List'),
                'options'  => Mage::getSingleton('bseller_flag/system_config_source_align')->toArray(),
                'required' => true
            )
        );

        $dependence->addFieldMap('align_list', 'align_list');
        $dependence->addFieldDependence('align_list', 'is_stamp', true);

        $fieldSet->addField(
            'align_view',
            'select',
            array(
                'name'     => 'align_view',
                'label'    => $this->__('Align on Product View'),
                'options'  => Mage::getSingleton('bseller_flag/system_config_source_align')->toArray(),
                'required' => true
            )
        );

        $dependence->addFieldMap('align_view', 'align_view');
        $dependence->addFieldDependence('align_view', 'is_stamp', true);

        $fieldSet->addField(
            'display',
            'select',
            array(
                'name'     => 'display',
                'label'    => $this->__('Type'),
                'options'  => Mage::getSingleton('bseller_flag/system_config_source_display')->toArray(),
                'required' => true
            )
        );

        $dependence->addFieldMap('display', 'display');

        $fieldSet->addField(
            'image',
            'image',
            array(
                'name'     => 'image',
                'label'    => $this->__('Image'),
                'note'     => '(*.jpg, *.png)',
                'required' => true
            )
        );

        $dependence->addFieldMap('image', 'image');
        $dependence->addFieldDependence('image', 'display', BSeller_Flag_Model_System_Config_Source_Display::IMAGE);

        $fieldSet->addField(
            'text',
            'text',
            array(
                'name'     => 'text',
                'label'    => $this->__('Text'),
                'note'     => $this->__('Use the %s to print the value (argument).'),
                'required' => true
            )
        );

        $dependence->addFieldMap('text', 'text');
        $dependence->addFieldDependence('text', 'display', BSeller_Flag_Model_System_Config_Source_Display::TEXT);

        $fieldSet->addField(
            'action',
            'select',
            array(
                'name'    => 'action',
                'label'   => $this->__('Text Argument'),
                'options' => Mage::getSingleton('bseller_flag/system_config_source_action')->toArray()
            )
        );

        $dependence->addFieldMap('action', 'action');
        $dependence->addFieldDependence('action', 'display', BSeller_Flag_Model_System_Config_Source_Display::TEXT);

        Mage::dispatchEvent(
            'adminhtml_bseller_flag_edit_tab_display_prepare_form',
            array(
                'flag'       => $object,
                'form'       => $form,
                'dependence' => $dependence
            )
        );

        $form->setValues($object->getData());
        $this->setForm($form);

        $this->setChild('form_after', $dependence);

        return parent::_prepareForm();
    }
}
