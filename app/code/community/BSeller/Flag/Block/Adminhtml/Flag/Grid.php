<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Flag
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_Flag_Block_Adminhtml_Flag_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Initialize constructor
     */
    public function _construct()
    {
        parent::_construct();

        $this->setId('bseller_flag_grid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setData('use_ajax', true);
    }

    /**
     * Prepare grid collection
     *
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('bseller_flag/flag')->getCollection();

        $this->setCollection($collection);
        
        parent::_prepareCollection();

        return $this;
    }

    /**
     * Prepare grid columns
     *
     * @return $this
     * @throws Exception
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'entity_id',
            array(
                'header' => $this->__('ID'),
                'align'  => 'left',
                'index'  => 'entity_id',
                'type'   => 'number'
            )
        );

        $this->addColumn(
            'title',
            array(
                'header' => $this->__('Title'),
                'align'  => 'left',
                'index'  => 'title',
                'type'   => 'text'
            )
        );

        $this->addColumn(
            'is_stamp',
            array(
                'header'  => $this->__('Is Stamp'),
                'align'   => 'left',
                'index'   => 'is_stamp',
                'type'    => 'options',
                'options' => Mage::getSingleton('adminhtml/system_config_source_yesno')->toArray()
            )
        );

        $this->addColumn(
            'updated_at',
            array(
                'header'   => $this->__('Updated At'),
                'index'    => 'updated_at',
                'type'     => 'datetime',
                'renderer' => 'bseller_flag/adminhtml_widget_grid_column_renderer_datetime'
            )
        );

        $this->addColumn(
            'created_at',
            array(
                'header'   => $this->__('Created At'),
                'index'    => 'created_at',
                'type'     => 'datetime',
                'renderer' => 'bseller_flag/adminhtml_widget_grid_column_renderer_datetime'
            )
        );

        $this->addColumn(
            'status',
            array(
                'header'  => $this->__('Status'),
                'align'   => 'left',
                'index'   => 'status',
                'type'    => 'options',
                'options' => Mage::getSingleton('bseller_flag/system_config_source_status')->toArray()
            )
        );

        return parent::_prepareColumns();
    }

    /**
     * Return grid url
     *
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current' => true));
    }

    /**
     * Return row url
     *
     * @param Varien_Object $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }
}
