<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Flag
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_Flag_Block_Adminhtml_Flag_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * Initialize constructor
     */
    public function _construct()
    {
        parent::_construct();

        $this->_objectId   = 'id';
        $this->_controller = 'adminhtml_flag';
        $this->_blockGroup = 'bseller_flag';
        $this->_headerText = $this->__('Flag');

        $this->_addButton(
            'save_and_continue',
            array(
                'label'   => $this->__('Save and Continue Edit'),
                'onclick' => "editForm.submit($('edit_form').readAttribute('action') + 'back/edit')",
                'class'   => 'save'
            ),
            -100
        );
    }
}
