<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Flag
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_Flag_Block_Adminhtml_Flag_Edit_Tab_Main extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Prepare form before rendering html
     *
     * @return Mage_Adminhtml_Block_Widget_Form
     * @throws Exception
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();

        $fieldSet = $form->addFieldset('main_field_set_general', array('legend' => $this->__('General')));

        $object = $this->helper('bseller_flag/flag')->getObjectInfo();

        /** @var Mage_Adminhtml_Block_Widget_Form_Element_Dependence $dependence */
        $dependence = $this->getLayout()->createBlock('adminhtml/widget_form_element_dependence');

        if ($object->getId()) {
            $fieldSet->addField(
                'entity_id',
                'hidden',
                array(
                    'name'     => 'id',
                    'required' => true
                )
            );
        }

        $fieldSet->addField(
            'title',
            'text',
            array(
                'name'     => 'title',
                'label'    => $this->__('Title'),
                'required' => true
            )
        );

        $fieldSet->addField(
            'status',
            'select',
            array(
                'name'     => 'status',
                'label'    => $this->__('Status'),
                'options'  => Mage::getSingleton('bseller_flag/system_config_source_status')->toArray(),
                'required' => true
            )
        );

        if (!$object->getId()) {
            $object->setData('status', true);
        }

        $fieldSet->addField(
            'position',
            'text',
            array(
                'name'  => 'position',
                'label' => $this->__('Position'),
                'class' => 'validate-number'
            )
        );

        if (!$object->getId()) {
            $object->setData('position', 0);
        }

        Mage::dispatchEvent(
            'adminhtml_bseller_flag_edit_tab_main_prepare_form',
            array(
                'flag'       => $object,
                'form'       => $form,
                'dependence' => $dependence
            )
        );

        $form->setValues($object->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }
}
