<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Flag
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_Flag_Block_Adminhtml_Flag_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * Initialize constructor
     */
    public function _construct()
    {
        parent::_construct();

        $this->setId('bseller_flag_tabs');
        $this->setDestElementId('edit_form');
        $this->setData('title', $this->__('Information'));
    }

    /**
     * Add tabs before to html
     *
     * @return Mage_Core_Block_Abstract
     * @throws Exception
     */
    protected function _beforeToHtml()
    {
        $this->addTab(
            'main_section',
            array(
                'label'   => $this->__('Information'),
                'content' => $this->getLayout()->createBlock('bseller_flag/adminhtml_flag_edit_tab_main')->toHtml()
            )
        );

        $this->addTab(
            'display_section',
            array(
                'label'   => $this->__('Display'),
                'content' => $this->getLayout()->createBlock('bseller_flag/adminhtml_flag_edit_tab_display')->toHtml()
            )
        );

        $this->addTab(
            'condition_section',
            array(
                'label'   => $this->__('Conditions'),
                'content' => $this->getLayout()->createBlock('bseller_flag/adminhtml_flag_edit_tab_condition')->toHtml()
            )
        );

        return parent::_beforeToHtml();
    }
}
