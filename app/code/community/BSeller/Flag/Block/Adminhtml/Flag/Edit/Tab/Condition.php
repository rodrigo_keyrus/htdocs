<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Flag
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_Flag_Block_Adminhtml_Flag_Edit_Tab_Condition extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Prepare form before rendering html
     *
     * @return Mage_Adminhtml_Block_Widget_Form
     * @throws Exception
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();

        $fieldSet = $form->addFieldset('main_field_set_attribute', array('legend' => $this->__('Product Attributes')));

        $object = $this->helper('bseller_flag/flag')->getObjectInfo();
        $object->setData('condition', $object->getConditions());

        /** @var Mage_Adminhtml_Block_Widget_Form_Element_Dependence $dependence */
        $dependence = $this->getLayout()->createBlock('adminhtml/widget_form_element_dependence');

        $fieldSet->addField(
            'condition',
            'text',
            array(
                'name'  => 'condition',
                'label' => $this->__('Conditions')
            )
        );

        $form->getElement('condition')->setRenderer(
            $this->getLayout()->createBlock('bseller_flag/adminhtml_system_config_form_field_condition')
        );

        Mage::dispatchEvent(
            'adminhtml_bseller_flag_edit_tab_attribute_prepare_form',
            array(
                'flag'       => $object,
                'form'       => $form,
                'dependence' => $dependence
            )
        );

        $form->setValues($object->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }
}
