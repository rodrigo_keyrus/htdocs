<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Flag
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_Flag_Block_Adminhtml_System_Config_Form_Field_Condition
    extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
{
    /**
     * Prepare to render
     *
     * @return void
     */
    protected function _prepareToRender()
    {
        $this->addColumn(
            'attribute',
            array(
                'label'    => $this->__('Attribute'),
                'renderer' => $this->getFieldRenderer('attribute')->setClass('input-text required-entry')
            )
        );

        $this->addColumn(
            'operator',
            array(
                'label'    => $this->__('Operator'),
                'renderer' => $this->getFieldRenderer('operator')->setClass('input-text required-entry')
            )
        );

        $this->addColumn(
            'value',
            array(
                'label' => $this->__('Value'),
                'class' => 'input-text required-entry'
            )
        );

        $this->_addAfter = false;
    }

    /**
     * Retrieve installment column renderer
     *
     * @param string $rendererClass
     * @return Mage_Core_Block_Html_Select
     */
    protected function getFieldRenderer($rendererClass)
    {
        $objectKey = 'field_' . $rendererClass;

        if ($this->hasData($objectKey)) {
            return $this->getData($objectKey);
        }

        $block = $this->getLayout()
            ->createBlock(
                'bseller_flag/adminhtml_system_config_form_renderer_' . $rendererClass,
                'bseller_flag_adminhtml_system_config_form_renderer_' . $rendererClass,
                array('is_render_to_js_template' => true)
            );

        $this->setData($objectKey, clone $block);

        return $this->getData($objectKey);
    }

    /**
     * Prepare existing row data object
     *
     * @param Varien_Object
     * @return void
     */
    protected function _prepareArrayRow(Varien_Object $row)
    {
        $row->setData(
            'option_extra_attr_' . $this->getFieldRenderer('attribute')->{'calcOptionHash'}($row->getData('attribute')),
            'selected="selected"'
        );

        $row->setData(
            'option_extra_attr_' . $this->getFieldRenderer('operator')->{'calcOptionHash'}($row->getData('operator')),
            'selected="selected"'
        );
    }
}
