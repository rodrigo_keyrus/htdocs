<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Flag
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_Flag_Block_Adminhtml_System_Config_Form_Renderer_Attribute extends Mage_Core_Block_Html_Select
{
    /**
     * Set input name
     *
     * @param string $value
     * @return string
     */
    public function setInputName($value)
    {
        return $this->setData('name', $value);
    }

    /**
     * Return options of the element
     *
     * @return array
     */
    public function getOptions()
    {
        if ($this->_options) {
            return $this->_options;
        }

        $this->_options = Mage::getSingleton('bseller_flag/system_config_source_attribute')->toOptionArray();

        return $this->_options;
    }
}
