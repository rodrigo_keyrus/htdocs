<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Flag
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_Flag_Block_Adminhtml_Flag extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * Initialize constructor
     */
    public function _construct()
    {
        parent::_construct();

        $this->_controller = 'adminhtml_flag';
        $this->_blockGroup = 'bseller_flag';
        $this->_headerText = $this->__('Flags');
    }
}
