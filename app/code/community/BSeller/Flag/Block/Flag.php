<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Flag
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

/**
 * Class BSeller_Flag_Block_Flag
 *
 * Getters
 *
 * @method Mage_Catalog_Model_Product getProduct()
 * @method bool getIsOnlyStamp()
 * @method string getPageType()
 * @method int getLimit()
 *
 * Setters
 *
 * @method setProduct($product)
 * @method setIsOnlyStamp($isOnlyStamp)
 * @method setPageType($pageType)
 * @method setLimit($limit)
 */
class BSeller_Flag_Block_Flag extends BSeller_Core_Block_Template
{
    /**
     * Cache tag
     *
     * @var string
     */
    protected $_cacheTag = 'bseller_flag';

    /**
     * Cache lifetime
     *
     * @var int
     */
    protected $_cacheLifetime = 3600;

    /**
     * Singleton key
     *
     * @var string
     */
    protected $_singletonKey = 'bseller_flag_collection';

    /**
     * Initialize constructor
     */
    public function _construct()
    {
        parent::_construct();

        $this->setTemplate('bseller/flag/default.phtml');
    }

    /**
     * Return available flag collection
     *
     * @return array
     */
    public function getCollection()
    {
        if ($this->hasData($this->_singletonKey)) {
            return $this->getData($this->_singletonKey);
        }

        /** @var BSeller_Flag_Model_Process $process */
        $process = Mage::getModel('bseller_flag/process');

        /** @var BSeller_Flag_Model_Resource_Flag_Collection $collection */
        $collection = Mage::getResourceModel('bseller_flag/flag_collection')
            ->addIsStampToFilter($this->getIsOnlyStamp())
            ->addStatusToFilter()
            ->addPositionToOrder();

        /** @var BSeller_Flag_Model_Flag $object */
        foreach ($collection as $key => $object) {
            if ($process->validate($this->getProduct(), $object)) {
                continue;
            }

            $collection->removeItemByKey($key);
        }

        $itemsArray = array_slice($collection->getItems(), 0, $this->getLimit());

        $this->setData($this->_singletonKey, $itemsArray);

        return $this->getData($this->_singletonKey);
    }

    /**
     * Return cache lifetime
     *
     * @return int
     */
    public function getCacheLifetime()
    {
        return $this->_cacheLifetime;
    }

    /**
     * Return cache key
     *
     * @return string
     */
    public function getCacheKey()
    {
        $cacheKeys = array(
            $this->getProduct()->getId(),
            $this->getPageType()
        );

        if ($this->getIsOnlyStamp() === true) {
            $cacheKeys[] = 'stamp';
        }

        $cacheKeys[] = $this->getLimit();

        return implode('_', $cacheKeys);
    }

    /**
     * Return cache tags
     *
     * @return array
     */
    public function getCacheTags()
    {
        return array($this->_cacheTag);
    }

    /**
     * Render block HTML
     *
     * @return string
     */
    protected function _toHtml()
    {
        if (!$this->helper('bseller_flag')->getIsActive()) {
            return '';
        }

        return parent::_toHtml();
    }
}
