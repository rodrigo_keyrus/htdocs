<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Flag
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

/** @var Varien_Db_Adapter_Pdo_Mysql $connection */
$connection = $installer->getConnection();

/**
 * Create flag table
 */
$table = $connection->newTable($installer->getTable('bseller_flag/flag'))
    ->addColumn(
        'entity_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        11,
        array(
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
            'primary'  => true
        ),
        'Entity ID'
    )
    ->addColumn(
        'title',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        255,
        array(
            'nullable' => false,
            'default'  => ''
        ),
        'Title'
    )
    ->addColumn(
        'status',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        11,
        array(
            'unsigned' => true,
            'nullable' => false,
            'default'  => 0
        ), 'Status'
    )
    ->addColumn(
        'display',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        11,
        array(
            'nullable' => false,
            'default'  => 0
        ),
        'Display'
    )
    ->addColumn(
        'image',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        null,
        array(
            'nullable' => false,
            'default'  => ''
        ),
        'Image Path'
    )
    ->addColumn(
        'text',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        null,
        array(
            'nullable' => false,
            'default'  => ''
        ),
        'Text'
    )
    ->addColumn(
        'action',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        255,
        array(
            'nullable' => false,
            'default'  => ''
        ),
        'Action'
    )
    ->addColumn(
        'min_discount_percent',
        Varien_Db_Ddl_Table::TYPE_DECIMAL,
        '11,4',
        array(
            'nullable' => false,

        )
    )
    ->addColumn(
        'condition_json',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        null,
        array(
            'nullable' => false,
            'default'  => ''
        ),
        'Condition JSON'
    )
    ->addColumn(
        'position',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        11,
        array(
            'unsigned' => true,
            'nullable' => false
        ),
        'Position'
    )
    ->addColumn(
        'is_stamp',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        11,
        array(
            'unsigned' => true,
            'nullable' => false
        ),
        'Is Stamp'
    )
    ->addColumn(
        'align_list',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        null,
        array(
            'nullable' => false,
            'default'  => ''
        ),
        'Align List'
    )
    ->addColumn(
        'align_view',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        null,
        array(
            'nullable' => false,
            'default'  => ''
        ),
        'Align View'
    )
    ->addColumn(
        'updated_at',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        array(
            'nullable' => false,
            'default'  => '0000-00-00 00:00:00'
        ),
        'Updated At'
    )
    ->addColumn(
        'created_at',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        array(
            'nullable' => false,
            'default'  => '0000-00-00 00:00:00'
        ),
        'Created At'
    )
    ->setComment('BSeller Flag Entity');

$connection->createTable($table);

$installer->endSetup();
