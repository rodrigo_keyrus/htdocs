<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Flag
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_Flag_Helper_Data extends BSeller_Core_Helper_Data
{
    use BSeller_Core_Trait_Data;

    /**
     * Page type view
     *
     * @var string
     */
    const PAGE_TYPE_VIEW = 'view';

    /**
     * Page type list
     *
     * @var string
     */
    const PAGE_TYPE_LIST = 'list';

    /**
     * Limit size
     *
     * @var int
     */
    const LIMIT = 2;

    /**
     * Register key prefix
     *
     * @var string
     */
    protected $_registerKeyPrefix = '';

    /**
     * Save object data in register
     *
     * @param Varien_Object $value
     * @param bool $graceful
     * @return $this
     */
    public function addObjectInfo($value, $graceful = true)
    {
        Mage::register($this->_registerKeyPrefix, $value, $graceful);

        return $this;
    }

    /**
     * Return object data of register
     *
     * @return Varien_Object
     */
    public function getObjectInfo()
    {
        if ($object = Mage::registry($this->_registerKeyPrefix)) {
            return $object;
        }

        return new Varien_Object();
    }

    /**
     * Convert data to json
     *
     * @param Varien_Object $object
     * @param string $key
     * @return $this
     */
    public function convertToJson($object, $key)
    {
        if (!$object->hasData($key)) {
            return $this;
        }

        if (!is_array($object->getData($key))) {
            return $this;
        }

        $value = array_values($object->getData($key));
        $value = array_filter($value);
        $value = Mage::helper('core')->jsonEncode($value);

        $object->setData($key . '_json', $value);

        return $this;
    }

    /**
     * Return string formatted int class
     *
     * @param string $string
     * @return string
     */
    public function formatStringToClass($string)
    {
        $string = trim($string);
        $string = Mage::helper('core')->removeAccents($string);
        $string = preg_replace('/[^a-zA-Z0-9\s]/', '', $string);
        $string = str_replace(' ', '-', $string);
        $string = strtolower($string);

        return $string;
    }

    /**
     * Return if module is active
     *
     * @return bool
     */
    public function getIsActive()
    {
        return (bool) $this->getStoreConfig('active');
    }

    /**
     * Return store config data
     *
     * @param string $key
     * @return string
     */
    public function getStoreConfig($key)
    {
        return Mage::getStoreConfig('bseller_flag/settings/' . $key);
    }

    public function getStoreConfigFlag($key)
    {
        return Mage::getStoreConfigFlag('bseller_flag/settings/' . $key);
    }
}
