<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSeller
 * @package   BSeller_Flag
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

class BSeller_Flag_Helper_Flag extends BSeller_Flag_Helper_Data
{
    /**
     * Register key prefix
     *
     * @var string
     */
    protected $_registerKeyPrefix = BSeller_Flag_Model_Flag::ENTITY;

    /**
     * Save object data in register
     *
     * @param BSeller_Flag_Model_Flag $value
     * @param bool $graceful
     * @return $this
     */
    public function addObjectInfo($value, $graceful = true)
    {
        parent::addObjectInfo($value, $graceful);

        return $this;
    }

    /**
     * Return object data of register
     *
     * @return BSeller_Flag_Model_Flag
     */
    public function getObjectInfo()
    {
        return parent::getObjectInfo();
    }

    /**
     * Check if exist image to upload
     *
     * @return bool
     */
    public function hasImageToUpload()
    {
        if ($this->getRequest()->getPost('display') !== BSeller_Flag_Model_System_Config_Source_Display::IMAGE) {
            return false;
        }
        
        if (!isset($_FILES['image'])) {
            return false;
        }

        $image = $_FILES['image'];

        if (empty($image['name'])) {
            return false;
        }

        if ($image['size'] <= 0) {
            return false;
        }

        if ($image['error']) {
            return false;
        }

        return true;
    }

    /**
     * Check if exists option to delete image
     *
     * @return bool
     */
    public function hasImageToDelete()
    {
        $post = $this->getRequest()->getPost();

        return (bool) isset($post['image']['delete']);
    }

    /**
     * Return block content
     *
     * @param Mage_Catalog_Model_Product $product
     * @param bool $isOnlyStamp
     * @param string $pageType
     * @param int $limit
     * @return string
     */
    public function getBlockHtml($product, $isOnlyStamp = false, $pageType = self::PAGE_TYPE_LIST, $limit = self::LIMIT)
    {
        /** @var BSeller_Flag_Block_Flag $block */
        $block = Mage::app()->getLayout()->createBlock('bseller_flag/flag');

        $block->setProduct($product);
        $block->setIsOnlyStamp($isOnlyStamp);
        $block->setPageType($pageType);
        $block->setLimit($limit);

        return $block->toHtml();
    }
}
