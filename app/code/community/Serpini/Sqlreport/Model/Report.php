<?php
class Serpini_Sqlreport_Model_Report extends Mage_Core_Model_Abstract
{
	protected $columnsName = array();
	protected $result;
	protected $title;
	protected $combosList = array();
	protected $combosListBasic = array();
	protected $resultSum = array();
	protected $id;
	protected $sql;
	protected $group;
	protected $links = array();
	
	protected $log="";
	protected $combosSeted;
	
	protected $error;
	protected $errorMsg;
	protected $errorSQL;
	
	protected $setupmanager;
	protected $utils;
	
	protected $atributes = array();
	protected $chart_series = array();
	
	
		
    protected function _construct(){	
        $this->_init('sqlreport/report');
        $this->id = "";
        $this->error=false;
        $this->setupmanager= Mage::getModel('sqlreport/setup');
        $this->group=Mage::getModel('sqlreport/group');
        $this->utils= Mage::helper('sqlreport/Utils');
    }
    
    protected function _construct2($id,$title,$sql,$group_code){
    	$this->_init('sqlreport/report');
    	$this->id = $id;
    	$this->error=false;
    	$this->setupmanager= Mage::getModel('sqlreport/setup');
    	$this->title=$title;
    	$this->sql=$sql;
    	$this->group->loadMe($group_code);
    	$this->utils= Mage::helper('sqlreport/Utils');
    	
    }
    
    public function isLoaded(){
    	return $this->id!="";
    }
    
    
    public function loadReport($id){
    	try{
	    	$this->id = $id;
	    	$this->combosSeted=false;
	    	$coreResource = Mage::getSingleton('core/resource');
    		$connection = $coreResource->getConnection('core_read');
	    	$select = $connection->select()
	    			->from(array('a' => $this->gtn('sqlrpt_report')), 
	    				   array('a.title','a.report_sql','a.group_id'))
	    			->join(array('b' => $this->gtn('sqlrpt_group')),
	    					'a.group_id = b.group_id',
	    				   array('group_des' => 'b.description','group_orden' => 'b.orden'))
	    			->where('report_id = ?',$this->getId());
	    	$readresult=$connection->fetchAll($select);
	    	foreach ($readresult as $fila){
	    		$this->title = $fila['title'];
	    		$this->sql = $fila['report_sql'];
	    		$this->group->loadMe($fila['group_id']);
	    	}
	    	$this->loadComboList();
	    	$this->loadAtributes();
	    	if($this->hasChart()){
	    		$this->loadChartSeries();
	    	}
	    	$this->loadLinkList();
	    	
	    	
    	}catch (Exception  $err){
    		$this->error=true;
    		$this->errorMsg = "Error loading report params:".$err->getMessage();
    		$this->errorSQL = $sql;
    	}
    }
    
    
    public function loadDataReport($limit = null){
    	if(!$this->hasCombos()){
    		$this->executeQuery($limit);
    	}
    }
    
    public function setComboValues($comboValues){
    	if($this->hasCombos()){
    		$this->combosSeted = true;
    		$combosValuesDecode = base64_decode($comboValues);
    		$comboListValues = explode("&",$combosValuesDecode);
    		foreach ($comboListValues as $comboValue){
    			$parameterList = explode("=",$comboValue);
    			$parameter = $parameterList[0];
    			$valueList = explode("=",$comboValue);
    			$value = urldecode($valueList[1]);
    			foreach($this->getComboList() as $combo){
    				if(($combo->getParameter() == $parameter) && (""!=$parameter)){
    					$combo->setValue($value);
    				}
    			}
    		}
    		$this->executeQuery(' LIMIT 20');
    	}
    }
    
    protected function loadComboList(){    
    	try{
    		$coreResource = Mage::getSingleton('core/resource');
    		$connection = $coreResource->getConnection('core_read');
    		//$sql = "SELECT combo_id FROM ".$coreResource->getTableName('sqlrpt_report_combo')." WHERE report_id='".$this->id."' ORDER BY ORDER";
    		$select = $connection->select()
    				->from($this->gtn('sqlrpt_report_combo'), array('combo_id' => 'combo_id'))
    				->where('report_id = ?',$this->id)
    				->order(array('order_n'));
    		$readresult=$connection->fetchAll($select);
    		
	    	//$readresult=$connection->query($sql)->fetchAll();
	    	foreach ($readresult as $fila){
	    		$combo = Mage::getModel('sqlreport/combo');
	    		$combo->loadCombo($fila['combo_id']);
	    		if($combo->isError()){
	    			$this->error=true;
	    			$this->errorMsg= "Error in combo [".$combo->getId()."]: ".$combo->getErrorMsg();
	    			$this->errorSQL;
	    		}
	    		array_push($this->combosList,$combo);
	    		array_push($this->combosListBasic,$fila['combo_id']);
	    	}
    	}catch (Exception  $err){
    		$this->error=true;
	    	$this->errorMsg = "Error loading combo list :".$err->getMessage();
	    	$this->errorSQL = $sql;
    	}
    }
    
    
    public function loadAtributes(){
    	try{
    		$coreResource = Mage::getSingleton('core/resource');
    		$connection = $coreResource->getConnection('core_read');
    		$select = $connection->select()
    				->from(array('a' => $this->gtn('sqlrpt_report_type')),
    					   array('a.type_code', 'a.type'))
    				->joinLeft(array('b' => $this->gtn('sqlrpt_report_int')),
    						'a.type_id = b.type_id'.
    					    ' AND b.report_id = "'.$this->getId().'"',
    						array('value_int' => 'b.value'))
					->joinLeft(array('c' => $this->gtn('sqlrpt_report_text')),
    						'a.type_id = c.type_id'.
    					    ' AND c.report_id = "'.$this->getId().'"',
    						array('value_text' => 'c.value'))
					->joinLeft(array('d' => $this->gtn('sqlrpt_report_varchar')),
    						'a.type_id = d.type_id'.
    					    ' AND d.report_id = "'.$this->getId().'"',
    						array('value_varchar' => 'd.value'));
    		$readresult=$connection->fetchAll($select);
    		foreach ($readresult as $fila){
    			switch ($fila['type']){
    				case 'int': $this->atributes[$fila['type_code']] = $fila['value_int'];
    					break;
    				case 'text': $this->atributes[$fila['type_code']] = $fila['value_text'];
    					break;
    				case 'varchar': $this->atributes[$fila['type_code']] = $fila['value_varchar'];
    					break;
    			}
    		}
    	}catch (Exception  $err){
    		$this->error=true;
	    	$this->errorMsg = "Error loading atributes :".$err->getMessage();
	    	$this->errorSQL = $sql;
    	}
    } 
    
    private function loadChartSeries(){
    	try{
    		$coreResource = Mage::getSingleton('core/resource');
    		$connection = $coreResource->getConnection('core_read');
    		$select = $connection->select()
					    		->from($this->gtn('sqlrpt_chart_series'), array('serie_num','column_num'))
					    		->where('report_id = ?',$this->getId())
					    		->order(array('serie_num'));
    		$readresult=$connection->fetchAll($select);
			
    		foreach ($readresult as $fila){
    			$this->chart_series[$fila['serie_num']] = $fila['column_num'];
    		}
    	}catch (Exception  $err){
    		$this->error=true;
    		$this->errorMsg = "Error loading combo list :".$err->getMessage();
    		$this->errorSQL = $sql;
    	}
    }
    
    public function setComboList($comboList){
    	$this->combosList = array();
    	$lista="";
    	foreach ($comboList as $comboId ){
    		if($comboId<>""){
    			$lista .= $comboId.",";
    			$combo = Mage::getModel('sqlreport/combo');
    			$combo->loadCombo($comboId);
    			if($combo->isError()){
    				$this->error=true;
    				$this->errorMsg= "Error in combo [".$combo->getId()."]: ".$combo->getErrorMsg();
    				$this->errorSQL;
    			}
    			array_push($this->combosList,$combo);
    		}
    	}
    }
    
    protected function executeQuery($limit = null){
    	$sql = $this->setParams($this->sql);


    	try{
	    	$connection = Mage::getSingleton('core/resource')->getConnection('reports');

            if ($limit !== null) {
                //has any limit at the end of query
                $len = strlen($sql);
                $pos = strpos(strtolower($sql), 'limit');
                if (empty($pos)
                    || ($len - 10) < $pos
                ) {
                   $sql = $sql.$limit;
                }
            }

            $readresult = $connection->query($sql);

	    	$this->result = $readresult->fetchAll();
	    	$this->resultSum = array();
	    	foreach ($this->result as $fila){
	    		foreach ($fila as $key => $value){
	    			array_push($this->columnsName,$key);
	    			array_push($this->resultSum,0);
	    		}
	    		break 1;
	    	}	


	    }catch (Exception  $err){
	    	$this->error=true;
	    	$this->errorMsg = "Error in sql :".$err->getMessage();
	    	$this->errorSQL = $sql;
	   	}

    }
    
    public function getColumnsName(){
    	return $this->columnsName;
    }
    
    public function getHtmlProperty($column){
    	return 'width="500"';
    }
    
    public function getWidthTable(){
    	return 100*sizeof($this->getColumnsName());
    }
    
    public function getCssTable(){
    	//return "width: ".$this->getWidthTable()."px";
    	return "";
    }
    
    public function getHeaderHtmlProperty($column){
    	return 'class=" no-link"';
    }
    
    public function getHeaderHtml($column){
    	return $column;
    }
    
    public function getSize(){
    	return sizeof($this->result);
    }
    
    public function getResults(){
    	return $this->result;
    }
    
    public function getCssProperty($value){
    	if(is_numeric($value)){
    		return "a-right";
    	}else{
    		return "";
    	}
    }
    
    public function getTitle(){
    	return $this->title;
    }
    
    public function setTitle($title){
    	$this->title = $title;
    }
    
    
    public function getComboList(){
    	return $this->combosList;
    }
    
    public function getId(){
    	return $this->id;
    }
    
    public function setId($id){
    	$this->id = $id;
    }
    
    public function hasCombos(){
    	if(sizeof($this->combosList)>0){
    		// No contamos con los evaluated
    		foreach ($this->combosList as $combo){
    			
    			if($combo->getType()!="evaluated"){
    				return true;
    			}
    		}
    		return false;
    	}else{
    		return false;
    	}
    }
    
    public function hasRows(){
    	return sizeof($this->result)>0;
    }
    
    private function addLog($texto){
    	$this->log .=$texto."<br />";
    }
    
    public function getLog(){
    	return $this->log;
    }
    
    public function isComboSeted(){
    	return $this->combosSeted;
    }
    
    public function isError(){
    	return $this->error;
    }
    
    public function getErrorMsg(){
    	return $this->errorMsg;
    }
    
    public function getResultSum(){
    	return $this->resultSum;
    }
    
    
    public function addResult2Sum($column,$value){
    	if(is_numeric($this->resultSum[$column])){
    		if(is_numeric($value)){
    			$this->resultSum[$column] =$this->resultSum[$column] + $value;
    		}else if($value!=""){
    			$this->resultSum[$column] = "s";
    		}
    	}
    }
    
    public function haveResultSum(){
    	foreach ($this->resultSum as $value){
    		if ($value!="s") {
    			return true;
    		}
    	}
    	return false;
    }
    
    private function setParams($sql){
    	$salida = $sql;
    	$salida = str_replace($this->setupmanager->getValue('prefix_table'),Mage::getConfig()->getTablePrefix() ,$salida);
    	foreach($this->getComboList() as $combo){
    		if($combo->getType()=="date"){
    			
    			$valor = $combo->getValueSet();
    			$valor = @$valor[0];
    			
    			$salida = str_replace ($this->setupmanager->getValue('prefix_parameter').$combo->getParameter(),"STR_TO_DATE('".$valor."','".$this->setupmanager->getValue('date_mask')."')",$salida);
    		}else if($combo->getType()=="evaluated"){
    			$valor = "";
    			$sentence = $combo->getAtribute("sql");
    			//TODO escapar todo
    			try{
			    	eval("\$valor = ".$sentence.";");
			    	$salida = str_replace ($this->setupmanager->getValue('prefix_parameter').$combo->getParameter(),$valor,$salida);
			    }catch (Exception  $err){
			    	echo "error";
			    }
    		}else if($combo->getType()=="text"){
    			
    			$valor = $combo->getValueSet();
    			$salida = str_replace ($this->setupmanager->getValue('prefix_parameter').$combo->getParameter(),$valor,$salida);
    		}else {
    			$valor="";
    			if("checkbox-multiple"==$combo->getAtribute("selectType")){
    				$valor ="(";
    				foreach($combo->getValueSet() as $valueSet){
    					$valor.="'".$valueSet."',";
    				}
    				$valor = substr($valor, 0, strlen($valor)-1);
    				$valor .=")";
    			}else{
    				$valor = $combo->getValueSet();
    				$valor= @$valor[0];
    			}
    			$salida = str_replace ($this->setupmanager->getValue('prefix_parameter').$combo->getParameter(),$valor,$salida);
    		}
    	}
    	
    	return $salida;
    }
    
    public function getNumColumns(){
    	return sizeof($this->columnsName);
    }
    
    public function getSqlNoParams(){
    	return $this->sql;
    }
    
    public function getSqlYesParams(){
    	return $this->setParams($this->sql);
    }
    
    public function setSql($sql){
    	$this->sql=$sql;
    }
    
    public function getGroup(){
    	return $this->group;
    }
    
    public function setGroup($groupId){
    	$this->group->loadMe($groupId);
    }
    
    public function getAtribute($atribute){
    	if(array_key_exists($atribute,$this->atributes )){
    		return $this->atributes[$atribute];
    	}else{
    		return '';
    	}
    }
    
    public function getCombosListBasic(){
    	return $this->combosListBasic;
    }
    
    public function setCombosListBasic($list){
    	$this->combosListBasic = $list;
    }
    
    public function setAtribute($atribute,$value){
    	$this->atributes[$atribute] = $value;
    }
    
    public function hasChart(){
    	return "" != $this->getAtribute('chart_type');
    }
    
    public function toJsonResponse(){
    	
    	$data[0] = $this->toArrayex();
    	return json_encode($data);
    }
    
    public function toJsonComplete(){
    	$data = array();
    	// GROUP
    	$data[0] = array("object_type" => "group",
    					 "group_id" => $this->getGroup()->getId(),
    					 "description" =>$this->getGroup()->getDescription(),
    					 "orden" => $this->getGroup()->getOrden());
    	// FILTERS
    	foreach($this->getComboList() as $combo){
    		array_push($data,$combo->toArrayex());
    	}
    	
    	// LINKS
    	foreach($this->links as $link){
    		array_push($data,$link->toArrayex());
    	}
    	
    	// REPORT
    	array_push($data,$this->toArrayex());
    	
    	return json_encode($data);
    }
    
    public function toArrayex(){
    	$comboList = $this->getComboList();
    	$comboList2json = array();
    	foreach ($comboList as $combo){
    		$comboList2json[]=$combo->getId();
    	}
    	 
    	$chart_series2json = array();
    	foreach ($this->chart_series as $serie => $column){
    		$chart_series2json[]=$column;
    	}
    	 
    	$atributes2json = array();
    	foreach($this->atributes as $key => $value){
    		if(""!=$value && null != $value){
    			$atributes2json[$key] = $value;
    		}
    	}
    	 
    	$salida = array("object_type" => "report",
    			"report_id" => $this->getId(),
    			"title" => $this->getTitle(),
    			"report_sql" => $this->getSqlNoParams(),
    			"group_id" => $this->getGroup()->getId(),
    			"atributes" => $atributes2json,
    			"chart_series" => $chart_series2json,
    			"combos" =>$comboList2json,
    			"linkTR" => $this->getLinkList("TR"),
    			"linkTRVariables" => $this->getLinkVariableTR(),
    			"linkTD" => $this->getLinkList("TD"));
    	return $salida;
    }
    
    public function saveReport(){
    	try{
    		$connection = Mage::getSingleton('core/resource')->getConnection('core_write');
    		
    		$data = array(
    				'title' => $this->getTitle(),
    				'report_sql'  => $this->getSqlNoParams(),
    				'group_id'    => $this->getGroup()->getId()
    		);
    		
    		$where = array($connection->quoteInto('report_id=?', $this->getId()));
    		$where2 = array($connection->quoteInto('report_link_id in (SELECT id FROM '.$this->gtn('sqlrpt_report_link').' WHERE report_id = ?)', $this->getId()));
    		$connection->beginTransaction();
    		
    		$connection->update($this->gtn('sqlrpt_report'), $data, $where);
    		
    		$connection->delete($this->gtn('sqlrpt_report_combo'), $where);
    		$connection->delete($this->gtn('sqlrpt_report_int'), $where);
    		$connection->delete($this->gtn('sqlrpt_report_text'), $where);
    		$connection->delete($this->gtn('sqlrpt_report_varchar'), $where);
    		$connection->delete($this->gtn('sqlrpt_chart_series'), $where);
    		
    		$connection->delete($this->gtn('sqlrpt_report_link_value'), $where2);
    		$connection->delete($this->gtn('sqlrpt_report_link'), $where);
    		
    		$i=1;
    		foreach ($this->getComboList() as $combo){
    			$dataInsert = array(
    					'report_id' => $this->getId(),
    					'combo_id' => $combo->getId(),
    					'order_n' => $i);
    			$connection->insert($this->gtn('sqlrpt_report_combo'),$dataInsert);
    			$i++;
    		}
    		
    		// ATTRIBUTES
    		$this->saveAttributes($connection);
    		
    		// CHART SERIES
    		$this->saveChartSeries($connection);
    		
    		// LINKS
    		$this->saveLinks($connection);
    		
    		$connection->commit();
    		$data[0] = array("type" => "success-msg",
    				"msg" => "Report saved");
    		return json_encode($data);
    		
    	}catch (Exception  $err){
	    	$data[0] = array("type" => "error-msg",
	    			"msg" => $err->getMessage());
	    	return json_encode($data);
	   	}
    }
    
    public function deleteReport(){
    	try{
    		$connection = Mage::getSingleton('core/resource')->getConnection('core_write');
    		
    		$connection->beginTransaction();
    		$where = array($connection->quoteInto('report_id=?', $this->getId()));  
    		$where2 = array($connection->quoteInto('report_link_id in (SELECT id FROM '.$this->gtn('sqlrpt_report_link').' WHERE report_id = ?)', $this->getId()));  		
    		
    		$connection->delete($this->gtn('sqlrpt_report_combo'), $where);
    		$connection->delete($this->gtn('sqlrpt_report_int'), $where);
    		$connection->delete($this->gtn('sqlrpt_report_text'), $where);
    		$connection->delete($this->gtn('sqlrpt_report_varchar'), $where);
    		$connection->delete($this->gtn('sqlrpt_chart_series'), $where);
    		$connection->delete($this->gtn('sqlrpt_report_link_value'), $where2);
    		$connection->delete($this->gtn('sqlrpt_report_link'), $where);
    		
    		$connection->delete($this->gtn('sqlrpt_report_role'), $where);
    		$connection->delete($this->gtn('sqlrpt_report'), $where);
    		
    		$connection->commit();
    		$data[0] = array("type" => "success-msg",
    				"msg" => "Report deleted");
    		return json_encode($data);
    	
    	}catch (Exception  $err){
    		$data[0] = array("type" => "error-msg",
    				"msg" => $err->getMessage());
    		return json_encode($data);
    	}
    	
    }
    
    public function hasCombo($combo_id){
    	foreach ($this->getComboList() as $combo){
    		if($combo->getId()==$combo_id){
    			return true;
    		}	
    	}
    	return false;
    }
    
    public function setChartSeries($series){
    	$this->chart_series=$series;
    }
    
    public function addMeAsNew(){
    	try{
    		$connection = Mage::getSingleton('core/resource')->getConnection('core_write');
    		$connection->beginTransaction();
    		// REPORT
    		$fields  = array(
    				'report_id' => $this->getId(),
    				'title' => $this->getTitle(),
    				'report_sql' => $this->getSqlNoParams(),
    				'group_id' => $this->getGroup()->getId()
    		);
    		$connection->insert($this->gtn('sqlrpt_report'), $fields);
    		// FILTERS
    		$i=1;
    		foreach ($this->combosListBasic as $combo){
    			if($combo!=""){
    				$dataInsert = array(
    						'report_id' => $this->getId(),
    						'combo_id' => $combo ,
    						'order_n' => $i);
    				$connection->insert($this->gtn('sqlrpt_report_combo'),$dataInsert);
    				$i++;
    			}
    		}
    		
    		// ATTRIBUTES
    		$this->saveAttributes($connection);
    		// CHART SERIES
    		$this->saveChartSeries($connection);
    		// LINKS
    		$this->saveLinks($connection);
    		
    		$connection->commit();
    		

    		return true;
    	}catch (Exception  $err){
    		return $err->getMessage();
    	}
    }
    
    public function saveAttributes($connection){
    	$connectionRead = Mage::getSingleton('core/resource')->getConnection('core_read');
    	
    	$select = $connectionRead->select()
    	->from(array('a' => $this->gtn('sqlrpt_report_type')),
    			array('a.type_id', 'a.type_code','a.type'));
    	
    	$readresult=$connectionRead->fetchAll($select);
    	foreach ($readresult as $fila){
    		if(""!=$this->getAtribute($fila['type_code'])){
    			$dataInsert = array(
    					'type_id' => $fila['type_id'],
    					'report_id' => $this->getId() ,
    					'value' => $this->getAtribute($fila['type_code']));
    			$connection->insert($this->gtn('sqlrpt_report_'.$fila['type']),$dataInsert);
    		}
    	}
    	
    }
    
    public function saveChartSeries($connection){
    	$i=0;
    	foreach($this->chart_series as $serie){
    		if($serie!=""){
    			$dataInsert = array(
    					'report_id' => $this->getId(),
    					'serie_num' =>  $i+1,
    					'column_num' => $serie);
    			$connection->insert($this->gtn('sqlrpt_chart_series'),$dataInsert);
    			$i++;
    		}
    		 
    	}
    }
    
    public function printJSChart(){
    	$xName = $this->getColumnsName();
    	$xName = $xName[$this->getAtribute('chartXValue') - 1];
    	
    	$salida = "nv.addGraph(function() {";
    	switch ($this->getAtribute('chart_type')){
    		case 'lineChart': 
    			$salida .="	var chart = nv.models.lineChart();";
    			break;
    		case 'stackedAreaChart': 
    			$salida .= "var chart = nv.models.stackedAreaChart()
                .x(function(d) { return d[0] })
                .y(function(d) { return d[1] })
                .clipEdge(true);";
    			break;
    		case 'discreteBarChart':
    			$salida .="var chart = nv.models.discreteBarChart()
					      .x(function(d) { return d.label })
					      .y(function(d) { return d.value })
					      .staggerLabels(true)
					      .tooltips(false)
					      .showValues(true);";
    			break;
    		case 'multiBarChart':
    			$salida .= "var chart = nv.models.multiBarChart();";
    			break;
    		case 'multiBarHorizontalChart':
    			$salida .= "var chart = nv.models.multiBarHorizontalChart()
					      .x(function(d) { return d.label })
					      .y(function(d) { return d.value })
					      .margin({top: 30, right: 20, bottom: 50, left: 175})
					      .showValues(true)
					      .tooltips(false)
					      .showControls(false);";
    			break;
    		case 'pieChart':
    			$salida .="var chart = nv.models.pieChart()
					      .x(function(d) { return d.label })
					      .y(function(d) { return d.value })
					      .showLabels(true);";
    			break;
    		case 'pieDonutChart':
    				$salida .="var chart = nv.models.pieChart()
					      .x(function(d) { return d.label })
					      .y(function(d) { return d.value })
					      .showLabels(true)
    					  .donut(true);";
    				break;
    	}
    	
    	/*if($this->getAtribute('chart_type') != 'pieChart' ||
    	   $this->getAtribute('chart_type') != 'pieDonutChart'){
    		$salida .= " chart.xAxis
			      .axisLabel('".$xName."')
			      .tickFormat(d3.format(',r'));";
		}
		if($this->getAtribute('chart_type') != 'pieChart' ||
    	   $this->getAtribute('chart_type') != 'pieDonutChart'){
			$salida .= "  chart.yAxis
			      .tickFormat(d3.format(',.2f'));";
    	}
    	*/
		$salida .= "d3.select('#chart svg')
			      .datum(data)
			      .transition().duration(500)
			      .call(chart);
			  nv.utils.windowResize(chart.update);
			  return chart;
			});";
    	
    	return $salida;
    }
    
    public function setChart_series($serie){
    	$this->chart_series=$serie;
    }
    
    public function printDataChart(){
    	
    	$xName = $this->getColumnsName();
    	$xName = $xName[$this->getAtribute('chartXValue') - 1];
    	$salida = "data = [";
    	
    	for($i = 1; $i<= sizeof($this->chart_series);$i++){
    		$columnNumber = $this->chart_series[$i];
    		$yName = $this->getColumnsName();
    		$yName = $yName[$columnNumber-1];
	    	switch ($this->getAtribute('chart_type')){
	    		case 'lineChart': 
	    		case 'multiBarChart':
	    			$salida .="{key: \"".$yName."\", values: [ ";
	    			foreach ($this->getResults() as $result){
			    		$salida .= "{x:".$this->prs($result[$xName]).",y:".$this->prs($result[$yName])."},";
			      	}
			      	$salida .= "]},";
	    		break;
	    		case 'stackedAreaChart':
	    			$salida .="{key: \"".$yName."\", values: [ ";
	    			foreach ($this->getResults() as $result){
	    				$salida .= "[".$this->prs($result[$xName]).",".$this->prs($result[$yName])."],";
	    			}
	    			$salida .= "]},";
	    		break;
	    		case 'discreteBarChart':
	    		case 'multiBarHorizontalChart':
	    			$salida .="{key: \"".$yName."\", values: [ ";
	    			foreach ($this->getResults() as $result){
			    		$salida .= "{\"label\":".$this->prs($result[$xName]).",\"value\":".$this->prs($result[$yName])."},";
			      	}
			      	$salida .= "]},";
			    break;
	    		case 'lineAndBar':
	    			$salida .="{key: \"".$yName."\",bar: true, values: [ ";
	    			foreach ($this->getResults() as $result){
			    		$salida .= "{\"label\":".$this->prs($result[$xName]).",\"value\":".$this->prs($result[$yName])."},";
			      	}
			      	$salida .= "]},";
	    			break;
	    		case 'pieChart':
	    		case 'pieDonutChart':
	    			foreach ($this->getResults() as $result){
			    		$salida .= "{\"label\":".$this->prs($result[$xName]).",\"value\":".$this->prs($result[$yName])."},";
			      	}
	    		break;
	    	}
	    	
    	}
		$salida = substr($salida,0,strlen ($salida)-1);
		$salida.="];";
    	
    	return $salida;
    }
    
    private function prs($value){
    	if(is_numeric($value)) {
    		return $value;
    	}else{
    		return '"'.$value.'"';
    	}
    }
   
    
    /**
     * Export report grid to CSV format
     */
    public function exportCsv(){
    	$result="";
    	$sep=$this->setupmanager->getValue("exp_col_delimiter");
    	
    	if($this->setupmanager->getValue("svn_print_header")){
    		$primero=true;
    		foreach ($this->getColumnsName() as $_column){
    			if($primero){
    				$result.= $_column;
    				$primero=false;
    			}else{
    				$result.= $sep.$_column;
    			}
    		}
    		$result.="\r\n";
    	}
    	 
    	foreach ($this->getResults() as $rowResult){
    		$primero=true;
    		foreach ($this->getColumnsName() as $_column){
    			if($primero){
    				$primero=false;
    			}else{
    				$result.=$sep;
    			}	
    			if($this->utils->getTypeData($rowResult[$_column])==1){
    				$result.=$this->setupmanager->getValue("svn_text_qualifier").$rowResult[$_column].$this->setupmanager->getValue("svn_text_qualifier");
    			}else{
    				$result.=$rowResult[$_column];
    			}
    		}
    		$result.="\r\n";
    	}
    	 
    	return $result;
    }
    
    /**
     * Export report grid to XML format
     */
    public function exportXml($wsName) {
    	$xml = '<'.'?xml version="1.0"?'.'><'.'?mso-application progid="Excel.Sheet"?'.'>
				<Workbook xmlns:x="urn:schemas-microsoft-com:office:excel"
  				xmlns="urn:schemas-microsoft-com:office:spreadsheet"
  				xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet">';
    	
    	
    	$xml .= '<Worksheet ss:Name="'.$wsName.'"><ss:Table>';
    	// HEADER
    	$xml .= '<ss:Row>';
    	foreach ($this->getColumnsName() as $_column){
    		$xml .= '<ss:Cell><Data ss:Type="String">'.$_column.'</Data></ss:Cell>';
    	}
    	$xml .= '</ss:Row>';
    	
    	// DATA
    	foreach ($this->getResults() as $rowResult){
    		$xml .= '<ss:Row>';
    		foreach ($this->getColumnsName() as $_column){
    			$xml .= '<ss:Cell><Data ss:Type="String">'.$rowResult[$_column].'</Data></ss:Cell>';
    		}
    		$xml .= '</ss:Row>';
    	}
    	$xml .= '</ss:Table></Worksheet>';
    	$xml .= '</Workbook>';
    	
    	return $xml;
    }
    
    public function gtn($tableName){
    	return Mage::getSingleton('core/resource')->getTableName($tableName);
    }
    
	protected function loadLinkList(){    
    	try{
    		$coreResource = Mage::getSingleton('core/resource');
    		$connection = $coreResource->getConnection('core_read');
    		$select = $connection->select()
    				->from(array('a' => $this->gtn('sqlrpt_report_link')),
    					   array('id_report_link' =>'a.id','a.column'))
    				->join(array('b' => $this->gtn('sqlrpt_link')),
    						'a.link_id=b.link_id'.
    					    ' AND a.report_id = "'.$this->getId().'"',
    						array('b.link_id','b.description','b.url','b.type'));
    		$readresult=$connection->fetchAll($select);
	    	foreach ($readresult as $fila){
	    		$link = Mage::getModel('sqlreport/link');
	    		$link->loadMe2($fila['link_id'],$fila['description'],$fila['url'],$fila['type']);
	    		$link->loadVariable($fila['id_report_link'],$fila['column']);
	    		$link->setReportLinkId($this->getId());
	    		array_push($this->links,$link);
	    	}
    	}catch (Exception  $err){
    		$this->error=true;
	    	$this->errorMsg = "Error loading combo list :".$err->getMessage();
	    	$this->errorSQL = $sql;
    	}
    }
    
    public function addLink($link){
    	array_push($this->links,$link);
    }
    
    public function getLinkList($type){
    	$list = array();
    	foreach ($this->links as $link){
    		if($type==$link->getType()){
    			$data = array($link->getId() => $link->getColumn());
    			array_push($list,$data);
    		}
    	}
    	return $list;
    }
    
    public function getLinkVariableTR(){
    	foreach ($this->links as $link){
    		if("TR"==$link->getType()){
    			return $link->getVariables();
    		}
    	}
    	return array();
    }
    
    public function getLinkResult($type,$column,$result){
    	if("TR" == $type){
    		$urlLink = "";
    		foreach ($this->links as $link){
    			
    			if($type==$link->getType()){
    				$urlLink = $link->replaceVariables($link->getUrl(),$result,$this->getColumnsName());
    			}
    		}
    		if(""!=$urlLink){
    			$title = $link->replaceVariables($link->getDescription(),$result,$this->getColumnsName());
    			return 'title= "'.$title.'" href = "'.Mage::helper('adminhtml')->getUrl($urlLink).'" target="'.$link->getTarget().'"';
    		}else{
    			return "";
    		}	
    	}else if("TD"==$type){
	    	// LINKS
		    $textoFinal = "";
		    if($result!=""){
	    		foreach ($this->links as $link){
	    			$urlLink = $link->getType();
	    			if($type==$link->getType() && $column==$link->getColumn()){
	    				$urlLink = $link->replaceVariables($link->getUrl(),$result,$this->getColumnsName());
	    				$ruta = $link->getUrl();
		    			$parametro = "id";
		    			$valor = $result;
		    			$urlLink = Mage::helper('adminhtml')->getUrl($ruta, array($parametro => $valor));
		    			$textoFinal = '<a href="'.$urlLink.'" title="'.$link->getDescription().'" target="'.$link->getTarget().'">'.$result.'</a>';
	    			}
	    		}
	    		
				if($textoFinal==""){
					// EMAIL
					$regex = '/(\S+@\S+\.\S+)/';
			    	$replace = '<a href="mailto:$1">$1</a>';
			    	$textoFinal = preg_replace($regex, $replace, $result);
				}
	    	}

		    return $textoFinal;
    	}
    	
    }
    
    public function resetLinks(){
    	$this->links=array();
    }
    
    private function saveLinks($connection){
    	foreach($this->links as $link){
    		$dataInsert = array(
    				'report_id' => $this->getId(),
    				'link_id' =>  $link->getId(),
    				'column' => $link->getColumn());
    		$connection->insert($this->gtn('sqlrpt_report_link'),$dataInsert);
    		$id = $connection->lastInsertId();
    		$variablesList = $link->getVariables();
    		foreach($variablesList as $key=>$value){
    			$dataInsert = array(
    				'report_link_id' => $id,
    				'variable' =>  $key,
    				'column_num' => $value);
    			$connection->insert($this->gtn('sqlrpt_report_link_value'),$dataInsert);
    		}
    	}
    }


}
?>