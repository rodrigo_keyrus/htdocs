<?php
class Serpini_Sqlreport_Model_Group extends Mage_Core_Model_Abstract
{
	protected $id;
	protected $description;
	protected $orden;
	
	protected function _construct(){
		$this->_init('sqlreport/group');
		
	}
	
	public function loadMe($id){
		$utils = Mage::helper('sqlreport/Utils');
		$this->id=$id;
		$coreResource = Mage::getSingleton('core/resource');
    	$connection = $coreResource->getConnection('core_read');
		$select = $connection->select()
	    			->from(array('a' => $utils->gtn('sqlrpt_group')), 
	    				   array('a.group_id','a.description','a.orden'))
	    			->where('group_id = ?',$this->getId());
	    $readresult=$connection->fetchAll($select);
		foreach ($readresult as $fila){
    		$this->setDescription($fila['description']);
    		$this->setOrden($fila['orden']);
	    }
	    	
	}
	
	public function getId(){
		return $this->id;
	}
	
	
	public function getDescription(){
		return $this->description;
	}
	
	public function setDescription($description){
		$this->description=$description;
	}
	
	public function getOrden(){
		return $this->orden;
	}
	
	public function setOrden($orden){
		$this->orden=$orden;
	}
	
}

