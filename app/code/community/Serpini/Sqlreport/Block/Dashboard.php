<?php
class Serpini_Sqlreport_Block_Dashboard extends Mage_Adminhtml_Block_Template
{
    protected $cacheLifetime = 300;
    const CACHE_LIFETIME = 'sqlreport/settings/cache_lifetime';

    protected function _construct()
    {
        $configCacheLifetime = Mage::getStoreConfig(self::CACHE_LIFETIME);
        if ($configCacheLifetime) {
            $this->cacheLifetime = $configCacheLifetime;
        }
    }

    /**
     * @return Varien_Db_Adapter_Interface
     */
    protected function _getCoreRead()
    {
        $readConn = Mage::getSingleton('core/resource')->getConnection('reports');
        return $readConn;
    }

    /**
     * @return string
     */
    public function getNowDate()
    {
        $validator = new Zend_Validate_Date();
        $date = $this->getRequest()->getParam('report_date');

        if ($validator->isValid($date)) {
            return $date;
        }

        return Mage::getModel('core/date')->date('Y-m-d');
    }


    /**
     * Checking all configured alerts
     *
     * @return array
     */
    public function getSalesAlert()
    {

        $monitoring         = Mage::getModel('bseller_monitoring/monitoring');
        $lastOrderInterval  = $monitoring->getLastOrderInterval();
        $hourRange          = $monitoring->getSalesByHourRange();

        $maxSeconds = isset($hourRange[$lastOrderInterval['hour_range']]) ? $hourRange[$lastOrderInterval['hour_range']]['seconds'] : 1;
        $flYellow   = $maxSeconds * Mage::getStoreConfig('bseller_monitoring/configuration/fl_yellow');
        $flRed      = $maxSeconds * Mage::getStoreConfig('bseller_monitoring/configuration/fl_red');

        $data = [
            'increment_id' => $lastOrderInterval['increment_id'],
            'sales_time' => $lastOrderInterval['sales_time']
        ];

        /** Verifica se entra no criterio de alerta verde */
        if ($lastOrderInterval['dif'] <= $flYellow) {
            $data['time_alert'] = $flYellow;
            $data['percent_bar'] = ( $lastOrderInterval['dif'] * 100) / $flYellow ;
            $data['time_remaining'] = $flYellow - $lastOrderInterval['dif'];
            $data['danger_time'] = $lastOrderInterval['dif'] - $flYellow;
            $data['status'] = BSeller_Monitoring_Model_Monitoring::ALERT_STATUS_ID_YELLOW;
            $data['class'] = 'success';
            return $data;
        }

        /** Verifica se entra no criterio de alerta amarelo */
        if ($lastOrderInterval['dif'] <= $flRed) {
            $data['time_alert'] = $flRed;
            $data['percent_bar'] = ( $lastOrderInterval['dif']* 100) / $flRed;
            $data['time_remaining'] = $flRed - $lastOrderInterval['dif'];
            $data['danger_time'] = $lastOrderInterval['dif'] - $flRed;
            $data['status'] = BSeller_Monitoring_Model_Monitoring::ALERT_STATUS_ID_YELLOW;
            $data['class'] = 'notice';
            return $data;
        }

        /** Verifica se entra no criterio de alerta vermelho */
        if ($lastOrderInterval['dif'] >= $flRed) {
            $data['time_alert'] = $flRed;
            $data['percent_bar'] = ( $lastOrderInterval['dif']* 100) / $flRed;
            $data['time_remaining'] = 0;
            $data['danger_time'] = $lastOrderInterval['dif'] - $flRed;
            $data['status'] = BSeller_Monitoring_Model_Monitoring::ALERT_STATUS_ID_RED;
            $data['class'] = 'danger';
        }

        return $data;

    }


    /**
     * Checking all configured alerts
     *
     * @return array
     */
    public function getSalesIntegratedAlert()
    {

        $monitoring         = Mage::getModel('bseller_monitoring/monitoring');
        $lastOrderInterval  = $monitoring->getLastOrderIntegratedInterval();

        $maxItervalInvoiced = 60 * Mage::getStoreConfig('bseller_monitoring/configuration/max_iterval_integrated');

        $data = $lastOrderInterval;

        $data['percent_bar'] = ( $lastOrderInterval['dif'] * 100) / $maxItervalInvoiced;
        $data['danger_time'] = $lastOrderInterval['dif'] - $maxItervalInvoiced;
        $data['time_alert'] = $maxItervalInvoiced;

        /** Verifica se entra no criterio de alerta verde */
        if ($lastOrderInterval['dif'] <= $maxItervalInvoiced) {
            $data['time_remaining'] = $maxItervalInvoiced - $lastOrderInterval['dif'];
            $data['status'] = BSeller_Monitoring_Model_Monitoring::ALERT_STATUS_ID_YELLOW;
            $data['class'] = 'success';
            return $data;
        }

        /** Verifica se entra no criterio de alerta amarelo */
        if ($lastOrderInterval['dif'] <= $maxItervalInvoiced) {
            $data['time_remaining'] = $maxItervalInvoiced - $lastOrderInterval['dif'];
            $data['status'] = BSeller_Monitoring_Model_Monitoring::ALERT_STATUS_ID_YELLOW;
            $data['class'] = 'notice';
            return $data;
        }

        /** Verifica se entra no criterio de alerta vermelho */
        if ($lastOrderInterval['dif'] >= $maxItervalInvoiced) {
            $data['time_remaining'] = 0;
            $data['status'] = BSeller_Monitoring_Model_Monitoring::ALERT_STATUS_ID_RED;
            $data['class'] = 'danger';
            $data['percent_bar'] = 100;
        }

        return $data;

    }


    /**
     * Checking all configured alerts
     *
     * @return array
     */
    public function getLastIntegratedPayment()
    {

        $monitoring         = Mage::getModel('bseller_monitoring/monitoring');
        $lastOrderInterval  = $monitoring->getLastPaymentIntegratedInterval();

        $minItervalInvoiced = 60 * Mage::getStoreConfig('bseller_monitoring/configuration/min_iterval_invoiced');
        $maxItervalInvoiced = 60 * Mage::getStoreConfig('bseller_monitoring/configuration/max_iterval_integrated');

        $data = $lastOrderInterval;

        $data['percent_bar']    = 1 * 100 / $maxItervalInvoiced;
        $data['danger_time']    = $lastOrderInterval['dif'] - $maxItervalInvoiced;
        $data['time_alert']     = $maxItervalInvoiced;

        /** Verifica se entra no criterio de alerta verde */
        if ($lastOrderInterval['dif'] <= $minItervalInvoiced) {
            $data['percent_bar']    = 1 * 100 / $minItervalInvoiced;
            $data['danger_time']    = $lastOrderInterval['dif'] - $minItervalInvoiced;
            $data['time_alert']     = $minItervalInvoiced;
            $data['time_remaining'] = $minItervalInvoiced - $lastOrderInterval['dif'];
            $data['status'] = BSeller_Monitoring_Model_Monitoring::ALERT_STATUS_ID_YELLOW;
            $data['class'] = 'success';
            return $data;
        }

        /** Verifica se entra no criterio de alerta amarelo */
        if ($lastOrderInterval['dif'] <= $maxItervalInvoiced) {
            $data['time_remaining'] = $maxItervalInvoiced - $lastOrderInterval['dif'];
            $data['status'] = BSeller_Monitoring_Model_Monitoring::ALERT_STATUS_ID_YELLOW;
            $data['class'] = 'notice';
            return $data;
        }

        /** Verifica se entra no criterio de alerta vermelho */
        if ($lastOrderInterval['dif'] >= $maxItervalInvoiced) {
            $data['time_remaining'] = 0;
            $data['status'] = BSeller_Monitoring_Model_Monitoring::ALERT_STATUS_ID_RED;
            $data['class'] = 'danger';
            $data['percent_bar'] = 100;
        }

        return $data;

    }



    /**
     * @return string
     */
    public function getTotalQtyCaptured()
    {

        $cacheKey = 'bseller_reports_totalqtycaptured_' . $this->getNowDate();
        $data     = Mage::app()->getCache()->load($cacheKey);

        if ($data) {
            return Zend_Json::decode($data);
        }

        $sql = "select count(entity_id) as grand_total from sales_flat_order ";
        $sql .= " WHERE DATE_FORMAT(CONVERT_TZ(created_at,'+00:00','-03:00'),'%Y-%m-%d') BETWEEN '".$this->getNowDate()."' ";
        $sql .= " AND '".$this->getNowDate()." 23:59:59'  ";

        $data = $this->_getCoreRead()->query($sql)->fetchColumn();
        Mage::app()->getCache()->save(Zend_Json::encode($data), $cacheKey, [], $this->cacheLifetime, null);

        return $data;
    }


    /**
     * @return string
     */
    public function getTotalCaptured()
    {

        $cacheKey = 'bseller_reports_totalcaptured_' . $this->getNowDate();
        $data     = Mage::app()->getCache()->load($cacheKey);

        if ($data) {
            return Zend_Json::decode($data);
        }

        $sql = "select sum(grand_total) as grand_total from sales_flat_order ";
        $sql .= " WHERE DATE_FORMAT(CONVERT_TZ(created_at,'+00:00','-03:00'),'%Y-%m-%d') BETWEEN '".$this->getNowDate()."' ";
        $sql .= " AND '".$this->getNowDate()." 23:59:59'  ";

        $data = $this->_getCoreRead()->query($sql)->fetchColumn();
        Mage::app()->getCache()->save(Zend_Json::encode($data), $cacheKey, [], $this->cacheLifetime, null);

        return $data;
    }


    /**
     * @return string
     */
    public function getTotalExtraCaptured()
    {

        $cacheKey = 'bseller_reports_totalextracaptured_' . $this->getNowDate();
        $data     = Mage::app()->getCache()->load($cacheKey);

        if ($data) {
            return Zend_Json::decode($data);
        }

        $sql = "select
                    DATE_FORMAT( CONVERT_TZ( ped.created_at, '+00:00', '-03:00' ), '%Y-%m-%d' ) as date,
                    sum( ped.shipping_amount ) as shipping_amount,
                    sum( ped.voucher_credit_amount_used ) as voucher
                from
                    sales_flat_order ped ";
        $sql .= " WHERE DATE_FORMAT(CONVERT_TZ(created_at,'+00:00','-03:00'),'%Y-%m-%d') BETWEEN '".$this->getNowDate()."' ";
        $sql .= " AND '".$this->getNowDate()." 23:59:59'  ";
        $sql .= " group by DATE_FORMAT( CONVERT_TZ( ped.created_at, '+00:00', '-03:00' ), '%Y-%m-%d' )  ";

        $data = $this->_getCoreRead()->query($sql)->fetch();
        Mage::app()->getCache()->save(Zend_Json::encode($data), $cacheKey, [], $this->cacheLifetime, null);

        return $data;
    }

    /**
     * @return string
     */
    public function getAverageTicket()
    {

        $cacheKey = 'bseller_reports_averageticket_' . $this->getNowDate();
        $data     = Mage::app()->getCache()->load($cacheKey);

        if ($data) {
            return Zend_Json::decode($data);
        }

        $sql = "select (sum(grand_total)/ count(*)) as ticket from sales_flat_order";
        $sql .= " WHERE DATE_FORMAT(CONVERT_TZ(created_at,'+00:00','-03:00'),'%Y-%m-%d') BETWEEN '".$this->getNowDate()."' ";
        $sql .= " AND '".$this->getNowDate()." 23:59:59'  ";

        $data = $this->_getCoreRead()->query($sql)->fetchColumn();
        Mage::app()->getCache()->save(Zend_Json::encode($data), $cacheKey, [], $this->cacheLifetime, null);

        return $data;

    }

    /**
     * @return string
     */
    public function getTotalBilled()
    {

        $cacheKey = 'bseller_reports_totalbilled_' . $this->getNowDate();
        $data     = Mage::app()->getCache()->load($cacheKey);

        if ($data) {
            return Zend_Json::decode($data);
        }

        $sql = "select sum(grand_total) as grand_total from sales_flat_order where erp_status = 'success_payment_approve'";
        $sql .= " AND DATE_FORMAT(CONVERT_TZ(created_at,'+00:00','-03:00'),'%Y-%m-%d') BETWEEN '".$this->getNowDate()."' ";
        $sql .= " AND '".$this->getNowDate()." 23:59:59'  ";

        $data = $this->_getCoreRead()->query($sql)->fetchColumn();
        Mage::app()->getCache()->save(Zend_Json::encode($data), $cacheKey, [], $this->cacheLifetime, null);

        return $data;


    }

    /**
     * @return string
     */
    public function getTotalPending()
    {
        $cacheKey = 'bseller_reports_totalpending_' . $this->getNowDate();
        $data     = Mage::app()->getCache()->load($cacheKey);

        if ($data) {
            return Zend_Json::decode($data);
        }

        $sql = "select sum(grand_total) as grand_total from sales_flat_order where erp_status not in ('success_payment_approve')";
        $sql .= " AND DATE_FORMAT(CONVERT_TZ(created_at,'+00:00','-03:00'),'%Y-%m-%d') BETWEEN '".$this->getNowDate()."' ";
        $sql .= " AND '".$this->getNowDate()." 23:59:59'  ";


        $data = $this->_getCoreRead()->query($sql)->fetchColumn();
        Mage::app()->getCache()->save(Zend_Json::encode($data), $cacheKey, [], $this->cacheLifetime, null);

        return $data;


    }


    /**
     * @return array
     */
    public function getTotalSalesByDevices()
    {
        $cacheKey = 'bseller_reports_totalsalesbydevice_'. $this->getNowDate();
        $data     = Mage::app()->getCache()->load($cacheKey);

        if ($data) {
            return Zend_Json::decode($data);
        }

        $sql = "SELECT sales.device_id as device,	count( * ) as total FROM sales_flat_order as sales ";
        $sql .= " WHERE DATE_FORMAT(CONVERT_TZ(sales.created_at,'+00:00','-03:00'),'%Y-%m-%d') BETWEEN '".$this->getNowDate()."' ";
        $sql .= " AND '".$this->getNowDate()." 23:59:59' GROUP BY sales.device_id ";

        $data = $this->_getCoreRead()->query($sql)->fetchAll();
        Mage::app()->getCache()->save(Zend_Json::encode($data), $cacheKey, [], $this->cacheLifetime, null);

        return $data;

    }


    /**
     * @return string
     */
    public function getTotalSalesByDeviceOs()
    {
        $cacheKey = 'bseller_reports_totalsalesbydeviceos'. $this->getNowDate();
        $data     = Mage::app()->getCache()->load($cacheKey);

        if ($data) {
            return Zend_Json::decode($data);
        }

        $sql = "SELECT sales.device_os as deviceos,	count(*) as total FROM sales_flat_order as sales ";
        $sql .= " WHERE sales.device_id = '".Esmart_Sales_Model_Device::DEVICE_ID_MOBILE."' ";
        $sql .= " AND DATE_FORMAT(CONVERT_TZ(sales.created_at,'+00:00','-03:00'),'%Y-%m-%d') BETWEEN '".$this->getNowDate()."' ";
        $sql .= " AND '".$this->getNowDate()." 23:59:59' GROUP BY sales.device_os ";

        $data = $this->_getCoreRead()->query($sql)->fetchAll();
        Mage::app()->getCache()->save(Zend_Json::encode($data), $cacheKey, [], $this->cacheLifetime, null);

        return $data;

    }


    /**
     * @return Zend_Db_Statement_Interface
     */
    public function getPaymentSales()
    {

        $cacheKey = 'bseller_reports_paymentsales_' . $this->getNowDate();
        $data     = Mage::app()->getCache()->load($cacheKey);

        if ($data) {
            return Zend_Json::decode($data);
        }

        $sql = "
				select method, count(method) as total, sum(grandtotal) as grandtotal
from (select DATE_FORMAT(ped.created_at, '%d/%m/%Y')                                         as DataPedido,
             case
               when pay.method = 'mundipagg_boleto' then 'Boleto'
               when pay.method = 'mundipagg_creditcard' then 'Cartão de Crédito'
               when pay.method = 'free' then 'Crédito Sephora'
               when pay.method = 'bseller_skyhub_standard' then 'Marketplace'
               else 'Desconhecido' end as method,
             ped.grand_total                                                                 as grandtotal
      from sales_flat_order ped
             inner join sales_flat_order_payment pay on pay.parent_id = ped.entity_id";
        $sql .= " WHERE DATE_FORMAT(CONVERT_TZ(created_at,'+00:00','-03:00'),'%Y-%m-%d') BETWEEN '".$this->getNowDate()."' AND DATE_ADD('".$this->getNowDate()." 23:59:59 ', INTERVAL 1 DAY) ";
        $sql .= "	)  as aux_payment
					group by method order by grandtotal DESC ";

        $data = $this->_getCoreRead()->query($sql)->fetchAll();
        Mage::app()->getCache()->save(Zend_Json::encode($data), $cacheKey, [], $this->cacheLifetime, null);
        return $data;

    }


    /**
     * @param bool $billed
     * @return array|false|mixed
     */
    public function getSalesProductByHour($billed = false)
    {
        $cacheKey = 'bseller_reports_salesproductbyhour_' . $this->getNowDate();
        $data     = Mage::app()->getCache()->load($cacheKey);

        if ($data) {
            return Zend_Json::decode($data);
        }

        $sql = "
            SELECT
                 HOUR(CONVERT_TZ(ped.created_at, '+00:00', '-03:00')) AS hour
                ,COUNT(ped.entity_id) AS cont
                ,SUM((
                    SELECT 
                        SUM(item.qty_ordered)
                    FROM
                        sales_flat_order_item AS item
                    WHERE
                        item.order_id = ped.entity_id
                )) as produtos
                ,SUM(ped.grand_total) AS grand_total
                ,SUM(ped.total_invoiced ) AS invoiced
                ,SUM(ped.grand_total) / COUNT(ped.entity_id) AS ticket
            FROM
              sales_flat_order ped";
        $sql .= " WHERE CONVERT_TZ(ped.created_at,'+00:00','-03:00')  BETWEEN '".$this->getNowDate()."' AND DATE_ADD('".$this->getNowDate()."', INTERVAL 1 DAY) ";

        if ($billed) {
            $sql .= " AND erp_status = 'success_payment_approve'";
        }

        $sql .= " GROUP BY hour(CONVERT_TZ(ped.created_at, '+00:00', '-03:00')) ";

        $data = $this->_getCoreRead()->query($sql)->fetchAll();
        Mage::app()->getCache()->save(Zend_Json::encode($data), $cacheKey, [], $this->cacheLifetime, null);
        return $data;

    }


    /**
     * @return array
     */
    public function getBestSellers()
    {

        $cacheKey = 'bseller_reports_bestsellers_' . $this->getNowDate();
        $data     = Mage::app()->getCache()->load($cacheKey);

        if ($data) {
            return Zend_Json::decode($data);
        }

        $sql = " 
                select
                    itm.sku,
                    itm.name as produto,
                    manufacturer.manufacturer_name 	as marca,
                    sum( itm.price * itm.qty_ordered - itm.discount_amount ) as total,
                    sum( itm.qty_ordered ) as qty
                from
                    sales_flat_order ped 
                    inner join sales_flat_order_item itm        on itm.order_id = ped.entity_id 
                    inner join catalog_product_flat_1 flat      on flat.entity_id = itm.product_id and flat.tax_treatment = '1'
                    inner join zeon_manufacturer manufacturer   on manufacturer.manufacturer_id = flat.manufacturer
                where
                    CONVERT_TZ( ped.created_at, '+00:00', '-03:00' ) BETWEEN '".$this->getNowDate()." 00:00:00' AND '".$this->getNowDate()." 23:59:59'
                group by
                    itm.sku,
                    itm.name
                order by
                    sum( itm.qty_ordered ) desc limit 20;
                 ";

        $data = $this->_getCoreRead()->query($sql)->fetchAll();
        Mage::app()->getCache()->save(Zend_Json::encode($data), $cacheKey, [], $this->cacheLifetime, null);
        return $data;

    }



    /**
     * @return array
     */
    public function getBestSellersLowStock()
    {

        $cacheKey = 'bseller_reports_bestsellerslowstock_' . $this->getNowDate();
        $data     = Mage::app()->getCache()->load($cacheKey);

        if ($data) {
            return Zend_Json::decode($data);
        }

        $sql = " 
                select
                    itm.sku as sku,
                    itm.name as produto,
                    sum( itm.price * itm.qty_ordered - itm.discount_amount ) as total,
                    sum( itm.qty_ordered ) as qty,
                    stk.stock as stock,
                    stk.reserved_qty as reserved_qty,
                    stk.available_qty as available_qty
                from
                    sales_flat_order ped inner join sales_flat_order_item itm on
                    itm.order_id = ped.entity_id inner join catalog_product_flat_1 flat on
                    flat.entity_id = itm.product_id
                    and flat.tax_treatment = '1' inner join(
                        select
                            qtd.product_id,
                            qtd.qty as stock,
                            case
                                when sum( res.reserved_qty ) is null then 0
                                else sum( res.reserved_qty )
                            end as reserved_qty,
                            case
                                when qtd.qty - sum( res.reserved_qty ) is null then 0
                                else qtd.qty - sum( res.reserved_qty )
                            end as available_qty
                        from
                            cataloginventory_stock_item qtd left join esmart_axstock_detail res on
                            res.product_id = qtd.product_id
                        group by
                            qtd.product_id,
                            qtd.qty
                    ) stk on
                    stk.product_id = itm.product_id
                where
                    CONVERT_TZ( ped.created_at, '+00:00', '-03:00' ) BETWEEN '".$this->getNowDate()." 00:00:00' AND '".$this->getNowDate()." 23:59:59'
                    and stk.available_qty <= 30
                group by
                    itm.sku,
                    itm.name
                order by
                    sum( itm.qty_ordered ) desc,
                    stk.available_qty asc limit 20;
                 ";

        $data = $this->_getCoreRead()->query($sql)->fetchAll();
        Mage::app()->getCache()->save(Zend_Json::encode($data), $cacheKey, [], $this->cacheLifetime, null);
        return $data;

    }



    /**
     * @return array
     */
    public function getSampleBestSellers()
    {

        $cacheKey = 'bseller_reports_samplebestsellers_' . $this->getNowDate();
        $data     = Mage::app()->getCache()->load($cacheKey);

        if ($data) {
            return Zend_Json::decode($data);
        }

        $sql = " 
                SELECT 
                    B.sku 							as sku, 
                    B.name 							as produto, 
                    manufacturer.manufacturer_name 	as marca,
                    sum(B.qty_ordered) 	  			as qty
                FROM sales_flat_order A
                    INNER JOIN sales_flat_order_item B 				ON A.entity_id					= B.order_id
                    INNER JOIN catalog_product_flat_1 product_flat 	ON B.product_id 				= product_flat.entity_id
                    INNER JOIN zeon_manufacturer manufacturer 		ON product_flat.manufacturer 	= manufacturer.manufacturer_id
                WHERE product_flat.tax_treatment = '2' AND CONVERT_TZ( A.created_at, '+00:00', '-03:00' ) BETWEEN '".$this->getNowDate()." 00:00:00' AND '".$this->getNowDate()." 23:59:59'
                GROUP BY B.sku, B.name, manufacturer.manufacturer_name
                ORDER BY qty DESC
                LIMIT 20
                 ";

        $data = $this->_getCoreRead()->query($sql)->fetchAll();
        Mage::app()->getCache()->save(Zend_Json::encode($data), $cacheKey, [], $this->cacheLifetime, null);
        return $data;

    }


    /**
     * @return array
     */
    public function getGiftBestSellers()
    {

        $cacheKey = 'bseller_reports_giftbestsellers_' . $this->getNowDate();
        $data     = Mage::app()->getCache()->load($cacheKey);

        if ($data) {
            return Zend_Json::decode($data);
        }

        $sql = " 
                SELECT 
                    B.sku 							as sku, 
                    B.name 							as produto, 
                    manufacturer.manufacturer_name 	as marca,
                    sum(B.qty_ordered) 	  			as qty
                FROM sales_flat_order A
                    INNER JOIN sales_flat_order_item B 				ON A.entity_id					= B.order_id
                    INNER JOIN catalog_product_flat_1 product_flat 	ON B.product_id 				= product_flat.entity_id
                    INNER JOIN zeon_manufacturer manufacturer 		ON product_flat.manufacturer 	= manufacturer.manufacturer_id
                WHERE product_flat.tax_treatment = '3' AND CONVERT_TZ( A.created_at, '+00:00', '-03:00' ) BETWEEN '".$this->getNowDate()." 00:00:00' AND '".$this->getNowDate()." 23:59:59'
                GROUP BY B.sku, B.name, manufacturer.manufacturer_name
                ORDER BY qty DESC
                LIMIT 20
                 ";

        $data = $this->_getCoreRead()->query($sql)->fetchAll();
        Mage::app()->getCache()->save(Zend_Json::encode($data), $cacheKey, [], $this->cacheLifetime, null);
        return $data;

    }


    /**
     * @return array
     */
    public function getSalesStatus()
    {

        $cacheKey = 'bseller_reports_salesstatus_' . $this->getNowDate();
        $data     = Mage::app()->getCache()->load($cacheKey);

        if ($data) {
            return Zend_Json::decode($data);
        }

        $sql = " SELECT B.label as 'status', COUNT(*) AS 'qty', ROUND(SUM(grand_total), 2) as 'total' ";
        $sql .= " FROM sales_flat_order A ";
        $sql .= " INNER JOIN sales_order_status_label B 		ON A.status 	= B.status ";
        $sql .= " WHERE CONVERT_TZ( created_at, '+00:00', '-03:00' ) BETWEEN '".$this->getNowDate()." 00:00:00' AND '".$this->getNowDate()." 23:59:59' ";
        $sql .= " GROUP BY B.label ORDER BY total DESC ";

        $data = $this->_getCoreRead()->query($sql)->fetchAll();
        Mage::app()->getCache()->save(Zend_Json::encode($data), $cacheKey, [], $this->cacheLifetime, null);
        return $data;
    }

    /**
     * @return array
     */
    public function getSalesByUtmSource()
    {

        $cacheKey = 'bseller_reports_salesbyutmsource_' . $this->getNowDate();
        $data     = Mage::app()->getCache()->load($cacheKey);

        if ($data) {
            return Zend_Json::decode($data);
        }

        $sql = " SELECT count(*) as qty, utm_source, sum(grand_total) as total FROM sales_flat_order ";
        $sql .= " WHERE utm_source != '' ";
        $sql .= " AND CONVERT_TZ( created_at, '+00:00', '-03:00' ) BETWEEN '".$this->getNowDate()." 00:00:00' AND '".$this->getNowDate()." 23:59:59' ";

        $sql .= " GROUP BY utm_source ORDER BY qty DESC LIMIT 10 ";

        $data = $this->_getCoreRead()->query($sql)->fetchAll();
        Mage::app()->getCache()->save(Zend_Json::encode($data), $cacheKey, [], $this->cacheLifetime, null);
        return $data;
    }




    /**
     * @return array
     */
    public function getSalesByCampaigns()
    {

        $cacheKey = 'bseller_reports_salesbycampaigns_' . $this->getNowDate();
        $data     = Mage::app()->getCache()->load($cacheKey);

        if ($data) {
            return Zend_Json::decode($data);
        }

        $sql = " SELECT
                rules.name as name
                ,count(sales_flat_order.increment_id) as sales
                ,sum(sales_flat_order.grand_total) as total
                ,sum(sales_flat_order.subtotal) as subtotal
                ,sum(sales_flat_order.discount_amount) as discount_amount
                FROM
                sales_flat_order
                INNER JOIN  salesrule rules ON FIND_IN_SET(rules.rule_id, sales_flat_order.applied_rule_ids)
                where
                rules.name not  like  'Frete%' 
        ";
        $sql .= " AND CONVERT_TZ( sales_flat_order.created_at, '+00:00', '-03:00' ) BETWEEN '".$this->getNowDate()." 00:00:00' AND '".$this->getNowDate()." 23:59:59' ";

        $sql .= " group by rules.name order by total DESC LIMIT 5 ";

        $data = $this->_getCoreRead()->query($sql)->fetchAll();
        Mage::app()->getCache()->save(Zend_Json::encode($data), $cacheKey, [], $this->cacheLifetime, null);
        return $data;
    }


    /**
     * @return array
     */
    public function getSalesbyManufacturer()
    {

        $cacheKey = 'bseller_reports_salesbymanufacturer_' . $this->getNowDate();
        $data     = Mage::app()->getCache()->load($cacheKey);

        if ($data) {
            return Zend_Json::decode($data);
        }

        $sql = "  SELECT
            manufacturer.manufacturer_name as name
            ,count(sales_flat_order_item.qty_ordered) as qty
            ,sum(sales_flat_order_item.price) as total
            FROM
            sales_flat_order_item
            INNER JOIN  catalog_product_flat_1 flat ON flat.entity_id = sales_flat_order_item.product_id
            INNER JOIN  zeon_manufacturer manufacturer ON manufacturer.manufacturer_id = flat.manufacturer
            INNER JOIN  sales_flat_order sales ON sales.entity_id = sales_flat_order_item.order_id
            WHERE flat.tax_treatment = 1
        ";

        $sql .= " AND CONVERT_TZ( sales.created_at, '+00:00', '-03:00' ) BETWEEN '".$this->getNowDate()." 00:00:00' AND '".$this->getNowDate()." 23:59:59' ";

        $sql .=" group by flat.manufacturer order by total DESC LIMIT 20 ";

        $data = $this->_getCoreRead()->query($sql)->fetchAll();
        Mage::app()->getCache()->save(Zend_Json::encode($data), $cacheKey, [], $this->cacheLifetime, null);
        return $data;
    }


    /**
     * @return array
     */
    public function getSalesByDepartment()
    {

        $cacheKey = 'bseller_reports_salesbydepartment_' . $this->getNowDate();
        $data     = Mage::app()->getCache()->load($cacheKey);

        if ($data) {
            return Zend_Json::decode($data);
        }

        $sql = "  
                  select
                    flt.ax_department as ax_department,
                    sum( itm.price * itm.qty_ordered - itm.discount_amount ) as total
                from
                    sales_flat_order ped inner join sales_flat_order_item itm on
                    itm.order_id = ped.entity_id inner join catalog_product_flat_1 flt on
                    flt.entity_id = itm.product_id
                    and flt.ax_department not in('DEP00010')
                where
                    CONVERT_TZ(
                        ped.created_at,
                        '+00:00',
                        '-03:00'
                    ) BETWEEN '".$this->getNowDate()." 00:00:00' AND '".$this->getNowDate()." 23:59:59'
                group by
                    flt.ax_department
                order by total desc
                  
        ";

        $data = $this->_getCoreRead()->query($sql)->fetchAll();
        Mage::app()->getCache()->save(Zend_Json::encode($data), $cacheKey, [], $this->cacheLifetime, null);
        return $data;
    }




    /**
     * @return array
     */
    public function getSalesIncludingRescue()
    {

        $cacheKey = 'bseller_reports_salesincludingrescue_' . $this->getNowDate();
        $data     = Mage::app()->getCache()->load($cacheKey);

        if ($data) {
            return Zend_Json::decode($data);
        }

        $sql = "  
                SELECT count(*) AS cont
                FROM sales_flat_order  O
                INNER JOIN sales_flat_order_item I ON I.order_id = O.entity_id
                WHERE 
                  I.points != ''
                AND CONVERT_TZ(O.created_at,'+00:00','-03:00')  BETWEEN '".$this->getNowDate()." 00:00:00' AND '".$this->getNowDate()." 23:59:59'
                GROUP BY
                DATE_FORMAT(CONVERT_TZ(O.created_at,'+00:00','-03:00'), '%Y-%m-%d' )
        ";

        $data = $this->_getCoreRead()->query($sql)->fetchColumn();
        Mage::app()->getCache()->save(Zend_Json::encode($data), $cacheKey, [], $this->cacheLifetime, null);
        return $data;
    }




    /**
     * @return array
     */
    public function getCustomerNewAccounts()
    {

        $cacheKey = 'bseller_reports_customernewaccounts_' . $this->getNowDate();
        $data     = Mage::app()->getCache()->load($cacheKey);

        if ($data) {
            return Zend_Json::decode($data);
        }

        $sql = "
                select
                 count(cus.entity_id)    as total_customer
                ,count(distinct nws.customer_id) as total_newsletter
                ,count( distinct ped.entity_id)  as total_buyer
                ,count(distinct bc.entity_id)  as total_bc
                from
                   customer_entity   cus
                left join  customer_entity_int  bc on bc.entity_id    = cus.entity_id
                          and bc.attribute_id   = '193'
                          and bc.value     = '1'
                          and cus.entity_type_id  = '1'
                left join newsletter_subscriber nws on nws.customer_id    = cus.entity_id
                left join  sales_flat_order  ped on ped.customer_id   = cus.entity_id
                          and CONVERT_TZ(ped.created_at,'+00:00','-03:00')  BETWEEN '".$this->getNowDate()." 00:00:00' AND '".$this->getNowDate()." 23:59:59'
                where
                 CONVERT_TZ(cus.created_at,'+00:00','-03:00')  BETWEEN '".$this->getNowDate()." 00:00:00' AND '".$this->getNowDate()." 23:59:59'
        ";

        $data = $this->_getCoreRead()->query($sql)->fetchAll();
        Mage::app()->getCache()->save(Zend_Json::encode($data), $cacheKey, [], $this->cacheLifetime, null);
        return $data;
    }



    /**
     * @return array
     */
    public function getSalesBySource()
    {

        $cacheKey = 'bseller_reports_salesbysource_' . $this->getNowDate();
        $data     = Mage::app()->getCache()->load($cacheKey);

        if ($data) {
            return Zend_Json::decode($data);
        }

        $sql = "
        SELECT
            campaign_source.channel as 'channel',
            count( * ) as 'sales',
            sum( flat_order.grand_total ) as 'total'
        FROM
            sales_flat_order as flat_order
        LEFT JOIN bseller_campaign_source AS campaign_source ON	trim(campaign_source.origin) = trim(flat_order.ga_history_last)
        WHERE
            flat_order.ga_history_last is not null
            and campaign_source.channel is not null
            and CONVERT_TZ(
                flat_order.created_at,
                '+00:00',
                '-03:00'
            ) BETWEEN '".$this->getNowDate()." 00:00:00' AND '".$this->getNowDate()." 23:59:59'
        GROUP BY
            campaign_source.channel
        ORDER BY
            total DESC
        LIMIT 20
        ";



        $data = $this->_getCoreRead()->query($sql)->fetchAll();
        Mage::app()->getCache()->save(Zend_Json::encode($data), $cacheKey, [], $this->cacheLifetime, null);
        return $data;
    }


    /**
     * Get sales origin (marketplace and magento) data
     *
     * @return array
     */
    public function getSalesOriginData()
    {
        $cacheKey = 'bseller_reports_sales_origin_' . $this->getNowDate();
        $data     = Mage::app()->getCache()->load($cacheKey);

        if ($data) {
            return Zend_Json::decode($data);
        }

        $sql = "
        SELECT
            flat_order.bseller_skyhub as skyhub,
            count(*) as 'sales',
            sum(flat_order.grand_total) as 'total'
        FROM sales_flat_order as flat_order
        WHERE
            CONVERT_TZ(
                flat_order.created_at,
                '+00:00',
                '-03:00'
            ) BETWEEN '".$this->getNowDate()." 00:00:00' AND '".$this->getNowDate()." 23:59:59'
        GROUP BY flat_order.bseller_skyhub
        ORDER BY total DESC
        ";

        $data = $this->_getCoreRead()->query($sql)->fetchAll();
        Mage::app()->getCache()->save(Zend_Json::encode($data), $cacheKey, [], $this->cacheLifetime, null);
        return $data;
    }


}
