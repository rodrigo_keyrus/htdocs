<?php
class Serpini_Sqlreport_Helper_GroupManager extends Mage_Core_Helper_Abstract {
	
	protected $groupList = null;
	
	public function getGroupList(){
		if($this->groupList == null){
			$groupList = array();
			$coreResource = Mage::getSingleton('core/resource');
			$connection = $coreResource->getConnection('core_read');
			$select = $connection->select()
								->from($this->gtn('sqlrpt_group'), array('group_id','description'))
								->order('orden');
			try{
				$readresult=$connection->fetchAll($select);
				foreach ($readresult as $fila){
					$group_id = $fila['group_id'];
					$description = $fila['description'];
					$groupList[]=array($group_id,$description);
				}
				$this->groupList= $groupList;
			}catch (Exception  $err){
				echo $err->getMessage();
			}
			
		}
		return $this->groupList;
		
	}
	
	public function saveGroup($group_id,$description,$orden){
		try{
			$coreResource = Mage::getSingleton('core/resource');
			$connection = $coreResource->getConnection('core_write');
			if($this->groupExists($group_id,$this->getGroupList())){
				$fields = array('description' => $description,
						'orden'=>$orden);
				$where = $connection->quoteInto('group_id =?', $group_id);
				$connection->update($this->gtn('sqlrpt_group'), $fields, $where);
			}else{
				$fields = array(
						'group_id' => $group_id,
						'description' => $description,
						'orden'=>$orden);
				$connection->insert($this->gtn('sqlrpt_group'),$fields);
				$this->groupList[]=array($group_id,$description);
			}
			$connection->commit();
			return true;
		}catch (Exception  $err){
			return $err->getMessage();
		}
	}
	
	public function saveGroupByList($lista){
		$groupListModified = array();
		$groupsList = explode("|",$lista);
		$coreResource = Mage::getSingleton('core/resource');
		$connection = $coreResource->getConnection('core_write');
		try{
			$connection->beginTransaction();
			foreach($groupsList as $groupData){
				
				if($groupData!=""){
					$groupDataArray = explode(";",$groupData);
					$code = $groupDataArray[0];
					$description = $groupDataArray[1];
					$orden = $groupDataArray[2];
					$this->saveGroup($code,$description,$orden);
					$groupListModified[]=array($code,$description);
				}
			}
			
			// Eliminamos lo que no hayan aparecido
			foreach($this->getGroupList() as $grupo){
				if(!$this->groupExists($grupo[0], $groupListModified)){
					if($this->getReportsNumber($grupo[0])>0){
						$connection->rollBack();
						$data[0] = array("type" => "error-msg",
										 "msg" => "The ".$grupo[0]." group can not be deleted because it is associated reports");
						return json_encode($data);
					}else {
						$where = $connection->quoteInto('group_id =?', $grupo[0]);
						$connection->delete($this->gtn('sqlrpt_group'), $where);
					}
				}
			}
			
			$connection->commit();
	
			$data[0] = array("type" => "success-msg",
					"msg" => "Groups successfully saved");
			return json_encode($data);
		}catch (Exception  $err){
			$connection->rollBack();
			$data[0] = array("type" => "error-msg",
							 "msg" => $err->getMessage());
			return json_encode($data);
		}
	}
	
	protected function groupExists($group_id,$groupLista){
		foreach($groupLista as $grupo){
			if($grupo[0]==$group_id){
				return true;
			}
		}
		return false;
	}
	
	public function getReportsNumber($group_id){
		$coreResource = Mage::getSingleton('core/resource');
		$connection = $coreResource->getConnection('core_write');
		// SELECT COUNT(1) AS numero FROM sqlrpt_report where group_id='$group_id'
		$select = $connection->select()
					->from($this->gtn('sqlrpt_report'), array('numero' => 'COUNT(1)'))
					->where('group_id = ?',$group_id);
		
		$readresult=$connection->fetchRow($select);
		return $readresult['numero'];
	}
	
	public function gtn($tableName){
		return Mage::getSingleton('core/resource')->getTableName($tableName);
	}
	
}
?>