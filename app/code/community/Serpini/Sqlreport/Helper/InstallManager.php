<?php
/**
 * NOTICE OF LICENSE
 *
 */

class Serpini_Sqlreport_Helper_InstallManager extends Mage_Core_Helper_Abstract
{
	protected $versionSql2Report = "0.3.2"; 
	
	public function checkInstall(){
		$utils = Mage::helper('sqlreport/Utils');
		$currentConfigVersion = $this->getActualVersion();
		$importPath = Mage::getBaseDir().DS."app".DS."code".DS."community".DS."Serpini".DS."Sqlreport".DS."sql".DS."sqlreport_setup";	
		Mage::log(get_class($this).'.'.__FUNCTION__.' currentConfigVersion:'.$currentConfigVersion.' versionSql2Report:'.$this->versionSql2Report, Zend_Log::INFO,'sqlreport.log');
		if(version_compare($this->versionSql2Report,$currentConfigVersion)>0){
			Mage::log(get_class($this).'.'.__FUNCTION__.' upgrade', Zend_Log::INFO,'sqlreport.log');

			if($this->runDbSchemaUpgrade($currentConfigVersion)){
				if($currentConfigVersion=="0.0.0"){
					$importPath = Mage::getBaseDir().DS."app".DS."code".DS."community".DS."Serpini".DS."Sqlreport".DS."sql".DS."sqlreport_setup";
					$this->importXML($importPath.DS."reports.xml");
					$importReports = $utils->getFilesDirectoryByExtension($importPath,"json");
					$impotadoBien = true;
					foreach($importReports as $report ){
						$estadoImportacion = $this->importJSON($importPath.DS.$report);
						foreach ($estadoImportacion as $estado){
							if($estado['type']=="error-msg"){
								$this->printMessage("error",$estado['msg']);
								$impotadoBien=false;
							}
						}
					}
					if($impotadoBien){
						$this->printMessage("success","Instalation plugin Sql2Report correctly");
					}else{
						$this->printMessage("warning","Instalation plugin Sql2Report correctly but errors importing reports");
					}
					
				}else{
					$this->printMessage("success","Upgrade plugin Sql2Report from ".$currentConfigVersion." to ".$this->versionSql2Report." correctly");
				}
				
				$mageCore = new Mage_Core_Model_Config();
				$mageCore->saveConfig('serpini/sqlreport/version', $this->versionSql2Report, 'default', 0);
				$this->getActualVersion();
				
			}
		}
	}
	
	public function getActualVersion(){
		//$currentConfigVersion = Mage::getConfig()->getNode('default/serpini/sqlreport/version');
		//if($currentConfigVersion == ""){
			$connection = Mage::getSingleton('core/resource')->getConnection('core_read');
			$select = $connection->select()
			->from($this->gtn('core_config_data'), array('value'))
			->where('path = ?','serpini/sqlreport/version');
			try{
				$readresult=$connection->fetchAll($select);
				if(sizeof($readresult)>0){
					foreach ($readresult as $fila){
						$currentConfigVersion = $fila['value'];
					}
				}else{
					$select = $connection->select()
					->from($this->gtn('sqlrpt_combo'), array('combo_id','description','type','parameter'))
					->order('combo_id');
					try{
						$readresult=$connection->fetchAll($select);
						$currentConfigVersion="0.1.0";
					}catch (Exception  $err){
						$currentConfigVersion="0.0.0";
					}
				}
	
			}catch (Exception  $err){
				$currentConfigVersion="0.0.0";
			}	
		//}
		return $currentConfigVersion;
	}
	
	public function getDbSchema ($fromVersion, $returnComplete=false)
	{
		$instructions = array();
		$actualVersion = $fromVersion;
		if(version_compare("0.1.0",$actualVersion)>0){
			$actualVersion = "0.1.0";
			$instructions = array_merge(
					$instructions,
						array(array("type" => "table", "name" => "sqlrpt_combo", "items" =>
								array(
										array("sql-column", "combo_id", "varchar(32) NOT NULL"),
										array("sql-column", "description", "varchar(32) NOT NULL"),
										array("sql-column", "type", "varchar(10) NOT NULL"),
										array("sql-column", "parameter", "varchar(32) NOT NULL"),
										array("key", "PRIMARY KEY", "combo_id")
								)
						)),array(array("type" => "table", "name" => "sqlrpt_combo_int", "items" =>
									array(
										array("sql-column", "value_id", "int(10) unsigned NOT NULL AUTO_INCREMENT"),
										array("sql-column", "type_id", "int(10) NOT NULL"),
										array("sql-column", "combo_id", "varchar(32) NOT NULL"),
										array("sql-column", "value", "int(10) NOT NULL"),
										array("key", "PRIMARY KEY", "value_id")
									)
						)),array(array("type" => "table", "name" => "sqlrpt_combo_text", "items" =>
									array(
										array("sql-column", "value_id", "int(10) unsigned NOT NULL AUTO_INCREMENT"),
										array("sql-column", "type_id", "int(10) NOT NULL"),
										array("sql-column", "combo_id", "varchar(32) NOT NULL"),
										array("sql-column", "value", "text NOT NULL"),
										array("key", "PRIMARY KEY", "value_id")
									)
						)),array(array("type" => "table", "name" => "sqlrpt_combo_type", "items" =>
									array(
										array("sql-column", "type_id", "int(10) unsigned NOT NULL AUTO_INCREMENT"),
										array("sql-column", "type_code", "varchar(32) NOT NULL"),
										array("sql-column", "type", "varchar(10) NOT NULL"),
										array("key", "PRIMARY KEY", "type_id")
									)
						)),array(array("type" => "table", "name" => "sqlrpt_combo_varchar", "items" =>
									array(
										array("sql-column", "value_id", "int(10) unsigned NOT NULL AUTO_INCREMENT"),
										array("sql-column", "type_id", "int(10) NOT NULL"),
										array("sql-column", "combo_id", "varchar(32) NOT NULL"),
										array("sql-column", "value", "varchar(32) NOT NULL"),
										array("key", "PRIMARY KEY", "value_id")
									)
						)),array(array("type" => "table", "name" => "sqlrpt_group", "items" =>
									array(
										array("sql-column", "group_id", "varchar(32) NOT NULL"),
										array("sql-column", "description", "varchar(32) NOT NULL"),
										array("sql-column", "orden", "int(10) NOT NULL"),
										array("key", "PRIMARY KEY", "group_id")
									)
						)),array(array("type" => "table", "name" => "sqlrpt_report", "items" =>
									array(
										array("sql-column", "report_id", "varchar(32) NOT NULL"),
										array("sql-column", "description", "varchar(32) NOT NULL"),
										array("sql-column", "report_sql", "text NOT NULL"),
										array("sql-column", "group_id", "varchar(32) NOT NULL"),
										array("key", "PRIMARY KEY", "report_id"),
										array("constraint", "sqlrpt_report_group_fk", "group_id","sqlrpt_group","group_id")
									)
						)),array(array("type" => "table", "name" => "sqlrpt_report_combo", "items" =>
									array(
										array("sql-column", "id", "int(10) unsigned NOT NULL AUTO_INCREMENT"),
										array("sql-column", "report_id", "varchar(32) NOT NULL"),
										array("sql-column", "combo_id", "varchar(32) NOT NULL"),
										array("sql-column", "order_n", "int(10) NOT NULL"),
										array("key", "PRIMARY KEY", "id")
									)
						)),array(array("type" => "table", "name" => "sqlrpt_report_int", "items" =>
									array(
										array("sql-column", "value_id", "int(10) unsigned NOT NULL AUTO_INCREMENT"),
										array("sql-column", "type_id", "int(10) NOT NULL"),
										array("sql-column", "report_id", "varchar(32) NOT NULL"),
										array("sql-column", "value", "int(10) NOT NULL"),
										array("key", "PRIMARY KEY", "value_id")
									)
						)),array(array("type" => "table", "name" => "sqlrpt_report_text", "items" =>
									array(
										array("sql-column", "value_id", "int(10) unsigned NOT NULL AUTO_INCREMENT"),
										array("sql-column", "type_id", "int(10) NOT NULL"),
										array("sql-column", "report_id", "varchar(32) NOT NULL"),
										array("sql-column", "value", "text NOT NULL"),
										array("key", "PRIMARY KEY", "value_id")
									)
						)),array(array("type" => "table", "name" => "sqlrpt_report_type", "items" =>
									array(
										array("sql-column", "type_id", "int(10) unsigned NOT NULL AUTO_INCREMENT"),
										array("sql-column", "type_code", "varchar(32) NOT NULL"),
										array("sql-column", "type", "varchar(10) NOT NULL"),
										array("key", "PRIMARY KEY", "type_id")
									)
						)),array(array("type" => "table", "name" => "sqlrpt_report_varchar", "items" =>
									array(
										array("sql-column", "value_id", "int(10) unsigned NOT NULL AUTO_INCREMENT"),
										array("sql-column", "type_id", "int(10) NOT NULL"),
										array("sql-column", "report_id", "varchar(32) NOT NULL"),
										array("sql-column", "value", "varchar(32) NOT NULL"),
										array("key", "PRIMARY KEY", "value_id")
									)
						)),array(array("type" => "table", "name" => "sqlrpt_setup", "items" => 
								array(
										array("sql-column", "value_id", "int(10) unsigned NOT NULL AUTO_INCREMENT"),
										array("sql-column", "name", "varchar(32) NOT NULL"),
										array("sql-column", "value", "text NOT NULL"),
										array("key", "PRIMARY KEY", "value_id")
									)
						)),array(array("type" => "insert", "name" => "sqlrpt_setup", "items" => 
								array(
									array("sql-column", "name", "prefix_parameter"),
									array("sql-column", "value", ":")
									)		
						)),array(array("type" => "insert", "name" => "sqlrpt_setup", "items" => array(
								array("sql-column", "name", "prefix_table"),
								array("sql-column", "value", "@")
									)
						)),array(array("type" => "insert", "name" => "sqlrpt_setup", "items" => array(
								array("sql-column", "name", "date_mask"),
								array("sql-column", "value", "%m/%e/%y")
									)
						)),array(array("type" => "insert", "name" => "sqlrpt_setup", "items" => array(
								array("sql-column", "name", "exp_col_delimiter"),
								array("sql-column", "value", ";")
									)
						)),array(array("type" => "insert", "name" => "sqlrpt_setup", "items" => array(
								array("sql-column", "name", "prefix_parameter"),
								array("sql-column", "value", ":")
									)
						)),array(array("type" => "insert", "name" => "sqlrpt_setup", "items" => array(
								array("sql-column", "name", "exp_dec_separator"),
								array("sql-column", "value", ".")
									)
						)),array(array("type" => "insert", "name" => "sqlrpt_setup", "items" => array(
								array("sql-column", "name", "svn_print_header"),
								array("sql-column", "value", "true")
									)
						)),array(array("type" => "insert", "name" => "sqlrpt_report_type", "items" => array(
								array("sql-column", "type_code", "order"),
								array("sql-column", "type", "int")
									)
						)),array(array("type" => "insert", "name" => "sqlrpt_combo_type", "items" => array(
								array("sql-column", "type_code", "sql"),
								array("sql-column", "type", "text")
									)
						)),array(array("type" => "insert", "name" => "sqlrpt_combo_type", "items" => array(
								array("sql-column", "type_code", "set"),
								array("sql-column", "type", "text")
									)
						))
			);
		}
		
		if( version_compare("0.2.0",$actualVersion)>0){
			$actualVersion = "0.2.0";
			$instructions = array_merge(
					$instructions,
						array(array("type" => "table", "name" => "sqlrpt_chart_series", "items" =>
							array(
									array("sql-column", "id", "int(11) NOT NULL AUTO_INCREMENT"),
									array("sql-column", "report_id", "varchar(32) NOT NULL"),
									array("sql-column", "column_num", "int(11) NOT NULL"),
									array("sql-column", "serie_num", "int(11) NOT NULL"),
									array("key", "PRIMARY KEY", "id")
							)
						)),array(array("type" => "insert", "name" => "sqlrpt_setup", "items" =>
							array(
									array("sql-column", "name", "chart_height_default"),
									array("sql-column", "value", "400")
							)
						)),array(array("type" => "sql-column-change", "table" => "sqlrpt_report", 
								"oldname" => "description",
								"newname" => "title",
								"newtype" => "varchar(32)"
						)),array(array("type" => "sql-column-change", "table" => "sqlrpt_combo", 
								"oldname" => "description",
								"newname" => "title",
								"newtype" => "varchar(32)"
						)),array(array("type" => "insert", "name" => "sqlrpt_report_type", "items" => array(
								array("sql-column", "type_code", "chart_type"),
								array("sql-column", "type", "varchar")
									)
						)),array(array("type" => "insert", "name" => "sqlrpt_report_type", "items" => array(
								array("sql-column", "type_code", "chartXValue"),
								array("sql-column", "type", "int")
									)
						)),array(array("type" => "insert", "name" => "sqlrpt_report_type", "items" => array(
								array("sql-column", "type_code", "chart_height"),
								array("sql-column", "type", "int")
									)
						)),array(array("type" => "table", "name" => "sqlrpt_report_role", "items" =>
							array(
									array("sql-column", "id", "int(11) NOT NULL AUTO_INCREMENT"),
									array("sql-column", "report_id", "varchar(32) NOT NULL"),
									array("sql-column", "role", "int(10) unsigned NOT NULL"),
									array("sql-column", "read", "tinyint(1) NOT NULL"),
									array("sql-column", "edit", "tinyint(1) NOT NULL"),
									array("key", "PRIMARY KEY", "id")
							)
						))
					
			);
		}
		
		if( version_compare("0.3.0",$actualVersion)>0){
			$actualVersion = "0.3.0";
			$instructions = array_merge(
					$instructions,
						array(array("type" => "table", "name" => "sqlrpt_link", "items" =>
							array(
									array("sql-column", "link_id", "varchar(32) NOT NULL"),
									array("sql-column", "description", "varchar(1024) NOT NULL"),
									array("sql-column", "url", "varchar(1024) NOT NULL"),
									array("sql-column", "type", "varchar(32) NOT NULL"),
									array("key", "PRIMARY KEY", "link_id")
							)
						)),array(array("type" => "table", "name" => "sqlrpt_report_link", "items" =>
							array(
									array("sql-column", "id", "int(11) NOT NULL AUTO_INCREMENT"),
									array("sql-column", "report_id", "varchar(32) NOT NULL"),
									array("sql-column", "link_id", "varchar(32) NOT NULL"),
									array("sql-column", "column", "int(11) NULL"),
									array("key", "PRIMARY KEY", "id")
							)
						)),array(array("type" => "table", "name" => "sqlrpt_report_link_value", "items" =>
							array(
									array("sql-column", "id", "int(11) NOT NULL AUTO_INCREMENT"),
									array("sql-column", "report_link_id", "int(11) NOT NULL"),
									array("sql-column", "variable", "varchar(32) NOT NULL"),
									array("sql-column", "column_num", "int(11) NOT NULL"),
									array("key", "PRIMARY KEY", "id")
							)
						)),array(array("type" => "insert", "name" => "sqlrpt_combo_type", "items" => 
								array(
									array("sql-column", "type_code", "selectType"),
									array("sql-column", "type", "text")
									)		
						))
						
						);
		}
		 
		return $instructions;
		
	}
    
    public function gtn($tableName){
    	return Mage::getSingleton('core/resource')->getTableName($tableName);
    }
    
    public function run($sql,$printError){
    	$connection = Mage::getSingleton('core/resource')->getConnection('core_write');
    	Mage::log(get_class($this).'.'.__FUNCTION__.' Sql: '.$sql, Zend_Log::INFO,'sqlreport.log');
    	try{
    		$connection->query($sql);
    		return true;
    	}catch (Exception  $err){
    		Mage::log(get_class($this).'.'.__FUNCTION__.' Error: '.$err->getMessage(), Zend_Log::ERR,'sqlreport.log');
    		if($printError) $this->printMessage("error",$err->getMessage()."<br/>".$sql);
			return false;
		}
    }

    public function runDbSchemaUpgrade ($fromVersion)
    {
    	$continue=false;
    	$instructions = $this->getDbSchema($fromVersion);
    	
        foreach ($instructions as $instruction) {
        	try{
        		Mage::log(get_class($this).'.'.__FUNCTION__.' Instruccion: '.implode("; ", $instruction), Zend_Log::INFO,'sqlreport.log');
        	} catch (Exception $e) {
                Mage::logException($e);
            }
            switch ($instruction['type']) {
                case 'table':
                    $keys = array();
                    $columns = array();

                    foreach ($instruction['items'] as $item) {
                        switch ($item[0]) {
                            case 'sql-column':
                                $columns[] = '`'.$item[1].'` '.$item[2];
                                break;
                            case 'key':
                                $keys[] = $item[1] .' (`'.$item[2].'`)';
                                break;
                            case 'constraint':
                            	$keys[] = 'CONSTRAINT `'.$item[1] .'` FOREIGN KEY (`'.$item[2].'`) REFERENCES `'.$this->gtn($item[3]).'` (`'.$item[4].'`)';
                            	break;
                            	
                        }
                    }
                    $tableDetails = implode(",",array_merge($columns,$keys));
                    $sql = "DROP TABLE IF EXISTS `{$this->gtn($instruction['name'])}`;\n";
                    $sql .="CREATE TABLE `{$this->gtn($instruction['name'])}` (".$tableDetails.") ;";
                    try {
                        $continue=$this->run($sql,true);
                    } catch (Exception $e) {
                        Mage::logException($e);
                    }
                    break;
                case 'sql-column':
                    try {
                        $continue = $this->run("
                        ALTER TABLE `{$this->gtn($instruction['table'])}` ADD COLUMN `{$instruction['name']}` {$instruction['params']}",true);
                    } catch (Exception $e) {
                        Mage::logException($e);
                    }
                    break;
                case 'sql-column-delete':
                    try{
                    	$continue = $this->run("
                        ALTER TABLE `{$this->gtn($instruction['table'])}` DROP COLUMN `{$instruction['name']}`",true);
                    } catch (Exception $e) {
                        Mage::logException($e);
                    }
                    break;
                case 'index':
                    try {
                        $columns = implode(',',$instruction['on']);
                        $continue = $this->run("
                            {$instruction['name']} ON `{$this->gtn($instruction['table'])}` ({$columns})
                        ",true);
                    } catch (Exception $e) {
                        Mage::logException($e);
                    }
                    break;
                 case 'insert':
                 	
                    	$keys = array();
                    	$columns = array();
                    	$values = array();
                    
                    	foreach ($instruction['items'] as $item) {
                    		switch ($item[0]) {
                    			case 'sql-column':
                    				$columns[] = '`'.$item[1].'`';
                    				$values[] = (gettype($item[2])!="string")?$item[2]: "'".$item[2]."'";
                    				break;
                    				 
                    		}
                    	}
                    	$tableDetails = implode(",",array_merge($columns,$keys));
                    	$tableValues = implode(",",array_merge($values,$keys));
                    	$sql ="INSERT INTO `{$this->gtn($instruction['name'])}` (".$tableDetails.") VALUES (".$tableValues.");";
                    	try {
                    		$continue=$this->run($sql,true);
                    	} catch (Exception $e) {
                    		Mage::logException($e);
                    	}
                    	break;
                case 'sql-column-change':
                    		try{
                    			$continue = $this->run("
                    					ALTER TABLE `{$this->gtn($instruction['table'])}` CHANGE {$instruction['oldname']} {$instruction['newname']} {$instruction['newtype']}".";",true);
                    			} catch (Exception $e) {
                    			Mage::logException($e);
                    	}
                    	break;
                case 'error':
                	try {
                        $continue = $this->run("sql error",true);
                    } catch (Exception $e) {
                        Mage::logException($e);
                    }
                    break;
                case 'delete':

                    	$where = array();
                    	foreach ($instruction['items'] as $item) {
                    		switch ($item[0]) {
                    			case 'sql-column':
                    				$value = (gettype($item[2])!="string")?$item[2]: "'".$item[2]."'";
                    				$where[] = $item[1]." = ".$value;
                    				break; 
                    		}
                    	}
                    	$where_clausure = implode(" AND ",$where);
                    	$sql ="DELETE FROM ".$this->gtn($instruction['name'])." WHERE ".$where_clausure;
                    	$continue=$this->run($sql,true);
                    	
                	break;
            }
            
			/*if(!$continue){
				
				$this->resetDb($fromVersion);
				if($fromVersion=="0.0.0"){
					$this->printMessage("error","Instalation plugin Sql2Report faliled");
				}else{
					$this->printMessage("error","Upgrade plugin Sql2Report from ".$fromVersion." to ".$this->versionSql2Report." faliled");
				}
				return false;
			}*/
        }
        Mage::log(get_class($this).'.'.__FUNCTION__.' Upgrade finish estatus : '.$continue, Zend_Log::INFO,'sqlreport.log');
        return true;
        
        
    }
    
    public function resetDb ($fromVersion)
    {
        foreach ($this->getDbSchema($fromVersion) as $instruction) {
            switch ($instruction['type']) {
                case 'table':
                    $sql = "DROP TABLE IF EXISTS `{$this->gtn($instruction['name'])}`;\n";
                    $return = $this->run($sql,false);
                    break;
                case 'sql-column':
                    try {
                        $return = $this->run("
                            ALTER TABLE `{$this->gtn($instruction['table'])}` DROP COLUMN `{$instruction['name']}`
                        ",false);
                    } catch (Exception $e) {
                        Mage::logException($e);
                    }
                    break;
            }
        }
    }
    
    public function importXML($filename){
  		$estadoFunction = true;
    	$connection = Mage::getSingleton('core/resource')->getConnection('core_write');
    	$xmlObj = new Varien_Simplexml_Config($filename);
    	$xmlData = $xmlObj->getNode();
    	$connection->beginTransaction();
    	foreach ($xmlData as $tabla=>$columnas){
    		$dataInsert = array();
    		foreach ($columnas as $columnName=>$columnValue){
    			$dataInsert[$columnName]=(string)$columnValue;
    		}
    		try{
    			$connection->insert($this->gtn($tabla),$dataInsert);
    		} catch (Exception $e) {
    			$estadoFunction = false;
    			$this->printMessage("warning","Not import data into table ".$tabla." data:".var_dump($dataInsert)." ERROR:".$e->getMessage());
    		}
    		
    	}
    	$connection->commit();
    	return $estadoFunction;
    }
    
    public function importJSON($filename){
    	$estado = array();
    	$filecontect = file_get_contents ($filename);
    	$data = json_decode($filecontect, TRUE);
    	$groupmanager = Mage::helper('sqlreport/GroupManager');
    	$combomanager = Mage::helper('sqlreport/ComboManager');
    	$reportmanager = Mage::helper('sqlreport/ReportManager');
    	$linkmanager = Mage::helper('sqlreport/LinkManager');
    	$group_description="";
    	$links = array();
    	foreach ($data as $dataImport) {
    		switch ($dataImport['object_type']) {
    			case 'group':
    					$result=$groupmanager->saveGroup($dataImport['group_id'],$dataImport['description'],$dataImport['orden']);
    					$group_description=$dataImport['description'];
    					if($result===true){
    						$estado[] = array("type" => "success-msg",
    								"msg" => "Group ".$dataImport['description']." (".$dataImport['group_id'].") imported");
    					}else{
    						$estado[] = array("type" => "error-msg",
    										  "msg" => "Error importing group  ".$dataImport['description']."(".$dataImport['group_id'].") :".$result,
    										  "object_type" => $dataImport['object_type']);
    					}
    				break;
    			case 'combo':
    				if($combomanager->comboExists($dataImport['combo_id'])){
    					$estado[] = array("type" => "warning-msg",
    								"msg" => "The filter  ".$dataImport['title']." (".$dataImport['combo_id'].") already exists, The filter will not be imported",
    								"object_type" => $dataImport['object_type']);

    				}else{
    					$result=$combomanager->addCombo($dataImport['combo_id'],$dataImport['title'],$dataImport['parameter'],$dataImport['type'],$dataImport['atributes']);
    					if($result===true){
    						$estado[] = array("type" => "success-msg",
    								"msg" => "Filter ".$dataImport['title']." (".$dataImport['combo_id'].") imported");
    					}else{
    						$estado[] = array("type" => "error-msg",
    								"msg" => "Error importing filter  ".$dataImport['title']."(".$dataImport['combo_id'].") :".$result,
    								"object_type" => $dataImport['object_type']);
    					}
    				}
    				break;
    			case 'report':
    				// Recorremos el array de resultados, si hay alg�n error no se inserta
    				$hayerror = false;
    				foreach ($result as $resu){
    					if($resu['type']=="error-msg"){
    						$hayerror=true;
    					}
    				}
    				if(!$hayerror){
    					if($reportmanager->reportExists($dataImport['report_id'],'edit')){
    						$estado[] = array("type" => "warning-msg",
    								"msg" => "The report  ".$dataImport['title']." (".$dataImport['report_id'].") already exists, The report will not be imported",
    								"object_type" => $dataImport['object_type']);
    											  
    					}else{
    						
    						$result=$reportmanager->addNewReport($dataImport['report_id'],$dataImport['title'],$dataImport['report_sql'],$dataImport['group_id'],$dataImport['atributes'],$dataImport['combos'],$dataImport['chart_series'],$links);
    						if($result){
    							$estado[] = array("type" => "success-msg",
    											  "msg" => "Report  ".$dataImport['title']."(".$dataImport['report_id'].") imported",
    											  "object_type" => $dataImport['object_type'],
    											  "report_id"=>$dataImport['report_id'],
    											  "description"=>$dataImport['title'],
    									          "group_description"=>$group_description);
    						}else{
    							$estado[] = array("type" => "error-msg",
    									"msg" => "Error importing report  ".$dataImport['title']." (".$dataImport['report_id'].") :".$result,
    									"object_type" => $dataImport['object_type']);
    						}
    						
    					}
    					
    				}else{
    					$estado[] = array("type" => "error-msg",
    							"msg" => "There are some error importing, please solve it to import report ",
    							"object_type" => $dataImport['object_type']);
    				}
    				break;
    			case 'link':
    				if($linkmanager->linkExists($dataImport['id'])){
    					$estado[] = array("type" => "warning-msg",
    								"msg" => "The link  ".$dataImport['description']." (".$dataImport['id'].") already exists, The link will not be imported",
    								"object_type" => $dataImport['object_type']);
						$link = Mage::getModel('sqlreport/link');
    					$link->loadMe2($dataImport['id'],$dataImport['description'],$dataImport['url'],$dataImport['type']);
    					if("TD"==$dataImport['type']){
    						$link->setColumn($dataImport['column']);
    					}else{
    						foreach ($dataImport['variables'] as $key => $value){
								$link->addVariable($dataImport['variables'][$key][0],$dataImport['variables'][$key][1]);
							}
    					}
    					array_push($links,$link);
    				}else{
    					$result=$linkmanager->createLink($dataImport['id'],$dataImport['description'],$dataImport['url'],$dataImport['type']);
    					
    					if($result===true){
    						$link = Mage::getModel('sqlreport/link');
    						$link->loadMe2($dataImport['id'],$dataImport['description'],$dataImport['url'],$dataImport['type']);
    						if("TD"==$dataImport['type']){
    							$link->setColumn($dataImport['column']);
    						}else{
	    						foreach ($dataImport['variables'] as $key => $value){
									$link->addVariable($dataImport['variables'][$key][0],$dataImport['variables'][$key][1]);
								}
    						}
    						array_push($links,$link);
    						
    						$estado[] = array("type" => "success-msg",
    								"msg" => "Link ".$dataImport['description']." (".$dataImport['id'].") imported");
    					}else{
    						$estado[] = array("type" => "error-msg",
    								"msg" => "Error importing link  ".$dataImport['description']."(".$dataImport['id'].") :".$result,
    								"object_type" => $dataImport['object_type']);
    					}
    				}
    				break;
    		}
    	}
    	return $estado;
    	
    }
    
    public function printMessage($type,$message){
    	echo  "<ul class=\"messages\"><li class=\"".$type."-msg\"><ul><li>".$message."</li></ul></li></ul>";
    }
  

}
