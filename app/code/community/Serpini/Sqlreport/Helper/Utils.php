<?php
class Serpini_Sqlreport_Helper_Utils extends Mage_Core_Helper_Abstract
{
	
	public function getFilesDirectoryByExtension($path,$extension){
    	$lista = array();
    	try{
    		$directorio=opendir($path);
	    	while ($archivo = readdir($directorio)){
	    		$ext = pathinfo($archivo, PATHINFO_EXTENSION);
	    		if($ext == $extension){
	    			$lista[] = $archivo;
	    		}
	    	}
    	}catch (Exception $e) {}
    	closedir($directorio); 
    	
		return $lista;
    }
    
	public function gtn($tableName){
    	return Mage::getSingleton('core/resource')->getTableName($tableName);
    }
    
    /* Devuelve el tipo de dato pasado
     *  return: 0: Null
     *  		1: String
     *  		2: Number
     *  		3: Boolean
     */
    public function getTypeData($data){
    	if(is_null($data)){
    		return 0;
    	}else if(""==$data){
    		return 0;
    	}else if(is_bool($data)){
    		return 3;
    	}else if(is_numeric($data)){
    		return 2;
    	}else{
    		return 1;
    	}
    	
    }
    
}