<?php
class Serpini_Sqlreport_Helper_LinkManager extends Mage_Core_Helper_Abstract {
	
	protected $linkListTR = null;
	protected $linkListTD = null;
	
	public function getLinkList($type){
		if("TR" == $type){
			if($this->linkListTR == null){
				$this->linkListTR= $this->getLinkListDB($type);
			}
			return $this->linkListTR;
		} else if("TD" == $type){
			if($this->linkListTD == null){
				$this->linkListTD= $this->getLinkListDB($type);
			}
			return $this->linkListTD;
		} else {
			return array();
		}
	}
	
	private function getLinkListDB($type){
		$utils = Mage::helper('sqlreport/Utils');
		$linkList = array();
		$coreResource = Mage::getSingleton('core/resource');
		$connection = $coreResource->getConnection('core_read');
		$select = $connection->select()
					->from($utils->gtn('sqlrpt_link'), array('link_id','description','url','type'))
					->where('type = ?',$type)
					->order('link_id');
		try{
			$readresult=$connection->fetchAll($select);
			foreach ($readresult as $fila){
				$linkList[]=array($fila['link_id'],$fila['description'],$fila['url'],$fila['type']);
			}
			return $linkList;
		}catch (Exception  $err){
			echo $err->getMessage();
		}
	}
	
	public function updateLink($linkId,$description,$url,$type){
		if($this->linkExists($linkId)){
			$link = Mage::getModel('sqlreport/link');
			$link->loadMe2($linkId,$description,$url,$type);
			return $link->saveLink();
		}else{
			return "link not exists";
		}
	}
	
	public function createLink($linkId,$description,$url,$type){
		if(!$this->linkExists($linkId)){
			$link = Mage::getModel('sqlreport/link');
			$link->loadMe2($linkId,$description,$url,$type);
			return $link->addMeAsNew();
		}else{
			return "the link already exists";
		}
	}
	
	public function linkExists($linkId){
		$linkLista=$this->getLinkList("TR");
		foreach($linkLista as $link){
			if($link[0]==$linkId){
				return true;
			}
		}
		
		$linkLista=$this->getLinkList("TD");
		foreach($linkLista as $link){
			if($link[0]==$linkId){
				return true;
			}
		}
		return false;
	}
	
	public function deleteLink($linkId){
		$link = Mage::getModel('sqlreport/link');
    	$link->loadMe($linkId);
    	return $link->delete();
		
	}
}