<?php
/**
 * BSeller Platform | B2W - Companhia Digital
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category  BSeller
 * @package   BSeller_Thai_S3
 *
 * @copyright Copyright (c) 2018 B2W Digital - BSeller Platform. (http://www.bseller.com.br)
 *
 * @author   Danilo Cavalcanti <danilo.moura@e-smart.com.br>
 */

class Thai_S3_Block_Adminhtml_Block_System_Config_System_Export
    extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    /*
 * Set template
 */
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('bseller/thai_s3/system/config/export/button.phtml');
    }

    /**
     * Remove scope label
     *
     * @param  Varien_Data_Form_Element_Abstract $element
     * @return string
     */
    public function render(Varien_Data_Form_Element_Abstract $element)
    {
        $element->unsScope()->unsCanUseWebsiteValue()->unsCanUseDefaultValue();
        return parent::render($element);
    }

    /**
     * Return element html
     *
     * @param  Varien_Data_Form_Element_Abstract $element
     * @return string
     */
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        return $this->_toHtml();
    }

    /**
     * Return ajax url for export button
     *
     * @return string
     */
    public function getExportUrl()
    {
        return Mage::getSingleton('adminhtml/url')->getUrl('*/thais3export/getFiles');
    }

    /**
     * Generate export button html
     *
     * @return string
     */
    public function getButtonHtml()
    {
        $button = $this->getLayout()->createBlock('adminhtml/widget_button')
            ->setData(array(
                'id'        => 'export_button',
                'label'     => $this->helper('adminhtml')->__('Export'),
                'onclick'   => 'javascript:exportFiles(); return false;'
            ));

        return $button->toHtml();
    }
}