<?php 

//Lista de pastas a serem ignoradas ex.: 'skin/, app/'
$excludeList = array('skin/frontend/sephora/default/images/sprite/'); 

//Pasta raiz de onde o script irá começar a percorrer
$di  = new RecursiveDirectoryIterator(getcwd());

//Interador recursivo
$iter  = new RecursiveIteratorIterator($di);

//Interar por cada arquivo através de expressão regular, neste caso .png e .jpg
$regexIter = new RegexIterator($iter, '/^.+\.(jpg|png)$/i', RecursiveRegexIterator::GET_MATCH);

//Contador
$count = 0;

//Scaneando arquivos
foreach ($regexIter as $fileInfo) { //$fileInfo[0] = path/to/image.png
    $continue = false;

    //Verificando pastas ignoradas
    foreach ($excludeList as $excludeDir) {
        if (strripos($fileInfo[0], $excludeDir)) {
            $continue = true;
        }
    }

    //Ignorando
    if ($continue) {
        continue;
    }

    //Inicio da compressão de images png 
    if (strripos($fileInfo[0], '.png')) {

        //Criando uma nova imagem apartir do arquivo encontrado
        //return true or false
        $img = imagecreatefrompng($fileInfo[0]); 

        if ($img) {

            // imagepng(source, destination, quality)
            // Qualidade da imagem png entre 0 - 9
            $compressImage = imagepng($img,$fileInfo[0], 9);

            if ($compressImage) {
                echo "\n\nCompress image PNG: " . $fileInfo[0];
                $count++;
            }
        }        
    }

    //Inicio da compressão de images jpg
    if (strripos($fileInfo[0], '.jpg')) {

        //Criando uma nova imagem apartir do arquivo encontrado
        //return true or false
        $img = imagecreatefromjpeg($fileInfo[0]);   
        
        if ($img) {

            // imagepng(source, destination, quality)
            // Qualidade da imagem png entre 0 - 100                
            $compressImage = imagejpeg($img,$fileInfo[0], 100);      

            if ($compressImage) {
                echo "\n\nCompress image JPEG: " . $fileInfo[0];
                $count++;
            }
        }
    }    
} 

echo 'Total of  ' . $count . ' images compress!';