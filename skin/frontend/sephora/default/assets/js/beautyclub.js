/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition End User License Agreement
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magento.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    sephora
 * @package     sephora_default
 * @copyright   Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license     http://www.magento.com/license/enterprise-edition
 * @author      Tiago Silva <tiago.silva@e-smart.com.br>
 */

$j(document).ready(function() {

    // dashboard
    $j('.beautyclub-slider .navigation li[id=beautyclub-slider_one]').addClass('active');
    $j('.beautyclub-slider .banners li[tab=beautyclub-slider_one]').show();

    $j('.beautyclub-slider .navigation li').click(function(e) {
        e.preventDefault();
        $j('.beautyclub-slider .navigation li').removeClass('active');
        $j('.beautyclub-slider .navigation li[id=' + $j(this).attr('id') + ']').addClass('active');
        $j('.beautyclub-slider .banners li').hide();
        $j('.beautyclub-slider .banners li[tab=' + $j(this).attr('id') + ']').show();
    });

    //faq
    sephora.acordionElements('#faq');

    //faq search
    $j('#faqsearch').keyup(function() {
        var value = $j(this).val();
        var exp = new RegExp(value, 'i');
        $j('#faq dt').each(function() {
            var isMatch = exp.test($j('p', this).text());
            $j(this).toggle(isMatch);
        });
    });

});

var bc = {

    /**
     * category
     * Show points to category
     *
     * @param points
     * @param limit
     * @param content
     * @param load
     * @param carrousel
     */
    category: function(points, limit, content, load, carrousel) {
        var url = '/beautyclub/category/list/points/' + points;

        if (carrousel) {
            url = url + '/limit/' + limit;
        }

        $j.ajax({
            url: url,
            type: 'get',
            dataType: 'json',
            beforeSend: function() {
                bc.loadAjax(content, load, points, carrousel);
            },
            success: function(data) {
                bc.successAjax(content, load, points, carrousel, data);
            }
        });

    },

    /**
     * categoryAll
     * Show carrousel all points
     */
    categoryAll: function() {

        var html  = '';
        html += '<div id="bc-products-300" class="swiper-container"></div>';
        html += '<div id="bc-products-500" class="swiper-container"></div>';
        html += '<div id="bc-products-1000" class="swiper-container"></div>';

        $j('#bc-products').html(html);

        bc.category(300, 10, '#bc-products-300', 'bc-load', true);
        bc.category(500, 10, '#bc-products-500', 'bc-load', true);
        bc.category(1000, 10, '#bc-products-1000', 'bc-load', true);
    },

    /**
     * loadAjax
     * load ajax
     */
    loadAjax: function(content, load, points, carrousel) {
        $j(content).html('<div class="' + load + '"></div>');
        $j('.filter li').removeClass('active');

        if (carrousel) {
            $j('#all').addClass('active');

            return false;
        }

        $j('#' + points).addClass('active');
    },

    /**
     * successAjax
     * success ajax
     */
    successAjax: function(content, load, points, carrousel, data) {

        if (!carrousel) {
            $j(content).html('<div id="bc-products-' + points + '" class="no-slider-bc">' + data.html + '</div>');

            return false;
        }

        $j('.' + load).remove();

        $j(content).append(bc.sliderHtml(points, data));

        sephora.carouselSlider('#bc-products-' + points, 5);
    },

    /**
     * sliderHtml
     * slider html
     */
    sliderHtml: function(points, data) {
        var html  = '';

        html += '<div class="label-points">' + points + ' pontos</div>';
        html += data.html;
        if(bc.countProducts(data.html)) {
            html += '<div id="bc-products-' + points + '-prev" class="swiper-button-prev"></div>';
            html += '<div id="bc-products-' + points + '-next" class="swiper-button-next"></div>';
        }

        return html;
    },

    /**
     * countProducts
     * Count Products
     */
    countProducts: function(data) {

        if(data.length > 80) {
            return true;
        }

    }

}
