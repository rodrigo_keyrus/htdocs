/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition End User License Agreement
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magento.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    sephora
 * @package     sephora_default
 * @copyright   Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license     http://www.magento.com/license/enterprise-edition
 * @author      Tiago Silva <tiago.silva@e-smart.com.br>
 */

Validation.add('validate-postcode', 'Por favor insira um CEP com 8 dígitos.', function (value) {
    return sephora.validatePostcode(value);
});

var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);

var sephora = {

    /**
     * tabsShowHide
     * Tab's function and mobile responsive
     *
     * @param element
     * @param elementContent
     * @param elementActive
     */
    tabsShowHide: function (element, elementContent, elementActive) {

        $j(elementContent).addClass('hide');

        $j(element).click(function (e) {
            e.preventDefault();
            $j(element).toggleClass('active');
            $j(elementContent).toggleClass('hide');

            if (elementActive) {
                $j(elementActive).toggleClass('active');
            }

        });

    },

    /**
     * tabsShowHideHover
     * Tab's function mouse hover
     *
     * @param element
     * @param elementContent
     * @param elementActive
     */
    tabsShowHideHover: function (element, elementContent, elementActive) {

        $j(element).hover(function () {
            $j(element).addClass('active');
            $j(elementActive).addClass('active');
            $j(elementContent).fadeIn(200);
        }, function () {
            var timer = setTimeout(function () {
                $j(elementContent).fadeOut(100);
                $j(element).removeClass('active');
                $j(elementActive).removeClass('active');
            }, 50);
            $j(elementContent).data('hoverTimer', timer);
        });

        $j(elementContent).hover(function () {
            clearTimeout($j(this).data('hoverTimer'));
        }, function () {
            $j(this).fadeOut(100);
            $j(element).removeClass('active');
            $j(elementActive).removeClass('active');
        });

    },

    /**
     * tabsNavigation
     * Tab's function mouse hover
     *
     * @param element
     * @param elementContent
     */
    tabsNavigation: function (element, elementContent) {

        $j(element).find('li').click(function () {
            var tab = $j(this).attr('data-tab');

            $j(element).find('li').removeClass('active');
            $j(elementContent).addClass('no-display');

            $j(this).addClass('active');
            $j(tab).removeClass('no-display');
        })

    },

    /**
     * changeQuantity
     * Change product quantity, more and less function
     *
     * @param element
     * @param qty
     * @param submit
     */
    changeQuantity: function (element, qty, submit, id, name, price, category, brand, variant) {
        var currentVal = parseInt($j(element).val());

        if (!isNaN(currentVal)) {
            if (currentVal === 1 && qty === -1) {
                return;
            } else {
                $j(element).val(currentVal + qty);
                if (submit) {
                    setTimeout(function () {
                        $j(element).parents().children('form').submit();

                        if (qty === -1) {
                            dataLayer.push({
                                'event': 'removeFromCart',
                                'ecommerce': {
                                    'currencyCode': dlCurrencyCode,
                                    'remove': {
                                        'products': [{
                                            'id': id,
                                            'name': name,
                                            'price': price,
                                            'quantity': 1,
                                            'category': category,
                                            'brand': brand,
                                            'variant': variant
                                        }]
                                    }
                                }
                            });
                        } else {
                            dataLayer.push({
                                'event': 'addToCart',
                                'ecommerce': {
                                    'currencyCode': dlCurrencyCode,
                                    'add': {
                                        'products': [{
                                            'id': id,
                                            'name': name,
                                            'price': price,
                                            'quantity': 1,
                                            'category': category,
                                            'brand': brand,
                                            'variant': variant
                                        }]
                                    }
                                }
                            });
                        }
                    }, 500);
                }
            }
        }

    },

    /**
     * acordionElements
     * Acordion elements dt, dd
     *
     * @param element
     */
    acordionElements: function (element) {

        var allPanels = $j(element + ' > dd').hide();

        $j(element + ' > dt').click(function () {
            var target = $j(this).next().stop(true, true).slideToggle();

            allPanels.not(target).stop(true, true).slideUp();

            allPanels.not(target).removeClass('active');
            allPanels.not(target).prev().removeClass('active');

            target.toggleClass('active');
            target.prev().toggleClass('active');

            return false;
        });

    },

    /**
     * bannerSlider
     * Default Banner Slider
     *
     * @param element
     * @param autoplay
     * @param loop
     */
    bannerSlider: function (element, autoplay) {

        var element = new Swiper(element, {
            autoplay: {
                delay: 5000,
            }, 
            pagination: {
                el: element + '-pagination',
                clickable: true,
            },
            navigation: {
                nextEl: element + '-next',
                prevEl: element + '-prev',
            }
        });

    },

    /**
     * carouselSlider
     * Default Carousel Slider
     *
     * @param element
     * @param items
     */
    carouselSlider: function (element, items) {

        var element = new Swiper(element, {
            slidesPerView: items,
            pagination: {
                el: element + '-pagination',
                clickable: true,
            },
            navigation: {
                nextEl: element + '-next',
                prevEl: element + '-prev',
            }
        });

    },

    /**
     * achorAnimate
     * Content animate scroll
     *
     * @param element
     * @param content
     * @param header
     */
    achorAnimate: function (element, content, header) {

        var header = (header) ? $j(header).height() : 0;

        if (!$j(element).offset()) {
            return false;
        }

        if (content == 'body, html') {

            $j(content).animate({
                scrollTop: $j(element).offset().top - header
            }, 300);

            return false;
        }

        $j(content).animate({
            scrollTop: $j(content).scrollTop() + $j(element).offset().top - $j(content).offset().top
        }, 300);

        return true;

    },


    /**
     * equalheight
     *
     * @param container
     * @param cols
     */
    equalheight: function (container, cols) {
        var major = $j(container).height();

        $j(cols).css('min-height', major);
    },

    /**
     * maskAll
     */
    maskAll: function () {

        if (!isMobile) {

            // all
            $j('input[id*="cpf"]').mask('999.999.999-99');
            $j('input[id*="postcode"], input[id*="zip"]').mask('99999-999').addClass('validate-postcode');
            $j('input[id*="dob"]').mask('99/99/9999');
            $j('input[id*="telephone"], input[id*="cellphone"]').focusout(function () {
                var phone, element;
                element = $j(this);
                element.unmask();
                phone = element.val().replace(/\D/g, '');
                if (phone.length > 10) {
                    element.mask("(99) 99999-999?9");
                } else {
                    element.mask("(99) 9999-9999?9");
                }
            }).trigger('focusout');

            // checkout
            $j('#mundipagg_creditcard_1_1_cc_number, #cc_number').mask('9999 9999 9999 99?99');
            $j('#mundipagg_creditcard_cc_cid_1_1, #cc_cvv').mask('999?9');
            $j('#mundipagg_boleto_boleto_taxvat').mask('999.999.999-99');
            $j('#delivery_date').mask('99/99/9999');

            // maxlength
            /*
            $j('input[type=text]').attr('maxlength', 200);
            $j('input[id*="email"]').attr('maxlength', 60);
            $j('input[id*="street_1"]').attr('maxlength', 60);
            $j('input[id*="street_2"]').attr('maxlength', 10);
            $j('input[id*="street_3"]').attr('maxlength', 50);
            $j('input[id*="street_4"]').attr('maxlength', 35);
            $j('input[id*="city"]').attr('maxlength', 100);
            $j('input[id*="firstname"]').attr('maxlength', 35);
            $j('input[id*="lastname"]').attr('maxlength', 35);
            */

        } else {

            // all
            $j('input[id*="cpf"]').attr({
                type: 'tel',
                onkeyup: 'sephmob.Masc(this, sephmob.Cpf)',
                maxlength: 14
            });
            $j('input[id*="postcode"], input[id*="zip"]').attr({
                type: 'tel',
                onkeyup: 'sephmob.Masc(this, sephmob.Cep)',
                maxlength: 9
            });
            $j('input[id*="dob"]').attr({
                type: 'tel',
                onkeyup: 'sephmob.Masc(this, sephmob.Date)',
                maxlength: 10
            });
            $j('input[id*="telephone"], input[id*="cellphone"]').focusout(function () {
                var phone, element;
                element = $j(this);
                element.unmask();
                phone = element.val().replace(/\D/g, '');
                if (phone.length > 10) {
                    element.mask("(99) 99999-999?9");
                } else {
                    element.mask("(99) 9999-9999?9");
                }
            }).trigger('focusout');

            // checkout
            $j('#mundipagg_creditcard_1_1_cc_number, #cc_number').attr({
                type: 'tel',
                maxlength: 16
            });
            $j('#mundipagg_creditcard_cc_cid_1_1, #cc_cvv').attr({
                type: 'tel',
                maxlength: 4
            });
            $j('#mundipagg_boleto_boleto_taxvat').attr({
                type: 'tel',
                onkeyup: 'sephmob.Masc(this, sephmob.Cpf)',
                maxlength: 14
            });
            $j('#delivery_date').attr({
                type: 'tel',
                onkeyup: 'sephmob.Masc(this, sephmob.Date)',
                maxlength: 10
            });

        }

    },

    /**
     * header fixed
     *
     * @param element
     * @param classfixed
     * @param height
     */
    headerFixed: function (element, classfixed, height) {

        $j(window).scroll(function () {

            if ($j(window).scrollTop() < height) {

                $j(element).removeClass(classfixed);

                return false;
            }

            $j(element).addClass(classfixed);
        });

    },

    /**
     * seo pagination
     *
     * @param nexturl
     * @param prevurl
     * @param first
     * @param last
     */
    seoPagination: function (nexturl, prevurl, first, last) {
        var prevElement = $j('link[rel="prev"]');
        var nextElement = $j('link[rel="next"]');

        var nexturl = (nexturl) ? nexturl.replace('isAjax=1&amp;', '') : false;
        var prevurl = (prevurl) ? prevurl.replace('isAjax=1&amp;', '') : false;

        prevElement.attr('href', prevurl);
        nextElement.attr('href', nexturl);

        if (first) {
            nextElement.attr('href', nexturl);
            prevElement.remove();
        }

        if (last) {
            prevElement.attr('href', prevurl);
            nextElement.remove();
        }

    },

    /**
     * openModal
     *
     * @param element
     */
    openModal: function (element) {

        $j(element).fancybox({
            maxWidth: 800,
            maxHeight: 600,
            fitToView: false,
            width: '70%',
            height: '70%',
            autoSize: true,
            closeClick: false,
            openEffect: 'none',
            closeEffect: 'none'
        });

    },

    /**
     * customScroll
     *
     * @param element
     */
    customScroll: function (element) {

        $j(element).mCustomScrollbar({
            theme: 'dark-2'
        });

    },

    /**
     * detectIE
     *
     * @param element
     */
    detectIE: function () {
        var ua = window.navigator.userAgent;

        var msie = ua.indexOf('MSIE ');
        if (msie > 0) {
            return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
        }

        var trident = ua.indexOf('Trident/');
        if (trident > 0) {
            var rv = ua.indexOf('rv:');
            return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
        }

        var edge = ua.indexOf('Edge/');
        if (edge > 0) {
            return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
        }

        return false;
    },

    /**
     * checkIeVersion
     *
     * @param element
     */
    checkIeVersion: function (element) {

        if (sephora.detectIE()) {
            $j(element).addClass('ie' + sephora.detectIE());
        }

    },

    /**
     * checkMacOs
     *
     * @param element
     */
    checkMac: function (element) {

        if (navigator.userAgent.indexOf('Mac') > 0) {
            $j(element).addClass('mac');
        }

    },

    /**
     * miniCartHover
     * Tab's function mouse hover
     *
     * @param element
     * @param elementContent
     * @param elementActive
     */
    miniCartHover: function (element, elementContent) {
        $j(element).hover(function (e) {
            jQuery('#overlay-minicart').stop(true).show();
            $j(elementContent).stop(true).fadeIn(300);
        }, function () {
            $j(elementContent).stop(true).fadeOut(500);
            jQuery('#overlay-minicart').stop(true).fadeOut(500);
        });

        $j(elementContent).hover(function () {
            jQuery('#overlay-minicart').stop(true).show();
            $j(elementContent).stop(true).fadeIn();
        }, function () {
            $j(elementContent).stop(true).hide();
            jQuery('#overlay-minicart').stop(true).hide();
        });
    },

    hideMinicart: function () {
        jQuery('.block-cart').stop(true).hide();
        jQuery('#overlay-minicart').stop(true).hide();

        return this;
    },

    staticMinicart: function () {
        event.stopImmediatePropagation();
        jQuery('.block-cart').stop(true).show();
        jQuery('#overlay-minicart').stop(true).show();
    },

    /**
     * validatePostcode
     *
     * @param value
     */
    validatePostcode: function (value) {
        if (value.replace(/\D/g, '').length == 8) {
            return true;
        }

        return false;
    },

}

var sephmob = {

    /**
     * Masc
     *
     * @param o
     * @param f
     */
    Masc: function (o, f) {
        v_obj = o;
        v_fun = f;

        setTimeout("sephmob.Execmasc()", 1);
    },

    /**
     * ExecMasc
     */
    Execmasc: function () {
        v_obj.value = v_fun(v_obj.value)
    },

    /**
     * Cep
     *
     * @param v
     */
    Cep: function (v) {
        v = v.replace(/\D/g, "");
        v = v.replace(/^(\d{5})(\d)/, "$1-$2");

        return v;
    },

    /**
     * Tel
     *
     * @param v
     */
    Tel: function (v) {
        v = v.replace(/\D/g, "");
        v = v.replace(/^(\d{2})(\d)/g, "($1) $2");
        v = v.replace(/(\d)(\d{4})$/, "$1-$2");

        return v;
    },

    /**
     * Date
     *
     * @param v
     */
    Date: function (v) {
        v = v.replace(/\D/g, "");
        v = v.replace(/(\d{2})(\d)/, "$1/$2");
        v = v.replace(/(\d{2})(\d)/, "$1/$2");
        v = v.replace(/(\d{2})(\d{2})$/, "$1$2");

        return v;
    },

    /**
     * Cpf
     *
     * @param v
     */
    Cpf: function (v) {
        v = v.replace(/\D/g, "");
        v = v.replace(/(\d{3})(\d)/, "$1.$2");
        v = v.replace(/(\d{3})(\d)/, "$1.$2");
        v = v.replace(/(\d{3})(\d{1,2})$/, "$1-$2");

        return v;
    },

    /**
     * Num
     *
     * @param v
     */
    Num: function (v) {
        v = v.replace(/\D/g, "");

        return v;
    }

}