module.exports = function(grunt) {
	'use strict';

    //URL LOCAL PARA O BROWSERSYNC
    var $proxy = "sephora.dev.com";
    var $hash  = Math.random().toString(30).substring(5);

    // Configurations
	grunt.initConfig({

		watch: {
            gruntfile: {
                files: ['./gruntfile.js'],
            },
            src: {
                files: ['src/scss/**/*.scss','src/js/**/*.js', 'src/img/**/*'],
                tasks: ['dev']
            }
        },

		sass: {
			'build' : {
				'options': {
					'trace': true,
					'style': 'compressed',
					'noCache': true,
					'sourcemap': false
				},
				'files': [{
					'expand': true,
					'cwd': 'src/scss',
					'src': ['*.scss'],
					'dest': 'assets/css/',
					'ext': '.css'
				}]
			}
		},

		uglify: {
		    my_target: {
		      	files: [{
					expand: true,
					cwd: 'src/js',
					src: '**/*.js',
					dest: 'assets/js'
		      	}],
		      	options: {
			        beautify: false
			    }
		    }
		},

		clean: {
			contents: ['images/sprite/*']
		},

		sprite:{
			all: {
				src: ['src/img/icons-sprite/*.{png,jpg,gif}'],
				dest: 'images/sprite/' + $hash + '.png',
				destCss: 'src/scss/settings/_icons-img.scss',
				imgPath: '../../images/sprite/' + $hash + '.png',
				algorithm: 'binary-tree',
				padding: 10
			}
	    },

		imagemin: {
			dist: {
		        options: {
		        	optimizationLevel: 3
		        },
		        files: [
		        	{
			            expand: true,
			            cwd: 'assets/img/',
			            src: ['**/*.jpg'],
			            dest: 'assets/img/',
			            ext: '.jpg'
		        	},
		        	{
			            expand: true,
			            cwd: 'assets/img/',
			            src: ['**/*.png'],
			            dest: 'assets/img/',
			            ext: '.png'
					}
		        ]
		    }
		},

		browserSync: {
		    dev: {
		        bsFiles: {
		            src : [
		            	'assets/css/*.css',
		            ]
		        },
		        options: {
                    watchTask: true,		        	
                    proxy: $proxy
                }
		    }
		}
	});

	// Load Tasks
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-spritesmith');
	grunt.loadNpmTasks('grunt-contrib-imagemin');
	grunt.loadNpmTasks('grunt-browser-sync');
	grunt.loadNpmTasks('grunt-contrib-watch');

	//Tasks carregadas ao rodar o comando "grunt"
	grunt.registerTask('default', ['browserSync', 'watch']);
	//Tasks que são carregadas no evento "watch"
	grunt.registerTask('dev', ['sass', 'uglify']);
	//Tasks que são carregadas no evento "img"
	//Rodar grunt e salvar o arquivo src/scss/settings/_icons-img.scss
	grunt.registerTask('img', ['clean', 'sprite']);
	//Tasks para o build do projeto
	grunt.registerTask('build', ['imagemin:dist']);

};
