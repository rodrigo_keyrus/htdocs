/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition End User License Agreement
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magento.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    sephora
 * @package     sephora_default
 * @copyright   Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license     http://www.magento.com/license/enterprise-edition
 * @author      Tiago Silva <tiago.silva@e-smart.com.br>
 */

var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);

$j(function(){

    // header fixed
    sephora.headerFixed('.header-fixed', 'fixed', 50);

    // beautyclub hover
    sephora.tabsShowHide('.nav-beautyclub .arrow', '.beautyclub-menu');

    // mask
    sephora.maskAll();

    // modal
    sephora.openModal('.faq-help');
    sephora.openModal('.modal');

    // custom scroll - manufacturer
    sephora.customScroll('.manufacturer-index-view .brands-aplhas');

    // custom scroll - header
    sephora.customScroll('.nav-brands #brands .scrool');
    sephora.customScroll('.nav-brands #brand_category .scrool dd');
    sephora.customScroll('.header-minicart .cart-items');

    // custom scroll - product
    sephora.customScroll('.catalog-product-view .grouped-list');

    // custom scroll - customer
    sephora.customScroll('.customer-account .box-wishlist .scroll');

    // custom scroll - checkout
    if (!isMobile) {
        sephora.customScroll('.firecheckout-index-index .address-select');
        sephora.customScroll('.firecheckout-index-index .checkout-review');
    }

    // custom scroll - stores
    sephora.customScroll('.esmart-storelocator-index-index .list-stores');

    // check ie version
    sephora.checkIeVersion('html');

    // check mac osx
    sephora.checkMac('html');

});

$j(window).load(function(){

    // product fixed sidebar
    setTimeout(function(){
        sephora.equalheight('.container .col-infos', '.col-infos, .col-actions');
    }, 3000);

});
