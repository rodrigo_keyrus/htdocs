module.exports = function(grunt) {
	'use strict';

    //URL LOCAL PARA O BROWSERSYNC
    var $proxy = "sephora.dev.com";
    var $hash  = Math.random().toString(30).substring(5);

    // Configurations
	grunt.initConfig({

		watch: {
            gruntfile: {
                files: ['gruntfile.js'],
            },
            src: {
                files: ['src/scss/**/*.scss','src/js/**/*.js'],
                tasks: ['dev']
            }
        },

		sass: {
			'build' : {
				'options': {
					'trace': true,
					'style': 'compressed',
					'noCache': true,
					'sourcemap': false
				},
				'files': [{
					'expand': true,
					'cwd': 'src/scss/',
					'src': ['*.scss'],
					'dest': 'assets/css/',
					'ext': '.css'
				}]
			}
		},

        clean: {
            contents: ['images/sprite/*']
        },

        sprite:{
            all: {
                src: ['src/icons/*.{png,jpg,gif}'],
                dest: 'images/sprite/' + $hash + '.png',
                destCss: 'src/scss/settings/icons.scss',
                imgPath: '../../images/sprite/' + $hash + '.png',
                algorithm: 'binary-tree',
                padding: 10
            }
        },

		browserSync: {
		    dev: {
		        bsFiles: {
		            src : [
		            	'assets/css/*.css',
                        'assets/js/*.js'
		            ]
		        },
		        options: {
                    watchTask: true,
                    proxy: $proxy
                }
		    }
		}

	});

	// Load Tasks
	grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-spritesmith');
	grunt.loadNpmTasks('grunt-browser-sync');
	grunt.loadNpmTasks('grunt-contrib-watch');

	//Tasks carregadas ao rodar o comando "grunt"
	grunt.registerTask('default', ['browserSync', 'watch']);

	//Tasks que são carregadas no evento "watch"
	grunt.registerTask('dev', ['sass']);

    //Tasks que são carregadas no evento "img"
    //Rodar grunt e salvar o arquivo src/scss/settings/icons.scss
    grunt.registerTask('img', ['clean', 'sprite']);
};
