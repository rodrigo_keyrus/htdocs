/**
 * Smart E-commerce do Brasil Tecnologia LTDA
 *
 * DISCLAIMER
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category      Esmart
 * @package       Esmart_Sales
 *
 * @copyright     Copyright (c) 2016 Smart E-commerce do Brasil Tecnologia LTDA. (http://www.e-smart.com.br)
 *
 * @author        Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

var SalesHistory = Class.create();
SalesHistory.prototype = {

    /**
     * Initialize constructor
     */
    initialize: function()
    {
        this.addButtonEvent();
    },

    /**
     * Add button click event
     *
     * @returns {SalesHistory}
     */
    addButtonEvent: function()
    {
        var $this = this;

        var buttons = $('my-orders-table').select('.view .button');

        buttons.each(function(button) {
            button.observe('click', function(event) {
                event.preventDefault();

                var methodName = (button.hasClassName('clicked') === true)
                    ? 'hideView'
                    : 'showView';

                $this[methodName](this);

                return this;
            });
        });

        return this;
    },

    /**
     * Hide sales order view
     *
     * @param button
     * @returns {SalesHistory}
     */
    hideView: function(button)
    {
        var orderId = button.readAttribute('data-order-id');

        var view = $('order_view_' + orderId);

        if (view === null) {
            return this;
        }

        view.hide();

        button.removeClassName('clicked');
        button.update(Translator.translate('<span class="icon-arrow-down"></span>'));

        return this;
    },

    /**
     * Show sales order view
     *
     * @param button
     * @returns {SalesHistory}
     */
    showView: function(button)
    {
        var orderId = button.readAttribute('data-order-id');

        var view = $('order_view_' + orderId);

        if (view === null) {
            this.loadView(button);

            return this;
        }

        button.addClassName('clicked');
        button.update(Translator.translate('<span class="icon-arrow-up"></span>'));

        view.appear({ duration: 1.0 });

        return this;
    },

    /**
     * Load sales order view
     * Dispatch request
     *
     * @param button
     * @returns {SalesHistory}
     */
    loadView: function(button)
    {
        var $this = this;

        var orderId = button.readAttribute('data-order-id');

        new Ajax.Request('/bseller_sales/order/view', {
            method: 'POST',
            asynchronous: false,
            parameters: { order_id: orderId },
            onSuccess: function(transport)
            {
                var response = transport.responseText.evalJSON();

                if (response.error === true) {
                    console.info(response.message);

                    return this;
                }

                button.up('li').insert({
                    bottom : new Element('li', { style: 'display:none', id: 'order_view_' + orderId, class: 'sales-info' })
                        .update(new Element('li', { class: 'container' }).update(response.html))
                });

                $this.showView(button);

                return this;
            }
        });

        return this;
    }
};
