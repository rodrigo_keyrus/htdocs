<?php
require 'app/Mage.php';

try {
    Mage::app();
    Mage::getSingleton('core/session', array('name'=>'adminhtml'));

    //Check if admin user is logged
    if(!Mage::getSingleton('admin/session')->isLoggedIn()){
        echo 'Necessário estar logado no admin!';
        exit;
    }

    $resource = Mage::getSingleton('core/resource');
    $query = <<<SQL
SELECT
  cpf1.sku,
  cpf1.name,
  cpemg.value AS images
FROM catalog_product_flat_1 cpf1
  INNER JOIN catalog_product_entity_media_gallery cpemg
    ON cpf1.entity_id = cpemg.entity_id
WHERE cpf1.type_id = 'grouped'
  AND cpf1.status = '1'
  AND cpf1.visibility = '4'
ORDER BY cpf1.entity_id ASC
SQL;

    $readConnection = $resource->getConnection('core_read');
    $result = $readConnection->fetchAll($query);

    //Download file CSV
    if ($_GET['download']) {
        $delimiter = ',';
        $filename  = 'get_images_all_products_' . date('Y-m-d-his') . '.csv';

        $f = fopen(Mage::getBaseDir() . DIRECTORY_SEPARATOR . 'media' . DIRECTORY_SEPARATOR . $filename, 'w');
        $fields = array('SKU', 'Name', 'Path Image', 'Size Bytes');

        $count = 0;
        foreach ($result as $item) {
            $image = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'catalog/product' . $item['images'];

            $ch = curl_init();
            curl_setopt(
                $ch,
                CURLOPT_URL,
                $image
            );
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_VERBOSE, 1);
            curl_setopt($ch, CURLOPT_HEADER, 1);
            curl_exec($ch);
            $size = curl_getinfo($ch, CURLINFO_SIZE_DOWNLOAD);
            curl_close($ch);

            $lineData = array($item['sku'], $item['name'], $image, $size);

            if ($count == 0) {
                $lineData = $fields;
            }

            fputcsv($f, $lineData, $delimiter);

            $count++;
        }

        fseek($f, 0);
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename=' . $filename . '');
        header('Pragma: no-cache');
        readfile(Mage::getBaseDir() . DIRECTORY_SEPARATOR . 'media' . DIRECTORY_SEPARATOR . $filename);

        fclose($f);
        exit;
    }
}
catch (Exception $e) {
    echo 'Error exec script...<br/>';
    echo $e->getMessage();
}
?>

<!DOCTYPE>
<html lang="pt-BR" xml:lang="pt-BR">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <style>
            table { font-family: arial, sans-serif; border-collapse: collapse; width: 100%; font-size: 12px; }
            td,
            th { border: 1px solid #dddddd; text-align: left; padding: 8px; }
            tr:nth-child(even) { background-color: #dddddd; }
            body { max-width: 1400px; margin: 30px auto; }
        </style>
    </head>
    <body>
        <h1>Products images total: <?php echo count($result); ?></h1>
        <form action="">
            <input type="hidden" name="download" value="1" />
            <button type="submit">Download</button>
        </form>
        <table>
            <tr>
                <th>SKU</th>
                <th>Name</th>
                <th>Path Image</th>
                <th>Size bytes</th>
            </tr>
            <?php foreach ($result as $item): ?>
                <?php $tmpname = $item['name'] ?>
                <tr>
                    <th><?php echo $item['sku'] ?></th>
                    <th><?php echo $item['name'] ?></th>
                    <th><?php echo Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'catalog/product' . $item['images'] ?></th>

                    <!-- Starting get image size -->
                    <?php
                        $ch = curl_init();
                        curl_setopt(
                            $ch,
                            CURLOPT_URL,
                            Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'catalog/product' . $item['images']
                        );
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_VERBOSE, 1);
                        curl_setopt($ch, CURLOPT_HEADER, 1);
                        curl_exec($ch);
                        $size = curl_getinfo($ch, CURLINFO_SIZE_DOWNLOAD);
                        curl_close($ch);
                    ?>
                    <th><?php echo $size; ?></th>
                </tr>
            <?php endforeach; ?>
        </table>
    </body>
</html>
